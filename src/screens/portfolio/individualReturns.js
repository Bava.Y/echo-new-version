import {useFocusEffect} from '@react-navigation/native';
import React, {useCallback, useState} from 'react';
import {
  ActivityIndicator,
  Image,
  Platform,
  ScrollView,
  Text,
  View,
} from 'react-native';
import {ALPACA, AMERITRADE} from '../../assets/images/index';
import BottomNav from '../../components/BottomNav';
import HeaderNav from '../../components/HeaderNav';
import Colors from '../../utils/theme';
import styles from './style';

const IndividualReturns = (props) => {
  // Props Variables
  const {from} = props.route.params;

  // IndividualReturns Variables
  const [data, setData] = useState(null);

  useFocusEffect(
    useCallback(() => {
      setData([
        {
          symbol: 'AAPL',
          shares: '10',
          price: 150.0,
          brokerConnection: '1',
          returns: 5.23,
        },
        {
          symbol: 'ZOM',
          shares: '50',
          price: 0.5,
          brokerConnection: '0',
          returns: -15.23,
        },
      ]);
    }, []),
  );

  return (
    <>
      <HeaderNav
        title={`Individual${from ? ` ${from} ` : ' '}Returns`}
        isBack="true"
        routeName="Individual Returns"
      />

      {data ? (
        data.length != 0 ? (
          <ScrollView
            keyboardShouldPersistTaps={'handled'}
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
            style={styles.container}>
            <View style={{paddingHorizontal: 16, paddingVertical: 16}}>
              {data.map((lol, index) => (
                <View
                  key={index}
                  style={[
                    styles.companylist,
                    {marginTop: index != 0 ? 16 : 0},
                  ]}>
                  <View style={{flex: 0.2671875, justifyContent: 'center'}}>
                    <Text style={styles.companySymbolTxt}>{lol.symbol}</Text>

                    <Text style={styles.companySharesTxt}>
                      {lol.shares} Share(s)
                    </Text>
                  </View>

                  <View style={{flex: 0.2671875, justifyContent: 'center'}}>
                    <Text
                      style={{
                        fontSize: Platform.OS == 'ios' ? 10 : 12,
                        color: '#ffffffa1',
                        fontWeight: '800',
                      }}>
                      $ {lol.price}
                    </Text>
                  </View>

                  <View
                    style={{
                      flex: 0.2375,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Image
                      source={lol.brokerConnection == 1 ? AMERITRADE : ALPACA}
                      style={{width: 60, height: 40, resizeMode: 'contain'}}
                    />
                  </View>

                  <View style={{flex: 0.178125, justifyContent: 'center'}}>
                    <Text
                      style={{
                        fontSize: Platform.OS == 'ios' ? 10 : 12,
                        fontWeight: 'bold',
                        color:
                          Math.sign(lol.returns) == -1
                            ? Colors.declientColor
                            : Colors.successGreen,
                        textAlign: 'right',
                      }}>
                      {lol.returns} %
                    </Text>
                  </View>
                </View>
              ))}
            </View>
          </ScrollView>
        ) : (
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: Colors.themeColor,
            }}>
            <Text
              style={{
                fontSize: Platform.OS == 'ios' ? 14 : 16,
                fontWeight: '600',
                color: Colors.white,
              }}>
              No trade(s) found!
            </Text>
          </View>
        )
      ) : (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: Colors.themeColor,
          }}>
          <ActivityIndicator size={'large'} color={Colors.white} />
        </View>
      )}

      <BottomNav routeName="Portfolio" />
    </>
  );
};

export default IndividualReturns;
