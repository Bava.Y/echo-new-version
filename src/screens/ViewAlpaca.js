import _ from 'lodash';
import moment from 'moment';
import React, {
  useContext,
  useState,
  useRef,
  useCallback,
  useEffect,
} from 'react';
import {StyleSheet} from 'react-native';
import {
  ActivityIndicator,
  Image,
  Platform,
  ScrollView,
  Text,
  TouchableOpacity,
  KeyboardAvoidingView,
  View,
  RefreshControl,
  Button,
} from 'react-native';

import {auth, portfolio} from '../api/index';
import BottomNav from '../components/BottomNav';
import HeaderNav from '../components/HeaderNav';
import theme from '../utils/theme';
import Icon from 'react-native-vector-icons/FontAwesome';
import DocumentPicker, {types} from 'react-native-document-picker';
import Modal from 'react-native-modal';
import Snackbar from 'react-native-snackbar';
import RBSheet from 'react-native-raw-bottom-sheet';

const ViewAlpaca = ({navigation}) => {
  // ViewAlpaca Variables
  const [responseData, setResponseData] = useState(null);
  const [taxId, setTaxId] = useState(null);
  const [selectOne, setSelectOne] = useState(false);
  const [trade, setTrade] = useState(null);
  const [PlaidConnect, setPlaidConnect] = useState(null);
  const [noanyOne, setnoanyOne] = useState(null);
  const [fileUploadResponse, setFileUploadResponse] = useState([]);
  const [FileName, setFileName] = useState([]);
  const [folder, setFolder] = useState(null);
  const [closeDocument, setCloseDocument] = useState(false);
  const [uploadingtLoadingStatus, setuploadingtLoadingStatus] = useState(false);
  const [uploadingtLoadingStatuss, setuploadingtLoadingStatuss] = useState(
    false,
  );

  const sentGmail = useRef();

  useEffect(() => {
    getAlpacaAccountDetails();
  }, []);

  const getAlpacaAccountDetails = async () => {
    let response = await auth.viewAlpaca();

    if (response.keyword == 'success') {
      Snackbar.show({
        duration: Snackbar.LENGTH_SHORT,
        text: response.message,
      });

      setResponseData(response.data.alpaca_profile_data);
      setTaxId(response.data.taxid);
    } else {
      Snackbar.show({
        backgroundColor: 'red',
        duration: Snackbar.LENGTH_LONG,
        text: response.message,
      });
    }
  };

  const fileUploadData = [];
  fileUploadData.push(FileName);

  const removeName = (index) => {
    const files = [...fileUploadResponse];
    files.splice(index, 1);
    setFileUploadResponse(files);
  };

  const removeFileName = (index) => {
    const files = [...FileName];
    files.splice(index, 1);
    setFileName(files);
  };

  const Confirm = () => {
    if (trade === true) {
      sentGmail.current.open();
    } else if (PlaidConnect === true) {
      navigation.navigate('Profile', {
        screen: 'Financial',
        params: {
          screen: 'Bank Account',
        },
      });
      setSelectOne(false);
    } else if (noanyOne === true) {
      navigation.navigate('Profile');
      setSelectOne(false);
    }
  };

  const handleDocumentPicker = async (onSuccess) => {
    try {
      const fileData = await DocumentPicker.pick({
        type: [DocumentPicker.types.allFiles],
        allowMultiSelection: true,
      });
      onSuccess(fileData);

      setFileUploadResponse([...fileUploadResponse, fileData]);
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
      } else {
        Snackbar.show(err);
      }
    }
  };

  const handleDocumentSelection = () => {
    try {
      handleDocumentPicker(async (onSuccess) => {
        setuploadingtLoadingStatus(true);
        let requestData = new FormData();
        Platform.OS === 'android'
          ? requestData.append('file', {
              uri: onSuccess[0].uri,
              type: onSuccess[0].type,
              name: onSuccess[0].name,
            })
          : requestData.append('file', onSuccess);
        let response = await auth.portfolioFilesupload(requestData);
        setFolder(response.folder);
        FileName.push(response.data);
        setuploadingtLoadingStatus(false);
        Snackbar.show({
          backgroundColor: 'green',
          duration: Snackbar.LENGTH_SHORT,
          text: response.message,
        });
      });
    } catch (err) {
      Snackbar.show(err);
    }
  };

  const uploadDataFile = async () => {
    setuploadingtLoadingStatuss(true);
    let requestData = new FormData();
    requestData.append('file_names', JSON.stringify(FileName));
    requestData.append('folder', folder);

    if (FileName.length !== 0 && folder !== null) {
      let response = await auth.portfolioFilesInsert(requestData);
      setuploadingtLoadingStatuss(false);
      sentGmail.current.close();
      setSelectOne(false);
      setFileUploadResponse([]);

      setTrade(false);
      setFileName([]);
      setFolder(null);
      Snackbar.show({
        backgroundColor: 'green',
        duration: Snackbar.LENGTH_LONG,
        text: 'Email Sent Successfully...',
      });
    } else {
      setuploadingtLoadingStatuss(false);
      Snackbar.show({
        backgroundColor: 'red',
        duration: Snackbar.LENGTH_SHORT,
        text: 'Please Upload The File..!',
      });
    }
  };

  const CancelBtn = () => {
    setFileUploadResponse([]);
    setCloseDocument(false);
    setTrade(false);
    sentGmail.current.close();
  };

  const handleRemoveDocuments = async () => {
    //setFileUploadResponse([]);
    setCloseDocument(false);
    setFileName([]);
    setFolder(null);

    Snackbar.show({
      duration: Snackbar.LENGTH_SHORT,
      text: 'Removing The File...',
    });
  };

  const cancelpopup = () => {
    setTrade(false);
    setPlaidConnect(false);
    setnoanyOne(false);
    setSelectOne(!selectOne);
  };

  const selectName = fileUploadResponse.map((file, index) => {
    return Platform.OS === 'android' ? file[0]?.name : file?.name;
  });

  const renderAccountInformation = () => {
    const accountInfo = [
      {label: 'Account Number', value: responseData.account_number},
      {label: 'Status', value: responseData.status},
      {label: 'Currency', value: responseData.currency},
      {
        label: 'Created on',
        value: moment(responseData.created_at).format('LLL'),
      },
    ];

    return (
      <>
        {renderSectionHeader('Account Information')}

        <View style={{paddingBottom: 8}}>
          {accountInfo.map((accountData, index) => (
            <View
              key={index}
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                paddingVertical: 8,
              }}>
              {renderLabel(accountData.label)}

              {renderValue(accountData)}
            </View>
          ))}
        </View>
      </>
    );
  };

  const renderPersonalInformation = () => {
    const personalInfo = [
      {label: 'First Namer', value: responseData.identity.given_name},
      {label: 'Last Name', value: responseData.identity.family_name},
      {
        label: 'Date of Birth',
        value: moment(responseData.identity.date_of_birth).format('ll'),
      },
      {label: 'Email', value: responseData.contact.email_address},
      {label: 'Phone Number', value: responseData.contact.phone_number},
      {label: 'Street Address', value: responseData.contact.street_address},
      {label: 'City', value: responseData.contact.city},
      {label: 'State', value: responseData.contact.state},
      {label: 'Postal Code', value: responseData.contact.postal_code},
      {
        label: 'Tax Id Type',
        value: _.startCase(responseData.identity.tax_id_type),
      },
      {label: 'Tax Id', value: taxId},

      {
        label: 'Country of Birth',
        value: responseData.identity.country_of_birth || '-',
      },
      {
        label: 'Country of Citizenship',
        value: responseData.identity.country_of_citizenship || '-',
      },

      {
        label: 'Visa Type',
        value: responseData.identity.visa_type || '-',
      },
      {
        label: 'Visa Expiration Date',
        value: responseData.identity.visa_expiration_date || '-',
      },
      {
        label: ' Date of Departure From Usa',
        value: responseData.identity.date_of_departure_from_usa || '-',
      },
      {
        label: 'Country of Tax Residence',
        value: responseData.identity.country_of_tax_residence,
      },

      {
        label: 'Funding Source',
        value: _.startCase(responseData.identity.funding_source),
      },
      {
        label: 'Annual Income Min',
        value: `$ ${responseData.identity.annual_income_min || '-'}`,
      },
      {
        label: 'Annual Income Max',
        value: `$ ${responseData.identity.annual_income_max || '-'}`,
      },
      {
        label: 'Liquid Net Worth Min',
        value: `$ ${responseData.identity.liquid_net_worth_min || '-'}`,
      },
      {
        label: 'Liquid Net Worth Max',
        value: `$ ${responseData.identity.liquid_net_worth_max || '-'}`,
      },
      {
        label: 'Total Net Worth Min',
        value: `$ ${responseData.identity.total_net_worth_min || '-'}`,
      },
      {
        label: 'Total Net Worth Max',
        value: `$ ${responseData.identity.total_net_worth_max || '-'}`,
      },
      {
        label: 'Extra',
        value: responseData.identity.extra || '-',
      },
    ];

    return (
      <>
        {renderSectionHeader('Personal Information')}

        <View style={{paddingBottom: 8}}>
          {personalInfo.map((personalData, index) => (
            <View
              key={index}
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                paddingVertical: 8,
              }}>
              {renderLabel(personalData.label)}

              {renderValue(personalData)}
            </View>
          ))}
        </View>
      </>
    );
  };

  const renderDisclosureInformation = () => {
    const disclosureInfo = [
      {
        label:
          'Affiliated or work with a US registered broker-dealer or FINRA.',
        value:
          handleNoYes(
            responseData.disclosures.is_affiliated_exchange_or_finra,
          ) || null,
      },

      {
        label:
          'Senior executive at or a 10% or greater shareholder of a publicly traded company.',
        value: handleNoYes(responseData.disclosures.is_control_person) || null,
      },
      {
        label: 'I am a senior political figure.',
        value:
          handleNoYes(responseData.disclosures.is_politically_exposed) || null,
      },
      {
        label: 'I am a family member or relative of a senior political figure.',
        value:
          handleNoYes(responseData.disclosures.immediate_family_exposed) ||
          null,
      },
      // {
      //   label: 'None of the above apply to me or my family.',
      //   value: none ||
      //     null,
      // },
      {
        label: 'Employment Status',
        value: _.startCase(responseData.disclosures.employment_status),
      },
      {
        label: 'Employer Name',
        value: responseData.disclosures.employer_name,
      },
      {
        label: 'Employer Address',
        value: responseData.disclosures.employer_address,
      },
      {
        label: 'Employment Position',
        value: responseData.disclosures.employment_position,
      },
    ];

    return (
      <>
        {renderSectionHeader('Disclosure Information')}

        <View style={{paddingBottom: 8}}>
          {disclosureInfo.map((disclosureData, index) => (
            <View
              key={index}
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                paddingVertical: 8,
              }}>
              {renderLabel(disclosureData.label)}

              {renderValue(disclosureData)}
            </View>
          ))}
        </View>
      </>
    );
  };

  const handleNoYes = (value) => {
    switch (value) {
      case false:
        return 'No';

      case true:
        return 'Yes';

      default:
        return 'No';
    }
  };

  const renderAgreementInformation = () => {
    const agreementInfo = [
      {
        label: 'Account Signed At',
        value: moment(responseData.agreements[2].signed_at).format('LLL'),
      },
      {
        label: 'Account IP Address',
        value: responseData.agreements[2].ip_address,
      },
      {
        label: 'Customer Signed At',
        value: moment(responseData.agreements[1].signed_at).format('LLL'),
      },
      {
        label: 'Customer IP Address',
        value: responseData.agreements[1].ip_address,
      },
      {
        label: 'Margin Signed At',
        value: moment(responseData.agreements[0].signed_at).format('LLL'),
      },
      {
        label: 'Margin IP Address',
        value: responseData.agreements[0].ip_address,
      },
    ];

    return (
      <>
        {renderSectionHeader('Agreement Information')}

        <View style={{paddingBottom: 8}}>
          {agreementInfo.map((agreementData, index) => (
            <View
              key={index}
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                paddingVertical: 8,
              }}>
              {renderLabel(agreementData.label)}

              {renderValue(agreementData)}
            </View>
          ))}
        </View>
      </>
    );
  };

  const renderDocumentInformation = () => {
    let documentInfo = null;

    if (responseData?.documents) {
      documentInfo = [];

      responseData.documents.map((documentData) => {
        const helperObject = {
          imageLoadingStatus: false,
          label: `${_.startCase(documentData.document_type)}${
            documentData.document_sub_type &&
            documentData.document_sub_type != 'null'
              ? ` (${_.startCase(documentData.document_sub_type)})`
              : ''
          }`,
          value:
            'https://play-lh.googleusercontent.com/nufRXPpDI9XP8mPdAvOoJULuBIH_OK4YbZZVu8i_-eDPulZpgb-Xp-EmI8Z53AlXHpqX',
        };

        documentInfo.push(helperObject);
      });
    } else {
      documentInfo = [];
    }

    return (
      <>
        {renderSectionHeader('Document Information')}

        <View style={{paddingBottom: 8}}>
          {Boolean(documentInfo) ? (
            documentInfo.length != 0 ? (
              documentInfo.map((documentData, index) => (
                <View
                  key={index}
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    paddingVertical: 8,
                  }}>
                  {renderLabel(documentData.label)}

                  <View style={{flex: 0.575, justifyContent: 'center'}}>
                    <Image
                      source={{uri: documentData.value}}
                      style={{
                        width: 40,
                        height: 40,
                        borderRadius: 4,
                        borderColor: 'white',
                        borderWidth: 1,
                      }}
                    />
                  </View>
                </View>
              ))
            ) : (
              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  paddingTop: 16,
                }}>
                <Text
                  style={{
                    fontSize: 12,
                    color: theme.white,
                    fontWeight: '400',
                    paddingHorizontal: 8,
                  }}>
                  No document(s) found
                </Text>
              </View>
            )
          ) : (
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
                paddingTop: 16,
              }}>
              <ActivityIndicator size="small" color={theme.white} />

              <Text
                style={{
                  fontSize: 12,
                  color: theme.white,
                  fontWeight: '400',
                  paddingHorizontal: 8,
                }}>
                Loading document(s)
              </Text>
            </View>
          )}
        </View>
      </>
    );
  };

  const renderTrustedContactInformation = () => {
    const tructedContactInfo = [
      {
        label: 'First Name',
        value: responseData.trusted_contact.given_name,
      },
      {
        label: 'Last Name',
        value: responseData.trusted_contact.family_name,
      },
      {
        label: 'Email',
        value: responseData.trusted_contact.email_address,
      },
      {
        label: 'Phone Number',
        value: responseData.trusted_contact.phone_number,
      },
      {
        label: 'Street Address',
        value: responseData.trusted_contact.street_address,
      },
      {
        label: 'City',
        value: responseData.trusted_contact.city,
      },
      {
        label: 'State',
        value: responseData.trusted_contact.state,
      },
      {
        label: 'Postal Code',
        value: responseData.trusted_contact.postal_code,
      },
    ];

    return (
      <>
        {renderSectionHeader('Trusted Contact Information')}

        <View style={{paddingBottom: 8}}>
          {tructedContactInfo.map((trustedContactData, index) => (
            <View
              key={index}
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                paddingVertical: 8,
              }}>
              {renderLabel(trustedContactData.label)}

              {renderValue(trustedContactData)}
            </View>
          ))}
        </View>
      </>
    );
  };

  const renderSectionHeader = (sectionName) => {
    return (
      <View
        style={{
          backgroundColor: theme.themeColor,
          borderBottomColor: theme.white,
          borderBottomWidth: 1,
          paddingVertical: 8,
        }}>
        <Text
          style={{
            fontSize: Platform.OS == 'ios' ? 14 : 16,
            color: theme.secondryColor,
            fontWeight: 'bold',
          }}>
          {sectionName}
        </Text>
      </View>
    );
  };

  const renderLabel = (labelName) => {
    return (
      <View
        style={{
          flex: 0.4,
          flexDirection: 'row',
          justifyContent: 'space-between',
        }}>
        <Text
          style={{
            fontSize: Platform.OS == 'ios' ? 12 : 14,
            color: theme.white,
            fontWeight: '600',
            lineHeight: 20,
          }}>
          {labelName}
        </Text>

        <Text
          style={{
            fontSize: Platform.OS == 'ios' ? 16 : 18,
            color: theme.white,
            fontWeight: 'bold',
          }}>
          :
        </Text>
      </View>
    );
  };

  const renderStatus = (status) => {
    return (
      <View
        style={{
          width: '62.5%',
          backgroundColor: handleStatusColor(status),
          borderRadius: 4,
          paddingHorizontal: 12,
          paddingVertical: 6,
        }}>
        <Text
          style={{
            fontSize: Platform.OS == 'ios' ? 10 : 12,
            color: theme.white,
            fontWeight: 'bold',
            textAlign: 'center',
          }}>
          {status || '-'}
        </Text>
      </View>
    );
  };

  const handleStatusColor = (status) => {
    switch (status) {
      case 'ACCOUNT_CLOSED':
        return '#FA8072';

      case 'ACTION_REQUIRED':
        return '#800000';

      case 'ACTIVE':
        return '#008000';

      case 'APPROVED':
        return '#808000';

      case 'APPROVAL_PENDING':
        return '#FFA500';

      case 'DISABLED':
        return '#CCCCFF';

      case 'REJECTED':
        return '#FF0000';

      case 'SUBMITTED':
        return '#8000FF';

      default:
        return '#6495ED';
    }
  };

  const renderValue = (sectionData) => {
    return (
      <View style={{flex: 0.575, justifyContent: 'center'}}>
        {sectionData.label === 'Status' ? (
          renderStatus(sectionData.value)
        ) : (
          <Text
            style={{
              fontSize: Platform.OS == 'ios' ? 12 : 14,
              color: theme.white,
              fontWeight: '400',
              lineHeight: 20,
              top: 1,
            }}>
            {sectionData.value || '-'}
          </Text>
        )}
      </View>
    );
  };

  return (
    <>
      <HeaderNav title="Alpaca Account View" isBack={true} />

      {responseData ? (
        <ScrollView
          keyboardShouldPersistTaps={'handled'}
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
          style={{flex: 1, backgroundColor: theme.themeColor}}>
          <View style={{paddingHorizontal: 8, paddingVertical: 8}}>
            {renderAccountInformation()}

            {renderPersonalInformation()}

            {renderDisclosureInformation()}

            {renderAgreementInformation()}

            {renderDocumentInformation()}

            {renderTrustedContactInformation()}

            <View style={{paddingVertical: 12}} />
            <TouchableOpacity
              style={{bottom: 36}}
              onPress={() => setSelectOne(true)}>
              <View style={styles.btnview}>
                <Text style={styles.btntext}>Next</Text>
              </View>
            </TouchableOpacity>
          </View>
        </ScrollView>
      ) : (
        <View
          style={{
            flex: 1,
            backgroundColor: theme.themeColor,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <ActivityIndicator size="large" color={theme.white} />
        </View>
      )}

      <Modal isVisible={selectOne}>
        <View style={{backgroundColor: theme.primaryColor, borderRadius: 8}}>
          <View
            style={{
              backgroundColor: theme.secondryColor,
              borderBottomColor: 'grey',
              borderBottomWidth: 1,
              borderTopLeftRadius: 8,
              borderTopRightRadius: 8,
              paddingVertical: 8,
            }}>
            <Text
              style={{
                fontSize: Platform.OS == 'ios' ? 14 : 16,
                color: 'white',
                fontWeight: 'bold',
                textAlign: 'center',
              }}></Text>
          </View>

          <View style={{marginHorizontal: 12, marginVertical: 12}}>
            <TouchableOpacity
              onPress={() => {
                setTrade(true);
                setPlaidConnect(false);
                setnoanyOne(false);
              }}
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginVertical: 8,
              }}>
              <View
                style={{
                  flex: 0.0875,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    width: 20,
                    height: 20,
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderColor: theme.secondryColor,
                    borderWidth: 1,
                    borderRadius: 20 / 2,
                  }}>
                  {trade && (
                    <View
                      style={{
                        width: 10,
                        height: 10,
                        backgroundColor: theme.secondryColor,
                        borderRadius: 10 / 2,
                      }}
                    />
                  )}
                </View>
              </View>

              <View
                style={{
                  flex: 0.8875,
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    fontSize: Platform.OS == 'ios' ? 12 : 14,
                    color: 'white',
                    fontWeight: '600',
                  }}>
                  Trade blotter email
                </Text>
              </View>
            </TouchableOpacity>

            <RBSheet
              ref={sentGmail}
              closeOnDragDown={false}
              closeOnPressMask={false}
              closeOnPressBack={true}
              height={175}
              openDuration={250}
              customStyles={{
                container: {
                  backgroundColor: theme.themeColor,
                  borderTopLeftRadius: 16,
                  borderTopRightRadius: 16,
                },
                wrapper: {backgroundColor: 'grey', opacity: 0.9},
                draggableIcon: {
                  display: 'none',
                },
              }}>
              <KeyboardAvoidingView
                behavior={Platform.OS === 'ios' ? 'padding' : null}
                keyboardVerticalOffset={Platform.OS === 'ios' ? 40 : 0}
                style={{paddingHorizontal: 16, paddingVertical: 16}}>
                <ScrollView>
                  <View>
                    <View style={{flexDirection: 'row'}}>
                      <View style={{flex: 1, justifyContent: 'center'}}>
                        <Text style={styles.Title}>
                          Trade Blotter Email Upload :
                        </Text>
                      </View>

                      <TouchableOpacity
                        onPress={() => {
                          handleDocumentSelection();
                        }}
                        style={styles.uploadbtn}>
                        {uploadingtLoadingStatus ? (
                          <ActivityIndicator size="small" color={theme.white} />
                        ) : (
                          <Icon name="upload" color={'white'} size={20} />
                        )}
                      </TouchableOpacity>
                    </View>

                    <View
                      style={{
                        flexDirection: 'row',
                        marginTop: 7,
                        alignItems: 'center',
                      }}>
                      <View style={{paddingVertical: 0.7, marginLeft: 8}}>
                        {selectName.map((file, index) =>
                          Platform.OS === 'android' ? (
                            <Text
                              key={index.toString()}
                              style={{color: 'white', fontSize: 13}}
                              numberOfLines={1}
                              ellipsizeMode={'tail'}>
                              {file.slice(0, 40)}
                            </Text>
                          ) : (
                            <Text
                              key={index.toString()}
                              style={{color: 'white', fontSize: 14.2}}
                              numberOfLines={1}
                              ellipsizeMode={'tail'}>
                              {file.slice(0, 40)}
                            </Text>
                          ),
                        )}
                      </View>

                      <View>
                        {fileUploadResponse.map((file, index) => (
                          <TouchableOpacity
                            key={index}
                            style={{
                              //borderTopRightRadius: 4,
                              // paddingHorizontal: 1,
                              paddingVertical: 0.7,
                              marginLeft: 8,
                              marginTop: 6,
                            }}
                            onPress={() => {
                              removeName(index), removeFileName(index);
                            }}>
                            <Icon name={'times'} size={10} color={'red'} />
                          </TouchableOpacity>
                        ))}
                      </View>
                    </View>

                    {uploadingtLoadingStatuss && (
                      <ActivityIndicator size="small" color={theme.white} />
                    )}

                    <View
                      style={{
                        flexDirection: 'row',
                        width: '100%',
                        justifyContent: 'space-between',
                      }}>
                      <TouchableOpacity
                        style={styles.sentbtn}
                        onPress={uploadDataFile}>
                        {/* {uploadingtLoadingStatuss ? (
                          <ActivityIndicator size="small" color={theme.white} />
                        ) : ( */}
                        <Text style={styles.senttxt}>Sent Email</Text>
                        {/* )} */}
                      </TouchableOpacity>
                      <TouchableOpacity
                        onPress={CancelBtn}
                        style={styles.cancelbtn}>
                        <Text style={styles.senttxt}>Cancel</Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </ScrollView>
              </KeyboardAvoidingView>
            </RBSheet>

            <TouchableOpacity
              onPress={() => {
                setPlaidConnect(true);
                setTrade(false);
                setnoanyOne(false);
              }}
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginVertical: 8,
              }}>
              <View
                style={{
                  flex: 0.0875,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    width: 20,
                    height: 20,
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderColor: theme.secondryColor,
                    borderWidth: 1,
                    borderRadius: 20 / 2,
                  }}>
                  {PlaidConnect && (
                    <View
                      style={{
                        width: 10,
                        height: 10,
                        backgroundColor: theme.secondryColor,
                        borderRadius: 10 / 2,
                      }}
                    />
                  )}
                </View>
              </View>

              <View
                style={{
                  flex: 0.8875,
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    fontSize: Platform.OS == 'ios' ? 12 : 14,
                    color: 'white',
                    fontWeight: '600',
                  }}>
                  Connect with plaid
                </Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                setnoanyOne(true);
                setTrade(false);
                setPlaidConnect(false);
              }}
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginVertical: 8,
              }}>
              <View
                style={{
                  flex: 0.0875,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    width: 20,
                    height: 20,
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderColor: theme.secondryColor,
                    borderWidth: 1,
                    borderRadius: 20 / 2,
                  }}>
                  {noanyOne && (
                    <View
                      style={{
                        width: 10,
                        height: 10,
                        backgroundColor: theme.secondryColor,
                        borderRadius: 10 / 2,
                      }}
                    />
                  )}
                </View>
              </View>

              <View
                style={{
                  flex: 0.8875,
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    fontSize: Platform.OS == 'ios' ? 12 : 14,
                    color: 'white',
                    fontWeight: '600',
                  }}>
                  I will do later
                </Text>
              </View>
            </TouchableOpacity>

            {/* {selectedEchoingOptionError && (
                <Text
                  style={{
                    fontSize: Platform.OS == 'ios' ? 12 : 14,
                    color: 'red',
                    fontWeight: '600',
                    marginHorizontal: 4,
                    marginVertical: 8,
                  }}>
                  Please! Select any one of the echoing(s)!
                </Text>
              )} */}

            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
                marginVertical: 4,
              }}>
              <TouchableOpacity onPress={Confirm} style={styles.btnview1}>
                <Text style={styles.btntext}>Confirm</Text>
              </TouchableOpacity>

              <TouchableOpacity onPress={cancelpopup}>
                <Text
                  style={{
                    fontSize: Platform.OS == 'ios' ? 14 : 16,
                    color: theme.secondryColor,
                    fontWeight: 'bold',
                    textAlign: 'center',
                    marginHorizontal: 16,
                  }}>
                  Cancel
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>

      <BottomNav routeName="Profile" />
    </>
  );
};

export default ViewAlpaca;
const styles = StyleSheet.create({
  title: {
    fontSize: Platform.OS == 'ios' ? 16 : 18,
    color: theme.white,
    fontWeight: 'bold',
    marginBottom: 8,
  },

  uploadbtn: {
    width: '30%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: theme.secondryColor,
    borderRadius: 24,
    paddingHorizontal: 12,
    paddingVertical: 8,
    marginLeft: 9,
  },

  sentbtn: {
    width: '50',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: theme.secondryColor,
    borderRadius: 24,
    paddingHorizontal: 12,
    paddingVertical: 8,
    alignSelf: 'center',
    marginTop: 15,
  },

  cancelbtn: {
    width: '43%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: theme.secondryColor,
    borderRadius: 24,
    paddingHorizontal: 12,
    paddingVertical: 8,
    alignSelf: 'center',
    marginTop: 15,
  },

  uploadtxt: {
    fontSize: Platform.OS == 'ios' ? 12 : 14,
    color: theme.white,
    fontWeight: '400',
    textAlign: 'center',
    marginHorizontal: 4,
  },

  senttxt: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    color: theme.white,
    fontWeight: '700',
    textAlign: 'center',
    marginHorizontal: 4,
  },
  btnview: {
    width: '75%',
    alignSelf: 'center',
    backgroundColor: theme.secondryColor,
    borderRadius: 8,
    paddingHorizontal: 16,
    paddingVertical: 8,
  },
  btntext: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  btnview1: {
    backgroundColor: theme.secondryColor,
    borderRadius: 4,
    paddingHorizontal: 12,
    paddingVertical: 6,
  },
  Title: {
    fontSize: Platform.OS == 'ios' ? 16 : 18,
    color: theme.white,
    fontWeight: 'bold',
    marginBottom: 8,
  },
});
