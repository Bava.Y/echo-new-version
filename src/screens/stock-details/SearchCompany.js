import React from 'react';
import {Button, StyleSheet, TextInput, View} from 'react-native';
import HeaderNav from '../../components/HeaderNav';
import theme from '../../utils/theme';

const SearchCompany = ({navigation}) => {
  const [Symbol, setSymbol] = React.useState('');
  return (
    <>
      <HeaderNav title="Company" isBack="true" />
      <View style={styles.container}>
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
          <TextInput
            style={{
              height: 40,
              width: 250,
              margin: 20,
              borderColor: 'gray',
              borderWidth: 1,
              borderRadius: 50,
              color: 'white',
              paddingHorizontal: 20,
            }}
            placeholder="Search Stocks"
            placeholderTextColor="white"
            onChangeText={(text) => setSymbol(text)}
            value={Symbol}
            autoCapitalize="characters"
          />
          <View style={{width: '50%', marginTop: 10}}>
            <Button
              title="Search"
              onPress={() => {
                navigation.navigate('company', {symbol: Symbol.toUpperCase()});
              }}
            />
          </View>
        </View>
      </View>
    </>
  );
};

export default SearchCompany;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    padding: 15,
    backgroundColor: theme.primaryColor,
  },
});
