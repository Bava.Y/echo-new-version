import React, {Component} from 'react';
import {Platform} from 'react-native';
import WebView from 'react-native-webview';

export default class MyWeb extends Component {
  render() {
    const isAndroid = Platform.OS === 'android';

    return (
      <WebView
        originWhitelist={['*']}
        allowFileAccess={true}
        domStorageEnabled={true}
        allowUniversalAccessFromFileURLs={true}
        allowFileAccessFromFileURLs={true}
        mixedContentMode="always"
        source={{
          uri: isAndroid
            ? `file:///android_asset/charting_library/index.html?symbol=${this.props.route.params.symbol}`
            : `./charting_library/mobile_white.html?symbol=${this.props.route.params.symbol}`,
        }}
      />
    );
  }
}
