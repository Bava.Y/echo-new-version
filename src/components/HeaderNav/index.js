import AsyncStorage from '@react-native-community/async-storage';
import {useNavigation} from '@react-navigation/native';
import React, {useContext, useEffect, useRef, useState} from 'react';
import {Platform} from 'react-native';
import { Button } from 'react-native';
import {BackHandler, Image, Text, TouchableOpacity, View} from 'react-native';
import DeviceInfo from 'react-native-device-info';
import Modal from 'react-native-modal';
import Icon from 'react-native-vector-icons/FontAwesome';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {auth, profile} from '../../api';
import {BEAR, BULL, NEUTRAL, STAR} from '../../assets/images';
import {UserDataContext} from '../../context/UserDataContext';
import CompanyDescribe from '../../screens/company/CompanyDescribe';
import CompanyRate from '../../screens/company/CompanyRate';
import CompanyList from '../../screens/discover-people/CompanyList';
import Filter from '../../screens/discover-people/filter';
import theme from '../../utils/theme';
import styles from './style';
import Subscription from './subscription';
import SelectDropdown from 'react-native-select-dropdown';
import FontAwesomeIcons from 'react-native-vector-icons/FontAwesome5';
import {getPortfolioData} from '../../screens/portfolio/portfolio'

const listof=["Alpaca","TD Ameritrade"]
const HeaderNav = (props) => {
  let {title, isBack, modalDisable, routeName,drop,ha} = props;
  const FilterlistPopup = useRef();
  const filterRef = useRef();
  let deviceId = DeviceInfo.getUniqueId();

  const {
    token,
    rating,
    setPrimary,
    setSecondary,
    setSecondaryInd,
    setSecondarySect,
    setPortfolioConnection,
  } = useContext(UserDataContext);

  const navigation = useNavigation();

  useEffect(() => {
    checkSession();
  }, []);

  const checkSession = async () => {
    let formData = new FormData();

    formData && formData.append('unique_id', deviceId);

    let response = await auth.getMpinStatus(formData);

    setAlreadyExists(Boolean(response.data.enabled) ? false : true);
  };
  const[broker,setBroker]=useState(null)
  const [isDescriptionVisible, setDescriptionVisible] = useState(false);
  const [companyRateVisible, setCompanyRateVisible] = useState(false);

  //investment philosophies
  useEffect(() => {
    let ac = new AbortController();

    routeName == 'Discover People' && getInvestments();

    return () => ac.abort();
  }, []);

  const getInvestments = async () => {
    let primeInvest = await profile.getPrimaryInv(token);
    setPrimary(primeInvest);
    let secondaryInvest = await profile.getSecondaryInv(token);
    setSecondary(secondaryInvest);
    let secondaryInd = await profile.getSecondaryInvSubCatergory(1, token);
    setSecondaryInd(secondaryInd);
    let secondarySect = await profile.getSecondaryInvSubCatergory(2, token);
    setSecondarySect(secondarySect);
  };

  const [exitApp, setExitApp] = useState(0);

  const backAction = () => {
    setTimeout(() => {
      setExitApp(0);
    }, 2000);

    if (exitApp === 0) {
      setExitApp(exitApp + 1);
    } else if (exitApp === 1) {
      BackHandler.exitApp();
    }

    return true;
  };

  useEffect(() => {
    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => backHandler.remove();
  });

  const SubscribePopup = useRef();

  const makeModalFalse = () => {
    setDescriptionVisible(false);
  };

  const [isAlreadyExists, setAlreadyExists] = useState(false);

  return (
    <>
      <View style={styles.mainContainer}>
        {isBack ? (
          <TouchableOpacity
            onPress={() => {
              navigation.goBack();
            }}>
            <Ionicons name="arrow-back" color={theme.white} size={24}/>
          </TouchableOpacity>
        ) : (
          <TouchableOpacity onPress={() => SubscribePopup.current.open()}> 
            <Image source={STAR} style={{width: 24, height: 24}} />
          </TouchableOpacity>
        )}


        {routeName == 'Company' ? (
          <Text
            onPress={() => {
              setDescriptionVisible(true);
            }}
            style={styles.title}
            numberOfLines={1}
            ellipsizeMode="tail">
            {title}
          </Text>
        ) : (
          <Text style={styles.title}>{title}</Text>
        )}

{drop ? (
          <SelectDropdown
          buttonStyle={styles.listBroker}
          buttonTextStyle={styles.listBrokerText}
           data={listof}
           dropdownStyle={{alignSelf: 'center',}}
          
          defaultValue='Alpaca'
          
          onSelect={(selectedItem) => {
           setBroker(selectedItem)
          switch (selectedItem) {
            case 'Alpaca':
              setPortfolioConnection('0');
             
              break;
              case 'TD Ameritrade':
                setPortfolioConnection('1');
              break;
          }
          }}
          renderDropdownIcon={() => (
           <FontAwesomeIcons name="caret-down" size={16} color={'white'}/>
          )}
          rowStyle={{borderBottomWidth: 0}}
          rowTextStyle={styles.brokerItemText}
          selectedRowTextStyle={styles.listBrokerSelectedItemText}
         />
         ) : (
          <TouchableOpacity>
          <Text style={styles.title}>{ha}</Text>
          </TouchableOpacity>
        )}

       
        {routeName == 'Company' ? (
          <TouchableOpacity
            onPress={() => {
              setCompanyRateVisible(true);
            }}>
            {rating == null ? (
              <Ionicons
                onPress={() => {
                  setCompanyRateVisible(true);
                }}
                name="filter"
                color={theme.white}
                size={24}
              />
            ) : (
              <Image
                source={
                  rating == 'bull'
                    ? BULL
                    : rating == 'bear'
                    ? BEAR
                    : rating == 'neutral'
                    ? NEUTRAL
                    : null
                }
                style={{
                  width: 28,
                  height: 28,
                  borderRadius: 28 / 2,
                }}
              />
            )}
          </TouchableOpacity>
        ) : routeName == 'Discover People' ||
          routeName == 'Echoes | Following | Followers' ||
          routeName == 'Leaderboard' ||
          routeName == 'Individual Returns' ? (
          <TouchableOpacity onPress={() => FilterlistPopup.current.open()}>
            <Ionicons name="filter" color={theme.white} size={24} />
          </TouchableOpacity>
        ) : routeName == 'Portfolio' || routeName == 'Order History' ? (
          <TouchableOpacity
            onPress={() => {
            //  filterRef.current.open();
            }}>
            <MaterialIcons name="more-vert" color={theme.white} size={24}/>
          </TouchableOpacity>
        ) : (
          <View style={{justifyContent: 'center', alignItems: 'center'}}>
            <Icon
              name="bell-o"
              color={theme.white}
              size={24}
              onPress={() => {
                navigation.navigate('Notification');
              }}
            />
          </View>
        )}
      </View>

      {/* Subscription */}
      <Subscription
        subscription={SubscribePopup}
        onChange={() => SubscribePopup.current.close()}
      />

      {/* Discover People filter */}
      {(routeName == 'Discover People' ||
        routeName == 'Echoes | Following | Followers' ||
        routeName == 'Leaderboard' ||
        routeName == 'Individual Returns') && (
        <Filter
          filterlist={FilterlistPopup}
          onChange={() => FilterlistPopup.current.close()}
          onFilterApply={props.onFilterApply}
          routeName={routeName}
        />
      )}

      {/* Company List */}
      <CompanyList
        forStatus={routeName}
        filterRef={filterRef}
        onClose={() => filterRef.current.close()}
      />

      {/* Expiry checking */}
      <Modal
        animationType="fade"
        transparent={true}
        isVisible={isAlreadyExists}>
        <View style={styles.welcomecontainer}>
          <Text style={styles.welcometxt}>Oops!</Text>
          <Text style={styles.steptext}>
            You are logged in with a new device!!!
          </Text>
          <View
            style={styles.startbtn}
            onStartShouldSetResponder={async () => {
              const keys = await AsyncStorage.getAllKeys();
              if (keys.length > 0) {
                await AsyncStorage.clear();
                navigation.navigate('Login');
              }
              setAlreadyExists(false);
            }}>
            <Text style={styles.starttxt}>Ok</Text>
          </View>
        </View>
      </Modal>

      {/* Company Rate */}
      <Modal
        style={{
          flex: 1,
          justifyContent: 'flex-start',
          alignItems: 'flex-end',
          marginTop: Platform.OS == 'ios' ? 94 : 48,
          marginRight: 0,
        }}
        isVisible={companyRateVisible}
        backdropColor="transparent"
        swipeDirection="up"
        onBackdropPress={() => setCompanyRateVisible(false)}
        onSwipeComplete={() => setCompanyRateVisible(false)}>
        <CompanyRate hide={setCompanyRateVisible} />
      </Modal>

      {/* Company description */}
      <Modal
        backdropColor="black"
        hasBackdrop={true}
        isVisible={modalDisable ? false : isDescriptionVisible}
        onBackdropPress={() => setDescriptionVisible(false)}
        style={{maxHeight: '100%', margin: 0}}>
        <View style={{height: '90%'}}>
          <View style={styles.mainContainer}>
            {isBack ? (
              <TouchableOpacity
                onPress={() => {
                  isDescriptionVisible
                    ? setDescriptionVisible(false)
                    : isBack && navigation.goBack();
                }}>
                <Ionicons name="arrow-back" color={theme.white} size={24} />
              </TouchableOpacity>
            ) : (
              <TouchableOpacity onPress={() => SubscribePopup.current.open()}>
                <Image source={STAR} style={{width: 24, height: 24}} />
              </TouchableOpacity>
            )}

            {routeName == 'Company' ? (
              <Text
                onPress={() => {
                  setDescriptionVisible(true);
                }}
                style={styles.title}
                numberOfLines={1}
                ellipsizeMode="tail">
                {title}
              </Text>
            ) : (
              <Text style={styles.title}>{title}</Text>
            )}

            {routeName == 'Company' && Boolean(rating) && (
              <Image
                source={
                  rating == 'bull'
                    ? BULL
                    : rating == 'bear'
                    ? BEAR
                    : rating == 'neutral'
                    ? NEUTRAL
                    : null
                }
                style={{
                  width: 28,
                  height: 28,
                  borderRadius: 28 / 2,
                }}
              />
            )}
          </View>

          <CompanyDescribe makeModalFalse={makeModalFalse} />
        </View>
      </Modal>
    </>
  );
};

export default HeaderNav;
/*{routeName == 'Company' ? (
          <SelectDropdown
          buttonStyle={styles.dropdownWrapper}
          data={selectData?.ethnicity ?? []}
          defaultButtonText="Ethnicity"
          dropdownStyle={{width: '80%', marginLeft: 8, alignSelf: 'center'}}
          onSelect={(selectedItem) => {
            setEthinicity(selectedItem);
          }}
          renderCustomizedButtonChild={(selectedItem) => {
            return (
              <View style={styles.dropdownItemContainer}>
                <View style={styles.dropdownLabelContainer}>
                  <Text style={styles.dropdownLabelText}>
                    {selectedItem?.label ?? 'Ethnicity'}
                  </Text>
                </View>
                <View style={styles.dropdownIconContainer}>
                  <FontAwesomeIcons
                    name="caret-down"
                    size={16}
                    color={'white'}
                  />
                </View>
              </View>
            );
          }}
          rowStyle={{borderBottomWidth: 0}}
          rowTextForSelection={(item) => item?.label}
          rowTextStyle={styles.dropdownItemText}
          selectedRowTextStyle={styles.dropdownSelectedItemText}
          defaultValue={ethnicity}
        />
        ) : (
          <TouchableOpacity>
          <Text style={styles.title}>{ha}</Text>
          </TouchableOpacity>
        )}*/

         /*
         <SelectDropdown
         buttonStyle={styles.listBroker}
         buttonTextStyle={styles.listBrokerText}
          data={listof}
         defaultButtonText="CLICK"
         onSelect={(selectedItem) => {
          setBroker(selectedItem)
         }}
         renderDropdownIcon={() => (
          <FontAwesomeIcons name="caret-down" size={16} color={'white'}/>
         )}
         rowStyle={{borderBottomWidth: 0}}
         rowTextStyle={styles.brokerItemText}
         selectedRowTextStyle={styles.dropdownSelectedItemText}
        />
         */