import React, {Component} from 'react';
import {Animated, StyleSheet, Text, TouchableOpacity, View} from 'react-native';

export default class Flipcard extends Component {
  componentWillMount() {
    this.animatedvalue = new Animated.Value(0);
    this.value = 0;
    this.frontInterpolate = this.animatedvalue.interpolate({
      inputRange: [0, 180],
      outputRange: ['0deg', '180deg'],
    });
    this.backInterpolate = this.animatedvalue.interpolate({
      inputRange: [0, 180],
      outputRange: ['180deg', '360deg'],
    });
  }

  flipcard() {
    if (this.value >= 90) {
      Animated.spring(this.animatedvalue, {
        toValue: 0,
        friction: 8,
        tension: 10,
      }).start();
    } else {
      Animated.spring(this.animatedvalue, {
        toValue: 180,
        friction: 8,
        tension: 10,
      }).start();
    }
  }

  render() {
    const frontAnimatedStyle = {
      transform: [{rotateY: this.frontInterpolate}],
    };
    const backAnimatedStyle = {
      transform: [{rotateY: this.backInterpolate}],
    };

    return (
      <>
        <View>
          <TouchableOpacity onPress={() => this.flipcard()}>
            <Animated.View style={[styles.flipcard, frontAnimatedStyle]}>
              <Text>Flip card front </Text>
            </Animated.View>
          </TouchableOpacity>
          <Animated.View
            style={[styles.flipcard, backAnimatedStyle, styles.flipcardback]}>
            <Text>Flip card Back </Text>
          </Animated.View>
        </View>
        <TouchableOpacity onPress={() => this.flipcard()}>
          <Text>Flip</Text>
        </TouchableOpacity>
      </>
    );
  }
}

const styles = StyleSheet.create({
  flipcard: {
    backgroundColor: 'red',
    width: 200,
    height: 200,
    backfaceVisibility: 'hidden',
  },
  flipcardback: {
    backgroundColor: 'green',
    position: 'absolute',
    top: 0,
  },
});
