import {useFocusEffect} from '@react-navigation/native';
import moment from 'moment';
import React, {useCallback, useContext, useState} from 'react';
import {
  ActivityIndicator,
  Platform,
  ScrollView,
  Text,
  View,
} from 'react-native';
import Snackbar from 'react-native-snackbar';
import {dashboard} from '../api';
import HeaderNav from '../components/HeaderNav';
import {UserDataContext} from '../context/UserDataContext';
import Theme from '../utils/theme';

const Notification = () => {
  // Notification Variables
  const [notificationData, setNotificationData] = useState(null);

  // Context Variables
  const {token} = useContext(UserDataContext);

  useFocusEffect(
    useCallback(() => {
      let isFocus = true;

      fetchNotifications();

      return () => {
        isFocus = false;
      };
    }, []),
  );

  const fetchNotifications = async () => {
    const res = await dashboard.getNotifications(token);

    if (res) {
      if (res.keyword === 'success') {
        setNotificationData(res.data ? res.data : []);
      } else {
        Snackbar.show({duration: Snackbar.LENGTH_SHORT, text: res.message});

        setNotificationData([]);
      }
    } else {
      setNotificationData([]);
    }
  };

  const handleTitleColor = (page) => {
    switch (page) {
      default:
        return Theme.secondryColor;
    }
  };

  return (
    <>
      <HeaderNav title="Notification" isBack="true" />

      {notificationData ? (
        notificationData.length != 0 ? (
          <ScrollView
            keyboardShouldPersistTaps={'handled'}
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
            style={{flex: 1, backgroundColor: Theme.themeColor}}>
            <View style={{paddingHorizontal: 16, paddingVertical: 16}}>
              {notificationData.map((lol, index) => (
                <View
                  key={index}
                  style={{
                    backgroundColor: Theme.primaryColor,
                    borderRadius: 8,
                    marginTop: index != 0 ? 16 : 0,
                    paddingHorizontal: 16,
                    paddingVertical: 12,
                  }}>
                  <Text
                    style={{
                      fontSize: Platform.OS == 'ios' ? 14 : 16,
                      fontWeight: 'bold',
                      color: handleTitleColor(lol.page),
                    }}>
                    {lol.title}
                  </Text>
                  <Text
                    style={{
                      fontSize: Platform.OS == 'ios' ? 10 : 12,
                      fontWeight: '600',
                      color: Theme.white,
                      marginVertical: 8,
                      lineHeight: 18,
                    }}>
                    {lol.body}
                  </Text>
                  <Text
                    style={{
                      fontSize: Platform.OS == 'ios' ? 10 : 12,
                      fontWeight: '600',
                      color: Theme.textColor,
                      textAlign: 'right',
                    }}>
                    {moment(lol.created_on, 'YYYY-MM-DD HH:mm:ss').fromNow()}
                  </Text>
                </View>
              ))}
            </View>
          </ScrollView>
        ) : (
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: Theme.themeColor,
            }}>
            <View style={{paddingHorizontal: 16, paddingVertical: 16}}>
              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 14 : 16,
                  fontWeight: '600',
                  color: Theme.white,
                }}>
                No notification(s) found
              </Text>
            </View>
          </View>
        )
      ) : (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: Theme.themeColor,
          }}>
          <ActivityIndicator size={'large'} color={Theme.white} />
        </View>
      )}
    </>
  );
};

export default Notification;
