import {useFocusEffect} from '@react-navigation/core';
import {isEmpty} from 'lodash';
import React, {
  useCallback,
  useContext,
  useEffect,
  useRef,
  useState,
} from 'react';
import {
  ActivityIndicator,
  Dimensions,
  Image,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import Draggable from 'react-native-draggable';
import Snackbar from 'react-native-snackbar';
import Icon from 'react-native-vector-icons/FontAwesome';
import MIcon from 'react-native-vector-icons/MaterialIcons';
import {
  VictoryAxis,
  VictoryChart,
  VictoryLegend,
  VictoryLine,
} from 'victory-native';
import {company, dashboard} from '../../api';
import {DIAMOND, ALPACA, AMERITRADE, TLOGO} from '../../assets/images/index';
import LITERALS from '../../assets/strings';
import BottomNav from '../../components/BottomNav';
import HeaderNav from '../../components/HeaderNav';
import {getData} from '../../containers/getMarketData';
import {UserDataContext} from '../../context/UserDataContext';
import theme from '../../utils/theme';
import Chart from './chart';
import OrderShareAmount from './order-amount';
import OrderShareQuantity from './order-quantity';
import OverallFunctionality from './overall-functionality';
import Search from './Search';
import SmallChart from './smallChart';
import styles from './styles';
import Ticker from './ticker';
import Modal from 'react-native-modal';
import Loading from '../../components/Loading';

const Dashboard = ({navigation}) => {
  const {
    token,
    setTickerData,
    dashboardPreference,
    singleChartSymbol,
    fourChartSymbols,
    setSingleChartSymbol,
    setFourChartSymbols,
    userId,
    setUserId,
    ownStock,
    setOwnStock,
    othersStock,
    setOthersStock,
    setSellBtn,
  } = useContext(UserDataContext);

  const chartSwipe = useRef();
  const overAll = useRef();
  const chart4a = useRef();
  const chart4b = useRef();
  const chart4c = useRef();
  const chart4d = useRef();

  const [isActive, setIsActive] = useState(
    dashboardPreference.default_chart || 'single',
  );
  const deviceWidth = Dimensions.get('window').width;
  const LIGHT_GREY = 'hsl(355, 20%, 90%)';
  const device_width = Dimensions.get('window').width;
  const [compChanging, setCompChanging] = useState(null);
  console.log('compChanging>>>>>', compChanging);
  const [ticker, SetTicker] = useState([]);
  const [sChartPos, setSChartPos] = useState({
    x1: deviceWidth - 90,
    x2: -280,
    y1: 280,
    y2: -(deviceWidth - 90),
  });
  const [fourChartPos, setFourChartPos] = useState({
    a: {x1: -(deviceWidth * 0.5), x2: 140, y1: -140, y2: deviceWidth * 0.5},
    b: {x1: deviceWidth - deviceWidth * 0.5, x2: deviceWidth, y1: 0, y2: 0},
    c: {x1: -(deviceWidth * 0.5), x2: 140, y1: 140, y2: deviceWidth * 0.5},
    d: {x1: deviceWidth - deviceWidth * 0.5, x2: deviceWidth, y1: 0, y2: 0},
  });
  const [fourChartSymA, setFourChartSymA] = useState(fourChartSymbols[0]);
  const [fourChartSymB, setFourChartSymB] = useState(fourChartSymbols[1]);
  const [fourChartSymC, setFourChartSymC] = useState(fourChartSymbols[2]);
  const [fourChartSymD, setFourChartSymD] = useState(fourChartSymbols[3]);
  const [symALoading, setsymALoading] = useState(false);
  const [symBLoading, setsymBLoading] = useState(false);
  const [symCLoading, setsymCLoading] = useState(false);
  const [symDLoading, setsymDLoading] = useState(false);
  const [fourChartDataA, setFourChartDataA] = useState({});
  const [fourChartDataB, setFourChartDataB] = useState({});
  const [fourChartDataC, setFourChartDataC] = useState({});
  const [fourChartDataD, setFourChartDataD] = useState({});
  const [chartData, SetChartData] = useState({});
  const [range, setRange] = useState('1m');
  const [fourRange, setFourRange] = useState('1m');
  const [symbol, setSymbol] = useState(singleChartSymbol);
  const [orderData, setOrderData] = useState({});
  const [loading, setLoading] = useState(null);
  const [showDrag, setShowDrag] = useState(false);
  const [combinedChart, setCombinedChart] = useState(false);
  const [disAllowedAlert, setDisAllowedAlert] = useState(false);
  const [disAllowedProceedStatus, setDisAllowedProceedStatus] = useState(false);
  const [showList, setShowList] = useState(false);
  const [stockData, setStockData] = useState(null);
  const [tickerLoading, setTickerLoading] = useState(undefined);
  const [searchedResult, setSearchedResult] = useState({});
  const [sellLoading, setSellLoading] = useState(null);

  console.log('fourChartDataA>>>>>', fourChartDataA);
  useEffect(() => {
    let ac = new AbortController();

    getTickerData();

    return () => {
      ac.abort();
    };
  }, []);

  useFocusEffect(
    useCallback(() => {
      setSearchedResult({});
      setCompChanging(true);
      let ac = new AbortController();

      chartSwipe.current &&
        chartSwipe.current.scrollTo({
          x: device_width + 1,
          y: 0,
        });

      return () => {
        ac.abort();
      };
    }, [chartSwipe.current]),
  );

  useFocusEffect(
    useCallback(() => {
      let ac = new AbortController();

      setShowDrag(false);

      return () => {
        setShowDrag(false);

        ac.abort();
      };
    }, []),
  );

  useEffect(() => {
    let ac = new AbortController();

    SetChartData({});

    getChartData();

    updateSingleChartSymbol(symbol);

    return () => {
      ac.abort();
    };
  }, [range, symbol]);

  useEffect(() => {
    let ac = new AbortController();

    getFourChartData();

    return () => {
      ac.abort();
    };
  }, [fourRange]);

  // useEffect(() => {
  //   Object.keys(searchedResult).length !== 0 && listenForSymbol();
  // });

  function listenForSymbol() {
    const socket = new WebSocket('wss://data.alpaca.markets/stream');

    const requestData = {
      action: 'authenticate',
      data: {
        key_id: 'PKC58UAZYDQDTQ1EYO37',
        secret_key: 'EDtZVuH6mMUi1cs1HwA549GlegeK1o65Uswu8hMq',
      },
    };

    socket.onopen = () => socket.send(JSON.stringify(requestData));

    socket.onmessage = () => {
      let helperObject = {...searchedResult};

      const listenRequestData = {
        action: 'listen',
        data: {
          streams: [
            `T.${searchedResult.symbol}`,
            `Q.${searchedResult.symbol}`,
            `AM.${searchedResult.symbol}`,
          ],
        },
      };

      socket.send(JSON.stringify(listenRequestData));

      socket.onmessage = ({data}) => {
        const response = JSON.parse(data);
        const responseData = response.data;

        if (response.stream === `Q.${searchedResult.symbol}`) {
          helperObject = {
            ...searchedResult,
            change:
              responseData.P && responseData.p
                ? calculateChangePercentage(responseData).change
                : searchedResult.change,
            change_percent:
              responseData.P && responseData.p
                ? calculateChangePercentage(responseData).changepercent
                : searchedResult.change_percent,
            isNegative:
              responseData.P && responseData.p
                ? calculateChangePercentage(responseData).isNegative
                : searchedResult.isNegative,
            latest_price: responseData.P || searchedResult.latest_price,
          };
        }
      };

      setTimeout(async () => {
        setSearchedResult(helperObject);
      }, 1000);
    };
  }

  const calculateChangePercentage = companyData => {
    const p1 = companyData.P;
    const p2 = companyData.p;

    const changePrice = (p1 - p2) / p2;

    const changepercent = changePrice * 100;

    return {
      changepercent: parseFloat(changepercent).toFixed(2),
      isNegative: Math.sign(changePrice) == -1 ? true : false,
      change: parseFloat(changePrice).toFixed(2),
    };
  };

  const getChartData = async () => {
    const result = await getData(symbol, range);
    SetChartData(result);
  };

  const getFourChartData = () => {
    FourChartDataA(fourChartSymbols[0], false);
    FourChartDataB(fourChartSymbols[1], false);
    FourChartDataC(fourChartSymbols[2], false);
    FourChartDataD(fourChartSymbols[3], false);
  };

  const FourChartDataA = async (sym, setState) => {
    setsymALoading(true);
    const data = await getData(sym, fourRange);
    setState && updateFourChartSymbols(0, sym);
    setFourChartDataA(data);
    setsymALoading(false);
  };

  const FourChartDataB = async (sym, setState) => {
    setsymBLoading(true);
    const data = await getData(sym, fourRange);
    setState && updateFourChartSymbols(1, sym);
    setFourChartDataB(data);
    setsymBLoading(false);
  };

  const FourChartDataC = async (sym, setState) => {
    setsymCLoading(true);
    const data = await getData(sym, fourRange);
    setState && updateFourChartSymbols(2, sym);
    setFourChartDataC(data);
    setsymCLoading(false);
  };

  const FourChartDataD = async (sym, setState) => {
    setsymDLoading(true);
    const data = await getData(sym, fourRange);
    setState && updateFourChartSymbols(3, sym);
    setFourChartDataD(data);
    setsymDLoading(false);
  };

  const updateFourChartSymbols = async (index, sym) => {
    const requestData = {
      index: index,
      four_chart_symbols: sym,
    };

    await dashboard.updateChartpreferences(requestData);

    let customArray = fourChartSymbols;

    customArray.splice(index, 1, sym);

    setFourChartSymbols(customArray);

    switch (index) {
      case 0:
        setFourChartSymA(sym);
        break;

      case 1:
        setFourChartSymB(sym);
        break;

      case 2:
        setFourChartSymC(sym);
        break;

      case 3:
        setFourChartSymD(sym);
        break;
    }
  };

  const updateSingleChartSymbol = async sym => {
    const requestData = {
      single_chart_symbol: sym,
    };

    await dashboard.updateChartpreferences(requestData);
  };

  const FunctionalitylistPopup = useRef();
  const Order = useRef();

  const changeTF = val => {
    setRange(val);
  };

  const changeFourChartRange = val => {
    setFourRange(val);
  };

  const searchResponse = async symbol => {
    setSearchedResult({});
    setLoading(true);
    const res = await company.getQuotesLite(symbol);
    if (res) {
      if (res.keyword == 'success') {
        if (
          res.data &&
          res.data.hasOwnProperty('disallowed_status') &&
          res.data.disallowed_status
        ) {
          setDisAllowedAlert(true);
          setSearchedResult(res.data);
        } else {
          setSearchedResult(res.data);
        }
      } else {
        Snackbar.show({
          duration: Snackbar.LENGTH_SHORT,
          text: res.message,
          backgroundColor: 'red',
        });
      }
    } else {
    }
    setLoading(false);
    setCompChanging(false);
  };

  const companyChanging = () => {
    setCompChanging(true);
  };

  const processOrder = async type => {
    let order = {
      ...searchedResult,
      side: type,
      fromPage: disAllowedProceedStatus ? 'Direct' : '',
    };

    await setOrderData(order);

    Order.current.open();
  };

  const getTickerData = async () => {
    setTickerLoading(true);
    const symbols = [
      'WMT',
      'AAPL',
      'DIS',
      'V',
      'MSFT',
      // 'TSLA',
      // 'INTC',
      // 'PFE',
      // 'AMZN',
    ];
    const query = symbols.toString();
    const res = await dashboard.getTickerData(query);
    if (res) {
      if (res.keyword == 'success') {
        setTickerData(res.data);
        SetTicker(res.data);
        setTickerLoading(false);
      } else {
        Snackbar.show({
          duration: Snackbar.LENGTH_SHORT,
          text: res.message,
          backgroundColor: 'red',
        });
        setTickerData(res.data);
        SetTicker(res.data);
      }
    } else {
      setTickerData([]);
      SetTicker([]);
    }
  };

  const ToggleToScroll = singleChart => {
    chartSwipe.current.scrollTo({
      x: singleChart ? 0 : device_width + 1,
      y: 0,
      animated: true,
    });
    singleChart ? setIsActive('single') : setIsActive('four');
  };

  const isSingleChart = gesture => {
    return (
      gesture.moveX > sChartPos.x1 &&
      gesture.moveX < sChartPos.x2 &&
      gesture.moveY > sChartPos.y1 &&
      gesture.moveY < sChartPos.y2
    );
  };

  const isFourChartA = gesture => {
    return (
      gesture.moveX > fourChartPos.a.x1 &&
      gesture.moveX < fourChartPos.a.x2 &&
      gesture.moveY > fourChartPos.a.y1 &&
      gesture.moveY < fourChartPos.a.y2 &&
      FourChartDataA(searchedResult.symbol, true)
    );
  };

  const isFourChartB = gesture => {
    return (
      gesture.moveX > fourChartPos.b.x1 &&
      gesture.moveX < fourChartPos.b.x2 &&
      gesture.moveY > fourChartPos.a.y1 &&
      gesture.moveY < fourChartPos.a.y2 &&
      FourChartDataB(searchedResult.symbol, true)
    );
  };

  const isFourChartC = gesture => {
    return (
      gesture.moveX > fourChartPos.c.x1 &&
      gesture.moveX < fourChartPos.c.x2 &&
      gesture.moveY > fourChartPos.c.y1 &&
      gesture.moveY < fourChartPos.c.y2 &&
      FourChartDataC(searchedResult.symbol, true)
    );
  };

  const isFourChartD = gesture => {
    return (
      gesture.moveX > fourChartPos.d.x1 &&
      gesture.moveX < fourChartPos.d.x2 &&
      gesture.moveY > fourChartPos.c.y1 &&
      gesture.moveY < fourChartPos.c.y2 &&
      FourChartDataD(searchedResult.symbol, true)
    );
  };

  const getFourChartPos = () => {
    let tempChart = {};

    chart4a.current.measure((_x, _y, width, height, px, py) => {
      const posA = {
        x1: px,
        x2: px + width,
        y1: py,
        y2: py + height,
      };
      const a = px + width;
      const b = py + height;
      const posB = {
        x1: a,
        x2: a + width,
        y1: b,
        y2: b + height,
      };
      tempChart = {...tempChart, a: posA, b: posB};
      setFourChartPos({...fourChartPos, a: posA, b: posB});
    });

    chart4c.current.measure((_x, _y, width, height, px, py) => {
      const posC = {
        x1: px,
        x2: px + width,
        y1: py,
        y2: py + height,
      };
      const a = px + width;
      const b = py + height;
      const posD = {
        x1: a,
        x2: a + width,
        y1: b,
        y2: b + height,
      };
      tempChart = {...tempChart, c: posC, d: posD};
      setFourChartPos(tempChart);
    });
  };

  const getStockDetailsByEchoers = async () => {
    setSellLoading(true);
    let requestData = searchedResult.symbol;
    const res = await dashboard.getStockDetailsByEchoers(requestData);
    if (res) {
      if (res.keyword == 'success') {
        if (res?.data) {
          setStockData(res.data);
          setOwnStock(res.data.own);
          setOthersStock(res.data.others);
          //setShowList(true);
          setSellLoading(false);
          navigation.navigate('stockList', {processOrder});
        }
      } else {
        Snackbar.show({
          duration: Snackbar.LENGTH_SHORT,
          text: res.message,
          backgroundColor: 'red',
        });
      }
    } else {
    }
  };

  const renderCompany = () => {
    return (
      <TouchableOpacity
        onPress={() => {
          navigation.navigate('CompanyDetail', {
            symbol: searchedResult.symbol,
            name: searchedResult.name,
            ticker,
          });
          setSearchedResult({});
        }}>
        <Text
          style={[
            styles.Companyname,
            {
              width: deviceWidth * 0.5,
              fontSize: Platform.OS == 'ios' ? 14 : 16,
            },
          ]}
          numberOfLines={1}
          ellipsizeMode="tail">
          {searchedResult.company_name}
        </Text>

        <Text
          style={[styles.symbol, {fontSize: Platform.OS == 'ios' ? 12 : 14}]}>
          {searchedResult.symbol} - $ {searchedResult.latest_price}
        </Text>

        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <Text
            style={
              searchedResult.change && Math.sign(searchedResult.change) == -1
                ? styles.textred
                : styles.textgreen
            }>
            {searchedResult.change}
          </Text>

          <View
            style={{
              width: 16,
              alignSelf: 'center',
              backgroundColor:
                searchedResult.change && Math.sign(searchedResult.change) == -1
                  ? 'red'
                  : 'green',
              marginHorizontal: 8,
              paddingVertical: 4,
              borderRadius: 4,
            }}>
            <Icon
              name={
                searchedResult.change && Math.sign(searchedResult.change) == -1
                  ? 'arrow-down'
                  : 'arrow-up'
              }
              color="white"
              size={8}
              style={{alignSelf: 'center'}}
            />
          </View>

          <Text
            style={
              searchedResult.change && Math.sign(searchedResult.change) == -1
                ? styles.textred
                : styles.textgreen
            }>
            {searchedResult.change_percent}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <>
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : null}
        keyboardVerticalOffset={Platform.OS === 'ios' && !showDrag ? 40 : 0}
        style={{flex: 1, backgroundColor: theme.themeColor}}>
        <HeaderNav
          title={LITERALS.dashboard}
          isBack={false}
          modalDisable={true}
        />

        {ticker?.length === 0 ? (
          <View style={{marginTop: 5}}>
            <Ticker loading={tickerLoading} />
          </View>
        ) : (
          <Ticker data={ticker} loading={tickerLoading} />
        )}
        <>
          <ScrollView
            scrollEnabled={!showDrag}
            keyboardShouldPersistTaps="handled"
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}>
            <View style={styles.chart}>
              <ScrollView
                onScroll={event => {
                  if (event.nativeEvent.contentOffset.x < device_width * 0.5) {
                    setIsActive('single');
                  } else if (
                    event.nativeEvent.contentOffset.x >
                    device_width * 0.5
                  ) {
                    setIsActive('four');
                  } else {
                  }
                }}
                ref={chartSwipe}
                contentOffset={{x: 100, y: 1000}}
                keyboardShouldPersistTaps="handled"
                horizontal={true}
                pagingEnabled={true}
                showsHorizontalScrollIndicator={false}>
                <View style={styles.firstView}>
                  {showDrag ? (
                    <View
                      style={{
                        width: deviceWidth * 0.875,
                        justifyContent: 'center',
                        alignItems: 'center',
                        borderColor: 'white',
                        borderStyle: 'dashed',
                        borderWidth: 2,
                        borderRadius: 4,
                      }}>
                      <MIcon name="addchart" color="white" size={50} />
                    </View>
                  ) : (
                    <Chart
                      onLayout={event => {
                        event.target.measure(
                          (_x, _y, width, height, px, py) => {
                            const pos = {
                              x1: px,
                              x2: px + width,
                              y1: py,
                              y2: py + height,
                            };

                            setSChartPos(pos);
                          },
                        );
                      }}
                      chartData={chartData}
                      changeTF={changeTF}
                      range={range}
                    />
                  )}
                </View>

                {/* //{////////ITEM)} */}
                <View style={styles.secondView} ref={overAll}>
                  {!combinedChart ? (
                    <View style={styles.fourchart}>
                      {/* chart 1 and 2 */}
                      <View
                        style={{
                          height: 140,
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                        }}>
                        <View
                          ref={chart4a}
                          style={{
                            width: '50%',
                            borderRightColor: theme.white,
                            borderRightWidth: 1,
                            paddingHorizontal: 4,
                            paddingRight: 8,
                            paddingVertical: 4,
                          }}>
                          {showDrag ? (
                            <View style={styles.dragfourchart}>
                              <MIcon name="addchart" color="white" size={48} />
                            </View>
                          ) : (
                            <>
                              {fourChartDataA && fourChartDataA.data ? (
                                <SmallChart
                                  data={fourChartDataA}
                                  loading={symALoading}
                                />
                              ) : null}
                            </>
                          )}
                        </View>
                        <View
                          style={{
                            width: '50%',
                            paddingHorizontal: 4,
                            paddingLeft: 8,
                            paddingVertical: 4,
                          }}
                          ref={chart4b}>
                          {showDrag ? (
                            <View style={styles.dragfourchartRight}>
                              <MIcon name="addchart" color="white" size={48} />
                            </View>
                          ) : (
                            <>
                            {fourChartDataB && fourChartDataB.data ? (
                              <SmallChart
                                data={fourChartDataB}
                                loading={symALoading}
                              />
                            ) : null}
                          </>
                          )}
                        </View>
                      </View>
                      <View
                        style={{
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                          alignItems: 'center',
                        }}>
                        <View
                          style={{
                            width: '47.5%',
                            borderBottomColor: 'white',
                            borderBottomWidth: 1,
                          }}
                        />
                        <View
                          style={{
                            width: '5%',
                            justifyContent: 'center',
                            alignItems: 'center',
                            right: 1,
                            zIndex: 1,
                          }}>
                          <TouchableWithoutFeedback
                            onPress={() => {
                              setCombinedChart(!combinedChart);
                            }}>
                            <Image
                              resizeMode={'contain'}
                              source={DIAMOND}
                              style={{width: 24, height: 24}}
                            />
                          </TouchableWithoutFeedback>
                        </View>
                        <View
                          style={{
                            width: '47.5%',
                            borderBottomColor: 'white',
                            borderBottomWidth: 1,
                          }}
                        />
                      </View>
                      {/* Chart 3 and 4 */}
                      <View
                        style={{
                          height: 140,
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                          borderBottomColor: theme.white,
                          borderBottomWidth: 1,
                          marginTop: -2,
                        }}>
                        <View
                          ref={chart4c}
                          style={{
                            width: '50%',
                            borderRightColor: theme.white,
                            borderRightWidth: 1,
                            paddingHorizontal: 4,
                            paddingRight: 8,
                            paddingVertical: 4,
                          }}>
                          {showDrag ? (
                            <View style={styles.dragfourchart}>
                              <MIcon name="addchart" color="white" size={48} />
                            </View>
                          ) : (
                            <>
                            {fourChartDataC && fourChartDataC.data ? (
                              <SmallChart
                                data={fourChartDataC}
                                loading={symALoading}
                              />
                            ) : null}
                          </>
                          
                          )}
                        </View>
                        <View
                          ref={chart4d}
                          style={{
                            width: '50%',
                            paddingHorizontal: 4,
                            paddingLeft: 8,
                            paddingVertical: 4,
                          }}>
                          {showDrag ? (
                            <View style={styles.dragfourchartRight}>
                              <MIcon name="addchart" color="white" size={48} />
                            </View>
                          ) : (
                            <>
                            {fourChartDataD && fourChartDataD.data ? (
                              <SmallChart
                                data={fourChartDataD}
                                loading={symALoading}
                              />
                            ) : null}
                          </>
                          )}
                        </View>
                      </View>
                      {/* chart 3 and 4 */}
                      <View style={styles.dayview}>
                        <TouchableOpacity
                          onPress={() => changeFourChartRange('1d')}>
                          <Text
                            style={
                              fourRange === '1d' ? styles.active : styles.day
                            }>
                            1D
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          onPress={() => changeFourChartRange('7d')}>
                          <Text
                            style={
                              fourRange === '7d' ? styles.active : styles.day
                            }>
                            1W
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          onPress={() => changeFourChartRange('1m')}>
                          <Text
                            style={
                              fourRange === '1m' ? styles.active : styles.day
                            }>
                            1M
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          onPress={() => changeFourChartRange('6m')}>
                          <Text
                            style={
                              fourRange === '6m' ? styles.active : styles.day
                            }>
                            6M
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          onPress={() => changeFourChartRange('1y')}>
                          <Text
                            style={
                              fourRange === '1y' ? styles.active : styles.day
                            }>
                            1Y
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          onPress={() => changeFourChartRange('5y')}>
                          <Text
                            style={
                              fourRange === '5y' ? styles.active : styles.day
                            }>
                            5Y
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                          onPress={() => changeFourChartRange('max')}>
                          <Text
                            style={
                              fourRange === 'max' ? styles.active : styles.day
                            }>
                            MAX
                          </Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                  ) : (
                    <View style={styles.areachart}>
                      {/* <VictoryChart
                        width={deviceWidth * 0.875}
                        height={320}
                        padding={{top: 60, bottom: 60, left: 64, right: 0}}>
                        <VictoryAxis
                          label={'Dates'}
                          fixLabelOverlap={true}
                          tickCount={4}
                          style={{
                            axis: {
                              stroke: 'transparent',
                            },
                            tickLabels: {
                              fill: LIGHT_GREY,
                            },
                            axisLabel: {
                              fill: theme.secondryColor,
                              padding: 40,
                              fontSize: 12,
                              fontStyle: 'italic',
                              fontWeight: 'bold',
                            },
                          }}
                        />
                        <VictoryAxis
                          label={'Dollar ($)'}
                          fixLabelOverlap={true}
                          orientation="left"
                          dependentAxis
                          style={{
                            axis: {
                              stroke: 'transparent',
                            },
                            tickLabels: {
                              fill: LIGHT_GREY,
                              fontSize: 13,
                            },
                            axisLabel: {
                              fill: theme.secondryColor,
                              padding: 40,
                              fontSize: 12,
                              fontStyle: 'italic',
                              fontWeight: 'bold',
                            },
                          }}
                        />
                        <VictoryLine
                          style={{
                            data: {stroke: theme.secondryColor},
                          }}
                          data={fourChartDataA.data}
                        />
                        <VictoryLine
                          style={{
                            data: {stroke: 'lime'},
                          }}
                          data={fourChartDataB.data}
                        />
                        <VictoryLine
                          style={{
                            data: {stroke: 'red'},
                          }}
                          data={fourChartDataC.data}
                        />
                        <VictoryLine
                          style={{
                            data: {stroke: 'yellow'},
                          }}
                          data={fourChartDataD.data}
                        />
                        <VictoryLegend
                          x={deviceWidth * 0.125}
                          orientation="horizontal"
                          gutter={20}
                          data={[
                            {
                              name: `${fourChartSymA}`,
                              symbol: {fill: '#08d0df'},
                              labels: {fill: 'white'},
                            },
                            {
                              name: `${fourChartSymB}`,
                              symbol: {fill: 'lime'},
                              labels: {fill: 'white'},
                            },
                            {
                              name: `${fourChartSymC}`,
                              symbol: {fill: 'red'},
                              labels: {fill: 'white'},
                            },
                            {
                              name: `${fourChartSymD}`,
                              symbol: {fill: 'yellow'},
                              labels: {fill: 'white'},
                            },
                          ]}
                        />
                      </VictoryChart> */}
                    </View>
                  )}
                </View>
              </ScrollView>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <View>
                  {combinedChart && (
                    <TouchableOpacity
                      onPress={() => setCombinedChart(!combinedChart)}
                      style={{
                        alignSelf: 'flex-end',
                        backgroundColor: theme.greenBg,
                        borderRadius: 4,
                        paddingHorizontal: 12,
                        paddingVertical: 10,
                      }}>
                      <Icon name="arrow-left" color="white" size={16} />
                    </TouchableOpacity>
                  )}
                </View>

                <View
                  style={[
                    {
                      flexDirection: 'row',
                      justifyContent: 'center',
                      alignItems: 'center',
                    },
                    combinedChart ? {marginRight: 8} : {marginLeft: 32},
                  ]}>
                  <TouchableOpacity
                    onPress={() => {
                      ToggleToScroll(true);
                    }}>
                    <View
                      style={
                        isActive == 'single'
                          ? styles.activedot
                          : styles.inactivedot
                      }
                    />
                  </TouchableOpacity>

                  <TouchableOpacity
                    onPress={() => {
                      ToggleToScroll(false);
                    }}>
                    <View
                      style={
                        isActive == 'four'
                          ? styles.activedot
                          : styles.inactivedot
                      }
                    />
                  </TouchableOpacity>
                </View>

                <TouchableOpacity
                  onPress={() => FunctionalitylistPopup.current.open()}
                  style={{
                    alignSelf: 'flex-end',
                    backgroundColor: theme.greenBg,
                    borderRadius: 4,
                    paddingHorizontal: 16,
                    paddingVertical: 8,
                  }}>
                  <Text style={styles.icontext}>I</Text>
                </TouchableOpacity>
              </View>
            </View>

            <View style={styles.tabssec}>
              <View style={styles.tabhead}>
                <View style={[styles.headingactivebtn, styles.tabheadlabel]}>
                  <Text style={styles.headingactivetxt}>Trading</Text>
                </View>

                <View style={[styles.headingbtn, styles.tabheadlabel]}>
                  <TouchableOpacity
                    onPress={() => {
                      navigation.navigate('Trending');
                      setSearchedResult({});
                    }}>
                    <Text style={styles.headingtxt}>Trending</Text>
                  </TouchableOpacity>
                </View>

                <View style={[styles.headingbtn, styles.tabheadlabel]}>
                  <TouchableOpacity
                    onPress={() => {
                      navigation.navigate('News');
                      setSearchedResult({});
                    }}>
                    <Text style={styles.headingtxt}>News</Text>
                  </TouchableOpacity>
                </View>
              </View>

              <View>
                <Search
                  req={searchResponse}
                  companyChanging={companyChanging}
                  onFocus={() => {}}
                  onBlur={() => {}}
                />

                {loading ? (
                  <View
                    style={{
                      flex: 1,
                      justifyContent: 'center',
                      alignItems: 'center',
                      height: 40,
                      margin: 20,
                    }}>
                    <ActivityIndicator
                      size="large"
                      color={theme.secondryColor}
                    />
                  </View>
                ) : null}

                {compChanging == true ? null : (
                  <View style={styles.searchdetails}>
                    <View style={{width: '70%', justifyContent: 'center'}}>
                      {!combinedChart ? (
                        <Draggable
                          shouldReverse={true}
                          onDrag={() => {
                            setShowDrag(true);
                          }}
                          onDragRelease={(_event, gestureState) => {
                            isActive == 'four' && getFourChartPos();

                            isActive == 'single'
                              ? isSingleChart(gestureState) && [
                                  setSymbol(searchedResult.symbol),
                                  setSingleChartSymbol(searchedResult.symbol),
                                ]
                              : isFourChartA(gestureState),
                              isFourChartB(gestureState),
                              isFourChartC(gestureState),
                              isFourChartD(gestureState);

                            setShowDrag(false);
                          }}
                          onRelease={(_event, draggingStatus) => {
                            !draggingStatus && setShowDrag(false);
                          }}
                          onReverse={() => {
                            setShowDrag(false);
                          }}
                          onShortPressRelease={() => {
                            setShowDrag(false);
                          }}>
                          {renderCompany()}
                        </Draggable>
                      ) : (
                        renderCompany()
                      )}
                      {showDrag ? renderCompany() : null}
                    </View>

                    <View style={{width: '30%', justifyContent: 'center'}}>
                      <TouchableOpacity
                        onPress={() => {
                          processOrder('buy'), setSellBtn(false);
                        }}>
                        <View style={styles.buybtn}>
                          <Text style={styles.buyText}>Buy</Text>
                        </View>
                      </TouchableOpacity>

                      <TouchableOpacity
                        onPress={() => {
                          getStockDetailsByEchoers();
                          // processOrder('sell');
                          // setShowList(true);
                          setSellBtn(true);
                        }}>
                        <View style={styles.sellbtn}>
                          {sellLoading ? (
                            <ActivityIndicator
                              size="small"
                              color={theme.white}
                            />
                          ) : (
                            <Text style={styles.sellText}>Sell</Text>
                          )}
                        </View>
                      </TouchableOpacity>
                    </View>
                  </View>
                )}
              </View>
            </View>

            <View style={{paddingVertical: 20}} />
          </ScrollView>
        </>

        <BottomNav routeName="Dashboard" />
      </KeyboardAvoidingView>

      <OverallFunctionality
        functionalitylist={FunctionalitylistPopup}
        onChange={() => FunctionalitylistPopup.current.close()}
        toggle={ToggleToScroll}
        isActive={isActive}
      />

      {isEmpty(orderData) ? null : orderData?.fractionable ? (
        <OrderShareAmount
          buyform={Order}
          orderData={orderData}
          stockData={stockData}
          onChange={() => Order.current.close()}
        />
      ) : (
        <OrderShareQuantity
          buyform={Order}
          orderData={orderData}
          onChange={() => Order.current.close()}
        />
      )}

      {disAllowedAlert &&
        searchedResult &&
        Object.keys(searchedResult).length !== 0 && (
          <Modal
            animationType="slide"
            transparent={true}
            visible={disAllowedAlert}
            onRequestClose={() => {
              setDisAllowedAlert(!disAllowedAlert);
            }}>
            <View
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <View
                style={{
                  width: '87.5%',
                  backgroundColor: theme.primaryColor,
                  borderRadius: 8,
                  margin: 16,
                  padding: 16,
                  shadowColor: '#000',
                  shadowOffset: {
                    width: 0,
                    height: 2,
                  },
                  shadowOpacity: 0.25,
                  shadowRadius: 4,
                  elevation: 4,
                }}>
                <Text
                  style={{
                    fontSize: Platform.OS == 'ios' ? 16 : 18,
                    fontWeight: 'bold',
                    color: theme.secondryColor,
                    textAlign: 'center',
                  }}>
                  {searchedResult.symbol}
                </Text>

                <Text
                  style={{
                    fontSize: Platform.OS == 'ios' ? 12 : 14,
                    fontWeight: '600',
                    color: theme.white,
                    textAlign: 'center',
                    lineHeight: 18,
                    marginVertical: 8,
                  }}>
                  {searchedResult.symbol} trade disallowed due to '
                  <Text
                    style={{
                      fontSize: Platform.OS == 'ios' ? 12 : 14,
                      fontWeight: 'bold',
                      color: theme.white,
                    }}>
                    {searchedResult.disallowed_reason}
                  </Text>
                  '.
                </Text>

                <Text
                  style={{
                    fontSize: Platform.OS == 'ios' ? 12 : 14,
                    fontWeight: '400',
                    color: theme.white,
                    textAlign: 'left',
                    lineHeight: 16,
                    marginBottom: 16,
                  }}>
                  Are you sure that you want to proceed further with{' '}
                  {searchedResult.symbol} trade?
                </Text>

                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-evenly',
                    alignItems: 'center',
                  }}>
                  <TouchableOpacity
                    onPress={() => {
                      setDisAllowedProceedStatus(true);

                      setDisAllowedAlert(false);
                    }}
                    style={{
                      width: '31.25%',
                      backgroundColor: theme.successGreen,
                      paddingHorizontal: 8,
                      paddingVertical: 8,
                      borderRadius: 4,
                    }}>
                    <Text
                      style={{
                        fontSize: 12,
                        fontWeight: '600',
                        color: theme.white,
                        textAlign: 'center',
                      }}>
                      Yes
                    </Text>
                  </TouchableOpacity>

                  <TouchableOpacity
                    onPress={() => {
                      setDisAllowedProceedStatus(false);

                      setSearchedResult({});

                      setDisAllowedAlert(false);
                    }}
                    style={{
                      width: '31.25%',
                      backgroundColor: theme.danger,
                      paddingHorizontal: 8,
                      paddingVertical: 8,
                      borderRadius: 4,
                    }}>
                    <Text
                      style={{
                        fontSize: 12,
                        fontWeight: '600',
                        color: theme.white,
                        textAlign: 'center',
                      }}>
                      No
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </Modal>
        )}

      <View>
        <Modal isVisible={showList}>
          <View style={{backgroundColor: theme.themeColor, borderRadius: 8}}>
            <View
              style={{
                backgroundColor: theme.secondryColor,
                borderBottomColor: 'grey',
                borderBottomWidth: 1,
                borderTopLeftRadius: 8,
                borderTopRightRadius: 8,
                paddingVertical: 8,
                alignItems: 'center',
              }}>
              <Text style={{fontSize: 18, color: 'white', fontWeight: '500'}}>
                Portfolio Share
              </Text>
            </View>

            <Text style={styles.title}>My own Stock</Text>
            {ownStock?.map((item, index) => {
              return (
                <View style={[styles.companylist, {marginTop: 16}]}>
                  <Text key={index} style={[styles.companySymbolTxt]}>
                    {item?.symbol}
                  </Text>

                  <Text style={[styles.companyShareAmtTxt]}>
                    {item?.qty.toFixed(2)} Shares
                  </Text>

                  <Text style={[styles.companyShareAmtTxt]}>
                    $ {item.total_amount.toFixed(2)}
                  </Text>

                  <TouchableOpacity
                    style={[styles.sell]}
                    onPress={() => {
                      setUserId(0), processOrder('sell'), setShowList(false);
                    }}>
                    <Text style={styles.companySellTxt}>Sell</Text>
                  </TouchableOpacity>
                </View>
              );
            })}

            <Text style={[styles.title, {marginTop: 14}]}>Echoed Stocks</Text>
            {othersStock?.map((lol, index) => {
              return (
                <View style={[styles.companylist2, {marginTop: 16}]}>
                  <View style={{flexDirection: 'row'}}>
                    <TouchableOpacity style={styles.logosec}>
                      <Image
                        resizeMode={'contain'}
                        source={TLOGO}
                        style={styles.applogo}
                      />
                    </TouchableOpacity>
                    <View>
                      <TouchableOpacity style={styles.nameView}>
                        <Text key={index} style={styles.nameStyle}>
                          {lol?.name}
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      marginTop: 3,
                    }}>
                    <Text style={styles.companySymbolTxt}>{lol?.symbol}</Text>
                    <Text style={styles.companyShareAmtTxt}>
                      {lol?.qty.toFixed(2)} Shares
                    </Text>
                    <Text style={styles.companyShareAmtTxt}>
                      $ {lol?.total_amount.toFixed(2)}
                    </Text>
                    <TouchableOpacity
                      style={styles.sell}
                      onPress={() => {
                        setUserId(lol.created_by);
                        processOrder('sell');
                        setShowList(false);
                      }}>
                      <Text style={styles.companySellTxt}>Sell</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              );
            })}

            <View style={{marginHorizontal: 12, marginVertical: 12}}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginVertical: 4,
                }}>
                <TouchableOpacity
                  onPress={() => {
                    setShowList(false);
                  }}>
                  <Text
                    style={{
                      fontSize: Platform.OS == 'ios' ? 14 : 16,
                      color: theme.secondryColor,
                      fontWeight: 'bold',
                      textAlign: 'center',
                      marginHorizontal: 16,
                    }}>
                    Cancel
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
      </View>
    </>
  );
};

export default Dashboard;
