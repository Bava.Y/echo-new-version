import React from 'react';
import {ScrollView, Text, View} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import styles from './styles';

const Echo = () => {
  return (
    <>
      <View style={styles.walletContainer}>
        <View style={styles.walletLabelContainer}>
          <Text style={styles.walletLabelTxt}>Echo Wallet Balance</Text>
        </View>

        <View style={styles.walletValueContainer}>
          <Text style={styles.walletValueTxt}>$ 5035.00</Text>
        </View>
      </View>

      <ScrollView
        keyboardShouldPersistTaps={'handled'}
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
        style={[styles.container, {marginVertical: 8}]}>
        <View style={styles.mainsec}>
          <View style={styles.lineContainer}>
            <View style={styles.lineLabelContainer}>
              <Text style={styles.finalLabelTxt}>Echoing</Text>
            </View>

            <View style={styles.lineValueContainer}>
              <Text style={[styles.finalValueTxt, {color: 'red'}]}>
                $ 10.00
              </Text>
            </View>
          </View>

          <View style={{marginTop: 16}}>
            <View style={styles.lineContainer}>
              <View
                style={[
                  styles.lineLabelContainer,
                  {flexDirection: 'row', justifyContent: 'space-between'},
                ]}>
                <View style={{flex: 0.5, justifyContent: 'center'}}>
                  <Text style={styles.lineLabelTxt}>Daniel Jones</Text>
                </View>

                <View
                  style={{
                    flex: 0.5,
                    justifyContent: 'center',
                    alignItems: 'flex-end',
                  }}>
                  <Text style={[styles.lineLabelTxt, {color: 'green'}]}>
                    <Icon name="arrow-up" size={16} color="green" /> 29.5%
                  </Text>
                </View>
              </View>

              <View style={styles.lineValueContainer}>
                <Text style={styles.lineValueRedTxt}>$ 5.00</Text>
              </View>
            </View>

            <View style={[styles.lineContainer, {marginTop: 8}]}>
              <View
                style={[
                  styles.lineLabelContainer,
                  {flexDirection: 'row', justifyContent: 'space-between'},
                ]}>
                <View style={{flex: 0.5, justifyContent: 'center'}}>
                  <Text style={styles.lineLabelTxt}>Kathi Soohey</Text>
                </View>

                <View
                  style={{
                    flex: 0.5,
                    justifyContent: 'center',
                    alignItems: 'flex-end',
                  }}>
                  <Text style={[styles.lineLabelTxt, {color: 'red'}]}>
                    <Icon name="arrow-down" size={16} color="red" /> -2.5%
                  </Text>
                </View>
              </View>

              <View style={styles.lineValueContainer}>
                <Text style={styles.lineValueRedTxt}>$ 5.00</Text>
              </View>
            </View>
          </View>
        </View>

        <View style={[styles.mainsec, {marginTop: 16}]}>
          <View style={styles.lineContainer}>
            <View style={styles.lineLabelContainer}>
              <Text style={styles.finalLabelTxt}>Echoers</Text>
            </View>

            <View style={styles.lineValueContainer}>
              <Text style={[styles.finalValueTxt, {color: 'green'}]}>
                $ 12.00
              </Text>
            </View>
          </View>

          <View style={{marginTop: 16}}>
            <View style={styles.lineContainer}>
              <View
                style={[
                  styles.lineLabelContainer,
                  {flexDirection: 'row', justifyContent: 'space-between'},
                ]}>
                <View style={{flex: 0.5, justifyContent: 'center'}}>
                  <Text style={styles.lineLabelTxt}>Kathi Soohay</Text>
                </View>

                <View
                  style={{
                    flex: 0.5,
                    justifyContent: 'center',
                    alignItems: 'flex-end',
                  }}>
                  <Text style={[styles.lineLabelTxt, {color: 'green'}]}>
                    <Icon name="arrow-up" size={16} color="green" /> 2.5%
                  </Text>
                </View>
              </View>

              <View style={styles.lineValueContainer}>
                <Text style={[styles.lineValueTxt, {color: 'green'}]}>
                  $ 5.00
                </Text>
              </View>
            </View>

            <View style={[styles.lineContainer, {marginTop: 8}]}>
              <View
                style={[
                  styles.lineLabelContainer,
                  {flexDirection: 'row', justifyContent: 'space-between'},
                ]}>
                <View style={{flex: 0.5, justifyContent: 'center'}}>
                  <Text style={styles.lineLabelTxt}>Kathi Soohay</Text>
                </View>

                <View
                  style={{
                    flex: 0.5,
                    justifyContent: 'center',
                    alignItems: 'flex-end',
                  }}>
                  <Text style={[styles.lineLabelTxt, {color: 'green'}]}>
                    <Icon name="arrow-up" size={16} color="green" /> 2%
                  </Text>
                </View>
              </View>

              <View style={styles.lineValueContainer}>
                <Text style={[styles.lineValueTxt, {color: 'green'}]}>
                  $ 5.00
                </Text>
              </View>
            </View>

            <View style={[styles.lineContainer, {marginTop: 8}]}>
              <View
                style={[
                  styles.lineLabelContainer,
                  {flexDirection: 'row', justifyContent: 'space-between'},
                ]}>
                <View style={{flex: 0.5, justifyContent: 'center'}}>
                  <Text style={styles.lineLabelTxt}>Kathi Soohay</Text>
                </View>

                <View
                  style={{
                    flex: 0.5,
                    justifyContent: 'center',
                    alignItems: 'flex-end',
                  }}>
                  <Text style={[styles.lineLabelTxt, {color: 'green'}]}>
                    <Icon name="arrow-up" size={16} color="green" /> 72%
                  </Text>
                </View>
              </View>

              <View style={styles.lineValueContainer}>
                <Text style={[styles.lineValueTxt, {color: 'green'}]}>
                  $ 5.00
                </Text>
              </View>
            </View>
          </View>
        </View>

        <View style={{paddingVertical: 16}} />
      </ScrollView>
    </>
  );
};

export default Echo;
