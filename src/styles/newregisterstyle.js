import {Platform, StyleSheet, Dimensions} from 'react-native';
import theme from '../utils/theme';


const deviceHeight = Dimensions.get('window').height;

let style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.themeColor,
  },
  errormsg: {
    fontSize: Platform.OS == 'ios' ? 12 : 14,
    color: '#ff0000a1',
    marginHorizontal: 4,
    marginVertical: 10,
  },
  signinBtn: {
    alignItems: 'center',
    backgroundColor: theme.secondryColor,
    borderRadius: 4,
    marginHorizontal: 16,
    marginVertical: 16,
    paddingHorizontal: 16,
    paddingVertical: 8,
  },
  signintext: {
    fontSize: Platform.OS == 'ios' ? 12 : 14,
    color: theme.white,
    fontWeight: '400',
    textAlign: 'center',
  },
  datePickerStyle: {
    width: '100%',
  },
  spaceBetween: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: 4,
    paddingVertical: 4,
  },
  selectfieldvalue: {
    borderBottomColor: 'white',
    borderBottomWidth: 1,
    paddingBottom: 8,
  },
  pickerItemStyle: {
    height: 36,
    fontSize: Platform.OS == 'ios' ? 12 : 14,
    fontWeight: '400',
    color: 'white',
    textAlign: 'left',
    left: Platform.OS == 'ios' ? -8 : 0,
  },
  dropdownWrapper: {
    width: '100%',
    height: Platform.OS == 'ios' ? 36 : 48,
    // borderBottomColor: 'white',
    // borderBottomWidth: 1,
    backgroundColor: 'transparent',
  },
  dropdownItemContainer: {
   
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  dropdownLabelContainer: {
    flex: 0.9,
    justifyContent: 'center',
  },
  dropdownLabelText: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    fontWeight: '400',
    color: 'white',
    textAlign: 'left',
  },
  dropdownIconContainer: {
    flex: 0.1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  dropdownItemText: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    fontWeight: '400',
    color: 'black',
    textAlign: 'left',
  },
  dropdownSelectedItemText: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    fontWeight: '400',
    color: theme.secondryColor,
    textAlign: 'left',
  },
  uploadbtn: {
    width: '62.5%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: theme.secondryColor,
    borderRadius: 24,
    paddingHorizontal: 16,
    paddingVertical: 8,
  },
  uploadtxt: {
    fontSize: Platform.OS == 'ios' ? 12 : 14,
    color: theme.white,
    fontWeight: '400',
    textAlign: 'center',
    marginHorizontal: 4,
  },
  listBroker:{
    flex:1,
    color:'white',
    height: Platform.OS == 'ios' ? 36 : 48,
    borderColor: 'white',
    borderBottomWidth:1,
    backgroundColor: 'transparent',
   marginVertical: 8,
    width:"100%"
  },

  listBrokerText:{
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    fontWeight: '500',
    color: 'white',
    textAlign: 'left',
    marginLeft:-3
   // paddingHorizontal: Platform.OS == 'ios' ? 0 : 0,
  },

  brokerItemText:{
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    fontWeight: '400',
    color: 'black',
    textAlign: 'left',
  },
  listBrokerSelectedItemText:{
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    fontWeight: '400',
    color: theme.secondryColor,
    textAlign: 'left',
  },
  tdModalContainer: {
    height: Platform.OS == 'ios' ? deviceHeight * 0.75 : deviceHeight * 0.9,
    backgroundColor: theme.white,
    borderRadius: 8,
  },
  tdModalHeaderContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: theme.themeColor,
    borderBottomColor: 'grey',
    borderBottomWidth: 1,
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
    paddingHorizontal: 16,
    paddingVertical: 16,
  },
  headingTxt: {
    fontSize: Platform.OS == 'ios' ? 16 : 18,
    color: theme.white,
    fontWeight: 'bold',
  },
});

export default style;
