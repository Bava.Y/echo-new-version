import {useFocusEffect} from '@react-navigation/core';
import React, {useCallback, useContext, useState} from 'react';
import {Platform, Text, TouchableOpacity, View} from 'react-native';
import Modal from 'react-native-modal';
import {profile} from '../api';
import {UserDataContext} from '../context/UserDataContext';
import theme from '../utils/theme';
import AmeritradeRefreshToken from './AmeritradeRefreshToken';

const AmeritradeAlert = (props) => {
  // AmeritradeAlert Variables
  const [
    ameritradeSelectedAccountId,
    setAmeritradeSelectedAccountId,
  ] = useState(null);

  // Context Variables
  const {
    ameritradeRefreshTokenStatus,
    setAmeritradeRefreshTokenStatus,
    token,
  } = useContext(UserDataContext);

  // Other Variables
  const [blinkStatus, setBlinkStatus] = useState(false);

  useFocusEffect(
    useCallback(() => {
      let isFocus = true;

      getBankDetail();

      // Change the state every second or the time given by User.
      const interval = setInterval(() => {
        setBlinkStatus((blinkStatus) => !blinkStatus);
      }, 1000);

      return () => {
        isFocus = false;

        clearInterval(interval);
      };
    }, []),
  );

  const getBankDetail = async () => {
    let response = await profile.getBankDetail(token);

    if (response.data) {
      let myData = response.data;

      setAmeritradeSelectedAccountId(myData.account_id || null);
    }
  };

  const handleAmeritradeRefreshTokenStatus = (status = false) => {
    status && props.close();

    setAmeritradeRefreshTokenStatus(!ameritradeRefreshTokenStatus);
  };

  return (
    <Modal
      isVisible={props.modalStatus}
      onBackdropPress={() => {
        props.close();
      }}>
      <View
        style={{
          backgroundColor: theme.primaryColor,
          borderRadius: 4,
        }}>
        <View
          style={{
            backgroundColor: theme.secondryColor,
            paddingHorizontal: 8,
            paddingVertical: 8,
            borderTopLeftRadius: 4,
            borderTopRightRadius: 4,
          }}>
          <Text
            style={{
              fontSize: Platform.OS == 'ios' ? 14 : 16,
              color: blinkStatus ? '#FF0000' : theme.white,
              fontWeight: 'bold',
              textAlign: 'center',
            }}>
            ALERT
          </Text>
        </View>

        <View
          style={{
            marginVertical: 16,
            marginHorizontal: 16,
          }}>
          <Text
            style={{
              fontSize: Platform.OS == 'ios' ? 12 : 14,
              fontWeight: '900',
              color: theme.white,
              textAlign: 'center',
              marginVertical: 8,
            }}>
            Your TD Ameritrade session got expired!
          </Text>

          <Text
            style={{
              fontSize: Platform.OS == 'ios' ? 10 : 12,
              color: theme.textColor,
              fontWeight: '600',
              textAlign: 'left',
              lineHeight: 16,
              marginVertical: 8,
            }}>
            NOTE: You can trade only if you have refreshed your TD Ameritrade
            session.
          </Text>

          <Text
            style={{
              fontSize: 12,
              color: theme.white,
              fontWeight: '600',
              textAlign: 'left',
              marginVertical: 8,
            }}>
            Would you like to refresh the session?
          </Text>

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-evenly',
              marginTop: 8,
            }}>
            <TouchableOpacity
              style={{
                flex: 0.25,
                alignItems: 'center',
                backgroundColor: theme.danger,
                paddingHorizontal: 6,
                paddingVertical: 6,
                borderRadius: 4,
              }}
              onPress={() => {
                props.close();
              }}>
              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 10 : 12,
                  color: theme.white,
                  fontWeight: '600',
                  textAlign: 'center',
                }}>
                No
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                flex: 0.25,
                alignItems: 'center',
                backgroundColor: theme.successGreen,
                paddingHorizontal: 6,
                paddingVertical: 6,
                borderRadius: 4,
              }}
              onPress={() => {
                handleAmeritradeRefreshTokenStatus();
              }}>
              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 10 : 12,
                  color: theme.white,
                  fontWeight: '600',
                  textAlign: 'center',
                }}>
                Yes
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>

      {ameritradeSelectedAccountId && (
        <AmeritradeRefreshToken
          ameritradeSelectedAccountId={ameritradeSelectedAccountId}
          modalStatus={ameritradeRefreshTokenStatus}
          close={handleAmeritradeRefreshTokenStatus}
          getBankDetail={() => {}}
        />
      )}
    </Modal>
  );
};

export default AmeritradeAlert;
