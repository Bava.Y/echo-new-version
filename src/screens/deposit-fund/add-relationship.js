import {useFocusEffect} from '@react-navigation/native';
import React, {useCallback, useContext, useState} from 'react';
import {
  ActivityIndicator,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import SelectDropdown from 'react-native-select-dropdown';
import Snackbar from 'react-native-snackbar';
import FontAwesomeIcons from 'react-native-vector-icons/FontAwesome5';
import {profile} from '../../api/index';
import BottomNav from '../../components/BottomNav';
import HeaderNav from '../../components/HeaderNav';
import {UserDataContext} from '../../context/UserDataContext';
import theme from '../../utils/theme';

const AddRelationship = (props) => {
  // AddRelationship Variables
  const [relationshiptype, setRelationshipType] = useState(null);
  const [accountNumber, setAccountNumber] = useState(null);
  const [accountName, setAccountName] = useState(null);
  const [accountType, setAccountType] = useState(null);
  const [bankRoutingNumber, setBankRoutingNumber] = useState(null);
  const [bankName, setBankName] = useState(null);
  const [bankCode, setBankCode] = useState(null);
  const [bankCodeType, setBankCodeType] = useState('ABA');

  // Context Variables
  const {achRelationshipStatus, wireRelationshipStatus} = useContext(
    UserDataContext,
  );

  // Error Variables
  const initialError = {
    relationshiptype: false,
    accountNumber: false,
    invalidAccountNumber: false,
    accountName: false,
    accountType: false,
    bankRoutingNumber: false,
    bankName: false,
    bankCode: false,
    invalidBankCode: false,
    bankCodeType: false,
  };
  const [validationError, setValidationError] = useState({...initialError});

  // Other Variables
  const [loading, setLoading] = useState(false);
  const [realtionshipOptions, setRealtionshipOptions] = useState(null);
  const [helperAccountNumber, setHelperAccountNumber] = useState(null);

  const accountOptions = [
    {label: 'Checking', value: 'CHECKING'},
    {label: 'Savings', value: 'SAVINGS'},
  ];

  const bankCodeOptions = ['ABA'];

  useFocusEffect(
    useCallback(() => {
      let isFocus = true;

      handleRelationshipTypes();

      return () => {
        isFocus = false;
      };
    }, []),
  );

  const handleRelationshipTypes = () => {
    let realtionshipOptions = [];

    !achRelationshipStatus &&
      realtionshipOptions.push({label: 'ACH', value: 'ACH'});

    !wireRelationshipStatus &&
      realtionshipOptions.push({
        label: 'Wire',
        value: 'Wire',
      });

    achRelationshipStatus &&
      setRelationshipType({
        label: 'Wire',
        value: 'Wire',
      });

    wireRelationshipStatus && setRelationshipType({label: 'ACH', value: 'ACH'});

    setRealtionshipOptions(realtionshipOptions);
  };

  const encryptText = (string, showDigits) => {
    let encryptedStr;

    string = string ? string : '';

    if (string.length > showDigits) {
      const str = string
        .slice(0, string.length - showDigits)
        .replace(/[a-z0-9]/g, 'X');

      encryptedStr = str + string.slice(-showDigits);
    } else {
      encryptedStr = string ? string : null;
    }

    return encryptedStr;
  };

  const handleSave = () => {
    if (checkCondition()) {
      handleRequest();
    } else {
      handleErrorValidation();
    }
  };

  const checkCondition = () => {
    if (
      (relationshiptype?.value ?? null) &&
      helperAccountNumber &&
      helperAccountNumber.length >= 1
    ) {
      switch (relationshiptype?.value ?? null) {
        case 'ACH':
          if (
            accountName &&
            (accountType?.value ?? null) &&
            bankRoutingNumber
          ) {
            return true;
          } else {
            return false;
          }

        case 'Wire':
          if (bankName && bankCode && bankCode.length === 1 && bankCodeType) {
            return true;
          } else {
            return false;
          }

        default:
          return true;
      }
    } else {
      return false;
    }
  };

  const handleRequest = () => {
    setLoading(true);

    const requestData = handleRequestData();

    switch (relationshiptype?.value ?? null) {
      case 'ACH':
        handleACHRequest(requestData);
        break;

      case 'Wire':
        handleWireRequest(requestData);
        break;

      default:
        break;
    }
  };

  const handleRequestData = () => {
    switch (relationshiptype?.value ?? null) {
      case 'ACH':
        return {
          bank_account_number: helperAccountNumber,
          account_owner_name: accountName,
          bank_account_type: accountType?.value ?? null,
          bank_routing_number: bankRoutingNumber,
          
        };

      case 'Wire':
        return {
          account_number: helperAccountNumber,
          name: bankName,
          bank_code: bankCode,
          bank_code_type: bankCodeType,
        };

      default:
        break;
    }
  };

  const handleACHRequest = async (requestData) => {
    const response = await profile.createACH(requestData);

    handleResponse(response);
  };

  const handleWireRequest = async (requestData) => {
    const response = await profile.createWire(requestData);

    handleResponse(response);
  };

  const handleResponse = (response) => {
    if (response && response.keyword == 'success') {
      setLoading(false);

      Snackbar.show({duration: Snackbar.LENGTH_SHORT, text: response.message});

      props.navigation.goBack();
    } else {
      setLoading(false);

      Snackbar.show({
        backgroundColor: 'red',
        duration: Snackbar.LENGTH_SHORT,
        text: response.message,
      });
    }
  };

  const handleErrorValidation = () => {
    setValidationError({
      ...validationError,
      relationshiptype: relationshiptype?.value ? false : true,
      accountNumber: helperAccountNumber ? false : true,
      invalidAccountNumber:
        helperAccountNumber && helperAccountNumber.length >= 1 ? false : true,
      accountName:
        relationshiptype?.value == 'ACH' && !accountName ? true : false,
      accountType:
        relationshiptype?.value == 'ACH' && !(accountType?.value ?? null)
          ? true
          : false,
      bankRoutingNumber:
        relationshiptype?.value == 'ACH' && !bankRoutingNumber ? true : false,
      bankName: relationshiptype?.value == 'Wire' && !bankName ? true : false,
      bankCode: relationshiptype?.value == 'Wire' && !bankCode ? true : false,
      invalidBankCode:
        relationshiptype?.value == 'Wire' && bankCode && bankCode.length === 9
          ? false
          : true,
      bankCodeType:
        relationshiptype?.value == 'Wire' && !bankCodeType ? true : false,
    });
  };

  return (
    <>
      <HeaderNav title="Add Relationship" isBack={true} />

      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : null}
        keyboardVerticalOffset={Platform.OS === 'ios' ? 40 : 0}
        style={styles.container}>
        <ScrollView
          keyboardShouldPersistTaps="handled"
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
          style={{flex: 1, paddingHorizontal: 8, paddingVertical: 8}}>
          <View style={styles.pageContainer}>
            <Text style={styles.heading}>
              Relationship Type<Text style={styles.mandatory}> *</Text>
            </Text>

            <SelectDropdown
              buttonStyle={styles.dropdownWrapper}
              data={realtionshipOptions ?? []}
              defaultButtonText="Relationship Type"
              dropdownStyle={{width: '80%', marginLeft: 8, alignSelf: 'center'}}
              onSelect={(selectedItem) => {
                setValidationError({
                  ...validationError,
                  relationshiptype: false,
                });

                setRelationshipType(selectedItem);
              }}
              renderCustomizedButtonChild={(selectedItem) => {
                return (
                  <View style={styles.dropdownItemContainer}>
                    <View style={styles.dropdownLabelContainer}>
                      <Text style={styles.dropdownLabelText}>
                        {selectedItem?.label ?? 'Relationship Type'}
                      </Text>
                    </View>
                    <View style={styles.dropdownIconContainer}>
                      <FontAwesomeIcons
                        name="caret-down"
                        size={16}
                        color={'white'}
                      />
                    </View>
                  </View>
                );
              }}
              rowStyle={{borderBottomWidth: 0}}
              rowTextForSelection={(item) => item?.label}
              rowTextStyle={styles.dropdownItemText}
              selectedRowTextStyle={styles.dropdownSelectedItemText}
              defaultValue={relationshiptype}
            />

            {validationError.relationshiptype && (
              <Text style={styles.errorMsg}>Select relationship type</Text>
            )}

            <View style={styles.selectfield}>
              <Text style={styles.heading}>
                Account No<Text style={styles.mandatory}> *</Text>
              </Text>

              <TextInput
                keyboardType="default"
                maxLength={17}
                value={accountNumber}
                onFocus={() => {
                  setAccountNumber(helperAccountNumber);
                }}
                onBlur={() => {
                  setAccountNumber(encryptText(accountNumber, 4));
                }}
                onChangeText={(txt) => {
                  setValidationError({
                    ...validationError,
                    accountNumber: txt ? false : true,
                    invalidAccountNumber: false,
                  });

                  setAccountNumber(txt);
                  setHelperAccountNumber(txt);
                }}
                style={styles.selectinput}
              />

              {validationError.accountNumber ? (
                <Text style={styles.errorMsg}>Enter account no</Text>
              ) : (
                validationError.invalidAccountNumber && (
                  <Text style={styles.errorMsg}>Enter a valid account no</Text>
                )
              )}
            </View>

            {relationshiptype?.value == 'ACH' && (
              <>
                <View style={styles.selectfield}>
                  <Text style={styles.heading}>
                    Account Name<Text style={styles.mandatory}> *</Text>
                  </Text>

                  <TextInput
                    keyboardType="default"
                    value={accountName}
                    onChangeText={(txt) => {
                      setValidationError({
                        ...validationError,
                        accountName: txt ? false : true,
                      });

                      setAccountName(txt);
                    }}
                    style={styles.selectinput}
                  />

                  {validationError.accountName && (
                    <Text style={styles.errorMsg}>Enter account name</Text>
                  )}
                </View>

                <View style={styles.selectfield}>
                  <Text style={styles.heading}>
                    Account Type<Text style={styles.mandatory}> *</Text>
                  </Text>

                  <SelectDropdown
                    buttonStyle={styles.dropdownWrapper}
                    data={accountOptions ?? []}
                    defaultButtonText="Account Type"
                    dropdownStyle={{
                      width: '80%',
                      marginLeft: 8,
                      alignSelf: 'center',
                    }}
                    onSelect={(selectedItem) => {
                      setValidationError({
                        ...validationError,
                        accountType: selectedItem ? false : true,
                      });

                      setAccountType(selectedItem);
                    }}
                    renderCustomizedButtonChild={(selectedItem) => {
                      return (
                        <View style={styles.dropdownItemContainer}>
                          <View style={styles.dropdownLabelContainer}>
                            <Text style={styles.dropdownLabelText}>
                              {selectedItem?.label ?? 'Account Type'}
                            </Text>
                          </View>
                          <View style={styles.dropdownIconContainer}>
                            <FontAwesomeIcons
                              name="caret-down"
                              size={16}
                              color={'white'}
                            />
                          </View>
                        </View>
                      );
                    }}
                    rowStyle={{borderBottomWidth: 0}}
                    rowTextForSelection={(item) => item?.label}
                    rowTextStyle={styles.dropdownItemText}
                    selectedRowTextStyle={styles.dropdownSelectedItemText}
                    defaultValue={accountType}
                  />

                  {validationError.accountType && (
                    <Text style={styles.errorMsg}>Select account type</Text>
                  )}
                </View>

                <View style={styles.selectfield}>
                  <Text style={styles.heading}>
                    Routing No<Text style={styles.mandatory}> *</Text>
                  </Text>

                  <TextInput
                    keyboardType="number-pad"
                    value={bankRoutingNumber}
                    onChangeText={(txt) => {
                      setValidationError({
                        ...validationError,
                        bankRoutingNumber: txt ? false : true,
                      });

                      setBankRoutingNumber(txt);
                    }}
                    style={styles.selectinput}
                  />

                  {validationError.bankRoutingNumber && (
                    <Text style={styles.errorMsg}>Enter routing no</Text>
                  )}
                </View>
              </>
            )}

            {relationshiptype?.value == 'Wire' && (
              <>
                <View style={styles.selectfield}>
                  <Text style={styles.heading}>
                    Bank Name<Text style={styles.mandatory}> *</Text>
                  </Text>

                  <TextInput
                    keyboardType="default"
                    value={bankName}
                    onChangeText={(txt) => {
                      setValidationError({
                        ...validationError,
                        bankName: txt ? false : true,
                      });

                      setBankName(txt);
                    }}
                    style={styles.selectinput}
                  />

                  {validationError.bankName && (
                    <Text style={styles.errorMsg}>Enter bank name</Text>
                  )}
                </View>

                <View style={styles.selectfield}>
                  <Text style={styles.heading}>
                    Bank Code<Text style={styles.mandatory}> *</Text>
                  </Text>

                  <TextInput
                    keyboardType="default"
                    autoCapitalize="characters"
                    maxLength={9}
                    value={bankCode}
                    onChangeText={(txt) => {
                      setValidationError({
                        ...validationError,
                        bankCode: txt ? false : true,
                        invalidBankCode: false,
                      });

                      setBankCode(txt);
                    }}
                    style={styles.selectinput}
                  />

                  {validationError.bankCode ? (
                    <Text style={styles.errorMsg}>Enter bank code</Text>
                  ) : (
                    validationError.invalidBankCode && (
                      <Text style={styles.errorMsg}>
                        Enter a valid bank code
                      </Text>
                    )
                  )}
                </View>

                <View style={styles.selectfield}>
                  <Text style={styles.heading}>
                    Bank Code Type<Text style={styles.mandatory}> *</Text>
                  </Text>

                  <SelectDropdown
                    buttonStyle={styles.dropdownWrapper}
                    data={bankCodeOptions ?? []}
                    defaultButtonText="Account Type"
                    dropdownStyle={{
                      width: '80%',
                      marginLeft: 8,
                      alignSelf: 'center',
                    }}
                    onSelect={(selectedItem) => {
                      setValidationError({
                        ...validationError,
                        bankCodeType: selectedItem ? false : true,
                      });

                      setBankCodeType(selectedItem);
                    }}
                    renderCustomizedButtonChild={(selectedItem) => {
                      return (
                        <View style={styles.dropdownItemContainer}>
                          <View style={styles.dropdownLabelContainer}>
                            <Text style={styles.dropdownLabelText}>
                              {selectedItem ?? 'Account Type'}
                            </Text>
                          </View>
                          <View style={styles.dropdownIconContainer}>
                            <FontAwesomeIcons
                              name="caret-down"
                              size={16}
                              color={'white'}
                            />
                          </View>
                        </View>
                      );
                    }}
                    rowStyle={{borderBottomWidth: 0}}
                    rowTextForSelection={(item) => item}
                    rowTextStyle={styles.dropdownItemText}
                    selectedRowTextStyle={styles.dropdownSelectedItemText}
                    defaultValue={bankCodeType}
                  />
                </View>

                {validationError.bankCodeType && (
                  <Text style={styles.errorMsg}>Select bank code type</Text>
                )}
              </>
            )}

            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-evenly',
                alignItems: 'center',
                marginVertical: 16,
              }}>
              {loading ? (
                <View style={styles.savebtn}>
                  <ActivityIndicator size="small" color={theme.white} />
                </View>
              ) : (
                <TouchableOpacity
                  onPress={() => {
                    handleSave();
                  }}
                  style={styles.savebtn}>
                  <Text style={styles.btntxt}>Save</Text>
                </TouchableOpacity>
              )}
              <TouchableOpacity
                onPress={() => {
                  props.navigation.goBack();
                }}
                style={styles.cancelbtn}>
                <Text style={styles.btntxt}>Cancel</Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
      <BottomNav routeName="Profile" />
    </>
  );
};

const styles = StyleSheet.create({
  container: {flex: 1, backgroundColor: theme.themeColor},
  pageContainer: {
    backgroundColor: theme.primaryColor,
    borderRadius: 8,
    paddingHorizontal: 16,
    paddingVertical: 16,
  },
  heading: {
    fontSize: Platform.OS == 'ios' ? 12 : 14,
    color: theme.white,
    fontWeight: '600',
  },
  mandatory: {
    fontSize: Platform.OS == 'ios' ? 12 : 14,
    color: 'red',
    fontWeight: '600',
  },
  selectfield: {marginVertical: 8},
  selectfieldvalue: {
    borderBottomColor: 'white',
    borderBottomWidth: 1,
    paddingBottom: 8,
  },
  pickerItemStyle: {
    height: Platform.OS == 'ios' ? 36 : 48,
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    fontWeight: '400',
    color: 'white',
    textAlign: 'left',
    left: -8,
  },
  dropdownWrapper: {
    width: '100%',
    height: Platform.OS == 'ios' ? 36 : 48,
    borderBottomColor: 'white',
    borderBottomWidth: 1,
    backgroundColor: 'transparent',
    marginVertical: 8,
  },
  dropdownItemContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  dropdownLabelContainer: {
    flex: 0.9,
    justifyContent: 'center',
  },
  dropdownLabelText: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    fontWeight: '400',
    color: 'white',
    textAlign: 'left',
  },
  dropdownIconContainer: {
    flex: 0.1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  dropdownItemText: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    fontWeight: '400',
    color: 'black',
    textAlign: 'left',
  },
  dropdownSelectedItemText: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    fontWeight: '400',
    color: theme.secondryColor,
    textAlign: 'left',
  },
  selectinput: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    color: 'white',
    borderBottomColor: 'white',
    borderBottomWidth: 1,
    paddingVertical: 10,
  },
  errorMsg: {
    fontSize: Platform.OS == 'ios' ? 12 : 14,
    color: '#ff0000a1',
    textAlign: 'left',
    marginVertical: 8,
  },
  savebtn: {
    width: '37.5%',
    backgroundColor: theme.secondryColor,
    borderColor: theme.secondryColor,
    borderWidth: 1,
    borderRadius: 24,
    paddingHorizontal: 16,
    paddingVertical: 8,
  },
  cancelbtn: {
    width: '37.5%',
    backgroundColor: '#ff0000c4',
    borderColor: '#ff0000c4',
    borderWidth: 1,
    borderRadius: 24,
    paddingHorizontal: 16,
    paddingVertical: 8,
  },
  btntxt: {
    fontSize: Platform.OS == 'ios' ? 12 : 14,
    color: theme.white,
    fontWeight: '400',
    textAlign: 'center',
  },
});

export default AddRelationship;
