import {isEmpty} from 'lodash';
import React from 'react';
import {
  ActivityIndicator,
  Dimensions,
  Platform,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {
  VictoryArea,
  VictoryAxis,
  VictoryChart,
  VictoryTooltip,
  VictoryVoronoiContainer,
} from 'victory-native';
import moment from 'moment';
import theme from '../../utils/theme';
import styles from './styles';
import {
  CandlestickChart,
  LineChart,
  LineChartCursorCrosshair,
  LineChartCursorLine,
  LineChartCursorProps,
} from 'react-native-wagmi-charts';
import {GestureHandlerRootView} from 'react-native-gesture-handler';

const Chart = ({chartData, changeTF, range, onLayout}) => {
  const deviceWidth = Dimensions.get('window').width;

  const price = chartData.price ? parseFloat(chartData.price) : chartData.price;

  return (
    <>
      <View
        style={styles.areachart}
        //onLayout={onLayout}
      >
        {!chartData.data ? (
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <ActivityIndicator size="large" color="white" />
          </View>
        ) : (
          <>
            <View style={styles.sharerate}>
              <Text style={styles.Companyname}>{chartData.symbol}</Text>

              <Text
                style={
                  chartData.change && Math.sign(chartData.change) == -1
                    ? styles.sharepercentred
                    : styles.sharepercentgreen
                }>
                {isEmpty(chartData) ? null : `${chartData.changePercent} %`}
              </Text>
            </View>

            <View style={[styles.sharerate, {marginVertical: 4}]}>
              <Text style={styles.shareamt}>
                {price ? `$ ${price.toFixed(2)}` : ''}
              </Text>

              <Text
                style={
                  chartData.change && Math.sign(chartData.change) == -1
                    ? styles.sharepercentred
                    : styles.sharepercentgreen
                }>
                {isEmpty(chartData)
                  ? null
                  : `${chartData.change}${
                      Platform.OS == 'ios' ? '    ' : '\t\t\t'
                    }`}
              </Text>
            </View>

            {chartData.data.length == 0 && (
              <View
                style={{
                  flex: 1,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    fontSize: Platform.OS == 'ios' ? 12 : 14,
                    color: theme.white,
                    textAlign: 'center',
                  }}>
                  No chart preview available
                </Text>
              </View>
            )}

            {chartData.data.length != 0 && (
              <GestureHandlerRootView>
                <LineChart.Provider data={chartData?.data}>
                  <LineChart width={deviceWidth * 0.88}>
                    <LineChart.Path color={theme.secondryColor} width={1.5}>
                      <LineChart.Tooltip
                        textStyle={{color: 'white', fontSize: 13}}
                      />
                      <LineChart.Gradient />
                    </LineChart.Path>
                    <LineChart.CursorLine color="white" />

                    <LineChart.Tooltip position="bottom">
                      <LineChart.DatetimeText
                        style={{
                          borderRadius: 4,
                          color: 'white',
                          fontSize: 13,
                          padding: 4,
                        }}
                      />
                    </LineChart.Tooltip>
                  </LineChart>
                </LineChart.Provider>
              </GestureHandlerRootView>
              // <VictoryChart
              //   width={deviceWidth * 0.875}
              //   height={260}
              //   padding={{top: 0, bottom: 0, left: 0, right: 0}}
              //   maxDomain={{y: chartData.max}}
              //   minDomain={{y: chartData.min}}
              //   containerComponent={
              //     <VictoryVoronoiContainer
              //       voronoiDimension="x"
              //       labels={({datum}) =>
              //         `$ ${
              //           Boolean(datum?.y ?? null)
              //             ? parseFloat(datum.y).toFixed(2)
              //             : '0.00'
              //         }${
              //           Boolean(datum?.x ?? null)
              //             ? ` | ${moment(datum.x).format('ll hh:mm A')}`
              //             : ''
              //         }`
              //       }
              //       labelComponent={
              //         <VictoryTooltip
              //           flyoutPadding={12}
              //           center={{
              //             x: deviceWidth * 0.5,
              //           }}
              //         />
              //       }
              //     />
              //   }>
              //   <VictoryAxis
              //     style={{
              //       axis: {
              //         stroke: 'transparent',
              //       },
              //       axisLabel: {
              //         fill: 'transparent',
              //       },
              //       ticks: {stroke: 'transparent'},
              //       tickLabels: {
              //         fill: 'transparent',
              //       },
              //     }}
              //   />

              //   <VictoryAxis
              //     dependentAxis={false}
              //     style={{
              //       axis: {
              //         stroke: 'transparent',
              //       },
              //       tickLabels: {
              //         fill: 'transparent',
              //       },
              //       grid: {
              //         fill: 'transparent',
              //         stroke: 'transparent',
              //       },
              //     }}
              //   />

              //   <VictoryArea
              //     interpolation="natural"
              //     style={{
              //       data: {
              //         fill: theme.secondryColor,
              //         fillOpacity: 0.5,
              //         stroke: theme.secondryColor,
              //         strokeWidth: 2,
              //       },
              //     }}
              //     data={chartData.data}
              //   />
              // </VictoryChart>
            )}

            <View style={styles.dayview}>
              <TouchableOpacity onPress={() => changeTF('1d')}>
                <Text style={range === '1d' ? styles.active : styles.day}>
                  1D
                </Text>
              </TouchableOpacity>

              <TouchableOpacity onPress={() => changeTF('7d')}>
                <Text style={range === '7d' ? styles.active : styles.day}>
                  1W
                </Text>
              </TouchableOpacity>

              <TouchableOpacity onPress={() => changeTF('1m')}>
                <Text style={range === '1m' ? styles.active : styles.day}>
                  1M
                </Text>
              </TouchableOpacity>

              <TouchableOpacity onPress={() => changeTF('6m')}>
                <Text style={range === '6m' ? styles.active : styles.day}>
                  6M
                </Text>
              </TouchableOpacity>

              <TouchableOpacity onPress={() => changeTF('1y')}>
                <Text style={range === '1y' ? styles.active : styles.day}>
                  1Y
                </Text>
              </TouchableOpacity>

              <TouchableOpacity onPress={() => changeTF('5y')}>
                <Text style={range === '5y' ? styles.active : styles.day}>
                  5Y
                </Text>
              </TouchableOpacity>

              <TouchableOpacity onPress={() => changeTF('max')}>
                <Text style={range === 'max' ? styles.active : styles.day}>
                  MAX
                </Text>
              </TouchableOpacity>
            </View>
          </>
        )}
      </View>
    </>
  );
};

export default Chart;
