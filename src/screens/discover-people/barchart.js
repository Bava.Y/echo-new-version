import React from 'react';
import {Dimensions, Text, View} from 'react-native';
import {
  VictoryAxis,
  VictoryBar,
  VictoryChart,
  VictoryContainer,
  VictoryGroup,
  VictoryLegend,
} from 'victory-native';
import styles from './style';

const Charts = () => {
  const LIGHT_GREY = 'hsl(355, 20%, 90%)';

  const deviceWidth = Dimensions.get('window').width;

  return (
    <View style={{paddingVertical: 8}}>
      <Text style={styles.Companyname}>My Portfolio</Text>

      <View style={styles.sharerate}>
        <Text style={styles.shareamt}>$ 5666.00</Text>
        <Text style={styles.sharepercentage}>+ 12.09 (0.34%)</Text>
      </View>

      <VictoryChart
        width={deviceWidth * 0.875}
        height={280}
        padding={{top: 60, bottom: 20, left: 0, right: 40}}
        containerComponent={<VictoryContainer responsive={true} />}>
        <VictoryAxis
          style={{
            axis: {
              stroke: 'grey',
            },
            axisLabel: {
              fill: LIGHT_GREY,
              fontSize: 10,
              fontStyle: 'italic',
            },
            tickLabels: {
              fill: LIGHT_GREY,
              fontSize: 10,
            },
          }}
        />

        <VictoryAxis
          orientation="right"
          dependentAxis
          style={{
            axis: {
              stroke: 'grey',
            },
            tickLabels: {
              fill: LIGHT_GREY,
              fontSize: 10,
            },
            axisLabel: {
              fill: 'red',
              fontSize: 10,
              fontStyle: 'italic',
            },
            grid: {
              fill: LIGHT_GREY,
              stroke: LIGHT_GREY,
              strokeWidth: 0.1,
            },
          }}
        />

        <VictoryGroup
          offset={16}
          colorScale={['#08d0df', '#0f676d']}
          domainPadding={{x: [8, 8], y: 8}}>
          <VictoryBar
            data={[
              {x: 1, y: Math.random() * (1 - 0) + 0},
              {x: 2, y: Math.random() * (1 - 0) + 0},
              {x: 3, y: Math.random() * (1 - 0) + 0},
              {x: 4, y: Math.random() * (1 - 0) + 0},
              {x: 5, y: Math.random() * (1 - 0) + 0},
            ]}
          />
          <VictoryBar
            data={[
              {x: 1, y: Math.random() * (1 - 0) + 0},
              {x: 2, y: Math.random() * (1 - 0) + 0},
              {x: 3, y: Math.random() * (1 - 0) + 0},
              {x: 4, y: Math.random() * (1 - 0) + 0},
              {x: 5, y: Math.random() * (1 - 0) + 0},
            ]}
          />
        </VictoryGroup>

        <VictoryLegend
          x={deviceWidth * 0.175}
          y={16}
          orientation="horizontal"
          gutter={16}
          data={[
            {
              name: 'Revenue',
              symbol: {fill: '#08d0df'},
              labels: {fill: 'white'},
            },
            {
              name: 'Gross Profit',
              symbol: {fill: '#0f676d'},
              labels: {fill: 'white'},
            },
          ]}
        />
      </VictoryChart>
    </View>
  );
};

export default Charts;
