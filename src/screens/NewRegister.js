import AsyncStorage from '@react-native-community/async-storage';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import moment from 'moment';
import React, {useContext, useEffect, useState, useRef} from 'react';
import {
  ActivityIndicator,
  Image,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
  SafeAreaView,
  Linking,
  Alert,
} from 'react-native';
import DatePicker from 'react-native-datepicker';
import DeviceInfo from 'react-native-device-info';
import DocumentPicker from 'react-native-document-picker';
import SelectDropdown from 'react-native-select-dropdown';
import Snackbar from 'react-native-snackbar';
import Icon from 'react-native-vector-icons/FontAwesome';
import FontAwesomeIcons from 'react-native-vector-icons/FontAwesome5';
import {auth, profile} from '../api/index';
import BottomNav from '../components/BottomNav';
import FloatingTitleSelect from '../components/FloatingTitleSelect';
import {FloatingTitleTextInputField} from '../components/FloatingTitleTextInputField';
import HeaderNav from '../components/HeaderNav';
import RadioButton from '../components/RadioButton';
import {UserDataContext} from '../context/UserDataContext';
import style from '../styles/newregisterstyle';
import {LOGIN_USER, TOKEN} from '../utils/constants';
import theme from '../utils/theme';
import MyTabBar from './../components/RegisterTab';
import CheckBox from '@react-native-community/checkbox';
import WebView from 'react-native-webview';
import Modal from 'react-native-modal';
import _ from 'lodash';
import Loading from '../components/Loading';
import {set} from 'react-native-reanimated';
import {TextInput} from 'react-native-gesture-handler';
import Ionicons from 'react-native-vector-icons/Ionicons';
import DateTimePicker from '@react-native-community/datetimepicker';

const resources = {
  url: 'https://files.alpaca.markets/disclosures/library/UseAndRisk.pdf',
};
const NewRegisterScreen = ({navigation}) => {
  // NewRegisterScreen Variables
  const Tab = createMaterialTopTabNavigator();
  const [requestData, setRequestData] = useState({});
  const [formStatus, setFormStatus] = useState({
    personalFormStatus: false,
    disclosuresFormStatus: false,
    agreementsFormStatus: false,
    documentsFormStatus: false,
  });
  const [uploadingtLoadingStatuss, setUploadingtLoadingStatuss] =
    useState(false);

  const UseAndRisk = () => {
    Linking.openURL(
      'https://files.alpaca.markets/disclosures/library/UseAndRisk.pdf',
    );
    // <WebView source={{ uri: 'https://files.alpaca.markets/disclosures/Form+CRS.pdf' }} />;
  };
  const TermsAndConditions = () => {
    Linking.openURL(
      'https://files.alpaca.markets/disclosures/library/TermsAndConditions.pdf',
    );
    // <WebView source={{ uri: 'https://files.alpaca.markets/disclosures/Form+CRS.pdf' }} />;
  };
  const PrivacyNotice = () => {
    Linking.openURL(
      'https://files.alpaca.markets/disclosures/library/PrivacyNotice.pdf',
    );
    // <WebView source={{ uri: 'https://files.alpaca.markets/disclosures/Form+CRS.pdf' }} />;
  };
  const PFOF = () => {
    Linking.openURL(
      'https://files.alpaca.markets/disclosures/library/PFOF.pdf',
    );
    // <WebView source={{ uri: 'https://files.alpaca.markets/disclosures/Form+CRS.pdf' }} />;
  };
  const MarginDiscStmt = () => {
    Linking.openURL(
      'https://files.alpaca.markets/disclosures/library/MarginDiscStmt.pdf',
    );
    // <WebView source={{ uri: 'https://files.alpaca.markets/disclosures/Form+CRS.pdf' }} />;
  };
  const ExtHrsRisk = () => {
    Linking.openURL(
      'https://files.alpaca.markets/disclosures/library/ExtHrsRisk.pdf',
    );
    // <WebView source={{ uri: 'https://files.alpaca.markets/disclosures/Form+CRS.pdf' }} />;
  };
  const BCPSummary = () => {
    Linking.openURL(
      'https://files.alpaca.markets/disclosures/library/BCPSummary.pdf',
    );
    // <WebView source={{ uri: 'https://files.alpaca.markets/disclosures/Form+CRS.pdf' }} />;
  };

  const OpForm = () => {
    Linking.openURL('https://files.alpaca.markets/disclosures/Form+CRS.pdf');
    // <WebView source={{ uri: 'https://files.alpaca.markets/disclosures/Form+CRS.pdf' }} />;
  };

  // Context Variables
  const {setAlpacaConfigStatus} = useContext(UserDataContext);

  // Other Variables
  const [userData, setUserData] = useState(null);
  const [selectData, setSelectdata] = useState({
    state: [],
    // taxIdType: [],
    listOfVisaType: [],
    countryList: [],
    //  fundingsource: [],
    employeementStatus: [],
  });
  const [uploadError, setUploadError] = useState(null);
  const noYesOptions = [
    {id: '1', value: 'Yes'},
    {id: '0', value: 'No'},
  ];

  useEffect(() => {
    getSelectValues();
    getUserDetails();
    //getCountry();
  }, []);

  const getSelectValues = async () => {
    const stateData = await auth.getState();
    const filterState = stateData.filter(
      item => item.state_name !== 'Outside US',
    );
    let state = selectMap(filterState, 'state');

    const countryData = await auth.getCountryCodes();
    const filterCountry = countryData.filter(item => item.country_code);
    let countryList = selectMap(filterCountry, 'countryList');

    const taxIdTypeData = await auth.getTaxIdType();

    console.log('countryData', countryData);

    let employeementStatus = selectMap(taxIdTypeData.employment_status);

    let myArray = Array.from({length: 30}, (_, i) => i + 1);

    var exp = [];

    myArray.forEach(item => exp.push({label: item, value: item}));

    const setSelect = {
      state,
      //taxIdType,
      countryList,
      //fundingsource,
      employeementStatus,
    };

    setSelectdata(setSelect);
  };

  let selectMap = (selectData, fieldLabel) => {
    return selectData.map(data => {
      let selectArray = {
        value:
          Object.values(data)[fieldLabel === 'state' || 'countryList' ? 1 : 0],
        label: Object.values(data)[1],
      };

      return selectArray;
    });
  };

  const getUserDetails = async () => {
    let userInfo = JSON.parse(await AsyncStorage.getItem(LOGIN_USER));
    let token = await AsyncStorage.getItem(TOKEN);

    token = 'Bearer ' + token;

    let response = await profile.getUserData(userInfo.user_id, token);

    response && setUserData(response);
  };

  const PersonalScreen = () => {
    // Personal Variables
    const [firstname, setFirstname] = useState(null);
    const [familyname, setFamilyname] = useState(null);
    const [dob, setDob] = useState(null);
    const [datePicker, setDatePicker] = useState(false);
    const [email, setEmail] = useState(null);
    const [phoneno, setPhoneno] = useState(null);
    const [street, setStreet] = useState(null);
    const [city, setCity] = useState(null);
    const [stateSelected, setStateSelected] = useState(null);
    const [postalcode, setPostalcode] = useState(null);
    const [taxidtype, setTaxidtype] = useState(null);
    const [taxId, setTaxId] = useState(null);
    const [citizenShipYes, setcitizenShipYes] = useState(null);
    const [citizenShipNo, setcitizenShipNo] = useState(null);
    const [countryofcitizenUSA, setCountryCitizenUSA] = useState(null);
    const [countryofbirthUSA, setCountryBirthUSA] = useState(null);
    const [PermanentorVisa, setPermanentorVisa] = useState(null);
    const [PermanentResident, setPermanentResident] = useState(null);
    const [VisaHolder, setVisaHolder] = useState(null);
    const [VisaList, setVisaList] = useState(null);
    const [VisaExpireDate, setVisaExpireDate] = useState(null);
    const [VisaDeparture, setVisaDeparture] = useState(null);
    const [countryoftax, setCountrytax] = useState('USA');
    const [fundingList, setfundingList] = useState(null);
    const [annualincomemin, setAnnualmin] = useState(null);
    const [annualincomemax, setAnnualmax] = useState(null);
    const [liquidnetworthmin, setLiquidmin] = useState(null);
    const [liquidnetworthmax, setLiquidmax] = useState(null);
    const [totalnetworthmin, setTotalmin] = useState(null);
    const [totalnetworthmax, setTotalmax] = useState(null);
    const [extra, setExtra] = useState(null);
    const [annualincome, setAnnualincome] = useState(null);
    const [liquidnetworth, setLiquid] = useState(null);
    const [citizenship, setcitizenship] = useState(null);

    console.log('countryoftax>>>>', countryoftax);

    const initPersonalError = {
      firstname: false,
      familyname: false,
      dob: false,
      email: false,
      phoneno: false,
      street: false,
      city: false,
      state: false,
      countryList: false,
      taxId: false,
      citizenship: false,
      PermanentorVisa: false,
      countryofcitizenUSA: false,
      countryofbirthUSA: false,
      VisaDeparture: false,
      VisaExpireDate: false,
      countryofcitizenUSA: false,
      countryofbirthUSA: false,
      PermanentResident: false,
      VisaList: false,
      countryoftax: false,
      fundingList: false,
    };
    const [personalError, setPersonalError] = useState(initPersonalError);

    // Other Variables
    const [personalLoadingStatus, setPersonalLoadingStatus] = useState(true);

    useEffect(() => {
      loadStates();
    }, []);

    const loadStates = async () => {
      setFirstname(requestData.given_name || userData?.first_name || null);
      setFamilyname(requestData.family_name || userData?.last_name || null);
      setDob(requestData.date_of_birth || userData?.dob || null);
      setEmail(requestData.email || userData?.email || null);
      setPhoneno(requestData.phone_number || userData?.phone || null);
      setStreet(requestData.street_address || null);
      setCity(requestData.city || null);
      loadState(requestData.state || userData?.state_name || null);
      loadCountry(requestData.countryList || userData?.country_code || null);
      setPostalcode(requestData.postal_code || null);
      setTaxId(requestData.tax_id || null);
      setTaxidtype(requestData.tax_id_type || null);
      setcitizenship(requestData.citizenShipId || null);
      setcitizenShipYes(requestData.citizenShipYes || null);
      setcitizenShipNo(requestData.citizenShipNo || null);
      setVisaHolder(requestData.VisaHolder || null);
      setCountryCitizenUSA(requestData.country_of_citizenship);
      setCountryBirthUSA(requestData.country_of_birth);
      setPermanentResident(requestData.permanent_resident || null);
      setVisaList(requestData.visa_type || null);
      setVisaDeparture(requestData.date_of_departure_from_usa || null);
      setVisaExpireDate(requestData.visa_expiration_date || null);
      setCountrytax(requestData.country_of_tax_residence || null);
      setPermanentorVisa(requestData.PermanentorVisa || null);
      setfundingList(requestData.funding_source || null);
      setAnnualmin(requestData.annual_income_min || null);
      setAnnualmax(requestData.annual_income_max || null);
      setLiquidmin(requestData.liquid_net_worth_min || null);
      setLiquidmax(requestData.liquid_net_worth_max || null);
      setTotalmin(requestData.total_net_worth_min || null);
      setTotalmax(requestData.total_net_worth_max || null);
      setExtra(requestData.extra || null);
      setAnnualincome(requestData.annualincome || null);
      setLiquid(requestData.liquidnetworth || null);
      setPersonalLoadingStatus(false);
    };

    //Getting The States

    const loadState = value => {
      if (
        Boolean(selectData?.state) &&
        Array.isArray(selectData.state) &&
        selectData.state.length != 0
      ) {
        const filteredState = selectData.state.filter(
          lol => lol?.value == value,
        );

        setStateSelected(filteredState.length != 0 ? filteredState[0] : null);
      } else {
        setStateSelected(null);
      }
    };

    //Getting The CountryCode

    const loadCountry = value => {
      if (
        Boolean(selectData?.countryList) &&
        Array.isArray(selectData.countryList) &&
        selectData.countryList.length != 0
      ) {
        const filteredCountry = selectData.countryList.filter(
          lol => lol?.value == value,
        );

        setCountryCitizenUSA(
          filteredCountry.length != 0 ? filteredCountry[0] : null,
        );
      } else {
        setCountryCitizenUSA(null);
      }
    };

    const handlePresonalForm = () => {
      const checkPrimary =
        firstname &&
        familyname &&
        dob &&
        email &&
        phoneno &&
        street &&
        city &&
        !taxidtype &&
        !taxId &&
        countryoftax &&
        fundingList &&
        ((countryofcitizenUSA && countryofbirthUSA) ||
          (PermanentResident && countryofcitizenUSA && countryofbirthUSA) ||
          (VisaHolder &&
            countryofcitizenUSA &&
            countryofbirthUSA &&
            VisaList &&
            VisaDeparture &&
            VisaExpireDate)) &&
        liquidnetworth;

      const checkSecondary =
        firstname &&
        familyname &&
        dob &&
        email &&
        phoneno &&
        street &&
        city &&
        taxidtype &&
        taxId &&
        countryoftax &&
        fundingList &&
        ((countryofcitizenUSA && countryofbirthUSA) ||
          (PermanentResident && countryofcitizenUSA && countryofbirthUSA) ||
          (VisaHolder &&
            countryofcitizenUSA &&
            countryofbirthUSA &&
            VisaList &&
            VisaDeparture &&
            VisaExpireDate));

      if (checkPrimary || checkSecondary) {
        const personalRequestData = {
          ...requestData,
          given_name: firstname,
          family_name: familyname,
          date_of_birth: dob,
          email: email,
          phone_number: phoneno,
          street_address: street,
          city: city,
          state: stateSelected?.value ?? null,
          postal_code: postalcode,
          tax_id_type: taxidtype,
          tax_id: taxId,
          citizenShipId: citizenship,
          citizenShipYes: citizenShipYes,
          citizenShipNo: citizenShipNo,
          country_of_citizenship:
            countryofcitizenUSA === 'USA'
              ? countryofcitizenUSA
              : countryofcitizenUSA?.value ?? null,
          country_of_birth:
            countryofbirthUSA === 'USA'
              ? countryofbirthUSA
              : countryofbirthUSA?.value ?? null,
          permanent_resident: PermanentResident,
          PermanentorVisa: PermanentorVisa,
          visa_type: VisaList,
          VisaHolder: VisaHolder,
          funding_source: fundingList,
          visa_expiration_date: VisaExpireDate,
          date_of_departure_from_usa: VisaDeparture,
          country_of_tax_residence: countryoftax?.value ?? null,
          liquidnetworth: liquidnetworth,
          annualincome: annualincome,
          annual_income_min: annualincomemin,
          annual_income_max: annualincomemax,
          liquid_net_worth_min: liquidnetworthmin,
          liquid_net_worth_max: liquidnetworthmax,
          total_net_worth_min: totalnetworthmin,
          total_net_worth_max: totalnetworthmax,
          extra: extra,
        };

        setRequestData(personalRequestData);
        setFormStatus({...formStatus, personalFormStatus: true});

        navigation.navigate('Affilliations');
      } else {
        handleErrorValidation();
      }
    };

    const handleErrorValidation = () => {
      setPersonalError({
        ...personalError,
        firstname: firstname ? false : true,
        familyname: familyname ? false : true,
        dob: dob ? false : true,
        email: email ? false : true,
        phoneno: phoneno ? false : true,
        street: street ? false : true,
        city: city ? false : true,
        state: stateSelected?.value ? false : true,
        countryList: countryofcitizenUSA?.value ? false : true,
        taxId: taxidtype && !taxId ? true : false,
        countryoftax: countryoftax ? false : true,
        citizenship: citizenship ? false : true,
        countryofcitizenUSA: countryofcitizenUSA ? false : true,
        countryofbirthUSA: countryofbirthUSA ? false : true,
        PermanentResident: PermanentResident ? false : true,
        PermanentorVisa: PermanentorVisa ? false : true,
        VisaList: VisaList ? false : true,
        fundingList: fundingList ? false : true,
        VisaDeparture: VisaDeparture ? false : true,
        VisaExpireDate: VisaExpireDate ? false : true,
        liquidnetworth: liquidnetworth ? false : true,
      });
    };

    const fundingsourceList = [
      'employment_income',
      'investments',
      'inheritance',
      'business_income',
      'savings',
      'family',
    ];
    const income = [
      '$0-$20000',
      '$20000-$49999',
      '$50000-$99999',
      '$100000-$499999',
      '$500000-$999999',
      '$500000-$999999',
    ];
    const noTypes = ['Permanent Resident', 'Visa Holder'];
    const TaxIdTypes = ['USA_SSN'];
    const visatypes = [
      'B1',
      'B2',
      'DACA',
      'E1',
      'E2',
      'E3',
      'F1',
      'G4',
      'H1B',
      'J1',
      'L1',
      'OTHER',
      'O1',
      'TN1',
    ];

    function onDateSelected(event, value) {
      setDatePicker(false);
      setDob(moment(value).format('YYYY-MM-DD'));
    }

    return !personalLoadingStatus ? (
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : null}
        keyboardVerticalOffset={Platform.OS === 'ios' ? 40 : 0}
        style={style.container}>
        <ScrollView
          keyboardShouldPersistTaps="handled"
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}>
          <FloatingTitleTextInputField
            ispassword={false}
            attrName="first"
            title={
              <Text>
                First Name<Text style={{color: 'red'}}> *</Text>
              </Text>
            }
            value={firstname}
            updateMasterState={txt => {
              setPersonalError({...personalError, firstname: false});

              setFirstname(txt);
            }}
          />

          {personalError.firstname && (
            <Text style={style.errormsg}>Enter First name</Text>
          )}

          <FloatingTitleTextInputField
            ispassword={false}
            attrName="familyname"
            title={
              <Text>
                Last Name<Text style={{color: 'red'}}> *</Text>
              </Text>
            }
            value={familyname}
            updateMasterState={txt => {
              setPersonalError({...personalError, familyname: false});

              setFamilyname(txt);
            }}
          />

          {personalError.familyname && (
            <Text style={style.errormsg}>Enter Last name</Text>
          )}

          <View style={{paddingVertical: 8}}>
            <Text
              style={{
                fontSize: Platform.OS == 'ios' ? 14 : 16,
                color: '#ffffffc9',
                paddingHorizontal: 4,
              }}>
              Date Of Birth<Text style={{color: 'red'}}> *</Text>
            </Text>
            <TouchableOpacity
              onPress={() => {
                setDatePicker(true);
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  borderBottomWidth: 1,
                  borderBottomColor: 'white',
                }}>
                <Ionicons
                  name="calendar-outline"
                  size={18}
                  color={theme.white}
                />
                <Text
                  style={{color: 'white', textAlign: 'center', marginLeft: 5}}>
                  {Boolean(dob) ? dob : 'MM-DD-YYYY'}
                </Text>
              </View>
            </TouchableOpacity>

            {datePicker && (
              <DateTimePicker
                value={new Date()}
                display={Platform.OS === 'android' ? 'default' : 'spinner'}
                onChange={onDateSelected}
              />
            )}
            {/* <DatePicker
              date={dob}
              mode="date"
              placeholder="Date Of Birth"
              format="YYYY-MM-DD"
              confirmBtnText="Confirm"
              cancelBtnText="Cancel"
              style={style.datePickerStyle}
              customStyles={{
                dateIcon: {
                  position: 'absolute',
                  width: 24,
                  height: 24,
                  marginLeft: 0,
                  left: 0,
                },
                dateText: {
                  color: 'white',
                  textAlign: 'center',
                },
                dateInput: {
                  alignItems: 'flex-start',
                  borderWidth: 0,
                  borderBottomWidth: 1,
                  paddingLeft: 28,
                },
                datePicker: {
                  justifyContent: 'center',
                },
              }}
              onDateChange={date => {
                setPersonalError({...personalError, dob: false});

                setDob(date);
              }}
            /> */}
          </View>

          {personalError.dob && (
            <Text style={[style.errormsg, {marginTop: 8}]}>
              Select date of birth
            </Text>
          )}

          <FloatingTitleTextInputField
            keyboardType={'email-address'}
            ispassword={false}
            attrName="email"
            title={
              <Text>
                Email<Text style={{color: 'red'}}> *</Text>
              </Text>
            }
            value={email}
            updateMasterState={txt => {
              setPersonalError({...personalError, email: false});

              setEmail(txt);
            }}
          />

          {personalError.email && (
            <Text style={style.errormsg}>Enter email</Text>
          )}

          <FloatingTitleTextInputField
            keyboardType="number-pad"
            ispassword={false}
            attrName="phoneno"
            title={
              <Text>
                Phone No<Text style={{color: 'red'}}> *</Text>
              </Text>
            }
            value={phoneno}
            updateMasterState={txt => {
              setPersonalError({...personalError, phoneno: false});

              setPhoneno(txt);
            }}
          />

          {personalError.phoneno && (
            <Text style={style.errormsg}>Enter phone no</Text>
          )}

          <FloatingTitleTextInputField
            ispassword={false}
            attrName="street"
            title={
              <Text>
                Street address<Text style={{color: 'red'}}> *</Text>
              </Text>
            }
            value={street}
            updateMasterState={txt => {
              setPersonalError({...personalError, street: false});

              setStreet(txt);
            }}
          />

          {personalError.street && (
            <Text style={style.errormsg}>Enter street address</Text>
          )}

          <FloatingTitleTextInputField
            ispassword={false}
            attrName="city"
            title={
              <Text>
                City<Text style={{color: 'red'}}> *</Text>
              </Text>
            }
            value={city}
            updateMasterState={txt => {
              setPersonalError({...personalError, city: false});

              setCity(txt);
            }}
          />

          {personalError.city && <Text style={style.errormsg}>Enter city</Text>}

          <FloatingTitleSelect
            options={selectData?.state ?? []}
            displayLabel="Select State"
            title={
              <Text>
                State<Text style={{color: 'red'}}> *</Text>
              </Text>
            }
            selectedValue={stateSelected}
            onValueChange={itemValue => {
              setPersonalError({...personalError, state: false});

              setStateSelected(itemValue);
            }}
          />

          {personalError.state && (
            <Text style={style.errormsg}>Enter state</Text>
          )}

          <FloatingTitleTextInputField
            keyboardType="number-pad"
            ispassword={false}
            attrName="postalcode"
            title={<Text>Postal code</Text>}
            value={postalcode}
            updateMasterState={txt => {
              setPostalcode(txt);
            }}
          />

          {/* <FloatingTitleSelect
            options={selectData?.taxIdType ?? []}
            displayLabel="Tax id type"
            title="Tax id type"
            selectedValue={taxidtype}
            onValueChange={(itemValue) => {
              setTaxidtype(itemValue);
            }}
          /> */}

          <View style={{flexDirection: 'row'}}>
            <View style={{marginLeft: 5}}>
              <Text style={{fontFamily: 'Roboto', color: '#ffffffc9'}}>
                SSN<Text style={{color: 'red'}}> *</Text>
              </Text>
            </View>
          </View>

          <View style={{marginTop: -10}}>
            <SelectDropdown
              buttonStyle={style.listBroker}
              buttonTextStyle={style.listBrokerText}
              data={TaxIdTypes}
              dropdownStyle={{alignSelf: 'center'}}
              defaultButtonText="SSN"
              defaultValue={taxidtype}
              onSelect={selectedItem => {
                setTaxidtype(selectedItem);
              }}
              renderDropdownIcon={() => (
                <FontAwesomeIcons
                  name="caret-down"
                  size={16}
                  color={'white'}
                  style={{marginRight: 15}}
                />
              )}
              rowStyle={{borderBottomWidth: 0}}
              rowTextStyle={style.brokerItemText}
              selectedRowTextStyle={style.listBrokerSelectedItemText}
              updateMasterState={txt => {
                setTaxidtype(txt);
              }}
            />
          </View>
          {taxidtype && (
            <>
              <FloatingTitleTextInputField
                keyboardType="number-pad"
                ispassword={false}
                attrName="taxId"
                title={
                  <Text>
                    Tax id<Text style={{color: 'red'}}> *</Text>
                  </Text>
                }
                value={taxId}
                updateMasterState={txt => {
                  setTaxId(txt);
                  setPersonalError({...personalError, taxId: false});
                }}
              />

              {personalError.taxId && (
                <Text style={style.errormsg}>Enter tax id</Text>
              )}
            </>
          )}

          <View style={[style.spaceBetween, {marginTop: 5}]}>
            <View style={{flex: 0.75, justifyContent: 'center', marginLeft: 3}}>
              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 12 : 14,
                  color: '#ffffffc9',
                }}>
                Are you a citizen of the united states?{' '}
                <Text style={{color: 'red'}}> *</Text>
              </Text>
            </View>

            <View style={{flex: 0.35, justifyContent: 'center'}}>
              <RadioButton
                onChange={answer => {
                  setcitizenship(answer.id);
                  if (answer.id == 1) {
                    setcitizenShipYes(true);
                    setcitizenShipNo(false);
                    setPermanentResident(false);
                    setVisaHolder(false);
                    setCountryCitizenUSA('USA');
                    setCountryBirthUSA('USA');
                    setVisaList(null);
                    setVisaDeparture(null);
                    setVisaExpireDate(null);

                    //setyesorno(false)
                  } else if (answer.id == 0) {
                    setcitizenShipNo(true);
                    setcitizenShipYes(false);
                    //  setyesorno(false)
                  }
                }}
                options={noYesOptions}
                selected={citizenship}
                isOption={false}
              />
            </View>
          </View>
          <View>
            {personalError.citizenship && (
              <Text style={style.errormsg}>Select any yes & no</Text>
            )}
          </View>

          {citizenShipYes && (
            <View>
              <FloatingTitleTextInputField
                ispassword={false}
                attrName="Country of citrizeship"
                title={
                  <Text>
                    Country of citrizeship<Text style={{color: 'red'}}> *</Text>
                  </Text>
                }
                value={'USA'}
                updateMasterState={txt => {
                  setPersonalError({...personalError, street: false});

                  setCountryCitizenUSA(txt);
                }}
              />

              {personalError.countryofcitizenUSA && (
                <Text style={style.errormsg}>Enter country of citizenship</Text>
              )}

              <FloatingTitleTextInputField
                ispassword={false}
                attrName="country of birth USA"
                title={
                  <Text>
                    Country of Birth<Text style={{color: 'red'}}> *</Text>
                  </Text>
                }
                value={'USA'}
                updateMasterState={txt => {
                  setPersonalError({
                    ...personalError,
                    countryofbirthUSA: false,
                  });

                  setCountryBirthUSA(txt);
                }}
              />
              {personalError.countryofbirthUSA && (
                <Text style={style.errormsg}>Enter country of Birth</Text>
              )}
            </View>
          )}

          {/*citizenShipNoYes != null && !*/}
          {citizenShipNo && (
            <View>
              <View style={{flexDirection: 'row'}}>
                <View style={{marginLeft: 5}}>
                  <Text style={{color: 'white'}}>
                    Permanent resident or Visa Holder
                    <Text style={{color: 'red'}}> *</Text>
                  </Text>
                </View>
              </View>

              <SelectDropdown
                buttonStyle={style.listBroker}
                buttonTextStyle={style.listBrokerText}
                data={noTypes}
                dropdownStyle={{alignSelf: 'center'}}
                defaultButtonText="Select Permanent resident or Visa Holder"
                defaultValue={PermanentorVisa}
                onSelect={selectedItem => {
                  setPermanentorVisa(selectedItem);
                  if (selectedItem == 'Permanent Resident') {
                    setPermanentResident(true);
                    setCountryCitizenUSA(null);
                    setCountryBirthUSA(null);
                    setVisaHolder(false);
                    setVisaExpireDate(null);
                    setVisaDeparture(null);
                    setVisaList(null);
                  } else if (selectedItem == 'Visa Holder') {
                    setPermanentResident(false);
                    setVisaHolder(true);
                    setCountryCitizenUSA(null);
                    setCountryBirthUSA(null);
                  }
                }}
                renderDropdownIcon={() => (
                  <FontAwesomeIcons
                    name="caret-down"
                    size={16}
                    color={'white'}
                    style={{marginRight: 15}}
                  />
                )}
                rowStyle={{borderBottomWidth: 0}}
                rowTextStyle={style.brokerItemText}
                selectedRowTextStyle={style.listBrokerSelectedItemText}
                updateMasterState={txt => {
                  setPersonalError({...personalError, PermanentorVisa: false});

                  setPermanentorVisa(txt);
                }}
              />
              {personalError.PermanentorVisa && (
                <Text style={style.errormsg}>
                  Select Permanent Resident or Visa Holder
                </Text>
              )}
            </View>
          )}

          {PermanentResident && (
            <View>
              <FloatingTitleSelect
                options={selectData?.countryList ?? []}
                displayLabel={countryofcitizenUSA}
                title={
                  <Text>
                    Country Of Citizenship<Text style={{color: 'red'}}> *</Text>
                  </Text>
                }
                selectedValue={countryofcitizenUSA}
                onValueChange={itemValue => {
                  setPersonalError({
                    ...personalError,
                    countryofcitizenUSA: false,
                  });

                  setCountryCitizenUSA(itemValue);
                }}
              />

              {personalError.countryofcitizenUSA && (
                <Text style={style.errormsg}>
                  Select Country of Citizenship
                </Text>
              )}

              <FloatingTitleSelect
                options={selectData?.countryList ?? []}
                displayLabel={countryofbirthUSA}
                title={
                  <Text>
                    Country of Birth<Text style={{color: 'red'}}> *</Text>
                  </Text>
                }
                selectedValue={countryofbirthUSA}
                onValueChange={itemValue => {
                  setPersonalError({
                    ...personalError,
                    countryofbirthUSA: false,
                  });

                  setCountryBirthUSA(itemValue);
                }}
              />

              {personalError.countryofbirthUSA && (
                <Text style={style.errormsg}>Select Country of Birth</Text>
              )}
            </View>
          )}

          {VisaHolder && (
            <View>
              <View>
                <View style={{flexDirection: 'row'}}>
                  <View style={{marginLeft: 5}}>
                    <Text style={{color: 'white'}}>
                      Visa Type<Text style={{color: 'red'}}> *</Text>
                    </Text>
                  </View>
                </View>

                <SelectDropdown
                  buttonStyle={style.listBroker}
                  buttonTextStyle={style.listBrokerText}
                  data={visatypes}
                  dropdownStyle={{alignSelf: 'center'}}
                  defaultButtonText="Visa Type"
                  defaultValue={VisaList}
                  onSelect={selectedItem => {
                    setVisaList(selectedItem);
                  }}
                  renderDropdownIcon={() => (
                    <FontAwesomeIcons
                      name="caret-down"
                      size={16}
                      color={'white'}
                      style={{marginRight: 15}}
                    />
                  )}
                  rowStyle={{borderBottomWidth: 0}}
                  rowTextStyle={style.brokerItemText}
                  selectedRowTextStyle={style.listBrokerSelectedItemText}
                  updateMasterState={txt => {
                    setPersonalError({...personalError, VisaList: false});

                    setVisaList(txt);
                  }}
                />
                {personalError.VisaList && (
                  <Text style={style.errormsg}>Select Visa Type</Text>
                )}
              </View>

              <View>
                <View style={{paddingVertical: 8}}>
                  <Text
                    style={{
                      fontSize: Platform.OS == 'ios' ? 14 : 16,
                      color: '#ffffffc9',
                      paddingHorizontal: 4,
                    }}>
                    Visa Expiration Date <Text style={{color: 'red'}}> *</Text>
                  </Text>

                  <DatePicker
                    date={VisaExpireDate}
                    mode="date"
                    placeholder="Visa Expiration Date "
                    format="YYYY-MM-DD"
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    style={style.datePickerStyle}
                    customStyles={{
                      dateIcon: {
                        position: 'absolute',
                        width: 24,
                        height: 24,
                        marginLeft: 0,
                        left: 0,
                      },
                      dateText: {
                        color: 'white',
                        textAlign: 'center',
                      },
                      dateInput: {
                        alignItems: 'flex-start',
                        borderWidth: 0,
                        borderBottomWidth: 1,
                        paddingLeft: 28,
                      },
                      datePicker: {
                        justifyContent: 'center',
                      },
                    }}
                    onDateChange={date => {
                      setPersonalError({
                        ...personalError,
                        VisaExpireDate: false,
                      });

                      setVisaExpireDate(date);
                    }}
                  />

                  {personalError.VisaExpireDate && (
                    <Text style={style.errormsg}>
                      Select Visa Expiration Date
                    </Text>
                  )}
                </View>
                <FloatingTitleSelect
                  options={selectData?.countryList ?? []}
                  displayLabel={countryofcitizenUSA}
                  title={
                    <Text>
                      Country Of Citizenship
                      <Text style={{color: 'red'}}> *</Text>
                    </Text>
                  }
                  selectedValue={countryofcitizenUSA}
                  onValueChange={itemValue => {
                    setPersonalError({
                      ...personalError,
                      countryofcitizenUSA: false,
                    });

                    setCountryCitizenUSA(itemValue);
                  }}
                />

                {personalError.countryofcitizenUSA && (
                  <Text style={style.errormsg}>
                    Select Country of Citizenship
                  </Text>
                )}

                <FloatingTitleSelect
                  options={selectData?.countryList ?? []}
                  displayLabel={countryofbirthUSA}
                  title={
                    <Text>
                      Country of Birth<Text style={{color: 'red'}}> *</Text>
                    </Text>
                  }
                  selectedValue={countryofbirthUSA}
                  onValueChange={itemValue => {
                    setPersonalError({
                      ...personalError,
                      countryofbirthUSA: false,
                    });

                    setCountryBirthUSA(itemValue);
                  }}
                />

                {personalError.countryofbirthUSA && (
                  <Text style={style.errormsg}>Select Country of Birth</Text>
                )}

                <View style={{paddingVertical: 8}}>
                  <Text
                    style={{
                      fontSize: Platform.OS == 'ios' ? 14 : 16,
                      color: '#ffffffc9',
                      paddingHorizontal: 4,
                    }}>
                    Date of Departure from USA{' '}
                    <Text style={{color: 'red'}}> *</Text>
                  </Text>

                  <DatePicker
                    date={VisaDeparture}
                    mode="date"
                    placeholder="Date of Departure from USA"
                    format="YYYY-MM-DD"
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    style={style.datePickerStyle}
                    customStyles={{
                      dateIcon: {
                        position: 'absolute',
                        width: 24,
                        height: 24,
                        marginLeft: 0,
                        left: 0,
                      },
                      dateText: {
                        color: 'white',
                        textAlign: 'center',
                      },
                      dateInput: {
                        alignItems: 'flex-start',
                        borderWidth: 0,
                        borderBottomWidth: 1,
                        paddingLeft: 28,
                      },
                      datePicker: {
                        justifyContent: 'center',
                      },
                    }}
                    onDateChange={date => {
                      setPersonalError({
                        ...personalError,
                        VisaDeparture: false,
                      });

                      setVisaDeparture(date);
                    }}
                  />
                  {personalError.VisaDeparture && (
                    <Text style={style.errormsg}>
                      Select Date of Departure from USA
                    </Text>
                  )}
                </View>
              </View>
            </View>
          )}

          <FloatingTitleSelect
            options={selectData?.countryList ?? []}
            displayLabel="USA"
            title={
              <Text>
                Country of tax residency <Text style={{color: 'red'}}> *</Text>
              </Text>
            }
            selectedValue={countryoftax}
            onValueChange={itemValue => {
              setPersonalError({...personalError, countryoftax: false});

              setCountrytax(itemValue);
            }}
          />
          {personalError.countryoftax && (
            <Text style={style.errormsg}>Select Country of tax residency</Text>
          )}

          <View style={{flexDirection: 'row'}}>
            <View style={{marginLeft: 5}}>
              <Text style={{color: 'white'}}>
                Funding Source<Text style={{color: 'red'}}> *</Text>
              </Text>
            </View>
          </View>

          <SelectDropdown
            buttonStyle={style.listBroker}
            buttonTextStyle={style.listBrokerText}
            data={fundingsourceList}
            dropdownStyle={{alignSelf: 'center'}}
            defaultButtonText="Select funding source"
            defaultValue={fundingList}
            onSelect={selectedItem => {
              setfundingList(selectedItem);
            }}
            renderDropdownIcon={() => (
              <FontAwesomeIcons
                name="caret-down"
                size={16}
                color={'white'}
                style={{marginRight: 15}}
              />
            )}
            rowStyle={{borderBottomWidth: 0}}
            rowTextStyle={style.brokerItemText}
            selectedRowTextStyle={style.listBrokerSelectedItemText}
            updateMasterState={txt => {
              setPersonalError({...personalError, fundingList: false});

              setfundingList(txt);
            }}
          />
          {personalError.fundingsource && (
            <Text style={style.errormsg}>Select any one funding source</Text>
          )}

          <View style={{flexDirection: 'row'}}>
            <View style={{marginLeft: 5}}>
              <Text style={{fontFamily: 'Roboto', color: '#ffffffc9'}}>
                Annual Income
              </Text>
            </View>
          </View>

          <View style={{marginTop: -10}}>
            <SelectDropdown
              buttonStyle={style.listBroker}
              buttonTextStyle={style.listBrokerText}
              data={income}
              dropdownStyle={{alignSelf: 'center'}}
              defaultButtonText="Select Annual Income"
              defaultValue={annualincome}
              onSelect={selectedItem => {
                if (selectedItem) {
                  setAnnualincome(selectedItem);

                  const splitValue = selectedItem.split('-');
                  // const splitValue2 = selectedItem.split(',');

                  // console.log('><><Split><><', splitValue[0],splitValue[0].split('$')[1]);

                  setAnnualmin(splitValue[0].split('$')[1]);

                  setAnnualmax(splitValue[1].split('$')[1]);
                  //   const min = Spi[0].slice(1)
                  //   return(
                  //  setLiquidmin(min)
                  //   )
                  // console.log('||||||||||', liquidnetworthmin);
                } else {
                  setAnnualincome(null);
                }
              }}
              renderDropdownIcon={() => (
                <FontAwesomeIcons
                  name="caret-down"
                  size={16}
                  color={'white'}
                  style={{marginRight: 15}}
                />
              )}
              rowStyle={{borderBottomWidth: 0}}
              rowTextStyle={style.brokerItemText}
              selectedRowTextStyle={style.listBrokerSelectedItemText}
            />
          </View>

          <View style={{flexDirection: 'row'}}>
            <View style={{marginLeft: 5}}>
              <Text style={{fontFamily: 'Roboto', color: '#ffffffc9'}}>
                Liquid Assets<Text style={{color: 'red'}}> *</Text>
              </Text>
            </View>
          </View>
          <View style={{marginTop: -10}}>
            <SelectDropdown
              buttonStyle={style.listBroker}
              buttonTextStyle={style.listBrokerText}
              data={income}
              dropdownStyle={{alignSelf: 'center'}}
              defaultButtonText="Select Liquid Assests"
              defaultValue={liquidnetworth}
              onSelect={selectedItem => {
                if (selectedItem) {
                  setLiquid(selectedItem);

                  const splitValue = selectedItem.split('-');
                  // const splitValue2 = selectedItem.split(',');

                  setLiquidmin(splitValue[0].split('$')[1]);

                  setLiquidmax(splitValue[1].split('$')[1]);
                  //   const min = Spi[0].slice(1)
                  //   return(
                  //  setLiquidmin(min)
                  //   )
                } else {
                  setLiquid(null);
                }
              }}
              renderDropdownIcon={() => (
                <FontAwesomeIcons
                  name="caret-down"
                  size={16}
                  color={'white'}
                  style={{marginRight: 15}}
                />
              )}
              rowStyle={{borderBottomWidth: 0}}
              rowTextStyle={style.brokerItemText}
              selectedRowTextStyle={style.listBrokerSelectedItemText}
              updateMasterState={txt => {
                setPersonalError({...personalError, liquidnetworth: false});

                setLiquid(txt);
              }}
            />
            {personalError.liquidnetworth && (
              <Text style={style.errormsg}>Select Any One Liquid Assets</Text>
            )}
          </View>

          <FloatingTitleTextInputField
            keyboardType="number-pad"
            ispassword={false}
            attrName="totalnetworthmin"
            title={<Text>Total net worth min</Text>}
            value={totalnetworthmin}
            updateMasterState={txt => {
              setTotalmin(txt);
            }}
          />

          <FloatingTitleTextInputField
            keyboardType="number-pad"
            ispassword={false}
            attrName="totalnetworthmax"
            title={<Text>Total net worth max</Text>}
            value={totalnetworthmax}
            updateMasterState={txt => {
              setTotalmax(txt);
            }}
          />

          <FloatingTitleTextInputField
            ispassword={false}
            attrName="extra"
            title={<Text>Extra</Text>}
            value={extra}
            updateMasterState={txt => {
              setExtra(txt);
            }}
          />

          <TouchableOpacity
            style={style.signinBtn}
            onPress={() => {
              handlePresonalForm();
            }}>
            <Text style={style.signintext}>Save the Details</Text>
          </TouchableOpacity>

          <View style={{paddingVertical: 16}} />
        </ScrollView>
      </KeyboardAvoidingView>
    ) : (
      renderScreenLoading()
    );
  };

  const AffilliationsScreen = () => {
    // AffilliationsScreen Variables
    const [controlPerson, setControlPerson] = useState(null);
    const [affiliatedExchange, setAffiliatedExchange] = useState(null);
    const [politicallyExposed, setPoliticallyExposed] = useState(null);
    const [familyExposed, setFamilyExposed] = useState(null);
    const [empstatus, setEmpstatus] = useState(null);
    const [empname, setEmpname] = useState(null);
    const [empaddress, setEmpaddress] = useState(null);
    const [empposition, setEmpposition] = useState(null);
    const [accountApproval1, setAccountApproval1] = useState(null);
    const [accountApproval2, setAccountApproval2] = useState(null);
    const [EnableFinra, setEnableFinra] = useState(false);
    const [traded, setTraded] = useState(false);
    const [politicalFigure, setPolicalFigure] = useState(false);
    const [familyFigure, setFamilyFigure] = useState(false);
    const [none, setNone] = useState(false);

    //DOUCMENT UPLOAD FUNCTION STATE

    // DocumentsScreen Variables
    const [doctype, setDoctype] = useState(null);
    const [idVerficationType, setIdVerficationType] = useState(null);
    const [document, setDocument] = useState(null);
    const [uploadResponse, setUploadResponse] = useState(null);

    // Error Variables
    const initDocumentsError = {
      doctype: false,
      document: false,
    };
    const [documentError, setDocumentError] = useState(initDocumentsError);

    // Other Variables
    const [documentsLoadingStatus, setDocumentsLoadingStatus] = useState(true);
    const [uploadingtLoadingStatus, setUploadingLoadingStatus] =
      useState(false);
    const [documentLoadingStatus, setDocumentLoadingStatus] = useState(false);

    const documentsOptions = [
      {label: '407 approval letter', value: 'account_approval_letter'},
    ];

    const idOptions = ['Passport', 'Visa'];

    useEffect(() => {
      loadStates();
    }, []);

    const loadDocType = value => {
      if (
        Boolean(documentsOptions) &&
        Array.isArray(documentsOptions) &&
        documentsOptions.length != 0
      ) {
        const filteredDocType = documentsOptions.filter(
          lol => lol?.value == value,
        );

        setDoctype(filteredDocType.length != 0 ? filteredDocType[0] : null);
      } else {
        setDoctype(null);
      }
    };

    const handleRemoveDocuments = async () => {
      Snackbar.show({
        duration: Snackbar.LENGTH_INDEFINITE,
        text: 'Deleting...',
      });

      let response = await auth.removeAlpacaDocuments(uploadResponse.id);

      if (response && response.status) {
        Snackbar.show({
          duration: Snackbar.LENGTH_SHORT,
          text: response.message,
        });

        setDocument(null);

        setUploadResponse(null);
      } else {
        Snackbar.show({
          backgroundColor: 'red',
          duration: Snackbar.LENGTH_SHORT,
          text: response.message,
        });
      }
    };

    const letter = () => {
      if (accountApproval1 == true) {
        if (doctype?.value ?? null) {
          handleDocumentPicker(
            async onSuccess => {
              setUploadingLoadingStatus(true);

              let requestData = new FormData();

              requestData.append('document_type', doctype?.value ?? null);
              requestData.append('sub_type', idVerficationType);
              requestData.append('content', {
                uri: onSuccess[0].uri,
                type: onSuccess[0].type,
                name: onSuccess[0].name,
              });
              requestData.append('mime_type', onSuccess.type);

              let response = await auth.uploadAlpacaDocuments(requestData);

              if (response && response.status && response.response) {
                setDocument(onSuccess);

                setUploadResponse(response.response);

                setUploadingLoadingStatus(false);
              } else {
                setDocument(null);

                setUploadResponse(null);

                setUploadingLoadingStatus(false);
              }
            },
            onError => {
              setUploadError(onError);

              setDocument(null);

              setUploadResponse(null);

              setUploadingLoadingStatus(false);
            },
            () => {
              setUploadError(null);

              setDocument(null);

              setUploadResponse(null);

              setUploadingLoadingStatus(false);
            },
          );
        } else {
          setDocumentError({
            ...documentError,
            doctype: doctype?.value ? false : true,
          });
        }
      }
    };

    // Error Variables
    const initDisclosureError = {
      EnableFinra: false,
      traded: false,
      politicalFigure: false,
      familyFigure: false,
      empstatus: false,
    };
    const [disclosureError, setDisclosureError] = useState(initDisclosureError);

    // Other Variables
    const [disclosureLoadingStatus, setDisclosureLoadingStatus] =
      useState(true);

    useEffect(() => {
      loadStates();
    }, []);

    const loadStates = async () => {
      setTraded(
        handleNoYesOptionValues(requestData.is_control_person) || false,
      );
      setEnableFinra(
        handleNoYesOptionValues(requestData.is_affiliated_exchange_or_finra) ||
          false,
      );
      setPolicalFigure(
        handleNoYesOptionValues(requestData.is_politically_exposed) || false,
      );
      setFamilyFigure(
        handleNoYesOptionValues(requestData.immediate_family_exposed) || false,
      );
      setNone(handleNoYesOptionValues(requestData.none) || false);

      loadDocType(requestData?.document_type ?? null);
      setIdVerficationType(requestData.sub_type || null);
      setDocument(requestData.document || null);
      setDocumentsLoadingStatus(false);
      setAccountApproval1(requestData.accountApproval1 || false);
      loadEmpStatus(
        requestData.employment_status ||
          (userData?.employedname && userData.employedname.toLowerCase()) ||
          null,
      );
      setEmpname(requestData.employer_name || userData?.company_name || null);
      setEmpaddress(requestData.employer_address || null);
      setEmpposition(
        requestData.employment_position || userData?.designation || null,
      );
      setDisclosureLoadingStatus(false);
    };

    const loadEmpStatus = value => {
      if (
        Boolean(selectData?.employeementStatus) &&
        Array.isArray(selectData.employeementStatus) &&
        selectData.employeementStatus.length != 0
      ) {
        const filteredEmpStatus = selectData.employeementStatus.filter(
          lol => lol?.value == value,
        );

        setEmpstatus(
          filteredEmpStatus.length != 0 ? filteredEmpStatus[0] : null,
        );
      } else {
        setEmpstatus(null);
      }
    };

    const handleDisclosureForm = () => {
      if (formStatus.personalFormStatus) {
        if (
          (EnableFinra || traded || politicalFigure || familyFigure || none) &&
          empstatus
        ) {
          const disclosureRequestData = {
            ...requestData,
            is_affiliated_exchange_or_finra: EnableFinra,
            document_type: doctype?.value ?? null,
            sub_type: idVerficationType,
            document: document,
            is_control_person: traded,
            is_politically_exposed: politicalFigure,
            immediate_family_exposed: familyFigure,
            employment_status: empstatus?.value ?? null,
            employer_name: empname,
            employer_address: empaddress,
            employment_position: empposition,
            none: none,
            accountApproval1: accountApproval1,
          };

          setRequestData(disclosureRequestData);
          setFormStatus({...formStatus, disclosuresFormStatus: true});

          navigation.navigate('Agreements');
        } else {
          handleErrorValidation();
        }
      } else {
        Snackbar.show({
          duration: Snackbar.LENGTH_LONG,
          text: 'Please! Fill the personal form',
        });
      }
    };

    const handleNoYesOptionValues = value => {
      switch (value) {
        case false:
          return false;

        case true:
          return true;
      }
    };

    const handleErrorValidation = () => {
      setDisclosureError({
        ...disclosureError,
        EnableFinra: EnableFinra ? false : true,
        traded: traded ? false : true,
        politicalFigure: politicalFigure ? false : true,
        familyFigure: familyFigure ? false : true,
        empstatus: empstatus ? false : true,
      });

      setDocumentError({
        ...documentError,
        doctype: doctype?.value ?? null ? false : true,
        document: document ? false : true,
      });
      // }
    };

    return !disclosureLoadingStatus ? (
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : null}
        keyboardVerticalOffset={Platform.OS === 'ios' ? 40 : 0}
        style={style.container}>
        <ScrollView
          keyboardShouldPersistTaps="handled"
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}>
          <View style={style.spaceBetween}>
            <View style={{flex: 1}}>
              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 12 : 14,
                  color: '#ffffffc9',
                }}>
                Affiliated or work with a US registered broker-dealer or FINRA?
                <Text style={{color: 'red'}}> *</Text>
              </Text>
            </View>

            <View>
              <CheckBox
                boxType={'square'}
                value={EnableFinra}
                onValueChange={() => {
                  setEnableFinra(!EnableFinra);
                  setAccountApproval1(!accountApproval1);
                  if (none === true) {
                    setEnableFinra(false);
                    setAccountApproval1(false);
                  }
                }}
                tintColors={{true: theme.secondryColor, false: 'white'}}
                style={[
                  {color: '#fff'},
                  Platform.OS == 'ios' && {
                    transform: [{scaleX: 0.75}, {scaleY: 0.75}],
                  },
                ]}
              />
            </View>
          </View>

          {accountApproval1 && (
            <View
              style={{paddingLeft: 2, marginTop: 3, flexDirection: 'column'}}>
              <View style={{flex: 0.9}}>
                <Text
                  style={{
                    color: theme.secondryColor,
                    fontWeight: 'bold',
                    fontSize: Platform.OS == 'ios' ? 14 : 16,
                  }}>
                  Account Approval Letter
                </Text>
              </View>
              <TouchableOpacity style={style.dropdownIconContainer}>
                <View style={style.spaceBetween}>
                  <View style={{flex: 0.5, justifyContent: 'center'}}>
                    <Text
                      style={{
                        fontSize: Platform.OS == 'ios' ? 12 : 14,
                        color: '#ffffffc9',
                      }}>
                      Document Type<Text style={{color: 'red'}}> *</Text>
                    </Text>
                  </View>

                  <View style={{flex: 0.6, justifyContent: 'center'}}>
                    <SelectDropdown
                      buttonStyle={style.dropdownWrapper}
                      data={documentsOptions}
                      defaultButtonText="Document Type"
                      dropdownStyle={{
                        width: '80%',
                        marginLeft: 8,
                        alignSelf: 'center',
                      }}
                      onSelect={selectedItem => {
                        setDocumentError({...documentError, doctype: false});

                        setDoctype(selectedItem);
                      }}
                      renderCustomizedButtonChild={selectedItem => {
                        return (
                          <View style={style.dropdownItemContainer}>
                            <View style={style.dropdownLabelContainer}>
                              <Text style={style.dropdownLabelText}>
                                {selectedItem?.label ?? 'Document Type'}
                              </Text>
                            </View>
                            <View style={style.dropdownIconContainer}>
                              <FontAwesomeIcons
                                name="caret-down"
                                size={16}
                                color={'white'}
                              />
                            </View>
                          </View>
                        );
                      }}
                      rowStyle={{borderBottomWidth: 0}}
                      rowTextForSelection={item => item?.label}
                      rowTextStyle={style.dropdownItemText}
                      selectedRowTextStyle={style.dropdownSelectedItemText}
                      defaultValue={doctype}
                    />
                  </View>
                </View>

                {documentError.doctype && (
                  <Text
                    style={[
                      style.errormsg,
                      {marginHorizontal: 0, marginVertical: 8},
                    ]}>
                    Select any one document type
                  </Text>
                )}

                {doctype?.value == 'identity_verification' && (
                  <View style={[style.spaceBetween, {marginTop: 8}]}>
                    <View style={{flex: 0.4, justifyContent: 'center'}}>
                      <Text
                        style={{
                          fontSize: Platform.OS == 'ios' ? 12 : 14,
                          color: '#ffffffc9',
                        }}>
                        Id verification type
                      </Text>
                    </View>

                    <View style={{flex: 0.6, justifyContent: 'center'}}>
                      <SelectDropdown
                        buttonStyle={style.dropdownWrapper}
                        data={idOptions}
                        defaultButtonText="ID Type"
                        dropdownStyle={{
                          width: '80%',
                          marginLeft: 8,
                          alignSelf: 'center',
                        }}
                        onSelect={selectedItem => {
                          setIdVerficationType(selectedItem);
                        }}
                        renderCustomizedButtonChild={selectedItem => {
                          return (
                            <View style={style.dropdownItemContainer}>
                              <View style={style.dropdownLabelContainer}>
                                <Text style={style.dropdownLabelText}>
                                  {selectedItem ?? 'ID Type'}
                                </Text>
                              </View>
                              <View style={style.dropdownIconContainer}>
                                <FontAwesomeIcons
                                  name="caret-down"
                                  size={16}
                                  color={'white'}
                                />
                              </View>
                            </View>
                          );
                        }}
                        rowStyle={{borderBottomWidth: 0}}
                        rowTextForSelection={item => item}
                        rowTextStyle={style.dropdownItemText}
                        selectedRowTextStyle={style.dropdownSelectedItemText}
                        defaultValue={idVerficationType}
                      />
                    </View>
                  </View>
                )}

                <View style={[style.spaceBetween, {marginTop: 8}]}>
                  <View style={{flex: 0.4, justifyContent: 'center'}}>
                    <Text
                      style={{
                        fontSize: Platform.OS == 'ios' ? 12 : 14,
                        color: '#ffffffc9',
                      }}>
                      Document<Text style={{color: 'red'}}> *</Text>
                    </Text>
                  </View>

                  <View style={{flex: 0.6, justifyContent: 'center'}}>
                    {document ? (
                      <View
                        style={{
                          width: 50,
                          height: 50,
                          justifyContent: 'center',
                          alignItems: 'center',
                          borderColor: 'white',
                          borderWidth: 1,
                          borderRadius: 4,
                        }}>
                        <Image
                          onLoadStart={() => {
                            setDocumentLoadingStatus(true);
                          }}
                          onLoadEnd={() => {
                            setDocumentLoadingStatus(false);
                          }}
                          source={{
                            uri:
                              document.type === 'application/pdf'
                                ? 'https://play-lh.googleusercontent.com/nufRXPpDI9XP8mPdAvOoJULuBIH_OK4YbZZVu8i_-eDPulZpgb-Xp-EmI8Z53AlXHpqX'
                                : document.uri,
                          }}
                          style={{width: 48, height: 48, borderRadius: 4}}
                        />

                        <TouchableOpacity
                          style={{
                            position: 'absolute',
                            backgroundColor: theme.secondryColor,
                            borderTopRightRadius: 4,
                            paddingHorizontal: 2,
                            paddingVertical: 2,
                            top: 0,
                            right: 0,
                          }}
                          onPress={() => {
                            handleRemoveDocuments();
                          }}>
                          <Icon name={'times'} size={14} color={theme.white} />
                        </TouchableOpacity>

                        {documentLoadingStatus && renderActivityIndicator()}
                      </View>
                    ) : (
                      <TouchableOpacity
                        style={style.uploadbtn}
                        onPress={() => {
                          setDocumentError({...documentError, document: false});

                          letter();
                        }}>
                        {uploadingtLoadingStatus ? (
                          <ActivityIndicator size="small" color={theme.white} />
                        ) : (
                          <>
                            <Icon name="upload" color={'white'} size={16} />
                            <Text style={style.uploadtxt}>Upload</Text>
                          </>
                        )}
                      </TouchableOpacity>
                    )}
                  </View>
                </View>

                {documentError.document && (
                  <Text
                    style={[
                      style.errormsg,
                      {marginHorizontal: 0, marginVertical: 8},
                    ]}>
                    Upload document
                  </Text>
                )}

                {uploadError && (
                  <Text
                    style={[
                      style.errormsg,
                      {marginHorizontal: 0, marginVertical: 8},
                    ]}>
                    {uploadError == 'sizeError'
                      ? 'File size must be less than 10 MB'
                      : 'Supported file formats are jpeg/jpg/pdf'}
                  </Text>
                )}

                <View style={{paddingVertical: 16}} />
              </TouchableOpacity>
              <View></View>
            </View>
          )}

          {disclosureError.EnableFinra && (
            <Text
              style={[
                style.errormsg,
                {marginHorizontal: 0, marginVertical: 8},
              ]}>
              Select any one option
            </Text>
          )}

          <View style={style.spaceBetween}>
            <View style={{flex: 1}}>
              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 12 : 14,
                  color: '#ffffffc9',
                }}>
                Senior executive at or a 10% or greater shareholder of a
                publicly traded company?
                <Text style={{color: 'red'}}> *</Text>
              </Text>
            </View>

            <View>
              <CheckBox
                boxType={'square'}
                value={traded}
                onValueChange={() => {
                  setTraded(!traded);
                  // setAccountApproval1(!accountApproval1)
                  if (none === true) {
                    setTraded(false);
                    // setAccountApproval2(false)
                  }
                }}
                tintColors={{true: theme.secondryColor, false: 'white'}}
                style={[
                  {color: '#fff'},
                  Platform.OS == 'ios' && {
                    transform: [{scaleX: 0.75}, {scaleY: 0.75}],
                  },
                ]}
              />
            </View>

            {/* <View style={{flex: 0.35, justifyContent: 'center'}}>
              <RadioButton
                onChange={(answer) => {
                  setDisclosureError({
                    ...disclosureError,
                    affiliatedExchange: false,
                  });

                  setAffiliatedExchange(answer.id);
                  if (answer.id == 1) {
                    setAccountApproval2(true);
                  } else if (answer.id == 0) {
                    setAccountApproval2(false);
                  }
                }}
                options={noYesOptions}
                selected={affiliatedExchange}
                isOption={false}
              />
            </View> */}
          </View>

          {accountApproval2 && (
            <View style={{paddingLeft: 2, marginTop: 3, flexDirection: 'row'}}>
              <View style={{flex: 0.9}}>
                <Text
                  style={{
                    color: theme.secondryColor,
                    fontWeight: 'bold',
                    fontSize: Platform.OS == 'ios' ? 14 : 16,
                  }}>
                  Account Approval Letter
                </Text>
              </View>
              {/* <TouchableOpacity style={style.dropdownIconContainer}>
                <FontAwesomeIcons name="upload" size={16} color={'white'} />
              </TouchableOpacity> */}

              {/* <View></View> */}

              {/*//UPLOAD CHECK*/}
            </View>
          )}

          {disclosureError.traded && (
            <Text
              style={[
                style.errormsg,
                {marginHorizontal: 0, marginVertical: 8},
              ]}>
              Select any one option
            </Text>
          )}

          <View style={style.spaceBetween}>
            <View style={{flex: 1}}>
              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 12 : 14,
                  color: '#ffffffc9',
                }}>
                I am a senior political figure?
                <Text style={{color: 'red'}}> *</Text>
              </Text>
            </View>

            {/* <View style={{flex: 0.35, justifyContent: 'center'}}>
              <RadioButton
                onChange={(answer) => {
                  setDisclosureError({
                    ...disclosureError,
                    politicallyExposed: false,
                  });

                  setPoliticallyExposed(answer.id);
                }}
                options={noYesOptions}
                selected={politicallyExposed}
                isOption={false}
              />
            </View> */}

            <View>
              <CheckBox
                boxType={'square'}
                value={politicalFigure}
                onValueChange={() => {
                  setPolicalFigure(!politicalFigure);
                  //setAccountApproval2(!accountApproval2)
                  if (none === true) {
                    setPolicalFigure(false);
                    // setAccountApproval2(false)
                  }
                }}
                tintColors={{true: theme.secondryColor, false: 'white'}}
                style={[
                  {color: '#fff'},
                  Platform.OS == 'ios' && {
                    transform: [{scaleX: 0.75}, {scaleY: 0.75}],
                  },
                ]}
              />
            </View>
          </View>

          {disclosureError.politicalFigure && (
            <Text
              style={[
                style.errormsg,
                {marginHorizontal: 0, marginVertical: 8},
              ]}>
              Select any one option
            </Text>
          )}

          <View style={style.spaceBetween}>
            <View style={{flex: 1}}>
              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 12 : 14,
                  color: '#ffffffc9',
                }}>
                I am a family member or relative of a senior political figure?
                <Text style={{color: 'red'}}> *</Text>
              </Text>
            </View>

            {/* <View style={{flex: 0.35, justifyContent: 'center'}}>
              <RadioButton
                onChange={(answer) => {
                  setDisclosureError({
                    ...disclosureError,
                    familyExposed: false,
                  });

                  setFamilyExposed(answer.id);
                }}
                options={noYesOptions}
                selected={familyExposed}
                isOption={false}
              />
            </View> */}
            <View>
              <CheckBox
                boxType={'square'}
                value={familyFigure}
                onValueChange={() => {
                  setFamilyFigure(!familyFigure);
                  // setAccountApproval1(!accountApproval1)
                  if (none === true) {
                    setFamilyFigure(false);
                    // setAccountApproval3(false)
                  }
                }}
                tintColors={{true: theme.secondryColor, false: 'white'}}
                style={[
                  {color: '#fff'},
                  Platform.OS == 'ios' && {
                    transform: [{scaleX: 0.75}, {scaleY: 0.75}],
                  },
                ]}
              />
            </View>
          </View>

          {disclosureError.familyFigure && (
            <Text
              style={[
                style.errormsg,
                {marginHorizontal: 0, marginVertical: 8},
              ]}>
              Select any one option
            </Text>
          )}

          <View style={style.spaceBetween}>
            <View style={{flex: 1}}>
              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 12 : 14,
                  color: '#ffffffc9',
                }}>
                None of the above apply to me or my family.
                <Text style={{color: 'red'}}> *</Text>
              </Text>
            </View>

            {/* <View style={{flex: 0.35, justifyContent: 'center'}}>
              <RadioButton
                onChange={(answer) => {
                  setDisclosureError({
                    ...disclosureError,
                    familyExposed: false,
                  });

                  setFamilyExposed(answer.id);
                }}
                options={noYesOptions}
                selected={familyExposed}
                isOption={false}
              />
            </View> */}
            <View>
              <CheckBox
                boxType={'square'}
                value={none}
                onValueChange={() => {
                  setNone(!none);
                  if (none == false) {
                    setEnableFinra(false);
                    setTraded(false);
                    setPolicalFigure(false);
                    setFamilyFigure(false);
                    setAccountApproval1(false);
                    setAccountApproval2(false);
                  }
                  // setAccountApproval1(!accountApproval1)
                }}
                tintColors={{true: theme.secondryColor, false: 'white'}}
                style={[
                  {color: '#fff'},
                  Platform.OS == 'ios' && {
                    transform: [{scaleX: 0.75}, {scaleY: 0.75}],
                  },
                ]}
              />
            </View>
          </View>

          {disclosureError.none && (
            <Text
              style={[
                style.errormsg,
                {marginHorizontal: 0, marginVertical: 8},
              ]}>
              Select any one option
            </Text>
          )}

          <FloatingTitleSelect
            options={selectData?.employeementStatus ?? []}
            displayLabel={<Text style={{fontSize: 14}}>Employment Status</Text>}
            title={
              <Text>
                Employment Status<Text style={{color: 'red'}}> *</Text>
              </Text>
            }
            selectedValue={empstatus}
            onValueChange={itemValue => {
              setDisclosureError({...disclosureError, empstatus: false});
              setEmpstatus(itemValue);
            }}
          />

          {disclosureError.empstatus && (
            <Text style={style.errormsg}>Select any Empolyment Statuse</Text>
          )}

          <FloatingTitleTextInputField
            ispassword={false}
            attrName="empname"
            title={<Text>Employer Name</Text>}
            value={empname}
            updateMasterState={txt => {
              setEmpname(txt);
            }}
          />

          <FloatingTitleTextInputField
            ispassword={false}
            attrName="empaddress"
            title={<Text>Employer Address</Text>}
            value={empaddress}
            updateMasterState={txt => {
              setEmpaddress(txt);
            }}
          />

          <FloatingTitleTextInputField
            ispassword={false}
            attrName="empposition"
            title={<Text>Employeement Position</Text>}
            value={empposition}
            updateMasterState={txt => {
              setEmpposition(txt);
            }}
          />

          <TouchableOpacity
            style={style.signinBtn}
            onPress={() => {
              handleDisclosureForm();
            }}>
            <Text style={style.signintext}>Save the Details</Text>
          </TouchableOpacity>

          <View style={{paddingVertical: 16}} />
        </ScrollView>
      </KeyboardAvoidingView>
    ) : (
      renderScreenLoading()
    );
  };

  const AgreementsScreen = () => {
    // AgreementsScreen Variables
    const [accountSignedAt, setAccountSignedAt] = useState(null);
    console.log('accountSignedAt<>>>><<<',accountSignedAt)
    const [accountSignedAtShow, setAccountSignedAtShow] = useState(false);
    const [accountIpAddress, setAccountIpAddress] = useState(null);
    const [customerSignedAt, setCustomerSignedAt] = useState(null);
    const [customerSignedAtShow, setCustomerSignedAtShow] = useState(false);
    const [customerIpAddress, setCustomerIpAddress] = useState(null);
    const [marginSignedAt, setMarginSignedAt] = useState(null);
    const [marginSignedAtShow, setMarginSignedAtShow] = useState(null);
    const [marginIpAddress, setMarginIpAddress] = useState(null);
    const [isSelected1, setSelection1] = useState(false);
    const [isSelected2, setSelection2] = useState(false);
    //const [selected, setSelected] = useState(true);

    const handleNoYesOptionValues = value => {
      switch (value) {
        case false:
          return false;

        case true:
          return true;
      }
    };

    function accountSignedAtTime(event, value) {
      setAccountSignedAtShow(false);
      setAccountSignedAt(moment(value).format());
    }

    function customerSignedAtTime(event, value) {
      setCustomerSignedAtShow(false);
      setCustomerSignedAt(moment(value).format());
    }

    function marginSignedAtTime(event, value) {
      setMarginSignedAtShow(false);
      setMarginSignedAt(moment(value).format());
    }
    // Error Variables
    const initAgreementsError = {
      accountSignedAt: false,
      accountIpAddress: false,
      customerSignedAt: false,
      customerIpAddress: false,
      marginSignedAt: false,
      marginIpAddress: false,
      terms: false,
    };
    const [agreementError, setAgreementError] = useState(initAgreementsError);

    // Other Variables
    const [agreementLoadingStatus, setAgreementLoadingStatus] = useState(true);

    useEffect(() => {
      loadStates();
    }, []);

    const loadStates = () => {
      setSelection1(handleNoYesOptionValues(requestData.isSelected1) || false);
      setSelection2(handleNoYesOptionValues(requestData.isSelected2) || false);

      setAccountSignedAt(requestData.aa_signed_at || '');
      setCustomerSignedAt(requestData.ca_signed_at || '');
      setMarginSignedAt(requestData.ma_signed_at || '');

      DeviceInfo.getIpAddress().then(ip => {
        setAccountIpAddress(
          requestData.aa_ip_address || Boolean(ip) ? String(ip) : '',
        );
        setCustomerIpAddress(
          requestData.ca_ip_address || Boolean(ip) ? String(ip) : '',
        );
        setMarginIpAddress(
          requestData.ma_ip_address || Boolean(ip) ? String(ip) : '',
        );
      });
      // setAccountIpAddress(requestData.aa_ip_address || '');
      // setCustomerIpAddress(requestData.ca_ip_address || '');
      // setMarginIpAddress(requestData.ma_ip_address || '');

      setAgreementLoadingStatus(false);
    };

    // const AlpacaCustomerAgreement = () => {
    //   const url =
    //     'https://files.alpaca.markets/disclosures/library/AcctAppMarginAndCustAgmt.pdf';
    //   if (InAppBrowser) {
    //     InAppBrowser.open(url, {
    //       showTitle: false,
    //       toolbarColor: 'red',
    //       secondaryToolbarColor: 'red',
    //       navigationBarColor: 'red',
    //       navigationBarDividerColor: 'red',
    //       enableUrlBarHiding: true,
    //       enableDefaultShare: true,
    //       forceCloseOnRedirection: false,
    //       hasBackButton: true,

    //       animations: {
    //         startEnter: 'slide_in_right',
    //         startExit: 'slide_out_left',
    //         endEnter: 'slide_in_left',
    //         endExit: 'slide_out_right',
    //       },
    //       headers: {
    //         'my-custom-header': 'my custom header value',
    //       },
    //     });
    //   }
    // };

    const handleAgreementsForm = () => {
      //  let error = agreementError;
      if (formStatus.disclosuresFormStatus) {
        if (
          accountSignedAt &&
          accountIpAddress &&
          customerSignedAt &&
          customerIpAddress &&
          marginSignedAt &&
          marginIpAddress &&
          isSelected1 &&
          isSelected2
        ) {
          const agreementRequestData = {
            ...requestData,
            account_agreement: 'account_agreement',
            aa_signed_at: accountSignedAt,
            aa_ip_address: accountIpAddress,
            customer_agreement: 'customer_agreement',
            ca_signed_at: customerSignedAt,
            ca_ip_address: customerIpAddress,
            margin_agreement: 'margin_agreement',
            ma_signed_at: marginSignedAt,
            ma_ip_address: marginIpAddress,
            isSelected1: isSelected1,
            isSelected2: isSelected2,
            // isSelected1: isSelected1,
            //isSelected2: isSelected2,
          };

          setRequestData(agreementRequestData);
          setFormStatus({...formStatus, agreementsFormStatus: true});

          navigation.navigate('Disclosures');
        } else {
          handleErrorValidation();
        }
      } else {
        Snackbar.show({
          duration: Snackbar.LENGTH_LONG,
          text: 'Please! Fill the Affilliations form',
        });
      }

      // if (isSelected1 === false) {
      //   error['terms'] = 'You should agree terms and conditions';
      // } else {
      //   error['terms'] = false;
      // }
    };

    const handleErrorValidation = () => {
      setAgreementError({
        ...agreementError,
        accountSignedAt: accountSignedAt ? false : true,
        accountIpAddress: accountIpAddress ? false : true,
        customerSignedAt: customerSignedAt ? false : true,
        customerIpAddress: customerIpAddress ? false : true,
        marginSignedAt: marginSignedAt ? false : true,
        marginIpAddress: marginIpAddress ? false : true,
        isSelected1: isSelected1 ? false : true,
        isSelected2: isSelected2 ? false : true,
        //terms: terms ? false : true
      });
    };

    return !agreementLoadingStatus ? (
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : null}
        keyboardVerticalOffset={Platform.OS === 'ios' ? 40 : 0}
        style={style.container}>
        <ScrollView
          keyboardShouldPersistTaps="handled"
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}>
          <View style={{backgroundColor: theme.themeColor, padding: 4}}>
            <Text
              style={{
                fontSize: Platform.OS == 'ios' ? 14 : 16,
                color: theme.secondryColor,
                fontWeight: 'bold',
              }}>
              Account Agreement
            </Text>
          </View>

          <View style={{paddingVertical: 8}}>
            <Text
              style={{
                fontSize: Platform.OS == 'ios' ? 14 : 16,
                color: '#ffffffc9',
                paddingHorizontal: 4,
              }}>
              Account signed on<Text style={{color: 'red'}}> *</Text>
            </Text>
            <View
              style={{
                marginTop: 5,
                borderBottomWidth: 1,
                borderBottomColor: 'white',
              }}>
              <TouchableOpacity
                onPress={() => {
                  setAccountSignedAtShow(true);
                }}>
                <View style={{flexDirection: 'row'}}>
                  <Ionicons
                    name="calendar-outline"
                    size={18}
                    color={theme.white}
                  />
                  <Text
                    style={{
                      color: 'white',
                      textAlign: 'center',
                      marginLeft: 8,
                    }}>
                    {Boolean(accountSignedAt) ? accountSignedAt : 'MM-DD-YYYY'}
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
            {accountSignedAtShow && (
              <DateTimePicker
                value={new Date()}
                display={Platform.OS === 'android' ? 'default' : 'spinner'}
                onChange={accountSignedAtTime}
              />
            )}
            {/* <DatePicker
              date={accountSignedAt}
              mode="date"
              placeholder="Click to confirm date"
              format="YYYY-MM-DD"
              confirmBtnText="Confirm"
              style={style.datePickerStyle}
              cancelBtnText="Cancel"
              customStyles={{
                dateIcon: {
                  position: 'absolute',
                  width: 24,
                  height: 24,
                  marginLeft: 0,
                  left: 0,
                },
                dateText: {
                  color: 'white',
                  textAlign: 'center',
                },
                dateInput: {
                  alignItems: 'flex-start',
                  borderWidth: 0,
                  borderBottomWidth: 1,
                  paddingLeft: 28,
                },
                datePicker: {
                  justifyContent: 'center',
                },
              }}
              onDateChange={date => {
                setAgreementError({...agreementError, accountSignedAt: false});

                setAccountSignedAt(
                  moment(`${date} ${moment().format('HH:mm:ss')}`).format(),
                );
              }}
            /> */}
          </View>

          {agreementError.accountSignedAt && (
            <Text style={[style.errormsg, {marginTop: 8}]}>
              Select account signed at
            </Text>
          )}

          <View>
            <Text
              style={{
                fontSize: Platform.OS == 'ios' ? 14 : 16,
                color: '#ffffffc9',
                paddingHorizontal: 4,
              }}>
              Account ip Address<Text style={{color: 'red'}}> *</Text>
            </Text>
          </View>
          <View
            style={{
              borderBottomWidth: 1,
              borderBottomColor: 'white',
              marginBottom: 12,
            }}>
            <TextInput
              placeholder={'ip Address'}
              defaultValue={accountIpAddress}
              onChangeText={txt => setAccountIpAddress(txt)}
              placeholderTextColor={'white'}
              style={{color: 'white', marginTop: 10}}
            />
          </View>

          {agreementError.accountIpAddress && (
            <Text style={style.errormsg}>Enter account ip address</Text>
          )}

          <View
            style={{
              backgroundColor: theme.themeColor,
              marginTop: 5,
              padding: 1,
            }}>
            <Text
              style={{
                fontSize: Platform.OS == 'ios' ? 14 : 16,
                color: theme.secondryColor,
                fontWeight: 'bold',
              }}>
              Customer Agreement
            </Text>
          </View>

          <View style={{paddingVertical: 8}}>
            <Text
              style={{
                fontSize: Platform.OS == 'ios' ? 14 : 16,
                color: '#ffffffc9',
                paddingHorizontal: 4,
              }}>
              Customer signed on<Text style={{color: 'red'}}> *</Text>
            </Text>

            <View
              style={{
                marginTop: 5,
                borderBottomWidth: 1,
                borderBottomColor: 'white',
              }}>
              <TouchableOpacity
                onPress={() => {
                  setCustomerSignedAtShow(true);
                }}>
                <View style={{flexDirection: 'row'}}>
                  <Ionicons
                    name="calendar-outline"
                    size={18}
                    color={theme.white}
                  />
                  <Text
                    style={{
                      color: 'white',
                      textAlign: 'center',
                      marginLeft: 8,
                    }}>
                    {Boolean(customerSignedAt)
                      ? customerSignedAt
                      : 'MM-DD-YYYY'}
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
            {customerSignedAtShow && (
              <DateTimePicker
                value={new Date()}
                display={Platform.OS === 'android' ? 'default' : 'spinner'}
                onChange={customerSignedAtTime}
              />
            )}
            {/* 
            <DatePicker
              date={customerSignedAt}
              mode="date"
              placeholder="Click to confirm date"
              format="YYYY-MM-DD"
              confirmBtnText="Confirm"
              cancelBtnText="Cancel"
              style={style.datePickerStyle}
              customStyles={{
                dateIcon: {
                  position: 'absolute',
                  width: 24,
                  height: 24,
                  marginLeft: 0,
                  left: 0,
                },
                dateText: {
                  color: 'white',
                  textAlign: 'center',
                },
                dateInput: {
                  alignItems: 'flex-start',
                  borderWidth: 0,
                  borderBottomWidth: 1,
                  paddingLeft: 28,
                },
                datePicker: {
                  justifyContent: 'center',
                },
              }}
              onDateChange={date => {
                setAgreementError({...agreementError, customerSignedAt: false});

                setCustomerSignedAt(
                  moment(`${date} ${moment().format('HH:mm:ss')}`).format(),
                );
              }}
            /> */}
          </View>

          {agreementError.customerSignedAt && (
            <Text style={[style.errormsg, {marginTop: 8}]}>
              Select customer signed at
            </Text>
          )}

          <View>
            <Text
              style={{
                fontSize: Platform.OS == 'ios' ? 14 : 16,
                color: '#ffffffc9',
                paddingHorizontal: 4,
              }}>
              Customer ip Address <Text style={{color: 'red'}}> *</Text>
            </Text>
          </View>
          <View
            style={{
              borderBottomWidth: 1,
              borderBottomColor: 'white',
              marginBottom: 12,
            }}>
            <TextInput
              placeholder={'Customer ip Address'}
              defaultValue={customerIpAddress}
              onChangeText={txt => setCustomerIpAddress(txt)}
              placeholderTextColor={'white'}
              style={{color: 'white', marginTop: 10}}
            />
          </View>
          {agreementError.customerIpAddress && (
            <Text style={style.errormsg}>Enter customer ip address</Text>
          )}

          <View
            style={{
              backgroundColor: theme.themeColor,
              marginTop: 5,
              padding: 1,
            }}>
            <Text
              style={{
                fontSize: Platform.OS == 'ios' ? 14 : 16,
                color: theme.secondryColor,
                fontWeight: 'bold',
              }}>
              Margin Agreement
            </Text>
          </View>

          <View style={{paddingVertical: 8}}>
            <Text
              style={{
                fontSize: Platform.OS == 'ios' ? 14 : 16,
                color: '#ffffffc9',
                paddingHorizontal: 4,
              }}>
              Margin signed on<Text style={{color: 'red'}}> *</Text>
            </Text>
            <View
              style={{
                marginTop: 5,
                borderBottomWidth: 1,
                borderBottomColor: 'white',
              }}>
              <TouchableOpacity
                onPress={() => {
                  setMarginSignedAtShow(true);
                }}>
                <View style={{flexDirection: 'row'}}>
                  <Ionicons
                    name="calendar-outline"
                    size={18}
                    color={theme.white}
                  />
                  <Text
                    style={{
                      color: 'white',
                      textAlign: 'center',
                      marginLeft: 8,
                    }}>
                    {Boolean(marginSignedAt) ? marginSignedAt : 'MM-DD-YYYY'}
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
            {marginSignedAtShow && (
              <DateTimePicker
                value={new Date()}
                display={Platform.OS === 'android' ? 'default' : 'spinner'}
                onChange={marginSignedAtTime}
              />
            )}
            {/* <DatePicker
              date={marginSignedAt}
              mode="date"
              placeholder="Click to confirm date"
              format="YYYY-MM-DD"
              confirmBtnText="Confirm"
              cancelBtnText="Cancel"
              style={style.datePickerStyle}
              customStyles={{
                dateIcon: {
                  position: 'absolute',
                  width: 24,
                  height: 24,
                  marginLeft: 0,
                  left: 0,
                },
                dateText: {
                  color: 'white',
                  textAlign: 'center',
                },
                dateInput: {
                  alignItems: 'flex-start',
                  borderWidth: 0,
                  borderBottomWidth: 1,
                  paddingLeft: 28,
                },
                datePicker: {
                  justifyContent: 'center',
                },
              }}
              onDateChange={date => {
                setAgreementError({...agreementError, marginSignedAt: false});

                setMarginSignedAt(
                  moment(`${date} ${moment().format('HH:mm:ss')}`).format(),
                );
              }}
            /> */}
          </View>

          {agreementError.marginSignedAt && (
            <Text style={[style.errormsg, {marginTop: 8}]}>
              Select margin signed at
            </Text>
          )}
          <View>
            <Text
              style={{
                fontSize: Platform.OS == 'ios' ? 14 : 16,
                color: '#ffffffc9',
                paddingHorizontal: 4,
              }}>
              Margin ip Address <Text style={{color: 'red'}}> *</Text>
            </Text>
          </View>
          <View
            style={{
              borderBottomWidth: 1,
              borderBottomColor: 'white',
              marginBottom: 12,
            }}>
            <TextInput
              placeholder={'Margin ip Address'}
              defaultValue={marginIpAddress}
              onChangeText={txt => setMarginIpAddress(txt)}
              placeholderTextColor={'white'}
              style={{color: 'white', marginTop: 10}}
            />
          </View>

          {agreementError.marginIpAddress && (
            <Text style={style.errormsg}>Enter margin ip address</Text>
          )}

          <View style={{paddingLeft: 4, marginTop: 10, flexDirection: 'row'}}>
            <View style={{flex: 0.9}}>
              <Text
                style={{
                  color: theme.secondryColor,
                  fontWeight: 'bold',
                  fontSize: Platform.OS == 'ios' ? 14 : 16,
                }}>
                Alpaca Customer Agreement
              </Text>
            </View>
            <TouchableOpacity
              //onPress={AlpacaCustomerAgreement}
              style={style.dropdownIconContainer}>
              <FontAwesomeIcons name="download" size={16} color={'white'} />
            </TouchableOpacity>
            <View></View>
          </View>

          <View style={{flexDirection: 'row', marginTop: 8}}>
            <View style={{flex: 0.09}}>
              <CheckBox
                boxType={'square'}
                value={isSelected1}
                // onValueChange={() => {
                //   setAgreementError({
                //     ...agreementError,
                //     terms: _.isBoolean(isSelected1)
                //     ? null
                //     : 'You should agree terms and conditions',
                //   });
                //   setSelection1(!isSelected1)
                // }}

                onValueChange={() => {
                  setSelection1(!isSelected1);
                }}
                tintColors={{true: theme.secondryColor, false: 'white'}}
                style={[
                  {color: '#fff'},
                  Platform.OS == 'ios' && {
                    transform: [{scaleX: 0.75}, {scaleY: 0.75}],
                  },
                ]}
              />
            </View>
            <View style={{marginTop: 6, flex: 0.9}}>
              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 12 : 14,
                  color: '#fff',
                  textAlign: 'justify',
                }}>
                I have read, understood, and agree to be bound by Alpaca
                Securities LLC and [your legal entity] account terms, and all
                other terms, disclosures and disclaimers applicable to me, as
                referenced in the Alpaca Customer Agreement. I also acknowledge
                that the Alpaca Customer Agreement contains a pre-dispute
                arbitration clause in Section 43.{' '}
              </Text>
            </View>
          </View>

          {agreementError.isSelected1 && (
            <Text style={[style.errormsg, {marginLeft: 10, marginVertical: 8}]}>
              You should agree terms and conditions
            </Text>
          )}

          <View style={{flexDirection: 'row', marginTop: 6}}>
            <View style={{flex: 0.09}}>
              <CheckBox
                boxType={'square'}
                value={isSelected2}
                onValueChange={() => {
                  setSelection2(!isSelected2);
                }}
                tintColors={{true: theme.secondryColor, false: 'white'}}
                style={[
                  {color: '#fff'},
                  Platform.OS == 'ios' && {
                    transform: [{scaleX: 0.75}, {scaleY: 0.75}],
                  },
                ]}
              />
            </View>
            <View style={{marginTop: 6, flex: 0.9}}>
              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 12 : 14,
                  color: '#fff',
                  textAlign: 'justify',
                }}>
                I understand I am signing this agreement electronically, and
                that my electronic signature will have the same effect as
                physically signing and returning the Application Agreement.
              </Text>
            </View>
          </View>

          {agreementError.isSelected2 && (
            <Text style={[style.errormsg, {marginLeft: 10, marginVertical: 8}]}>
              You should agree terms and conditions
            </Text>
          )}
          <View
            style={{
              justifyContent: 'center',
              alignSelf: 'center',
              marginTop: 13,
              marginLeft: 17,
            }}>
            <View style={{}}>
              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 12 : 14,
                  color: '#fff',
                }}>
                By clicking <Text style={{fontWeight: 'bold'}}>'I agree'</Text>{' '}
                I understand that I am signing...{' '}
              </Text>
            </View>
          </View>

          <TouchableOpacity
            style={style.signinBtn}
            onPress={() => {
              handleAgreementsForm();
            }}>
            <Text style={style.signintext}>I Agree</Text>
          </TouchableOpacity>
        </ScrollView>
      </KeyboardAvoidingView>
    ) : (
      renderScreenLoading()
    );
  };

  const DocumentsScreen = () => {
    // DocumentsScreen Variables
    const [doctype, setDoctype] = useState(null);
    const [idVerficationType, setIdVerficationType] = useState(null);
    const [document, setDocument] = useState(null);
    const [uploadResponse, setUploadResponse] = useState(null);

    // Error Variables
    const initDocumentsError = {
      doctype: false,
      document: false,
    };
    const [documentError, setDocumentError] = useState(initDocumentsError);

    // Other Variables
    const [documentsLoadingStatus, setDocumentsLoadingStatus] = useState(true);
    const [uploadingtLoadingStatus, setUploadingLoadingStatus] =
      useState(false);
    const [documentLoadingStatus, setDocumentLoadingStatus] = useState(false);

    const documentsOptions = [
      {label: 'Address verification', value: 'address_verification'},
      {
        label: 'Date of birth verification',
        value: 'date_of_birth_verification',
      },
      {label: 'Identity verification', value: 'identity_verification'},
      {label: 'Initial CIP result', value: 'cip_result'},
      {label: 'Tax ID verification', value: 'tax_id_verification'},
      {label: 'W-8 BEN tax form', value: 'w8ben'},
      {label: '407 approval letter', value: 'account_approval_letter'},
    ];

    const idOptions = ['Passport', 'Visa'];

    useEffect(() => {
      loadStates();
    }, []);

    const loadStates = () => {
      loadDocType(requestData?.document_type ?? null);
      setIdVerficationType(requestData.sub_type || null);
      setDocument(requestData.document || null);
      setDocumentsLoadingStatus(false);
    };

    const loadDocType = value => {
      if (
        Boolean(documentsOptions) &&
        Array.isArray(documentsOptions) &&
        documentsOptions.length != 0
      ) {
        const filteredDocType = documentsOptions.filter(
          lol => lol?.value == value,
        );

        setDoctype(filteredDocType.length != 0 ? filteredDocType[0] : null);
      } else {
        setDoctype(null);
      }
    };

    const handleRemoveDocuments = async () => {
      Snackbar.show({
        duration: Snackbar.LENGTH_INDEFINITE,
        text: 'Deleting...',
      });

      let response = await auth.removeAlpacaDocuments(uploadResponse.id);

      if (response && response.status) {
        Snackbar.show({
          duration: Snackbar.LENGTH_SHORT,
          text: response.message,
        });

        setDocument(null);

        setUploadResponse(null);
      } else {
        Snackbar.show({
          backgroundColor: 'red',
          duration: Snackbar.LENGTH_SHORT,
          text: response.message,
        });
      }
    };

    const handleUpload = () => {
      if (doctype?.value ?? null) {
        handleDocumentPicker(
          async onSuccess => {
            setUploadingLoadingStatus(true);

            let requestData = new FormData();

            requestData.append('document_type', doctype?.value ?? null);
            requestData.append('sub_type', idVerficationType);
            requestData.append('content', onSuccess);
            requestData.append('mime_type', onSuccess.type);

            let response = await auth.uploadAlpacaDocuments(requestData);

            if (response && response.status && response.response) {
              setDocument(onSuccess);

              setUploadResponse(response.response);

              setUploadingLoadingStatus(false);
            } else {
              setDocument(null);

              setUploadResponse(null);

              setUploadingLoadingStatus(false);
            }
          },
          onError => {
            setUploadError(onError);

            setDocument(null);

            setUploadResponse(null);

            setUploadingLoadingStatus(false);
          },
          () => {
            setUploadError(null);

            setDocument(null);

            setUploadResponse(null);

            setUploadingLoadingStatus(false);
          },
        );
      } else {
        setDocumentError({
          ...documentError,
          doctype: doctype?.value ? false : true,
        });
      }
    };

    const handleDocumentsForm = () => {
      if (formStatus.agreementsFormStatus) {
        if ((doctype?.value ?? null) && document) {
          const documentRequestData = {
            ...requestData,
            document_type: doctype?.value ?? null,
            sub_type: idVerficationType,
            document: document,
          };

          setRequestData(documentRequestData);
          setFormStatus({...formStatus, documentsFormStatus: true});

          navigation.navigate('Disclosures');
        } else {
          handleErrorValidation();
        }
      } else {
        Snackbar.show({
          duration: Snackbar.LENGTH_LONG,
          // text: 'Please! Fill the agreement form',
        });
      }
    };

    const handleErrorValidation = () => {
      setDocumentError({
        ...documentError,
        doctype: doctype?.value ?? null ? false : true,
        document: document ? false : true,
      });
    };

    return !documentsLoadingStatus ? (
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : null}
        keyboardVerticalOffset={Platform.OS === 'ios' ? 40 : 0}
        style={style.container}>
        <ScrollView
          keyboardShouldPersistTaps="handled"
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}>
          <View style={style.spaceBetween}>
            <View style={{flex: 0.4, justifyContent: 'center'}}>
              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 12 : 14,
                  color: '#ffffffc9',
                }}>
                Document Type<Text style={{color: 'red'}}> *</Text>
              </Text>
            </View>

            <View style={{flex: 0.6, justifyContent: 'center'}}>
              <SelectDropdown
                buttonStyle={style.dropdownWrapper}
                data={documentsOptions}
                defaultButtonText="Document Type"
                dropdownStyle={{
                  width: '80%',
                  marginLeft: 8,
                  alignSelf: 'center',
                }}
                onSelect={selectedItem => {
                  setDocumentError({...documentError, doctype: false});

                  setDoctype(selectedItem);
                }}
                renderCustomizedButtonChild={selectedItem => {
                  return (
                    <View style={style.dropdownItemContainer}>
                      <View style={style.dropdownLabelContainer}>
                        <Text style={style.dropdownLabelText}>
                          {selectedItem?.label ?? 'Document Type'}
                        </Text>
                      </View>
                      <View style={style.dropdownIconContainer}>
                        <FontAwesomeIcons
                          name="caret-down"
                          size={16}
                          color={'white'}
                        />
                      </View>
                    </View>
                  );
                }}
                rowStyle={{borderBottomWidth: 0}}
                rowTextForSelection={item => item?.label}
                rowTextStyle={style.dropdownItemText}
                selectedRowTextStyle={style.dropdownSelectedItemText}
                defaultValue={doctype}
              />
            </View>
          </View>

          {documentError.doctype && (
            <Text
              style={[
                style.errormsg,
                {marginHorizontal: 0, marginVertical: 8},
              ]}>
              Select any one document type
            </Text>
          )}

          {doctype?.value == 'identity_verification' && (
            <View style={[style.spaceBetween, {marginTop: 8}]}>
              <View style={{flex: 0.4, justifyContent: 'center'}}>
                <Text
                  style={{
                    fontSize: Platform.OS == 'ios' ? 12 : 14,
                    color: '#ffffffc9',
                  }}>
                  Id verification type
                </Text>
              </View>

              <View style={{flex: 0.6, justifyContent: 'center'}}>
                <SelectDropdown
                  buttonStyle={style.dropdownWrapper}
                  data={idOptions}
                  defaultButtonText="ID Type"
                  dropdownStyle={{
                    width: '80%',
                    marginLeft: 8,
                    alignSelf: 'center',
                  }}
                  onSelect={selectedItem => {
                    setIdVerficationType(selectedItem);
                  }}
                  renderCustomizedButtonChild={selectedItem => {
                    return (
                      <View style={style.dropdownItemContainer}>
                        <View style={style.dropdownLabelContainer}>
                          <Text style={style.dropdownLabelText}>
                            {selectedItem ?? 'ID Type'}
                          </Text>
                        </View>
                        <View style={style.dropdownIconContainer}>
                          <FontAwesomeIcons
                            name="caret-down"
                            size={16}
                            color={'white'}
                          />
                        </View>
                      </View>
                    );
                  }}
                  rowStyle={{borderBottomWidth: 0}}
                  rowTextForSelection={item => item}
                  rowTextStyle={style.dropdownItemText}
                  selectedRowTextStyle={style.dropdownSelectedItemText}
                  defaultValue={idVerficationType}
                />
              </View>
            </View>
          )}

          <View style={[style.spaceBetween, {marginTop: 8}]}>
            <View style={{flex: 0.4, justifyContent: 'center'}}>
              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 12 : 14,
                  color: '#ffffffc9',
                }}>
                Document<Text style={{color: 'red'}}> *</Text>
              </Text>
            </View>

            <View style={{flex: 0.6, justifyContent: 'center'}}>
              {document ? (
                <View
                  style={{
                    width: 50,
                    height: 50,
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderColor: 'white',
                    borderWidth: 1,
                    borderRadius: 4,
                  }}>
                  <Image
                    onLoadStart={() => {
                      setDocumentLoadingStatus(true);
                    }}
                    onLoadEnd={() => {
                      setDocumentLoadingStatus(false);
                    }}
                    source={{
                      uri:
                        document.type === 'application/pdf'
                          ? 'https://play-lh.googleusercontent.com/nufRXPpDI9XP8mPdAvOoJULuBIH_OK4YbZZVu8i_-eDPulZpgb-Xp-EmI8Z53AlXHpqX'
                          : document.uri,
                    }}
                    style={{width: 48, height: 48, borderRadius: 4}}
                  />

                  <TouchableOpacity
                    style={{
                      position: 'absolute',
                      backgroundColor: theme.secondryColor,
                      borderTopRightRadius: 4,
                      paddingHorizontal: 2,
                      paddingVertical: 2,
                      top: 0,
                      right: 0,
                    }}
                    onPress={() => {
                      handleRemoveDocuments();
                    }}>
                    <Icon name={'times'} size={14} color={theme.white} />
                  </TouchableOpacity>

                  {documentLoadingStatus && renderActivityIndicator()}
                </View>
              ) : (
                <TouchableOpacity
                  style={style.uploadbtn}
                  onPress={() => {
                    setDocumentError({...documentError, document: false});

                    handleUpload();
                  }}>
                  {uploadingtLoadingStatus ? (
                    <ActivityIndicator size="small" color={theme.white} />
                  ) : (
                    <>
                      <Icon name="upload" color={'white'} size={16} />
                      <Text style={style.uploadtxt}>Upload</Text>
                    </>
                  )}
                </TouchableOpacity>
              )}
            </View>
          </View>

          {documentError.document && (
            <Text
              style={[
                style.errormsg,
                {marginHorizontal: 0, marginVertical: 8},
              ]}>
              Upload document
            </Text>
          )}

          {uploadError && (
            <Text
              style={[
                style.errormsg,
                {marginHorizontal: 0, marginVertical: 8},
              ]}>
              {uploadError == 'sizeError'
                ? 'File size must be less than 10 MB'
                : 'Supported file formats are jpeg/jpg/pdf'}
            </Text>
          )}

          <TouchableOpacity
            style={style.signinBtn}
            onPress={() => {
              handleDocumentsForm();
            }}>
            <Text style={style.signintext}>Next</Text>
          </TouchableOpacity>

          <View style={{paddingVertical: 16}} />
        </ScrollView>
      </KeyboardAvoidingView>
    ) : (
      renderScreenLoading()
    );
  };

  const TrustedScreen = () => {
    // TrustedScreen Variables
    const [firstname, setFirstname] = useState(null);
    const [familyname, setFamilyname] = useState(null);
    const [email, setEmail] = useState(null);
    const [phoneno, setPhoneno] = useState(null);
    const [street, setStreet] = useState(null);
    const [city, setCity] = useState(null);
    const [stateSelected, setStateSelected] = useState(null);
    const [postalcode, setPostalcode] = useState(null);
    const [country, setCountry] = useState(null);
    const [all, setAll] = useState(false);
    const [selectEmail, setSelectEmail] = useState(false);
    const [selectAddress, setSelectAddress] = useState(false);
    const [selectPhoneno, setSelectPhoneno] = useState(false);
    const [selectTrustedType, setSelectTrustedType] = useState(null);

    console.log('selectEmail///:::::', selectEmail);
    console.log('selectAddress///:::::', selectAddress);
    console.log('selectPhoneno///:::::', selectPhoneno);

    // Error Variables
    const initTrustedContactError = {
      firstname: false,
      familyname: false,
      email: false,
      phoneno: false,
      streetAddress: false,
      city: false,
      country: false,
      state: false,
      postalcode: false,
    };
    const [trustedContactError, setTrustedContactError] = useState(
      initTrustedContactError,
    );

    // Other Variables
    const [tcLoadingStatus, setTCLoadingStatus] = useState(true);

    useEffect(() => {
      loadStates();
    }, []);

    const loadStates = async () => {
      setFirstname(requestData.tc_given_name || null);
      setFamilyname(requestData.tc_family_name || null);

      setEmail(requestData.tc_email_address || null);
      // if(requestData.tc_email_address){
      //   setSelectEmail(true);
      // }
      setPhoneno(requestData.tc_phone_number || null);
      setStreet(requestData.tc_street_address || null);
      setCity(requestData.tc_city || null);
      //loadCountry(requestData.tc_country || null);
      setCountry(requestData.tc_country || null);

      //loadCountry(requestData.tc_country || userData?.country_code || null);
      loadState(requestData.tc_state || null);
      setPostalcode(requestData.tc_postal_code || null);
      setTCLoadingStatus(false);
    };

    const loadState = value => {
      if (
        Boolean(selectData?.state) &&
        Array.isArray(selectData.state) &&
        selectData.state.length != 0
      ) {
        const filteredState = selectData.state.filter(
          lol => lol?.value == value,
        );

        setStateSelected(filteredState.length != 0 ? filteredState[0] : null);
      } else {
        setStateSelected(null);
      }
    };

    const loadCountry = value => {
      if (
        Boolean(selectData?.countryList) &&
        Array.isArray(selectData.countryList) &&
        selectData?.countryList?.length != 0
      ) {
        const filteredCountry = selectData.countryList.filter(
          lol => lol?.value == value,
        );

        setCountry(filteredCountry?.length != 0 ? filteredCountry[0] : null);
      } else {
        setCountry(null);
      }
    };

    const handleTrustedContactForm = async () => {
      // Hided If-Else to avoid Documents form validation
      // if (formStatus.documentsFormStatus) {
      if (
        firstname &&
        familyname &&
        email &&
        phoneno &&
        country &&
        street &&
        city &&
        stateSelected &&
        postalcode
      ) {
        const trustedContactRequestData = {
          ...requestData,
          tc_given_name: firstname,
          tc_family_name: familyname,
          tc_email_address: email,
          tc_phone_number: phoneno,
          tc_country: country?.value ?? null,
          tc_street_address: street,
          tc_city: city,
          tc_state: stateSelected?.value ?? null,
          tc_postal_code: postalcode,
          // tc_country: 'USA',
        };

        await setRequestData(trustedContactRequestData);

        let response = await auth.registerAlpaca(trustedContactRequestData);
        setUploadingtLoadingStatuss(true);
        setTCLoadingStatus(true);
        if (response.keyword == 'success') {
          Snackbar.show({
            duration: Snackbar.LENGTH_SHORT,
            text: response.message,
          });

          setUploadingtLoadingStatuss(false);
          setAlpacaConfigStatus(true);

          await setRequestData({});

          navigation.navigate('ViewAlpaca');
          // setTCLoadingStatus(false);
        } else {
          Snackbar.show({
            backgroundColor: 'red',
            duration: Snackbar.LENGTH_LONG,
            text: response.message,
          });
          setUploadingtLoadingStatuss(false);
        }
      } else {
        handleErrorValidation();
      }
      // } else {
      //   Snackbar.show({
      //     duration: Snackbar.LENGTH_LONG,
      //     text: 'Please! Fill the document form',
      //   });
      // }
    };
    // const options =(()=>{
    //   if(selectEmail)
    // })
    const handleErrorValidation = () => {
      setTrustedContactError({
        ...trustedContactError,
        firstname: firstname ? false : true,
        familyname: familyname ? false : true,
        email: email ? false : true,
        phoneno: phoneno ? false : true,
        streetAddress: street ? false : true,
        city: city ? false : true,
        state: stateSelected ? false : true,
        country: country ? false : true,
        PostalCode: postalcode ? false : true,

        // selectEmail: selectEmail== true ? (email ? false : true):null,
        // selectPhoneno: selectPhoneno == true ? (phoneno ? false : true):null,
        //   selectAddress:  selectAddress ==true ? (street ? false : true)||(city ? false : true)||(stateSelected ? false : true)||(country ? false : true):null,
      });
    };

    // const trustedSelect =['Email','Phoneno','Address','All']

    return !tcLoadingStatus ? (
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : null}
        keyboardVerticalOffset={Platform.OS === 'ios' ? 40 : 0}
        style={style.container}>
        <ScrollView
          keyboardShouldPersistTaps="handled"
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}>
          <FloatingTitleTextInputField
            ispassword={false}
            attrName="firstname"
            title={
              <Text>
                First Name<Text style={{color: 'red'}}> *</Text>
              </Text>
            }
            value={firstname}
            updateMasterState={txt => {
              setTrustedContactError({
                ...trustedContactError,
                firstname: false,
              });

              setFirstname(txt);
            }}
          />

          {trustedContactError.firstname && (
            <Text style={style.errormsg}>Enter First name</Text>
          )}

          <FloatingTitleTextInputField
            ispassword={false}
            attrName="familyname"
            title={
              <Text>
                Last Name<Text style={{color: 'red'}}> *</Text>
              </Text>
            }
            value={familyname}
            updateMasterState={txt => {
              setTrustedContactError({
                ...trustedContactError,
                familyname: false,
              });

              setFamilyname(txt);
            }}
          />

          {trustedContactError.familyname && (
            <Text style={style.errormsg}>Enter Last name</Text>
          )}

          {/* {selectEmail && ( */}
          <FloatingTitleTextInputField
            keyboardType="email-address"
            ispassword={false}
            attrName="email"
            title={
              <Text>
                Email<Text style={{color: 'red'}}> *</Text>
              </Text>
            }
            value={email}
            updateMasterState={txt => {
              setTrustedContactError({
                ...trustedContactError,
                email: false,
              });

              setEmail(txt);
            }}
          />
          {/* )} */}
          {trustedContactError.email && (
            <Text style={style.errormsg}>Enter email</Text>
          )}

          {/* {selectPhoneno && ( */}
          <FloatingTitleTextInputField
            keyboardType="number-pad"
            ispassword={false}
            attrName="phoneno"
            title={
              <Text>
                Phone No<Text style={{color: 'red'}}> *</Text>
              </Text>
            }
            value={phoneno}
            updateMasterState={txt => {
              setTrustedContactError({
                ...trustedContactError,
                phoneno: false,
              });

              setPhoneno(txt);
            }}
          />
          {/* )} */}

          {trustedContactError.phoneno && (
            <Text style={style.errormsg}>Enter phone no</Text>
          )}

          {/* {selectAddress && ( */}
          {/* <View> */}
          <FloatingTitleTextInputField
            ispassword={false}
            attrName="street"
            title={
              <Text>
                Street address<Text style={{color: 'red'}}> *</Text>
              </Text>
            }
            value={street}
            updateMasterState={txt => {
              setStreet(txt);
            }}
          />
          {trustedContactError.streetAddress && (
            <Text style={style.errormsg}>Enter Street Address</Text>
          )}

          <FloatingTitleTextInputField
            ispassword={false}
            attrName="city"
            title={
              <Text>
                City<Text style={{color: 'red'}}> *</Text>
              </Text>
            }
            value={city}
            updateMasterState={txt => {
              setCity(txt);
            }}
          />
          {trustedContactError.city && (
            <Text style={style.errormsg}>Enter City </Text>
          )}
          <FloatingTitleSelect
            options={selectData?.countryList ?? []}
            displayLabel="Select Country"
            title={
              <Text>
                Country<Text style={{color: 'red'}}> *</Text>
              </Text>
            }
            selectedValue={country}
            onValueChange={itemValue => {
              setCountry(itemValue);
            }}
          />
          {trustedContactError.country && (
            <Text style={style.errormsg}>Select Country</Text>
          )}
          <FloatingTitleSelect
            options={selectData?.state ?? []}
            displayLabel="Select State"
            title={
              <Text>
                State<Text style={{color: 'red'}}> *</Text>
              </Text>
            }
            selectedValue={stateSelected}
            onValueChange={itemValue => {
              setStateSelected(itemValue);
            }}
          />
          {trustedContactError.state && (
            <Text style={style.errormsg}>Select State</Text>
          )}
          <FloatingTitleTextInputField
            keyboardType="number-pad"
            ispassword={false}
            attrName="postalcode"
            title={
              <Text>
                Postal code<Text style={{color: 'red'}}> *</Text>
              </Text>
            }
            value={postalcode}
            updateMasterState={txt => {
              setPostalcode(txt);
            }}
          />
          {/* </View>
          )} */}
          {/* 
          {trustedContactError.selectAddress && (
            <Text style={style.errormsg}>Enter All The Address Details no</Text>
          )} */}
          {/* {trustedContactError.cite && (
            <Text style={style.errormsg}>Enter Cite Name</Text>
          )}
          {trustedContactError.country && (
            <Text style={style.errormsg}>Select Country</Text>
          )}

          {trustedContactError.stateSelected && (
            <Text style={style.errormsg}>Select State </Text>
          )} */}
          {trustedContactError.PostalCode && (
            <Text style={style.errormsg}>Enter Postal Code </Text>
          )}
          <TouchableOpacity
            style={style.signinBtn}
            onPress={() => {
              handleTrustedContactForm();
            }}>
            {uploadingtLoadingStatuss ? (
              <ActivityIndicator size="small" color={theme.white} />
            ) : (
              <Text style={style.signintext}>Register</Text>
            )}
          </TouchableOpacity>

          <View style={{paddingVertical: 16}} />
        </ScrollView>
      </KeyboardAvoidingView>
    ) : (
      renderScreenLoading()
    );
  };

  const DisclosuresScreen = () => {
    const [UseandRisk, setUseandRisk] = useState(false);

    const webView = useRef(null);
    const Pdf = useRef(null);
    // const resourceType = 'url';
    let source = {
      uri: 'http://samples.leanpub.com/thereactnativebook-sample.pdf',
      cache: true,
    };
    // let uri2 =`https://www.npmjs.com/package/react-native-pdf`
    const displayActivityIndicator = () => {
      return <ActivityIndicator size={24} color={theme.primaryColor} />;
    };

    return (
      <ScrollView>
        <View
          style={{
            marginTop: 10,
            flexDirection: 'row',
            justifyContent: 'center',
            backgroundColor: theme.primaryColor,
            borderRadius: 5,
            padding: 10,
          }}>
          <View style={{flex: 0.9, marginLeft: 9}}>
            <Text
              style={{
                color: theme.secondryColor,
                fontSize: Platform.OS == 'ios' ? 14 : 16,
                fontWeight: 'bold',
                textAlign: 'left',
              }}>
              Use and Risk
            </Text>
          </View>
          <TouchableOpacity
            onPress={UseAndRisk}
            style={[style.dropdownIconContainer, {flex: 0.1}]}>
            <FontAwesomeIcons name="download" size={16} color={'white'} />
          </TouchableOpacity>
        </View>

        <Modal
          isVisible={UseandRisk}
          onBackdropPress={() => {
            setUseandRisk(false);
          }}>
          <View style={style.tdModalContainer}>
            <View style={style.tdModalHeaderContainer}>
              <Text style={style.headingTxt}>Use and Risk</Text>

              <TouchableOpacity
                onPress={() => {
                  setUseandRisk(false);
                }}>
                <Icon name="close" size={18} color="white" />
              </TouchableOpacity>
            </View>

            {/* <WebView
            source={uri2}
            ref={webView}
            startInLoadingState={true}
            renderLoading={() => {
              return displayActivityIndicator();
            }}
            cacheEnabled={true}
            style={{borderBottomLeftRadius: 8, borderBottomRightRadius: 8}}
       
           /> */}
          </View>
        </Modal>
        {/* <View style={{   flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginTop: 25,}}>
          <Pdf
                    source={source}
                 
                   />
                   </View> */}

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            backgroundColor: theme.primaryColor,
            borderRadius: 5,
            padding: 10,
            marginTop: 15,
          }}>
          <View style={{flex: 0.9, marginLeft: 9}}>
            <Text
              style={{
                color: theme.secondryColor,
                fontSize: Platform.OS == 'ios' ? 14 : 16,
                fontWeight: 'bold',
                textAlign: 'left',
              }}>
              Terms and Conditions
            </Text>
          </View>
          <TouchableOpacity
            onPress={TermsAndConditions}
            style={[style.dropdownIconContainer, {flex: 0.1}]}>
            <FontAwesomeIcons name="download" size={16} color={'white'} />
          </TouchableOpacity>
        </View>

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            backgroundColor: theme.primaryColor,
            borderRadius: 5,
            padding: 10,
            marginTop: 15,
          }}>
          <View style={{flex: 0.9, marginLeft: 9}}>
            <Text
              style={{
                color: theme.secondryColor,
                fontSize: Platform.OS == 'ios' ? 14 : 16,
                fontWeight: 'bold',
                textAlign: 'left',
              }}>
              Privacy Notice
            </Text>
          </View>
          <TouchableOpacity
            onPress={PrivacyNotice}
            style={[style.dropdownIconContainer, {flex: 0.1}]}>
            <FontAwesomeIcons name="download" size={16} color={'white'} />
          </TouchableOpacity>
        </View>

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            backgroundColor: theme.primaryColor,
            borderRadius: 5,
            padding: 10,
            marginTop: 15,
          }}>
          <View style={{flex: 0.9, marginLeft: 9}}>
            <Text
              style={{
                color: theme.secondryColor,
                fontSize: Platform.OS == 'ios' ? 14 : 16,
                fontWeight: 'bold',
                textAlign: 'left',
              }}>
              PFOF
            </Text>
          </View>
          <TouchableOpacity
            onPress={PFOF}
            style={[style.dropdownIconContainer, {flex: 0.1}]}>
            <FontAwesomeIcons name="download" size={16} color={'white'} />
          </TouchableOpacity>
        </View>

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            backgroundColor: theme.primaryColor,
            borderRadius: 5,
            padding: 10,
            marginTop: 15,
          }}>
          <View style={{flex: 0.9, marginLeft: 9}}>
            <Text
              style={{
                color: theme.secondryColor,
                fontSize: Platform.OS == 'ios' ? 14 : 16,
                fontWeight: 'bold',
                textAlign: 'left',
              }}>
              Margin Disclosure Statement
            </Text>
          </View>
          <TouchableOpacity
            onPress={MarginDiscStmt}
            style={[style.dropdownIconContainer, {flex: 0.1}]}>
            <FontAwesomeIcons name="download" size={16} color={'white'} />
          </TouchableOpacity>
        </View>

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            backgroundColor: theme.primaryColor,
            borderRadius: 5,
            padding: 10,
            marginTop: 15,
          }}>
          <View style={{flex: 0.9, marginLeft: 9}}>
            <Text
              style={{
                color: theme.secondryColor,
                fontSize: Platform.OS == 'ios' ? 14 : 16,
                fontWeight: 'bold',
                textAlign: 'left',
              }}>
              Extended Hours Trading Risk
            </Text>
          </View>
          <TouchableOpacity
            onPress={ExtHrsRisk}
            style={[style.dropdownIconContainer, {flex: 0.1}]}>
            <FontAwesomeIcons name="download" size={16} color={'white'} />
          </TouchableOpacity>
        </View>

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            backgroundColor: theme.primaryColor,
            borderRadius: 5,
            padding: 10,
            marginTop: 15,
          }}>
          <View style={{flex: 0.9, marginLeft: 9}}>
            <Text
              style={{
                color: theme.secondryColor,
                fontSize: Platform.OS == 'ios' ? 14 : 16,
                fontWeight: 'bold',
                textAlign: 'left',
              }}>
              Business Continuity Plan Summary
            </Text>
          </View>
          <TouchableOpacity
            onPress={BCPSummary}
            style={[style.dropdownIconContainer, {flex: 0.1}]}>
            <FontAwesomeIcons name="download" size={16} color={'white'} />
          </TouchableOpacity>
        </View>

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            backgroundColor: theme.primaryColor,
            borderRadius: 5,
            padding: 10,
            marginTop: 15,
          }}>
          <View style={{flex: 0.9, marginLeft: 9}}>
            <Text
              style={{
                color: theme.secondryColor,
                fontSize: Platform.OS == 'ios' ? 14 : 16,
                fontWeight: 'bold',
                textAlign: 'left',
              }}>
              Form CRS
            </Text>
          </View>
          <TouchableOpacity
            onPress={OpForm}
            style={[style.dropdownIconContainer, {flex: 0.1}]}>
            <FontAwesomeIcons name="download" size={16} color={'white'} />
          </TouchableOpacity>
        </View>
        <View style={{paddingVertical: 16}} />
        <TouchableOpacity
          style={style.signinBtn}
          onPress={() => {
            navigation.navigate('Trusted Contact');
          }}>
          <Text style={style.signintext}>Next</Text>
        </TouchableOpacity>
      </ScrollView>
    );
  };

  const renderScreenLoading = () => {
    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <ActivityIndicator size="large" color={theme.white} />
      </View>
    );
  };

  const handleDocumentPicker = async (onSuccess, onError, onCatch) => {
    try {
      const fileData = await DocumentPicker.pick({
        type: [DocumentPicker.types.allFiles],
      });

      if (fileData.size > 1024 * 1024 * 10) {
        onError('sizeError');
      } else {
        onError(null);

        onSuccess(fileData);
      }
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        onCatch(true);
      } else {
        alert(err);
      }
    }
  };

  const renderActivityIndicator = () => {
    return (
      <ActivityIndicator
        size="small"
        color={theme.secondryColor}
        style={{
          position: 'absolute',
          left: 0,
          top: 0,
          right: 0,
          bottom: 0,
        }}
      />
    );
  };

  return (
    <>
      <HeaderNav title="Alpaca New Register" isBack={true} />

      <View style={style.container}>
        <Tab.Navigator
          style={{backgroundColor: theme.primaryColor}}
          sceneContainerStyle={{
            backgroundColor: theme.themeColor,
            paddingHorizontal: 8,
            paddingVertical: 8,
          }}
          tabBar={props => <MyTabBar {...props} />}
          backBehavior="initialRoute"
          initialRouteName="Personal"
          animationEnabled={true}
          swipeEnabled={true}
          tabBarOptions={{scrollEnabled: true}}>
          <Tab.Screen name="Personal" component={PersonalScreen} />

          <Tab.Screen name="Affilliations" component={AffilliationsScreen} />

          <Tab.Screen name="Agreements" component={AgreementsScreen} />

          {/* Hided temporarily as per client request */}
          {/*<Tab.Screen name="Documents" component={DocumentsScreen} />*/}
          <Tab.Screen name="Disclosures" component={DisclosuresScreen} />
          <Tab.Screen name="Trusted Contact" component={TrustedScreen} />
        </Tab.Navigator>
      </View>

      <BottomNav routeName="Profile" />
    </>
  );
};

export default NewRegisterScreen;
{
  /* <UserDataContext.Provider 
value={{
  citizenShipYes,
  setcitizenShipYes,
}}>

</UserDataContext.Provider> */
}
