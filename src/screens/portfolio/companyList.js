import React, {useRef, useState, useContext, useEffect} from 'react';
import {
  ActivityIndicator,
  Image,
  Platform,
  Text,
  TouchableOpacity,
  View,
  FlatList,
} from 'react-native';
import {VictoryChart, VictoryLine} from 'victory-native';
import {ALPACA, AMERITRADE} from '../../assets/images/index';
import theme from '../../utils/theme';
import SellShareAmount from '../Dashboard/order-amount';
import SellShareQuantity from '../Dashboard/order-quantity';
import styles from './style';
import {UserDataContext} from '../../context/UserDataContext';
import {ScrollView} from 'react-native-gesture-handler';
const PortfolioTab = (props) => {
  // PortfolioTab Variables
  const orderList = props.orderList || [];

  const [sellData, setSellData] = useState(null);
  const [currentPage, setCurrentPage] = useState(0);
  const {
    userId,
    setUserId,
    setSellBtn,
    showCurrentPage,
    setShowCurrentPage,
    loadMoreData,
    setLoadMoreData,
    cardLoad,
  } = useContext(UserDataContext);
  console.log('userId<<?>>',userId)
  // Ref Variables
  const sellRef = useRef();
  // Other Variables
  const loading = props.loading;
  const style = getStyles();
  function getStyles() {
    const GREEN_COLOR = '#00BD9A';
    const RED_COLOR = '#FF6960';
    return {
      lineOne: {
        data: {stroke: GREEN_COLOR, strokeWidth: 1},
      },
      lineTwo: {
        data: {stroke: RED_COLOR, strokeWidth: 1},
      },
    };
  }
  const isFloat = (n) => {
    return Number(n) % 1 !== 0;
  };

  const renderItem = (lol, index) => {
    return (
      <View
        style={[styles.companylist, {marginTop: index != 0 ? 5 : 0},{marginBottom: index != 0 ? 12 : 0}]}
        key={index?.toString()}>
        <View
          style={{
            flex: lol.percentage ? 0.2 : 0.325,
            justifyContent: 'center',
          }}>
          <Text style={styles.companySymbolTxt}>{lol.item.symbol}</Text>
          <Text style={styles.companySharesTxt}>
            {lol.item.qty
              ? parseFloat(lol?.item?.qty).toFixed(isFloat(lol?.item?.qty) ? 2 : 0)
              : 0}{' '}
            Shares
          </Text>
          <Text style={styles.companyShareAmtTxt}>
            ${' '}
            {lol?.item?.latestPrice && lol?.item?.qty
              ? parseFloat(lol?.item?.latestPrice * lol?.item?.qty).toFixed(2)
              : 0.0}
          </Text>
        </View>
        {Boolean(lol.percentage) && (
          <View style={{flex: 0.2, justifyContent: 'center'}}>
            <View style={styles.companyPercentageContainer}>
              <Text style={styles.companyPercentageTxt}>
                {lol.item.percentage || 0}%
              </Text>
            </View>
          </View>
        )}
        <View
          style={{
            flex: lol.item.percentage ? 0.1875 : 0.2,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Image
            source={lol.broker_connection == 1 ? AMERITRADE : ALPACA}
            style={{width: 48, height: 36, resizeMode: 'contain'}}
          />
        </View>
        <View
          style={{
            flex: lol.percentage ? 0.1875 : 0.25,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <VictoryChart
            padding={{top: 0, bottom: 0, right: 0, left: 0}}
            height={48}
            width={lol.item.percentage ? 60 : 80}>
            <VictoryLine data={lol.item.chartdata} style={style.lineOne} />
          </VictoryChart>
        </View>
        <View
          style={{
            flex: 0.225,
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <View style={[lol.change < 0 ? styles.downgrade : styles.upgrade]}>
            <Text style={styles.companyChangeTxt}>$ {lol?.item?.change}</Text>
          </View>
          <TouchableOpacity
            style={styles.sell}
            onPress={async () => {
              let sellDetails = {
                brokerConnection: lol.item.broker_connection,
                name: lol.item.name,
                latest_price: lol.item.latestPrice,
                qty: lol.item.qty,
                side: 'sell',
                symbol: lol.item.symbol,
                stock_buyed_by: userId,
                fromPage:
                  props.Path && props.Path.includes('people/overall')
                    ? 'Portfolio People'
                    : '',
                id: '',
                creatorId: '',
                directUserTotalAmount: null,
                directUserPercentage: null,
                fractionable: lol.item.fractionable,
              };
              await setSellData(sellDetails);
              sellRef.current.open();
            }}>
            <Text onPress={setSellBtn(true)} style={styles.companySellTxt}>
              Sell
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  };
  const renderLoader = () => {
    return cardLoad ? (
      <View style={styles.companyListLoader}>
        <ActivityIndicator size="large" color={theme.secondryColor} />
      </View>
    ) : null;
  };
  const loadMoreItem = () => {
    if (loadMoreData) {
      setCurrentPage(currentPage + 1);
    }
  };

  const ItemSeparatorView = () => {
    return (
      // Flat List Item Separator
      <View />
    );
  };
  return (
    <>
      <ScrollView
        showsVerticalScrollIndicator={false}
        style={{
          backgroundColor: theme.primaryColor,
          borderRadius: 8,
          paddingVertical: 10,
          paddingHorizontal: 10,
          width: '98%',
          marginTop: 8,
          alignSelf: 'center',
        }}
        >
        <FlatList
          data={orderList}
          renderItem={renderItem}
          ItemSeparatorComponent={ItemSeparatorView}
          keyExtractor={(item) => {
            item.qty;
          }}
          ListFooterComponent={renderLoader}
          onEndReached={loadMoreItem}
          onEndReachedThreshold={0.01}
          showsVerticalScrollIndicator={false}
        />
        {sellData && sellData?.fractionable && (
          <SellShareAmount
            buyform={sellRef}
            orderData={sellData}
            onChange={() => sellRef.current.close()}
          />
        )}
        {sellData && !sellData?.fractionable && (
          <SellShareQuantity
            buyform={sellRef}
            orderData={sellData}
            onChange={() => sellRef.current.close()}
          />
        )}
      </ScrollView>
    </>
  );
};
export default PortfolioTab;
