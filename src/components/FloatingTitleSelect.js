import React from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import theme from '../utils/theme';
import SelectDropdown from 'react-native-select-dropdown';
import FontAwesomeIcons from 'react-native-vector-icons/FontAwesome5';

const FloatingTitleSelect = ({...props}) => {
  return (
    <View style={Styles.container}>
      <Text style={Styles.titleStyles}>{props.title}</Text>

      <SelectDropdown
        buttonStyle={Styles.dropdownWrapper}
        data={props.options}
        defaultButtonText={props.displayLabel ?? 'Select'}
        dropdownStyle={{
          width: '80%',
          marginLeft: 8,
          alignSelf: 'center',
        }}
        onSelect={props.onValueChange}
        renderCustomizedButtonChild={(selectedItem) => {
          return (
            <View style={Styles.dropdownItemContainer}>
              <View style={Styles.dropdownLabelContainer}>
                <Text style={Styles.dropdownLabelText}>
                  {selectedItem?.label ?? props.displayLabel ?? 'Select'}
                </Text>
              </View>
              <View style={Styles.dropdownIconContainer}>
                <FontAwesomeIcons name="caret-down" size={16} color={'white'} />
              </View>
            </View>
          );
        }}
        rowStyle={{borderBottomWidth: 0}}
        rowTextForSelection={(item) => item?.label}
        rowTextStyle={Styles.dropdownItemText}
        selectedRowTextStyle={Styles.dropdownSelectedItemText}
        defaultValue={props.selectedValue}
      />
    </View>
  );
};

export default FloatingTitleSelect;

const Styles = StyleSheet.create({
  container: {
    width: '100%',
    marginVertical: 8,
  },
  selectfieldvalue: {
    borderBottomColor: 'white',
    borderBottomWidth: 1,
    paddingBottom: 8,
  },
  pickerItemStyle: {
    height: 36,
    fontSize: Platform.OS == 'ios' ? 12 : 14,
    fontWeight: '400',
    color: 'white',
    textAlign: 'left',
    left: Platform.OS == 'ios' ? -8 : -4,
  },
  dropdownWrapper: {
    width: '100%',
    height: Platform.OS == 'ios' ? 36 : 48,
    borderBottomColor: 'white',
    borderBottomWidth: 1,
    backgroundColor: 'transparent',
  },
  dropdownItemContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  dropdownLabelContainer: {
    flex: 0.9,
    justifyContent: 'center',
  },
  dropdownLabelText: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    fontWeight: '400',
    color: 'white',
    textAlign: 'left',
  },
  dropdownIconContainer: {
    flex: 0.1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  dropdownItemText: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    fontWeight: '400',
    color: 'black',
    textAlign: 'left',
  },
  dropdownSelectedItemText: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    fontWeight: '400',
    color: theme.secondryColor,
    textAlign: 'left',
  },
  titleStyles: {
    fontFamily: 'Roboto',
    color: '#ffffffc9',
    left: 4,
  },
});
