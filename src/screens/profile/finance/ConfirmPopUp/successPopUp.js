import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import Modal from 'react-native-modal';
import styles from './styles';

const SuccessPopUp = ({modal, onBackdropPress}) => {
  const navigation = useNavigation();

  const submit = () => {
    onBackdropPress(false);
    navigation.navigate('NewRegister');
  };

  return (
    <Modal
      animationType="fade"
      transparent={true}
      isVisible={modal}
      onBackdropPress={onBackdropPress}
      backdropOpacity={0.7}
      useNativeDriver={true}>
      <View style={styles.modalView}>
        <Text style={styles.questionTxt}>
          Are you sure that you want to continue?
        </Text>

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-evenly',
            marginTop: 16,
            marginBottom: 8,
          }}>
          <TouchableOpacity style={styles.btnView} onPress={submit}>
            <Text style={styles.btnTxt}>Yes</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.btnView}
            onPress={() => {
              onBackdropPress(false);
            }}>
            <Text style={styles.btnTxt}>No</Text>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
};

export default SuccessPopUp;
