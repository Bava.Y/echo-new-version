import AsyncStorage from '@react-native-community/async-storage';
import QueryString from 'query-string';
import axiosInstance from '../utils/Apicall';
import {TOKEN} from '../utils/constants';

const getUserData = async (id, token) => {
  let response = await axiosInstance.get(`customer/${id}`, {
    headers: {
      Authorization: token,
    },
  });
  try {
    if (response && response.data && response.data.keyword === 'success') {
      return response.data.data;
    } else {
      return response.data;
    }
  } catch (err) {
    return response.data.message;
  }
};

const updateBio = async (token, body) => {
  try {
    let response = await axiosInstance.post(`customer/bioUpdate`, body, {
      headers: {
        Authorization: token,
        'Content-Type': 'multipart/form-data',
      },
    });
    if (response && response.data && response.data.keyword === 'success') {
      return response.data.data;
    } else {
      return response.data.data;
    }
  } catch (err) {
    return response.data.message;
  }
};

const updateBasicInfo = async (token, body) => {
  try {
    let response = await axiosInstance.post(`customer/basicUpdate`, body, {
      headers: {
        Authorization: token,
        'Content-Type': 'multipart/form-data',
      },
    });
    if (response && response.data && response.data.keyword === 'success') {
      return response.data.data;
    } else {
      return response.data.message;
    }
  } catch (err) {
    return false;
  }
};

const getSecondaryInv = async (token) => {
  try {
    let response = await axiosInstance.get(`getSecondaryInv`, {
      headers: {
        Authorization: token,
      },
    });
    if (response && response.data && response.data.keyword === 'success') {
      return response.data.data;
    } else {
      return response.data.message;
    }
  } catch (err) {
    return response.data.message;
  }
};

const getSecondaryInvSubCatergory = async (id, token) => {
  let response = await axiosInstance.get(`getSecondarySubCategory/${id}`, {
    headers: {
      Authorization: token,
    },
  });
  try {
    if (response && response.data && response.data.keyword === 'success') {
      return response.data.data;
    } else {
      return response.data;
    }
  } catch (err) {
    return response.data.message;
  }
};

const getPrimaryInv = async (token) => {
  try {
    let response = await axiosInstance.get(`getPrimaryInv`, {
      headers: {
        Authorization: token,
      },
    });
    if (response && response.data && response.data.keyword === 'success') {
      return response.data.data;
    } else {
      return response.data.message;
    }
  } catch (err) {
    return response.data.message;
  }
};

const updatePrimaryInvestment = async (token, body) => {
  try {
    let response = await axiosInstance.post(
      `customer/primaryInvestUpdate`,
      body,
      {
        headers: {
          Authorization: token,
          'Content-Type': 'multipart/form-data',
        },
      },
    );
    if (response && response.data && response.data.keyword === 'success') {
      return response.data.data;
    } else {
      return response.data.message;
    }
  } catch (err) {
    return response.message;
  }
};

const updateSecondaryInvestment = async (token, body) => {
  try {
    let response = await axiosInstance.post(
      `customer/secondaryInvestUpdate`,
      body,
      {
        headers: {
          Authorization: token,
          'Content-Type': 'multipart/form-data',
        },
      },
    );
    if (response && response.data && response.data.keyword === 'success') {
      return response.data.data;
    } else {
      return response.data.message;
    }
  } catch (err) {
    return false;
  }
};

const updatePreference = async (token, body) => {
  try {
    let response = await axiosInstance.post(`customerPreference/update`, body, {
      headers: {
        Authorization: token,
        'Content-Type': 'multipart/form-data',
      },
    });
    if (response && response.data && response.data.keyword === 'success') {
      return response.data.data;
    } else {
      return response.data.message;
    }
  } catch (err) {
    return response.message;
  }
};

const updateStatus = async (token, body) => {
  try {
    let response = await axiosInstance.post(`customer/overall_update`, body, {
      headers: {
        Authorization: token,
        'Content-Type': 'multipart/form-data',
      },
    });
    if (response && response.data && response.data.keyword === 'success') {
      return response.data.data;
    } else {
      return response.data.message;
    }
  } catch (err) {
    return response.message;
  }
};

const expirationChecking = async (token, body) => {
  try {
    let response = await axiosInstance.post(`chk_expired`, body, {
      headers: {
        Authorization: token,
        'Content-Type': 'multipart/form-data',
      },
    });
    if (response && response.data && response.data.keyword === 'success') {
      return response.data.data;
    } else {
      return response.data.message;
    }
  } catch (err) {
    return response.message;
  }
};

const getPreference = async (token, id) => {
  try {
    let response = await axiosInstance.get(`customerPreference/${id}`, {
      headers: {
        Authorization: token,
      },
    });
    if (response && response.data && response.data.keyword === 'success') {
      return response.data.data;
    } else {
      return response.data.message;
    }
  } catch (err) {
    return response.message;
  }
};

const getIndustry = async (token) => {
  try {
    let response = await axiosInstance.get(`loadIndustry`, {
      headers: {
        Authorization: token,
      },
    });
    if (response && response.data && response.data.keyword === 'success') {
      return response.data.data;
    } else {
      return response.data.message;
    }
  } catch (err) {
    return response.message;
  }
};

const getCompany = async (token, id) => {
  try {
    let response = await axiosInstance.get(`loadCompany/[${id}]`, {
      headers: {
        Authorization: token,
      },
    });
    if (response && response.data && response.data.keyword === 'success') {
      return response.data.data;
    } else {
      return response.data.message;
    }
  } catch (err) {
    return response.message;
  }
};

const performanceUpdate = async (token, body) => {
  try {
    let response = await axiosInstance.post(`performancePreference`, body, {
      headers: {
        Authorization: token,
        'Content-Type': 'multipart/form-data',
      },
    });
    if (response && response.data && response.data.keyword === 'success') {
      return response.data;
    } else {
      return response.data;
    }
  } catch (err) {
    return response.data;
  }
};

const getPermormance = async (token) => {
  let response = await axiosInstance.get(`performancePreference`, {
    headers: {
      Authorization: token,
    },
  });
  try {
    if (response && response.data && response.data.keyword === 'success') {
      return response.data.data;
    } else {
      return response.data.message;
    }
  } catch (err) {
    return response.data.message;
  }
};

const updateBankDetail = async (token, body) => {
  let response = await axiosInstance.post(`updateBankDetail`, body, {
    headers: {
      Authorization: token,
      'Content-Type': 'multipart/form-data',
    },
  });
  try {
    if (response && response.data && response.data.keyword === 'success') {
      return response.data.data;
    } else {
      return response.data.message;
    }
  } catch (err) {
    return response.message;
  }
};

const updateBroker = async (token, body) => {
  let response = await axiosInstance.post(`broker_connection_update`, body, {
    headers: {
      Authorization: token,
      'Content-Type': 'multipart/form-data',
    },
  });
  try {
    if (response && response.data && response.data.keyword === 'success') {
      return response.data;
    } else {
      return response.data.message;
    }
  } catch (err) {
    return response.message;
  }
};

const updatePlaid = async (token, body) => {
  let response = await axiosInstance.post(`updatePlaidConnection`, body, {
    headers: {
      Authorization: token,
      'Content-Type': 'multipart/form-data',
    },
  });
  try {
    if (response && response.data && response.data.keyword === 'success') {
      return response.data;
    } else {
      return response.data.message;
    }
  } catch (err) {
    return response.message;
  }
};

const getBankDetail = async (token) => {
  let response = await axiosInstance.get(`getBankDetail`, {
    headers: {
      Authorization: token,
    },
  });
  try {
    if (response && response.data && response.data.keyword === 'success') {
      return response.data;
    } else {
      return response.data;
    }
  } catch (err) {
    return response.data.message;
  }
};

const updateInvestment = async (token,body) => {
 // let token = await AsyncStorage.getItem(TOKEN);
  try {
    let response = await axiosInstance.post(`customer/investUpdate`, body, {
      headers: {
        Authorization: token,
        'Content-Type': 'multipart/form-data',
      },
    });
    if (response && response.data && response.data.keyword === 'success') {
      return response.data;
    } else {
      return response.data;
    }
  } catch (err) {
    return response.message;
  }
};

const getInvestment = async (token) => {
  let response = await axiosInstance.get(`investData`, {
    headers: {
      Authorization: token,
    },
  });
  try {
    if (response && response.data && response.data.keyword === 'success') {
      return response.data;
    } else {
      return response.data;
    }
  } catch (err) {
    return response.data.message;
  }
};

const getTransactionList = async (token) => {
  let response = await axiosInstance.get(`transaction_list`, {
    headers: {
      Authorization: token,
    },
  });
  try {
    if (response && response.data && response.data.keyword === 'success') {
      return response.data;
    } else {
      return response.data;
    }
  } catch (err) {
    return response.data.message;
  }
};

const getTrackingError = async (token) => {
  let response = await axiosInstance.get(`tracking`, {
    headers: {
      Authorization: token,
    },
  });
  try {
    if (response && response.data && response.data.keyword === 'success') {
      return response.data;
    } else {
      return response.data.message;
    }
  } catch (err) {
    return response.data.message;
  }
};

const updateTracking = async (token, body) => {
  try {
    let response = await axiosInstance.post(`tracking/create`, body, {
      headers: {
        Authorization: token,
        'Content-Type': 'multipart/form-data',
      },
    });
    if (response && response.data && response.data.keyword === 'success') {
      return response.data;
    } else {
      return response.data;
    }
  } catch (err) {
    return response.message;
  }
};

const fundDeposit = async (token, body) => {
  try {
    let response = await axiosInstance.post(`fund/deposit`, body, {
      headers: {
        Authorization: token,
        'Content-Type': 'multipart/form-data',
      },
    });
    if (response && response.data && response.data.keyword === 'success') {
      return response.data;
    } else {
      return response.data;
    }
  } catch (err) {
    return response.message;
  }
};

const postDisallowedTrade = async (token, body) => {
  try {
    let response = await axiosInstance.post(`disAllowedTrade`, body, {
      headers: {
        Authorization: token,
        'Content-Type': 'multipart/form-data',
      },
    });
    if (response && response.data && response.data.keyword === 'success') {
      return response.data;
    } else {
      return response.data;
    }
  } catch (err) {
    return response.message;
  }
};

const getDisallowedTrade = async (token) => {
  let response = await axiosInstance.get(`getDisallowedTrade`, {
    headers: {
      Authorization: token,
    },
  });
  try {
    if (response && response.data && response.data.keyword === 'success') {
      return response.data;
    } else {
      return response.data;
    }
  } catch (err) {
    return response.data.message;
  }
};

const createACH = async (body) => {
  let token = await AsyncStorage.getItem(TOKEN);

  let response = await axiosInstance.post(
    `alpaca_ach_relationships`,
    QueryString.stringify(body),
    {
      headers: {
        Authorization: token,
        'Content-Type': 'multipart/form-data',
      },
    },
  );

  try {
    if (response && response.data && response.data.keyword === 'success') {
      return response.data;
    } else {
      return response.data;
    }
  } catch (err) {
    return response.data.message;
  }
};

const createWire = async (body) => {
  let token = await AsyncStorage.getItem(TOKEN);

  let response = await axiosInstance.post(
    `alpaca_recipient_banks`,
    QueryString.stringify(body),
    {
      headers: {
        Authorization: token,
        'Content-Type': 'multipart/form-data',
      },
    },
  );

  try {
    if (response && response.data && response.data.keyword === 'success') {
      return response.data;
    } else {
      return response.data;
    }
  } catch (err) {
    return response.data.message;
  }
};

const getACHRelationships = async () => {
  let token = await AsyncStorage.getItem(TOKEN);

  let response = await axiosInstance.get(`alpaca_ach_relationships`, {
    headers: {
      Authorization: token,
    },
  });

  try {
    if (response && response.data && response.data.keyword === 'success') {
      return response.data;
    } else {
      return response.data;
    }
  } catch (err) {
    return response.data.message;
  }
};

const getWireRelationships = async () => {
  let token = await AsyncStorage.getItem(TOKEN);

  let response = await axiosInstance.get(`alpaca_recipient_banks`, {
    headers: {
      Authorization: token,
    },
  });

  try {
    if (response && response.data && response.data.keyword === 'success') {
      return response.data;
    } else {
      return response.data;
    }
  } catch (err) {
    return response.data.message;
  }
};

const cancelRelationShip = async (endpoint, relationshipId) => {
  let token = await AsyncStorage.getItem(TOKEN);

  let response = await axiosInstance.get(`${endpoint}/${relationshipId}`, {
    headers: {
      Authorization: token,
    },
  });

  try {
    if (response && response.data && response.data.keyword === 'success') {
      return response.data;
    } else {
      return response.data;
    }
  } catch (err) {
    return response.data.message;
  }
};

const transferAmount = async (body) => {
  let token = await AsyncStorage.getItem(TOKEN);

  let response = await axiosInstance.post(
    `alpaca_transfers`,
    QueryString.stringify(body),
    {
      headers: {
        Authorization: token,
        'Content-Type': 'multipart/form-data',
      },
    },
  );

  try {
    if (response && response.data && response.data.keyword === 'success') {
      return response.data;
    } else {
      return response.data;
    }
  } catch (err) {
    return response.data.message;
  }
};

const getTransactionHistory = async () => {
  let token = await AsyncStorage.getItem(TOKEN);

  let response = await axiosInstance.get(`alpaca_transfers`, {
    headers: {
      Authorization: token,
    },
  });

  try {
    if (response && response.data && response.data.keyword === 'success') {
      return response.data;
    } else {
      return response.data;
    }
  } catch (err) {
    return response.data.message;
  }
};

const getPortfolio = async () => {
  let token = await AsyncStorage.getItem(TOKEN);

  let response = await axiosInstance.get(`financial_details`, {
    headers: {
      Authorization: token,
    },
  });

  try {
    if (response && response.data && response.data.keyword === 'success') {
      return response.data;
    } else {
      return response.data;
    }
  } catch (err) {
    return response.data.message;
  }
};

export const profile = {
  getUserData,
  updateBio,
  updateBasicInfo,
  getSecondaryInv,
  getPrimaryInv,
  getSecondaryInvSubCatergory,
  updatePrimaryInvestment,
  updateSecondaryInvestment,
  updatePreference,
  getPreference,
  updateStatus,
  expirationChecking,
  getIndustry,
  getCompany,
  performanceUpdate,
  getPermormance,
  updateBankDetail,
  updateBroker,
  getBankDetail,
  updateInvestment,
  getInvestment,
  getTransactionList,
  getTrackingError,
  updateTracking,
  fundDeposit,
  getDisallowedTrade,
  postDisallowedTrade,
  createACH,
  createWire,
  getACHRelationships,
  getWireRelationships,
  cancelRelationShip,
  transferAmount,
  getTransactionHistory,
  getPortfolio,
  updatePlaid,
};
