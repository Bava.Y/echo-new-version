import moment from 'moment';
import React, {useContext, useEffect, useState} from 'react';
import {
  ActivityIndicator,
  Dimensions,
  LogBox,
  Modal,
  Platform,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Snackbar from 'react-native-snackbar';
import {
  VictoryArea,
  VictoryAxis,
  VictoryChart,
  VictoryTooltip,
  VictoryVoronoiContainer,
} from 'victory-native';
import {dashboard} from '../../api';
import {UserDataContext} from '../../context/UserDataContext';
import theme from '../../utils/theme';
import styles from './style';
import {
  CandlestickChart,
  LineChart,
  LineChartCursorCrosshair,
  LineChartCursorLine,
  LineChartCursorProps,
} from 'react-native-wagmi-charts';
import {GestureHandlerRootView} from 'react-native-gesture-handler';

LogBox.ignoreLogs(['Require cycle:']);

const CompanyChart = ({symbol}) => {
  // CompanyChart Variables
  const [chartData, setChartData] = useState(null);
  const [range, setRange] = useState('1m');
  console.log('chartData>>>>>', chartData?.data);
  // Context Variables
  const {token} = useContext(UserDataContext);

  // Other Variables
  const [modalVisible, setModalVisible] = useState(false);

  const LIGHT_GREY = 'hsl(355, 20%, 90%)';

  const deviceWidth = Dimensions.get('window').width;
  console.log('deviceWidth>>>>>', deviceWidth);
  useEffect(() => {
    let ac = new AbortController();

    setChartData(null);

    getCompanyReports();

    return () => {
      ac.abort();
    };
  }, [range]);

  const getCompanyReports = async () => {
    const res = await dashboard.getDashBoardQuote(symbol, range);

    if (res) {
      if (res.keyword == 'success') {
        setChartData(res?.data?.chartdata);
        // let filtered = [];

        // res.data.chartdata
        //   .filter((x) => x.price != 0)
        //   .map((data) => {
        //     filtered.push({x: data.date, y: parseFloat(data.price)});
        //   });

        // const addPercent = (val) => {
        //   let twoPercent = (val * 2) / 100;
        //   return twoPercent;
        // };

        // let max = Math.max.apply(
        //   Math,
        //   filtered.map(function (o) {
        //     return o.y;
        //   }),
        // );

        // let min = Math.min.apply(
        //   Math,
        //   filtered.map(function (o) {
        //     return o.y;
        //   }),
        // );

        // const addedmax = max + addPercent(max);
        // const addedmin = min - addPercent(min);

        // setChartData({
        //   data: filtered,
        //   max: addedmax,
        //   min: addedmin,
        // });
      } else {
        Snackbar.show({
          duration: Snackbar.LENGTH_SHORT,
          text: res.message,
          backgroundColor: 'red',
        });

        setChartData(
          [],
          // data: [],
          // max: 1,
          // min: 0,
        );
      }
    } else {
      setChartData(
        [],
        // data: [],
        // max: 1,
        // min: 0,
      );
    }
  };

  const changeTF = val => {
    setRange(val);
  };

  return (
    <>
      <View style={styles.chartdetail}>
        {chartData ? (
          <>
            {chartData?.data?.length != 0 ? (
              <GestureHandlerRootView>
                <LineChart.Provider data={chartData}>
                  <LineChart width={deviceWidth * 0.85}>
                    <LineChart.Path color={theme.secondryColor} width={1.5}>
                      <LineChart.Tooltip
                        textStyle={{color: 'white', fontSize: 13}}
                      />
                      <LineChart.Gradient />
                    </LineChart.Path>
                    <LineChart.CursorLine color="white" />

                    <LineChart.Tooltip position="bottom">
                      <LineChart.DatetimeText
                        style={{
                          borderRadius: 4,
                          color: 'white',
                          fontSize: 13,
                          padding: 4,
                        }}
                      />
                    </LineChart.Tooltip>
                  </LineChart>
                </LineChart.Provider>
              </GestureHandlerRootView>
            ) : (
              // <VictoryChart
              //   height={200}
              //   padding={{top: 0, bottom: 0, left: 0, right: 88}}
              //   maxDomain={{y: chartData.max}}
              //   minDomain={{y: chartData.min}}
              //   containerComponent={
              //     <VictoryVoronoiContainer
              //       voronoiDimension="x"
              //       labels={({datum}) =>
              //         `$ ${
              //           Boolean(datum?.y ?? null)
              //             ? parseFloat(datum.y).toFixed(2)
              //             : '0.00'
              //         }${
              //           Boolean(datum?.x ?? null)
              //             ? ` | ${moment(datum.x).format('ll hh:mm A')}`
              //             : ''
              //         }`
              //       }
              //       labelComponent={
              //         <VictoryTooltip
              //           flyoutPadding={12}
              //           center={{
              //             x: deviceWidth * 0.375,
              //             y: deviceWidth * 0.125,
              //           }}
              //         />
              //       }
              //     />
              //   }>
              //   <VictoryAxis
              //     orientation="right"
              //     dependentAxis
              //     style={{
              //       axis: {
              //         stroke: 'transparent',
              //       },
              //       tickLabels: {
              //         fill: LIGHT_GREY,
              //         fontSize: 10,
              //       },
              //       axisLabel: {
              //         fill: 'red',
              //         fontSize: 10,
              //         fontStyle: 'italic',
              //       },
              //       grid: {
              //         fill: LIGHT_GREY,
              //         stroke: LIGHT_GREY,
              //         strokeWidth: 0.1,
              //       },
              //     }}
              //   />

              //   <VictoryArea
              //     interpolation="natural"
              //     style={{
              //       data: {
              //         fill: theme.secondryColor,
              //         fillOpacity: 0.5,
              //         stroke: theme.secondryColor,
              //         strokeWidth: 2,
              //       },
              //     }}
              //     data={chartData.data}
              //   />
              // </VictoryChart>
              <View
                style={{
                  height: 200,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text style={{color: theme.white}}>
                  No chart preview available
                </Text>
              </View>
            )}

            <View style={styles.dayview}>
              <TouchableOpacity onPress={() => changeTF('1d')}>
                <Text style={range === '1d' ? styles.active : styles.day}>
                  1D
                </Text>
              </TouchableOpacity>

              <TouchableOpacity onPress={() => changeTF('7d')}>
                <Text style={range === '7d' ? styles.active : styles.day}>
                  1W
                </Text>
              </TouchableOpacity>

              <TouchableOpacity onPress={() => changeTF('1m')}>
                <Text style={range === '1m' ? styles.active : styles.day}>
                  1M
                </Text>
              </TouchableOpacity>

              <TouchableOpacity onPress={() => changeTF('6m')}>
                <Text style={range === '6m' ? styles.active : styles.day}>
                  6M
                </Text>
              </TouchableOpacity>

              <TouchableOpacity onPress={() => changeTF('1y')}>
                <Text style={range === '1y' ? styles.active : styles.day}>
                  1Y
                </Text>
              </TouchableOpacity>

              <TouchableOpacity onPress={() => changeTF('5y')}>
                <Text style={range === '5y' ? styles.active : styles.day}>
                  5Y
                </Text>
              </TouchableOpacity>

              <TouchableOpacity onPress={() => changeTF('max')}>
                <Text style={range === 'max' ? styles.active : styles.day}>
                  MAX
                </Text>
              </TouchableOpacity>
            </View>
          </>
        ) : (
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              height: 230,
            }}>
            <ActivityIndicator size="large" color="white" />
          </View>
        )}

        <View style={styles.bottomiconsec}>
          <View style={{width: '30%', justifyContent: 'center'}}>
            <TouchableOpacity
              style={{
                alignSelf: 'flex-start',
                backgroundColor: theme.secondryColor,
                paddingHorizontal: 16,
                paddingVertical: 8,
              }}
              onPress={() => {
                setModalVisible(!modalVisible);
              }}>
              <Text style={styles.icontext}>VM</Text>
            </TouchableOpacity>
          </View>

          <View style={{width: '40%', justifyContent: 'center'}}>
            <TouchableOpacity
              style={{
                alignSelf: 'center',
                backgroundColor: theme.secondryColor,
                paddingHorizontal: 16,
                paddingVertical: 8,
              }}
              onPress={() => {
                setModalVisible(!modalVisible);
              }}>
              <Text style={styles.icontext}>SEC EDGAR</Text>
            </TouchableOpacity>
          </View>

          <View style={{width: '30%', justifyContent: 'center'}}>
            <TouchableOpacity
              style={{
                alignSelf: 'flex-end',
                backgroundColor: theme.secondryColor,
                paddingHorizontal: 16,
                paddingVertical: 8,
              }}
              onPress={() => {
                setModalVisible(!modalVisible);
              }}>
              <Text style={styles.icontext}>E</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>

      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(!modalVisible);
        }}>
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View
            style={{
              width: '75%',
              backgroundColor: theme.primaryColor,
              borderColor: theme.secondryColor,
              borderWidth: 1,
              borderRadius: 8,
              margin: 16,
              padding: 16,
              shadowColor: '#000',
              shadowOffset: {
                width: 0,
                height: 2,
              },
              shadowOpacity: 0.25,
              shadowRadius: 4,
              elevation: 4,
            }}>
            <Text
              style={{
                fontSize: Platform.OS == 'ios' ? 14 : 16,
                color: theme.white,
                fontWeight: 'bold',
                textAlign: 'center',
              }}>
              This feature is comming soon
            </Text>

            <TouchableOpacity
              style={{
                alignSelf: 'flex-end',
                backgroundColor: theme.secondryColor,
                borderRadius: 5,
                marginTop: 16,
                paddingHorizontal: 8,
                paddingVertical: 4,
              }}
              onPress={() => {
                setModalVisible(!modalVisible);
              }}>
              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 12 : 14,
                  color: theme.white,
                  textAlign: 'center',
                }}>
                Close
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    </>
  );
};

export default CompanyChart;
