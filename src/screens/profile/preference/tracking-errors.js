import React, {useContext, useEffect, useState} from 'react';
import {
  Dimensions,
  Keyboard,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {profile} from '../../../api';
import Loading from '../../../components/Loading';
import {UserDataContext} from '../../../context/UserDataContext';
import theme from '../../../utils/theme';
import SuccessPopUp from '../components/successPopUp/successPopUp';
import styles from './styles';

const PreferencesScreen = () => {
  // PreferencesScreen Variables
  const [percentage, setPercentage] = useState(undefined);
  const [res, setRes] = useState(undefined);

  // Context Variables
  const {token} = useContext(UserDataContext);

  // Other Variables
  const [loading, setLoading] = useState(false);
  const [modal, setModal] = useState(false);
  const [successText, setSuccessText] = useState('');
  const windowHeight = Dimensions.get('window').height;

  useEffect(() => {
    getTrackingError();
  }, []);

  const getTrackingError = async () => {
    setLoading(true);

    const response = await profile.getTrackingError(token);

    setPercentage(String(response.data.tracking_error));

    setLoading(false);
  };

  const submitForm = async () => {
    Keyboard.dismiss();

    let formData = new FormData();

    formData && formData.append('tracking_error', Number(percentage));

    if (formData != undefined) {
      setModal(true);

      setSuccessText('Tracking error updated');

      let response = await profile.updateTracking(token, formData);

      setRes(response);

      setTimeout(() => {
        setRes(undefined);
      }, 1500);

      setTimeout(() => {
        setModal(false);
      }, 1000);
    }
  };

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : null}
      keyboardVerticalOffset={Platform.OS === 'ios' ? 40 : 0}
      style={styles.container}>
      {!loading ? (
        <ScrollView
          keyboardShouldPersistTaps={'handled'}
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}>
          <View style={styles.mainsec}>
            <Text style={styles.headingTxt}>Tracking Errors</Text>

            <Text style={[styles.paraTxt, {marginTop: 8}]}>
              When you echo another user, issues like rounding and disallowed
              trades can mean that your portfolio may deviate from the person
              you are echoing. 
            </Text>

            <Text style={[styles.paraTxt, {marginTop: 8}]}>
             The Tracking Error is a measure of how much your
              portfolio deviates from the person you are Echoing. Once the
              threshold you set is reached, you will be notified so that you may
              decide whether to rebalance your portfolio or not.
            </Text>

            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginTop: 16,
              }}>
              <View style={{flex: 0.6, justifyContent: 'center'}}>
                <Text
                  style={{
                    fontSize: Platform.OS == 'ios' ? 12 : 14,
                    color: theme.white,
                    fontWeight: '600',
                    lineHeight: 16,
                  }}>
                  You may change the percentage to
                </Text>
              </View>

              <View
                style={{
                  flex: 0.175,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <TextInput
                  keyboardType={'number-pad'}
                  style={styles.textinput}
                  onChangeText={(text) => setPercentage(text)}
                  value={percentage}
                />
              </View>

              <View
                style={{
                  flex: 0.225,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <TouchableOpacity onPress={submitForm}>
                  <View style={styles.btnview}>
                    <Text style={styles.btntxt}>Update</Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </View>

          <View style={{paddingVertical: 12}} />
        </ScrollView>
      ) : (
        <View
          style={{
            height: windowHeight * 0.5625,
            backgroundColor: theme.themeColor,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Loading color="white" />
        </View>
      )}

      <SuccessPopUp
        res={res}
        successText={successText}
        modal={modal}
        onBackdropPress={() => setModal(false)}
      />
    </KeyboardAvoidingView>
  );
};

export default PreferencesScreen;
