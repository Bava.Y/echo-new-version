import {debounce} from 'lodash';
import React, {useContext, useRef, useState} from 'react';
import {
  ActivityIndicator,
  Keyboard,
  Platform,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import Snackbar from 'react-native-snackbar';
import Icon from 'react-native-vector-icons/FontAwesome';
import {dashboard} from '../../api';
import {UserDataContext} from '../../context/UserDataContext';
import styles from './styles';

const Search = ({req, onFocus, onBlur, companyChanging}) => {
  const [searchlist, SetSearchlist] = useState([]);
  const [show, setShow] = useState(false);
  const [value, setValue] = useState();
  const [loading, setLoading] = useState(false);
  const {token} = useContext(UserDataContext);
  const inputRef = useRef();

  const initSearch = debounce(async (query) => {
    SetSearchlist([]);

    if (query !== '') {
      setLoading(true);

      const res = await dashboard.searchStock(query);

      if (res) {
        if (res.keyword == 'success') {
          let filtered = [];

          res.data.map((o) => {
            let result = {
              symbol: o.symbol,
              name: o.securityName,
            };
            filtered.push(result);
          });

          SetSearchlist(filtered);
          setLoading(false);
        } else {
          SetSearchlist([]);
          setLoading(false);
        }
      } else {
      }
    }
  }, 500);

  const processResult = (symbol) => {
    setShow(false);

    req && req(symbol);

    setValue(undefined);

    inputRef.current.clear();
  };

  const changeText = (text) => {
    if (text.length > 0) {
      setShow(true);
      companyChanging && companyChanging();
    }

    initSearch(text);
  };

  return (
    <>
      <View
        style={[
          styles.searchSection1,
          Boolean(searchlist) &&
            Array.isArray(searchlist) &&
            searchlist.length != 0 && {
              borderBottomLeftRadius: 0,
              borderBottomRightRadius: 0,
            },
        ]}>
        <Icon style={styles.searchIcon1} name="search" size={16} color="grey" />

        <TextInput
          ref={inputRef}
          style={styles.input1}
          placeholder="Search"
          placeholderTextColor="grey"
          onChangeText={(text) => changeText(text)}
          onFocus={() => {
            setShow(false);

            onFocus();

            setValue(undefined);

            SetSearchlist([]);

            inputRef.current.clear();
          }}
          onBlur={() => {
            onBlur();
          }}
          value={value}
        />

        {value && (
          <Icon
            onPress={() => {
              setShow(false);

              setValue(undefined);

              SetSearchlist([]);

              inputRef.current.clear();
            }}
            style={styles.searchIcon1}
            name="arrow-left"
            size={16}
            color="grey"
          />
        )}
      </View>

      {show && (
        <View style={styles.searchlist}>
          {loading ? (
            <ActivityIndicator
              color="white"
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
              }}
            />
          ) : searchlist.length != 0 ? (
            <ScrollView
              keyboardShouldPersistTaps="handled"
              nestedScrollEnabled={true}
              showsVerticalScrollIndicator={false}>
              {searchlist.map((item, index) => {
                return (
                  <TouchableOpacity
                    key={index.toString()}
                    onPressIn={() => {
                      Keyboard.dismiss();
                    }}
                    onPressOut={() => {
                      processResult(item.symbol);
                    }}>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        alignItems: 'center',
                        paddingVertical: 8,
                        paddingHorizontal: 16,
                      }}>
                      <View style={styles.searchdata}>
                        <Text
                          style={{
                            fontSize: Platform.OS == 'ios' ? 12 : 14,
                            fontWeight: '600',
                            color: 'white',
                            textAlign: 'left',
                          }}
                          numberOfLines={1}
                          ellipsizeMode="tail">
                          {item.name}
                        </Text>
                      </View>

                      <View style={styles.searchsymbol}>
                        <Text
                          style={{
                            fontSize: Platform.OS == 'ios' ? 12 : 14,
                            fontWeight: '600',
                            color: 'grey',
                            textAlign: 'right',
                          }}>
                          {item.symbol}
                        </Text>
                      </View>
                    </View>
                  </TouchableOpacity>
                );
              })}
            </ScrollView>
          ) : (
            <View
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 12 : 14,
                  color: 'white',
                }}>
                No Data Found
              </Text>
            </View>
          )}
        </View>
      )}
    </>
  );
};

export default Search;
