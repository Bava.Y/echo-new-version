import {useFocusEffect} from '@react-navigation/native';
import React, {useCallback, useContext, useRef, useState} from 'react';
import {
  Image,
  Platform,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Snackbar from 'react-native-snackbar';
import {portfolio} from '../api/index';
import {ALPACA, AMERITRADE} from '../assets/images/index';
import BottomNav from '../components/BottomNav';
import HeaderNav from '../components/HeaderNav';
import Loading from '../components/Loading';
import {UserDataContext} from '../context/UserDataContext';
import theme from '../utils/theme';
import OrderShareAmount from './Dashboard/order-amount';
import OrderShareQuantity from './Dashboard/order-quantity';

const DisallowedTrades = (props) => {
  // Props Variables
  const id = props.route.params.id;

  // DisallowedTrades Variables
  const [responseData, setResponseData] = useState(null);
  const [orderData, setOrderData] = useState(null);

  // Context Variables
  const {token} = useContext(UserDataContext);

  // Ref Variables
  const orderRef = useRef();

  useFocusEffect(
    useCallback(() => {
      let isFocus = true;

      getDisallowedTrades();

      return () => {
        isFocus = false;
      };
    }, []),
  );

  const getDisallowedTrades = async () => {
    const res = await portfolio.getDisallowedTrade(id, token);

    if (res) {
      if (res.keyword === 'success') {
        Snackbar.show({duration: Snackbar.LENGTH_SHORT, text: res.message});

        setResponseData(res.data ? res.data : []);
      } else {
        Snackbar.show({duration: Snackbar.LENGTH_SHORT, text: res.message});

        setResponseData([]);
      }
    } else {
      setResponseData([]);
    }
  };

  const isFloat = (n) => {
    return Number(n) % 1 !== 0;
  };

  return (
    <>
      <HeaderNav title="Disallowed Trades" isBack="true" />

      {responseData ? (
        responseData.length != 0 ? (
          <ScrollView
            keyboardShouldPersistTaps="handled"
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
            style={{
              flex: 1,
              backgroundColor: theme.themeColor,
              paddingBottom: 24,
            }}>
            <View style={{paddingHorizontal: 8, paddingVertical: 16}}>
              {responseData.map((lol, index) => {
                return (
                  <View key={index} style={{marginVertical: 8}}>
                    <Text
                      style={{
                        fontSize: Platform.OS == 'ios' ? 12 : 14,
                        color: theme.white,
                        fontWeight: '600',
                      }}>
                      Disallowed trade(s) due to{' '}
                      <Text
                        style={{
                          fontSize: Platform.OS == 'ios' ? 14 : 16,
                          color: theme.white,
                          fontWeight: '600',
                          textTransform: 'uppercase',
                        }}>
                        {lol.follower.name}
                      </Text>{' '}
                      : -
                    </Text>

                    {lol.shares.map((shareData, index) => (
                      <View
                        key={index}
                        style={{
                          flex: 1,
                          backgroundColor: theme.primaryColor,
                          borderRadius: 8,
                          marginVertical: 8,
                          padding: 8,
                        }}>
                        <View
                          style={{
                            flex: 1,
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            backgroundColor: theme.primaryColor,
                          }}>
                          <View
                            style={{
                              flex: 0.25,
                              justifyContent: 'center',
                              alignItems: 'center',
                            }}>
                            <Text
                              style={{
                                fontSize: Platform.OS == 'ios' ? 12 : 14,
                                fontWeight: 'bold',
                                color: theme.white,
                                textTransform: 'uppercase',
                              }}>
                              {shareData.symbol}
                            </Text>
                          </View>

                          <View
                            style={{
                              flex: 0.25,
                              justifyContent: 'center',
                              alignItems: 'center',
                            }}>
                            <Text
                              style={{
                                fontSize: Platform.OS == 'ios' ? 10 : 12,
                                color: theme.white,
                              }}>
                              {parseFloat(shareData.quantity).toFixed(
                                isFloat(shareData.quantity) ? 2 : 0,
                              )}{' '}
                              Shares
                            </Text>
                          </View>

                          <View
                            style={{
                              flex: 0.25,
                              justifyContent: 'center',
                              alignItems: 'center',
                            }}>
                            <Image
                              source={
                                shareData.brokerConnection == 1
                                  ? AMERITRADE
                                  : ALPACA
                              }
                              style={{
                                width: 60,
                                height: 30,
                                resizeMode: 'contain',
                              }}
                            />
                          </View>

                          <View
                            style={{
                              flex: 0.25,
                              justifyContent: 'center',
                              alignItems: 'center',
                            }}>
                            <TouchableOpacity
                              style={{
                                width: '75%',
                                alignItems: 'center',
                                backgroundColor:
                                  shareData.type.toLowerCase() === 'buy'
                                    ? theme.successGreen
                                    : theme.danger,
                                borderRadius: 4,
                                paddingHorizontal: 8,
                                paddingVertical: 4,
                              }}
                              onPress={async () => {
                                const orderDetails = {
                                  brokerConnection: shareData.brokerConnection,
                                  latest_price: shareData.amount,
                                  qty: shareData.quantity,
                                  side: shareData.type,
                                  symbol: shareData.symbol.toUpperCase(),
                                  fromPage: 'Disallowed Trades',
                                  id: shareData.id,
                                  creatorId: shareData.creatorId,
                                  directUserTotalAmount:
                                    shareData.total_amount_direct_user,
                                  directUserPercentage:
                                    shareData.percentage_direct_user,
                                  fractionable: shareData.fractionable,
                                };

                                await setOrderData(orderDetails);

                                orderRef.current.open();
                              }}>
                              <Text
                                style={{
                                  fontSize: Platform.OS == 'ios' ? 10 : 12,
                                  fontWeight: 'bold',
                                  color: theme.white,
                                  textTransform: 'capitalize',
                                }}>
                                {shareData.type}
                              </Text>
                            </TouchableOpacity>
                          </View>
                        </View>

                        <View
                          style={{
                            backgroundColor: theme.themeColor,
                            borderRadius: 4,
                            marginHorizontal: 8,
                            marginVertical: 4,
                            padding: 4,
                          }}>
                          <Text
                            style={{
                              fontSize: Platform.OS == 'ios' ? 12 : 14,
                              fontWeight: '600',
                              color: theme.textColor,
                              lineHeight: 20,
                            }}>
                            Reason:{' '}
                            <Text
                              style={{
                                fontSize: Platform.OS == 'ios' ? 10 : 12,
                                fontWeight: '600',
                                color: theme.white,
                                lineHeight: 20,
                              }}>
                              {shareData.reason}
                            </Text>
                          </Text>
                        </View>
                      </View>
                    ))}
                  </View>
                );
              })}
            </View>
          </ScrollView>
        ) : (
          <View
            style={{
              flex: 1,
              backgroundColor: theme.themeColor,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontSize: Platform.OS == 'ios' ? 12 : 14,
                color: theme.white,
              }}>
              No disallowed trade(s) found
            </Text>
          </View>
        )
      ) : (
        <View style={{flex: 1, backgroundColor: theme.themeColor}}>
          <Loading color={theme.white} />
        </View>
      )}

      {orderData && orderData?.fractionable && (
        <OrderShareAmount
          buyform={orderRef}
          orderData={orderData}
          onChange={() => {
            getDisallowedTrades();

            orderRef.current.close();
          }}
        />
      )}

      {orderData && !orderData?.fractionable && (
        <OrderShareQuantity
          buyform={orderRef}
          orderData={orderData}
          onChange={() => {
            getDisallowedTrades();

            orderRef.current.close();
          }}
        />
      )}

      <BottomNav routeName="Discover People" />
    </>
  );
};

export default DisallowedTrades;
