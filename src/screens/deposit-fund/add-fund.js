import React, {useContext, useEffect, useState} from 'react';
import {
  ActivityIndicator,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import SelectDropdown from 'react-native-select-dropdown';
import Snackbar from 'react-native-snackbar';
import FontAwesomeIcons from 'react-native-vector-icons/FontAwesome5';
import {profile} from '../../api';
import Loading from '../../components/Loading';
import {UserDataContext} from '../../context/UserDataContext';
import axiosInstance from '../../utils/Apicall';
import theme from '../../utils/theme';
import BottomNav from './../../components/BottomNav';
import HeaderNav from './../../components/HeaderNav';

const AddDepositFund = ({navigation}) => {
  // AddDepositFund Variables
  const [ameritradeBalance, setAmeritradeBalance] = useState(0);
  const [alphacaBalance, setAlphacaBalance] = useState(0);
  const [selectedAccount, setSelectedAccount] = useState(null);
  const [depositAmount, setDepositAmount] = useState(null);

  // Error Variables
  const [accountError, setAccountError] = useState(false);
  const [depositAmountError, setDepositAmountError] = useState(false);

  // Other Variables
  const {token} = useContext(UserDataContext);
  const [loading, setLoading] = useState(true);
  const [payloading, setPayLoading] = useState(false);

  const accountOptions = [
    {label: 'Alpaca', value: '0'},
    {label: 'TD Amirtrade', value: '0'},
  ];

  useEffect(() => {
    getDepositData();

    // getBankDetail();
  }, []);

  const getDepositData = async () => {
    setLoading(true);

    let response = await axiosInstance.get('getDeposit', {
      headers: {
        Authorization: token,
      },
    });

    if (response && response.data && response.data.keyword === 'success') {
      setAlphacaBalance(response.data.data.alpaca_balance);

      setAmeritradeBalance(response.data.data.td_balance);

      setLoading(false);
    }
  };

  const validateFields = () => {
    let formData = new FormData();

    if (depositAmount != null || depositAmount > 0) {
      setDepositAmountError(false);

      formData && formData.append('amount', depositAmount);
    } else {
      setDepositAmountError(true);

      formData = undefined;
    }

    if (selectedAccount?.value ?? null) {
      setAccountError(false);

      formData &&
        formData.append('broker_connection', selectedAccount?.value ?? null);
    } else {
      setAccountError(true);

      formData = undefined;
    }

    formData && formData.append('credit_card_no', '424242424242');

    return formData;
  };

  const initPayment = async () => {
    setPayLoading(true);

    let formData = validateFields();

    if (formData) {
      const response = await profile.fundDeposit(token, formData);

      if (response.keyword === 'success' && response.data) {
        setSelectedAccount(null);
        setDepositAmount(null);

        Snackbar.show({
          text: response.message,
          duration: Snackbar.LENGTH_SHORT,
        });

        navigation.navigate('Fund');
      } else {
        Snackbar.show({
          text: 'Failed to deposit the fund',
          duration: Snackbar.LENGTH_LONG,
        });
      }
    }

    setPayLoading(false);
  };

  return (
    <>
      <View style={{flex: 1, backgroundColor: theme.themeColor}}>
        <HeaderNav title="Add Deposit Funds" isBack={true} />
        {loading ? (
          <Loading color="white" />
        ) : (
          <ScrollView>
            <View style={styles.containerWrapper}>
              <View style={styles.pageContainer}>
                <View>
                  <Text style={styles.heading}>
                    Which account would you like to choose?
                  </Text>
                  <SelectDropdown
                    buttonStyle={styles.dropdownWrapper}
                    data={accountOptions}
                    defaultButtonText="Broker"
                    dropdownStyle={{
                      width: '80%',
                      marginLeft: 8,
                      alignSelf: 'center',
                    }}
                    onSelect={(selectedItem) => {
                      accountError && setAccountError(false);

                      setSelectedAccount(selectedItem);
                    }}
                    renderCustomizedButtonChild={(selectedItem) => {
                      return (
                        <View style={styles.dropdownItemContainer}>
                          <View style={styles.dropdownLabelContainer}>
                            <Text style={styles.dropdownLabelText}>
                              {selectedItem?.label ?? 'Relationship Type'}
                            </Text>
                          </View>
                          <View style={styles.dropdownIconContainer}>
                            <FontAwesomeIcons
                              name="caret-down"
                              size={16}
                              color={'white'}
                            />
                          </View>
                        </View>
                      );
                    }}
                    rowStyle={{borderBottomWidth: 0}}
                    rowTextForSelection={(item) => item?.label}
                    rowTextStyle={styles.dropdownItemText}
                    selectedRowTextStyle={styles.dropdownSelectedItemText}
                    defaultValue={selectedAccount}
                  />
                  {accountError && (
                    <Text
                      style={{color: 'red', marginVertical: 4, marginLeft: 4}}>
                      Please! Choose any one account
                    </Text>
                  )}
                </View>
                <View style={{marginTop: 30}}>
                  <Text style={styles.heading}>
                    How much would you like to add?
                  </Text>
                  <View
                    style={{
                      flexDirection: 'row',
                      marginTop: 20,
                      marginLeft: 5,
                      borderBottomColor: 'white',
                      borderBottomWidth: 1,
                      width: '90%',
                    }}>
                    <Text
                      style={{color: 'white', marginTop: 5, marginRight: 5}}>
                      $
                    </Text>
                    <TextInput
                      keyboardType="number-pad"
                      value={depositAmount}
                      onChangeText={(text) => {
                        depositAmountError && setDepositAmountError(false);

                        setDepositAmount(text);
                      }}
                      style={{
                        width: '100%',
                        color: 'white',
                        paddingVertical: 0,
                      }}
                    />
                  </View>
                  {depositAmountError && (
                    <Text
                      style={{color: 'red', marginVertical: 4, marginLeft: 4}}>
                      Please add fund
                    </Text>
                  )}
                  {selectedAccount?.value && (
                    <Text
                      style={{color: 'white', marginLeft: 5, marginTop: 10}}>
                      Account balance - ${' '}
                      {selectedAccount?.value != 0
                        ? parseFloat(ameritradeBalance).toFixed(2)
                        : parseFloat(alphacaBalance).toFixed(2)}
                    </Text>
                  )}
                </View>
              </View>
              <TouchableOpacity
                onPress={initPayment}
                style={styles.continueButton}>
                <Text style={styles.continueButtonText}>
                  {payloading ? <ActivityIndicator color="white" /> : 'Deposit'}
                </Text>
              </TouchableOpacity>
            </View>
          </ScrollView>
        )}
      </View>
      <BottomNav routeName="Profile" />
    </>
  );
};

const styles = StyleSheet.create({
  containerWrapper: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    paddingTop: 10,
    paddingBottom: 30,
  },
  pageContainer: {
    paddingHorizontal: 10,
    paddingVertical: 20,
    borderRadius: 10,
    margin: 10,
    backgroundColor: theme.primaryColor,
  },
  heading: {
    color: 'white',
    fontWeight: 'bold',
    paddingLeft: 5,
  },
  option: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginLeft: 15,
    marginTop: 20,
    paddingHorizontal: 8,
    paddingVertical: 7,
    borderWidth: 1,
    borderColor: 'grey',
    borderRadius: 10,
  },
  optionSelected: {
    borderColor: theme.secondryColor,
  },
  radio: {
    backgroundColor: 'grey',
    marginTop: 4,
    width: 25,
    height: 25,
    borderRadius: 50,
  },
  radioOn: {
    backgroundColor: theme.secondryColor,
  },
  title: {
    color: 'white',
    fontSize: 16,
    marginTop: 5,
  },
  description: {
    color: 'white',
    marginTop: 5,
  },
  continueButton: {
    marginVertical: 10,
    marginBottom: 80,
    backgroundColor: theme.secondryColor,
    alignSelf: 'center',
    paddingVertical: 5,
    paddingHorizontal: 40,
    borderRadius: 50,
  },
  continueButtonText: {
    color: 'white',
    fontSize: 18,
    fontWeight: 'bold',
  },
  dropdownWrapper: {
    width: '100%',
    height: Platform.OS == 'ios' ? 36 : 48,
    borderBottomColor: 'white',
    borderBottomWidth: 1,
    backgroundColor: 'transparent',
    marginVertical: 8,
  },
  dropdownItemContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  dropdownLabelContainer: {
    flex: 0.9,
    justifyContent: 'center',
  },
  dropdownLabelText: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    fontWeight: '400',
    color: 'white',
    textAlign: 'left',
  },
  dropdownIconContainer: {
    flex: 0.1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  dropdownItemText: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    fontWeight: '400',
    color: 'black',
    textAlign: 'left',
  },
  dropdownSelectedItemText: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    fontWeight: '400',
    color: theme.secondryColor,
    textAlign: 'left',
  },
});

export default AddDepositFund;
