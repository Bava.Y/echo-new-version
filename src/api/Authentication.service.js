import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import jwt from 'jwt-decode';
import QueryString from 'query-string';
import axiosInstance from '../utils/Apicall';
import {IS_TWO_STEP, LOGIN_USER, MPIN, TOKEN} from '../utils/constants';

const login = async body => {
  let responce = await axiosInstance.post('mpinAssign', body, {
    headers: {
      'Content-Type': 'multipart/form-data',
    },
  });
  try {
    if (
      responce &&
      responce.data &&
      responce.data.keyword === 'success' &&
      responce.data.data
    ) {
      return responce.data.data;
    } else {
      return JSON.stringify(responce.data);
    }
  } catch (err) {
    return 'Something went wrong';
  }
};
const deviceChangeLogin = async body => {
  let responce = await axiosInstance.post('chk_continue', body, {
    headers: {
      'Content-Type': 'multipart/form-data',
    },
  });
  try {
    if (
      responce &&
      responce.data &&
      responce.data.keyword === 'success' &&
      responce.data.data
    ) {
      return responce.data.data;
    } else {
      return responce.data.message;
    }
  } catch (err) {
    return 'Something went wrong';
  }
};

const loginMpin = async body => {
  let responce = await axiosInstance.post('loginMpin', body, {
    headers: {
      'Content-Type': 'multipart/form-data',
    },
  });
  try {
    if (
      responce &&
      responce.data &&
      responce.data.keyword === 'success' &&
      responce.data.data
    ) {
      return responce.data.data;
    } else {
      return responce.data.message;
    }
  } catch (err) {
    return 'Something went wrong';
  }
};

const saveMpin = async body => {
  let responce = await axiosInstance.post('saveMpin', body, {
    headers: {
      'Content-Type': 'multipart/form-data',
    },
  });
  try {
    if (
      responce &&
      responce.data &&
      responce.data.keyword === 'success' &&
      responce.data.data
    ) {
      return responce.data;
    } else {
      return responce.data.message;
    }
  } catch (err) {
    return 'Something went wrong';
  }
};

const resetMpin = async body => {
  let responce = await axiosInstance.post('resetMpin', body, {
    headers: {
      'Content-Type': 'multipart/form-data',
    },
  });
  try {
    if (
      responce &&
      responce.data &&
      responce.data.keyword === 'success' &&
      responce.data.data
    ) {
      return responce.data.data;
    } else {
      return responce.data.message;
    }
  } catch (err) {
    return 'Something went wrong';
  }
};

const verifyUser = async email => {
  try {
    let response = await axiosInstance.get(`verifyUser/${email}`);
    if (response && response.data && response.data.keyword === 'success') {
      return response.data.data;
    } else {
      return response.data.message;
    }
  } catch (err) {
    return response.data.message;
  }
};

const forgotPassword = async body => {
  let responce = await axiosInstance.post('forgetPasswordOtp', body, {
    headers: {
      'Content-Type': 'multipart/form-data',
    },
  });
  try {
    if (responce && responce.data && responce.data.keyword === 'success') {
      return responce.data.data;
    } else {
      return responce.data.message;
    }
  } catch (err) {
    return 'Something went wrong';
  }
};

const forgotMpin = async body => {
  let responce = await axiosInstance.post('forgetMpin', body, {
    headers: {
      'Content-Type': 'multipart/form-data',
    },
  });
  try {
    if (responce && responce.data && responce.data.keyword === 'success') {
      return responce.data.data;
    } else {
      return responce.data.message;
    }
  } catch (err) {
    return 'Something went wrong';
  }
};

const resetPassword = async body => {
  let responce = await axiosInstance.post('passwordResetOtp', body, {
    headers: {
      'Content-Type': 'multipart/form-data',
    },
  });
  try {
    if (
      responce &&
      responce.data &&
      responce.data.keyword === 'success' &&
      responce.data.data
    ) {
      const token = responce.data.data.token;
      return {token};
    } else {
      return responce.data.message;
    }
  } catch (err) {
    return 'Something went wrong';
  }
};

const saveLogin = async token => {
  try {
    await AsyncStorage.setItem(TOKEN, token);
    let userData = jwt(token);
    await AsyncStorage.setItem(LOGIN_USER, JSON.stringify(userData));
    return userData;
  } catch (err) {
    return false;
  }
};

const getUserData = async () => {
  try {
    let mPin = await AsyncStorage.getItem(MPIN);
    return mPin;
  } catch (err) {
    return false;
  }
};

const getUserDataa = async () => {
  try {
    let token = await AsyncStorage.getItem(TOKEN);
    axios.defaults.headers.common.Authorization = token;
    let userData = await AsyncStorage.getItem(LOGIN_USER);
    return {token, userData};
  } catch (err) {
    return false;
  }
};
const getIsOtp = async () => {
  try {
    let token = await AsyncStorage.getItem(TOKEN);
    axios.defaults.headers.common.Authorization = token;
    let isTwoStep = await AsyncStorage.getItem(IS_TWO_STEP);
    return isTwoStep;
  } catch (err) {
    return false;
  }
};

const signUp = async data => {
  let responce = await axiosInstance.post('customer/create', data,{
    headers: {
      'Content-Type': 'multipart/form-data',
    },
  });
  try {
    if (responce && responce.data && responce.data.keyword === 'success') {
      return responce.data.data;
    } else {
      return responce.data.message;
    }
  } catch (err) {
    return 'Something went wrong';
  }
};

const getEthnicity = async () => {
  let response = await axiosInstance.get(`getEthnicity`);
  try {
    if (response && response.data && response.data.keyword === 'success') {
      return response.data.data;
    } else {
      return response.data.message;
    }
  } catch (err) {
    return response.data.message;
  }
};

const getIncome = async () => {
  let response = await axiosInstance.get(`getIncome`);
  try {
    if (response && response.data && response.data.keyword === 'success') {
      return response.data.data;
    } else {
      return response.data.message;
    }
  } catch (err) {
    return response.data.message;
  }
};

const getEmployed = async () => {
  let response = await axiosInstance.get(`getEmployed`);
  try {
    if (response && response.data && response.data.keyword === 'success') {
      return response.data.data;
    } else {
      return response.data.message;
    }
  } catch (err) {
    return response.data.data;
  }
};

const getCitizenship = async () => {
  let response = await axiosInstance.get(`getCitizenship`);
  try {
    if (response && response.data && response.data.keyword === 'success') {
      return response.data.data;
    } else {
      return response.data.message;
    }
  } catch (err) {
    return response.data.message;
  }
};

const getState = async () => {
  let response = await axiosInstance.get(`getState`);
  try {
    if (response && response.data && response.data.keyword === 'success') {
      return response.data.data;
    } else {
      return response.data.message;
    }
  } catch (err) {
    return response.data.message;
  }
};

const getCountryCodes = async () => {
  let response = await axiosInstance.get(`getCountryCodes`);
  try {
    if (response && response.data && response.data.keyword === 'success') {
      return response.data.data;
    } else {
      return response.data.message;
    }
  } catch (err) {
    return response.data.message;
  }
};

const getExperience = async () => {
  let response = await axiosInstance.get(`getExperience`);
  try {
    if (response && response.data && response.data.keyword === 'success') {
      return response.data.data;
    } else {
      return response.data.message;
    }
  } catch (err) {
    return response.data.message;
  }
};

const getEmpExperience = async () => {
  let response = await axiosInstance.get(`getEmpExperience`);
  try {
    if (response && response.data && response.data.keyword === 'success') {
      return response.data.data;
    } else {
      return response.data.message;
    }
  } catch (err) {
    return response.data.message;
  }
};

const getTaxIdType = async () => {
  let token = await AsyncStorage.getItem(TOKEN);

  let response = await axiosInstance.get(`alpaca_taxid_type`, {
    headers: {
      Authorization: token,
    },
  });
  try {
    if (response && response.data && response.data.keyword === 'success') {
      return response.data.data;
    } else {
      return response.data.message;
    }
  } catch (err) {
    return response.data.message;
  }
};

const updateToken = async (body, token) => {
  let {data} = await axiosInstance.post(``, body, {
    headers: {
      Authorization: token,
      'Content-Type': 'multipart/form-data',
    },
  });

  try {
    return data;
  } catch (err) {
    return 'Some thing went wrong';
  }
};

const getSession = async () => {
  let response = await axiosInstance.get(`session`);
  try {
    if (response && response.data && response.data.keyword === 'success') {
      return response.data;
    } else {
      return response.data;
    }
  } catch (err) {
    return response.data.message;
  }
};

const getMpinStatus = async body => {
  let response = await axiosInstance.post(`isMpinEnabled`, body, {
    headers: {
      'Content-Type': 'multipart/form-data',
    },
  });

  try {
    if (response && response.data && response.data.keyword === 'success') {
      return response.data;
    } else {
      return response.data;
    }
  } catch (err) {
    return 'Something went wrong';
  }
};

const uploadAlpacaDocuments = async body => {
  let token = await AsyncStorage.getItem(TOKEN);

  let response = await axiosInstance.post(`alpaca_document_upload`, body, {
    headers: {
      'Content-Type': 'multipart/form-data',
      Authorization: token,
    },
  });
  try {
    if (response && response.data && response.data.keyword === 'success') {
      return response.data;
    } else {
      return response.data;
    }
  } catch (err) {
    return 'Something went wrong';
  }
};

const portfolioFilesupload = async (body, options) => {
  let token = await AsyncStorage.getItem(TOKEN);

  let response = await axiosInstance.post(`portfolioFilesupload`, body, {
    headers: {
      'content-type': 'multipart/form-data',
      Authorization: `Bearer ${token}`,
      options,
    },
  });
  try {
    if (response && response.data && response.data.keyword === 'success') {
      return response.data;
    } else {
      return response.data;
    }
  } catch (err) {
    return 'Something went wrong';
  }
};

const portfolioFilesInsert = async body => {
  let token = await AsyncStorage.getItem(TOKEN);

  let response = await axiosInstance.post(`portfolioFilesInsert`, body, {
    headers: {
      Authorization: `Bearer ${token}`,
      'content-type': 'multipart/form-data',
    },
  });
  try {
    if (response && response.data && response.data.keyword === 'success') {
      return response.data;
    } else {
      return response.data;
    }
  } catch (err) {
    return 'Something went wrong';
  }
};

const removeAlpacaDocuments = async userId => {
  let token = await AsyncStorage.getItem(TOKEN);

  let response = await axiosInstance.get(`remove_document/${userId}`, {
    headers: {
      Authorization: token,
    },
  });
  try {
    if (response && response.data && response.data.keyword === 'success') {
      return response.data;
    } else {
      return response.data;
    }
  } catch (err) {
    return 'Something went wrong';
  }
};

const registerAlpaca = async body => {
  let token = await AsyncStorage.getItem(TOKEN);

  let response = await axiosInstance.post(
    `alpaca_register`,
    QueryString.stringify(body),
    {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        Authorization: token,
      },
    },
  );
  try {
    if (response && response.data && response.data.keyword === 'success') {
      return response.data;
    } else {
      return response.data;
    }
  } catch (err) {
    return 'Something went wrong';
  }
};

const viewAlpaca = async () => {
  let token = await AsyncStorage.getItem(TOKEN);

  let response = await axiosInstance.get(`alpaca_profile`, {
    headers: {
      Authorization: token,
    },
  });

  try {
    if (response && response.data && response.data.keyword === 'success') {
      return response.data;
    } else {
      return response.data;
    }
  } catch (err) {
    return response.data.message;
  }
};

const logout = async () => {
  let token = await AsyncStorage.getItem(TOKEN);

  let response = await axiosInstance.post(`logout`, null, {
    headers: {
      Authorization: token,
      'content-type': 'multipart/form-data',
    },
  });

  try {
    if (response && response.data && response.data.keyword === 'success') {
      return response.data;
    } else {
      return response.data;
    }
  } catch (err) {
    return response.data.message;
  }
};

const updateFCM = async body => {
  let token = await AsyncStorage.getItem(TOKEN);

  let response = await axiosInstance.post(`update_firebase_token`, body, {
    headers: {
      Authorization: token,
      'content-type': 'multipart/form-data',
    },
  });

  try {
    if (response && response.data && response.data.keyword === 'success') {
      return response.data;
    } else {
      return response.data;
    }
  } catch (err) {
    return response.data.message;
  }
};

const checkMarketOpen = async () => {
  let response = await axiosInstance.get(`checkMarketOpen`);
  try {
    if (response && response.data && response.data.keyword === 'success') {
      return response.data;
    } else {
      return response.data;
    }
  } catch (err) {
    return response.data.message;
  }
};

export const auth = {
  verifyUser,
  login,
  saveMpin,
  loginMpin,
  forgotMpin,
  resetMpin,
  saveLogin,
  getUserData,
  updateToken,
  signUp,
  getEthnicity,
  getIncome,
  getEmployed,
  getState,
  getCountryCodes,
  getExperience,
  forgotPassword,
  resetPassword,
  getIsOtp,
  getUserDataa,
  getCitizenship,
  getEmpExperience,
  getTaxIdType,
  deviceChangeLogin,
  getSession,
  getMpinStatus,
  uploadAlpacaDocuments,
  removeAlpacaDocuments,
  registerAlpaca,
  viewAlpaca,
  logout,
  updateFCM,
  portfolioFilesupload,
  portfolioFilesInsert,
  checkMarketOpen,
};
