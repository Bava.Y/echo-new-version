import {useFocusEffect} from '@react-navigation/core';
import React, {
  useCallback,
  useContext,
  useState,
  useRef,
  useEffect,
} from 'react';
import {
  Platform,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
  Image,
  ActivityIndicator,
  FlatList,
  Dimensions,
} from 'react-native';
import Snackbar from 'react-native-snackbar';
import {portfolio} from '../../api';
import AmeritradeAlert from '../../components/AmeritradeAlert';
import Loading from '../../components/Loading';
import {UserDataContext} from '../../context/UserDataContext';
import Colors from '../../utils/theme';
import PortfolioChart from './chart';
import CompanyList from './companyList';
import styles from './style';
import theme from '../../utils/theme';
import Modal from 'react-native-modal';
import {VictoryChart, VictoryLine} from 'victory-native';
import {ALPACA, AMERITRADE, TLOGO} from '../../assets/images/index';
import Icon from 'react-native-vector-icons/FontAwesome';
import {GestureHandlerRootView} from 'react-native-gesture-handler';
import {
  CandlestickChart,
  LineChart,
  LineChartCursorCrosshair,
  LineChartCursorLine,
  LineChartCursorProps,
} from 'react-native-wagmi-charts';
import Svg from 'react-native-svg';
import * as haptics from 'react-native-haptic-feedback';

export const PortfolioTab = ({navigation}) => {
  // PortfolioTab Variables
  const [alpachaAccountValue, setAlpacaAccountValue] = useState(null);
  const [alpachaCashValue, setAlpacaCashValue] = useState(null);
  const [alpachaPowerValue, setAlpacaPowerValue] = useState(null);
  const [tdAccountValue, setTDAccountValue] = useState(null);
  const [tdCashValue, setTDCashValue] = useState(null);
  const [tdPowerValue, setTDPowerValue] = useState(null);
  const [returnsValue, setReturnsValue] = useState(null);
  const [returnsAmount, setReturnsAmount] = useState(null);
  const [chartOrderData, setChartOrderData] = useState(null);
  const [listOrderData, setListOrderData] = useState([]);
  const [lineChartData, setLineChartData] = useState([]);
  const [orderData, setOrderData] = useState(null);
  const [chartRange, setChartRange] = useState('1d');
  const [showList, setShowList] = useState(false);
  const [people, setPeople] = useState(null);
  const [echoedPeople, setEchoedPeople] = useState(null);
  const [echoUser, setEchoUser] = useState(null);
  const [cardLoading, setCardLoading] = useState(null);

  console.log('chartRaNHEK>>>>>', lineChartData);
  // Context Variables
  const {
    ameritradeAlertStatus,
    portfolioConnection,
    portfolioTimePeriod,
    setAmeritradeAlertStatus,
    token,
    userId,
    setUserId,
    alpacaConfigStatus,
    tdConfigStatus,
    setPortfolioTimePeriod,

    showCurrentPage,
    setShowCurrentPage,
    setSellBtn,

    loadMoreData,
    setLoadMoreData,
    setCardLoad,
  } = useContext(UserDataContext);

  // Other VAriables
  const [portChartLoading, setportChartLoading] = useState(false);
  const [listLoading, setListLoading] = useState(false);
  const [currentPage, setCurrentPage] = useState(0);
  const [pieLoading, setpieLoading] = useState();
  console.log('portChartLoading', returnsAmount);

  useFocusEffect(
    useCallback(() => {
      let isFocus = true;

      getOrdersChartPortfolio();

      getechoingList();
      getechowerList();
      return () => {
        isFocus = false;
      };
    }, []),
  );
  useEffect(() => {
    fetchLineChartData();
    getPortfolioReturnsDetail();
  }, [chartRange,portfolioTimePeriod]);

  useEffect(() => {
    setChartRange('1d');
    setPortfolioTimePeriod('1d');
  }, []);
  useEffect(() => {
    setUserId(0);
    getPortfolioData();
  }, [currentPage]);

  

  const getPortfolioReturnsDetail = async () => {
    setCardLoading(true);

    let endpoint = `${portfolioConnection}`;

    endpoint = endpoint + '?time_period=' + portfolioTimePeriod;

    const response = await portfolio.getPortfolioReturnsDetails(endpoint);
    const res = response.data;

    if (res) {
      if (response.keyword == 'success') {
        setAlpacaAccountValue(res?.alpaca_portfolio_balance);
        setAlpacaCashValue(res?.alpaca_cash_balance);
        setAlpacaPowerValue(res?.alpaca_buying_power);
        setTDAccountValue(res?.td_portfolio_balance);
        setTDCashValue(res?.td_cash_balance);
        setTDPowerValue(res?.td_buying_power);
        setReturnsValue(res?.portfolio_returns);
        setReturnsAmount(res?.portfolio_returns_amount);
        setCardLoading(false);
      }
    else if(response.keyword == 'failed'){
      setCardLoading(false);
    }

    }
  };

  const getOrdersChartPortfolio = async () => {
    //setportChartLoading(true);
    setpieLoading(true);
    let endpoint = `overall/chart/${portfolioConnection}`;

    if (!!portfolioTimePeriod) {
      endpoint = endpoint + '?time_period=' + portfolioTimePeriod;
    }

    const response = await portfolio.getOrdersChartPortfolio(endpoint);

    const res = response.data;
    if (res) {
      if (response.keyword == 'success') {
        let orderArr = [];

        if (res?.length != 0) {
          res?.map(item => {
            orderArr.push({
              name: `${item?.symbol} (${item?.percentage}%)`,
              x: `${item?.percentage}%`,
              y: Number(item?.percentage / 30),
            });
          });

          setChartOrderData(orderArr);
          setpieLoading(false);
        }
      } else {
        if (response.hasOwnProperty('message')) {
          if (response.message.hasOwnProperty('error')) {
            setChartOrderData([]);
            setpieLoading(false);
            switch (response.message.error) {
              case 'invalid_grant':
                handleAmeritradeAlertStatus();
                break;

              default:
                Snackbar.show({
                  duration: Snackbar.LENGTH_SHORT,
                  text: response.message.error,
                  backgroundColor: 'red',
                });
                break;
            }
          } else {
            setChartOrderData([]);
            setpieLoading(false);
          }
        } else {
          setChartOrderData([]);
          setpieLoading(false);
        }
      }
    } else {
      setChartOrderData([]);
      setpieLoading(false);
    }

    // setportChartLoading(false);
  };

  const getPortfolioData = async () => {
    setListLoading(true);
    setCardLoad(true);
    const endpoint = `portfolio/overall/${portfolioConnection}?limit=4&offset=${currentPage}`;

    const res = await portfolio.getOrdersData(endpoint);
    if (res) {
      if (res.keyword == 'success') {
        setListLoading(true);
        if (res?.data?.symbol?.length == 0) {
          setLoadMoreData(false);
        }

        await setListOrderData([...listOrderData, ...res.data]);
        setListLoading(false);
        setCurrentPage(currentPage + 1);
        setCardLoad(true);
      } else if (res.keyword == 'failed') {
        setCardLoad(false);

        setListLoading(false);
      }
    } else {
      setCardLoad(false);
      setListOrderData([]);
    }

    setListLoading(false);
  };

  const getechowerList = async () => {
    setListLoading(true);

    const res = await portfolio.getechowerList();

    if (res) {
      if (res.keyword == 'success') {
        await setEchoedPeople(res?.data || []);
      } else {
        await setEchoedPeople([]);
      }
    }
    setListLoading(false);
  };

  const getechoingList = async () => {
    setListLoading(true);

    const res = await portfolio.getechoingList();

    if (res) {
      if (res.keyword == 'success') {
        await setPeople(res?.data || []);
      } else {
        await setPeople([]);
      }
    }
    setListLoading(false);
  };

  const fetchLineChartData = () => {
    switch (chartRange) {
      case '1d':
        setportChartLoading(true);
        getTotalPortfolioDayChart();

      default:
        break;

      case '1w':
        setportChartLoading(true);
        getTotalPortfolioWeekChart();

        break;

      case '1m':
      case '3m':
      case '6m':
      case '1y':
      case 'max':
        setportChartLoading(true);
        getTotalPortfolioMonthChart();
        break;
    }
  };

  const getTotalPortfolioDayChart = async () => {
    const res = await portfolio.getTotalPortfolioDayChart();

    if (res) {
      if (res.keyword == 'success') {
        if (res?.data) {
          setLineChartData(res.data);
          setportChartLoading(false);
        } else {
          setLineChartData([]);
          setportChartLoading(false);
        }
      } else {
        setLineChartData([]);
        setportChartLoading(false);
      }
    } else {
      setLineChartData([]);
      setportChartLoading(false);
    }
  };

  const getTotalPortfolioWeekChart = async () => {
    const res = await portfolio.getTotalPortfolioWeekChart();

    if (res) {
      if (res.keyword == 'success') {
        if (res?.data) {
          setLineChartData(res.data);
          setportChartLoading(false);
        } else {
          setLineChartData([]);
          setportChartLoading(false);
        }
      } else {
        setLineChartData([]);
        setportChartLoading(false);
      }
    } else {
      setLineChartData([]);
      setportChartLoading(false);
    }
  };

  const getTotalPortfolioMonthChart = async () => {
    const res = await portfolio.getTotalPortfolioMonthChart(chartRange);

    if (res) {
      if (res.keyword == 'success') {
        if (res?.data) {
          setLineChartData(res.data);
          setportChartLoading(false);
        } else {
          setLineChartData([]);
          setportChartLoading(false);
        }
      } else {
        setLineChartData([]);
        setportChartLoading(false);
      }
    } else {
      setLineChartData([]);
      setportChartLoading(false);
    }
  };

  const handleAmeritradeAlertStatus = () => {
    setAmeritradeAlertStatus(!ameritradeAlertStatus);
  };

  // const getUserId = () => {
  //   setUserId(data.followed_id);
  // };

  const peopleSymbolDetailsPortfolio = async () => {
    const res = await portfolio.peopleSymbolDetailsPortfolio(userId);
    if (res) {
      if (res.keyword == 'success') {
        if (res?.data) {
          setEchoUser(res.data);
          setShowList(true);
        } else {
          setEchoUser([]);
        }
      } else {
        setLineChartData([]);
        Snackbar.show({
          duration: Snackbar.LENGTH_SHORT,
          text: res.message,
          backgroundColor: 'red',
        });
      }
    } else {
    }
  };

  const sellRef = useRef();

  // Other Variables

  const style = getStyles();

  function getStyles() {
    const GREEN_COLOR = '#00BD9A';
    const RED_COLOR = '#FF6960';

    return {
      lineOne: {
        data: {stroke: GREEN_COLOR, strokeWidth: 1},
      },
      lineTwo: {
        data: {stroke: RED_COLOR, strokeWidth: 1},
      },
    };
  }

  const isFloat = n => {
    return Number(n) % 1 !== 0;
  };

  return (
    <>
      <ScrollView
        keyboardShouldPersistTaps="handled"
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
        style={styles.container}>
        <View
          style={{
            backgroundColor: theme.primaryColor,

            borderRadius: 8,
            paddingVertical: 10,
            paddingHorizontal: 10,
            width: '95%',

            marginTop: 8,
            alignSelf: 'center',
          }}>
          {cardLoading == true ? (
            <View
              style={{
                borderRadius: 8,

                paddingVertical: 25,
                paddingHorizontal: 25,
              }}>
              <ActivityIndicator size={'large'} color={theme.secondryColor} />
            </View>
          ) : (
            <View
              style={{
                left: 20,
              }}>
              {alpacaConfigStatus && (
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}>
                  <View
                    style={{
                      flex: 0.485,
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      alignItems: 'center',
                      backgroundColor: theme.themeColor,
                      borderRadius: 4,
                      marginRight: tdConfigStatus ? 0 : 40,
                      paddingHorizontal: 8,
                      paddingVertical: 6,
                    }}>
                    <Text style={styles.chartLabelTxt}>Total Portfolio</Text>

                    <Text style={styles.chartValueTxt}>
                      $ {alpachaAccountValue ? alpachaAccountValue : 0.0}
                    </Text>
                  </View>

                  <View
                    style={{
                      flex: 0.485,
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      alignItems: 'center',
                      backgroundColor: theme.themeColor,
                      borderRadius: 4,
                      marginRight: alpacaConfigStatus ? 0 : 40,
                      paddingHorizontal: 8,
                      paddingVertical: 6,
                      right: alpacaConfigStatus ? 40 : 0,
                    }}>
                    <Text style={styles.chartLabelTxt}>Cash</Text>

                    <Text style={styles.chartValueTxt}>
                      $ {alpachaCashValue ? alpachaCashValue : 0.0}
                    </Text>
                  </View>
                </View>
              )}

              {tdConfigStatus && (
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    marginTop: alpacaConfigStatus ? 8 : 0,
                  }}>
                  <View
                    style={{
                      flex: 0.475,
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      alignItems: 'center',
                      backgroundColor: theme.themeColor,
                      borderRadius: 4,
                      marginRight: tdConfigStatus ? 0 : 40,
                      paddingHorizontal: 8,
                      paddingVertical: 6,
                    }}>
                    <Text style={styles.chartLabelTxt}>TD Portfolio</Text>

                    <Text style={styles.chartValueTxt}>
                      $ {tdAccountValue ? tdAccountValue?.toFixed(2) : 0.0}
                    </Text>
                  </View>

                  <View
                    style={{
                      flex: 0.475,
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      alignItems: 'center',
                      backgroundColor: theme.themeColor,
                      borderRadius: 4,
                      marginRight: alpacaConfigStatus ? 0 : 40,
                      paddingHorizontal: 8,
                      paddingVertical: 6,
                      right: alpacaConfigStatus ? 40 : 0,
                    }}>
                    <Text style={styles.chartLabelTxt}>Cash</Text>

                    <Text style={styles.chartValueTxt}>
                      $ {tdCashValue ? tdCashValue?.toFixed(2) : 0.0}
                    </Text>
                  </View>
                </View>
              )}

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  marginTop: 8,
                }}>
                {alpachaPowerValue && (
                  <View
                    style={{
                      flex: tdPowerValue ? 0.88 : 1,
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      alignItems: 'center',
                      backgroundColor: theme.themeColor,
                      borderRadius: 4,
                      marginRight: tdPowerValue ? 0 : 40,
                      paddingHorizontal: 8,
                      paddingVertical: 6,
                    }}>
                    <Text style={styles.chartLabelTxt}>Buying Power</Text>

                    <Text style={styles.chartValueTxt}>
                      $ {alpachaPowerValue}
                    </Text>
                  </View>
                )}
              </View>

              <View
                style={{
                  backgroundColor: theme.themeColor,
                  borderRadius: 4,
                  marginTop: 8,
                  marginRight: 40,
                  paddingHorizontal: 8,
                  paddingVertical: 6,
                }}>
                <TouchableOpacity
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                  }}
                  onPress={() => {
                    navigation.navigate('IndividualReturns', {from});
                  }}>
                  <View style={{flex: 0.325, justifyContent: 'center'}}>
                    <Text style={styles.chartLabelTxt}>Total Returns</Text>
                  </View>

                  <View style={{flex: 0.125, justifyContent: 'center'}}>
                    <Icon
                      name={
                        Boolean(returnsValue) &&
                        Math.sign(Number(returnsValue)) == -1
                          ? 'arrow-down'
                          : 'arrow-up'
                      }
                      color={
                        Boolean(returnsValue) &&
                        Math.sign(Number(returnsValue)) == -1
                          ? theme.danger
                          : theme.successGreen
                      }
                      size={12}
                      style={{paddingHorizontal: 4, alignSelf: 'center'}}
                    />
                  </View>

                  <View style={{flex: 0.375, justifyContent: 'center'}}>
                    <Text style={styles.chartValueTxt}>
                      ${' '}
                      {Boolean(returnsAmount)
                        ? returnsAmount?.toFixed(2)
                        : '0.00'}
                    </Text>
                  </View>

                  <View
                    style={{
                      flex: 0.196,
                      justifyContent: 'center',
                      alignItems: 'flex-end',
                    }}>
                    <Text style={styles.chartValueTxt}>
                      {Boolean(returnsValue)
                        ? parseFloat(returnsValue)?.toFixed(2)
                        : '0.00'}
                      %
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          )}
        </View>
        <View style={{paddingHorizontal: 8, paddingVertical: 8}}>
          {!Boolean(lineChartData) &&
          Boolean(portChartLoading) &&
          Boolean(pieLoading) ? (
            <PortfolioChart loading={portChartLoading} />
          ) : (
            <PortfolioChart
              loading={portChartLoading}
              chartRange={chartRange}
              lineChartData={lineChartData}
              pieLoading={pieLoading}
              orderData={chartOrderData}
              from={'Portfolio'}
              onRangeChange={range => {
                console.log('range>>>>>', range);
                setChartRange(range);

                //setLineChartData(null);
              }}
            />
          )}
        </View>

        <View style={{height: 420}}>
          {!Boolean(listOrderData) ? (
            <CompanyList loading={listLoading} />
          ) : (
            <CompanyList
              loading={listLoading}
              orderList={listOrderData}
              Path={null}
            />
          )}
        </View>

        <View
          style={{
            backgroundColor: theme.primaryColor,
            paddingHorizontal: 8,
            paddingVertical: 8,
            marginTop: 10,
          }}>
          <Text
            style={{
              fontSize: Platform.OS == 'ios' ? 12 : 14,
              color: theme.textColor,
              fontWeight: 'bold',
            }}>
            People You Are Echoing
          </Text>
        </View>

        <View style={{paddingHorizontal: 8, paddingVertical: 16}}>
          {Boolean(people) ? (
            people.length != 0 ? (
              people?.map((data, index) => (
                <View
                  key={index}
                  style={[
                    styles.echoingdetail,
                    {
                      marginTop: index != 0 ? 8 : 0,
                      paddingHorizontal: 12,
                      paddingVertical: 12,
                    },
                  ]}>
                  <TouchableOpacity
                    style={{
                      flex: 0.425,
                      justifyContent: 'center',
                      alignItems: 'flex-start',
                    }}
                    onPress={() => {
                      // navigation.navigate('Echoers', {id: data.followed_id})
                      // peopleSymbolDetailsPortfolio(setUserId(data.followed_id))
                      setUserId(data.followed_id);
                      navigation.navigate('PortfolioShare', {
                        id: data.followed_id,
                        status: data.status,
                      });
                    }}>
                    <Text
                      style={{
                        fontSize: Platform.OS == 'ios' ? 14 : 16,
                        color: theme.white,
                        fontWeight: '600',
                      }}>
                      {data.first_name + ' ' + data.last_name}
                    </Text>
                  </TouchableOpacity>
                </View>
              ))
            ) : (
              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 12 : 14,
                  fontWeight: '600',
                  color: theme.white,
                  textAlign: 'center',
                  paddingHorizontal: 8,
                }}>
                No echoing(s) found
              </Text>
            )
          ) : (
            <View style={{paddingHorizontal: 8}}>
              <ActivityIndicator size={'large'} color={theme.secondryColor} />
            </View>
          )}
        </View>

        <View
          style={{
            backgroundColor: theme.primaryColor,
            paddingHorizontal: 8,
            paddingVertical: 8,
          }}>
          <Text
            style={{
              fontSize: Platform.OS == 'ios' ? 12 : 14,
              color: theme.textColor,
              fontWeight: 'bold',
            }}>
            Previous Echoes
          </Text>
        </View>

        <View style={{paddingHorizontal: 8, paddingVertical: 16}}>
          {Boolean(echoedPeople) ? (
            echoedPeople.length != 0 ? (
              echoedPeople?.map((data, index) => (
                <View
                  key={index}
                  style={[
                    styles.echoingdetail,
                    {
                      marginTop: index != 0 ? 8 : 0,
                      paddingHorizontal: 12,
                      paddingVertical: 12,
                    },
                  ]}>
                  <TouchableOpacity
                    style={{
                      flex: 0.425,
                      justifyContent: 'center',
                      alignItems: 'flex-start',
                    }}
                    onPress={() => {
                      //navigation.navigate('Echoers', {id: data.followed_id})
                      //peopleSymbolDetailsPortfolio()
                      setUserId(data.followed_id),
                        navigation.navigate('PortfolioShare', {
                          id: data.followed_id,
                        });
                    }}>
                    <Text
                      style={{
                        fontSize: Platform.OS == 'ios' ? 14 : 16,
                        color: theme.white,
                        fontWeight: '600',
                      }}>
                      {data.first_name + ' ' + data.last_name}
                    </Text>
                  </TouchableOpacity>

                  <View
                    style={{
                      flex: 0.125,
                      paddingHorizontal: 4,
                      paddingVertical: 4,
                    }}
                  />
                </View>
              ))
            ) : (
              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 12 : 14,
                  fontWeight: '600',
                  color: theme.white,
                  textAlign: 'center',
                  paddingHorizontal: 8,
                }}>
                No echoed(s) found
              </Text>
            )
          ) : (
            <View style={{paddingHorizontal: 8}}>
              <ActivityIndicator size={'large'} color={theme.secondryColor} />
            </View>
          )}
        </View>

        <TouchableOpacity
          style={{
            width: '37.5%',
            alignSelf: 'center',
            backgroundColor: Colors.secondryColor,
            borderRadius: 4,
            marginHorizontal: 16,
            marginVertical: 16,
            paddingHorizontal: 16,
            paddingVertical: 8,
          }}
          onPress={() => {
            navigation.navigate('Order');
          }}>
          <Text
            style={{
              fontSize: Platform.OS == 'ios' ? 14 : 16,
              color: Colors.white,
              textAlign: 'center',
            }}>
            Order History
          </Text>
        </TouchableOpacity>

        <View style={{paddingVertical: 12}} />
      </ScrollView>

      {/* Hided due to TD Ameritrade Echo App revoked issue */}
      {/* <AmeritradeAlert
        modalStatus={ameritradeAlertStatus}
        close={handleAmeritradeAlertStatus}
      /> */}

      {/*  */}
    </>
  );
};

export default PortfolioTab;
