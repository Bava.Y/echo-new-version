import AsyncStorage from '@react-native-community/async-storage';
import PushNotificationIOS from '@react-native-community/push-notification-ios';
import {StackActions, useFocusEffect} from '@react-navigation/native';
import jwt from 'jwt-decode';
import React, {useCallback, useContext, useRef, useState} from 'react';
import {Image, Platform, Text, TouchableOpacity, View} from 'react-native';
import DeviceInfo from 'react-native-device-info';
import firebase from '@react-native-firebase/app';
import ReactNativePinView from 'react-native-pin-view';
import PushNotification from 'react-native-push-notification';
import Snackbar from 'react-native-snackbar';
import {auth} from '../api/Authentication.service';
import {CANCEL, OK} from '../assets/images';
import Loading from '../components/Loading';
import {UserDataContext} from '../context/UserDataContext';
import style from '../styles/mpinstyle';
import {
  BEARER,
  IS_FIRST_USER,
  LOGIN_USER,
  TOKENN,
  WELCOME,
} from '../utils/constants';
import theme from '../utils/theme';

const MpinScreen = ({navigation}) => {
  // MpinScreen Variables
  const [otp, setOtp] = useState('');
  const [fcmToken, setFCMToken] = useState(null);

  // Context Variables
  const {
    setAlpacaConfigStatus,
    setFourChartSymbols,
    setSingleChartSymbol,
    setTDConfigStatus,
    setToken,
    setUserData,
    loginUserData, setLoginUserData
  } = useContext(UserDataContext);
console.log('loginUserData>?>>>>',loginUserData)
  // Error Variables
  const [errorMessage, setErrorMessage] = useState('');
  const [validationError, setValidationError] = useState(false);

  // Other Variables
  let deviceId = DeviceInfo.getUniqueId();
  const pinView = useRef(null);
  const [loading, setLoading] = useState(false);

  useFocusEffect(
    useCallback(() => {
      let isFocus = true;

      handlePushNotification();

      return () => {
        isFocus = false;
      };
    }, []),
  );

  const handlePushNotification = () => {
    PushNotification.checkPermissions((permission) => {});

    PushNotification.getChannels((channelId) => {});

    PushNotification.configure({
      onRegister: function (token) {
        Platform.OS === 'ios'
          ? getIOSToken()
          : [
              console.log('FCM Token::: ', token.token),

              setFCMToken(token.token),
            ];
      },

      onNotification: function (notification) {
        const clicked = notification.userInteraction;

        if (clicked) {
          handleNotificationNavigation(notification);
        }

        notification.finish(PushNotificationIOS.FetchResult.NoData);
      },

      onAction: function (notification) {
        PushNotification.invokeApp(notification);
      },

      onRegistrationError: function (registrationError) {},

      permissions: {
        alert: true,
        badge: true,
        sound: true,
      },

      popInitialNotification: true,
      requestPermissions: true,
      senderID: '103183859621',
    });
  };

  const getIOSToken = async () => {
    const fToken = await firebase.messaging().getToken();

    fToken && [console.log('FCM Token::: ', fToken), setFCMToken(fToken)];
  };

  const handleNotificationNavigation = (notification) => {
    const data = JSON.parse(notification.data.data);
    const page = notification.data.page;

    switch (page) {
      case 'admin':
        navigation.navigate('EchoAlert', {
          id: data.echo_message_id,
        });
        break;

      case 'EchoMetrics':
      case 'SymbolDisallowed':
        navigation.navigate('DisallowedTrades', {
          id: data.customer_id,
        });
        break;

      case 'rebalance':
        navigation.navigate('Rebalance', {
          id: data.customer_id,
        });
        break;

      case 'echoing':
        navigation.navigate('DiscoverPeopleProfile', {
          id: data.customer_id,
          loginStatus: data.login_status,
        });
        break;

      case 'people':
        navigation.navigate('Echoers', {id: data.direct_customer_id});
        break;

      case 'notification':
        navigation.navigate('Notification');
        break;

      default:
        break;
    }
  };

  const onTextChange = async (text) => {
    setValidationError(false);
    setOtp(text);

    if (text.length == 4) {
      setErrorMessage('');
      let formData = new FormData();

      formData && formData.append('mpin', text);
      formData && formData.append('unique_id', deviceId);

      if (formData !== undefined) {
        setLoading(true);
        let isFirst = await AsyncStorage.getItem(IS_FIRST_USER);
        const data = await auth.loginMpin(formData);

        if (typeof data === 'string') {
          if (data == 'Invalid MPIN') {
            setLoading(false);
            setValidationError(true);
            setErrorMessage('Incorrect PIN');
          }
          if (data === 'redirect') {
            return navigation.dispatch(StackActions.replace('Login'));
          }
          return Snackbar.show({
            text: data,
            duration: Snackbar.LENGTH_SHORT,
          });
        }
        await AsyncStorage.setItem(IS_FIRST_USER, data.is_first_loggedin);
        setLoading(false);
        Snackbar.show({
          text:
            data.two_step_verification == 0
              ? `Logged in successfully`
              : `OTP sent successfully`,
          duration: Snackbar.LENGTH_SHORT,
        });
        await auth.saveLogin(data.token);
        let bearer = 'Bearer ' + data.token;
        setToken(bearer);
        await AsyncStorage.setItem(BEARER, `${bearer}`);
        let decoded = jwt(data.token);
        setUserData(decoded);
        setLoginUserData(decoded?.user_id)
        await AsyncStorage.setItem(LOGIN_USER, JSON.stringify(decoded));
        const alpacaConfigStatus =
          data && data.hasOwnProperty('decoded')
            ? data.decoded.alpaca_account
            : false;

        setAlpacaConfigStatus(alpacaConfigStatus);
        data &&
          data.hasOwnProperty('decoded') &&
          data.decoded.single_chart_symbol &&
          setSingleChartSymbol(data.decoded.single_chart_symbol);
        data &&
          data.hasOwnProperty('decoded') &&
          data.decoded.four_chart_symbols &&
          setFourChartSymbols(data.decoded.four_chart_symbols);

        const ameritradeConfigStatus =
          data && data.hasOwnProperty('decoded')
            ? data.decoded.refresh_token
            : false;

        setTDConfigStatus(ameritradeConfigStatus);

        updateFCM();

        setTimeout(() => {
          setLoading(false);
          let userData = {...data, text, deviceId};

          navigation.dispatch(
            StackActions.replace(
              data.two_step_verification == 0
                ? // ? changeScreen == true
                  isFirst == 'no'
                  ? 'Dashboard'
                  : 'Profile'
                : // : 'Profile'
                  'MPinOTPScreen',
              {
                data: userData,
              },
            ),
          );
          AsyncStorage.setItem(WELCOME, 'true');
        }, 1500);
      }
    }
  };

  const validateFields = () => {
    let formData = new FormData();

    if (otp.length !== 4) {
      setValidationError(true);
      setErrorMessage('Enter proper MPIN');

      formData = undefined;
    } else {
      setErrorMessage('');
      formData && formData.append('mpin', otp);
      formData && formData.append('unique_id', deviceId);
    }
    return formData;
  };

  const handleSubmit = async () => {
    let formData = validateFields();
    if (formData !== undefined) {
      setLoading(true);
      const data = await auth.loginMpin(formData);

      if (typeof data === 'string') {
        if (data == 'Invalid MPIN') {
          setLoading(false);
          setValidationError(true);
          setErrorMessage('Incorrect PIN');
        }
        if (data === 'redirect') {
          return navigation.dispatch(StackActions.replace('Login'));
        }
        return Snackbar.show({
          text: data,
          duration: Snackbar.LENGTH_SHORT,
        });
      }
      setLoading(false);
      Snackbar.show({
        text:
          data.two_step_verification == 0
            ? `Logged in successfully`
            : `OTP sent successfully`,
        duration: Snackbar.LENGTH_SHORT,
      });

      await auth.saveLogin(data.token);
      let decoded = jwt(data.token);
      await AsyncStorage.setItem(TOKENN, JSON.stringify(decoded));

      let isFirst = await AsyncStorage.getItem(IS_FIRST_USER);

      const alpacaConfigStatus =
        data && data.hasOwnProperty('decoded')
          ? data.decoded.alpaca_account
          : false;

      setAlpacaConfigStatus(alpacaConfigStatus);
      data &&
        data.hasOwnProperty('decoded') &&
        data.decoded.single_chart_symbol &&
        setSingleChartSymbol(data.decoded.single_chart_symbol);
      data &&
        data.hasOwnProperty('decoded') &&
        data.decoded.four_chart_symbols &&
        setFourChartSymbols(data.decoded.four_chart_symbols);

      const ameritradeConfigStatus =
        data && data.hasOwnProperty('decoded')
          ? data.decoded.refresh_token
          : false;

      setTDConfigStatus(ameritradeConfigStatus);

      updateFCM();

      setTimeout(() => {
        setLoading(false);
        let userData = {...data, deviceId};

        navigation.dispatch(
          StackActions.replace(
            data.two_step_verification == 0
              ? // ? changeScreen == true
                isFirst == 'no'
                ? 'Dashboard'
                : 'Profile'
              : // : 'Profile'
                'MPinOTPScreen',
            {
              data: userData,
            },
          ),
        );
      }, 1500);
    }
  };

  const updateFCM = async () => {
    const requestData = {
      firebase_token: fcmToken,
    };

    await auth.updateFCM(requestData);
  };

  return (
    <>
      <View style={style.container}>
        <Text style={style.verifyText}>Login</Text>

        <Text style={style.mpinText}>Enter PIN</Text>

        <ReactNativePinView
          inputSize={20}
          ref={pinView}
          pinLength={4}
          buttonSize={60}
          buttonAreaStyle={{
            marginTop: 24,
          }}
          inputViewEmptyStyle={{
            backgroundColor: 'transparent',
            borderColor: '#FFF',
            borderWidth: 1,
          }}
          inputViewFilledStyle={{
            backgroundColor: validationError ? 'red' : '#FFF',
          }}
          buttonViewStyle={{
            borderColor: '#FFF',
            borderWidth: 1,
          }}
          onButtonPress={(key) => {
            if (key === 'custom_left') {
              pinView.current.clear();
            }
          }}
          customLeftButton={
            <View>
              <Image source={CANCEL} style={style.customButton} />
            </View>
          }
          customRightButton={
            <TouchableOpacity onPress={handleSubmit}>
              <Image source={OK} style={style.customButton} />
            </TouchableOpacity>
          }
          onValueChange={onTextChange}
        />
        {validationError ? (
          <Text
            style={{
              fontSize: Platform.OS == 'ios' ? 14 : 16,
              color: 'red',
              marginVertical: 20,
            }}>
            {errorMessage}
          </Text>
        ) : null}
        <View style={{flexDirection: 'row'}}>
          <TouchableOpacity
            style={style.containerResend}
            onPress={() => navigation.navigate('ResetMpinScreen')}>
            <Text style={style.forgot}>Forgot PIN?</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={style.containerResend}
            onPress={() => navigation.navigate('Login')}>
            <Text style={style.signin}>Sign In</Text>
          </TouchableOpacity>
        </View>
      </View>

      {loading && <Loading color={theme.white} />}
    </>
  );
};

export default MpinScreen;
