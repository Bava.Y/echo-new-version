import React from 'react';
import {Dimensions, Text, View} from 'react-native';
import {VictoryArea, VictoryAxis, VictoryChart} from 'victory-native';
import theme from '../../utils/theme';
import styles from './style';

const Charts = () => {
  const LIGHT_GREY = 'hsl(355, 20%, 90%)';

  const deviceWidth = Dimensions.get('window').width;

  return (
    <View style={{paddingVertical: 8}}>
      <Text style={styles.Companyname}>My Portfolio</Text>

      <View style={styles.sharerate}>
        <Text style={styles.shareamt}>$ 5666.00</Text>
        <Text style={styles.sharepercentage}>+ 12.09 (0.34%)</Text>
      </View>

      <VictoryChart
        width={deviceWidth * 0.875}
        height={280}
        padding={{top: 40, bottom: 20, left: 0, right: 40}}>
        <VictoryAxis
          orientation="right"
          dependentAxis
          style={{
            axis: {
              stroke: 'grey',
            },
            tickLabels: {
              fill: LIGHT_GREY,
              fontSize: 10,
            },
            axisLabel: {
              fill: 'red',
              fontSize: 10,
              fontStyle: 'italic',
            },
            grid: {
              fill: LIGHT_GREY,
              stroke: LIGHT_GREY,
              strokeWidth: 0.1,
            },
          }}
        />

        <VictoryArea
          interpolation="natural"
          style={{
            data: {
              fill: theme.secondryColor,
              fillOpacity: 0.5,
              stroke: theme.secondryColor,
              strokeWidth: 2,
            },
          }}
          data={[
            {x: 1, y: Math.random() * (1 - 0) + 0},
            {x: 2, y: Math.random() * (1 - 0) + 0},
            {x: 3, y: Math.random() * (1 - 0) + 0},
            {x: 4, y: Math.random() * (1 - 0) + 0},
            {x: 5, y: Math.random() * (1 - 0) + 0},
          ]}
        />
      </VictoryChart>
    </View>
  );
};

export default Charts;
