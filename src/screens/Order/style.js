import {StyleSheet} from 'react-native';
import theme from '../../utils/theme';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.themeColor,
  },
  tabsec: {
    backgroundColor: '#2e2e2e52',
    paddingVertical: 20,
  },
  tabhead: {
    flexDirection: 'row',
    alignSelf: 'center',
    backgroundColor: theme.themeColor,
    borderRadius: 10,
    padding: 5,
  },

  headingactivebtn: {
    backgroundColor: theme.secondryColor,

    paddingVertical: 4,
    borderRadius: 6,
    marginHorizontal: 4,
  },
  headingbtn: {
    backgroundColor: 'transparent',
    borderRadius: 6,
    paddingVertical: 4,
  },
  headingtxt: {
    color: 'white',
    textAlign: 'center',
    paddingHorizontal: 25,
  },
  headingactivetxt: {
    color: 'white',
    textAlign: 'center',
    paddingHorizontal: 20,
  },
  activedot: {
    backgroundColor: theme.secondryColor,
    width: 10,
    height: 10,
    borderRadius: 50,
    marginHorizontal: 8,
  },
  inactivedot: {
    backgroundColor: theme.white,
    width: 10,
    height: 10,
    borderRadius: 50,
    marginHorizontal: 8,
  },
  chartdetail: {
    backgroundColor: theme.primaryColor,
    padding: 10,
    marginHorizontal: 15,
    borderRadius: 10,
    marginTop: 10,
  },

  upgrade: {
    backgroundColor: '#00BD9A',
    paddingHorizontal: 10,
    paddingVertical: 7,
    alignSelf: 'center',
    borderRadius: 7,
  },
  downgrade: {
    backgroundColor: '#FF6960',
    paddingHorizontal: 10,
    paddingVertical: 7,
    alignSelf: 'center',
    borderRadius: 7,
  },
  companylist: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 15,
    paddingHorizontal: 10,
    borderRadius: 10,
    marginHorizontal: 15,
    backgroundColor: theme.primaryColor,
    marginBottom: 10,
  },
});

export default styles;
