import {isEmpty} from 'lodash';
import moment from 'moment';
import React, {useContext, useEffect, useRef, useState} from 'react';
import {
  ActivityIndicator,
  Image,
  Linking,
  Platform,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import FlipCard from 'react-native-flip-card';
import Snackbar from 'react-native-snackbar';
import {company} from '../../api';
import {DISCOVER} from '../../assets/images/index';
import BottomNav from '../../components/BottomNav';
import HeaderNav from '../../components/HeaderNav';
import StockPrice from '../../components/StockPrice';
import {UserDataContext} from '../../context/UserDataContext';
import theme from '../../utils/theme';
import OrderShareAmount from '../Dashboard/order-amount';
import OrderShareQuantity from '../Dashboard/order-quantity';
import Ticker from '../Dashboard/ticker';
import Metricslist from './metrics';
import RevenueChart from './companyChart';
import styles from './style';

const CompanyDetail = ({navigation, route}) => {
  const Order = useRef();
  const [symbol] = useState(route.params.symbol);
  const [quote, setQuote] = useState({});
  const [orderData, setOrderData] = useState({});
  const [metrics, setMetrics] = useState({});
  const [loading, setLoading] = useState();
  const [newsData, setNewsData] = useState(null);

  const {
    token,
    tickerData,
    setCompanyDescribe,
    setPeers,
    setRating,
  } = useContext(UserDataContext);

  useEffect(() => {
    let ac = new AbortController();

    getcompanyOverView();

    getCompanyNews();

    return () => {
      ac.abort();
    };
  }, []);

  useEffect(() => {
    let ac = new AbortController();

    getRating();

    return () => {
      setRating(null);

      ac.abort();
    };
  }, []);

  const [nodata, setNodata] = useState(false);

  const getcompanyOverView = async () => {
    setLoading(true);

    const res = await company.getCompanyOverview(symbol, token);

    if (res) {
      if (res.keyword == 'success') {
        if (res.message == 'Provide valid symbol') {
          setNodata(true);
        } else {
          setNodata(false);
        }

        setLoading(false);

        setQuote({
          ...res.data.quotes,
          symbol: res.data.symbol,
          companyName: res.data.company_name,
        });

        setMetrics(res.data.metrics);
        let CompanyDescribe = {
          companyName: res.data.company_name,
          symbol: res.data.symbol,
          logo: res.data.logo,
          industry: res.data.industry,
          sector: res.data.sector,
          ...res.data.description,
        };

        setCompanyDescribe(CompanyDescribe);

        setPeers(res.data.peers);
      } else {
        Snackbar.show({
          duration: Snackbar.LENGTH_SHORT,
          text: res.message,
          backgroundColor: 'red',
        });
      }
    } else {
    }
  };

  const getCompanyNews = async () => {
    const res = await company.getCompanyNews(symbol, token);

    if (res) {
      if (res.keyword == 'success') {
        if (res.data.length != 0) {
          let helperArray = [];

          res.data.map((lol) => {
            const helperObject = {...lol, imageLoadingStatus: true};

            helperArray.push(helperObject);
          });

          setNewsData(helperArray);
        } else {
          setNewsData(res.data);
        }
      } else {
        setNewsData([]);

        Snackbar.show({
          duration: Snackbar.LENGTH_SHORT,
          text: res.message,
          backgroundColor: 'red',
        });
      }
    } else {
      setNewsData([]);
    }
  };

  const getRating = async () => {
    const currentRating = await company.getRatings(symbol, token);

    currentRating != null ? setRating(currentRating.status) : setRating(null);
  };

  const processOrder = async (type) => {
    let order = {
      symbol: quote.symbol,
      name: quote.companyName,
      latest_price: quote.last_price,
      side: type,
      fromPage: '',
      id: '',
      creatorId: '',
      directUserTotalAmount: null,
      directUserPercentage: null,
    };

    await setOrderData(order);

    Order.current.open();
  };

  return (
    <React.Fragment>
      <HeaderNav
        title={quote.hasOwnProperty('companyName') ? quote.companyName : ''}
        isBack="true"
        routeName="Company"
      />

      {loading ? (
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: theme.primaryColor,
          }}>
          <ActivityIndicator size="large" color="white" />
        </View>
      ) : nodata ? (
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: theme.themeColor,
          }}>
          <Text
            style={{
              fontSize: Platform.OS == 'ios' ? 14 : 16,
              color: 'white',
              fontWeight: '600',
            }}>
            Details not available for this symbol
          </Text>

          <Text
            style={{
              fontSize: Platform.OS == 'ios' ? 12 : 14,
              color: 'white',
              fontWeight: '400',
              marginTop: 8,
            }}>
            Please! Try another symbol!
          </Text>
        </View>
      ) : (
        <View style={styles.container}>
          <Ticker data={tickerData} />

          <ScrollView
            keyboardShouldPersistTaps="handled"
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}>
            <View
              style={styles.companydetail}
              onPress={() => {
                setDescriptionVisible(true);
              }}>
              {quote.last_price !== undefined && (
                <StockPrice price={quote.last_price} />
              )}

              <Text style={styles.mediumtxt}>
                <Text
                  style={
                    quote.change < 0 ? styles.negativetxt : styles.successtxt
                  }>
                  {quote.change} ({quote.change_percent}) TODAY
                </Text>
              </Text>

              {quote.extendedPrice && (
                <Text style={styles.mediumtxt}>
                  <Text
                    style={
                      quote.extended_change < 0
                        ? styles.negativetxt
                        : styles.successtxt
                    }>
                    {quote.extended_change} ({quote.extended_change_percent}%)
                    AFTER-HOURS
                  </Text>
                </Text>
              )}
            </View>

            <FlipCard
              friction={6}
              perspective={1000}
              flipHorizontal={true}
              flipVertical={false}
              flip={false}
              clickable={true}>
              {/* Face Side */}
              {quote.symbol && <RevenueChart symbol={quote.symbol} />}

              {/* Back Side */}
              <Metricslist metrics={metrics} />
            </FlipCard>

            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
                marginVertical: 8,
              }}>
              <TouchableOpacity
                style={{marginHorizontal: 16}}
                onPress={() => processOrder('buy')}>
                <View style={styles.buybtn}>
                  <Text style={styles.buyText}>Buy</Text>
                </View>
              </TouchableOpacity>

              <TouchableOpacity
                style={{marginHorizontal: 16}}
                onPress={() => processOrder('sell')}>
                <View style={styles.sellbtn}>
                  <Text style={styles.sellText}>Sell</Text>
                </View>
              </TouchableOpacity>
            </View>

            <View style={[styles.tabssec1, {marginVertical: 8}]}>
              <View style={styles.tabhead}>
                <View style={[styles.headingactivebtn, styles.tabheadlabel]}>
                  <Text style={styles.headingactivetxt}>Fundamentals</Text>
                </View>

                <View style={[styles.headingbtn, styles.tabheadlabel]}>
                  <TouchableOpacity
                    onPress={() =>
                      navigation.navigate('Technicals', {
                        companyName: quote.companyName,
                        symbol: quote.symbol,
                      })
                    }>
                    <Text style={styles.headingtxt}>Technicals</Text>
                  </TouchableOpacity>
                </View>

                <View style={[styles.headingbtn, styles.tabheadlabel]}>
                  <TouchableOpacity
                    onPress={() =>
                      navigation.navigate('FinancialHistory', {
                        companyName: quote.companyName,
                        symbol: quote.symbol,
                      })
                    }>
                    <Text style={styles.headingtxt}>Financials</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>

            <View style={{marginVertical: 8}}>
              {newsData ? (
                newsData.length != 0 ? (
                  newsData.map((lol, index) => (
                    <View
                      key={index}
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        marginVertical: 8,
                      }}>
                      <View
                        style={{
                          width: '25%',
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}>
                        <Image
                          onLoadEnd={() => {
                            let helperAttachment = [...newsData];

                            helperAttachment[index].imageLoadingStatus = false;

                            setNewsData(helperAttachment);
                          }}
                          source={lol.image ? {uri: lol.image} : DISCOVER}
                          style={{
                            width: 60,
                            height: 60,
                            borderRadius: 4,
                            elevation: 4,
                          }}
                        />

                        {lol.imageLoadingStatus && (
                          <ActivityIndicator
                            color={theme.secondryColor}
                            style={{
                              position: 'absolute',
                            }}
                          />
                        )}
                      </View>

                      <View style={{width: '75%', justifyContent: 'center'}}>
                        <TouchableOpacity
                          onPress={async () => {
                            Linking.canOpenURL(lol.qmUrl)
                              .then((checkURL) => {
                                if (checkURL) {
                                  Linking.openURL(lol.qmUrl);
                                } else {
                                  Snackbar.show({
                                    backgroundColor: 'red',
                                    text: 'Unable to open the url',
                                    duration: Snackbar.LENGTH_LONG,
                                  });
                                }
                              })
                              .catch(() => {
                                Snackbar.show({
                                  backgroundColor: 'red',
                                  text: 'Facing some issues in open the url',
                                  duration: Snackbar.LENGTH_LONG,
                                });
                              });
                          }}>
                          <Text style={styles.desctxt}>{lol.headline}</Text>
                        </TouchableOpacity>

                        <Text style={styles.updatetime}>
                          {moment(lol.datetime, 'x').format('LLL')}
                        </Text>
                      </View>
                    </View>
                  ))
                ) : (
                  <View
                    style={{
                      justifyContent: 'center',
                      alignItems: 'center',
                      marginVertical: 24,
                    }}>
                    <Text
                      style={{
                        fontSize: Platform.OS == 'ios' ? 12 : 14,
                        color: theme.white,
                        paddingHorizontal: 8,
                      }}>
                      No news feed(s) found!
                    </Text>
                  </View>
                )
              ) : (
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginVertical: 24,
                  }}>
                  <ActivityIndicator size={24} color={theme.secondryColor} />
                  <Text
                    style={{
                      fontSize: Platform.OS == 'ios' ? 14 : 16,
                      color: theme.white,
                      paddingHorizontal: 8,
                    }}>
                    Loading news
                  </Text>
                </View>
              )}
            </View>
          </ScrollView>
        </View>
      )}

      {isEmpty(orderData) ? null : orderData?.fractionable ? (
        <OrderShareAmount
          buyform={Order}
          orderData={orderData}
          onChange={() => Order.current.close()}
        />
      ) : (
        <OrderShareQuantity
          buyform={Order}
          orderData={orderData}
          onChange={() => Order.current.close()}
        />
      )}

      <BottomNav routeName="Dashboard" />
    </React.Fragment>
  );
};

export default CompanyDetail;
