import {Dimensions, Platform, StyleSheet} from 'react-native';
import theme from '../../../utils/theme';

const deviceHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {flex: 1, backgroundColor: theme.themeColor},
  walletContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 8,
    paddingVertical: 8,
  },
  walletLabelContainer: {
    flex: 0.6,
    justifyContent: 'center',
  },
  walletLabelTxt: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    color: '#ffffffc2',
    fontWeight: '600',
  },
  walletValueContainer: {
    flex: 0.4,
    justifyContent: 'center',
  },
  walletValueTxt: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    color: theme.secondryColor,
    fontWeight: '600',
    textAlign: 'right',
  },
  mainsec: {
    backgroundColor: theme.primaryColor,
    borderRadius: 8,
    paddingHorizontal: 16,
    paddingVertical: 16,
  },
  headingTxt: {
    fontSize: Platform.OS == 'ios' ? 16 : 18,
    color: theme.white,
    fontWeight: 'bold',
  },
  lineContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  lineLabelContainer: {
    flex: 0.65,
    justifyContent: 'center',
  },
  lineValueContainer: {
    flex: 0.35,
    justifyContent: 'center',
  },
  lineLabelTxt: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    color: '#ffffffc2',
    fontWeight: '400',
  },
  lineValueTxt: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    color: '#ffffffc2',
    fontWeight: '400',
    textAlign: 'right',
  },
  lineValueRedTxt: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    color: 'red',
    fontWeight: '400',
    textAlign: 'right',
  },
  finalLabelTxt: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    color: theme.white,
    fontWeight: '600',
  },
  finalValueTxt: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    color: theme.white,
    fontWeight: '600',
    textAlign: 'right',
  },
  switchContainer: {flexDirection: 'row', justifyContent: 'space-between'},
  switchLabelContainer: {flex: 0.8, justifyContent: 'center'},
  switchToggleContainer: {
    flex: 0.2,
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  labelTxt: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    color: theme.white,
    fontWeight: '400',
  },
  noDataTxt: {
    fontSize: Platform.OS == 'ios' ? 12 : 14,
    color: theme.white,
  },
  selectfield: {marginVertical: 8},
  selectinput: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    color: 'white',
    borderColor: 'white',
    borderWidth: 1,
    borderRadius: 24,
    paddingHorizontal: 12,
    paddingVertical: 10,
  },
  selectIconinput: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderColor: 'white',
    borderWidth: 1,
    borderRadius: 24,
    paddingHorizontal: 12,
    paddingVertical: Platform.OS == 'ios' ? 10 : 0,
  },
  textInput: {fontSize: Platform.OS == 'ios' ? 14 : 16, color: 'white'},
  errorMsg: {
    fontSize: Platform.OS == 'ios' ? 12 : 14,
    color: '#ff0000a1',
    textAlign: 'left',
    marginHorizontal: 12,
    marginVertical: 4,
  },
  saveButton: {
    width: '62.5%',
    alignSelf: 'center',
    backgroundColor: theme.secondryColor,
    borderRadius: 8,
    marginVertical: 8,
    paddingHorizontal: 16,
    paddingVertical: 8,
  },
  btntext: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    color: '#fff',
    textAlign: 'center',
  },
  selectfieldvalue: {
    borderColor: 'white',
    borderWidth: 1,
    borderRadius: 50,
    paddingHorizontal: Platform.OS == 'ios' ? 0 : 4,
  },
  dropdownWrapper: {
    width: '100%',
    height: Platform.OS == 'ios' ? 36 : 48,
    borderColor: 'white',
    borderWidth: 1,
    backgroundColor: 'transparent',
    borderRadius: 50,
    marginVertical: 8,
  },
  dropdownItemContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  dropdownLabelContainer: {
    flex: 0.9,
    justifyContent: 'center',
  },
  dropdownLabelText: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    fontWeight: '400',
    color: 'white',
    textAlign: 'left',
    paddingHorizontal: Platform.OS == 'ios' ? 0 : 4,
  },
  dropdownIconContainer: {
    flex: 0.1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  dropdownItemText: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    fontWeight: '400',
    color: 'black',
    textAlign: 'left',
  },
  dropdownSelectedItemText: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    fontWeight: '400',
    color: theme.secondryColor,
    textAlign: 'left',
  },
  innerradius: {
    width: 8,
    height: 8,
    backgroundColor: theme.secondryColor,
    borderRadius: 8 / 2,
  },
  outerradius: {
    width: 16,
    height: 16,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: 'grey',
    borderWidth: 1,
    borderRadius: 16 / 2,
  },
  finalLabelTxt: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    color: theme.white,
    fontWeight: '600',
  },
  accountNoTxt: {
    fontSize: Platform.OS == 'ios' ? 12 : 14,
    color: theme.white,
    fontWeight: 'bold',
  },
  tokenExpiryTxt: {
    fontSize: Platform.OS == 'ios' ? 10 : 12,
    color: theme.white,
    fontWeight: '400',
    lineHeight: 16,
  },
  tokenDateTxt: {
    fontSize: Platform.OS == 'ios' ? 10 : 12,
    color: theme.secondryColor,
    fontWeight: '600',
  },
  plaidConnected: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: theme.successGreen,
    borderRadius: 4,
    marginHorizontal: 8,
    marginVertical: 8,
    paddingHorizontal: 16,
    paddingVertical: 8,
  },
  plaidNotConnected: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FF7171',
    borderRadius: 4,
    marginHorizontal: 8,
    marginVertical: 8,
    paddingHorizontal: 16,
    paddingVertical: 8,
  },
  brokerConnectionbtntext: {
    fontSize: Platform.OS == 'ios' ? 12 : 14,
    color: '#fff',
    textAlign: 'center',
  },
  tdModalContainer: {
    height: Platform.OS == 'ios' ? deviceHeight * 0.75 : deviceHeight * 0.9,
    backgroundColor: theme.white,
    borderRadius: 8,
  },
  tdModalHeaderContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: theme.themeColor,
    borderBottomColor: 'grey',
    borderBottomWidth: 1,
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
    paddingHorizontal: 16,
    paddingVertical: 16,
  },
  balanceTxt: {
    fontSize: Platform.OS == 'ios' ? 12 : 14,
    color: theme.successGreen,
    fontWeight: '800',
  },
  ConnectedPlaid:{
    width: '62.5%',
    alignSelf: 'center',
    backgroundColor: theme.secondryColor,
    borderRadius: 8,
    marginVertical: 8,
    paddingHorizontal: 16,
    paddingVertical: 8,
  },

  sendEmail:{
    width: '62.5%',
    alignSelf: 'center',
    backgroundColor: theme.secondryColor,
    borderRadius: 8,
    marginVertical: 8,
    paddingHorizontal: 16,
    paddingVertical: 8,
  },

  title: {
    fontSize: Platform.OS == 'ios' ? 16 : 18,
    color: theme.white,
    fontWeight: 'bold',
    marginBottom: 8,
  },

  uploadbtn: {
    width: '30%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: theme.secondryColor,
    borderRadius: 24,
    paddingHorizontal: 12,
    paddingVertical: 8,
    marginLeft:9
  },

  sentbtn: {
    width: '48%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: theme.secondryColor,
    borderRadius: 24,
    paddingHorizontal: 12,
    paddingVertical: 8,
alignSelf:'center',
marginTop:15
  },

  uploadtxt: {
    fontSize: Platform.OS == 'ios' ? 12 : 14,
    color: theme.white,
    fontWeight: '400',
    textAlign: 'center',
    marginHorizontal: 4,
  },

  senttxt: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    color: theme.white,
    fontWeight: '700',
    textAlign: 'center',
    marginHorizontal: 4,
  },

  title: {
    fontSize: Platform.OS == 'ios' ? 16 : 18,
    color: theme.white,
    fontWeight: 'bold',
    marginBottom: 8,
  },

  uploadbtn: {
    width: '30%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: theme.secondryColor,
    borderRadius: 24,
    paddingHorizontal: 12,
    paddingVertical: 8,
    marginLeft:9
  },

  sentbtnn: {
    width: '50%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: theme.secondryColor,
    borderRadius: 24,
    paddingHorizontal: 12,
    paddingVertical: 8,
alignSelf:'center',
marginTop:15
  },

  cancelbtn: {
    width: '48%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: theme.secondryColor,
    borderRadius: 24,
    paddingHorizontal: 12,
    paddingVertical: 8,
alignSelf:'center',
marginTop:15
  },

  uploadtxt: {
    fontSize: Platform.OS == 'ios' ? 12 : 14,
    color: theme.white,
    fontWeight: '400',
    textAlign: 'center',
    marginHorizontal: 4,
  },

  senttxt: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    color: theme.white,
    fontWeight: '700',
    textAlign: 'center',
    marginHorizontal: 4,
  },
  btnview: {
    width: '75%',
    alignSelf: 'center',
    backgroundColor: theme.secondryColor,
    borderRadius: 8,
    paddingHorizontal: 16,
    paddingVertical: 8,
  },
  btntext: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  btnview1: {
    backgroundColor: theme.secondryColor,
    borderRadius: 4,
    paddingHorizontal: 12,
    paddingVertical: 6,
  },
  Title: {
    fontSize: Platform.OS == 'ios' ? 16 : 18,
    color: theme.white,
    fontWeight: 'bold',
    marginBottom: 8,
  },
  addBrokerConnection: {
    width: '58%',
    alignSelf: 'center',
    backgroundColor: theme.secondryColor,
    borderRadius: 8,
    marginVertical: 8,
    paddingHorizontal: 16,
    paddingVertical: 8,
},

});

export default styles;
