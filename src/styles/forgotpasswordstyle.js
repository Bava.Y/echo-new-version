import {Platform} from 'react-native';
import {StyleSheet} from 'react-native';
import theme from '../utils/theme';

let style = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: theme.themeColor,
    paddingHorizontal: 20,
    paddingVertical: 20,
  },
  forgottitle: {
    fontSize: Platform.OS == 'ios' ? 18 : 20,
    color: '#fff',
  },
  forgotcontent: {
    color: '#fff',
    marginTop: 20,
    marginBottom: 20,
    fontSize: 16,
    textAlign: 'center',
  },
  signinBtn: {
    flex: 0.375,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: theme.secondryColor,
    borderRadius: 4,
    paddingHorizontal: 8,
    paddingVertical: 8,
  },
  cancelBtn: {
    flex: 0.375,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#2a587f',
    borderWidth: 1,
    borderRadius: 4,
    paddingHorizontal: 8,
    paddingVertical: 8,
  },
  canceltext: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    color: '#2a587f',
  },
  signintext: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    color: '#fff',
  },
  altersignintext: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    color: '#fff',
    marginTop: 6,
    textTransform: 'uppercase',
  },
  socialicon: {
    marginRight: 15,
    height: 25,
    width: 25,
  },
  errormsg: {
    alignSelf: 'flex-start',
    fontSize: Platform.OS == 'ios' ? 12 : 14,
    color: '#ff0000a1',
    marginHorizontal: 4,
    marginVertical: 10,
  },
  errorotp: {
    alignSelf: 'flex-start',
    fontSize: Platform.OS == 'ios' ? 12 : 14,
    color: '#ff0000a1',
    marginHorizontal: 4,
    marginVertical: 10,
  },
  errorotpinvalid: {
    alignSelf: 'flex-start',
    fontSize: Platform.OS == 'ios' ? 12 : 14,
    color: '#ff0000a1',
    marginHorizontal: 4,
    marginVertical: 10,
  },
  errorconf: {
    alignSelf: 'flex-start',
    fontSize: Platform.OS == 'ios' ? 12 : 14,
    color: '#ff0000a1',
    marginHorizontal: 4,
    marginVertical: 10,
  },
  errormismatch: {
    alignSelf: 'flex-start',
    fontSize: Platform.OS == 'ios' ? 12 : 14,
    color: '#ff0000a1',
    marginHorizontal: 4,
    marginVertical: 10,
  },
});

export default style;
