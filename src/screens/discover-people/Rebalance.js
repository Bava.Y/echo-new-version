import {useFocusEffect} from '@react-navigation/native';
import React, {useCallback, useContext, useRef, useState} from 'react';
import {
  Image,
  Platform,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Snackbar from 'react-native-snackbar';
import {portfolio} from '../../api';
import {ALPACA, AMERITRADE} from '../../assets/images/index';
import BottomNav from '../../components/BottomNav';
import HeaderNav from '../../components/HeaderNav';
import Loading from '../../components/Loading';
import {UserDataContext} from '../../context/UserDataContext';
import theme from '../../utils/theme';
import OrderShareAmount from '../Dashboard/order-amount';
import OrderShareQuantity from '../Dashboard/order-quantity';

const Rebalance = (props) => {
  // Props Variables
  const id = props.route.params.id;

  // Rebalance Variables
  const [responseData, setResponseData] = useState(null);
  const [orderData, setOrderData] = useState(null);

  // Context Variables
  const {token} = useContext(UserDataContext);

  // Ref Variables
  const orderRef = useRef();

  useFocusEffect(
    useCallback(() => {
      let isFocus = true;

      getRebalances();

      return () => {
        isFocus = false;
      };
    }, []),
  );

  const getRebalances = async () => {
    const res = await portfolio.getRebalance(id, token);

    if (res) {
      if (res.keyword === 'success') {
        Snackbar.show({duration: Snackbar.LENGTH_SHORT, text: res.message});

        setResponseData(res.data ? res.data : []);
      } else {
        Snackbar.show({duration: Snackbar.LENGTH_SHORT, text: res.message});

        setResponseData([]);
      }
    } else {
      setResponseData([]);
    }
  };

  const isFloat = (n) => {
    return Number(n) % 1 !== 0;
  };

  return (
    <>
      <HeaderNav title="Rebalance" isBack="true" />

      {responseData ? (
        responseData.length !== 0 ? (
          <ScrollView
            keyboardShouldPersistTaps={'handled'}
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
            style={{flex: 1, backgroundColor: theme.themeColor}}>
            <View style={{paddingHorizontal: 8, paddingVertical: 16}}>
              {responseData.map((lol, index) => {
                return (
                  <View key={index} style={{marginVertical: 8}}>
                    <Text
                      style={{
                        fontSize: Platform.OS == 'ios' ? 12 : 14,
                        color: theme.white,
                        fontWeight: '600',
                      }}>
                      <Text
                        style={{
                          fontSize: Platform.OS == 'ios' ? 14 : 16,
                          color: theme.white,
                          fontWeight: '600',
                          textTransform: 'uppercase',
                        }}>
                        {lol.follower.name}
                      </Text>{' '}
                      Portfolio changes: -
                    </Text>

                    {lol.shares.map((shareData, index) => (
                      <View
                        key={index}
                        style={{
                          flex: 1,
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                          backgroundColor: theme.primaryColor,
                          borderRadius: 8,
                          marginTop: 8,
                          padding: 8,
                        }}>
                        <View
                          style={{
                            flex: 0.25,
                            justifyContent: 'center',
                            alignItems: 'center',
                          }}>
                          <Text
                            style={{
                              fontSize: Platform.OS == 'ios' ? 12 : 14,
                              fontWeight: 'bold',
                              color: theme.white,
                              textTransform: 'uppercase',
                            }}>
                            {shareData.symbol}
                          </Text>
                        </View>

                        <View
                          style={{
                            flex: 0.25,
                            justifyContent: 'center',
                            alignItems: 'center',
                          }}>
                          <Text
                            style={{
                              fontSize: Platform.OS == 'ios' ? 10 : 12,
                              color: theme.white,
                            }}>
                            {parseFloat(shareData.quantity).toFixed(
                              isFloat(shareData.quantity) ? 2 : 0,
                            )}{' '}
                            Shares
                          </Text>
                        </View>

                        <View
                          style={{
                            flex: 0.25,
                            justifyContent: 'center',
                            alignItems: 'center',
                          }}>
                          <Image
                            source={
                              shareData.brokerConnection == 1
                                ? AMERITRADE
                                : ALPACA
                            }
                            style={{
                              width: 60,
                              height: 30,
                              resizeMode: 'contain',
                            }}
                          />
                        </View>

                        <View
                          style={{
                            flex: 0.25,
                            justifyContent: 'center',
                            alignItems: 'center',
                          }}>
                          <TouchableOpacity
                            style={{
                              width: '75%',
                              alignItems: 'center',
                              backgroundColor:
                                shareData.type.toLowerCase() === 'buy'
                                  ? theme.successGreen
                                  : theme.danger,
                              borderRadius: 4,
                              paddingHorizontal: 8,
                              paddingVertical: 4,
                            }}
                            onPress={async () => {
                              const orderDetails = {
                                brokerConnection: shareData.brokerConnection,
                                latest_price: shareData.amount,
                                qty: shareData.quantity,
                                side: shareData.type.toLowerCase(),
                                symbol: shareData.symbol.toUpperCase(),
                                fromPage: 'Rebalance',
                                id: shareData.id,
                                creatorId: shareData.creatorId,
                                directUserTotalAmount:
                                  shareData.total_amount_direct_user,
                                directUserPercentage:
                                  shareData.percentage_direct_user,
                                fractionable: shareData.fractionable,
                              };

                              await setOrderData(orderDetails);

                              orderRef.current.open();
                            }}>
                            <Text
                              style={{
                                fontSize: Platform.OS == 'ios' ? 10 : 12,
                                fontWeight: 'bold',
                                color: theme.white,
                                textTransform: 'capitalize',
                              }}>
                              {shareData.type}
                            </Text>
                          </TouchableOpacity>
                        </View>
                      </View>
                    ))}
                  </View>
                );
              })}
            </View>
          </ScrollView>
        ) : (
          <View
            style={{
              flex: 1,
              backgroundColor: theme.themeColor,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontSize: Platform.OS == 'ios' ? 12 : 14,
                color: theme.white,
              }}>
              No rebalance(s) found
            </Text>
          </View>
        )
      ) : (
        <View style={{flex: 1, backgroundColor: theme.themeColor}}>
          <Loading color={theme.white} />
        </View>
      )}

      {orderData && orderData?.fractionable && (
        <OrderShareAmount
          buyform={orderRef}
          orderData={orderData}
          onChange={() => {
            getRebalances();

            orderRef.current.close();
          }}
        />
      )}

      {orderData && !orderData?.fractionable && (
        <OrderShareQuantity
          buyform={orderRef}
          orderData={orderData}
          onChange={() => {
            getRebalances();

            orderRef.current.close();
          }}
        />
      )}

      <BottomNav routeName="Discover People" />
    </>
  );
};

export default Rebalance;
