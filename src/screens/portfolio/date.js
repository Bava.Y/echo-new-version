import moment from 'moment';
import React from 'react';
import {Platform} from 'react-native';
import DatePicker from 'react-native-datepicker';
import Ionicons from 'react-native-vector-icons/Ionicons';
import theme from '../../utils/theme';

const Date = (props) => {
  // Date Variables
  const fromDate = props.fromDate || null;
  const toDate = props.toDate || null;

  return (
    <>
      {/* <DatePicker
        cancelBtnText="Cancel"
        confirmBtnText="Confirm"
        date={fromDate}
        format="MM-DD-YYYY"
        iconComponent={
          <Ionicons name="calendar-outline" size={18} color={theme.white} />
        }
        maxDate={moment().format('MM-DD-YYYY')}
        mode="date"
        onDateChange={(selectedDate) => {
          props.handleDatePicker(selectedDate, 'from');
        }}
        placeholder="From"
        customStyles={{
          dateText: {
            fontSize: Platform.OS == 'ios' ? 12 : 14,
            color: theme.white,
            textAlign: 'center',
          },
          dateInput: {
            alignItems: 'flex-start',
            color: theme.white,
            borderColor: 'transparent',
          },
          datePicker: {
            justifyContent: 'center',
          },
        }}
        style={{
          flex: 0.4,
          backgroundColor: theme.themeColor,
          paddingHorizontal: 8,
          borderColor: 'transparent',
          borderRadius: 4,
        }}
      /> */}

      {/* <DatePicker
        cancelBtnText="Cancel"
        confirmBtnText="Confirm"
        date={toDate}
        format="MM-DD-YYYY"
        iconComponent={
          <Ionicons name="calendar-outline" size={18} color={theme.white} />
        }
        minDate={moment(fromDate, 'MM-DD-YYYY').format('MM-DD-YYYY')}
        maxDate={moment().format('MM-DD-YYYY')}
        mode="date"
        onDateChange={(selectedDate) => {
          props.handleDatePicker(selectedDate, 'to');
        }}
        placeholder="To"
        customStyles={{
          dateText: {
            fontSize: Platform.OS == 'ios' ? 12 : 14,
            color: theme.white,
            textAlign: 'center',
          },
          dateInput: {
            alignItems: 'flex-start',
            color: theme.white,
            borderColor: 'transparent',
          },datePicker: {
            justifyContent: 'center',
          },
        }}
        style={{
          flex: 0.4,
          backgroundColor: theme.themeColor,
          paddingHorizontal: 8,
          borderColor: 'transparent',
          borderRadius: 4,
        }}
      /> */}
    </>
  );
};

export default Date;
