import {useNavigation} from '@react-navigation/native';
import React, {useState} from 'react';
import {
  FlatList,
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

const RadioButtonVertical = ({peer, isVisible, hide}) => {
  const [peers] = useState(peer);

  const renderItem = ({item}) => (
    <Item item={item} isVisible={isVisible} hide={hide} />
  );

  const Item = ({item, isVisible, hide}) => {
    const navigation = useNavigation();

    let isNegative = item.change_percent < 0 ? true : false;

    return (
      <TouchableOpacity
        onPress={() => {
          hide();

          isVisible();

          navigation.push('CompanyDetail', {
            symbol: item.symbol,
            name: item.company_name,
          });
        }}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginHorizontal: 8,
            marginVertical: 8,
          }}>
          <View style={styles.company}>
            <Text
              style={{
                fontSize: Platform.OS == 'ios' ? 12 : 14,
                color: 'black',
                fontWeight: 'bold',
              }}>
              {item.symbol}
            </Text>

            <Text
              style={{
                fontSize: 10,
                color: 'black',
                fontWeight: '400',
                paddingHorizontal: 4,
              }}>
              ({item.company_name})
            </Text>
          </View>

          <View style={styles.percentage}>
            <View
              style={{
                width: 16,
                alignSelf: 'center',
                backgroundColor: isNegative ? 'red' : 'green',
                marginHorizontal: 8,
                paddingVertical: 4,
                borderRadius: 4,
              }}>
              <Icon
                name={isNegative ? 'arrow-down' : 'arrow-up'}
                color="white"
                size={8}
                style={{alignSelf: 'center'}}
              />
            </View>

            <Text style={isNegative ? styles.textred : styles.textgreen}>
              {item.change_percent}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  return peers.length == 0 ? (
    <View
      style={{
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        borderRadius: 8,
        paddingHorizontal: 16,
        paddingVertical: 16,
      }}>
      <Text style={{fontSize: Platform.OS == 'ios' ? 12 : 14}}>
        No peer group(s) available for this company
      </Text>
    </View>
  ) : (
    <View
      style={{
        backgroundColor: 'white',
        borderRadius: 8,
        paddingHorizontal: 8,
        paddingVertical: 8,
      }}>
      <FlatList
        data={peers}
        renderItem={renderItem}
        keyExtractor={(item) => item.symbol}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  company: {
    width: '75%',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  percentage: {
    width: '25%',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  textgreen: {
    fontSize: Platform.OS == 'ios' ? 12 : 14,
    color: 'green',
    fontWeight: '400',
  },
  textred: {
    fontSize: Platform.OS == 'ios' ? 12 : 14,
    color: 'red',
    fontWeight: '400',
  },
});

export default RadioButtonVertical;
