import React from 'react';
import {
  Dimensions,
  KeyboardAvoidingView,
  LogBox,
  Platform,
  Text,
  TextInput,
  TouchableHighlight,
  View,
} from 'react-native';
import theme from '../../utils/theme';
import styles from './style';

LogBox.ignoreAllLogs();

const EditBio = ({onChange, onSubmit, value}) => {
  const windowHeight = Dimensions.get('window').height;

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : null}
      keyboardVerticalOffset={Platform.OS === 'ios' ? 40 : 0}
      style={{flex: 1, backgroundColor: theme.themeColor}}>
      <View style={{marginBottom: 24, paddingHorizontal: 16}}>
        <Text style={styles.title}>Edit Bio</Text>

        <View style={styles.selectfield}>
          <TextInput
            style={[
              styles.selectinput,
              {height: windowHeight * 0.15, borderRadius: 8},
            ]}
            value={value}
            textAlignVertical={'top'}
            multiline={true}
            onChangeText={onChange}
          />
        </View>

        <TouchableHighlight style={styles.saveButton} onPress={onSubmit}>
          <Text style={styles.btntext}>Submit</Text>
        </TouchableHighlight>
      </View>
    </KeyboardAvoidingView>
  );
};

export default EditBio;
