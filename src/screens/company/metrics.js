import React from 'react';
import {Platform, Text, View} from 'react-native';
import {MoneyFormat} from '../../containers/valueConversion';
import theme from '../../utils/theme';

const Metrics = ({metrics}) => {
  return (
    <>
      <View>
        <Text
          style={{
            fontSize: Platform.OS == 'ios' ? 16 : 18,
            color: '#ffffff99',
            fontWeight: '600',
            marginHorizontal: 16,
            marginVertical: 8,
          }}>
          Metrics
        </Text>

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginHorizontal: 8,
          }}>
          <View
            style={{
              width: '45%',
              backgroundColor: theme.primaryColor,
              borderRadius: 8,
              marginHorizontal: 8,
              marginVertical: 8,
            }}>
            <View
              style={{
                borderBottomColor: 'grey',
                borderBottomWidth: 1,
                paddingHorizontal: 8,
                paddingVertical: 8,
              }}>
              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 14 : 16,
                  color: '#ffffff99',
                }}>
                {metrics.close}
              </Text>

              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 14 : 16,
                  color: '#ffffff99',
                  paddingTop: 4,
                }}>
                Prevoius Close
              </Text>
            </View>

            <View
              style={{
                borderBottomColor: 'grey',
                borderBottomWidth: 1,
                paddingHorizontal: 8,
                paddingVertical: 8,
              }}>
              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 14 : 16,
                  color: '#ffffff99',
                }}>
                {metrics.open}
              </Text>

              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 14 : 16,
                  color: '#ffffff99',
                  paddingTop: 4,
                }}>
                Open
              </Text>
            </View>

            <View
              style={{
                borderBottomColor: 'grey',
                borderBottomWidth: 1,
                paddingHorizontal: 8,
                paddingVertical: 8,
              }}>
              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 14 : 16,
                  color: '#ffffff99',
                }}>
                -
              </Text>
              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 14 : 16,
                  color: '#ffffff99',
                  paddingTop: 4,
                }}>
                Bid
              </Text>
            </View>

            <View
              style={{
                borderBottomColor: 'grey',
                borderBottomWidth: 1,
                paddingHorizontal: 8,
                paddingVertical: 8,
              }}>
              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 14 : 16,
                  color: '#ffffff99',
                }}>
                -
              </Text>
              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 14 : 16,
                  color: '#ffffff99',
                  paddingTop: 4,
                }}>
                Ask
              </Text>
            </View>

            <View
              style={{
                paddingHorizontal: 8,
                paddingVertical: 8,
              }}>
              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 14 : 16,
                  color: '#ffffff99',
                }}>
                {metrics.ask} - {metrics.bid}
              </Text>
            </View>
          </View>

          <View
            style={{
              width: '45%',
              backgroundColor: theme.primaryColor,
              borderRadius: 8,
              marginHorizontal: 8,
              marginVertical: 8,
            }}>
            <View
              style={{
                borderBottomColor: 'grey',
                borderBottomWidth: 1,
                paddingHorizontal: 8,
                paddingVertical: 8,
              }}>
              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 14 : 16,
                  color: '#ffffff99',
                }}>
                {MoneyFormat(metrics.market_capital)}
              </Text>

              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 14 : 16,
                  color: '#ffffff99',
                  paddingTop: 4,
                }}>
                Market Cap
              </Text>
            </View>

            <View
              style={{
                borderBottomColor: 'grey',
                borderBottomWidth: 1,
                paddingHorizontal: 8,
                paddingVertical: 8,
              }}>
              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 14 : 16,
                  color: '#ffffff99',
                }}>
                {metrics.beta}
              </Text>

              <Text
                style={{
                  cfontSize: Platform.OS == 'ios' ? 14 : 16,
                  color: '#ffffff99',
                  paddingTop: 4,
                }}>
                Beta
              </Text>
            </View>

            <View
              style={{
                borderBottomColor: 'grey',
                borderBottomWidth: 1,
                paddingHorizontal: 8,
                paddingVertical: 8,
              }}>
              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 14 : 16,
                  color: '#ffffff99',
                }}>
                {metrics.pe_ratio}
              </Text>

              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 14 : 16,
                  color: '#ffffff99',
                  paddingTop: 4,
                }}>
                PE Radio
              </Text>
            </View>

            <View
              style={{
                borderBottomColor: 'grey',
                borderBottomWidth: 1,
                paddingHorizontal: 8,
                paddingVertical: 8,
              }}>
              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 14 : 16,
                  color: '#ffffff99',
                }}>
                {metrics.eps}
              </Text>
              <Text
                style={{
                  cfontSize: Platform.OS == 'ios' ? 14 : 16,
                  color: '#ffffff99',
                  paddingTop: 4,
                }}>
                EPS
              </Text>
            </View>

            <View
              style={{
                paddingHorizontal: 8,
                paddingVertical: 8,
              }}>
              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 14 : 16,
                  color: '#ffffff99',
                }}>
                0.00
              </Text>
            </View>
          </View>
        </View>
      </View>
    </>
  );
};

export default Metrics;
