import {useFocusEffect} from '@react-navigation/core';
import moment from 'moment';
import React, {
  useContext,
  useRef,
  useState,
  useCallback,
  useEffect,
} from 'react';
import {
  FlatList,
  Text,
  TouchableHighlight,
  TouchableOpacity,
  View,
} from 'react-native';
import DatePicker from 'react-native-datepicker';
import {ScrollView} from 'react-native-gesture-handler';
import RBSheet from 'react-native-raw-bottom-sheet';
import Icons from 'react-native-vector-icons/FontAwesome';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {UserDataContext} from '../../context/UserDataContext';
import theme from '../../utils/theme';
import DateTimePicker from '@react-native-community/datetimepicker';

const Filter = props => {
  // Props Variables
  let {filterlist, onChange, routeName, filterListCall} = props;

  // Filter Variables
  const [industry, setShowIndustry] = useState(false);
  const [showSector, setShowSector] = useState(false);
  const {primary, secondaryInd, secondarySect} = useContext(UserDataContext);

  const [indSelected, setIndSelected] = useState(undefined);
  const [sectSelected, setSectSelected] = useState(undefined);
  const [secondarySelected, setSecondarySelected] = useState(null);
  const [secondarySubSelected, setSecondarySubSelected] = useState(null);

  const [datePicker, setDatePicker] = useState(false);
  const [toDatePicker, setDoDatePicker] = useState(false);

  const [date, setDate] = useState(null);
  const [toDate, setDoDate] = useState(null);

  const {
    portfolioTimePeriod,
    setPortfolioTimePeriod,
    setCurrentPageDiscover,
    primarySelected,
    setPrimarySelected,
    discoverApply,
    setDiscoverApply,
  } = useContext(UserDataContext);

  // Ref Variables
  const customFilter = useRef();

  function onDateSelected(event, value) {
    setDatePicker(false);
    setDate(moment(value).format('DD-MM-YYYY'));
    timePeroidOptions.map(item => {
      if (item?.label == 'Custom') {
        item.value = `&from_date=${moment(date, 'MM-DD-YYYY').format(
          'YYYY-MM-DD',
        )}&to_date=${moment(toDate, 'MM-DD-YYYY').format('YYYY-MM-DD')}`;
      }
      setTimePeroidOptions([...timePeroidOptions]);
    });
  }

  function onToDateSelected(event, value) {
    setDoDatePicker(false);
    setDoDate(moment(value).format('DD-MM-YYYY'));
  }

  const addPrimaryQuery = async item => {
    if (primarySelected?.length == 0) {
      item.check = true;
      let ChectItem = [];
      ChectItem?.push(item?.primary_investment_id);
      setPrimarySelected(ChectItem);
    } else {
      item.check = !item.check;
      let ChectItem = [];

      var obj = item?.primary_investment_id;
      ChectItem?.push(obj);
      if (item.check) {
        setPrimarySelected([...primarySelected, ...ChectItem]);
      } else {
        const filter = primarySelected.filter(
          res => res !== item.primary_investment_id,
        );
        setPrimarySelected(filter);
      }
    }
  };

  useFocusEffect(
    useCallback(() => {
      let isFocus = true;

      // setToDate(moment());

      return () => {
        isFocus = false;
      };
    }, [portfolioTimePeriod]),
  );
  useEffect(() => {
    setCurrentPageDiscover(0);
  }, []);
  const [timePeroidOptions, setTimePeroidOptions] = useState([
    {label: '1D', value: '1d'},
    {label: '1W', value: '1w'},
    {label: '1M', value: '1m'},
    {label: '3M', value: '3m'},
    {label: '6M', value: '6m'},
    {label: '1Y', value: '1y'},
    {label: 'Max', value: 'max'},
    {label: 'Custom', value: null},
  ]);

  return (
    <>
      <RBSheet
        ref={filterlist}
        closeOnDragDown={true}
        closeOnPressMask={false}
        closeOnPressBack={true}
        height={480}
        openDuration={250}
        customStyles={{
          container: {
            backgroundColor: theme.themeColor,
            borderTopLeftRadius: 16,
            borderTopRightRadius: 16,
          },
          wrapper: {
            backgroundColor: 'grey',
            opacity: 0.9,
          },
          draggableIcon: {
            display: 'none',
          },
        }}>
        <TouchableOpacity onPress={onChange}>
          <View
            style={{
              alignSelf: 'flex-end',
              paddingHorizontal: 16,
              paddingVertical: 16,
            }}>
            <Ionicons name="close" color="white" size={18} />
          </View>
        </TouchableOpacity>

        <View style={{flexDirection: 'row', justifyContent: 'center'}}>
          <TouchableOpacity
            onPress={() => {
              setCurrentPageDiscover(0);
              setDiscoverApply(true);
              let primaryResult = '';
              let secondaryResult = '';
              let timePeriodResult = '';
              let result = null;

              if (portfolioTimePeriod) {
                setCurrentPageDiscover(0);
                timePeriodResult = `time_period=${portfolioTimePeriod}${
                  timePeroidOptions?.label == 'Custom'
                    ? `&from_date=${moment(fromDate, 'MM-DD-YYYY').format(
                        'YYYY-MM-DD',
                      )}&to_date=${moment(toDate, 'MM-DD-YYYY').format(
                        'YYYY-MM-DD',
                      )}`
                    : ''
                }`.toString();
              }

              if (primarySelected.length) {
                setCurrentPageDiscover(0);
                primaryResult = (
                  'primaryPhilosophy=[' +
                  primarySelected +
                  ']'
                ).toString();
              }

              if (secondarySelected && secondarySubSelected) {
                setCurrentPageDiscover(0);
                secondaryResult = (
                  'secondaryPhilosophy=' +
                  secondarySelected +
                  '&secondary_investment_subcategory_id=' +
                  secondarySubSelected
                ).toString();
              } else {
                secondaryResult = (
                  'secondaryPhilosophy' +
                  (secondarySelected == null ? ' ' : secondarySelected)
                )?.toString();
              }

              if (!!timePeriodResult || !!primaryResult || !!secondaryResult) {
                result =
                  timePeriodResult +
                  '&' +
                  primaryResult +
                  '&' +
                  secondaryResult;
              } else if (!!timePeriodResult) {
                result = timePeriodResult;
              } else if (!!primaryResult) {
                result = primaryResult;
              } else if (!!secondaryResult) {
                result = secondaryResult;
              }

              props?.onFilterApply(result);

              onChange();
            }}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                backgroundColor: theme.secondryColor,
                borderRadius: 16,
                marginHorizontal: 8,
                paddingHorizontal: 16,
                paddingVertical: 8,
              }}>
              <View
                style={{
                  width: 20,
                  height: 20,
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: 'white',
                  borderRadius: 20 / 2,
                }}>
                <Icons name="check" color="black" size={12} />
              </View>
              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 14 : 16,
                  color: 'white',
                  paddingHorizontal: 4,
                }}>
                Apply
              </Text>
            </View>
          </TouchableOpacity>

          {/* <TouchableOpacity
            onPress={() => {
              setPrimarySelected([]);
              setIndSelected(undefined);
              setSectSelected(undefined);
              setSecondarySubSelected(undefined);

              props.onFilterApply(null);

              onChange();
            }}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                backgroundColor: 'red',
                borderRadius: 16,
                marginHorizontal: 8,
                paddingHorizontal: 16,
                paddingVertical: 8,
              }}>
              <View
                style={{
                  width: 20,
                  height: 20,
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: 'white',
                  borderRadius: 20 / 2,
                }}>
                <Icons name="close" color="black" size={12} />
              </View>
              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 14 : 16,
                  color: 'white',
                  paddingHorizontal: 4,
                }}>
                Clear
              </Text>
            </View>
          </TouchableOpacity> */}
        </View>

        <ScrollView
          keyboardShouldPersistTaps={'handled'}
          showsVerticalScrollIndicator={false}
          style={{width: '100%', marginVertical: 8}}>
          <View style={{paddingHorizontal: 16, paddingVertical: 8}}>
            <Text
              style={{
                fontSize: Platform.OS == 'ios' ? 14 : 16,
                color: 'white',
                fontWeight: 'bold',
                marginVertical: 8,
              }}>
              Time Period
            </Text>
            <View style={{paddingLeft: 24}}>
              {timePeroidOptions.map((timeFrame, index) => {
                return (
                  <TouchableOpacity
                    key={index}
                    onPress={() => {
                      setPortfolioTimePeriod(timeFrame?.value);

                      timeFrame?.label == 'Custom' &&
                        customFilter?.current.open();
                    }}>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                      {portfolioTimePeriod == timeFrame?.value && (
                        <Icons
                          name="check"
                          size={18}
                          color="grey"
                          style={{paddingRight: 4}}
                        />
                      )}
                      <Text
                        style={{
                          fontSize: Platform.OS == 'ios' ? 12 : 14,
                          color: 'white',
                          marginVertical: 6,
                        }}>
                        {timeFrame?.label}
                      </Text>
                    </View>
                  </TouchableOpacity>
                );
              })}
            </View>

            {routeName != 'Echoes | Following | Followers' &&
              routeName != 'Individual Returns' && (
                <View style={{paddingVertical: 8}}>
                  <Text
                    style={{
                      fontSize: Platform.OS == 'ios' ? 14 : 16,
                      color: 'white',
                      fontWeight: 'bold',
                      marginVertical: 8,
                    }}>
                    Primary Philosophy
                  </Text>
                  <View style={{paddingLeft: 24}}>
                    {primary?.map(item => (
                      <TouchableOpacity onPress={() => addPrimaryQuery(item)}>
                        <View
                          style={{flexDirection: 'row', alignItems: 'center'}}>
                          {!primarySelected.includes(
                            item?.primary_investment_id,
                          ) ? null : (
                            <Icons
                              name="check"
                              size={18}
                              color="grey"
                              style={{paddingRight: 4}}
                            />
                          )}
                          <Text
                            style={{
                              fontSize: Platform.OS == 'ios' ? 12 : 14,
                              color: 'white',
                              marginVertical: 6,
                            }}>
                            {item.primary_investment_name}
                          </Text>
                        </View>
                      </TouchableOpacity>
                    ))}
                  </View>
                </View>
              )}

            {routeName != 'Echoes | Following | Followers' &&
              routeName != 'Individual Returns' && (
                <View style={{paddingVertical: 8}}>
                  <Text
                    style={{
                      fontSize: Platform.OS == 'ios' ? 14 : 16,
                      color: 'white',
                      fontWeight: 'bold',
                      marginVertical: 8,
                    }}>
                    Secondary Philosophy
                  </Text>
                  <View style={{paddingHorizontal: 8}}>
                    <TouchableOpacity
                      onPress={() => setShowIndustry(!industry)}>
                      <View
                        style={{
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                          alignItems: 'center',
                        }}>
                        <Text
                          style={{
                            fontSize: Platform.OS == 'ios' ? 14 : 16,
                            color: 'white',
                            fontWeight: '600',
                            marginVertical: 8,
                          }}>
                          Industry
                        </Text>
                        <Icons
                          name={industry ? 'angle-up' : 'angle-down'}
                          size={22}
                          color="white"
                        />
                      </View>
                    </TouchableOpacity>
                    {industry && (
                      <View style={{paddingLeft: 16}}>
                        <FlatList
                          showsHorizontalScrollIndicator={false}
                          showsVerticalScrollIndicator={false}
                          data={secondaryInd}
                          initialNumToRender={secondaryInd.length}
                          keyExtractor={(_item, index) => index.toString()}
                          renderItem={({item}) => {
                            return (
                              <TouchableOpacity
                                onPress={() => {
                                  setIndSelected(
                                    item.secondary_investment_subcategory_id,
                                  );
                                  setSectSelected(undefined);
                                  setSecondarySelected(1);
                                  setSecondarySubSelected(
                                    item.secondary_investment_subcategory_id,
                                  );
                                }}>
                                <View
                                  style={{
                                    flexDirection: 'row',
                                    alignItems: 'center',
                                  }}>
                                  {Number(indSelected) ==
                                    Number(
                                      item.secondary_investment_subcategory_id,
                                    ) && (
                                    <Icons
                                      name="check"
                                      size={18}
                                      color="grey"
                                      style={{paddingRight: 4}}
                                    />
                                  )}
                                  <Text
                                    style={{
                                      fontSize: Platform.OS == 'ios' ? 12 : 14,
                                      color: 'white',
                                      marginVertical: 6,
                                    }}>
                                    {item.secondary_investment_subcategory_name}
                                  </Text>
                                </View>
                              </TouchableOpacity>
                            );
                          }}
                        />
                      </View>
                    )}
                  </View>

                  <View style={{paddingHorizontal: 8}}>
                    <TouchableOpacity
                      onPress={() => setShowSector(!showSector)}>
                      <View
                        style={{
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                          alignItems: 'center',
                        }}>
                        <Text
                          style={{
                            fontSize: Platform.OS == 'ios' ? 14 : 16,
                            color: 'white',
                            fontWeight: '600',
                            marginVertical: 8,
                          }}>
                          Sector
                        </Text>
                        <Icons
                          name={showSector ? 'angle-up' : 'angle-down'}
                          size={22}
                          color="white"
                        />
                      </View>
                    </TouchableOpacity>
                    {showSector && (
                      <View style={{paddingLeft: 16}}>
                        <FlatList
                          showsHorizontalScrollIndicator={false}
                          showsVerticalScrollIndicator={false}
                          data={secondarySect}
                          initialNumToRender={secondarySect.length}
                          keyExtractor={(_item, index) => index.toString()}
                          renderItem={({item}) => {
                            return (
                              <TouchableOpacity
                                onPress={() => {
                                  setSectSelected(
                                    item.secondary_investment_subcategory_id,
                                  );
                                  setIndSelected(undefined);
                                  setSecondarySelected(2);
                                  setSecondarySubSelected(
                                    item.secondary_investment_subcategory_id,
                                  );
                                }}>
                                <View
                                  style={{
                                    flexDirection: 'row',
                                    alignItems: 'center',
                                  }}>
                                  {Number(sectSelected) ==
                                    Number(
                                      item.secondary_investment_subcategory_id,
                                    ) && (
                                    <Icons
                                      name="check"
                                      size={18}
                                      color="grey"
                                      style={{paddingRight: 4}}
                                    />
                                  )}
                                  <Text
                                    style={{
                                      fontSize: Platform.OS == 'ios' ? 12 : 14,
                                      color: 'white',
                                      marginVertical: 6,
                                    }}>
                                    {item.secondary_investment_subcategory_name}
                                  </Text>
                                </View>
                              </TouchableOpacity>
                            );
                          }}
                        />
                      </View>
                    )}
                  </View>
                </View>
              )}
          </View>
        </ScrollView>

        <RBSheet
          ref={customFilter}
          closeOnDragDown={true}
          closeOnPressMask={false}
          closeOnPressBack={true}
          openDuration={250}
          customStyles={{
            container: {
              backgroundColor: theme.themeColor,
              borderTopLeftRadius: 16,
              borderTopRightRadius: 16,
            },
            wrapper: {
              backgroundColor: 'grey',
              opacity: 0.9,
            },
            draggableIcon: {
              display: 'none',
            },
          }}>
          <TouchableOpacity
            onPress={() => {
              customFilter.current.close();
            }}>
            <View
              style={{
                alignSelf: 'flex-end',
                paddingHorizontal: 16,
                paddingVertical: 16,
              }}>
              <Ionicons name="close" color="white" size={18} />
            </View>
          </TouchableOpacity>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              paddingHorizontal: 16,
              paddingVertical: 8,
            }}>
            <View style={{flex: 0.475, justifyContent: 'center'}}>
              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 14 : 16,
                  color: '#ffffffc9',
                  paddingHorizontal: 4,
                }}>
                From
              </Text>

              <TouchableOpacity
                onPress={() => {
                  setDatePicker(true);
                }}>
                <View style={{flexDirection: 'row'}}>
                  <Ionicons
                    name="calendar-outline"
                    size={18}
                    color={theme.white}
                  />
                  <Text style={{color: 'white', textAlign: 'center'}}>
                    {Boolean(date) ? date : 'MM-DD-YYYY'}
                  </Text>
                </View>
              </TouchableOpacity>

              {/* <DatePicker
                date={fromDate}
                mode="date"
                placeholder="MM-DD-YYYY"
                format="MM-DD-YYYY"
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                maxDate={moment().format('MM-DD-YYYY')}
                customStyles={{
                  dateIcon: {
                    position: 'absolute',
                    width: 24,
                    height: 24,
                    marginLeft: 0,
                    left: 0,
                  },
                  dateText: {
                    color: 'white',
                    textAlign: 'center',
                  },
                  dateInput: {
                    alignItems: 'flex-start',
                    borderWidth: 0,
                    borderBottomWidth: 1,
                    paddingLeft: 28,
                  },
                  datePicker: {
                    justifyContent: 'center',
                  },
                }}
                onDateChange={date => {
                  setFromDate(date);
                  timePeroidOptions.map(item => {
                    if (item?.label == 'Custom') {
                      item.value = `&from_date=${moment(
                        date,
                        'MM-DD-YYYY',
                      ).format('YYYY-MM-DD')}&to_date=${moment(
                        toDate,
                        'MM-DD-YYYY',
                      ).format('YYYY-MM-DD')}`;
                    }
                    setTimePeroidOptions([...timePeroidOptions]);
                  });
                }}
              /> */}

              {datePicker && (
                <DateTimePicker
                  value={new Date()}
                  display={Platform.OS === 'android' ? 'default' : 'spinner'}
                  onChange={onDateSelected}
                />
              )}
            </View>
            <View style={{flex: 0.475, justifyContent: 'center'}}>
              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 14 : 16,
                  color: '#ffffffc9',
                  paddingHorizontal: 4,
                }}>
                To
              </Text>
              <TouchableOpacity
                onPress={() => {
                  setDoDatePicker(true);
                }}
                //style={Styles.dateContainer}
              >
                <View style={{flexDirection: 'row'}}>
                  <Ionicons
                    name="calendar-outline"
                    size={18}
                    color={theme.white}
                  />
                  <Text style={{color: 'white', textAlign: 'center'}}>
                    {Boolean(toDate) ? toDate : 'MM-DD-YYYY'}
                  </Text>
                </View>
              </TouchableOpacity>

              {toDatePicker && (
                <DateTimePicker
                  value={new Date()}
                  display={Platform.OS === 'android' ? 'default' : 'spinner'}
                  onChange={onToDateSelected}
                />
              )}
              {/* <DatePicker
                date={toDate}
                mode="date"
                placeholder="MM-DD-YYYY"
                format="MM-DD-YYYY"
                minDate={moment(fromDate, 'MM-DD-YYYY').format('MM-DD-YYYY')}
                maxDate={moment().format('MM-DD-YYYY')}
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                customStyles={{
                  dateIcon: {
                    position: 'absolute',
                    width: 24,
                    height: 24,
                    marginLeft: 0,
                    left: 0,
                  },
                  dateText: {
                    color: 'white',
                    textAlign: 'center',
                  },
                  dateInput: {
                    alignItems: 'flex-start',
                    borderWidth: 0,
                    borderBottomWidth: 1,
                    paddingLeft: 28,
                  },
                  datePicker: {
                    justifyContent: 'center',
                  },
                }}
                onDateChange={date => {
                  setToDate(date);
                }}
              /> */}
            </View>
          </View>
          <TouchableHighlight
            style={{
              width: '50%',
              alignSelf: 'center',
              backgroundColor: theme.secondryColor,
              borderRadius: 16,
              marginTop: 20,
              padding: 8,
            }}
            onPress={() => {
              // setFromDate([...fromDate])
              // setToDate(...toDate)
              customFilter.current.close();
            }}>
            <Text
              style={{
                fontSize: Platform.OS == 'ios' ? 14 : 16,
                color: theme.white,
                fontWeight: 'bold',
                textAlign: 'center',
              }}>
              Submit
            </Text>
          </TouchableHighlight>
        </RBSheet>
      </RBSheet>
    </>
  );
};

export default Filter;
