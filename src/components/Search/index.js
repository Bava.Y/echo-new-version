import React from 'react';
import {View} from 'react-native';
import MultiSelect from 'react-native-multiple-select';
import theme from '../../utils/theme';

const SearchInput = ({searchData, fetchCompany, selectedItem}) => {
  return (
    <View style={{borderRadius: 10, marginTop: 20}}>
      <MultiSelect
        hideTags
        items={searchData}
        uniqueKey="id"
        onSelectedItemsChange={fetchCompany}
        selectedItems={selectedItem}
        single={true}
        selectText=""
        searchInputPlaceholderText="Search"
        altFontFamily="ProximaNova-Light"
        tagRemoveIconColor="#CCC"
        tagBorderColor="#CCC"
        tagTextColor="#CCC"
        selectedItemTextColor="white"
        selectedItemIconColor="#CCC"
        itemTextColor="white"
        styleInputGroup={{
          backgroundColor: theme.themeColor,
        }}
        styleSelectorContainer={{
          backgroundColor: theme.themeColor,
        }}
        searchInputStyle={{
          backgroundColor: theme.themeColor,
        }}
        styleMainWrapper={{
          backgroundColor: theme.themeColor,
        }}
        styleListContainer={{
          backgroundColor: theme.themeColor,
          maxHeight: 200,
        }}
        searchInputStyle={{
          backgroundColor: theme.themeColor,
        }}
        styleItemsContainer={{
          backgroundColor: theme.themeColor,
        }}
        styleDropdownMenuSubsection={{
          backgroundColor: 'transparent',
          borderColor: 'transparent',
          paddingLeft: 10,
          height: 30,
          borderRadius: 10,
        }}
        displayKey="name"
        searchInputStyle={{color: 'white'}}
      />
    </View>
  );
};

export default SearchInput;
