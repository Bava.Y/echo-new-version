import AsyncStorage from '@react-native-community/async-storage';
import {useFocusEffect} from '@react-navigation/core';
import React, {useContext, useState, useCallback, useEffect} from 'react';
import {ScrollView, Text, View} from 'react-native';
import {profile} from '../../../api';
import {UserDataContext} from '../../../context/UserDataContext';
import {EMAIL_SEEN, PHONE_SEEN, VISUAL} from '../../../utils/constants';
import Switch from './../components/switch';
import styles from './styles';

const Account = () => {
  const {
    echoingOn,
    setEchoingOn,
    futureOn,
    setFutureOn,
    snapshotOn,
    setSnapshotOn,
    authUserData,
    token,
    preferences,
    setPreferences,
  } = useContext(UserDataContext);

  // Context Variables
  useFocusEffect(
    useCallback(() => {
      let isFocus = true;

      getPreferences();
      return () => {
        isFocus = false;
      };
    }, [preferences]),
  );

  useEffect(() => {
    getPreferences();
    setVisual(preferences?.visual_alluser === 1 ? true : false);
    setEmailSeen(preferences?.email_icon === 1 ? true : false);
    setPhone(preferences?.phone_icon === 1 ? true : false);
    setEchoing(preferences?.echo_feature === 1 ? true : false);
  }, [preferences]);

  // Props Variables
  const [visual, setVisual] = useState(null);
  const [emailSeen, setEmailSeen] = useState(null);
  const [phone, setPhone] = useState(null);
  const [echoing, setEchoing] = useState(null);
  const [echoUser, setEchoUser] = useState(preferences?.customer_id);
 
  // const [percentageAllocation, setPercentageAllocation] = useState(
  //   preferences.echo_percentage_allocation
  //     ? Boolean(preferences.echo_percentage_allocation)
  //     : false,
  // );
  const [amountAllocation, setAmountAllocation] = useState(
    preferences?.echo_amount_allocation
      ? Boolean(preferences?.echo_amount_allocation)
      : true,
  );

  // const getPreferences = async () => {
  //   let preference = await profile.getPreference(token, authUserData.user_id);
  //   setPreferences(preference);
  // };
  // Other Variables

  const getPreferences = async () => {
    let preference = await profile.getPreference(token, authUserData.user_id);

    let findmap = preference?.map((item) => {
      return setPreferences(item);
    });

    setPreferences(preference);
  };
  const toggleVisual = async (current) => {
    let formData = new FormData();
    setVisual(current);

    let status = current == false ? 0 : 1;

    await AsyncStorage.setItem(VISUAL, JSON.stringify(status));

    formData && formData.append('status', status);
    formData && formData.append('type', 'visual_all');
    formData && formData.append('id', authUserData.user_id);

    await profile.updatePreference(token, formData);
  };

  const toggleMailSeen = async (current) => {
    let formData = new FormData();
    setEmailSeen(current);

    let status = current == false ? 0 : 1;

    await AsyncStorage.setItem(EMAIL_SEEN, JSON.stringify(status));

    formData && formData.append('status', status);
    formData && formData.append('type', 'email_icon');
    formData && formData.append('id', authUserData.user_id);

    await profile.updatePreference(token, formData);
  };

  const togglePhone = async (current) => {
    let formData = new FormData();
    setPhone(current);

    let status = current == false ? 0 : 1;

    await AsyncStorage.setItem(PHONE_SEEN, JSON.stringify(status));

    formData && formData.append('status', status);
    formData && formData.append('type', 'phone_icon');
    formData && formData.append('id', authUserData.user_id);

    await profile.updatePreference(token, formData);
  };

  const toggleEchoing = async (current) => {
    let formData = new FormData();
    setEchoing(current);

    let status = current == false ? 0 : 1;

    await AsyncStorage.setItem(EMAIL_SEEN, JSON.stringify(status));
    formData && formData.append('status', status);
    formData && formData.append('type', 'echo_feature');
    formData && formData.append('id', authUserData.user_id);

    await profile.updatePreference(token, formData);
  };

  // const togglePercentageAllocation = async (current) => {
  //   setPercentageAllocation(current);

  //   let status = current == false ? 0 : 1;

  //   formData && formData.append('status', status);
  //   formData && formData.append('type', 'percentage_allocation');
  //   formData && formData.append('id', authUserData.user_id);

  //   await profile.updatePreference(token, formData);
  // };

  const toggleAmountAllocation = async (current) => {
    let formData = new FormData();
    setAmountAllocation(true);

    let status = current == false ? 0 : 1;

    formData && formData.append('status', status);
    formData && formData.append('type', 'amount_allocation');
    formData && formData.append('id', authUserData.user_id);

    await profile.updatePreference(token, formData);
  };

  return (
    <ScrollView
      keyboardShouldPersistTaps={'handled'}
      showsHorizontalScrollIndicator={false}
      showsVerticalScrollIndicator={false}
      style={styles.container}>
      <View style={styles.mainsec}>
        <Text style={styles.headingTxt}>Profile</Text>

        <View style={{marginVertical: 8}}>
          <View style={styles.switchContainer}>
            <View style={styles.switchLabelContainer}>
              <Text style={styles.labelTxt}>
                {visual == true ? ' Public' : ' Private'}
              </Text>
            </View>

            <View style={styles.switchToggleContainer}>
              <Switch
                isEnabled={visual}
                toggleSwitch={(status) => {
                  if (status) {
                    toggleVisual(status);
                  } else {
                    setTimeout(() => {
                      toggleVisual(false);

                      setTimeout(() => {
                        toggleMailSeen(false);

                        setTimeout(() => {
                          togglePhone(false);

                          setTimeout(() => {
                            toggleEchoing(false);
                          }, 50);

                          // setTimeout(() => {
                          //   togglePercentageAllocation(false);
                          // }, 50);

                          setTimeout(() => {
                            toggleAmountAllocation(false);
                          }, 50);
                        }, 50);
                      }, 50);
                    }, 50);
                  }
                }}
              />
            </View>
          </View>
        </View>

        <Text style={styles.headingTxt}>Personal</Text>

        <View style={{marginVertical: 8}}>
          <View style={styles.switchContainer}>
            <View style={styles.switchLabelContainer}>
              <Text style={styles.labelTxt}>
                Email:{emailSeen == true ? ' Public' : ' Private'}
              </Text>
            </View>

            <View style={styles.switchToggleContainer}>
              <Switch isEnabled={emailSeen} toggleSwitch={toggleMailSeen} />
            </View>
          </View>

          <View style={[styles.switchContainer, {marginTop: 4}]}>
            <View style={styles.switchLabelContainer}>
              <Text style={styles.labelTxt}>
                Phone:{phone == true ? ' Public' : ' Private'}
              </Text>
            </View>

            <View style={styles.switchToggleContainer}>
              <Switch isEnabled={phone} toggleSwitch={togglePhone} />
            </View>
          </View>
        </View>

        <Text style={styles.headingTxt}>Echoing</Text>

        <View style={{marginVertical: 8}}>
          <View style={styles.switchContainer}>
            <View style={styles.switchLabelContainer}>
              <Text style={styles.labelTxt}>
                Enable Echoing for your portfolio
              </Text>
            </View>

            <View style={styles.switchToggleContainer}>
              <Switch isEnabled={echoing} toggleSwitch={toggleEchoing} />
            </View>
          </View>

          {/* <View style={[styles.switchContainer, {marginTop: 4}]}>
            <View style={styles.switchLabelContainer}>
              <Text style={styles.labelTxt}>Percentage Allocation</Text>
            </View>

            <View style={styles.switchToggleContainer}>
              <Switch
                isEnabled={percentageAllocation}
                toggleSwitch={(status) => {
                  togglePercentageAllocation(status);

                  setTimeout(() => {
                    toggleAmountAllocation(!status);
                  }, 250);
                }}
              />
            </View>
          </View> */}

          <View style={[styles.switchContainer, {marginTop: 4}]}>
            <View style={styles.switchLabelContainer}>
              <Text style={styles.labelTxt}>Amount Allocation</Text>
            </View>

            <View style={styles.switchToggleContainer}>
              <Switch isEnabled={amountAllocation} />
            </View>
          </View>
        </View>
      </View>

      <View style={{paddingVertical: 16}} />
    </ScrollView>
  );
};

export default Account;
