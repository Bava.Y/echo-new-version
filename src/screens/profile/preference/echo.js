import React, {useContext, useState, useCallback, useEffect} from 'react';
import {useFocusEffect} from '@react-navigation/core';
import {ScrollView, Text, View} from 'react-native';
import {profile} from '../../../api';
import {UserDataContext} from '../../../context/UserDataContext';
import theme from '../../../utils/theme';
import Switch from './../components/switch';
import styles from './styles';

const Echo = () => {
   // Context Variables
   const {preferences, setPreferences, authUserData, token} = useContext(
    UserDataContext,
  );

  useFocusEffect(
    useCallback(() => {
      let isFocus = true;
     
      getPreferences();

      return () => {
        isFocus = false;
      };
    }, [preferences]),
  );

  useEffect(() => {
    getPreferences();
    setEchoMoving(preferences?.echo_movingfwd === 1 ? true : false);
    setSnapshot(preferences?.snapshot === 1 ? true : false);
  }, [preferences]);
  // Props Variables
  const [echoMoving, setEchoMoving] = useState(null);
  const [snapshot, setSnapshot] = useState(null);
  const [hybrid, setHybrid] = useState(null);

 
  // Other Variables

  const toggleEchoMove = async (current) => {
    let formData = new FormData();
    setEchoMoving((previousState) => !previousState);

    let status = current == false ? 0 : 1;

    formData && formData.append('status', status);
    formData && formData.append('type', 'echo_moving');
    formData && formData.append('id', authUserData.user_id);

    await profile.updatePreference(token, formData);
  };

  const toggleSnapshot = async (current) => {
    let formData = new FormData();
    setSnapshot((previousState) => !previousState);

    let status = current == false ? 0 : 1;

    formData && formData.append('status', status);
    formData && formData.append('type', 'snapshot');
    formData && formData.append('id', authUserData.user_id);

    await profile.updatePreference(token, formData);
  };

  const toggleHybrid = async (current) => {
    let formData = new FormData();
    setHybrid((previousState) => !previousState);

    let status = current == false ? 0 : 1;

    formData && formData.append('status', status);
    formData && formData.append('type', 'hybrid');
    formData && formData.append('id', authUserData.user_id);

    await profile.updatePreference(token, formData);
  };

  const getPreferences = async () => {
    let preference = await profile.getPreference(token, authUserData.user_id);
    console.log('preferences?.email_icon>>>>>', preference);
    let findmap = preference?.map((item) => {
      return setPreferences(item);
    });

    setPreferences(preference);
  };

  return (
    <ScrollView
      keyboardShouldPersistTaps={'handled'}
      showsHorizontalScrollIndicator={false}
      showsVerticalScrollIndicator={false}
      style={styles.container}>
      <View style={styles.mainsec}>
        <Text style={styles.paraTxt}>
          When you choose to Echo (or copy) someone, you have the flexibility in
          how that Echoing works.
        </Text>

        <Text style={[styles.paraTxt, {marginTop: 8}]}>
          For the Future Echoing, all investment decisions made for your
          portfolio will be made based on the decisions made by the person you
          are Echoing from the moment you begin Echoing that person until you
          cease doing so.
        </Text>

        <Text style={[styles.paraTxt, {marginTop: 8}]}>
          Snapshot Echoing empowers you to Echo a user’s portfolio as it is in
          that moment, and does not cause you to make any subsequent purchases
          or sales based on the user in question
        </Text>

        <Text
          style={[
            styles.headingTxt,
            {color: theme.secondryColor, marginTop: 16},
          ]}>
          Echoing Settings
        </Text>

        <View style={{marginVertical: 8}}>
          <View style={styles.switchContainer}>
            <View style={styles.switchLabelContainer}>
              <Text style={styles.labelTxt}>Future Echoing</Text>
            </View>

            <View style={styles.switchToggleContainer}>
              <Switch isEnabled={echoMoving} toggleSwitch={toggleEchoMove} />
            </View>
          </View>

          <View style={[styles.switchContainer, {marginTop: 4}]}>
            <View style={styles.switchLabelContainer}>
              <Text style={styles.labelTxt}>Snapshot</Text>
            </View>

            <View style={styles.switchToggleContainer}>
              <Switch isEnabled={snapshot} toggleSwitch={toggleSnapshot} />
            </View>
          </View>
        </View>

        {/* <View style={[styles.switchContainer, {marginTop: 4}]}>
          <View style={styles.switchLabelContainer}>
            <Text style={styles.labelTxt}>Hybrid</Text>
          </View>

          <View style={styles.switchToggleContainer}>
            <Switch isEnabled={hybrid} toggleSwitch={toggleHybrid} />
          </View>
        </View> */}
      </View>

      <View style={{paddingVertical: 12}} />
    </ScrollView>
  );
};

export default Echo;
