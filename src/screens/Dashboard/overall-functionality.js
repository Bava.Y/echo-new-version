import React, {useContext, useRef} from 'react';
import {Platform} from 'react-native';
import {Text, TouchableOpacity, View} from 'react-native';
import RBSheet from 'react-native-raw-bottom-sheet';
import Snackbar from 'react-native-snackbar';
import Icon from 'react-native-vector-icons/FontAwesome';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {dashboard} from '../../api';
import {UserDataContext} from '../../context/UserDataContext';
import theme from '../../utils/theme';
import Functionality from './functionality';

const OverallFunctionality = ({
  functionalitylist,
  onChange,
  toggle,
  isActive,
}) => {
  const {
    setTickerVisible,
    tickerVisible,
    token,
    fourChartSymbols,
    singleChartSymbol,
  } = useContext(UserDataContext);

  const ChartlistPopup = useRef();

  const postTickerVisible = () => {
    const tempTicker = !tickerVisible;

    setTickerVisible(tempTicker);

    let formData = new FormData();

    formData && formData.append('ticker', tempTicker);

    onChange();
  };

  const sendDefault = async () => {
    let formData = new FormData();

    formData && formData.append('default_chart', isActive);
    formData && formData.append('single_chart', singleChartSymbol);
    formData && formData.append('four_chart', JSON.stringify(fourChartSymbols));

    const res = await dashboard.SendDefaultDashboard(formData);

    res &&
      Snackbar.show({
        duration: Snackbar.LENGTH_SHORT,
        text: res.message,
        backgroundColor: 'green',
      });
  };

  return (
    <RBSheet
      ref={functionalitylist}
      closeOnDragDown={true}
      closeOnPressMask={false}
      closeOnPressBack={true}
      height={280}
      openDuration={250}
      customStyles={{
        container: {
          backgroundColor: theme.themeColor,
          borderTopLeftRadius: 16,
          borderTopRightRadius: 16,
        },
        wrapper: {
          backgroundColor: 'grey',
          opacity: 0.9,
        },
        draggableIcon: {
          display: 'none',
        },
      }}>
      <TouchableOpacity onPress={onChange}>
        <View
          style={{
            alignSelf: 'flex-end',
            paddingHorizontal: 16,
            paddingVertical: 16,
          }}>
          <Ionicons name="close" color="white" size={18} />
        </View>
      </TouchableOpacity>

      <View style={{paddingHorizontal: 16}}>
        <TouchableOpacity
          onPress={() => {
            sendDefault();
            onChange();
          }}
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            marginBottom: 8,
          }}>
          <View
            style={{
              width: 32,
              height: 32,
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: theme.secondryColor,
              borderRadius: 32 / 2,
            }}>
            <Icon name="thumb-tack" size={16} color="white" />
          </View>
          <Text
            style={{
              fontSize: Platform.OS == 'ios' ? 14 : 16,
              color: 'white',
              paddingHorizontal: 8,
            }}>
            Secure Defaults on Chart
          </Text>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => {
            isActive === 'four' ? toggle(true) : toggle(false);
            onChange();
          }}
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            marginVertical: 8,
          }}>
          <View
            style={{
              width: 32,
              height: 32,
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: theme.secondryColor,
              borderRadius: 32 / 2,
            }}>
            <Icon name="exchange" size={16} color="white" />
          </View>
          <Text
            style={{
              fontSize: Platform.OS == 'ios' ? 14 : 16,
              color: 'white',
              paddingHorizontal: 8,
            }}>
            {isActive === 'four'
              ? 'Toggle to Single Chart'
              : 'Toggle to 4 Charts'}
          </Text>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => {
            ChartlistPopup.current.open();
          }}
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            marginVertical: 8,
          }}>
          <View
            style={{
              width: 32,
              height: 32,
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: theme.secondryColor,
              borderRadius: 32 / 2,
            }}>
            <Text
              style={{
                fontSize: Platform.OS == 'ios' ? 16 : 18,
                color: 'white',
                fontWeight: 'bold',
              }}>
              D
            </Text>
          </View>
          <Text
            style={{
              fontSize: Platform.OS == 'ios' ? 14 : 16,
              color: 'white',
              paddingHorizontal: 8,
            }}>
            Set as Default Chart
          </Text>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => {
            postTickerVisible();
            onChange();
          }}
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            marginTop: 8,
          }}>
          <View
            style={{
              width: 32,
              height: 32,
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: theme.secondryColor,
              borderRadius: 32 / 2,
            }}>
            <Ionicons name="film" size={18} color="white" />
          </View>
          <Text
            style={{
              fontSize: Platform.OS == 'ios' ? 14 : 16,
              color: 'white',
              paddingHorizontal: 8,
            }}>
            {tickerVisible ? 'Hide Ticker' : 'Show Ticker'}
          </Text>
        </TouchableOpacity>
      </View>

      <Functionality
        chartlist={ChartlistPopup}
        onChange={() => ChartlistPopup.current.close()}
      />
    </RBSheet>
  );
};

export default OverallFunctionality;
