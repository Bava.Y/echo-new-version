import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import React, {useEffect} from 'react';
import {BackHandler, View} from 'react-native';
import BottomNav from '../../components/BottomNav';
import HeaderNav from '../../components/HeaderNav';
import theme from '../../utils/theme';
import MyTabBar from './components/corpActionTab';
import CorporateActionScreen from './corporateAction';
import styles from './style';

const CorpAction = ({navigation, route}) => {
  const Tab = createMaterialTopTabNavigator();

  useEffect(() => {
    let ac = new AbortController();

    BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);

    return () => {
      BackHandler.removeEventListener(
        'hardwareBackPress',
        handleBackButtonClick,
      );

      ac.abort();
    };
  }, []);

  function handleBackButtonClick() {
    navigation.goBack();

    return true;
  }

  const company_name = route.params.companyName;
  const title = `${company_name} - Corp Action`;

  return (
    <>
      <HeaderNav title={title} isBack="true" routeName="Company" />

      <View style={styles.container}>
        <Tab.Navigator
          style={{backgroundColor: theme.primaryColor}}
          sceneContainerStyle={{backgroundColor: theme.themeColor}}
          tabBar={(props) => <MyTabBar {...props} />}
          swipeEnabled={true} //can change dynamically
          initialRouteName="Dividends"
          backBehavior="initialRoute"
          animationEnabled={true}
          tabBarOptions={{scrollEnabled: true}}>
          <Tab.Screen
            name="Dividends"
            component={CorporateActionScreen}
            initialParams={{name: 'advancedDividends'}}
          />

          <Tab.Screen
            name="Bonus Issue"
            component={CorporateActionScreen}
            initialParams={{name: 'advancedBonus'}}
          />

          <Tab.Screen
            name="Distribution"
            component={CorporateActionScreen}
            initialParams={{name: 'advancedDistribution'}}
          />

          <Tab.Screen
            name="Return of Capital"
            component={CorporateActionScreen}
            initialParams={{name: 'advancedReturnOfCapital'}}
          />

          <Tab.Screen
            name="Rights Issue"
            component={CorporateActionScreen}
            initialParams={{name: 'advancedRights'}}
          />

          <Tab.Screen
            name="Right to Purchase"
            component={CorporateActionScreen}
            initialParams={{name: 'advancedRightToPurchase'}}
          />

          <Tab.Screen
            name="Security Reclassification"
            component={CorporateActionScreen}
            initialParams={{name: 'advancedSecurityReclassification'}}
          />

          <Tab.Screen
            name="Security Swap"
            component={CorporateActionScreen}
            initialParams={{name: 'advancedSecuritySwap'}}
          />

          <Tab.Screen
            name="Spinoff"
            component={CorporateActionScreen}
            initialParams={{name: 'advancedSpinoff'}}
          />

          <Tab.Screen
            name="Splits"
            component={CorporateActionScreen}
            initialParams={{name: 'advancedSplits'}}
          />
        </Tab.Navigator>
      </View>

      <BottomNav routeName="Dashboard" />
    </>
  );
};

export default CorpAction;
