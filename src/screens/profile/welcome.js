import React from 'react';
import {Text, View} from 'react-native';
import Modal from 'react-native-modal';
import styles from './style';

const Welcome = ({
  visibility,
  onChange,
  text,
  stepText,
  startText,
  onPress,
  isProfile,
}) => {
  return (
    <Modal animationType="fade" transparent={true} isVisible={visibility}>
      <View style={styles.welcomecontainer}>
        <Text style={styles.welcometxt}>{text}</Text>

        <Text style={styles.steptext}>{stepText}</Text>

        <View
          style={styles.startbtn}
          onStartShouldSetResponder={isProfile == true ? onPress : onChange}>
          <Text style={styles.starttxt}>{startText}</Text>
        </View>
      </View>
    </Modal>
  );
};

export default Welcome;
