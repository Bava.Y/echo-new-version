import {StyleSheet} from 'react-native';
import theme from '../utils/theme';

const styles = StyleSheet.create({
  scene: {
    flex: 1,
  },
  container: {
    flex: 1,
    backgroundColor: theme.themeColor,
  },
  modalBackground: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height: 100,
    width: 100,
  },
  activityIndicatorWrapper: {
    backgroundColor: theme.primaryColor,
    height: 100,
    width: 100,
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    alignContent: 'center',
    alignSelf: 'center',
    top: 0,
    bottom: 0,
  },
  tabheader: {
    flexDirection: 'row',
  },

  tabcolumn: {
    width: '33%',
    padding: 15,
    borderWidth: 0.8,
    borderColor: theme.black,
  },
  tabtitle: {
    textAlign: 'center',
    color: theme.white,
    fontSize: 16,
  },
  active: {
    color: theme.secondryColor,
    fontWeight: 'bold',
  },
  activeBorder: {
    borderBottomColor: theme.secondryColor,
    borderBottomWidth: 3,
  },
  applogo: {
    width: 60,
    height: 60,
    borderRadius: 50,
    // borderWidth: 1,
    // borderColor: theme.secondryColor
  },
  userdetails: {
    flexDirection: 'row',
    backgroundColor: theme.secondryColor,
    borderWidth: 0,
  },
  actioncolumn: {
    width: '10%',
  },
  cameraicon: {
    marginTop: 30,
    marginLeft: -10,
  },
  actionicon: {
    marginTop: 30,
    marginRight: 15,
  },
  bioicon: {
    width: '10%',
  },
  username: {
    color: theme.white,
    fontWeight: 'bold',
    fontSize: 18,
  },
  usermail: {
    color: theme.white,
    fontSize: 16,
  },
  user: {
    flexDirection: 'row',
    padding: 10,
    width: '90%',
  },
  m10: {
    marginLeft: 10,
    marginTop: 7,
  },
  btnview: {
    backgroundColor: theme.primaryColor,
    paddingVertical: 20,
    width: '47%',
    margin: 5,
  },
  selecttext: {
    color: theme.white,
    textAlign: 'center',
    fontSize: 14,
  },
  actioncolumn: {
    flexDirection: 'row',
  },
  welcometxt: {
    color: theme.secondryColor,
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: 16,
  },
  steptext: {
    color: theme.white,
    textAlign: 'center',
    fontSize: 16,
    marginTop: 20,
    marginBottom: 20,
  },
  welcomecontainer: {
    backgroundColor: theme.primaryColor,
    padding: 20,
    borderWidth: 1,
    borderColor: theme.secondryColor,
    borderRadius: 10,
    margin: 10,
  },
  startbtn: {
    backgroundColor: theme.secondryColor,
    padding: 10,
    alignSelf: 'center',
    borderRadius: 7,
    paddingHorizontal: 15,
  },
  starttxt: {
    color: theme.black,
    fontSize: 16,
  },
  socialcontainer: {
    padding: 10,
  },
  title: {
    fontSize: 18,
    color: theme.white,
    marginBottom: 10,
  },
  list: {
    flexDirection: 'row',
    borderBottomWidth: 0.7,
    borderBottomColor: 'black',
    padding: 10,
  },
  socialtitle: {
    color: theme.white,
    fontSize: 16,
    marginLeft: 10,
  },
  socialstatus: {
    color: theme.white,
    fontSize: 16,
    textAlign: 'right',
  },
  tabStyle: {
    backgroundColor: theme.themeColor,
  },
  field: {
    marginTop: 15,
  },
  fieldlabel: {
    color: theme.white,
    fontSize: 16,
  },
  fieldValue: {
    borderBottomWidth: 1,
    borderBottomColor: 'white',
    color: theme.white,
  },
  fieldinput: {
    borderBottomWidth: 1,
    borderBottomColor: 'white',
    color: 'white',
  },
  financial: {
    backgroundColor: theme.primaryColor,
    padding: 20,
  },
  bio: {
    margin: 15,
    backgroundColor: theme.primaryColor,
    padding: 10,
    borderRadius: 10,
    paddingHorizontal: 10,
    paddingVertical: 10,
    borderBottomWidth: 1,
    borderBottomColor: 'black',
  },
  biodata: {
    color: theme.white,
    textAlign: 'justify',
    lineHeight: 22,
  },
  email: {
    color: 'white',
  },
  overlay: {
    backgroundColor: 'rgba(0,0,0,0.2)',
    flex: 1,
    justifyContent: 'flex-end',
  },
  count: {
    fontSize: 18,
    color: theme.secondryColor,
    fontWeight: 'bold',
  },
  followersection: {
    marginHorizontal: 15,
    paddingBottom: 20,
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: 'black',
  },
  followname: {
    fontSize: 16,
    color: theme.white,
  },
  followers: {
    width: '33.3%',
    alignItems: 'center',
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    margin: 10,
    backgroundColor: theme.primaryColor,
    borderRadius: 10,
    padding: 10,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    marginTop: '40%',
    justifyContent: 'center',
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  modalView1: {
    width: '100%',
    margin: 10,
    backgroundColor: theme.primaryColor,
    borderRadius: 10,
    padding: 10,
    height: 500,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    marginTop: 20,
    justifyContent: 'center',
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },

  openButton: {
    backgroundColor: '#F194FF',
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  saveButton: {
    paddingHorizontal: 50,
    paddingVertical: 10,
    marginTop: 15,
    marginBottom: 15,
  },
  cancelButton: {
    paddingHorizontal: 50,
    paddingVertical: 10,
    marginTop: 15,
    marginBottom: 15,
    marginRight: 10,
  },
  btntext: {
    color: theme.black,
    fontWeight: 'bold',
    textAlign: 'center',
    textTransform: 'uppercase',
  },
  border: {
    //   borderWidth: 1,
    // borderColor: 'black',
    marginVertical: 10,
    marginHorizontal: 5,
  },
  errorMsg: {
    fontWeight: 'bold',
    fontSize: 14,
    color: '#ff0000a1',
    textAlign: 'left',
    marginLeft: 10,
  },

  tabhead: {
    flexDirection: 'row',
    backgroundColor: '#00000038',
    opacity: 0.8,
    margin: 10,
  },
  tabheading: {
    textAlign: 'center',
    alignSelf: 'center',
    color: 'white',
    width: '33.3%',
    fontWeight: 'bold',
    paddingHorizontal: 10,
    paddingVertical: 5,
  },
  activetabheading: {
    textAlign: 'center',
    alignSelf: 'center',
    width: '33.3%',
    backgroundColor: 'white',
    fontWeight: 'bold',
    color: theme.secondryColor,
    backgroundColor: theme.white,
    paddingHorizontal: 10,
    paddingVertical: 5,
  },
});

export default styles;
