import React, {useState} from 'react';
import {
  KeyboardAvoidingView,
  LogBox,
  Platform,
  Text,
  TouchableHighlight,
  View,
} from 'react-native';
import theme from '../../utils/theme';
import styles from './style';
import SelectDropdown from 'react-native-select-dropdown';
import FontAwesomeIcons from 'react-native-vector-icons/FontAwesome5';
import {useFocusEffect} from '@react-navigation/native';
import {useCallback} from 'react';

LogBox.ignoreAllLogs();

const SecondaryInvestment = ({
  options,
  value,
  onChange,
  subOptions,
  subValue,
  onSubChange,
  onSecondarySubmit,
  validationError,
  enabled,
}) => {
  // SecondaryInvestment Variables
  const [catergory, setCatergory] = useState(null);
  const [subCatergory, setSubCatergory] = useState(null);

  useFocusEffect(
    useCallback(() => {
      loadCategory(value ?? null);
    }, [subValue]),
  );

  const loadCategory = (value) => {
    if (Boolean(options) && Array.isArray(options) && options.length != 0) {
      const filteredCategory = options.filter((lol) => lol?.value == value);

      setCatergory(filteredCategory.length != 0 ? filteredCategory[0] : null);
    } else {
      setCatergory(null);
    }
  };

  useFocusEffect(
    useCallback(() => {
      loadSubCategory(subValue ?? null);
    }, [subValue]),
  );

  const loadSubCategory = (value) => {
    if (
      Boolean(subOptions) &&
      Array.isArray(subOptions) &&
      subOptions.length != 0
    ) {
      const filteredSubCategory = subOptions.filter(
        (lol) => lol?.value == value,
      );

      setSubCatergory(
        filteredSubCategory.length != 0 ? filteredSubCategory[0] : null,
      );
    } else {
      setSubCatergory(null);
    }
  };

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : null}
      keyboardVerticalOffset={Platform.OS === 'ios' ? 40 : 0}
      style={{flex: 1, backgroundColor: theme.themeColor}}>
      <View style={{marginBottom: 24, paddingHorizontal: 16}}>
        <Text style={styles.title}>Secondary Investment Philosophy</Text>

        <SelectDropdown
          buttonStyle={styles.dropdownWrapper}
          data={options ?? []}
          defaultButtonText="Secondary Investment Category"
          dropdownStyle={{width: '80%', marginLeft: 8, alignSelf: 'center'}}
          onSelect={onChange}
          renderCustomizedButtonChild={(selectedItem) => {
            return (
              <View style={styles.dropdownItemContainer}>
                <View style={styles.dropdownLabelContainer}>
                  <Text style={styles.dropdownLabelText}>
                    {selectedItem?.label ?? 'Secondary Investment Category'}
                  </Text>
                </View>
                <View style={styles.dropdownIconContainer}>
                  <FontAwesomeIcons
                    name="caret-down"
                    size={16}
                    color={'white'}
                  />
                </View>
              </View>
            );
          }}
          rowStyle={{borderBottomWidth: 0}}
          rowTextForSelection={(item) => item?.label}
          rowTextStyle={styles.dropdownItemText}
          selectedRowTextStyle={styles.dropdownSelectedItemText}
          defaultValue={catergory}
        />

        {validationError.secondary ? (
          <Text style={[styles.errorMsg, {marginVertical: 4}]}>
            {validationError.secondary}
          </Text>
        ) : null}

        <SelectDropdown
          buttonStyle={styles.dropdownWrapper}
          data={subOptions ?? []}
          defaultButtonText="Secondary Investment Sub Category"
          dropdownStyle={{width: '80%', marginLeft: 8, alignSelf: 'center'}}
          onSelect={onSubChange}
          renderCustomizedButtonChild={(selectedItem) => {
            return (
              <View style={styles.dropdownItemContainer}>
                <View style={styles.dropdownLabelContainer}>
                  <Text style={styles.dropdownLabelText}>
                    {selectedItem?.label ?? 'Secondary Investment Sub Category'}
                  </Text>
                </View>
                <View style={styles.dropdownIconContainer}>
                  <FontAwesomeIcons
                    name="caret-down"
                    size={16}
                    color={'white'}
                  />
                </View>
              </View>
            );
          }}
          rowStyle={{borderBottomWidth: 0}}
          rowTextForSelection={(item) => item?.label}
          rowTextStyle={styles.dropdownItemText}
          selectedRowTextStyle={styles.dropdownSelectedItemText}
          defaultValue={subCatergory}
        />

        {/* {validationError.secondarySub ? (
          <Text style={[styles.errorMsg, {marginVertical: 4}]}>
            {validationError.secondarySub}
          </Text>
        ) : null} */}

        <TouchableHighlight
          style={styles.saveButton}
          onPress={onSecondarySubmit}>
          <Text style={styles.btntext}>Submit</Text>
        </TouchableHighlight>
      </View>
    </KeyboardAvoidingView>
  );
};

export default SecondaryInvestment;
