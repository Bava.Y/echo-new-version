import {StackActions} from '@react-navigation/native';
import jwt from 'jwt-decode';
import React, {useState} from 'react';
import {
  KeyboardAvoidingView,
  Platform,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import LITERALS from '../../assets/strings';
import Loading from '../../components/Loading';
import OTPView from '../../components/OTPView';
import theme from '../../utils/theme';
import style from './styles.js';

const MPinOTPScreen = ({navigation, route}) => {
  let otp = route.params.data.otp;
  let userInfo = jwt(route.params.data.token);

  const [otpLogin, setOtp] = useState('');
  const [validationError, setValidationError] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');
  const [loading, setLoading] = useState(false);

  const onTextChange = (text) => {
    setValidationError(false);
    setOtp(text);
  };

  const handleClick = async () => {
    setLoading(true);

    const isFirst = 'no';

    if (otpLogin.length === 4 && otp == otpLogin) {
      navigation.dispatch(
        StackActions.replace(isFirst == 'no' ? 'Dashboard' : 'Profile'),
      );
    } else if (otpLogin.length !== 4) {
      setErrorMessage('Enter proper OTP');
      setValidationError(true);
    } else if (otp !== otpLogin) {
      setErrorMessage('Enter valid OTP');
      setValidationError(true);
    }

    setLoading(false);
  };

  return (
    <>
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : null}
        keyboardVerticalOffset={Platform.OS === 'ios' ? 40 : 0}
        style={style.container}>
        <Text style={style.verifyText}>{LITERALS.verify}</Text>

        <Text
          style={{
            fontSize: Platform.OS == 'ios' ? 14 : 16,
            fontFamily: 'Roboto-Regular',
            color: 'white',
            marginTop: 8,
          }}>{`${LITERALS.sendTo} +1 - xxxxxxxx${
          userInfo.mobile ? userInfo.mobile.substr(8, 9) : '89'
        }`}</Text>

        <OTPView
          tintColor="#f0a223"
          handleTextChange={onTextChange}
          otpValue={otp}
        />

        <View style={style.containerResend}>
          <Text
            style={{
              fontSize: Platform.OS == 'ios' ? 14 : 16,
              fontFamily: 'Roboto-Regular',
              color: 'white',
            }}>
            {LITERALS.dontreceive}
          </Text>
        </View>

        {validationError ? (
          <Text
            style={{
              fontSize: Platform.OS == 'ios' ? 12 : 14,
              color: 'red',
              marginVertical: 10,
            }}>
            {errorMessage}
          </Text>
        ) : null}

        <TouchableOpacity activeOpacity={0.7} onPress={handleClick}>
          <View style={style.continueBtn}>
            <Text
              style={{
                fontSize: Platform.OS == 'ios' ? 14 : 16,
                color: 'white',
                fontFamily: 'Roboto-Regular',
              }}>
              {LITERALS.continue}
            </Text>
          </View>
        </TouchableOpacity>

        <Text
          style={{fontSize: Platform.OS == 'ios' ? 14 : 16, color: 'white'}}>
          {otp}
        </Text>
      </KeyboardAvoidingView>

      {loading && <Loading color={theme.white} />}
    </>
  );
};

export default MPinOTPScreen;
