import React, {useContext, useState} from 'react';
import {ScrollView, Text, View} from 'react-native';
import {profile} from '../../../api';
import {UserDataContext} from '../../../context/UserDataContext';
import Switch from './../components/switch';
import styles from './styles';

const FinancialTool = ({preferences}) => {
  // Props Variables
  const [tool, setTool] = useState(
    preferences.financial_tools != undefined
      ? Boolean(preferences.financial_tools)
      : false,
  );
  const [vm, setVM] = useState(
    preferences.vm_button != undefined ? Boolean(preferences.vm_button) : false,
  );
  const [sum, setSum] = useState(
    preferences.summation_tool != undefined
      ? Boolean(preferences.summation_tool)
      : false,
  );
  const [financial, setFinancial] = useState(
    preferences.comparsion_tool != undefined
      ? Boolean(preferences.comparsion_tool)
      : false,
  );

  // Context Variables
  const {authUserData, token} = useContext(UserDataContext);

  // Other Variables
  let formData = new FormData();

  const toggleFinTool = async (current) => {
    setTool((previousState) => !previousState);

    let status = current == false ? 0 : 1;

    if (current == false) {
      setVM(false);
      setFinancial(false);
      setSum(false);
    }

    if (current == true) {
      setVM(true);
      setFinancial(true);
      setSum(true);
    }

    formData && formData.append('status', status);
    formData && formData.append('type', 'finance_tools');
    formData && formData.append('id', authUserData.user_id);

    await profile.updatePreference(token, formData);

    formData && formData.append('status', status);
    formData && formData.append('type', 'vm_button');
    formData && formData.append('id', authUserData.user_id);

    await profile.updatePreference(token, formData);

    formData && formData.append('status', status);
    formData && formData.append('type', 'summation_tool');
    formData && formData.append('id', authUserData.user_id);

    await profile.updatePreference(token, formData);

    formData && formData.append('status', status);
    formData && formData.append('type', 'metric_tool');
    formData && formData.append('id', authUserData.user_id);

    await profile.updatePreference(token, formData);

    formData && formData.append('status', status);
    formData && formData.append('type', 'comparsion_tool');
    formData && formData.append('id', authUserData.user_id);

    await profile.updatePreference(token, formData);
  };

  const toggleVM = async (current) => {
    setVM((previousState) => !previousState);

    let status = current == false ? 0 : 1;

    formData && formData.append('status', status);
    formData && formData.append('type', 'vm_button');
    formData && formData.append('id', authUserData.user_id);

    await profile.updatePreference(token, formData);
  };

  const toggleSum = async (current) => {
    setSum((previousState) => !previousState);

    let status = current == false ? 0 : 1;

    formData && formData.append('status', status);
    formData && formData.append('type', 'summation_tool');
    formData && formData.append('id', authUserData.user_id);

    await profile.updatePreference(token, formData);
  };

  const toggleLit = async (current) => {
    setFinancial((previousState) => !previousState);

    let status = current == false ? 0 : 1;

    formData && formData.append('status', status);
    formData && formData.append('type', 'comparsion_tool');
    formData && formData.append('id', authUserData.user_id);

    await profile.updatePreference(token, formData);
  };

  return (
    <ScrollView
      keyboardShouldPersistTaps={'handled'}
      showsHorizontalScrollIndicator={false}
      showsVerticalScrollIndicator={false}
      style={styles.container}>
      <View style={styles.mainsec}>
        <View style={{marginVertical: 8}}>
          <View style={styles.switchContainer}>
            <View style={styles.switchLabelContainer}>
              <Text style={styles.headingTxt}>Financial Tools</Text>
            </View>

            <View style={styles.switchToggleContainer}>
              <Switch isEnabled={tool} toggleSwitch={toggleFinTool} />
            </View>
          </View>
        </View>

        <View style={{marginVertical: 8}}>
          <View style={styles.switchContainer}>
            <View style={styles.switchLabelContainer}>
              <Text style={styles.labelTxt}>Value Metrics</Text>
            </View>

            <View style={styles.switchToggleContainer}>
              <Switch isEnabled={vm} toggleSwitch={toggleVM} />
            </View>
          </View>

          <View style={[styles.switchContainer, {marginTop: 4}]}>
            <View style={styles.switchLabelContainer}>
              <Text style={styles.labelTxt}>Summation Tool</Text>
            </View>

            <View style={styles.switchToggleContainer}>
              <Switch isEnabled={sum} toggleSwitch={toggleSum} />
            </View>
          </View>

          <View style={[styles.switchContainer, {marginTop: 4}]}>
            <View style={styles.switchLabelContainer}>
              <Text style={styles.labelTxt}>Comparision Tool</Text>
            </View>

            <View style={styles.switchToggleContainer}>
              <Switch isEnabled={financial} toggleSwitch={toggleLit} />
            </View>
          </View>
        </View>
      </View>

      <View style={{paddingVertical: 12}} />
    </ScrollView>
  );
};

export default FinancialTool;
