import React, {Component} from 'react';
import {ActivityIndicator, View} from 'react-native';
import Snackbar from 'react-native-snackbar';
import WebView from 'react-native-webview';
import {payment, profile} from '../../api/index';
import theme from '../../utils/theme';

export default class Payment extends Component {
  constructor(props) {
    super(props);

    this.state = {
      formData: this.props.route.params.formData,
      depositAmount: this.props.route.params.depositAmount,
      token: this.props.route.params.token,
      accessToken: null,
      approvalUrl: null,
      paymentId: null,
      canGoBack: false,
      count: 0,
    };
  }

  componentDidMount = async () => {
    const dataDetail = {
      intent: 'sale',
      payer: {
        payment_method: 'paypal',
      },
      transactions: [
        {
          amount: {
            total: this.state.depositAmount,
            currency: 'USD',
            details: {
              subtotal: this.state.depositAmount,
              tax: '0',
              shipping: '0',
              handling_fee: '0',
              shipping_discount: '0',
              insurance: '0',
            },
          },
        },
      ],
      redirect_urls: {
        return_url: 'https://example.com',
        cancel_url: 'https://example.com',
      },
    };

    const accessTokenResponse = await payment.getAccessToken({
      grant_type: 'client_credentials',
    });

    if (accessTokenResponse) {
      await this.setState({
        accessToken: accessTokenResponse.data.access_token,
      });

      const createPaymentResponse = await payment.createPayment(
        dataDetail,
        accessTokenResponse.data.access_token,
      );

      if (createPaymentResponse) {
        const {id, links} = createPaymentResponse.data;

        const approvalUrl = links.find((data) => data.rel == 'approval_url');

        await this.setState({
          paymentId: id,
          approvalUrl: approvalUrl.href,
        });
      } else {
      }
    } else {
    }
  };

  _onNavigationStateChange = async (webViewState) => {
    if (webViewState.url.includes('https://example.com')) {
      if (this.state.count == 0) {
        await this.setState({count: 1});

        const res = await profile.fundDeposit(
          this.state.token,
          this.state.formData,
        );

        Snackbar.show({
          text: res.message,
          duration: Snackbar.LENGTH_SHORT,
        });

        this.props.navigation.goBack();
      }
    }
  };

  render() {
    const {accessToken, approvalUrl} = this.state;

    return accessToken && approvalUrl ? (
      <View style={{flex: 1, backgroundColor: theme.themeColor}}>
        <WebView
          source={{uri: approvalUrl}}
          onNavigationStateChange={(webViewState) => {
            this._onNavigationStateChange(webViewState);
          }}
          javaScriptEnabled={true}
          domStorageEnabled={true}
          startInLoadingState={true}
          contentMode="mobile"
          renderLoading={() => (
            <ActivityIndicator color="black" size="large" style={{flex: 0.5}} />
          )}
        />
      </View>
    ) : (
      <View
        style={{
          flex: 1,
          backgroundColor: theme.themeColor,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <ActivityIndicator size={'large'} color={theme.white} />
      </View>
    );
  }
}
