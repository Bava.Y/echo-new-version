import React, {useContext, useRef} from 'react';
import {
  ActivityIndicator,
  Alert,
  Dimensions,
  Platform,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Modal from 'react-native-modal';
import Snackbar from 'react-native-snackbar';
import Icon from 'react-native-vector-icons/FontAwesome';
import WebView from 'react-native-webview';
import {ameritrade, profile} from '../api';
import {UserDataContext} from '../context/UserDataContext';
import {AMERITRADE_CLIENT_ID} from '../utils/constants';
import theme from '../utils/theme';

const AmeritradeRefreshToken = (props) => {
  // Context Variables
  const {token} = useContext(UserDataContext);

  // Ref Variables
  const webView = useRef(null);

  // Other Variables
  let uri = `https://auth.tdameritrade.com/auth?response_type=code&redirect_uri=http%3A%2F%2Fechoapp.biz%2F&client_id=${AMERITRADE_CLIENT_ID}%40AMER.OAUTHAP`;
  const deviceHeight = Dimensions.get('window').height;

  const displayActivityIndicator = () => {
    return <ActivityIndicator size={24} color={theme.primaryColor} />;
  };

  return (
    <Modal
      isVisible={props.modalStatus}
      hasBackdrop={true}
      onBackdropPress={() => {
        props.close();
      }}>
      <View
        style={{
          height:
            Platform.OS == 'ios' ? deviceHeight * 0.75 : deviceHeight * 0.9,
          backgroundColor: theme.white,
          borderRadius: 8,
        }}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
            backgroundColor: theme.themeColor,
            borderBottomColor: 'grey',
            borderBottomWidth: 1,
            borderTopLeftRadius: 8,
            borderTopRightRadius: 8,
            paddingHorizontal: 16,
            paddingVertical: 16,
          }}>
          <Text
            style={{
              fontSize: Platform.OS == 'ios' ? 16 : 18,
              color: 'white',
              fontWeight: 'bold',
            }}>
            TD Ameritrade Login
          </Text>

          <TouchableOpacity
            onPress={() => {
              props.close();
            }}>
            <Icon name="close" size={18} color="white" />
          </TouchableOpacity>
        </View>

        <WebView
          source={{uri}}
          ref={webView}
          startInLoadingState={true}
          renderLoading={() => {
            return displayActivityIndicator();
          }}
          cacheEnabled={true}
          style={{borderBottomLeftRadius: 8, borderBottomRightRadius: 8}}
          onNavigationStateChange={async (state) => {
            if (state.url !== uri) {
              uri = state.url;

              if (uri.includes('https://echoapp.biz/?code=')) {
                Snackbar.show({
                  duration: Snackbar.LENGTH_INDEFINITE,
                  text: `Please wait! Your TD Ameritrade account is syncing!`,
                });

                const authorizationCode = decodeURIComponent(
                  uri.split('code=')[1],
                );

                const response = await ameritrade.generateAuthorizationCode(
                  authorizationCode,
                );

                if (response) {
                  const accountsResponse = await ameritrade.getAccounts(
                    response.access_token,
                  );

                  if (
                    Array.isArray(accountsResponse) &&
                    accountsResponse.length !== 0
                  ) {
                    const status = accountsResponse.some(
                      (lol) =>
                        lol.securitiesAccount.accountId ==
                        props.ameritradeSelectedAccountId,
                    );

                    if (status) {
                      let formData = new FormData();

                      formData.append('broker_connection', '1');
                      formData.append('refresh_token', response.refresh_token);
                      formData.append(
                        'expires_in',
                        response.refresh_token_expires_in,
                      );
                      formData.append(
                        'account_id',
                        props.ameritradeSelectedAccountId,
                      );

                      const updateResponse = await profile.updateBroker(
                        token,
                        formData,
                      );

                      Snackbar.dismiss();

                      if (updateResponse.keyword == 'success') {
                        Alert.alert(
                          'Confirmation',
                          'TD account re-syncing successfully',
                        );

                        props.getBankDetail(1);

                        props.close(true);
                      } else {
                        Alert.alert('Error', 'Failed to update token');

                        props.close(true);
                      }
                    } else {
                      Snackbar.dismiss();

                      Alert.alert(
                        'Error',
                        'Sorry! Unable to re-sync your account! Your TD Ameritrade account number is mismatching!',
                      );

                      props.close(true);
                    }
                  } else {
                    Snackbar.show({
                      duration: Snackbar.LENGTH_LONG,
                      text: 'Sorry! No account(s) found for syncing!',
                      backgroundColor: 'red',
                    });

                    props.close(true);
                  }
                } else {
                  Snackbar.show({
                    duration: Snackbar.LENGTH_LONG,
                    text: 'Failed to get authorization code',
                    backgroundColor: 'red',
                  });

                  props.close(true);
                }
              }
            }
          }}
        />
      </View>
    </Modal>
  );
};

export default AmeritradeRefreshToken;
