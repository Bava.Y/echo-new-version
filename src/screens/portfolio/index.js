import {useFocusEffect} from '@react-navigation/core';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import React, {useCallback, useContext, useState} from 'react';
import {View} from 'react-native';
import BottomNav from '../../components/BottomNav';
import HeaderNav from '../../components/HeaderNav';
import Loading from '../../components/Loading';
import {UserDataContext} from '../../context/UserDataContext';
import theme from '../../utils/theme';
import MystockTab from './mystock';
import PeopleTab from './people';
import PortfolioTab from './portfolio';
import styles from './style';
import MyTabBar from './Tab';
import SelectDropdown from 'react-native-select-dropdown';
import FontAwesomeIcons from 'react-native-vector-icons/FontAwesome5';
import listView from './dropList';

const listof = ['Alpaca', 'TD Ameritrade'];
const Portfolio = ({route}) => {
  // Portfolio Variables
  const Tab = createMaterialTopTabNavigator();

  // Context Varibles
  const [broker, setBroker] = useState(null);
  const {setPortfolioConnection,} = useContext(UserDataContext);

  // Other Variables
  const [loading, setLoading] = useState(false);

  useFocusEffect(
    useCallback(() => {
      let ac = new AbortController();

      setLoading(true);

      setPortfolioConnection('0');
      setTimeout(() => {
        setLoading(false);
      }, 10);

      return () => {
        ac.abort();
      };
    }, []),
  );
  const selectTap = route.params ? 'My Stocks' : 'portfolio';

  return (
    <>
      <HeaderNav
        title="Portfolio"
        isBack="true"
        routeName="Portfolio"
        drop={<listView />}
        ha="Close"
      />

      {/* {loading ? (
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: theme.themeColor,
          }}>
          <Loading color="white" />
        </View>
      ) : ( */}
      <View style={styles.container}>
        <Tab.Navigator
          style={{backgroundColor: theme.primaryColor}}
          sceneContainerStyle={{backgroundColor: theme.themeColor}}
          tabBar={(props) => <MyTabBar {...props} />}
          swipeEnabled={false} //can change dynamically
          initialRouteName={selectTap}
          backBehavior="initialRoute"
          animationEnabled={true}
          tabBarOptions={{scrollEnabled: false}}
          onIndexChange={(index) => this.setState({index})}>
          <Tab.Screen name="Portfolio" component={PortfolioTab} />

          <Tab.Screen name="My Stocks" component={MystockTab}/>

          <Tab.Screen name="People" component={PeopleTab} />
        </Tab.Navigator>
      </View>
      {/* )} */}

      <BottomNav routeName="Portfolio" /> 
    </>
  );
};

export default Portfolio;

/*<SelectDropdown
         buttonStyle={styles.listBroker}
         buttonTextStyle={styles.listBrokerText}
          data={listof}
          dropdownStyle={{alignSelf: 'center',}}
         defaultButtonText="CLICK"
         onSelect={(selectedItem) => {
          setBroker(selectedItem)
         }}
         renderDropdownIcon={() => (
          <FontAwesomeIcons name="caret-down" size={16} color={'white'}/>
         )}
         rowStyle={{borderBottomWidth: 0}}
         rowTextStyle={styles.brokerItemText}
         selectedRowTextStyle={styles.listBrokerSelectedItemText}
        />*/
