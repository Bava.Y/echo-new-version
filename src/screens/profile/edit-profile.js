import {useFocusEffect} from '@react-navigation/native';
import _ from 'lodash';
import React, {useCallback, useRef, useState} from 'react';
import {
  KeyboardAvoidingView,
  LogBox,
  Platform,
  ScrollView,
  Text,
  TextInput,
  TouchableHighlight,
  View,
} from 'react-native';
import SelectDropdown from 'react-native-select-dropdown';
import FontAwesomeIcons from 'react-native-vector-icons/FontAwesome5';
import {profile} from '../../api';
import theme from '../../utils/theme';
import styles from './style';

const initialvalidationErrors = {
  fName: false,
  lName: false,
  email: false,
  phone: false,
  state: false,
  ethnicity: false,
  experience: false,
  income: false,
  designation: false,
  company: false,
};

LogBox.ignoreAllLogs();

const EditProfile = ({
  userData,
  id,
  selectData,
  photoUrl,
  closeRbSheet,
  token,
  basicPopup,
  basicUpdate,
  overAllUpdate,
}) => {
  const [validationError, setValidationError] = useState({
    ...initialvalidationErrors,
  });

  const first = useRef();
  const last = useRef();
  const mail = useRef();
  const phoneNum = useRef();
  const design = useRef();
  const comp = useRef();
  
  const [firstname, SetFirstname] = useState(userData.first_name);
  const [lastname, SetLastname] = useState(userData.last_name);
  const [email, SetEmail] = useState(userData.email);
  const [phone, SetPhone] = useState(userData.phone);
  const [state, setState] = useState(null);
  const [gender, setGender] = useState(null);
  const [ethnicity, setEthinicity] = useState(null);
  const [income, setIncome] = useState(null);
  const [experience, setExperience] = useState(null);
  const [compName, setCompName] = useState(
    !_.isEmpty(userData.company_name) && !_.isNull(userData.company_name)
      ? userData.company_name
      : null,
  );

  const [designation, SetDesignation] = useState(
    !_.isEmpty(userData.designation) && !_.isNull(userData.designation)
      ? userData.designation
      : null,
  );

  useFocusEffect(
    useCallback(() => {
      loadState(userData?.resident_state ?? null);

      loadGender(userData?.gender ?? null);

      loadEthnicity(userData?.ethnicity ?? null);

      loadIncomeLevel(userData?.income_level ?? null);

      loadYearsOfExperience(userData?.years_emp_invest ?? null);
    }, []),
  );

  const loadState = (value) => {
    if (
      Boolean(selectData?.state) &&
      Array.isArray(selectData.state) &&
      selectData.state.length != 0
    ) {
      const filteredState = selectData.state.filter(
        (lol) => lol?.value == value,
      );

      setState(filteredState.length != 0 ? filteredState[0] : null);
    } else {
      setState(null);
    }
  };

  const loadGender = (value) => {
    if (
      Boolean(selectData?.gender) &&
      Array.isArray(selectData.gender) &&
      selectData.gender.length != 0
    ) {
      const filteredGender = selectData.gender.filter(
        (lol) => lol?.value == value,
      );

      setGender(filteredGender.length != 0 ? filteredGender[0] : null);
    } else {
      setGender(null);
    }
  };

  const loadEthnicity = (value) => {
    if (
      Boolean(selectData?.ethnicity) &&
      Array.isArray(selectData.ethnicity) &&
      selectData.ethnicity.length != 0
    ) {
      const filteredEthnicity = selectData.ethnicity.filter(
        (lol) => lol?.value == value,
      );

      setEthinicity(
        filteredEthnicity.length != 0 ? filteredEthnicity[0] : null,
      );
    } else {
      setEthinicity(null);
    }
  };

  const loadIncomeLevel = (value) => {
    if (
      Boolean(selectData?.income) &&
      Array.isArray(selectData.income) &&
      selectData.income.length != 0
    ) {
      const filteredIncome = selectData.income.filter(
        (lol) => lol?.value == value,
      );

      setIncome(filteredIncome.length != 0 ? filteredIncome[0] : null);
    } else {
      setIncome(null);
    }
  };

  const loadYearsOfExperience = (value) => {
    if (
      Boolean(selectData?.experience) &&
      Array.isArray(selectData.experience) &&
      selectData.experience.length != 0
    ) {
      const filteredExperience = selectData.experience.filter(
        (lol) => lol?.value == value,
      );

      setExperience(
        filteredExperience.length != 0 ? filteredExperience[0] : null,
      );
    } else {
      setExperience(null);
    }
  };

  const updateBasicInfo = async () => {
    let formData = await validateFields();

    if (!_.isUndefined(formData)) {
      closeRbSheet();
      basicPopup(true);
      let res = await profile.updateBasicInfo(token, formData);

      basicUpdate();
      setTimeout(() => {
        basicPopup(false);
      }, 2000);
      overAllUpdate(res.is_overall_update);
      SetFirstname(res.first_name);
      SetLastname(res.last_name);
      SetEmail(res.email);
      SetPhone(res.phone);
      loadState(res.resident_state);
      loadGender(res.gender);
      loadEthnicity(res.ethnicity);
      loadIncomeLevel(res.income_level);
      loadYearsOfExperience(res.years_emp_invest);
      setCompName(res.company_name);
      SetDesignation(res.designation);
    }
  };

  const validateFields = () => {
    let formData = new FormData();
    let error = validationError;

    formData && formData.append('id', id);

    if (firstname && firstname.trim().length == 0) {
      error['fName'] = 'Enter first name';
      first.current.focus();
      formData = undefined;
    } else {
      formData.append('first_name', firstname);
    }

    if (lastname && lastname.trim().length == 0) {
      error['lName'] = 'Enter last name';
      formData = undefined;
      last.current.focus();
    } else {
      formData.append('last_name', lastname);
    }

    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

    if (email && email.trim().length == 0) {
      error['email'] = 'Enter email';
      formData = undefined;
      mail.current.focus();
    } else if (reg.test(email) === false) {
      error['email'] = 'Enter valid email';
      formData = undefined;
    } else {
      formData.append('email', email);
    }

    if (phone && phone.trim().length == 0) {
      error['phone'] = 'Enter phone';
      formData = undefined;
      phoneNum.current.focus();
    } else {
      formData.append('phone', phone);
    }

    if (compName && compName.trim().length == 0) {
      error['company'] = 'Enter company name';
      formData = undefined;
      comp.current.focus();
    } else {
      formData.append('company_name', compName);
    }

    if (designation && designation.trim().length == 0) {
      error['designation'] = 'Enter designation';
      formData = undefined;
      design.current.focus();
    } else {
      formData.append('designation', designation);
    }

    formData && formData.append('resident_state', state?.value ?? null);
    formData && formData.append('gender', gender?.value ?? null);
    formData && formData.append('ethnicity', ethnicity?.value ?? null);
    formData && formData.append('income_level', income?.value ?? null);
    formData &&
      formData.append(
        'years_emp_invest_id',
        Boolean(experience) && Boolean(experience.value)
          ? parseInt(experience.value)
          : null,
      );

    formData && formData.append('photo', photoUrl);
    formData && formData.append('is_basic_update', 1);

    setValidationError({...validationError, ...error});

    return formData;
  };

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : null}
      keyboardVerticalOffset={Platform.OS === 'ios' ? 40 : 0}
      style={{flex: 1, backgroundColor: theme.themeColor}}>
      <ScrollView
        keyboardShouldPersistTaps="handled"
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
        style={{marginBottom: 24, paddingHorizontal: 16}}>
        <Text style={styles.title}>Edit Basic Info</Text>

        <View
          style={[
            styles.selectfield,
            {
              flexDirection: 'row',
              justifyContent: 'space-between',
            },
          ]}>
          <View style={{width: '47.5%', justifyContent: 'center'}}>
            <TextInput
              ref={(point) => {
                first.current = point;
              }}
              placeholder="First Name"
              placeholderTextColor="white"
              style={styles.selectinput}
              onChangeText={(text) => {
                SetFirstname(text);

                setValidationError({
                  ...validationError,
                  fName:
                    !text || text.trim().length == 0
                      ? 'Enter first name'
                      : false,
                });
              }}
              value={firstname}
            />

            {validationError.fName ? (
              <Text style={styles.errorMsg}>{validationError.fName}</Text>
            ) : null}

            {!validationError.fName && validationError.lName ? (
              <Text style={styles.errorMsg}></Text>
            ) : null}
          </View>

          <View style={{width: '47.5%', justifyContent: 'center'}}>
            <TextInput
              ref={(point) => {
                last.current = point;
              }}
              placeholder="Last Name"
              placeholderTextColor="white"
              style={styles.selectinput}
              onChangeText={(text) => {
                SetLastname(text);
                setValidationError({
                  ...validationError,
                  lName:
                    !text || text.trim().length == 0
                      ? 'Enter last name'
                      : false,
                });
              }}
              value={lastname}
            />

            {validationError.lName ? (
              <Text style={styles.errorMsg}>{validationError.lName}</Text>
            ) : null}

            {!validationError.lName && validationError.fName ? (
              <Text style={styles.errorMsg}></Text>
            ) : null}
          </View>
        </View>

        <View style={styles.selectfield}>
          <TextInput
            ref={(point) => {
              phoneNum.current = point;
            }}
            keyboardType="number-pad"
            placeholder="Phone"
            placeholderTextColor="white"
            style={styles.selectinput}
            onChangeText={(text) => {
              SetPhone(text);

              setValidationError({
                ...validationError,
                phone: !text || text.trim().length == 0 ? 'Enter phone' : false,
              });
            }}
            value={phone}
          />
        </View>

        {validationError.phone ? (
          <Text style={styles.errorMsg}>{validationError.phone}</Text>
        ) : null}

        <View style={styles.selectfield}>
          <TextInput
            ref={(point) => {
              mail.current = point;
            }}
            keyboardType="email-address"
            placeholder="Email"
            placeholderTextColor="white"
            style={styles.selectinput}
            onChangeText={(text) => {
              SetEmail(text);

              let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
              if (reg.test(text) === false) {
                setValidationError({
                  ...validationError,
                  email: 'Enter valid email',
                });
              } else {
                setValidationError({
                  ...validationError,
                  email:
                    !text || text.trim().length == 0 ? 'Enter email' : false,
                });
              }
            }}
            value={email}
          />
        </View>

        {validationError.email ? (
          <Text style={styles.errorMsg}>{validationError.email}</Text>
        ) : null}

        <SelectDropdown
          buttonStyle={styles.dropdownWrapper}
          data={selectData?.state ?? []}
          defaultButtonText="State of Residency"
          dropdownStyle={{width: '80%', marginLeft: 8, alignSelf: 'center'}}
          onSelect={(selectedItem) => {
            setState(selectedItem);
          }}
          renderCustomizedButtonChild={(selectedItem) => {
            return (
              <View style={styles.dropdownItemContainer}>
                <View style={styles.dropdownLabelContainer}>
                  <Text style={styles.dropdownLabelText}>
                    {selectedItem?.label ?? 'State of Residency'}
                  </Text>
                </View>
                <View style={styles.dropdownIconContainer}>
                  <FontAwesomeIcons
                    name="caret-down"
                    size={16}
                    color={'white'}
                  />
                </View>
              </View>
            );
          }}
          rowStyle={{borderBottomWidth: 0}}
          rowTextForSelection={(item) => item?.label}
          rowTextStyle={styles.dropdownItemText}
          selectedRowTextStyle={styles.dropdownSelectedItemText}
          defaultValue={state}
        />

        <SelectDropdown
          buttonStyle={styles.dropdownWrapper}
          data={selectData?.gender ?? []}
          defaultButtonText="Gender"
          dropdownStyle={{width: '80%', marginLeft: 8, alignSelf: 'center'}}
          onSelect={(selectedItem) => {
            setGender(selectedItem);
          }}
          renderCustomizedButtonChild={(selectedItem) => {
            return (
              <View style={styles.dropdownItemContainer}>
                <View style={styles.dropdownLabelContainer}>
                  <Text style={styles.dropdownLabelText}>
                    {selectedItem?.label ?? 'Gender'}
                  </Text>
                </View>
                <View style={styles.dropdownIconContainer}>
                  <FontAwesomeIcons
                    name="caret-down"
                    size={16}
                    color={'white'}
                  />
                </View>
              </View>
            );
          }}
          rowStyle={{borderBottomWidth: 0}}
          rowTextForSelection={(item) => item?.label}
          rowTextStyle={styles.dropdownItemText}
          selectedRowTextStyle={styles.dropdownSelectedItemText}
          defaultValue={gender}
        />



        <SelectDropdown
          buttonStyle={styles.dropdownWrapper}
          data={selectData?.ethnicity ?? []}
          defaultButtonText="Ethnicity"
          dropdownStyle={{width: '80%', marginLeft: 8, alignSelf: 'center'}}
          onSelect={(selectedItem) => {
            setEthinicity(selectedItem);
          }}
          renderCustomizedButtonChild={(selectedItem) => {
            return (
              <View style={styles.dropdownItemContainer}>
                <View style={styles.dropdownLabelContainer}>
                  <Text style={styles.dropdownLabelText}>
                    {selectedItem?.label ?? 'Ethnicity'}
                  </Text>
                </View>
                <View style={styles.dropdownIconContainer}>
                  <FontAwesomeIcons
                    name="caret-down"
                    size={16}
                    color={'white'}
                  />
                </View>
              </View>
            );
          }}
          rowStyle={{borderBottomWidth: 0}}
          rowTextForSelection={(item) => item?.label}
          rowTextStyle={styles.dropdownItemText}
          selectedRowTextStyle={styles.dropdownSelectedItemText}
          defaultValue={ethnicity}
        />

        <SelectDropdown
          buttonStyle={styles.dropdownWrapper}
          data={selectData?.income ?? []}
          defaultButtonText="Income Level"
          dropdownStyle={{width: '80%', marginLeft: 8, alignSelf: 'center'}}
          onSelect={(selectedItem) => {
            setIncome(selectedItem);
          }}
          renderCustomizedButtonChild={(selectedItem) => {
            return (
              <View style={styles.dropdownItemContainer}>
                <View style={styles.dropdownLabelContainer}>
                  <Text style={styles.dropdownLabelText}>
                    {selectedItem?.label ?? 'Income Level'}
                  </Text>
                </View>
                <View style={styles.dropdownIconContainer}>
                  <FontAwesomeIcons
                    name="caret-down"
                    size={16}
                    color={'white'}
                  />
                </View>
              </View>
            );
          }}
          rowStyle={{borderBottomWidth: 0}}
          rowTextForSelection={(item) => item?.label}
          rowTextStyle={styles.dropdownItemText}
          selectedRowTextStyle={styles.dropdownSelectedItemText}
          defaultValue={income}
        />

        <SelectDropdown
          buttonStyle={styles.dropdownWrapper}
          data={selectData?.experience ?? []}
          defaultButtonText="Years of Investment Exp"
          dropdownStyle={{width: '80%', marginLeft: 8, alignSelf: 'center'}}
          onSelect={(selectedItem) => {
            setExperience(selectedItem);
          }}
          renderCustomizedButtonChild={(selectedItem) => {
            return (
              <View style={styles.dropdownItemContainer}>
                <View style={styles.dropdownLabelContainer}>
                  <Text style={styles.dropdownLabelText}>
                    {selectedItem?.label ?? 'Years of Investment Exp'}
                  </Text>
                </View>
                <View style={styles.dropdownIconContainer}>
                  <FontAwesomeIcons
                    name="caret-down"
                    size={16}
                    color={'white'}
                  />
                </View>
              </View>
            );
          }}
          rowStyle={{borderBottomWidth: 0}}
          rowTextForSelection={(item) => item?.label}
          rowTextStyle={styles.dropdownItemText}
          selectedRowTextStyle={styles.dropdownSelectedItemText}
          defaultValue={experience}
        />

        <View style={styles.selectfield}>
          <TextInput
            ref={(point) => {
              comp.current = point;
            }}
            placeholder="Company Name"
            placeholderTextColor="white"
            style={styles.selectinput}
            onChangeText={(text) => {
              setCompName(text);

              setValidationError({
                ...validationError,
                company:
                  !text || text.trim().length == 0
                    ? 'Enter company name'
                    : false,
              });
            }}
            value={compName}
          />
        </View>

        {validationError.company ? (
          <Text style={styles.errorMsg}>{validationError.company}</Text>
        ) : null}

        <View style={styles.selectfield}>
          <TextInput
            ref={(point) => {
              design.current = point;
            }}
            placeholder="Designation"
            placeholderTextColor="white"
            style={styles.selectinput}
            onChangeText={(text) => {
              SetDesignation(text);

              setValidationError({
                ...validationError,
                designation:
                  !text || text.trim().length == 0
                    ? 'Enter designation'
                    : false,
              });
            }}
            value={designation}
          />
        </View>

        {validationError.designation ? (
          <Text style={styles.errorMsg}>{validationError.designation}</Text>
        ) : null}

        <TouchableHighlight style={styles.saveButton} onPress={updateBasicInfo}>
          <Text style={styles.btntext}>Submit</Text>
        </TouchableHighlight>
      </ScrollView>
    </KeyboardAvoidingView>
  );
};

export default EditProfile;
