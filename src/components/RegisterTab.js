import React, {useEffect, useRef} from 'react';
import {Platform, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import theme from '../utils/theme';

const MyTabBar = ({state, descriptors, navigation}) => {
  // MyTabBar Variables
  const focusedOptions = descriptors[state.routes[state.index].key].options;

  // Ref Variables
  const scrollViewRef = useRef(null);

  useEffect(() => {
    scrollViewRef.current.scrollTo({
      x: state.index * 100,
      y: 0,
      animated: true,
    });
  }, [state.index]);

  if (focusedOptions.tabBarVisible === false) {
    return null;
  }

  return (
    <View>
      <ScrollView
        horizontal={true}
        ref={(list) => (scrollViewRef.current = list)}
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            paddingHorizontal: 8,
            paddingVertical: 8,
          }}>
          {state.routes.map((route, index) => {
            const {options} = descriptors[route.key];

            const label =
              options.tabBarLabel !== undefined
                ? options.tabBarLabel
                : options.title !== undefined
                ? options.title
                : route.name;

            const isFocused = state.index === index;

            const onPress = () => {
              const event = navigation.emit({
                type: 'tabPress',
                target: route.key,
                canPreventDefault: true,
              });

              if (!isFocused && !event.defaultPrevented) {
                navigation.navigate(route.name); //can change dynamically
              }
            };

            const onLongPress = () => {
              navigation.emit({
                type: 'tabLongPress',
                target: route.key,
              });
            };

            return (
              <TouchableOpacity
                key={index}
                accessibilityRole="button"
                accessibilityState={isFocused ? {selected: true} : {}}
                accessibilityLabel={options.tabBarAccessibilityLabel}
                testID={options.tabBarTestID}
                onPress={onPress}
                onLongPress={onLongPress}>
                <View
                  style={{
                    backgroundColor: theme.themeColor,
                    borderColor: isFocused
                      ? theme.secondryColor
                      : theme.themeColor,
                    borderWidth: isFocused ? 1 : 0,
                    borderRadius: 50,
                    marginHorizontal: 4,
                    paddingHorizontal: 16,
                    paddingVertical: 8,
                  }}>
                  <Text
                    style={{
                      fontSize: Platform.OS == 'ios' ? 12 : 14,
                      color: 'white',
                      fontWeight: 'bold',
                      textAlign: 'center',
                    }}>
                    {label}
                  </Text>
                </View>
              </TouchableOpacity>
            );
          })}
        </View>
      </ScrollView>
    </View>
  );
};

export default MyTabBar;
