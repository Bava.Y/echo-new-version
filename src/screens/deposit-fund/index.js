import {useFocusEffect} from '@react-navigation/native';
import moment from 'moment';
import React, {useContext, useRef, useState} from 'react';
import {
  ActivityIndicator,
  Alert,
  Keyboard,
  KeyboardAvoidingView,
  Platform,
  RefreshControl,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import RBSheet from 'react-native-raw-bottom-sheet';
import Snackbar from 'react-native-snackbar';
import Icon from 'react-native-vector-icons/FontAwesome';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {profile} from '../../api/index';
import AmeritradeAlert from '../../components/AmeritradeAlert';
import Loading from '../../components/Loading';
import {UserDataContext} from '../../context/UserDataContext';
import styles from '../../styles/addFund';
import axiosInstance from '../../utils/Apicall';
import theme from '../../utils/theme';

const DepositFund = (props) => {
  // DepositFund Variables
  const [totalBalance, setTotalBalance] = useState(0);
  const [alphacaBalance, setAlphacaBalance] = useState(0);
  const [ameritradeBalance, setAmeritradeBalance] = useState(0);
  const [relationship, setRelationship] = useState(null);
  const [transferData, setTransferData] = useState(null);
  const [transferType, setTransferType] = useState(null);
  const [transferAction, setTransferAction] = useState('');
  const [amount, setAmount] = useState(null);

  // Context Variables
  const {
    alpacaConfigStatus,
    ameritradeAlertStatus,
    setACHRelationshipStatus,
    setAmeritradeAlertStatus,
    setWireRelationshipStatus,
    tdConfigStatus,
    token,
  } = useContext(UserDataContext);

  // Error Variables
  const [amountError, setAmountError] = useState(false);

  // Ref Variables
  const fundTransfer = useRef();

  // Other Variables
  const [loading, setLoading] = useState(true);
  const [screeenRefresh, setScreeenRefresh] = useState(false);

  useFocusEffect(
    React.useCallback(() => {
      let isActive = true;

      getDepositData();

      alpacaConfigStatus && getRelationShips();

      setScreeenRefresh(false);

      return () => {
        isActive = false;
      };
    }, [screeenRefresh]),
  );

  const getDepositData = async () => {
    setLoading(true);

    let response = await axiosInstance.get('get_balance', {
      headers: {
        Authorization: token,
      },
    });

    if (response && response.data && response.data.keyword === 'success') {
      setTotalBalance(response.data.data.total_balance);

      setAlphacaBalance(response.data.data.alpaca_balance);

      setAmeritradeBalance(response.data.data.td_balance);

      setAmeritradeAlertStatus(response.data.data.td_balance_error_response);
    } else {
      setTotalBalance(0.0);

      setAlphacaBalance(0.0);

      setAmeritradeBalance(0.0);

      setAmeritradeAlertStatus(false);
    }

    !alpacaConfigStatus && tdConfigStatus && setLoading(false);
  };

  const getRelationShips = async () => {
    const achResponse = await profile.getACHRelationships();
    const wireResponse = await profile.getWireRelationships();

    if (
      achResponse.keyword === 'success' &&
      wireResponse.keyword === 'success'
    ) {
      Snackbar.show({
        duration: Snackbar.LENGTH_SHORT,
        text: 'Successfully viewed the user realtionship(s)',
      });

      const mergeRelationships = wireResponse.data.concat(achResponse.data);

      checkRelationshipConfigStatus(mergeRelationships);

      setRelationship(mergeRelationships);
    } else if (
      achResponse.keyword === 'success' ||
      wireResponse.keyword === 'success'
    ) {
      Snackbar.show({
        duration: Snackbar.LENGTH_SHORT,
        text: 'Successfully viewed the user realtionship(s)',
      });

      const handleRelationship =
        achResponse.keyword === 'success'
          ? achResponse.data
          : wireResponse.data;

      checkRelationshipConfigStatus(handleRelationship);

      setRelationship(handleRelationship);
    } else if (
      achResponse.keyword === 'failed' &&
      wireResponse.keyword === 'failed'
    ) {
      Snackbar.show({
        duration: Snackbar.LENGTH_SHORT,
        text: 'No user realtionship(s) found',
      });

      checkRelationshipConfigStatus([]);

      setRelationship([]);
    } else {
      Snackbar.show({
        backgroundColor: 'red',
        duration: Snackbar.LENGTH_LONG,
        text: 'Failed fetching the user realtionship(s)',
      });

      checkRelationshipConfigStatus([]);

      setRelationship([]);
    }

    setLoading(false);
  };

  const checkRelationshipConfigStatus = (relationshipDetails) => {
    const achConfigStatus = relationshipDetails.some((relationshipData) =>
      relationshipData.hasOwnProperty('bank_account_number'),
    );

    setACHRelationshipStatus(achConfigStatus);

    const wireConfigStatus = relationshipDetails.some((relationshipData) =>
      relationshipData.hasOwnProperty('account_number'),
    );

    setWireRelationshipStatus(wireConfigStatus);
  };

  const encryptText = (string) => {
    let encryptedStr;

    string = string ? string : '';

    if (string.length > 4) {
      const str = string.slice(0, string.length - 4).replace(/[a-z0-9]/g, 'X');

      encryptedStr = str + string.slice(-4);
    } else {
      encryptedStr = string ? string : null;
    }

    return encryptedStr;
  };

  const handleStatusColor = (status) => {
    switch (status) {
      case 'APPROVED':
        return '#008000';

      case 'QUEUED':
        return '#FFA500';

      case 'PENDING':
        return '#8000FF';

      default:
        return '#6495ED';
    }
  };

  const renderDeleteConfirmation = (endpoint, relationshipId) => {
    Alert.alert(
      'Confirmation',
      'Are you sure that you want to delete this relationship?',
      [
        {
          text: 'Cancel',
          onPress: () => {},
          style: 'cancel',
        },
        {
          text: 'Confirm',
          onPress: () => {
            handleCancel(endpoint, relationshipId);
          },
        },
      ],
    );
  };

  const handleCancel = async (endpoint, relationshipId) => {
    Snackbar.show({
      duration: Snackbar.LENGTH_SHORT,
      text: 'Deleting...',
    });

    const response = await profile.cancelRelationShip(endpoint, relationshipId);

    if (response.keyword === 'success') {
      Snackbar.show({duration: Snackbar.LENGTH_SHORT, text: response.message});

      getRelationShips();
    } else {
      Snackbar.show({
        backgroundColor: 'red',
        duration: Snackbar.LENGTH_LONG,
        text: response.message,
      });
    }
  };

  const renderFundTransferSheet = () => {
    return (
      <RBSheet
        ref={fundTransfer}
        closeOnDragDown={true}
        closeOnPressMask={false}
        closeOnPressBack={true}
        height={220}
        openDuration={250}
        customStyles={{
          container: {
            backgroundColor: theme.themeColor,
            borderTopLeftRadius: 16,
            borderTopRightRadius: 16,
          },
          wrapper: {backgroundColor: 'grey', opacity: 0.9},
          draggableIcon: {
            display: 'none',
          },
        }}>
        <KeyboardAvoidingView
          behavior={Platform.OS === 'ios' ? 'padding' : null}
          keyboardVerticalOffset={Platform.OS === 'ios' ? 40 : 0}
          style={{paddingHorizontal: 16, paddingVertical: 16}}>
          <View style={styles.viewdetail}>
            <View style={{flex: 0.9, justifyContent: 'center'}}>
              <Text style={styles.title}>{transferAction} Amount</Text>
            </View>

            <View
              style={{
                flex: 0.1,
                justifyContent: 'center',
                alignItems: 'flex-end',
              }}>
              <TouchableOpacity
                onPress={() => {
                  fundTransfer.current.close();
                }}>
                <Ionicons name="close" color="white" size={18} />
              </TouchableOpacity>
            </View>
          </View>

          <View style={{marginVertical: 8}}>
            <Text style={[styles.labelTxt, {fontWeight: '400'}]}>
              How much would you like to {transferAction.toLowerCase()}?
            </Text>

            <View
              style={{
                flexDirection: 'row',
                borderBottomColor: theme.white,
                borderBottomWidth: 1,
                marginTop: 8,
                paddingBottom: 8,
              }}>
              <View style={{justifyContent: 'center', alignItems: 'baseline'}}>
                <Text style={[styles.labelTxt, {fontWeight: '600'}]}>$</Text>
              </View>

              <TextInput
                keyboardType="numeric"
                value={amount}
                onChangeText={(txt) => {
                  setAmountError(txt ? false : true);

                  setAmount(txt);
                }}
                style={styles.textInput}
              />
            </View>

            {transferAction == 'Withdraw' && (
              <Text style={styles.hintTxt}>
                Alpaca Balance - $ {parseFloat(alphacaBalance).toFixed(2)}
              </Text>
            )}

            {amountError && (
              <Text style={styles.errorMsg}>Enter transfer amount</Text>
            )}
          </View>

          <TouchableOpacity
            style={styles.saveButton}
            onPress={() => {
              handleTransfer();
            }}>
            <Text style={styles.btntxt}>{transferAction}</Text>
          </TouchableOpacity>
        </KeyboardAvoidingView>
      </RBSheet>
    );
  };

  const clearStates = () => {
    setAmountError(false);

    setTransferType(null);

    setTransferAction('');

    setAmount(null);
  };

  const handleTransfer = async () => {
    Keyboard.dismiss();

    if (amount) {
      Snackbar.show({
        duration: Snackbar.LENGTH_SHORT,
        text: 'Transferring amount...',
      });

      const requestData = {
        amount: amount,
        direction: transferAction === 'Withdraw' ? 'OUTGOING' : 'INCOMING',
        [transferType === 'ach'
          ? 'relationship_id'
          : 'bank_id']: transferData.id,
        transfer_type: transferType,
      };

      const response = await profile.transferAmount(requestData);

      if (response.keyword == 'success') {
        Snackbar.show({
          duration: Snackbar.LENGTH_SHORT,
          text: response.message,
        });

        clearStates();

        setTimeout(() => {
          fundTransfer.current.close();
        }, 1000);
      } else {
        Snackbar.show({
          backgroundColor: 'red',
          duration: Snackbar.LENGTH_LONG,
          text: response.message,
        });
      }
    } else {
      setAmountError(amount ? false : true);
    }
  };

  const handleAmeritradeAlertStatus = () => {
    setAmeritradeAlertStatus(!ameritradeAlertStatus);
  };

  return (
    <View style={styles.container}>
 
        <ScrollView
          keyboardShouldPersistTaps={'handled'}
          refreshControl={
            <RefreshControl
              refreshing={screeenRefresh}
              onRefresh={() => {
                setScreeenRefresh(true);
              }}
            />
          }
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}>
          <View style={styles.mainsec}>
            <View style={[styles.lineContainer, styles.borderView]}>
              <View style={styles.lineLabelContainer}>
                <Text style={[styles.lineLabelTxt, {color: theme.white}]}>
                  Available to Trade
                </Text>
              </View>

              <View style={styles.lineValueContainer}>
                <Text style={[styles.lineValueTxt, {color: theme.white}]}>
                  $ {totalBalance}
                </Text>
              </View>
            </View>

            <View style={[styles.borderView, {marginTop: 8}]}>
              <View style={styles.lineContainer}>
                <View style={styles.lineLabelContainer}>
                  <Text style={[styles.lineLabelTxt, {color: theme.white}]}>
                    Total Funds
                  </Text>
                </View>

                <View style={styles.lineValueContainer}>
                  <Text style={[styles.lineValueTxt, {color: theme.white}]}>
                    $ {totalBalance}
                  </Text>
                </View>
              </View>

              <View style={{marginTop: 8, paddingLeft: 8}}>
                {alpacaConfigStatus && (
                  <View style={styles.lineContainer}>
                    <View style={styles.lineLabelContainer}>
                      <Text style={[styles.lineLabelTxt, {color: theme.white}]}>
                        Alpaca
                      </Text>
                    </View>

                    <View style={styles.lineValueContainer}>
                      <Text style={[styles.lineValueTxt, {color: theme.white}]}>
                        $ {alphacaBalance}
                      </Text>
                    </View>
                  </View>
                )}

                {tdConfigStatus && (
                  <View style={[styles.lineContainer, {marginTop: 8}]}>
                    <View style={styles.lineLabelContainer}>
                      <Text style={[styles.lineLabelTxt, {fontWeight: '600'}]}>
                        TD Amiratrade
                      </Text>
                    </View>

                    <View style={styles.lineValueContainer}>
                      <Text style={[styles.lineValueTxt, {fontWeight: '600'}]}>
                        $ {parseFloat(ameritradeBalance).toFixed(2)}
                      </Text>
                    </View>
                  </View>
                )}
              </View>
            </View>

            <View
              style={[styles.lineContainer, styles.borderView, {marginTop: 8}]}>
              <View style={styles.lineLabelContainer}>
                <Text style={[styles.lineLabelTxt, {color: theme.white}]}>
                  Withdraw Funds
                </Text>
              </View>

              <View style={styles.lineValueContainer}>
                <Icon
                  name="arrow-right"
                  color="white"
                  size={16}
                  style={{alignSelf: 'flex-end'}}
                />
              </View>
            </View>

            {alpacaConfigStatus && (
              <TouchableOpacity
                onPress={() => {
                  props.navigation.navigate('TransactionHistory');
                }}>
                <View
                  style={[
                    styles.lineContainer,
                    styles.borderView,
                    {marginTop: 8},
                  ]}>
                  <View style={styles.lineLabelContainer}>
                    <Text style={[styles.lineLabelTxt, {color: theme.white}]}>
                      View Transaction History
                    </Text>
                  </View>

                  <View style={styles.lineValueContainer}>
                    <Icon
                      name="arrow-right"
                      color="white"
                      size={16}
                      style={{alignSelf: 'flex-end'}}
                    />
                  </View>
                </View>
              </TouchableOpacity>
            )}
          </View>

          {alpacaConfigStatus && relationship && relationship.length < 2 && (
            <View style={styles.submitview}>
              <TouchableOpacity
                onPress={() => {
                  props.navigation.navigate('AddRelationship');
                }}>
                <Text style={styles.btntxt}>Add Bank Account</Text>
              </TouchableOpacity>
            </View>
          )}

          {alpacaConfigStatus && (
            <>
              <Text style={styles.labelTxt}>Bank Account(s): -</Text>

              <View style={{marginVertical: 16}}>
                {relationship ? (
                  relationship.length != 0 ? (
                    relationship.map((relationshipData, index) => (
                      <View
                        key={index}
                        style={[
                          styles.mainsec,
                          {marginTop: index != 0 ? 8 : 0},
                        ]}>
                        <View
                          style={{
                            position: 'absolute',
                            backgroundColor: theme.secondryColor,
                            borderTopRightRadius: 8,
                            paddingHorizontal: 8,
                            paddingVertical: 4,
                            right: 0,
                          }}>
                          <Text style={styles.statusTxt}>
                            {relationshipData.bank_account_number
                              ? 'ACH'
                              : 'Wire'}
                          </Text>
                        </View>

                        <View style={styles.viewdetail}>
                          <View style={{flex: 0.5, justifyContent: 'center'}}>
                            <Text style={styles.viewheading}>
                              {relationshipData.account_owner_name
                                ? 'Account Name'
                                : 'Bank Name'}
                            </Text>

                            <Text style={styles.viewdata}>
                              {relationshipData.account_owner_name ||
                                relationshipData.name}
                            </Text>
                          </View>

                          <View style={{flex: 0.5, justifyContent: 'center'}}>
                            <Text style={styles.viewheading}>Account #</Text>
                            <Text style={styles.viewdata}>
                              {encryptText(
                                relationshipData.bank_account_number,
                              ) || encryptText(relationshipData.account_number)}
                            </Text>
                          </View>
                        </View>

                        <View style={[styles.viewdetail, {marginTop: 8}]}>
                          <View style={{flex: 0.275, justifyContent: 'center'}}>
                            <Text style={styles.viewheading}>
                              {relationshipData.bank_routing_number
                                ? 'Routing #'
                                : 'Bank #'}
                            </Text>

                            <Text style={styles.viewdata}>
                              {relationshipData.bank_routing_number ||
                                relationshipData.bank_code}
                            </Text>
                          </View>

                          <View style={{flex: 0.3, justifyContent: 'center'}}>
                            <Text style={styles.viewheading}>
                              {relationshipData.bank_account_type
                                ? 'Account Type'
                                : 'Bank Type'}
                            </Text>

                            <Text style={styles.viewdata}>
                              {relationshipData.bank_account_type ||
                                relationshipData.bank_code_type}
                            </Text>
                          </View>

                          <View style={{flex: 0.425, justifyContent: 'center'}}>
                            <Text style={styles.viewheading}>Date</Text>

                            <Text style={styles.viewdata}>
                              {relationshipData.created_at
                                ? moment(relationshipData.created_at).format(
                                    'LL',
                                  )
                                : '-'}
                            </Text>
                          </View>
                        </View>

                        <View style={[styles.viewdetail, {marginTop: 8}]}>
                          <View style={{flex: 0.2625, flexDirection: 'column'}}>
                            <Text style={styles.viewheading}>Status</Text>

                            <View
                              style={[
                                styles.status,
                                {
                                  borderColor: handleStatusColor(
                                    relationshipData.status || '-',
                                  ),
                                },
                              ]}>
                              <Text
                                style={[
                                  styles.statusTxt,
                                  {
                                    color: handleStatusColor(
                                      relationshipData.status || '-',
                                    ),
                                  },
                                ]}>
                                {relationshipData.status || '-'}
                              </Text>
                            </View>
                          </View>

                          <View style={styles.btnsec}>
                            <TouchableOpacity
                              onPress={() => {
                                setTransferData(relationshipData);

                                clearStates();

                                setTransferAction('Transfer');

                                setTransferType(
                                  relationshipData.bank_routing_number
                                    ? 'ach'
                                    : 'wire',
                                );

                                fundTransfer.current.open();
                              }}
                              style={styles.transferbtn}>
                              <Text style={styles.btntxt}>Transfer</Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                              onPress={() => {
                                setTransferData(relationshipData);

                                clearStates();

                                setTransferAction('Withdraw');

                                setTransferType(
                                  relationshipData.bank_routing_number
                                    ? 'ach'
                                    : 'wire',
                                );

                                fundTransfer.current.open();
                              }}
                              style={styles.cancelbtn}>
                              <Text style={styles.btntxt}>Withdraw</Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                              style={{
                                flex: 0.15,
                                paddingHorizontal: 2,
                                paddingVertical: 2,
                              }}
                              onPress={() => {
                                renderDeleteConfirmation(
                                  relationshipData.bank_routing_number
                                    ? 'alpaca_ach_relationships_destroy'
                                    : 'alpaca_recipient_banks_destroy',
                                  relationshipData.id,
                                );
                              }}>
                              <Ionicons
                                name="trash"
                                size={20}
                                color={theme.white}
                              />
                            </TouchableOpacity>
                          </View>
                        </View>
                      </View>
                    ))
                  ) : (
                    <View
                      style={{
                        flex: 1,
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      <Text style={styles.noDataTxt}>
                        No relationship(s) found
                      </Text>
                    </View>
                  )
                ) : (
                  <View
                    style={{
                      flex: 1,
                      flexDirection: 'row',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <ActivityIndicator size={24} color={theme.secondryColor} />
                    <Text
                      style={{
                        color: theme.white,
                        fontSize: 14,
                        paddingLeft: 4,
                      }}>
                      Loading relationship(s)
                    </Text>
                  </View>
                )}
              </View>
            </>
          )}

          <View style={{paddingVertical: 12}} />
        </ScrollView>
  

      {renderFundTransferSheet()}

      {/* Hided due to TD Ameritrade Echo App revoked issue */}
      {/* <AmeritradeAlert
        modalStatus={ameritradeAlertStatus}
        close={handleAmeritradeAlertStatus}
      /> */}
    </View>
  );
};

export default DepositFund;
