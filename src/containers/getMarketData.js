import {company, dashboard} from '../api';

export const getData = async (symbol, range) => {
  let res = await dashboard.getDashBoardQuote( symbol, range);

  if (res) {
    if (res.keyword == 'success') {
      console.log('res?.data?.chartdata>>>>>',res?.data?.chartdata)
      // let filtered = [];

      // res.data.chartdata
      //   .filter((x) => x.price != 0)
      //   .map((data) => {
      //     filtered.push({x: data.date, y: parseFloat(data.price)});
      //   });

      // const addPercent = (val) => {
      //   let twoPercent = (val * 2) / 100;
      //   return twoPercent;
      // };
      // let max = Math.max.apply(
      //   Math,
      //   filtered.map(function (o) {
      //     return o.y;
      //   }),
      // );
      // let min = Math.min.apply(
      //   Math,
      //   filtered.map(function (o) {
      //     return o.y;
      //   }),
      // );
      // const addedmax = max + addPercent(max);
      // const addedmin = min - addPercent(min);
      const result = {
        data: res?.data?.chartdata,
        // max: addedmax,
        // min: addedmin,
        symbol: symbol,
        price: res.data.latest_price,
        change: res.data.change,
        changePercent: res.data.change_percent,
      };

      console.log('result>>>>>>',result)

      return result;
    } else {
      return {
        data: [],
        // max: 1,
        // min: 0,
        symbol: symbol,
        price: 0,
        change: 0,
        changePercent: 0,
      };
    }
  } else {
    return {
      data: [],
      // max: 1,
      // min: 0,
      symbol: symbol,
      price: 0,
      change: 0,
      changePercent: 0,
    };
  }
};

export const getQuotes = async (symbol) => {
  let res = await company.getQuotesLite(symbol);
  let changePercent = res.change_percent;
  let p = res.latest_price * 10;
  let price = p.toFixed(2);
  let isNegative = res.change && Math.sign(res.change) != -1 ? true : false;
  const quotes = {
    price: symbol === 'SPY' ? price : res.latest_price,
    change: res.change,
    changepercent: changePercent + '%',
    isNegative: isNegative,
  };
  return quotes;
};

export const getSearchedQuotes = async (symbol) => {
  let res = await company.getQuotesLite(symbol);
  let isNegative = res.change < 0;
  const quotes = {
    symbol: res.symbol,
    name: res.company_name,
    price: res.latest_price,
    change: res.change,
    changepercent: res.change_percent + '%',
    isNegative: isNegative,
  };
  return quotes;
};
