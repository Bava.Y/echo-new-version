import * as ENV from '../../env';

const Errors = (error) => {
  switch (ENV.currentEnvironment) {
    case 'development':
      return error.toString().split(': ')[1];

    default:
      return 'Something went wrong. Please! Contact the support team.';
  }
};

export default Errors;
