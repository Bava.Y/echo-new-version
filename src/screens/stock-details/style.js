import {StyleSheet} from 'react-native';
import theme from '../../utils/theme';

const styles = StyleSheet.create({
  title: {
    fontSize: 16,
    color: theme.white,
  },
  lastpricevalue: {
    fontSize: 20,
    color: theme.white,
    fontWeight: 'bold',
    marginTop: 5,
  },
  todaychange: {
    color: theme.successGreen,
    fontSize: 20,
    fontWeight: 'bold',
    marginTop: 5,
  },
  stitle: {
    fontSize: 14,
    color: 'grey',
    textAlign: 'center',
  },
  pricevalue: {
    fontSize: 16,
    color: theme.white,
    textAlign: 'center',
  },
});

export default styles;
