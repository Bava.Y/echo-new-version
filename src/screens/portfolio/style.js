import {Dimensions, Platform, StyleSheet} from 'react-native';
import theme from '../../utils/theme';
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.themeColor,
  },
  flexLoader: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: theme.themeColor,
  },
  flexLoader1: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: theme.themeColor,
    height: 350,
  },
  chartdetail: {
    flex: 1,
    backgroundColor: theme.primaryColor,
    borderRadius: 8,
  },
  chartLabelTxt: {fontSize: 10, color: theme.white, fontWeight: '600'},
  chartValueTxt: {fontSize: 10, color: theme.white, fontWeight: '400'},
  dayview: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignSelf: 'center',
    right: 7,
  },
  day: {
    fontSize: Platform.OS == 'ios' ? 12 : 14,
    fontWeight: 'bold',
    color: 'white',
    paddingHorizontal: 8,
  },
  active: {
    fontSize: Platform.OS == 'ios' ? 12 : 14,
    fontWeight: 'bold',
    color: theme.secondryColor,
    paddingHorizontal: 8,
  },
  companyListLoader: {
    height: windowHeight * 0.1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  companylist: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: theme.themeColor,
    borderRadius: 8,
    paddingHorizontal: 10,
    paddingVertical: 10,
    marginBottom: 15,
  },
  companySymbolTxt: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    color: '#ffffffa1',
    fontWeight: '800',
  },
  companyShareAmtTxt: {
    fontSize: Platform.OS == 'ios' ? 10 : 12,
    color: '#ffffffa1',
    fontWeight: '400',
    marginTop: 4,
  },
  companySharesTxt: {
    fontSize: Platform.OS == 'ios' ? 10 : 12,
    color: '#ffffffa1',
    fontWeight: '600',
    marginTop: 4,
  },
  companyPercentageContainer: {
    backgroundColor: theme.secondryColor,
    borderRadius: 4,
    marginHorizontal: 2,
    paddingHorizontal: 4,
    paddingVertical: 4,
  },
  companyPercentageTxt: {
    fontSize: 10,
    fontWeight: '600',
    color: theme.white,
    textAlign: 'center',
  },
  upgrade: {
    width: '81.25%',
    alignItems: 'center',
    backgroundColor: '#00BD9A',
    borderRadius: 4,
    paddingHorizontal: 4,
    paddingVertical: 4,
    marginTop: 10.8,
  },
  downgrade: {
    width: '81.25%',
    alignItems: 'center',
    backgroundColor: '#FF6960',
    borderRadius: 4,
    paddingHorizontal: 4,
    paddingVertical: 4,
  },
  companyChangeTxt: {
    fontSize: 10,
    fontWeight: '600',
    color: theme.black,
    textAlign: 'center',
  },
  sell: {
    width: '81.25%',
    alignItems: 'center',
    backgroundColor: theme.danger,
    borderRadius: 4,
    paddingHorizontal: 4,
    paddingVertical: 4,
  },
  companySellTxt: {
    fontSize: 10,
    fontWeight: '600',
    color: theme.white,
    textAlign: 'center',
  },
  tabsec: {
    backgroundColor: '#2e2e2e52',
    paddingVertical: 20,
  },
  tabhead: {
    flexDirection: 'row',
    alignSelf: 'center',
    backgroundColor: theme.themeColor,
    borderRadius: 10,
    padding: 5,
  },
  headingactivebtn: {
    backgroundColor: theme.secondryColor,

    paddingVertical: 4,
    borderRadius: 6,
    marginHorizontal: 4,
  },
  headingbtn: {
    backgroundColor: 'transparent',
    borderRadius: 6,
    paddingVertical: 4,
  },
  headingtxt: {
    color: 'white',
    textAlign: 'center',
    paddingHorizontal: 25,
  },
  headingactivetxt: {
    color: 'white',
    textAlign: 'center',
    paddingHorizontal: 20,
  },
  activedot: {
    backgroundColor: theme.secondryColor,
    width: 10,
    height: 10,
    borderRadius: 50,
    marginHorizontal: 8,
  },
  inactivedot: {
    backgroundColor: theme.white,
    width: 10,
    height: 10,
    borderRadius: 50,
    marginHorizontal: 8,
  },

  listBroker: {
    flex: 0.04,
    color: 'white',
    height: Platform.OS == 'ios' ? 36 : 48,
    borderColor: 'white',
    borderWidth: 1,
    backgroundColor: theme.secondryColor,
    borderRadius: 10,
    marginVertical: 8,
    width: '28%',
  },

  listBrokerText: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    fontWeight: '500',
    color: 'white',
    textAlign: 'left',
    paddingHorizontal: Platform.OS == 'ios' ? 0 : 4,
  },

  brokerItemText: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    fontWeight: '400',
    color: 'black',
    textAlign: 'left',
  },
  listBrokerSelectedItemText: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    fontWeight: '400',
    color: theme.secondryColor,
    textAlign: 'left',
  },
  title: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    color: 'grey',
    fontWeight: 'bold',
    backgroundColor: theme.primaryColor,
    paddingHorizontal: 8,
    paddingVertical: 8,
  },
  imageView: {
    flex: 0.2875,
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageStyle: {
    width: 40,
    height: 40,
    borderRadius: 40 / 2,
  },
  applogo: {
    width: 40,
    height: 40,
    borderRadius: 40 / 2,
  },
  logosec: {
    width: 40,
    height: 41,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'black',
    borderRadius: 60 / 2,
  },
  userContainer: {
    flex: 0.7,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 5,
  },

  nameView: {
    flex: 0.7125,
    justifyContent: 'center',
    paddingHorizontal: 4,
    paddingVertical: 2,
    marginLeft: 5,
  },
  nameStyle: {
    fontSize: Platform.OS == 'ios' ? 12 : 14,
    fontWeight: '600',
    color: theme.white,
  },

  designationStyle: {
    fontSize: Platform.OS == 'ios' ? 10 : 12,
    fontWeight: '400',
    color: '#ffffff8a',
    marginTop: 2,
  },

  connectsocialstatus: {
    fontSize: Platform.OS == 'ios' ? 12 : 14,
    color: theme.secondryColor,
    fontWeight: 'bold',
    textAlign: 'center',
  },

  connect: {
    width: '30%',
    alignItems: 'center',
    backgroundColor: '#274f52',
    borderColor: theme.secondryColor,
    borderWidth: 1,
    borderRadius: 8,
    paddingHorizontal: 8,
    paddingVertical: 8,
    alignSelf: 'center',
  },
  btntext: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  btnview1: {
    backgroundColor: theme.secondryColor,
    borderRadius: 4,
    paddingHorizontal: 12,
    paddingVertical: 6,
  },
  username: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    color: 'white',
    fontWeight: '600',
  },

  echoingdetail: {
    flexDirection: 'row',

    justifyContent: 'space-between',
    backgroundColor: theme.primaryColor,
    borderRadius: 8,
    paddingHorizontal: 10,
    paddingVertical: 10,
  },
});

export default styles;
