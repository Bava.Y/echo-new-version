import AsyncStorage from '@react-native-community/async-storage';
import {useNavigation} from '@react-navigation/native';
import React, {useContext} from 'react';
import {Image, ImageBackground, TouchableOpacity, View} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {profile} from '../../api';
import {BOTTOMBG, TLOGO} from '../../assets/images/index';
import {UserDataContext} from '../../context/UserDataContext';
import theme from '../../utils/theme';
import styles from './style';

const BottomNav = ({routeName}) => {
  const navigation = useNavigation();

  const {authUserData, token} = useContext(UserDataContext);

  const chatter = async () => {
    let newdata = await profile.getUserData(authUserData.user_id, token);

    let password = newdata.password;

    let username = newdata.username;

    await AsyncStorage.setItem('pwd', password);
    await AsyncStorage.setItem('uname', username);

    navigation.push('Socialchatter');
  };

  return (
    <ImageBackground
      source={BOTTOMBG}
      style={{
        width: '100%',
        height: 48,
        justifyContent: 'center',
        backgroundColor: theme.themeColor,
      }}>
      <View style={styles.bottom}>
        <TouchableOpacity
          style={styles.menu}
          onPress={() => navigation.navigate('Dashboard')}>
          <Icon
            name="th-large"
            color={routeName == 'Dashboard' ? theme.secondryColor : theme.white}
            size={20}
          />
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.menu}
          onPress={() => navigation.push('Portfolio')}>
          <Icon
            name="dollar"
            color={routeName == 'Portfolio' ? theme.secondryColor : theme.white}
            size={20}
          />
        </TouchableOpacity>

        <View style={styles.menu}>
          <TouchableOpacity
            style={styles.logosec}
            onPress={() => navigation.navigate('DiscoverPeople')}>
            <Image
              resizeMode={'contain'}
              source={TLOGO}
              style={styles.applogo}
            />
          </TouchableOpacity>
        </View>

        <TouchableOpacity style={styles.menu}>
          <Icon
            onPress={() => {
              chatter();
            }}
            name="comment"
            color={routeName == 'Social' ? theme.secondryColor : theme.white}
            size={20}
          />
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.menu}
          onPress={() => navigation.navigate('Profile')}>
          <Icon
            name="user"
            color={routeName == 'Profile' ? theme.secondryColor : theme.white}
            size={20}
          />
        </TouchableOpacity>
      </View>
    </ImageBackground>
  );
};

export default BottomNav;
