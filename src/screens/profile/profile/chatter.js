import React, {useState} from 'react';
import {Image, NativeModules, Text, View} from 'react-native';
import {
  AccessToken,
  GraphRequest,
  GraphRequestManager,
  LoginManager,
} from 'react-native-fbsdk';
import {
  FACEBOOK,
  INSTAGRAM,
  LINKEDIN,
  TWITTER,
} from '../../../assets/images/index';
import {
  TWITTER_COMSUMER_KEY,
  TWITTER_CONSUMER_SECRET,
} from '../../../utils/constants';
import theme from '../../../utils/theme';
import styles from '../style';

const {RNTwitterSignIn} = NativeModules;

const Chatter = ({}) => {
  //status of connection
  const [fbConnect, setFbConnect] = useState('Connect');
  const [tConnect, setTConnect] = useState('Connect');
  const [lConnect, setLconnect] = useState('Connect');
  const [iConnect, setIcoonect] = useState('Connected');

  const _twitterSignIn = () => {
    RNTwitterSignIn.init(TWITTER_COMSUMER_KEY, TWITTER_CONSUMER_SECRET);
    RNTwitterSignIn.logIn()
      .then((loginData) => {
        const {authToken, authTokenSecret, email} = loginData;
        if (authToken && authTokenSecret && _.isEmpty(email)) {
        } else if (authToken && authTokenSecret && email) {
        }
      })
      .catch(() => {});
  };

  const fblogin = () => {
    LoginManager.logInWithPermissions([
      'public_profile',
      'email',
      'user_likes',
      'user_managed_groups',
    ]).then((result) => {
      if (result.error) {
      } else {
        if (result.isCancelled) {
        } else {
          AccessToken.getCurrentAccessToken().then((data) => {
            let accessToken = data.accessToken;
            setAccToken(data.accessToken);

            const responseInfoCallback = (error, result) => {
              if (error) {
                alert('FB Could not fetch data');
              } else {
                setFbConnect('Connected');
              }
            };

            const infoRequest = new GraphRequest(
              '/me/likes',
              {
                accessToken: accessToken,
                parameters: {
                  fields: {
                    string: 'email,name,first_name,middle_name,last_name,likes',
                  },
                },
              },
              responseInfoCallback,
            );

            // Start the graph request.
            new GraphRequestManager().addRequest(infoRequest).start();
          });
        }
      }
    });
  };
  const [accToken, setAccToken] = useState(undefined);

  return (
    <View style={{flex: 1, backgroundColor: theme.themeColor}}>
      <View style={styles.socialmedia}>
        <View style={styles.activesocialmedialist}>
          <View style={{flexDirection: 'row'}}>
            <View style={{width: '70%', flexDirection: 'row'}}>
              <View style={styles.bgsocialicon}>
                <Image source={INSTAGRAM} style={styles.socialicon}></Image>
              </View>
              <Text style={styles.socialmediatitle}>instagram</Text>
            </View>
            <View style={styles.connect}>
              <Text style={styles.connectsocialstatus}>{iConnect}</Text>
            </View>
          </View>

          <View style={styles.followersection}>
            <View style={[styles.followers, styles.borderRight]}>
              <Text style={styles.followname}>Followers</Text>
              <Text style={styles.count}>108k</Text>
            </View>
            <View style={[styles.followers, styles.borderRight]}>
              <Text style={styles.followname}>Following</Text>
              <Text style={styles.count}>50</Text>
            </View>
            <View style={styles.followers}>
              <Text style={styles.followname}>Echoers</Text>
              <Text style={styles.count}>95k</Text>
            </View>
          </View>
        </View>
        <View style={styles.socialmedialist}>
          <View style={{width: '70%', flexDirection: 'row'}}>
            <View style={styles.bgsocialicon}>
              <Image source={FACEBOOK} style={styles.socialicon}></Image>
            </View>
            <Text style={styles.socialmediatitle}>Facebook</Text>
          </View>
          <View
            style={fbConnect == 'Connect' ? styles.notconnect : styles.connect}
            onStartShouldSetResponder={fblogin}>
            <Text
              style={
                fbConnect == 'Connect'
                  ? styles.socialstatus
                  : styles.connectsocialstatus
              }>
              {fbConnect}
            </Text>
          </View>
        </View>
        <View style={styles.socialmedialist}>
          <View style={{width: '70%', flexDirection: 'row'}}>
            <View style={styles.bgsocialicon}>
              <Image source={TWITTER} style={styles.socialicon}></Image>
            </View>
            <Text style={styles.socialmediatitle}>Twitter</Text>
          </View>
          <View
            style={styles.notconnect}
            onStartShouldSetResponder={_twitterSignIn}>
            <Text style={styles.socialstatus}>{tConnect}</Text>
          </View>
        </View>
        <View style={styles.socialmedialist}>
          <View style={{width: '70%', flexDirection: 'row'}}>
            <View style={styles.bgsocialicon}>
              <Image source={LINKEDIN} style={styles.socialicon}></Image>
            </View>
            <Text style={styles.socialmediatitle}>LinkedIn</Text>
          </View>
          <View style={styles.notconnect}>
            <Text style={styles.socialstatus}>{lConnect}</Text>
          </View>
        </View>
      </View>
    </View>
  );
};

export default Chatter;
