import {useFocusEffect, useNavigation} from '@react-navigation/native';

import React, {
  useContext,
  useState,
  useRef,
  useCallback,
  useEffect,
} from 'react';
import {
  Platform,
  RefreshControl,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
  Image,
  KeyboardAvoidingView,
  Button,
  ActivityIndicator,
} from 'react-native';
import Modal from 'react-native-modal';
import {plaid} from '../../api';
import BottomNav from '../../components/BottomNav';
import HeaderNav from '../../components/HeaderNav';
import Loading from '../../components/Loading';
import {UserDataContext} from '../../context/UserDataContext';
import axiosInstance from '../../utils/Apicall';
import theme from '../../utils/theme';
import Metrics from './metrics';
import RenderEcho from './partials/RenderEcho';
import RBSheet from 'react-native-raw-bottom-sheet';
import styles from './style';
import Icon from 'react-native-vector-icons/FontAwesome';
import DocumentPicker, {types} from 'react-native-document-picker';
import {auth, portfolio} from '../../api';
import Snackbar from 'react-native-snackbar';
import AsyncStorage from '@react-native-community/async-storage';
import {TOKEN} from '../../utils/constants';
const DiscoverPeople = () => {
  const [isMetrics, setMetrics] = useState(false);
  const [loading, setLoading] = useState(true);
  const [echoing, setEchoing] = useState([]);
  const [echower, setEchower] = useState([]);

  const [refreshing, setRefreshing] = useState(false);
  const [selectOne, setSelectOne] = useState(false);
  const [trade, setTrade] = useState(null);
  const [PlaidConnect, setPlaidConnect] = useState(null);
  const [noanyOne, setnoanyOne] = useState(null);
  const [fileResponse, setFileResponse] = useState([]);
  const [FileUri, setFileUri] = useState(null);
  const [FileName, setFileName] = useState(null);
  const [FileType, setFileType] = useState(null);
  const [FileData, setFileData] = useState([]);
  const [selectFileName, setSelectFileName] = useState(null);
  const [fileSelectName, setFileSelectName] = useState(null);
  const [closeDocument, setCloseDocument] = useState(false);
  const [imageLoader, setImageLoader] = useState(true);
  const [showText, setShowText] = useState(true);
  const [isTrackingError, setTrackingError] = useState(false);
  const [trackingErrorData, setTrackingErrorData] = useState({});
  const [customerid, setCustomerid] = useState(null);
  const {token, discoverToken} = useContext(UserDataContext);
  const sentGmail = useRef();
  const navigation = useNavigation();
  // const fetchEchoLists = async (params = null) => {
  //   setLoading(true);
  //   let token = await AsyncStorage.getItem(TOKEN);
  //   let path = 'echoingList';

  //   if (!!params) {
  //     path = path + '?' + params;
  //   }

  //   let response = await axiosInstance.get(path, {
  //     headers: {
  //       Authorization: token,
  //     },
  //   });

  //   if (response && response.data && response.data.keyword === 'success') {
  //     setEchower(response.data.echower_data);
  //     setLoading(false);
  //   } else {
  //     setLoading(false);
  //   }
  // };

  const echoingPeopleList = async () => {
    setLoading(true);
    let token = await AsyncStorage.getItem(TOKEN);

    let path = 'echoing_data_list';

    let response = await axiosInstance.get(path, {
      headers: {
        Authorization: token,
      },
    });

    if (response) {
      if (response && response.data && response.data.keyword == 'success') {
        setEchoing(response?.data?.data);
        setLoading(false);
      } else {
        await setEchoing([]);
        setLoading(false);
      }
    }
  };

  const echowerPeopleList = async () => {
    setLoading(true);
    let token = await AsyncStorage.getItem(TOKEN);

    let path = 'echower_data_list';

    let response = await axiosInstance.get(path, {
      headers: {
        Authorization: token,
      },
    });

    if (response) {
      if (response && response.data && response.data.keyword == 'success') {
        setEchower(response?.data?.data);
        setLoading(false);
      } else {
        await setEchower([]);
        setLoading(false);
      }
    }
  };

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);

    setTimeout(() => {
      // fetchEchoLists();

      setRefreshing(false);
    }, 1500);
  }, []);

  useFocusEffect(
    React.useCallback(() => {
      let isActive = true;
      const interval = setInterval(() => {
        setShowText(showText => !showText);
      }, 1000);
      // fetchEchoLists();
      echoingPeopleList();
      echowerPeopleList();
      return () => {
        isActive = false;
        clearInterval(interval);
      };
    }, []),
  );

  const getTrackingError = async () => {
    setTrackingError(true);

    setLoading(true);

    const res = await portfolio.getTrackingError(customerid, token);

    res && res.keyword == 'success'
      ? setTrackingErrorData(res.data)
      : Snackbar.show({duration: Snackbar.LENGTH_SHORT, text: res.message});

    setLoading(false);
  };
  return (
    <>
      <HeaderNav
        title="Echoes | Following | Followers"
        isBack="true"
        routeName="Echoes | Following | Followers"
         onFilterApply={(query) => fetchEchoLists(query)}
      />

      <View style={styles.container}>
        <>
          <ScrollView
            keyboardShouldPersistTaps={'handled'}
            refreshControl={
              <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
            }
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}>
            {/* Echoing */}
            <Text style={styles.titles}>Echoing</Text>

            <View style={{paddingHorizontal: 8, paddingVertical: 8}}>
              {!loading && echoing ? (
                echoing.length !== 0 ? (
                  echoing.map((item, index) => (
                    <View
                      key={index?.toString()}
                      style={{
                        backgroundColor: theme.primaryColor,
                        borderTopEndRadius: 10,
                        borderTopStartRadius: 10,
                        //padding: 3,
                        // borderBottomWidth: 1,
                        // borderBottomColor: '#666262',
                        marginBottom: 10,
                        borderBottomEndRadius: 10,
                        borderBottomStartRadius: 10,
                      }}>
                      <View style={styles.listContainer}>
                        <View style={styles.userContainer}>
                          <TouchableOpacity style={styles.imageView}>
                            <Image
                              onLoadStart={() => {
                                setImageLoader(true);
                              }}
                              onLoadEnd={() => {
                                setImageLoader(false);
                              }}
                              resizeMode={'cover'}
                              style={styles.imageStyle}
                              source={{uri: item?.photo}}
                            />
                            {imageLoader && (
                              <ActivityIndicator
                                size={12}
                                style={{position: 'absolute'}}
                              />
                            )}
                          </TouchableOpacity>

                          <TouchableOpacity
                            style={styles.nameView}
                            onPress={() =>
                              navigation.navigate('DiscoverPeopleProfile', {
                                id: item?.followed_id,
                                loginStatus: item?.login_status,
                              })
                            }>
                            <Text style={styles.nameStyle}>
                              {item?.first_name + ' ' + item?.last_name}
                            </Text>

                            {item?.designation &&
                              (item?.designation === 'null' ? (
                                <Text style={styles.designationStyle}></Text>
                              ) : (
                                <Text style={styles.designationStyle}>
                                  {item?.designation}
                                </Text>
                              ))}
                          </TouchableOpacity>
                        </View>

                        <View style={styles.errorContainer}>
                          <TouchableOpacity
                            style={styles.blueDotView}
                            onPress={() =>
                              navigation.navigate('TrackingErrorHistory', {
                                echoerData: item,
                              })
                            }>
                            <Text style={styles.text12Style}>H</Text>
                          </TouchableOpacity>

                          <TouchableOpacity
                            style={styles.blueDotView}
                            onPress={() => {
                              navigation.navigate('Rebalance', {id: 'all'});
                            }}>
                            <Text style={styles.text12Style}>R</Text>
                          </TouchableOpacity>

                          <TouchableOpacity
                            style={[
                              styles.dotView,
                              {
                                backgroundColor: '#FF00005A',
                              },
                            ]}
                            onPress={() => {
                              setCustomerid(item?.followed_id),
                                getTrackingError();
                            }}>
                            <Text style={styles.text12Style}>T</Text>
                          </TouchableOpacity>
                        </View>

                        <View style={styles.smallBlueDotContainer}>
                          <TouchableOpacity
                            style={styles.smallBlueDotView}
                            onPress={() => setMetrics(true)}
                          />
                        </View>
                      </View>

                      {item?.portfolio_return?.map((lol, index) => {
                        return (
                          <View
                            key={index?.toString()}
                            style={styles.listReturn}>
                            <View
                              style={{
                                flexDirection: 'row',
                                marginTop: 6,
                                marginBottom: 3,
                              }}>
                              <Text
                                style={{
                                  flex: 0.9,
                                  marginHorizontal: '5%',
                                  color: 'white',
                                }}>
                                {lol?.broker_connection}
                              </Text>
                              <Text style={{color: 'white', fontSize: 13}}>
                                {Boolean(lol?.returns)
                                  ? parseFloat(lol?.returns).toFixed(2)
                                  : '0.00'}
                                %
                              </Text>
                            </View>
                          </View>
                        );
                      })}
                    </View>
                  ))
                ) : (
                  <View style={{marginHorizontal: 16, marginVertical: 16}}>
                    <Text
                      style={{
                        fontSize: Platform.OS == 'ios' ? 12 : 14,
                        color: theme.white,
                        textAlign: 'center',
                      }}>
                      No echoing(s) found!
                    </Text>
                  </View>
                )
              ) : (
                <View style={styles.companyListLoader}>
                  <ActivityIndicator size="large" color={theme.secondryColor} />
                </View>
              )}
            </View>

            {/* Echoers */}
            <Text style={styles.titles}>People Echoing You</Text>

            <View style={{paddingHorizontal: 8, paddingVertical: 8}}>
              {!loading && echower ? (
                echower.length !== 0 ? (
                  echower.map((item, index) => (
                    <View
                      key={index?.toString()}
                      style={{
                        backgroundColor: theme.primaryColor,
                        borderTopEndRadius: 10,
                        borderTopStartRadius: 10,
                        //padding: 3,
                        // borderBottomWidth: 1,
                        // borderBottomColor: '#666262',
                        marginBottom: 10,
                        borderBottomEndRadius: 10,
                        borderBottomStartRadius: 10,
                      }}>
                      <View style={styles.listContainer}>
                        <View style={styles.userContainer}>
                          <TouchableOpacity style={styles.imageView}>
                            <Image
                              onLoadStart={() => {
                                setImageLoader(true);
                              }}
                              onLoadEnd={() => {
                                setImageLoader(false);
                              }}
                              resizeMode={'cover'}
                              style={styles.imageStyle}
                              source={{uri: item?.photo}}
                            />
                            {imageLoader && (
                              <ActivityIndicator
                                size={12}
                                style={{position: 'absolute'}}
                              />
                            )}
                          </TouchableOpacity>

                          <TouchableOpacity
                            style={styles.nameView}
                            onPress={() =>
                              navigation.navigate('DiscoverPeopleProfile', {
                                id: item?.followed_id,
                                loginStatus: item?.login_status,
                              })
                            }>
                            <Text style={styles.nameStyle}>
                              {item?.first_name + ' ' + item?.last_name}
                            </Text>

                            {item?.designation &&
                              (item?.designation === 'null' ? (
                                <Text style={styles.designationStyle}></Text>
                              ) : (
                                <Text style={styles.designationStyle}>
                                  {item?.designation}
                                </Text>
                              ))}
                          </TouchableOpacity>
                        </View>

                        <View style={styles.errorContainer}>
                          <TouchableOpacity
                            style={styles.blueDotView}
                            onPress={() =>
                              navigation.navigate('TrackingErrorHistory', {
                                echoerData: item,
                              })
                            }>
                            <Text style={styles.text12Style}>H</Text>
                          </TouchableOpacity>

                          <TouchableOpacity
                            style={styles.blueDotView}
                            onPress={() => {
                              navigation.navigate('Rebalance', {id: 'all'});
                            }}>
                            <Text style={styles.text12Style}>R</Text>
                          </TouchableOpacity>

                          <TouchableOpacity
                            style={[
                              styles.dotView,
                              {
                                backgroundColor: '#FF00005A',
                              },
                            ]}
                            onPress={() => {
                              setCustomerid(item?.followed_id),
                                getTrackingError();
                            }}>
                            <Text style={styles.text12Style}>T</Text>
                          </TouchableOpacity>
                        </View>

                        <View style={styles.smallBlueDotContainer}>
                          <TouchableOpacity
                            style={styles.smallBlueDotView}
                            onPress={() => setMetrics(true)}
                          />
                        </View>
                      </View>

                      <View key={index?.toString()} style={styles.listReturn}>
                        <View
                          style={{
                            flexDirection: 'row',
                            marginTop: 6,
                            marginBottom: 3,
                          }}>
                          <Text
                            style={{
                              flex: 0.9,
                              marginHorizontal: '5%',
                              color: 'white',
                            }}>
                            My Stocks Returns
                          </Text>
                          <Text style={{color: 'white', fontSize: 13}}>
                            {Boolean(item?.mystocks_return)
                              ? parseFloat(item?.mystocks_return).toFixed(2)
                              : '0.00'}
                            %
                          </Text>
                        </View>
                      </View>
                    </View>
                  ))
                ) : (
                  <View style={{marginHorizontal: 16, marginVertical: 16}}>
                    <Text
                      style={{
                        fontSize: Platform.OS == 'ios' ? 12 : 14,
                        color: theme.white,
                        textAlign: 'center',
                      }}>
                      No echoer(s) found!
                    </Text>
                  </View>
                )
              ) : (
                <View style={styles.companyListLoader}>
                  <ActivityIndicator size="large" color={theme.secondryColor} />
                </View>
              )}
            </View>
          </ScrollView>

          <TouchableOpacity
            style={{bottom: 36}}
            onPress={() => navigation.navigate('DiscoverPeopleList')}>
            <View style={styles.btnview}>
              <Text style={styles.btntext}>Discover People</Text>
            </View>
          </TouchableOpacity>
        </>
      </View>

      {/* Metrics */}
      <Modal
        isVisible={isMetrics}
        coverScreen={false}
        onBackdropPress={() => setMetrics(false)}>
        <Metrics onSelect={setMetrics} />
      </Modal>

      <BottomNav routeName="Discover People" />
    </>
  );
};

export default DiscoverPeople;
