import React from 'react';
import {ScrollView, Text, View} from 'react-native';
import styles from './styles';

const Channels = () => {
  return (
    <>
      <View style={styles.walletContainer}>
        <View style={styles.walletLabelContainer}>
          <Text style={styles.walletLabelTxt}>Echo Wallet Balance</Text>
        </View>

        <View style={styles.walletValueContainer}>
          <Text style={styles.walletValueTxt}>$ 5035.00</Text>
        </View>
      </View>

      <ScrollView
        keyboardShouldPersistTaps={'handled'}
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
        style={[styles.container, {marginVertical: 8}]}>
        <View style={styles.mainsec}>
          <Text style={styles.headingTxt}>Your Paid Channels</Text>

          <View style={{marginTop: 16}}>
            <View style={styles.lineContainer}>
              <View
                style={[
                  styles.lineLabelContainer,
                  {flexDirection: 'row', justifyContent: 'space-between'},
                ]}>
                <View style={{flex: 0.5, justifyContent: 'center'}}>
                  <Text style={styles.lineLabelTxt}>Apple</Text>
                </View>

                <View style={{flex: 0.5, justifyContent: 'center'}}>
                  <Text style={styles.lineLabelTxt}>75 Members</Text>
                </View>
              </View>

              <View style={styles.lineValueContainer}>
                <Text style={styles.lineValueTxt}>$ 5035.00</Text>
              </View>
            </View>

            <View style={[styles.lineContainer, {marginTop: 8}]}>
              <View
                style={[
                  styles.lineLabelContainer,
                  {flexDirection: 'row', justifyContent: 'space-between'},
                ]}>
                <View style={{flex: 0.5, justifyContent: 'center'}}>
                  <Text style={styles.lineLabelTxt}>4/12/2020</Text>
                </View>

                <View style={{flex: 0.5, justifyContent: 'center'}}>
                  <Text style={styles.lineLabelTxt}>25 Members</Text>
                </View>
              </View>

              <View style={styles.lineValueContainer}>
                <Text style={styles.lineValueTxt}>$ 10.00</Text>
              </View>
            </View>

            <View style={[styles.lineContainer, {marginTop: 8}]}>
              <View
                style={[
                  styles.lineLabelContainer,
                  {flexDirection: 'row', justifyContent: 'space-between'},
                ]}>
                <View style={{flex: 0.5, justifyContent: 'center'}}>
                  <Text style={styles.lineLabelTxt}>4/12/2020</Text>
                </View>

                <View style={{flex: 0.5, justifyContent: 'center'}}>
                  <Text style={styles.lineLabelTxt}>25 Members</Text>
                </View>
              </View>

              <View style={styles.lineValueContainer}>
                <Text style={styles.lineValueTxt}>$ 8.00</Text>
              </View>
            </View>

            <View style={[styles.lineContainer, {marginTop: 8}]}>
              <View
                style={[
                  styles.lineLabelContainer,
                  {flexDirection: 'row', justifyContent: 'space-between'},
                ]}>
                <View style={{flex: 0.5, justifyContent: 'center'}}>
                  <Text style={styles.lineLabelTxt}>4/12/2020</Text>
                </View>

                <View style={{flex: 0.5, justifyContent: 'center'}}>
                  <Text style={styles.lineLabelTxt}>25 Members</Text>
                </View>
              </View>

              <View style={styles.lineValueContainer}>
                <Text style={styles.lineValueTxt}>$ 5.00</Text>
              </View>
            </View>
          </View>
        </View>

        <View style={[styles.mainsec, {marginTop: 16}]}>
          <Text style={styles.headingTxt}>Your Memberships to Channels</Text>

          <View style={{marginTop: 16}}>
            <View style={styles.lineContainer}>
              <View style={styles.lineLabelContainer}>
                <Text style={styles.lineLabelTxt}>Membership Fees</Text>
              </View>

              <View style={styles.lineValueContainer}>
                <Text style={styles.lineValueRedTxt}>$ 575.00</Text>
              </View>
            </View>

            <View style={[styles.lineContainer, {marginTop: 8}]}>
              <View
                style={[
                  styles.lineLabelContainer,
                  {flexDirection: 'row', justifyContent: 'space-between'},
                ]}>
                <View style={{flex: 0.5, justifyContent: 'center'}}>
                  <Text style={styles.lineLabelTxt}>Disney</Text>
                </View>

                <View style={{flex: 0.5, justifyContent: 'center'}}>
                  <Text style={styles.lineLabelTxt}>90 Members</Text>
                </View>
              </View>

              <View style={styles.lineValueContainer}>
                <Text style={styles.lineValueRedTxt}>$ 5035.00</Text>
              </View>
            </View>

            <View style={[styles.lineContainer, {marginTop: 8}]}>
              <View
                style={[
                  styles.lineLabelContainer,
                  {flexDirection: 'row', justifyContent: 'space-between'},
                ]}>
                <View style={{flex: 0.5, justifyContent: 'center'}}>
                  <Text style={styles.lineLabelTxt}>General Electric</Text>
                </View>

                <View style={{flex: 0.5, justifyContent: 'center'}}>
                  <Text style={styles.lineLabelTxt}>50 Members</Text>
                </View>
              </View>

              <View style={styles.lineValueContainer}>
                <Text style={styles.lineValueRedTxt}>$ 5.00</Text>
              </View>
            </View>
          </View>
        </View>

        <View style={{paddingVertical: 16}} />
      </ScrollView>
    </>
  );
};

export default Channels;
