import React from 'react';
import {Platform, Switch} from 'react-native';

const SwitchScreen = ({isEnabled, toggleSwitch}) => {
  return (
    <Switch
      style={
        Platform.OS == 'ios' && {transform: [{scaleX: 0.625}, {scaleY: 0.625}]}
      }
      trackColor={{false: 'red', true: 'green'}}
      thumbColor={isEnabled ? '#f4f3f4' : '#f4f3f4'}
      ios_backgroundColor="#3e3e3e"
      onValueChange={toggleSwitch}
      value={isEnabled}
    />
  );
};

export default SwitchScreen;
