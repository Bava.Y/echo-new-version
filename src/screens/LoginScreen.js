import AsyncStorage from '@react-native-community/async-storage';
// google
import {GoogleSignin, statusCodes} from '@react-native-google-signin/google-signin';
import {StackActions} from '@react-navigation/native';
import _ from 'lodash';
import React, {useState} from 'react';
import {
  Image,
  Keyboard,
  KeyboardAvoidingView,
  NativeModules,
  Platform,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import DeviceInfo from 'react-native-device-info';
import {
  AccessToken,
  GraphRequest,
  GraphRequestManager,
  LoginManager,
} from 'react-native-fbsdk';
import Modal from 'react-native-modal';
import Snackbar from 'react-native-snackbar';
import * as ENV from '../../env';
import {auth} from '../api';
import {APP_LOGO, FACEBOOK, GOOGLE, TWITTER} from '../assets/images';
import {FloatingTitleTextInputField} from '../components/FloatingTitleTextInputField';
import Loading from '../components/Loading';
import style from '../styles/loginstyle';
import {
  GOOGLE_WEB_CLIENT_ID,
  LOGIN_USER,
  TOKEN,
  TOKENN,
  TWITTER_COMSUMER_KEY,
  TWITTER_CONSUMER_SECRET,
} from '../utils/constants';
import theme from '../utils/theme';

const LoginScreen = ({navigation, route}) => {
  // LoginScreen Variables
  const [username, setUserName] = useState('');
  const [password, setPassword] = useState('');
  const [id, setId] = useState(undefined);
  const [isAlreadyExists, setAlreadyExists] = useState(false);

  // Error Variables
  const initialValidationError = {
    userName: false,
    passWord: false,
  };
  const [validationError, setValidationError] = useState({
    ...initialValidationError,
  });

  // Other Variables
  const [loading, setLoading] = useState(false);
  let deviceId = DeviceInfo.getUniqueId();
  const {RNTwitterSignIn} = NativeModules;

  GoogleSignin.configure({
    scopes: ['https://www.googleapis.com/auth/drive.readonly'],
    webClientId: GOOGLE_WEB_CLIENT_ID,
  });

  const showSnackBar = (message) =>
    Snackbar.show({
      text: message,
      duration: Snackbar.LENGTH_SHORT,
    });

  const _twitterSignIn = () => {
    RNTwitterSignIn.init(TWITTER_COMSUMER_KEY, TWITTER_CONSUMER_SECRET);

    RNTwitterSignIn.logIn()
      .then((loginData) => {
        const {authToken, authTokenSecret, email} = loginData;

        if (authToken && authTokenSecret && _.isEmpty(email)) {
          navigation.navigate('Registration', {
            eMail: loginData.email,
            fiRstName: loginData.name,
          });
        } else if (authToken && authTokenSecret && email) {
          if (!_.isEmpty(loginData)) {
            oauthSignIn(loginData, 'twitter');
          }
        }
      })
      .catch(() => {});
  };

  const twlogout = () => {
    RNTwitterSignIn.init(TWITTER_COMSUMER_KEY, TWITTER_CONSUMER_SECRET);

    RNTwitterSignIn.logOut();
  };

  const oauthSignIn = async (authData, loginType) => {
    if (!_.isUndefined(authData)) {
      setLoading(true);

      await AsyncStorage.removeItem(TOKENN);

      await AsyncStorage.removeItem(LOGIN_USER);

      let responce = await auth.verifyUser(authData.email);

      if (responce == 'E-mail not registered with us') {
        if (loginType === 'google') {
          navigation.navigate('Registration', {
            eMail: authData.email,
            fiRstName: authData.name,
          });
        } else if (loginType === 'fb') {
          navigation.navigate('Registration', {
            eMail: authData.email,
            fiRstName: authData.first_name,
            laStName: authData.last_name,
          });
        } else if (loginType === 'twitter') {
          navigation.navigate('Registration', {
            eMail: authData.email,
            fiRstName: authData.name,
          });
        }
      }
      if (typeof responce === 'string') {
        setLoading(false);

        showSnackBar(responce);

        if (loginType === 'fb') {
          fblogout();
        }
        if (loginType === 'google') {
          _gsignOut();
        }
        if (loginType === 'twitter') {
          twlogout();
        }

        return;
      }

      showSnackBar('Login successful');

      setTimeout(async () => {
        setLoading(false);

        AsyncStorage.setItem('LOGIN_TYPE', loginType);

        navigation.dispatch(
          StackActions.replace('SocialMpin', {
            user: authData.email,
            data: {...responce},
          }),
        );
      }, 1500);
    } else {
      showSnackBar('Could not fetch data');
    }
  };

  const deviceChangeLogin = async () => {
    let formData = changingFields();

    if (!_.isUndefined(formData)) {
      setAlreadyExists(false);

      setLoading(true);

      await AsyncStorage.removeItem(TOKENN);

      await AsyncStorage.removeItem(LOGIN_USER);

      let responce = await auth.deviceChangeLogin(formData);

      if (typeof responce === 'string') {
        setLoading(false);

        showSnackBar(responce);

        return;
      }
      showSnackBar('Login Successful');

      setTimeout(() => {
        setLoading(false);
        let userData = {...responce, username, password};

        navigation.dispatch(
          StackActions.replace('OTPScreen', {
            data: userData,
          }),
        );
      }, 1500);
    } else {
      showSnackBar('Enter valid username or password');
    }
  };

  const signIn = async () => {
    Keyboard.dismiss();

    let formData = validateFields();

    if (!_.isUndefined(formData)) {
      setLoading(true);

      await AsyncStorage.removeItem(TOKENN);

      await AsyncStorage.removeItem(LOGIN_USER);

      let responce = await auth.login(formData);

      if (typeof responce === 'string') {
        let myResponse = JSON.parse(responce);

        setLoading(false);

        if (myResponse.message == 'This user already exist in another device') {
          setAlreadyExists(true);

          setId(Number(myResponse.data));
        } else {
          showSnackBar(myResponse.message);
        }
        return;
      }

      showSnackBar('Login Successful');

      setTimeout(() => {
        setLoading(false);
        let userData = {...responce, username, password};

        navigation.dispatch(
          StackActions.replace('OTPScreen', {
            data: userData,
          }),
        );
      }, 1500);
    } else {
      showSnackBar('Enter valid username or password');
    }
  };

  const validateFields = () => {
    let formData = new FormData();

    let error = validationError;

    if (username === '') {
      error['userName'] = 'Enter user name';

      formData = undefined;
    } else {
      error['userName'] = false;

      formData && formData.append('username', username);
    }

    if (password === '') {
      error['passWord'] = 'Enter password';

      formData = undefined;
    } else {
      error['passWord'] = false;

      formData && formData.append('password', password);
    }

    formData && formData.append('unique_id', deviceId);

    setValidationError({...validationError, ...error});

    return formData;
  };

  const changingFields = () => {
    let formData = new FormData();

    formData && formData.append('id', id);
    formData && formData.append('status', 1);
    formData && formData.append('unique_id', deviceId);
    formData && formData.append('username', username);
    formData && formData.append('password', password);

    return formData;
  };

  const fblogin = () => {
    LoginManager.logInWithPermissions([
      'public_profile',
      'email',
      'user_managed_groups',
    ]).then((result) => {
      if (result.error) {
      } else {
        if (result.isCancelled) {
        } else {
          AccessToken.getCurrentAccessToken().then((data) => {
            let accessToken = data.accessToken;

            const responseInfoCallback = (error, result) => {
              if (error) {
                alert('FB Could not fetch data');
              } else {
                oauthSignIn(result, 'fb');
              }
            };

            const infoRequest = new GraphRequest(
              '/me',
              {
                accessToken: accessToken,
                parameters: {
                  fields: {
                    string: 'email,name,first_name,middle_name,last_name',
                  },
                },
              },
              responseInfoCallback,
            );

            new GraphRequestManager().addRequest(infoRequest).start();
          });
        }
      }
    });
  };

  const fblogout = () => {
    LoginManager.logOut();
  };

  const _gsignIn = async () => {
    try {
      await GoogleSignin.hasPlayServices({
        showPlayServicesUpdateDialog: true,
      });

      const userInfo = await GoogleSignin.signIn();

      if (!_.isEmpty(userInfo.user)) {
        oauthSignIn(userInfo.user, 'google');
      }
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
      } else if (error.code === statusCodes.IN_PROGRESS) {
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
      } else {
      }
    }
  };

  const _gsignOut = async () => {
    try {
      await GoogleSignin.revokeAccess();
      await GoogleSignin.signOut();
    } catch (error) {}
  };

  return (
    <>
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : null}
        keyboardVerticalOffset={Platform.OS === 'ios' ? 40 : 0}
        style={style.container}>
        <Image source={APP_LOGO} style={style.appLogo} />

        <Text
          style={{
            fontSize: Platform.OS == 'ios' ? 12 : 14,
            color: theme.secondryColor,
            textTransform: 'capitalize',
            marginTop: 8,
          }}>
          ({ENV.currentEnvironment})
        </Text>

        <FloatingTitleTextInputField
          ispassword={false}
          attrName="username"
          title="User Name"
          value={username}
          updateMasterState={(text) => {
            setValidationError({
              ...validationError,
              userName: _.isEmpty(text) ? 'Enter user name' : false,
            });
            setUserName(text);
          }}
        />
        {validationError.userName ? (
          <Text style={style.errormsg}>{validationError.userName}</Text>
        ) : null}

        <FloatingTitleTextInputField
          ispassword={true}
          attrName="password"
          title="Password"
          value={password}
          updateMasterState={(text) => {
            setValidationError({
              ...validationError,
              passWord: _.isEmpty(text) ? 'Enter password' : false,
            });
            setPassword(text);
          }}
        />
        {validationError.passWord ? (
          <Text style={style.errormsg}>{validationError.passWord}</Text>
        ) : null}

        <TouchableOpacity
          onPress={() => {
            setValidationError({
              ...validationError,
              userName: false,
              passWord: false,
            });
            navigation.navigate('Forgotpassword');
          }}>
          <Text style={style.forgot}>Forgot Password?</Text>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => {
            setValidationError({
              ...validationError,
              userName: false,
              passWord: false,
            });

            navigation.navigate('Registration', {
              eMail: '',
              fiRstName: '',
              laStName: '',
            });
          }}>
          <Text style={style.forgot}>Sign Up</Text>
        </TouchableOpacity>
        <TouchableOpacity style={style.signinBtn} onPress={signIn}>
          <Text style={style.signintext}>Sign In</Text>
        </TouchableOpacity>

        {Platform.OS != 'ios' && (
          <View style={{marginVertical: 20}}>
            <TouchableOpacity
              style={{flexDirection: 'row', marginTop: 10}}
              onPress={fblogin}>
              <Image source={FACEBOOK} style={style.socialicon} />
              <Text style={style.altersignintext}>Login With Facebook</Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={_twitterSignIn}
              style={{flexDirection: 'row', marginTop: 10}}>
              <Image source={TWITTER} style={style.socialicon} />
              <Text style={style.altersignintext}>Login With Twitter</Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={_gsignIn}
              style={{flexDirection: 'row', marginTop: 10}}>
              <Image source={GOOGLE} style={style.socialicon} />
              <Text style={style.altersignintext}>Login With Google</Text>
            </TouchableOpacity>
          </View>
        )}
      </KeyboardAvoidingView>

      <Modal
        animationType="fade"
        transparent={true}
        isVisible={isAlreadyExists}
        onBackdropPress={() => setAlreadyExists(false)}>
        <View style={style.welcomecontainer}>
          <Text style={style.welcometxt}>
            This user already exists in another device
          </Text>

          <Text style={style.steptext}>Do you want to continue?</Text>

          <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <TouchableOpacity
              style={style.startbtn}
              onPress={deviceChangeLogin}>
              <Text style={style.starttxt}>Yes</Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={style.startbtn}
              onPress={() => setAlreadyExists(false)}>
              <Text style={style.starttxt}>No</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>

      {loading && <Loading color={theme.white} />}
    </>
  );
};

export default LoginScreen;
