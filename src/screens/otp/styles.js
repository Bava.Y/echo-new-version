import {Platform} from 'react-native';
import {Dimensions, StyleSheet} from 'react-native';

let style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#1e1e1e',
    justifyContent: 'center',
    alignItems: 'center',
  },
  continueBtn: {
    width: Dimensions.get('window').width * 0.625,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#0d0d0d',
    marginVertical: 20,
    paddingHorizontal: 8,
    paddingVertical: 12,
    borderRadius: 12,
  },
  containerResend: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 12,
  },
  verifyText: {
    fontSize: Platform.OS == 'ios' ? 18 : 20,
    color: '#ffffff',
    fontFamily: 'Roboto-Regular',
  },
});

export default style;
