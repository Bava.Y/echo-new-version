import {Platform, StyleSheet} from 'react-native';
import theme from '../../../../utils/theme';

const styles = StyleSheet.create({
  modalView1: {
    width: '87.5%',
    backgroundColor: theme.primaryColor,
    borderRadius: 8,
    margin: 16,
    padding: 16,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 4,
  },
  starttxt: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    color: theme.white,
    textAlign: 'center',
    lineHeight: 20,
  },
});
export default styles;
