import {StackActions} from '@react-navigation/native';
import jwt from 'jwt-decode';
import React, {useState} from 'react';
import {
  KeyboardAvoidingView,
  Platform,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import LITERALS from '../../assets/strings';
import OTPView from '../../components/OTPView';
import style from './styles.js';

const OTPScreen = ({navigation, route}) => {
  let otp = route.params.data.otp;
  let userInfo = jwt(route.params.data.token);

  const [otpLogin, setOtp] = useState('');
  const [validationError, setValidationError] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');

  const onTextChange = (text) => {
    setOtp(text);
  };

  const handleClick = () => {
    if (otpLogin.length === 4 && otp == otpLogin) {
      navigation.dispatch(
        StackActions.replace('SetMpinScreen', {userInfo: userInfo}),
      );
    } else {
      setErrorMessage('Enter proper OTP');
      setValidationError(true);
    }
  };

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : null}
      keyboardVerticalOffset={Platform.OS === 'ios' ? 40 : 0}
      style={style.container}>
      <Text style={style.verifyText}>{LITERALS.verify}</Text>

      <Text
        style={{
          fontSize: Platform.OS == 'ios' ? 14 : 16,
          fontFamily: 'Roboto-Regular',
          color: 'white',
          marginTop: 8,
        }}>{`${LITERALS.sendTo} +1 - xxxxxxxx${
        userInfo.mobile ? userInfo.mobile.substr(8, 9) : '89'
      }`}</Text>

      <OTPView
        tintColor="#f0a223"
        handleTextChange={onTextChange}
        otpValue={otp}
      />

      <View style={style.containerResend}>
        <Text
          style={{
            fontSize: Platform.OS == 'ios' ? 14 : 16,
            fontFamily: 'Roboto-Regular',
            color: 'white',
          }}>
          {LITERALS.dontreceive}
        </Text>
      </View>

      {validationError ? (
        <Text
          style={{
            fontSize: Platform.OS == 'ios' ? 12 : 14,
            color: 'red',
            marginVertical: 10,
          }}>
          {errorMessage}
        </Text>
      ) : null}

      <TouchableOpacity activeOpacity={0.7} onPress={handleClick}>
        <View style={style.continueBtn}>
          <Text
            style={{
              fontSize: Platform.OS == 'ios' ? 14 : 16,
              color: 'white',
              fontFamily: 'Roboto-Regular',
            }}>
            {LITERALS.continue}
          </Text>
        </View>
      </TouchableOpacity>
      
      <Text style={{fontSize: Platform.OS == 'ios' ? 14 : 16, color: 'white'}}>
        {otp}
      </Text>
    </KeyboardAvoidingView>
  );
};

export default OTPScreen;
