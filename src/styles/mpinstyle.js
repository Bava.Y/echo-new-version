import {Platform} from 'react-native';
import {StyleSheet} from 'react-native';
import theme from '../utils/theme';

let style = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: theme.themeColor,
  },
  verifyText: {
    fontSize: Platform.OS == 'ios' ? 18 : 20,
    color: '#ffffff',
    fontWeight: 'bold',
    fontFamily: 'Roboto-Regular',
  },
  mpinText: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    fontFamily: 'Roboto-Regular',
    color: 'white',
    textAlign: 'center',
    marginTop: 10,
  },
  customButton: {
    width: 60,
    height: 60,
    backgroundColor: 'transparent',
    borderColor: '#FFF',
    borderWidth: 1,
    borderRadius: 30,
  },
  containerResend: {
    alignItems: 'center',
    marginVertical: 15,
    paddingHorizontal:8
  },
  forgot: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    fontFamily: 'Roboto-Bold',
    color: 'white',
  },
  signin: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    fontFamily: 'Roboto-Bold',
    color: theme.secondryColor,
  },
});

export default style;
