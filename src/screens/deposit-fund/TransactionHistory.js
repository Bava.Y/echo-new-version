import {useFocusEffect} from '@react-navigation/native';
import moment from 'moment';
import React, {useCallback, useState} from 'react';
import {ActivityIndicator, ScrollView, Text, View} from 'react-native';
import Snackbar from 'react-native-snackbar';
import {profile} from '../../api/index';
import styles from '../../styles/addFund';
import theme from '../../utils/theme';
import BottomNav from './../../components/BottomNav';
import HeaderNav from './../../components/HeaderNav';

const TransactionHistory = () => {
  // TransactionHistory Variables
  const [responseData, setResponseData] = useState(null);

  useFocusEffect(
    useCallback(() => {
      getTransactionHistory();
    }, []),
  );

  const getTransactionHistory = async () => {
    const response = await profile.getTransactionHistory();

    if (response && response.keyword == 'success') {
      Snackbar.show({
        duration: Snackbar.LENGTH_SHORT,
        text: response.message,
      });

      setResponseData(response.data);
    } else if (response.keyword == 'failed') {
      Snackbar.show({
        duration: Snackbar.LENGTH_SHORT,
        text: response.message,
      });

      setResponseData([]);
    } else {
      Snackbar.show({
        backgroundColor: 'red',
        duration: Snackbar.LENGTH_SHORT,
        text: response.message,
      });

      setResponseData([]);
    }
  };

  const handleStatusColor = (status) => {
    switch (status) {
      case 'approved':
        return '#808000';

      case 'canceled':
      case 'rejected':
        return '#FF0000';

      case 'complete':
        return '#008000';

      case 'queued':
        return '#FFA500';

      case 'pending':
        return '#8000FF';

      default:
        return '#6495ED';
    }
  };

  return (
    <>
      <HeaderNav title="Transaction History" isBack={true} />

      {responseData ? (
        responseData.length != 0 ? (
          <ScrollView
            keyboardShouldPersistTaps={'handled'}
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
            style={styles.container}>
            <View style={{paddingHorizontal: 8, paddingVertical: 8}}>
              {responseData.map((transactionData, index) => (
                <View
                  key={index}
                  style={{
                    backgroundColor: theme.primaryColor,
                    borderRadius: 4,
                    marginVertical: 8,
                    paddingHorizontal: 16,
                    paddingVertical: 16,
                  }}>
                  <View style={styles.viewdetail}>
                    <View style={{flex: 0.5, justifyContent: 'center'}}>
                      <Text style={styles.viewheading}>Relationship Type</Text>

                      <Text
                        style={[styles.viewdata, {textTransform: 'uppercase'}]}>
                        {transactionData.type}
                      </Text>
                    </View>

                    <View style={{flex: 0.5, justifyContent: 'center'}}>
                      <Text style={styles.viewheading}>Amount</Text>

                      <Text style={styles.viewdata}>
                        $ {transactionData.amount}
                      </Text>
                    </View>
                  </View>

                  <View style={{marginTop: 8}}>
                    <Text style={styles.viewheading}>
                      Transaction Id:{' '}
                      <Text style={styles.viewdata}>{transactionData.id}</Text>
                    </Text>
                  </View>

                  {transactionData.reason && (
                    <View style={{marginTop: 8}}>
                      <Text style={styles.viewheading}>
                        Reason:{' '}
                        <Text style={styles.viewdata}>
                          {transactionData.reason}
                        </Text>
                      </Text>
                    </View>
                  )}

                  <View style={{marginTop: 8}}>
                    <Text style={[styles.viewdata, {color: '#ffffffc2'}]}>
                      {moment(transactionData.created_at).format('LLL')}
                    </Text>
                  </View>

                  <View
                    style={{
                      position: 'absolute',
                      backgroundColor: handleStatusColor(
                        transactionData.status.toLowerCase(),
                      ),
                      borderBottomRightRadius: 4,
                      paddingHorizontal: 8,
                      paddingVertical: 4,
                      right: 0,
                      bottom: 0,
                    }}>
                    <Text style={styles.statusTxt}>
                      {transactionData.status}
                    </Text>
                  </View>
                </View>
              ))}
            </View>
          </ScrollView>
        ) : (
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: theme.themeColor,
            }}>
            <Text style={styles.noDataTxt}>No transaction(s) found</Text>
          </View>
        )
      ) : (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: theme.themeColor,
          }}>
          <ActivityIndicator size="large" color={theme.white} />
        </View>
      )}

      <BottomNav routeName="Profile" />
    </>
  );
};

export default TransactionHistory;
