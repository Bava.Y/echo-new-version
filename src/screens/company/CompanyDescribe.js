import {useNavigation} from '@react-navigation/native';
import React, {useContext, useEffect, useState} from 'react';
import {
  BackHandler,
  Platform,
  Image,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Modal from 'react-native-modal';
import {DISCOVER} from '../../assets/images/index';
import {UserDataContext} from '../../context/UserDataContext';
import theme from '../../utils/theme';
import CorpAction from './CorpAction';
import PeerGroup from './PearGroup';
import styles from './style';

const CompanyDescribe = ({makeModalFalse}) => {
  const navigation = useNavigation();
  const [isPeargroupVisible, setPeargroupVisible] = useState(false);
  const [isCorpActionVisible, setCorpActionVisible] = useState(false);
  const {companyDescribe} = useContext(UserDataContext);
  const [details] = useState(companyDescribe);

  const hidePeer = () => setPeargroupVisible(false);

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);

    return () => {
      BackHandler.removeEventListener(
        'hardwareBackPress',
        handleBackButtonClick,
      );
    };
  }, []);

  function handleBackButtonClick() {
    isPeargroupVisible ? setPeargroupVisible(false) : navigation.goBack();
    return true;
  }
  
  return (
    <>
      <View style={{height: '100%', backgroundColor: theme.secondryColor}}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-evenly',
            alignItems: 'center',
            marginHorizontal: 16,
            marginVertical: 16,
          }}>
          <TouchableOpacity
            onPress={() => {
              setPeargroupVisible(true);
            }}>
            <View
              style={{
                width: 72,
                height: 72,
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: theme.white,
                borderColor: theme.secondryColor,
                borderWidth: 1,
                borderRadius: 72 / 2,
                padding: 8,
              }}>
              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 12 : 14,
                  textAlign: 'center',
                }}>
                Peer
              </Text>
              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 12 : 14,
                  textAlign: 'center',
                }}>
                Group
              </Text>
            </View>
          </TouchableOpacity>

          <View
            style={{
              width: 72,
              height: 72,
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: theme.white,
              borderColor: theme.secondryColor,
              borderWidth: 1,
              borderRadius: 72 / 2,
              padding: 8,
            }}>
            <Image
              source={details.logo ? {uri: details.logo} : DISCOVER}
              style={{width: 68, height: 68, borderRadius: 68 / 2}}
            />
          </View>

          <TouchableOpacity
            onPress={() => {
              makeModalFalse();
              navigation.navigate('CorpAction', {
                companyName: details.companyName,
              });
            }}>
            <View
              style={{
                width: 72,
                height: 72,
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: theme.white,
                borderColor: theme.secondryColor,
                borderWidth: 1,
                borderRadius: 72 / 2,
                padding: 8,
              }}>
              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 12 : 14,
                  textAlign: 'center',
                }}>
                Corp
              </Text>
              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 12 : 14,
                  textAlign: 'center',
                }}>
                Actions
              </Text>
            </View>
          </TouchableOpacity>
        </View>

        <View style={styles.toptabhead}>
          <View style={styles.topheadingbtn}>
            <TouchableOpacity
              onPress={() => {
                makeModalFalse();
                navigation.navigate('Inside', {
                  initial: 'Inside Roaster',
                  symbol: details.symbol,
                  companyName: details.companyName,
                });
              }}>
              <Text style={styles.topheadingtxt}>Insider Roster</Text>
            </TouchableOpacity>
          </View>

          <View style={styles.topheadingbtn}>
            <TouchableOpacity
              onPress={() => {
                makeModalFalse();
                navigation.navigate('Inside', {
                  initial: 'Inside Summary',
                  symbol: details.symbol,
                  companyName: details.companyName,
                });
              }}>
              <Text style={styles.topheadingtxt}>Insider Summary</Text>
            </TouchableOpacity>
          </View>

          <View style={styles.topheadingbtn}>
            <TouchableOpacity
              onPress={() => {
                makeModalFalse();
                navigation.navigate('Inside', {
                  initial: 'Inside Transaction',
                  symbol: details.symbol,
                  companyName: details.companyName,
                });
              }}>
              <Text style={styles.topheadingtxt}>Insider Transactions</Text>
            </TouchableOpacity>
          </View>
        </View>

        <ScrollView
          keyboardShouldPersistTaps={'handled'}
          showsVerticalScrollIndicator={false}
          style={{marginTop: 8, marginBottom: 24}}>
          <View style={styles.describeitem}>
            <Text style={styles.describetitle}>Description</Text>
            <Text style={[styles.describecontent, {textAlign: 'justify'}]}>
              {'\t'}
              {details.description}
            </Text>
          </View>
          <View style={styles.describeitem}>
            <Text style={styles.describetitle}>CEO</Text>
            <Text style={styles.describecontent}>{details.ceo}</Text>
          </View>
          <View style={styles.describeitem}>
            <Text style={styles.describetitle}>Website</Text>
            <Text style={styles.describecontent}>{details.website}</Text>
          </View>
          <View style={styles.describeitem}>
            <Text style={styles.describetitle}>Address</Text>
            <Text style={styles.describecontent}>
              {details.address} {details.address2}, {details.city},
              {details.state} - {details.zip}
            </Text>
          </View>
          <View style={styles.describeitem}>
            <Text style={styles.describetitle}>Number of Employees</Text>
            <Text style={styles.describecontent}>
              {details.employees === null ? ' - ' : details.employees}
            </Text>
          </View>
          <View style={styles.describeitem}>
            <Text style={styles.describetitle}>Phone</Text>
            <Text style={styles.describecontent}>{details.phone}</Text>
          </View>
          <View style={styles.describeitem}>
            <Text style={styles.describetitle}>Industry</Text>
            <Text style={styles.describecontent}>{details.industry}</Text>
          </View>
          <View style={styles.describeitem}>
            <Text style={styles.describetitle}>Sector</Text>
            <Text style={styles.describecontent}>{details.sector}</Text>
          </View>
        </ScrollView>
      </View>

      {/* Peer Group */}
      <Modal
        isVisible={isPeargroupVisible}
        onBackdropPress={() => setPeargroupVisible(false)}>
        <PeerGroup isVisible={makeModalFalse} hide={hidePeer} />
      </Modal>

      {/* Corp Action */}
      <Modal
        isVisible={isCorpActionVisible}
        onBackdropPress={() => setCorpActionVisible(false)}>
        <CorpAction />
      </Modal>
    </>
  );
};

export default CompanyDescribe;
