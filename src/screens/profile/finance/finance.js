import React, {useContext} from 'react';
import {View} from 'react-native';
import {UserDataContext} from '../../../context/UserDataContext';
import theme from '../../../utils/theme';
import BankAccount from './bank-account';
import FinancialSummary from './finance-summary-tab';

const FinancialScreen = (props) => {
  // Context Variables
  const {alpacaConfigStatus, tdConfigStatus} = useContext(UserDataContext);

  return !alpacaConfigStatus && !tdConfigStatus ? (
    <View
      style={{
        flex: 1,
        backgroundColor: theme.themeColor,
        paddingHorizontal: 8,
        paddingVertical: 8,
      }}>
      <BankAccount {...props} />
    </View>
  ) : (
    <FinancialSummary initialRoute={'Fund'} />
  );
};

export default FinancialScreen;
