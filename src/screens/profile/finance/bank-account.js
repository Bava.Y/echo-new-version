import AsyncStorage from '@react-native-community/async-storage';
import _ from 'lodash';
import moment from 'moment';
import React, {useContext, useEffect, useRef, useState} from 'react';
import {
  ActivityIndicator,
  FlatList,
  Image,
  Platform,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  KeyboardAvoidingView,
} from 'react-native';
import Modal from 'react-native-modal';
import {PlaidLink, PlaidProduct} from 'react-native-plaid-link-sdk';
import RBSheet from 'react-native-raw-bottom-sheet';
import SelectDropdown from 'react-native-select-dropdown';
import Snackbar from 'react-native-snackbar';
import Icon from 'react-native-vector-icons/FontAwesome';
import FontAwesomeIcons from 'react-native-vector-icons/FontAwesome5';
import Ionicons from 'react-native-vector-icons/Ionicons';
import WebView from 'react-native-webview';
import {ameritrade, plaid, profile} from '../../../api';
import {ALPACA, TDAMIRTADE} from '../../../assets/images/index';
import AmeritradeRefreshToken from '../../../components/AmeritradeRefreshToken';
import Loading from '../../../components/Loading';
import {UserDataContext} from '../../../context/UserDataContext';
import {
  AMERITRADE_CLIENT_ID,
  PLAID_CLIENT_ID,
  PLAID_CLIENT_NAME,
  PLAID_COUNTRY_CODES,
  PLAID_SECRET_KEY,
} from '../../../utils/constants';
import theme from '../../../utils/theme';
import SuccessPopUp from '../components/successPopUp/successPopUp';
import ConfirmPopUp from './ConfirmPopUp/successPopUp';
import styles from './styles';
import DocumentPicker, {types} from 'react-native-document-picker';
import axios, {cancelToken, isCancel} from 'axios';
import {auth} from '../../../api/index';

const BankAccount = (props) => {
  // BankAccount Variables
  const [accShow, setAccShow] = useState(false);
  const [alpacaAccountNo, setAlpacaAccountNo] = useState(null);
  const [
    ameritradeSelectedAccountId,
    setAmeritradeSelectedAccountId,
  ] = useState(null);
  const [ameritradeTokenExpiry, setAmeritradeTokenExpiry] = useState(null);
  const [ameritradeTokenResponse, setAmeritradTokenResponse] = useState(null);
  const [tdAmeritradeUserAccounts, setTdAmeritradeAccounts] = useState([]);
  const [bankaccountno, SetBankAccountNo] = useState(null);
  const [bankrouting, SetBankRouting] = useState(null);
  const [brokerSelected, setBrokerSelected] = useState(null);
  const [creditShow, setCreditShow] = useState(false);
  const [creditcardno, SetCreditcardno] = useState(null);
  const [responseData, setResponseData] = useState(null);
  const [
    encryptAmeritradeSelectedAccountId,
    setEncryptAmeritradeSelectedAccountId,
  ] = useState(null);
  const [expiration, SetExpiration] = useState(null);
  const [res, setRes] = useState(null);
  const [securitycode, SetSecuritycode] = useState(null);
  const [selectedAccountType, setSelectedAccountType] = useState(null);
  const [selectedBank, setSelectedBank] = useState('');
  const [linkToken, setLinkToken] = useState(null);

  const [selectOne, setSelectOne] = useState(false);
  const [trade, setTrade] = useState(null);
  const [PlaidConnect, setPlaidConnect] = useState(null);
  const [noanyOne, setnoanyOne] = useState(null);
  const [fileUploadResponse, setFileUploadResponse] = useState([]);
  const [FileName, setFileName] = useState([]);
  const [folder, setFolder] = useState(null);
  const [uploadingtLoadingStatus, setuploadingtLoadingStatus] = useState(false);
  const [uploadingtLoadingStatuss, setuploadingtLoadingStatuss] = useState(
    false,
  );

  const sentGmail = useRef();

  // Context Variables
  const {
    authUserData,
    alpacaConfigStatus,
    ameritradeRefreshTokenStatus,
    setAmeritradeRefreshTokenStatus,
    setAlpacaConfigStatus,
    setTDConfigStatus,
    tdConfigStatus,
    token,
  } = useContext(UserDataContext);

  // Error Variables
  const initialValidationError = {
    route: false,
    account: false,
    expiry: false,
    security: false,
    card: false,
    brokerSelected: false,
  };
  const [validationError, setValidationError] = useState({
    ...initialValidationError,
  });
  const [
    ameritradeSelectedAccountIdError,
    setAmeritradeSelectedAccountIdError,
  ] = useState(false);
  const [selectedAccountTypeError, setSelectedAccountTypeError] = useState(
    false,
  );

  // Ref Variables
  const selectAccount = useRef();
  const webView = useRef(null);

  // Other Variables
  const [helperbankaccountno, SetHelperBankAccountNo] = useState(null);
  const [helpercreditcardno, SetHelperCreditCardNo] = useState(null);
  const [helperSecuritycode, SetHelperSecuritycode] = useState(null);
  const [ameritradeAccountModal, setAmeritradeAccountModal] = useState(false);
  const [confmodal, setConfModal] = useState(false);
  const [loading, setLoading] = useState(false);
  const [modal, setModal] = useState(false);
  const [successText, setSuccessText] = useState(undefined);
  const [tdAmeritradeLoginModal, setTdAmeritradeLoginModal] = useState(false);
  const [connectionOptions, setConnectionOptions] = useState(null);

  let brokerOptions = [
    {
      height: 33,
      logo: ALPACA,
      option_id: '0',
      option_name: 'Alphaca',
      status: alpacaConfigStatus,
      width: '60%',
    },
    {
      height: 25,
      logo: TDAMIRTADE,
      option_id: '1',
      option_name: 'TD Ameritrade',
      status: tdConfigStatus,
      width: '80%',
    },
  ];
  let helperBrokerConnectionID;
  let uri = `https://auth.tdameritrade.com/auth?response_type=code&redirect_uri=http%3A%2F%2Fechoapp.biz%2F&client_id=${AMERITRADE_CLIENT_ID}%40AMER.OAUTHAP`;

  useEffect(() => {
    handleConnectionOptions();

    getBankDetail(1);
  }, [alpacaConfigStatus, tdConfigStatus]);

  const handleConnectionOptions = () => {
    let connectionOptions = [];

    !alpacaConfigStatus &&
      connectionOptions.push({label: 'Alpaca', value: 'Alpaca'});

    !tdConfigStatus &&
      connectionOptions.push({
        label: 'TD Ameritrade',
        value: 'TD Ameritrade',
      });

    alpacaConfigStatus && setBrokerSelected('0');

    tdConfigStatus && setBrokerSelected('1');

    setConnectionOptions(connectionOptions);

    if (connectionOptions.length == 1) {
      setSelectedAccountType(connectionOptions[0]);
    } else {
      setSelectedAccountType(null);
    }
  };

  const getBankDetail = async (status) => {
    status == 1 && setLoading(true);

    let response = await profile.getBankDetail(token);

    if (response.data) {
      let myData = response.data;

      setResponseData(myData);

      SetBankRouting(myData.bank_routing);
      myData.account_no
        ? SetBankAccountNo(encryptText(myData.account_no, 4))
        : SetBankAccountNo(null);
      SetHelperBankAccountNo(myData.account_no || null);
      setAccShow(false);
      myData.credit_card_no
        ? SetCreditcardno(encryptText(myData.credit_card_no, 4))
        : SetCreditcardno(null);
      SetHelperCreditCardNo(myData.credit_card_no || null);
      setCreditShow(false);
      setSelectedBank(myData.type || '');
      myData.security_code
        ? SetSecuritycode(encryptText(myData.security_code, 1))
        : SetSecuritycode(null);
      SetHelperSecuritycode(myData.security_code || null);
      SetExpiration(myData.expiry_date || null);

      setAlpacaAccountNo(myData.alpaca_account_no || null);
      setAmeritradeSelectedAccountId(myData.account_id || null);
      setAmeritradTokenResponse(
        myData.refresh_token
          ? {
              refresh_token: myData.refresh_token,
              refresh_token_expires_in: myData.expired_token_at * 86400,
            }
          : null,
      );
      setEncryptAmeritradeSelectedAccountId(myData.account_id || null);
      setAmeritradeTokenExpiry(myData.expired_dt_token || null);

      const brokerStatus =
        myData.broker_connection != 'null'
          ? String(myData.broker_connection)
          : null;

      setBrokerSelected(brokerStatus || null);

      brokerStatus &&
        AsyncStorage.setItem('brokerConnection', JSON.stringify(brokerStatus));

      !Boolean(myData?.plaid_access_token) &&
        !Boolean(myData?.plaid_account_id) &&
        fetchPlaidLinkToken();
    }

    setLoading(false);
  };

  const fetchPlaidLinkToken = async () => {
    const requestData = {
      client_id: PLAID_CLIENT_ID,
      secret: PLAID_SECRET_KEY,
      client_name: PLAID_CLIENT_NAME,
      country_codes: PLAID_COUNTRY_CODES,
      language: 'en',
      products: [PlaidProduct.INVESTMENTS],
      user: {client_user_id: String(authUserData.user_id)},
    };

    const response = await plaid.getLinkToken(requestData);

    Boolean(response?.link_token) && setLinkToken(response.link_token);
  };

  const encryptText = (string, showDigits) => {
    let encryptedStr;

    string = string ? string : '';

    if (string.length > showDigits) {
      const str = string
        .slice(0, string.length - showDigits)
        .replace(/[a-z0-9]/g, 'X');

      encryptedStr = str + string.slice(-showDigits);
    } else {
      encryptedStr = string ? string : null;
    }

    return encryptedStr;
  };

  const creditType = (text) => {
    let american = /^(?:3[47][0-9]{1,13})$/;
    let discover = /^(?:6(?:011|5[0-9][0-9])[0-9]{1,12})$/;
    let visa = /^(?:4[0-9]{1,16})$/;

    if (american.test(text)) {
      return 'american';
    } else if (discover.test(text)) {
      return 'discover';
    } else if (visa.test(text)) {
      return 'visa';
    } else {
      return 'Other';
    }
  };

  const displayActivityIndicator = () => {
    return <ActivityIndicator size={24} color={theme.primaryColor} />;
  };

  const ameritradeUserAccounts = async (authResponse) => {
    const tdAmeritradeTokenResponse = await ameritrade.generateAccessToken(
      authResponse.refresh_token,
    );

    if (
      Boolean(tdAmeritradeTokenResponse) &&
      tdAmeritradeTokenResponse.access_token
    ) {
      const accountsResponse = await ameritrade.getAccounts(
        tdAmeritradeTokenResponse.access_token,
      );

      if (accountsResponse.length !== 0) {
        if (accountsResponse.length > 1) {
          setTdAmeritradeAccounts(accountsResponse);

          setAmeritradeAccountModal(true);
        } else {
          Snackbar.show({
            duration: Snackbar.LENGTH_LONG,
            text: `Please wait! Your TD Ameritrade account [${accountsResponse[0].securitiesAccount.accountId}] is syncing!`,
          });

          setAmeritradeSelectedAccountId(
            accountsResponse[0].securitiesAccount.accountId,
          );

          setEncryptAmeritradeSelectedAccountId(
            accountsResponse[0].securitiesAccount.accountId,
          );

          let formData = new FormData();

          formData.append('broker_connection', '1');
          formData.append('refresh_token', authResponse.refresh_token);
          formData.append('expires_in', authResponse.refresh_token_expires_in);
          formData.append(
            'account_id',
            accountsResponse[0].securitiesAccount.accountId,
          );

          await AsyncStorage.setItem('brokerConnection', JSON.stringify('1'));

          let brokerUpdateResponse = await profile.updateBroker(
            token,
            formData,
          );

          if (
            brokerUpdateResponse &&
            brokerUpdateResponse.keyword == 'success'
          ) {
            setTdAmeritradeLoginModal(false);

            setTDConfigStatus(true);

            Snackbar.dismiss();

            getBankDetail(1);
          } else {
            Snackbar.dismiss();

            setTdAmeritradeLoginModal(false);
          }
        }
      } else {
        Snackbar.show({
          duration: Snackbar.LENGTH_LONG,
          text: 'No trading accounts found in TD Ameritrade!',
          backgroundColor: 'red',
        });
      }
    } else {
      Snackbar.show({
        duration: Snackbar.LENGTH_LONG,
        text: 'Failed to get access token',
        backgroundColor: 'red',
      });
    }
  };

  const handleAddAccount = () => {
    setSelectedAccountTypeError(false);

    setSelectedAccountType(null);

    selectAccount.current.open();
  };

  const submit = () => {
    if (selectedAccountType?.value ?? null) {
      handleAccounts();
    } else {
      setSelectedAccountTypeError(true);

      setConfModal(false);
    }
  };

  const handleAccounts = () => {
    selectAccount.current.close();

    switch (selectedAccountType?.value ?? null) {
      case 'Alpaca':
        setTimeout(() => {
          setConfModal(true);
        }, 250);
        break;

      case 'TD Ameritrade':
        setTimeout(() => {
          helperBrokerConnectionID = 1;

          setTdAmeritradeLoginModal(true);
        }, 250);
        break;

      default:
        break;
    }
  };

  const submitForm = async () => {
    let formData = await validateFormData();

    if (formData != undefined) {
      setModal(true);
      setSuccessText('Bank account information updated');
      let responce = await profile.updateBankDetail();

      setTimeout(() => {
        setRes(responce);
      }, 500);

      setTimeout(() => {
        setRes(null);
      }, 2000);

      setTimeout(() => {
        setModal(false);
      }, 2000);
    }
  };

  const validateFormData = async () => {
    let formData = new FormData();
    let error = validationError;

    if (!bankrouting) {
      error.route = 'Enter bank routing number';
      formData = undefined;
    } else if (validRoutingNumber(bankrouting) == false) {
      error.route = 'Enter valid bank routing number';
      formData = undefined;
    } else {
      error.route = false;
      formData && formData.append('bank_routing', bankrouting);
    }

    if (!helperbankaccountno) {
      error.account = 'Enter bank account number';
      formData = undefined;
    } else if (String(helperbankaccountno).length < 10) {
      error.account = 'Enter valid bank account number';
      formData = undefined;
    } else {
      error.account = false;
      formData && formData.append('account_no', helperbankaccountno);
    }

    formData && formData.append('type', selectedBank);

    if (!helpercreditcardno) {
      error.card = 'Enter credit card number';
      formData = undefined;
    } else if (
      ((selectedBank == 'VI' || selectedBank == 'DS') &&
        helpercreditcardno.length < 16) ||
      (selectedBank == 'AX' && helpercreditcardno.length < 15)
    ) {
      error.card = 'Enter valid credit card number';
      formData = undefined;
    } else {
      error.card = false;
      formData && formData.append('credit_card_no', helpercreditcardno);
    }

    if (!expiration) {
      error.expiry = 'Enter expiry period';
      formData = undefined;
    } else if (String(expiration).length < 5) {
      error.expiry = 'Enter valid expiry period';
      formData = undefined;
    } else if (String(expiration).length == 5) {
      let date = JSON.stringify(new Date());
      let year = date.split('-')[0].substr(3, 4);
      let month = date.split('-')[1];
      let userMonth = expiration.split('/')[0];
      let userYear = expiration.split('/')[1];

      if (Number(userYear) < Number(year)) {
        error.expiry = 'Enter valid period';

        formData = undefined;
      } else if (
        Number(userYear) == Number(year) &&
        Number(userMonth) < Number(month)
      ) {
        error.expiry = 'Enter valid period';

        formData = undefined;
      } else if (Number(userMonth) > 12) {
        error.expiry = 'Enter valid period';

        formData = undefined;
      } else {
        error.expiry = false;

        formData && formData.append('expiry_date', expiration);
      }
    }

    if (!helperSecuritycode) {
      error.security = 'Enter security code';
      formData = undefined;
    } else if (String(helperSecuritycode).length < 3) {
      error.security = 'Enter valid security code';
      formData = undefined;
    } else {
      error.security = false;
      formData && formData.append('security_code', helperSecuritycode);
    }

    setValidationError({...validationError, error});

    return formData;
  };

  const submitBrokerConnection = async () => {
    let error = validationError;

    if (brokerSelected) {
      let formData = new FormData();

      error.brokerSelected = false;

      formData.append('broker_connection', brokerSelected);

      brokerSelected == '1' && [
        formData.append(
          'refresh_token',
          responseData.refresh_token
            ? responseData.refresh_token
            : ameritradeTokenResponse
            ? ameritradeTokenResponse.refresh_token
            : null,
        ),
        formData.append(
          'expires_in',
          responseData.expired_token_at
            ? responseData.expired_token_at * 86400
            : ameritradeTokenResponse
            ? ameritradeTokenResponse.refresh_token_expires_in
            : null,
        ),
        formData.append('account_id', ameritradeSelectedAccountId),
      ];

      await AsyncStorage.setItem(
        'brokerConnection',
        JSON.stringify(brokerSelected),
      );

      setValidationError({...validationError, error});

      setModal(true);

      setSuccessText('Broker connection updated successfully');
      let responce = await profile.updateBroker(token, formData);

      setTimeout(() => {
        setRes(responce);
      }, 500);

      setTimeout(() => {
        setRes(null);

        brokerSelected == '0' && setAlpacaConfigStatus(true);
      }, 2000);

      setTimeout(() => {
        setModal(false);

        getBankDetail(1);
      }, 2000);
    } else {
      error.brokerSelected = 'Please choose any one broker connection';

      setValidationError({...validationError, error});
    }
  };

  const validRoutingNumber = (routing) => {
    if (routing.length !== 9) {
      return false;
    }

    let checksumTotal =
      7 *
        (parseInt(routing.charAt(0), 10) +
          parseInt(routing.charAt(3), 10) +
          parseInt(routing.charAt(6), 10)) +
      3 *
        (parseInt(routing.charAt(1), 10) +
          parseInt(routing.charAt(4), 10) +
          parseInt(routing.charAt(7), 10)) +
      9 *
        (parseInt(routing.charAt(2), 10) +
          parseInt(routing.charAt(5), 10) +
          parseInt(routing.charAt(8), 10));

    let checksumMod = checksumTotal % 10;

    if (checksumMod !== 0) {
      return false;
    } else {
      return true;
    }
  };

  const handleAmeritradeRefreshTokenStatus = () => {
    setAmeritradeRefreshTokenStatus(!ameritradeRefreshTokenStatus);
  };

  const fileUploadData = [];
  fileUploadData.push(FileName.slice(0, 13));

  const removeName = (index) => {
    const files = [...fileUploadResponse];
    files.splice(index, 1);
    setFileUploadResponse(files);
  };

  const removeFileName = (index) => {
    const files = [...FileName];
    files.splice(index, 1);
    setFileName(files);
  };

  const Confirm = () => {
    if (trade === true) {
      sentGmail.current.open();
    } else if (PlaidConnect === true) {
      navigation.navigate('Profile', {
        screen: 'Financial',
        params: {
          screen: 'Bank Account',
        },
      });
      setSelectOne(false);
    } else if (noanyOne === true) {
      navigation.navigate('Profile');
      setSelectOne(false);
    }
  };

  const handleDocumentPicker = async (onSuccess) => {
    try {
      const fileData = await DocumentPicker.pick({
        type: [DocumentPicker.types.allFiles],
        allowMultiSelection: true,
      });
      onSuccess(fileData);

      setFileUploadResponse([...fileUploadResponse, fileData]);
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
      } else {
        Snackbar.show(err);
      }
    }
  };

  // const handleDocumentSelection = () => {
  //   try {
  //     handleDocumentPicker(async (onSuccess) => {
  //       setuploadingtLoadingStatus(true);

  //       let requestData = new FormData();
  //       requestData.append('file', onSuccess);

  //       let response = await auth.portfolioFilesupload(requestData);

  //       setFolder(response.folder);
  //       FileName.push(response.data);
  //       Snackbar.show({
  //         backgroundColor: 'green',
  //         duration: Snackbar.LENGTH_SHORT,
  //         text: response.message,
  //       });
  //       setuploadingtLoadingStatus(false);
  //     });
  //   } catch (err) {
  //     Snackbar.show(err);
  //   }
  // };

  const handleDocumentSelection = () => {
    try {
      handleDocumentPicker(async (onSuccess) => {
        setuploadingtLoadingStatus(true);
        let requestData = new FormData();
        Platform.OS === 'android'
          ? requestData.append('file', {
              uri: onSuccess[0].uri,
              type: onSuccess[0].type,
              name: onSuccess[0].name,
            })
          : requestData.append('file', onSuccess);
        let response = await auth.portfolioFilesupload(requestData);
        setFolder(response.folder);
        FileName.push(response.data);
        setuploadingtLoadingStatus(false);
        Snackbar.show({
          backgroundColor: 'green',
          duration: Snackbar.LENGTH_SHORT,
          text: response.message,
        });
      });
    } catch (err) {
      Snackbar.show(err);
    }
  };

  const uploadDataFile = async () => {
    setuploadingtLoadingStatuss(true);
    let requestData = new FormData();
    requestData.append('file_names', JSON.stringify(FileName));
    requestData.append('folder', folder);

    if (FileName.length !== 0 && folder !== null) {
      let response = await auth.portfolioFilesInsert(requestData);
      setuploadingtLoadingStatuss(false);
      sentGmail.current.close();
      setSelectOne(false);
      setFileUploadResponse([]);

      setTrade(false);
      setFileName([]);
      setFolder(null);
      Snackbar.show({
        backgroundColor: 'green',
        duration: Snackbar.LENGTH_LONG,
        text: 'Email Sent Successfully...',
      });
    } else {
      setuploadingtLoadingStatuss(false);
      Snackbar.show({
        backgroundColor: 'red',
        duration: Snackbar.LENGTH_SHORT,
        text: 'Please Upload The File..!',
      });
    }
  };

  const CancelBtn = () => {
    setFileUploadResponse([]);

    setTrade(false);
    sentGmail.current.close();
  };

  const handleRemoveDocuments = async () => {
    // setFileUploadResponse([]);

    setFileName([]);
    setFolder(null);

    Snackbar.show({
      duration: Snackbar.LENGTH_SHORT,
      text: 'Removing The File...',
    });
  };

  const cancelpopup = () => {
    setTrade(false);
    setPlaidConnect(false);
    setnoanyOne(false);
    setSelectOne(!selectOne);
  };

  const selectName = fileUploadResponse.map((file, index) => {
    return Platform.OS === 'android' ? file[0]?.name : file?.name;
  });

  return (
    <View style={styles.container}>
      {!loading ? (
        <ScrollView
          keyboardShouldPersistTaps={'handled'}
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}>
          <View style={styles.mainsec}>
            <Text style={styles.headingTxt}>Manage Bank Accounts</Text>

            <View style={styles.selectfield}>
              <View style={styles.selectfield}>
                <TextInput
                  placeholder="Bank Routing"
                  keyboardType={'number-pad'}
                  placeholderTextColor="white"
                  style={styles.selectinput}
                  onChangeText={(text) => {
                    setValidationError({
                      ...validationError,
                      route: text ? false : 'Please enter valid bank routing',
                    });

                    SetBankRouting(text);
                  }}
                  value={bankrouting}
                />
              </View>

              {validationError.route && (
                <Text style={styles.errorMsg}>{validationError.route}</Text>
              )}

              <View style={styles.selectfield}>
                <View style={styles.selectIconinput}>
                  <TextInput
                    keyboardType={'number-pad'}
                    maxLength={17}
                    value={bankaccountno}
                    onFocus={() => {
                      SetBankAccountNo(helperbankaccountno);
                    }}
                    onBlur={() => {
                      SetBankAccountNo(encryptText(bankaccountno, 4));
                    }}
                    placeholder="Bank Account Number"
                    placeholderTextColor="white"
                    onChangeText={(text) => {
                      setValidationError({
                        ...validationError,
                        account: text ? false : 'Enter bank account number',
                      });

                      SetBankAccountNo(text);
                      SetHelperBankAccountNo(text);
                    }}
                    style={styles.textInput}
                  />

                  <TouchableOpacity
                    onPress={() => {
                      setAccShow(!accShow);

                      accShow
                        ? SetBankAccountNo(encryptText(bankaccountno, 4))
                        : SetBankAccountNo(helperbankaccountno);
                    }}>
                    <Icon
                      name={accShow ? 'eye' : 'eye-slash'}
                      size={18}
                      color="#fff"
                    />
                  </TouchableOpacity>
                </View>
              </View>

              {validationError.account && (
                <Text style={styles.errorMsg}>{validationError.account}</Text>
              )}

              {!_.isEmpty(selectedBank) &&
                !_.isNull(selectedBank) &&
                selectedBank != 'N' && (
                  <View style={styles.selectfield}>
                    <TextInput
                      editable={false}
                      placeholder="Bank"
                      placeholderTextColor="white"
                      style={styles.selectinput}
                      value={selectedBank}
                    />
                  </View>
                )}

              <View style={styles.selectfield}>
                <View style={styles.selectIconinput}>
                  <TextInput
                    maxLength={16}
                    keyboardType={'number-pad'}
                    value={creditcardno}
                    onFocus={() => {
                      SetCreditcardno(helpercreditcardno);
                    }}
                    onBlur={() => {
                      SetCreditcardno(encryptText(creditcardno, 4));
                    }}
                    placeholder="Credit Card Number"
                    placeholderTextColor="white"
                    textContentType="creditCardNumber"
                    style={styles.textInput}
                    onChangeText={(text) => {
                      setValidationError({
                        ...validationError,
                        card: text ? false : 'Enter credit card number',
                      });

                      SetCreditcardno(text);
                      SetHelperCreditCardNo(text);

                      switch (creditType(text)) {
                        case 'american':
                          setSelectedBank('AX');
                          break;

                        case 'discover':
                          setSelectedBank('DS');
                          break;

                        case 'visa':
                          setSelectedBank('VI');
                          break;

                        default:
                          setSelectedBank('N');
                          break;
                      }
                    }}
                  />

                  <TouchableOpacity
                    onPress={() => {
                      setCreditShow(!creditShow);

                      creditShow
                        ? SetCreditcardno(encryptText(creditcardno, 4))
                        : SetCreditcardno(helpercreditcardno);
                    }}>
                    <Icon
                      name={creditShow ? 'eye' : 'eye-slash'}
                      size={18}
                      color="#fff"
                    />
                  </TouchableOpacity>
                </View>
              </View>

              {validationError.card && (
                <Text style={styles.errorMsg}>{validationError.card}</Text>
              )}

              <View
                style={[
                  styles.selectfield,
                  {
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  },
                ]}>
                <View style={{width: '47.5%', justifyContent: 'center'}}>
                  <TextInput
                    keyboardType={'number-pad'}
                    maxLength={5}
                    value={expiration}
                    placeholder="Expiry Date"
                    placeholderTextColor="white"
                    style={styles.selectinput}
                    onChangeText={(text) => {
                      text.length == 2 && [(text = text.concat('/'))];

                      let date = JSON.stringify(new Date());
                      let year = date.split('-')[0].substr(3, 4);
                      let month = date.split('-')[1];
                      let userMonth = text.split('/')[0];
                      let userYear = text.split('/')[1];

                      if (
                        Number(userYear) < Number(year) ||
                        (Number(userYear) == Number(year) &&
                          Number(userMonth) < Number(month)) ||
                        Number(userMonth) > 12
                      ) {
                        setValidationError({
                          ...validationError,
                          expiry: 'Enter valid expiration',
                        });
                      } else {
                        setValidationError({
                          ...validationError,
                          expiry: text ? false : 'Enter valid expiration',
                        });
                      }

                      SetExpiration(text);
                    }}
                  />

                  {validationError.expiry && (
                    <Text style={styles.errorMsg}>
                      {validationError.expiry}
                    </Text>
                  )}

                  {!validationError.expiry && validationError.security && (
                    <Text style={styles.errorMsg}></Text>
                  )}
                </View>

                <View style={{width: '47.5%', justifyContent: 'center'}}>
                  <TextInput
                    keyboardType="number-pad"
                    maxLength={3}
                    value={securitycode}
                    onFocus={() => {
                      SetSecuritycode(helperSecuritycode);
                    }}
                    onBlur={() => {
                      SetSecuritycode(encryptText(securitycode, 1));
                    }}
                    placeholder="Security Code"
                    placeholderTextColor="white"
                    style={styles.selectinput}
                    onChangeText={(text) => {
                      if (!text || text.length < 3) {
                        setValidationError({
                          ...validationError,
                          security: 'Enter valid security code',
                        });
                      } else {
                        setValidationError({
                          ...validationError,
                          security: false,
                        });
                      }

                      SetSecuritycode(text);
                      SetHelperSecuritycode(text);
                    }}
                  />

                  {validationError.security && (
                    <Text style={styles.errorMsg}>
                      {validationError.security}
                    </Text>
                  )}

                  {!validationError.security && validationError.expiry && (
                    <Text style={styles.errorMsg}></Text>
                  )}
                </View>
              </View>
            </View>

            <TouchableOpacity
              style={styles.saveButton}
              onPress={() => {
                submitForm();
              }}>
              <Text style={styles.btntext}>Save Bank Details</Text>
            </TouchableOpacity>
          </View>

          {!alpacaConfigStatus && !tdConfigStatus && (
            <TouchableOpacity
              style={[styles.addBrokerConnection, {marginVertical: 16}]}
              onPress={() => {
                handleAddAccount();
              }}>
              <Text style={styles.btntext}>Add Broker Connection</Text>
            </TouchableOpacity>
          )}

          {(alpacaConfigStatus || tdConfigStatus) && (
            <View style={[styles.mainsec, {marginTop: 16}]}>
              <Text style={styles.headingTxt}>Broker Connections</Text>

              <FlatList
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}
                data={brokerOptions}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({item}, index) => {
                  return (
                    item.status && (
                      <View style={{marginTop: index != 0 ? 8 : 0}}>
                        <View
                          style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                          }}>
                          <View
                            style={{
                              flex: 0.1,
                              justifyContent: 'center',
                              alignItems: 'center',
                            }}>
                            <TouchableOpacity
                              onPress={() => {
                                setValidationError({
                                  ...validationError,
                                  brokerSelected: false,
                                });

                                setBrokerSelected(item.option_id);
                              }}>
                              <View style={styles.outerradius}>
                                {brokerSelected === item.option_id && (
                                  <View style={styles.innerradius} />
                                )}
                              </View>
                            </TouchableOpacity>
                          </View>

                          <TouchableOpacity
                            onPress={() => {
                              setValidationError({
                                ...validationError,
                                brokerSelected: false,
                              });

                              setBrokerSelected(item.option_id);
                            }}
                            style={{
                              flex: 0.275,
                              justifyContent: 'center',
                              alignItems: 'flex-start',
                              paddingHorizontal: 4,
                              paddingVertical: 4,
                            }}>
                            <Image
                              resizeMode="contain"
                              source={item.logo}
                              style={{
                                width: item.width,
                                height: item.height,
                              }}
                            />
                          </TouchableOpacity>

                          <View
                            style={{
                              flex: 0.625,
                              justifyContent: 'center',
                              paddingHorizontal: 4,
                              paddingVertical: 4,
                            }}>
                            {item.option_name === 'TD Ameritrade' ? (
                              <TouchableOpacity
                                onPress={() => {
                                  Snackbar.show({
                                    duration: Snackbar.LENGTH_LONG,
                                    text:
                                      'Please wait! Fetching trading account(s) in TD Ameritrade!',
                                  });

                                  ameritradeUserAccounts(
                                    ameritradeTokenResponse,
                                  );
                                }}>
                                <Text style={styles.accountNoTxt}>
                                  {encryptText(
                                    encryptAmeritradeSelectedAccountId,
                                    4,
                                  )}
                                </Text>
                              </TouchableOpacity>
                            ) : (
                              alpacaAccountNo && (
                                <TouchableOpacity
                                  onPress={() => {
                                    props.navigation.navigate('ViewAlpaca');
                                  }}>
                                  <Text style={styles.accountNoTxt}>
                                    {encryptText(alpacaAccountNo, 4)}
                                  </Text>
                                </TouchableOpacity>
                              )
                            )}

                            {item.option_name === 'TD Ameritrade' &&
                              Boolean(ameritradeTokenExpiry) && (
                                <View style={{marginTop: 4}}>
                                  <Text style={styles.tokenExpiryTxt}>
                                    Token Expires On:{' '}
                                    <Text style={styles.tokenDateTxt}>
                                      {moment(ameritradeTokenExpiry).format(
                                        'LL',
                                      )}
                                    </Text>
                                  </Text>
                                </View>
                              )}
                          </View>
                        </View>

                        {item.option_name === 'TD Ameritrade' &&
                          (Boolean(responseData) &&
                          Boolean(responseData.plaid_access_token) &&
                          Boolean(responseData.plaid_access_token) ? (
                            <View style={styles.plaidConnected}>
                              <Text style={styles.btntext}>
                                Plaid Connected!
                              </Text>
                            </View>
                          ) : (
                            <PlaidLink
                              tokenConfig={{token: linkToken}}
                              onSuccess={async (successResponse) => {
                                Snackbar.show({
                                  duration: Snackbar.LENGTH_INDEFINITE,
                                  text: `Please wait! Syncing your TD Ameritrade account with Plaid!`,
                                });

                                const requestData = {
                                  client_id: PLAID_CLIENT_ID,
                                  secret: PLAID_SECRET_KEY,
                                  public_token: successResponse.publicToken,
                                };

                                const response = await plaid.getAccessToken(
                                  requestData,
                                );

                                if (Boolean(response?.access_token)) {
                                  let formData = new FormData();

                                  formData.append(
                                    'plaid_access_token',
                                    response.access_token,
                                  );

                                  formData.append(
                                    'plaid_account_id',
                                    successResponse.metadata.accounts[0].id,
                                  );

                                  let plaidUpdateResponse = await profile.updatePlaid(
                                    token,
                                    formData,
                                  );

                                  Snackbar.dismiss();

                                  plaidUpdateResponse &&
                                    plaidUpdateResponse?.keyword == 'success' &&
                                    getBankDetail(1);
                                } else {
                                  Snackbar.show({
                                    duration: Snackbar.LENGTH_LONG,
                                    text: `Sorry! Unable to generate access token for Plaid!`,
                                  });
                                }
                              }}
                              onExit={() => {
                                Snackbar.show({
                                  duration: Snackbar.LENGTH_LONG,
                                  text:
                                    "Sorry! We couln't sync your TD Ameritrade account with Plaid! Please, try again!",
                                });
                              }}>
                              <View style={styles.plaidNotConnected}>
                                <Text style={styles.btntext}>
                                  Connect with Plaid
                                </Text>
                              </View>
                            </PlaidLink>
                          ))}
                      </View>
                    )
                  );
                }}
              />

              <FlatList
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}
                data={brokerOptions}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({item}, index) => {
                  return (
                    item.status && (
                      <View style={{marginTop: index != 0 ? 8 : 0}}>
                        {item.option_name === 'Alphaca' &&
                          (Boolean(responseData) &&
                          Boolean(responseData.plaid_access_token) &&
                          Boolean(responseData.plaid_access_token) ? (
                            <View style={styles.plaidConnected}>
                              <Text style={styles.btntext}>
                                Plaid Connected!
                              </Text>
                            </View>
                          ) : (
                            <PlaidLink
                              tokenConfig={{token: linkToken}}
                              onSuccess={async (successResponse) => {
                                Snackbar.show({
                                  duration: Snackbar.LENGTH_INDEFINITE,
                                  text: `Please wait! Syncing your TD Ameritrade account with Plaid!`,
                                });

                                const requestData = {
                                  client_id: PLAID_CLIENT_ID,
                                  secret: PLAID_SECRET_KEY,
                                  public_token: successResponse.publicToken,
                                };

                                const response = await plaid.getAccessToken(
                                  requestData,
                                );

                                if (Boolean(response?.access_token)) {
                                  let formData = new FormData();

                                  formData.append(
                                    'plaid_access_token',
                                    response.access_token,
                                  );

                                  formData.append(
                                    'plaid_account_id',
                                    successResponse.metadata.accounts[0].id,
                                  );

                                  let plaidUpdateResponse = await profile.updatePlaid(
                                    token,
                                    formData,
                                  );

                                  Snackbar.dismiss();

                                  plaidUpdateResponse &&
                                    plaidUpdateResponse?.keyword == 'success' &&
                                    getBankDetail(1);
                                } else {
                                  Snackbar.show({
                                    duration: Snackbar.LENGTH_LONG,
                                    text: `Sorry! Unable to generate access token for Plaid!`,
                                  });
                                }
                              }}
                              onExit={() => {
                                Snackbar.show({
                                  duration: Snackbar.LENGTH_LONG,
                                  text:
                                    "Sorry! We couln't sync your TD Ameritrade account with Plaid! Please, try again!",
                                });
                              }}>
                              <View style={styles.plaidNotConnected}>
                                <Text style={styles.btntext}>
                                   Plaid
                                </Text>
                              </View>
                            </PlaidLink>
                          ))}
                      </View>
                    )
                  );
                }}
              />

              {validationError.brokerSelected && (
                <Text style={styles.errorMsg}>
                  {validationError.brokerSelected}
                </Text>
              )}

              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  marginHorizontal: 8,
                  marginVertical: 8,
                }}>
                {(!alpacaConfigStatus || !tdConfigStatus) && (
                  <TouchableOpacity
                    style={[
                      styles.saveButton,
                      {width: '57.5%', borderRadius: 4},
                    ]}
                    onPress={() => {
                      selectAccount.current.open();
                    }}>
                    <Text style={styles.brokerConnectionbtntext}>
                      Add Broker Connection
                    </Text>
                  </TouchableOpacity>
                )}

                <TouchableOpacity
                  style={[styles.saveButton, {width: '37.5%', borderRadius: 4}]}
                  onPress={() => {
                    responseData &&
                    brokerSelected == 1 &&
                    responseData.expired_token_at < 1
                      ? handleAmeritradeRefreshTokenStatus()
                      : submitBrokerConnection();
                  }}>
                  <Text style={styles.brokerConnectionbtntext}>
                    {responseData &&
                    brokerSelected == 1 &&
                    responseData.expired_token_at < 1
                      ? 'Re-Sync'
                      : 'Submit'}
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          )}

          {/* <View style={[styles.mainsec, {marginTop: 16}]}>
            <Text style={styles.headingTxt}>
              Connect With Financial Institutions:
            </Text>
            <TouchableOpacity style={[styles.ConnectedPlaid, {marginTop: 15}]}>
              <Text style={styles.btntext}>Connect With Plaid</Text>
            </TouchableOpacity>
          </View> */}

          <View style={[styles.mainsec, {marginTop: 16}]}>
            <Text style={styles.headingTxt}>Trade Blotter:</Text>
            <TouchableOpacity
              onPress={() => setSelectOne(true)}
              style={[styles.sendEmail, {marginTop: 15}]}>
              <Text style={styles.btntext}>Sent Trade Blotter</Text>
            </TouchableOpacity>
          </View>

          <View style={{paddingVertical: 16}} />
        </ScrollView>
      ) : (
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
          <Loading color="white" />
        </View>
      )}

      <RBSheet
        ref={selectAccount}
        closeOnDragDown={false}
        closeOnPressMask={false}
        closeOnPressBack={true}
        height={220}
        openDuration={250}
        customStyles={{
          container: {
            backgroundColor: theme.themeColor,
            borderTopLeftRadius: 16,
            borderTopRightRadius: 16,
          },
          wrapper: {backgroundColor: 'grey', opacity: 0.9},
          draggableIcon: {
            display: 'none',
          },
        }}>
        <View style={{paddingHorizontal: 16, paddingVertical: 16}}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <View style={{flex: 0.9, justifyContent: 'center'}}>
              <Text style={styles.headingTxt}>Select Account</Text>
            </View>

            <View
              style={{
                flex: 0.1,
                justifyContent: 'center',
                alignItems: 'flex-end',
              }}>
              <TouchableOpacity
                onPress={() => {
                  selectAccount.current.close();
                }}>
                <Ionicons name="close" color="white" size={18} />
              </TouchableOpacity>
            </View>
          </View>

          <View style={{marginVertical: 16}}>
            <Text style={styles.labelTxt}>
              Which account would you like to choose?
            </Text>

            <SelectDropdown
              buttonStyle={styles.dropdownWrapper}
              data={connectionOptions ?? []}
              defaultButtonText="Borker Account"
              dropdownStyle={{width: '80%', marginLeft: 8, alignSelf: 'center'}}
              onSelect={(selectedItem) => {
                setSelectedAccountTypeError(selectedItem ? false : true);

                setSelectedAccountType(selectedItem);
              }}
              renderCustomizedButtonChild={(selectedItem) => {
                return (
                  <View style={styles.dropdownItemContainer}>
                    <View style={styles.dropdownLabelContainer}>
                      <Text style={styles.dropdownLabelText}>
                        {selectedItem?.label ?? 'Broker Account'}
                      </Text>
                    </View>
                    <View style={styles.dropdownIconContainer}>
                      <FontAwesomeIcons
                        name="caret-down"
                        size={16}
                        color={'white'}
                      />
                    </View>
                  </View>
                );
              }}
              rowStyle={{borderBottomWidth: 0}}
              rowTextForSelection={(item) => item?.label}
              rowTextStyle={styles.dropdownItemText}
              selectedRowTextStyle={styles.dropdownSelectedItemText}
              defaultValue={selectedAccountType}
            />

            {selectedAccountTypeError && (
              <Text style={styles.errorMsg}>
                Please! Select a broker account
              </Text>
            )}
          </View>

          <TouchableOpacity style={styles.saveButton} onPress={submit}>
            <Text style={styles.btntext}>Submit</Text>
          </TouchableOpacity>
        </View>
      </RBSheet>

      <ConfirmPopUp
        modal={confmodal}
        onBackdropPress={() => setConfModal(false)}
      />

      <SuccessPopUp
        res={res}
        successText={successText}
        modal={modal}
        onBackdropPress={() => setModal(false)}
      />

      <Modal
        isVisible={tdAmeritradeLoginModal}
        onBackdropPress={() => {
          setTdAmeritradeLoginModal(false);
        }}>
        <View style={styles.tdModalContainer}>
          <View style={styles.tdModalHeaderContainer}>
            <Text style={styles.headingTxt}>TD Ameritrade Login</Text>

            <TouchableOpacity
              onPress={() => {
                setTdAmeritradeLoginModal(false);
              }}>
              <Icon name="close" size={18} color="white" />
            </TouchableOpacity>
          </View>

          <WebView
            source={{uri}}
            ref={webView}
            startInLoadingState={true}
            renderLoading={() => {
              return displayActivityIndicator();
            }}
            cacheEnabled={true}
            style={{borderBottomLeftRadius: 8, borderBottomRightRadius: 8}}
            onNavigationStateChange={async (state) => {
              if (state.url !== uri) {
                uri = state.url;

                if (uri.includes('https://echoapp.biz/?code=')) {
                  Snackbar.show({
                    duration: Snackbar.LENGTH_INDEFINITE,
                    text: `Please wait! Your TD Ameritrade account is syncing!`,
                  });

                  const authorizationCode = decodeURIComponent(
                    uri.split('code=')[1],
                  );

                  const response = await ameritrade.generateAuthorizationCode(
                    authorizationCode,
                  );

                  if (response) {
                    await setAmeritradTokenResponse(response);

                    ameritradeUserAccounts(response);

                    await setBrokerSelected(helperBrokerConnectionID);
                  } else {
                    Snackbar.show({
                      duration: Snackbar.LENGTH_LONG,
                      text: 'Failed to get authorization code',
                      backgroundColor: 'red',
                    });
                  }
                }
              }
            }}
          />
        </View>
      </Modal>

      <Modal
        isVisible={ameritradeAccountModal}
        onBackdropPress={() => {
          setAmeritradeAccountModal(false);
        }}>
        <View style={{backgroundColor: theme.white, borderRadius: 8}}>
          <View style={styles.tdModalHeaderContainer}>
            <Text style={styles.headingTxt}>TD Ameritrade Accounts</Text>

            <TouchableOpacity
              onPress={() => {
                if (Boolean(ameritradeSelectedAccountId))
                  setAmeritradeAccountModal(false);
                else setAmeritradeSelectedAccountIdError(true);
              }}>
              <Icon name="close" size={18} color="white" />
            </TouchableOpacity>
          </View>

          <View
            style={{
              backgroundColor: theme.white,
              borderBottomLeftRadius: 8,
              borderBottomRightRadius: 8,
              paddingHorizontal: 8,
              paddingVertical: 8,
            }}>
            {tdAmeritradeUserAccounts.length > 1 &&
              tdAmeritradeUserAccounts.map((lol, index) => (
                <View
                  key={index}
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}>
                  <TouchableOpacity
                    style={{
                      flex: 0.1,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}
                    onPress={async () => {
                      Snackbar.show({
                        duration: Snackbar.LENGTH_INDEFINITE,
                        text: `Please wait! Your TD Ameritrade account is syncing!`,
                      });

                      setAmeritradeSelectedAccountIdError(false);

                      setAmeritradeSelectedAccountId(
                        lol.securitiesAccount.accountId,
                      );

                      setEncryptAmeritradeSelectedAccountId(
                        lol.securitiesAccount.accountId,
                      );

                      let formData = new FormData();

                      formData.append('broker_connection', '1');
                      formData.append(
                        'refresh_token',
                        ameritradeTokenResponse.refresh_token,
                      );
                      formData.append(
                        'expires_in',
                        ameritradeTokenResponse.refresh_token_expires_in,
                      );
                      formData.append(
                        'account_id',
                        lol.securitiesAccount.accountId,
                      );

                      await AsyncStorage.setItem(
                        'brokerConnection',
                        JSON.stringify('1'),
                      );

                      let brokerUpdateResponse = await profile.updateBroker(
                        token,
                        formData,
                      );

                      Snackbar.dismiss();

                      if (
                        brokerUpdateResponse &&
                        brokerUpdateResponse.keyword == 'success'
                      ) {
                        setAmeritradeAccountModal(false);

                        setTdAmeritradeLoginModal(false);

                        setTDConfigStatus(true);

                        getBankDetail(1);
                      }
                    }}>
                    <View style={styles.outerradius}>
                      {ameritradeSelectedAccountId ==
                        lol.securitiesAccount.accountId && (
                        <View style={styles.innerradius} />
                      )}
                    </View>
                  </TouchableOpacity>

                  <TouchableOpacity
                    style={{
                      flex: 0.45,
                      justifyContent: 'center',
                      alignItems: 'flex-start',
                      paddingHorizontal: 4,
                      paddingVertical: 4,
                    }}
                    onPress={async () => {
                      Snackbar.show({
                        duration: Snackbar.LENGTH_INDEFINITE,
                        text: `Please wait! Your TD Ameritrade account is syncing!`,
                      });

                      setAmeritradeSelectedAccountIdError(false);

                      setAmeritradeSelectedAccountId(
                        lol.securitiesAccount.accountId,
                      );

                      setEncryptAmeritradeSelectedAccountId(
                        lol.securitiesAccount.accountId,
                      );

                      let formData = new FormData();

                      formData.append('broker_connection', '1');
                      formData.append(
                        'refresh_token',
                        ameritradeTokenResponse.refresh_token,
                      );
                      formData.append(
                        'expires_in',
                        ameritradeTokenResponse.refresh_token_expires_in,
                      );
                      formData.append(
                        'account_id',
                        lol.securitiesAccount.accountId,
                      );

                      await AsyncStorage.setItem(
                        'brokerConnection',
                        JSON.stringify('1'),
                      );

                      let brokerUpdateResponse = await profile.updateBroker(
                        token,
                        formData,
                      );

                      Snackbar.dismiss();

                      if (
                        brokerUpdateResponse &&
                        brokerUpdateResponse.keyword == 'success'
                      ) {
                        setAmeritradeAccountModal(false);

                        setTdAmeritradeLoginModal(false);

                        setTDConfigStatus(true);

                        getBankDetail(1);
                      }
                    }}>
                    <Text
                      style={[styles.accountNoTxt, {color: theme.textColor}]}>
                      {lol.securitiesAccount.accountId}
                    </Text>
                  </TouchableOpacity>

                  <View
                    style={{
                      flex: 0.45,
                      justifyContent: 'center',
                      alignItems: 'flex-end',
                    }}>
                    <Text style={styles.balanceTxt}>
                      $ {lol.securitiesAccount.projectedBalances.availableFunds}
                    </Text>
                  </View>
                </View>
              ))}

            {!ameritradeSelectedAccountIdError && (
              <Text
                style={[
                  styles.errorMsg,
                  {marginHorizontal: 8, marginVertical: 8},
                ]}>
                Please choose any one of the accounts!
              </Text>
            )}
          </View>
        </View>
      </Modal>

      {ameritradeSelectedAccountId && (
        <AmeritradeRefreshToken
          ameritradeSelectedAccountId={ameritradeSelectedAccountId}
          modalStatus={ameritradeRefreshTokenStatus}
          close={handleAmeritradeRefreshTokenStatus}
          getBankDetail={getBankDetail}
        />
      )}

      <Modal isVisible={selectOne}>
        <View style={{backgroundColor: theme.primaryColor, borderRadius: 8}}>
          <View
            style={{
              backgroundColor: theme.secondryColor,
              borderBottomColor: 'grey',
              borderBottomWidth: 1,
              borderTopLeftRadius: 8,
              borderTopRightRadius: 8,
              paddingVertical: 8,
            }}>
            <Text
              style={{
                fontSize: Platform.OS == 'ios' ? 14 : 16,
                color: 'white',
                fontWeight: 'bold',
                textAlign: 'center',
              }}></Text>
          </View>

          <View style={{marginHorizontal: 12, marginVertical: 12}}>
            <TouchableOpacity
              onPress={() => {
                setTrade(true);
                setPlaidConnect(false);
                setnoanyOne(false);
              }}
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginVertical: 8,
              }}>
              <View
                style={{
                  flex: 0.0875,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View
                  style={{
                    width: 20,
                    height: 20,
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderColor: theme.secondryColor,
                    borderWidth: 1,
                    borderRadius: 20 / 2,
                  }}>
                  {trade && (
                    <View
                      style={{
                        width: 10,
                        height: 10,
                        backgroundColor: theme.secondryColor,
                        borderRadius: 10 / 2,
                      }}
                    />
                  )}
                </View>
              </View>

              <View
                style={{
                  flex: 0.8875,
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    fontSize: Platform.OS == 'ios' ? 12 : 14,
                    color: 'white',
                    fontWeight: '600',
                  }}>
                  Trade blotter email
                </Text>
              </View>
            </TouchableOpacity>

            <RBSheet
              ref={sentGmail}
              closeOnDragDown={false}
              closeOnPressMask={false}
              closeOnPressBack={true}
              height={195}
              openDuration={250}
              customStyles={{
                container: {
                  backgroundColor: theme.themeColor,
                  borderTopLeftRadius: 16,
                  borderTopRightRadius: 16,
                },
                wrapper: {backgroundColor: 'grey', opacity: 0.9},
                draggableIcon: {
                  display: 'none',
                },
              }}>
              <KeyboardAvoidingView
                behavior={Platform.OS === 'ios' ? 'padding' : null}
                keyboardVerticalOffset={Platform.OS === 'ios' ? 40 : 0}
                style={{paddingHorizontal: 16, paddingVertical: 16}}>
                <ScrollView>
                  <View>
                    <View style={{flexDirection: 'row'}}>
                      <View style={{flex: 1, justifyContent: 'center'}}>
                        <Text style={styles.Title}>
                          Trade Blotter Email Upload :
                        </Text>
                      </View>

                      <TouchableOpacity
                        onPress={() => {
                          handleDocumentSelection();
                        }}
                        style={styles.uploadbtn}>
                        {uploadingtLoadingStatus ? (
                          <ActivityIndicator size="small" color={theme.white} />
                        ) : (
                          <Icon name="upload" color={'white'} size={20} />
                        )}
                      </TouchableOpacity>
                    </View>

                    <View
                      style={{
                        flexDirection: 'row',
                        marginTop: 7,
                        alignItems: 'center',
                      }}>
                      <View style={{paddingVertical: 0.7, marginLeft: 8}}>
                        {selectName.map((file, index) =>
                          Platform.OS === 'android' ? (
                            <Text
                              key={index.toString()}
                              style={{color: 'white', fontSize: 13}}
                              numberOfLines={1}
                              ellipsizeMode={'tail'}>
                              {file.slice(0, 40)}
                            </Text>
                          ) : (
                            <Text
                              key={index.toString()}
                              style={{color: 'white', fontSize: 14.2}}
                              numberOfLines={1}
                              ellipsizeMode={'tail'}>
                              {file.slice(0, 40)}
                            </Text>
                          ),
                        )}
                      </View>

                      <View>
                        {fileUploadResponse.map((file, index) => (
                          <TouchableOpacity
                            key={index}
                            style={{
                              //borderTopRightRadius: 4,
                              // paddingHorizontal: 1,
                              paddingVertical: 0.7,
                              marginLeft: 8,
                              marginTop: 6,
                            }}
                            onPress={() => {
                              removeName(index), removeFileName(index);
                            }}>
                            <Icon name={'times'} size={10} color={'red'} />
                          </TouchableOpacity>
                        ))}
                      </View>
                    </View>

                    {uploadingtLoadingStatuss && (
                      <ActivityIndicator size="small" color={theme.white} />
                    )}

                    <View
                      style={{
                        flexDirection: 'row',
                        width: '100%',
                        justifyContent: 'space-between',
                      }}>
                      <TouchableOpacity
                        style={styles.sentbtn}
                        onPress={uploadDataFile}>
                        {/* {uploadingtLoadingStatuss ? (
                          <ActivityIndicator size="small" color={theme.white} />
                        ) : ( */}
                        <Text style={styles.senttxt}>Sent Email</Text>
                        {/* )} */}
                      </TouchableOpacity>
                      <TouchableOpacity
                        onPress={CancelBtn}
                        style={styles.cancelbtn}>
                        <Text style={styles.senttxt}>Cancel</Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </ScrollView>
              </KeyboardAvoidingView>
            </RBSheet>

            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
                marginVertical: 4,
              }}>
              <TouchableOpacity onPress={Confirm} style={styles.btnview1}>
                <Text style={styles.btntext}>Confirm</Text>
              </TouchableOpacity>

              <TouchableOpacity onPress={cancelpopup}>
                <Text
                  style={{
                    fontSize: Platform.OS == 'ios' ? 14 : 16,
                    color: theme.secondryColor,
                    fontWeight: 'bold',
                    textAlign: 'center',
                    marginHorizontal: 16,
                  }}>
                  Cancel
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    </View>
  );
};

export default BankAccount;
