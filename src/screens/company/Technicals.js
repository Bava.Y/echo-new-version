import {useNavigation} from '@react-navigation/native';
import React, {useContext, useState} from 'react';
import {Dimensions, Platform, Text, TouchableOpacity, View} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {
  VictoryArea,
  VictoryAxis,
  VictoryBar,
  VictoryChart,
  VictoryGroup,
  VictoryLegend,
  VictoryLine,
} from 'victory-native';
import BottomNav from '../../components/BottomNav';
import HeaderNav from '../../components/HeaderNav';
import {UserDataContext} from '../../context/UserDataContext';
import theme from '../../utils/theme';
import Ticker from '../Dashboard/ticker';
import styles from './style';

const Technicals = ({route}) => {
  const companyName = route.params.companyName || null;
  const symbol = route.params.symbol || null;

  let navigation = useNavigation();

  const [Areashow, setAreaShow] = useState(true);
  const [Lineshow, setLineShow] = useState();
  const [Barshow, setBarShow] = useState();

  const LIGHT_GREY = 'hsl(355, 20%, 90%)';
  const deviceWidth = Dimensions.get('window').width;

  const {tickerData} = useContext(UserDataContext);

  return (
    <>
      <HeaderNav title={companyName} isBack="true" routeName="Company" />

      <View style={styles.container}>
        <Ticker data={tickerData} />

        <View style={styles.chartdetail}>
          <Text
            style={{
              fontSize: Platform.OS == 'ios' ? 16 : 18,
              color: theme.white,
              fontWeight: '600',
            }}>
            Technicals
          </Text>

          {/* Line Chart */}
          {Lineshow ? (
            <View style={{marginVertical: 8}}>
              <VictoryChart
                width={deviceWidth * 0.875}
                height={200}
                padding={{top: 20, bottom: 0, left: 0, right: 40}}>
                <VictoryAxis
                  orientation="right"
                  dependentAxis
                  style={{
                    axis: {
                      stroke: 'transparent',
                    },
                    tickLabels: {
                      fill: LIGHT_GREY,
                      fontSize: 10,
                    },
                    axisLabel: {
                      fill: 'red',
                      fontSize: 10,
                      fontStyle: 'italic',
                    },
                    grid: {
                      fill: LIGHT_GREY,
                      stroke: LIGHT_GREY,
                      strokeWidth: 0.1,
                    },
                  }}
                />

                <VictoryLine
                  style={{
                    data: {stroke: theme.secondryColor},
                  }}
                  data={[
                    {x: 1, y: Math.random() * (1 - 0) + 0},
                    {x: 2, y: Math.random() * (1 - 0) + 0},
                    {x: 3, y: Math.random() * (1 - 0) + 0},
                    {x: 4, y: Math.random() * (1 - 0) + 0},
                    {x: 5, y: Math.random() * (1 - 0) + 0},
                  ]}
                />
              </VictoryChart>
            </View>
          ) : (
            <></>
          )}

          {/* Area chart */}
          {Barshow ? (
            <View style={{marginVertical: 8}}>
              <VictoryChart
                width={deviceWidth * 0.875}
                height={200}
                padding={{top: 40, bottom: 0, left: 0, right: 40}}>
                <VictoryAxis
                  orientation="right"
                  dependentAxis
                  style={{
                    axis: {
                      stroke: 'transparent',
                    },
                    tickLabels: {
                      fill: LIGHT_GREY,
                      fontSize: 10,
                    },
                    axisLabel: {
                      fill: 'red',
                      fontSize: 10,
                      fontStyle: 'italic',
                    },
                    grid: {
                      fill: LIGHT_GREY,
                      stroke: LIGHT_GREY,
                      strokeWidth: 0.1,
                    },
                  }}
                />

                <VictoryGroup
                  offset={16}
                  colorScale={['#08d0df', '#0f676d']}
                  domainPadding={{x: [8, 8], y: 8}}>
                  <VictoryBar
                    data={[
                      {x: 1, y: Math.random() * (1 - 0) + 0},
                      {x: 2, y: Math.random() * (1 - 0) + 0},
                      {x: 3, y: Math.random() * (1 - 0) + 0},
                      {x: 4, y: Math.random() * (1 - 0) + 0},
                      {x: 5, y: Math.random() * (1 - 0) + 0},
                    ]}
                  />

                  <VictoryBar
                    data={[
                      {x: 1, y: Math.random() * (1 - 0) + 0},
                      {x: 2, y: Math.random() * (1 - 0) + 0},
                      {x: 3, y: Math.random() * (1 - 0) + 0},
                      {x: 4, y: Math.random() * (1 - 0) + 0},
                      {x: 5, y: Math.random() * (1 - 0) + 0},
                    ]}
                  />
                </VictoryGroup>

                <VictoryLegend
                  orientation="horizontal"
                  gutter={16}
                  data={[
                    {
                      name: 'Revenue',
                      symbol: {fill: '#08d0df'},
                      labels: {fill: 'white'},
                    },
                    {
                      name: 'Gross Profit',
                      symbol: {fill: '#0f676d'},
                      labels: {fill: 'white'},
                    },
                  ]}
                />
              </VictoryChart>
            </View>
          ) : (
            <></>
          )}

          {Areashow ? (
            <View style={{marginVertical: 8}}>
              <VictoryChart
                width={deviceWidth * 0.875}
                height={200}
                padding={{top: 20, bottom: 0, left: 0, right: 40}}>
                <VictoryAxis
                  orientation="right"
                  dependentAxis
                  style={{
                    axis: {
                      stroke: 'transparent',
                    },
                    tickLabels: {
                      fill: LIGHT_GREY,
                      fontSize: 10,
                    },
                    axisLabel: {
                      fill: 'red',
                      fontSize: 10,
                      fontStyle: 'italic',
                    },
                    grid: {
                      fill: LIGHT_GREY,
                      stroke: LIGHT_GREY,
                      strokeWidth: 0.1,
                    },
                  }}
                />

                <VictoryArea
                  interpolation="natural"
                  style={{
                    data: {
                      fill: theme.secondryColor,
                      fillOpacity: 0.5,
                      stroke: theme.secondryColor,
                      strokeWidth: 2,
                    },
                  }}
                  data={[
                    {x: 1, y: Math.random() * (1 - 0) + 0},
                    {x: 2, y: Math.random() * (1 - 0) + 0},
                    {x: 3, y: Math.random() * (1 - 0) + 0},
                    {x: 4, y: Math.random() * (1 - 0) + 0},
                    {x: 5, y: Math.random() * (1 - 0) + 0},
                  ]}
                />
              </VictoryChart>
            </View>
          ) : (
            <></>
          )}

          <View style={[styles.selectdata, {marginVertical: 16}]}>
            <TouchableOpacity
              style={{width: '33%'}}
              onPress={() => {
                setAreaShow(true);
                setBarShow(false);
                setLineShow(false);
              }}>
              <View
                style={Areashow ? styles.durationactive1 : styles.duration1}>
                <Text style={styles.durationtxt}>Area Chart</Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              style={{width: '33%'}}
              onPress={() => {
                setBarShow(true);
                setLineShow(false);
                setAreaShow(false);
              }}>
              <View style={Barshow ? styles.durationactive1 : styles.duration1}>
                <Text style={styles.durationtxt}>Bar Chart</Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              style={{width: '33%'}}
              onPress={() => {
                setLineShow(true);
                setBarShow(false);
                setAreaShow(false);
              }}>
              <View
                style={Lineshow ? styles.durationactive1 : styles.duration1}>
                <Text style={styles.durationtxt}>Line Chart</Text>
              </View>
            </TouchableOpacity>
          </View>

          {symbol && (
            <TouchableOpacity
              onPress={() => {
                navigation.navigate('WebView', {symbol: symbol});
              }}
              style={{
                flexDirection: 'row',
                alignSelf: 'center',
                backgroundColor: theme.secondryColor,
                borderRadius: 50,
                marginVertical: 8,
                paddingHorizontal: 24,
                paddingVertical: 8,
              }}>
              <Icon name={'bar-chart-o'} color="white" size={16} />
              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 14 : 16,
                  color: 'white',
                  fontWeight: '400',
                  paddingHorizontal: 8,
                }}>
                Trading View
              </Text>
            </TouchableOpacity>
          )}
        </View>
      </View>

      <BottomNav routeName="Dashboard" />
    </>
  );
};

export default Technicals;
