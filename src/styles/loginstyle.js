import {Platform} from 'react-native';
import {Dimensions, StyleSheet} from 'react-native';
import theme from '../utils/theme';
const {height} = Dimensions.get('window');

let style = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    paddingStart: 20,
    paddingEnd: 20,
    backgroundColor: theme.themeColor,
  },
  forgottitle: {
    fontSize: Platform.OS == 'ios' ? 18 : 20,
    color: '#fff',
    marginVertical: 8,
  },
  appLogo: {
    marginTop: height * 0.1,
    height: 100,
    width: 100,
  },
  forgot: {
    fontSize: Platform.OS == 'ios' ? 12 : 14,
    color: '#fff',
    marginTop: 20,
  },
  signinBtn: {
    width: '37.5%',
    backgroundColor: theme.secondryColor,
    borderRadius: 5,
    paddingHorizontal: 15,
    paddingVertical: 10,
    marginTop: 20,
    alignItems: 'center',
  },
  signintext: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    color: '#fff',
  },
  altersignintext: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    color: '#fff',
    marginTop: 6,
    textTransform: 'uppercase',
  },
  socialicon: {
    marginRight: 15,
    height: 25,
    width: 25,
    marginTop: 3,
  },
  errormsg: {
    alignSelf: 'flex-start',
    fontSize: Platform.OS == 'ios' ? 12 : 14,
    color: '#ff0000a1',
    marginHorizontal: 4,
    marginVertical: 10,
  },

  //welcome popup

  welcomecontainer: {
    backgroundColor: theme.primaryColor,
    borderRadius: 10,
    margin: 10,
    padding: 20,
  },
  welcometxt: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    color: theme.white,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  startbtn: {
    flex: 0.45,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: theme.secondryColor,
    borderRadius: 4,
    paddingHorizontal: 6,
    paddingVertical: 6,
  },
  steptext: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    color: theme.white,
    textAlign: 'center',
    marginVertical: 20,
  },
  starttxt: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    color: theme.white,
  },
});

export default style;
