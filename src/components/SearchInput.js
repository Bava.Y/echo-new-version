import React from 'react';
import {KeyboardAvoidingView, ScrollView, View} from 'react-native';
import MultiSelect from 'react-native-multiple-select';
import theme from '../utils/theme';

const SearchInput = ({searchData, selectedItem, fetchCompany}) => {
  const onFocus = () => {
    this.setState({
      backgroundColor: 'green',
    });
  };
  
  return (
    <ScrollView keyboardShouldPersistTaps="handled">
      <KeyboardAvoidingView enabled behavior="position">
        <View style={{borderRadius: 10, marginTop: 20}}>
          <MultiSelect
            hideTags
            items={searchData}
            uniqueKey="id"
            onSelectedItemsChange={(itemValue) => {
              fetchCompany(itemValue);
            }}
            onFocus={() => onFocus}
            selectedItems={selectedItem}
            single={true}
            selectText="Search"
            searchInputPlaceholderText="Search"
            altFontFamily="ProximaNova-Light"
            tagRemoveIconColor="transparent"
            tagBorderColor="#CCC"
            tagTextColor="#CCC"
            selectedItemTextColor="white"
            selectedItemIconColor="transparent"
            itemTextColor="white"
            styleInputGroup={{
              backgroundColor: theme.themeColor,
              borderRadius: 10,
            }}
            styleSelectorContainer={{
              backgroundColor: theme.themeColor,
              borderRadius: 10,
            }}
            searchInputStyle={{
              backgroundColor: theme.themeColor,
            }}
            styleMainWrapper={{
              backgroundColor: theme.themeColor,
              borderRadius: 10,
              minHeight: 20,
              paddingTop: 8,
            }}
            styleListContainer={{
              backgroundColor: theme.themeColor,
              maxHeight: 150,
            }}
            searchInputStyle={{
              backgroundColor: theme.themeColor,
            }}
            styleItemsContainer={{
              backgroundColor: theme.themeColor,
              borderRadius: 10,
            }}
            styleDropdownMenuSubsection={{
              backgroundColor: 'transparent',
              borderColor: 'transparent',
              paddingLeft: 10,
              height: 30,
              borderRadius: 10,
            }}
            displayKey="name"
            searchInputStyle={{color: 'white'}}
          />
        </View>
      </KeyboardAvoidingView>
    </ScrollView>
  );
};

export default SearchInput;
