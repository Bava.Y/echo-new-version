import React from 'react';
import {ScrollView, Text, View} from 'react-native';
import Switch from './../components/switch';
import styles from './styles';

const Subscription = () => {
  return (
    <View style={styles.container}>
      <ScrollView
        keyboardShouldPersistTaps={'handled'}
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}>
        <View style={styles.mainsec}>
          <View style={styles.switchContainer}>
            <View style={styles.switchLabelContainer}>
              <Text style={styles.headingTxt}>Profile</Text>
            </View>

            <View style={styles.switchToggleContainer}>
              <Switch />
            </View>
          </View>

          <View style={{marginTop: 4}}>
            <View style={styles.switchContainer}>
              <View style={styles.switchLabelContainer}>
                <Text style={styles.labelTxt}>Visual to all users</Text>
              </View>

              <View style={styles.switchToggleContainer}>
                <Switch />
              </View>
            </View>

            <View style={[styles.switchContainer, {marginTop: 4}]}>
              <View style={styles.switchLabelContainer}>
                <Text style={styles.labelTxt}>Not Visual to all users</Text>
              </View>

              <View style={styles.switchToggleContainer}>
                <Switch />
              </View>
            </View>
          </View>

          <View style={{marginTop: 16}}>
            <View style={styles.switchContainer}>
              <View style={styles.switchLabelContainer}>
                <Text style={styles.headingTxt}>Personal</Text>
              </View>

              <View style={styles.switchToggleContainer}>
                <Switch />
              </View>
            </View>

            <View style={{marginTop: 4}}>
              <View style={styles.switchContainer}>
                <View style={styles.switchLabelContainer}>
                  <Text style={styles.labelTxt}>Email Seen</Text>
                </View>

                <View style={styles.switchToggleContainer}>
                  <Switch />
                </View>
              </View>

              <View style={[styles.switchContainer, {marginTop: 4}]}>
                <View style={styles.switchLabelContainer}>
                  <Text style={styles.labelTxt}>Phone Seen</Text>
                </View>

                <View style={styles.switchToggleContainer}>
                  <Switch />
                </View>
              </View>
            </View>
          </View>

          <View style={{marginTop: 16}}>
            <View style={styles.switchContainer}>
              <View style={styles.switchLabelContainer}>
                <Text style={styles.headingTxt}>Portfolio</Text>
              </View>

              <View style={styles.switchToggleContainer}>
                <Switch />
              </View>
            </View>

            <View style={{marginTop: 4}}>
              <View style={styles.switchContainer}>
                <View style={styles.switchLabelContainer}>
                  <Text style={styles.labelTxt}>Returns</Text>
                </View>

                <View style={styles.switchToggleContainer}>
                  <Switch />
                </View>
              </View>

              <View style={[styles.switchContainer, {marginTop: 4}]}>
                <View style={styles.switchLabelContainer}>
                  <Text style={styles.labelTxt}>Investment Strageties</Text>
                </View>

                <View style={styles.switchToggleContainer}>
                  <Switch />
                </View>
              </View>

              <View style={[styles.switchContainer, {marginTop: 4}]}>
                <View style={styles.switchLabelContainer}>
                  <Text style={styles.labelTxt}>Metrics</Text>
                </View>

                <View style={styles.switchToggleContainer}>
                  <Switch />
                </View>
              </View>
            </View>
          </View>

          <View style={{marginTop: 16}}>
            <View style={styles.switchContainer}>
              <View style={styles.switchLabelContainer}>
                <Text style={styles.headingTxt}>Communication</Text>
              </View>

              <View style={styles.switchToggleContainer}>
                <Switch />
              </View>
            </View>

            <View style={{marginTop: 4}}>
              <View style={styles.switchContainer}>
                <View style={styles.switchLabelContainer}>
                  <Text style={styles.labelTxt}>Inside of app</Text>
                </View>

                <View style={styles.switchToggleContainer}>
                  <Switch />
                </View>
              </View>

              <View style={[styles.switchContainer, {marginTop: 4}]}>
                <View style={styles.switchLabelContainer}>
                  <Text style={styles.labelTxt}>Text Messages</Text>
                </View>

                <View style={styles.switchToggleContainer}>
                  <Switch />
                </View>
              </View>

              <View style={[styles.switchContainer, {marginTop: 4}]}>
                <View style={styles.switchLabelContainer}>
                  <Text style={styles.labelTxt}>Email</Text>
                </View>

                <View style={styles.switchToggleContainer}>
                  <Switch />
                </View>
              </View>
            </View>
          </View>
        </View>

        <View style={{paddingVertical: 16}} />
      </ScrollView>
    </View>
  );
};

export default Subscription;
