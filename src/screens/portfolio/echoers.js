import {useFocusEffect} from '@react-navigation/core';
import moment from 'moment';
import React, {useCallback, useContext, useEffect, useState} from 'react';
import {ScrollView, TouchableOpacity, View, FlatList} from 'react-native';
import Snackbar from 'react-native-snackbar';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {portfolio} from '../../api';
import HeaderNav from '../../components/HeaderNav';
import Loading from '../../components/Loading';
import {UserDataContext} from '../../context/UserDataContext';
import theme from '../../utils/theme';
import PortfolioChart from './chart';
import CompanyList from './companyList';
import Date from './date';
import styles from './style';

const Echoers = ({route}) => {
  // Props Variables
  const {id} = route.params;
  const {userId, setUserId} = useContext(UserDataContext);
  // Echoers Variables
  const [chartOrderData, setChartOrderData] = useState(null);
  const [returnsValue, setReturnsValue] = useState(null);
  const [returnsAmount, setReturnsAmount] = useState(null);
  const [listOrderData, setListOrderData] = useState([]);
  const [lineChartData, setLineChartData] = useState(null);
  const [chartRange, setChartRange] = useState('1d');
  const [rangeFrom, setrangeFrom] = useState(0);
  console.log('listOrderData++++++', listOrderData.length);
  // Context Variables
  const {
    portfolioConnection,
    portfolioTimePeriod,
    token,

    loadMoreData,
    setLoadMoreData,
    setCardLoad,
  } = useContext(UserDataContext);

  // Date Variables
  const [fromDate, setFromDate] = useState(moment());
  const [toDate, setToDate] = useState(moment());

  // Other VAriables
  const [chartLoading, setChartLoading] = useState(false);
  const [listLoading, setListLoading] = useState(false);
  const [filterStatus, setFilterStatus] = useState(false);
  const [currentPage, setCurrentPage] = useState(0);
  const [peopleScr,setPeopleScr] = useState(true)

  useFocusEffect(
    useCallback(() => {
      let isFocus = true;

      setFromDate(moment());
      setToDate(moment());
      setFilterStatus(false);
     // getFromToDateMyPeopleChart();
      getOrdersChart();
      // getPortfolioData();

      return () => {
        isFocus = false;
      };
    }, [portfolioConnection, portfolioTimePeriod]),
  );

  useEffect(() => {
    //getOrdersChart();
    getPortfolioData();
    // getFromToDateMyPeopleChart();
  }, [currentPage,filterStatus]);

  const handleDatePicker = (selectedDate, status) => {
    switch (status) {
      case 'from':
        setFromDate(selectedDate);
        break;

      case 'to':
        setToDate(selectedDate);
        break;
    }
  };

  const getOrdersChart = async () => {
    setChartLoading(true);

    let endpoint = `people/chart/${id}/${portfolioConnection}`;

    if (!!portfolioTimePeriod) {
      endpoint = endpoint + '?time_period=' + chartRange;
    }

    const response = await portfolio.getOrdersChart(endpoint);

    const res = response.data;

    if (res) {
      if (response.keyword == 'success') {
        let orderArr = [];

        if (res.length != 0) {
          for (let i = 0; i < res.chart.length; i++) {
            orderArr.push({
              name: `${res.chart[i].symbol} (${res.chart[i].percentage}%)`,
              x: `${res.chart[i].percentage}%`,
              y: Number(res.chart[i].percentage / 30),
            });
          }

          setChartOrderData(orderArr);
          setReturnsValue(res.portfolio_returns || null);
          setReturnsAmount(res.portfolio_returns_amount || null);
        }
      } else {
        setChartOrderData([]);
        setReturnsValue(null);
        setReturnsAmount(null);
      }
    } else {
      setChartOrderData([]);
      setReturnsValue(null);
      setReturnsAmount(null);
    }

    setChartLoading(false);
  };

  const getOrdersChartTime = async () => {
    setChartLoading(true);

    let endpoint = `people/chart/${id}/${portfolioConnection}?from_date=${moment(
      fromDate,
      'MM-DD-YYYY',
    ).format('YYYY-MM-DD')}&to_date=${moment(toDate, 'MM-DD-YYYY').format(
      'YYYY-MM-DD',
    )}`;
    const response = await portfolio.getOrdersChart(endpoint);
     const res = response.data;

    if (res) {
      if (response.keyword == 'success') {
        let orderArr = [];

        if (res.length != 0) {
          for (let i = 0; i < res.chart.length; i++) {
            orderArr.push({
              name: `${res.chart[i].symbol} (${res.chart[i].percentage}%)`,
              x: `${res.chart[i].percentage}%`,
              y: Number(res.chart[i].percentage / 30),
            });
          }

          setChartOrderData(orderArr);
          setReturnsValue(res.portfolio_returns || null);
          setReturnsAmount(res.portfolio_returns_amount || null);
        }
      } else {
        setChartOrderData([]);
        setReturnsValue(null);
        setReturnsAmount(null);
      }
    } else {
      setChartOrderData([]);
      setReturnsValue(null);
      setReturnsAmount(null);
    }

    setChartLoading(false);
  };

  const getPortfolioData = async () => {
    setListLoading(true);
    setCardLoad(true);
    if (peopleScr == true) {
      const endpoint =  `people/overall/${id}/${portfolioConnection}?limit=4&offset=${currentPage}`;
   
         const res = await portfolio.getOrdersData(endpoint);
         if (res) {
           if (res.keyword == 'success') {
             setListLoading(true);
             if (res?.data?.symbol?.length == 0) {
               setLoadMoreData(false);
             }
   
             await setListOrderData([...listOrderData, ...res.data]);
             setListLoading(false);
             setCurrentPage(currentPage + 1);
             setCardLoad(true);
   
           //  await setListOrderData([...listOrderData]);
   
             //setLoadMoreData(true);
           } else if (res.keyword == 'failed') {
             setCardLoad(false);
   
             //setCurrentPage([...currentPage]);
             //await setListOrderData([...listOrderData]);
             setListLoading(false);
           }
           // else {
           //   setLoadMoreData(false);
           //  await setListOrderData(listOrderData.concat(res.data));
           //   setListLoading(false);
           // }
         } else {
           setCardLoad(false);
           setListOrderData([]);
         }
       } else if (peopleScr == false) {
         const endpoint = `people/overall/${id}/${portfolioConnection}?from_date=${moment(
           fromDate,
           'MM-DD-YYYY',
         ).format('YYYY-MM-DD')}&to_date=${moment(toDate, 'MM-DD-YYYY').format(
           'YYYY-MM-DD',
         )}&limit=4&offset=${currentPage}`;
   
         const res = await portfolio.getOrdersData(endpoint);
         if (res) {
           if (res.keyword == 'success') {
             setListLoading(true);
             if (res?.data?.symbol?.length == 0) {
               setLoadMoreData(false);
             }
   
             await setListOrderData([...listOrderData, ...res.data]);
             setListLoading(false);
             setCurrentPage(currentPage + 1);
             setCardLoad(true);
   
             await setListOrderData([...listOrderData]);
   
             //setLoadMoreData(true);
           } else if (res.keyword == 'failed') {
             setCardLoad(false);
   
             setCurrentPage([...currentPage]);
             await setListOrderData([...listOrderData]);
             setListLoading(false);
           }
           // else {
           //   setLoadMoreData(false);
           //  await setListOrderData(listOrderData.concat(res.data));
           //   setListLoading(false);
           // }
         } else {
           setCardLoad(false);
           setListOrderData([]);
         }
       }
   
    setListLoading(false);
  };

  useFocusEffect(
    useCallback(() => {
      let isFocus = true;

      fetchLineChartData();

      return () => {
        isFocus = false;
      };
    }, [chartRange]),
  );

  const fetchLineChartData = () => {
    switch (chartRange) {
      case '1d':
      default:
        getTotalEchoerDayChart();
        break;

      case '1w':
        getTotalEchoerWeekChart();
        break;

      case '1m':
      case '3m':
      case '6m':
      case '1y':
      case 'max':
        getTotalEchoerMonthChart();
        break;
    }
  };

  const getTotalEchoerDayChart = async () => {
    const res = await portfolio.getTotalEchoerDayChart(id);

    if (res) {
      if (res.keyword == 'success') {
        if (res?.data) {
          // setLineChartData(res.data);

          const lineData = [];
          res.data?.map(item => {
            const valueData = {};
            (valueData['timestamp'] = item?.timestamp),
              (valueData['value'] = Number(item?.value)),
              lineData.push(valueData);
            setLineChartData(lineData);
          });
        } else {
          setLineChartData([]);
        }
      } else {
        setLineChartData([]);
        Snackbar.show({
          duration: Snackbar.LENGTH_SHORT,
          text: res.message,
          backgroundColor: 'red',
        });
      }
    } else {
    }
  };

  const getTotalEchoerWeekChart = async () => {
    const res = await portfolio.getTotalEchoerWeekChart(id);

    if (res) {
      if (res.keyword == 'success') {
        if (res?.data) {
          // setLineChartData(res.data);

          const lineData = [];
          res.data?.map(item => {
            const valueData = {};
            (valueData['timestamp'] = item?.timestamp),
              (valueData['value'] = Number(item?.value)),
              lineData.push(valueData);
            setLineChartData(lineData);
          });
        } else {
          setLineChartData([]);
        }
      } else {
        setLineChartData([]);
        Snackbar.show({
          duration: Snackbar.LENGTH_SHORT,
          text: res.message,
          backgroundColor: 'red',
        });
      }
    } else {
    }
  };

  const getFromToDateMyPeopleChart = async () => {
    setChartLoading(true);

    let endpoint = `totalPortfolioMonthChartPeople/${rangeFrom}/${id}?from_date=${moment(
      fromDate,
      'MM-DD-YYYY',
    ).format('YYYY-MM-DD')}&to_date=${moment(toDate, 'MM-DD-YYYY').format(
      'YYYY-MM-DD',
    )}`;

    const res = await portfolio.getFromToDateMyPeopleChart(
      id,
      endpoint,
      rangeFrom,
    );
    if (res) {
      if (res.keyword == 'success') {
        if (res?.data) {
          setLineChartData(res.data);
        } else {
          setLineChartData([]);
        }
      } else {
        setLineChartData([]);
        Snackbar.show({
          duration: Snackbar.LENGTH_SHORT,
          text: res.message,
          backgroundColor: 'red',
        });
      }
    } else {
    }
  };

  const getTotalEchoerMonthChart = async () => {
    const res = await portfolio.getTotalEchoerMonthChart(id, chartRange);

    if (res) {
      if (res.keyword == 'success') {
        if (res?.data) {
          // setLineChartData(res.data);

          const lineData = [];
          res.data?.map(item => {
            const valueData = {};
            (valueData['timestamp'] = item?.timestamp),
              (valueData['value'] = Number(item?.value)),
              lineData.push(valueData);
            setLineChartData(lineData);
          });
        } else {
          setLineChartData([]);
        }
      } else {
        setLineChartData([]);
        Snackbar.show({
          duration: Snackbar.LENGTH_SHORT,
          text: res.message,
          backgroundColor: 'red',
        });
      }
    } else {
    }
  };

  return (
    <>
      <HeaderNav title="Portfolio" isBack="true" routeName="Portfolio" />
      <View style={styles.container}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-evenly',
            alignItems: 'center',
            backgroundColor: '#2e2e2e52',
            padding: 8,
          }}>
          <Date
            fromDate={fromDate}
            toDate={toDate}
            handleDatePicker={handleDatePicker}
          />

          <View
            style={{
              flex: 0.1,
              justifyContent: 'center',
              alignItems: 'center',
              borderColor: 'transparent',
            }}>
            {filterStatus ? (
              <TouchableOpacity
                onPress={() => {
                  setFilterStatus(!filterStatus);
                  handleDatePicker(moment().format('MM-DD-YYYY'), 'from');
                  handleDatePicker(moment().format('MM-DD-YYYY'), 'to');
                }}>
                <Ionicons name="close" size={18} color={theme.white}  onPress={() => {
                    setPeopleScr(true),
                    getOrdersChart()
                  }} />
              </TouchableOpacity>
            ) : (
              <TouchableOpacity
              onPress={() => {
                setFilterStatus(!filterStatus), setPeopleScr(false),getOrdersChartTime()
              }}>
                <Ionicons name="search-outline" size={18} color={theme.white} />
              </TouchableOpacity>
            )}
          </View>
        </View>

        <ScrollView
          keyboardShouldPersistTaps="handled"
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}>
          {!Boolean(chartOrderData) && !Boolean(lineChartData) ? (
            <PortfolioChart loading={chartLoading} />
          ) : (
            <PortfolioChart
              loading={chartLoading}
              from={'Echoer'}
              chartRange={chartRange}
              lineChartData={lineChartData}
              returnsValue={returnsValue}
              returnsAmount={returnsAmount}
              orderData={chartOrderData}
              onRangeChange={(range) => {
                setChartRange(range);

                setLineChartData(null);
              }}
            />
          )}

          {/* <View style={{paddingVertical: 12}} /> */}

          <View style={{height: 420}}>
            {!Boolean(listOrderData) ? (
              <CompanyList loading={listLoading} />
            ) : (
              <CompanyList
                loading={listLoading}
                orderList={listOrderData}
                //Path={null}
              />
            )}
          </View>
          <View style={{paddingVertical: 12}} />
        </ScrollView>
      </View>
    </>
  );
};

export default Echoers;
