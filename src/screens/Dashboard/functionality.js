import React from 'react';
import {FlatList, Platform, Text, TouchableOpacity, View} from 'react-native';
import RBSheet from 'react-native-raw-bottom-sheet';
import Ionicons from 'react-native-vector-icons/Ionicons';
import theme from '../../utils/theme';

const Benefit = [
  {title: 'Echo Portfolio'},
  {title: 'Total Portfolio'},
  {title: 'Echoing Portfolio'},
  {title: 'S&P 500'},
  {title: 'Dow Jones'},
  {title: 'Nasdaq'},
  {title: 'Leaderboard'},
];

const Functionality = ({chartlist, onChange}) => {
  const renderItem = ({item}) => (
    <View style={{marginVertical: 8}}>
      <Item title={item.title} />
    </View>
  );

  const Item = ({title}) => (
    <Text
      style={{
        fontSize: Platform.OS == 'ios' ? 14 : 16,
        color: theme.white,
      }}>
      {title}
    </Text>
  );

  return (
    <RBSheet
      ref={chartlist}
      closeOnDragDown={true}
      closeOnPressMask={false}
      closeOnPressBack={true}
      height={320}
      openDuration={250}
      customStyles={{
        container: {
          backgroundColor: theme.themeColor,
          borderTopLeftRadius: 16,
          borderTopRightRadius: 16,
        },
        wrapper: {
          backgroundColor: 'grey',
          opacity: 0.9,
        },
        draggableIcon: {
          display: 'none',
        },
      }}>
      <TouchableOpacity onPress={onChange}>
        <View
          style={{
            alignSelf: 'flex-end',
            paddingHorizontal: 16,
            paddingVertical: 16,
          }}>
          <Ionicons name="close" color="white" size={18} />
        </View>
      </TouchableOpacity>

      <FlatList
        style={{paddingHorizontal: 16}}
        data={Benefit}
        renderItem={renderItem}
      />
    </RBSheet>
  );
};

export default Functionality;
