import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import React, {useContext}  from 'react';
import {View} from 'react-native';
import BottomNav from '../../components/BottomNav';
import HeaderNav from '../../components/HeaderNav';
import theme from '../../utils/theme';
import AllOrders from './allOrders';
import ClosedOrders from './closedOrders';
import OpenOrders from './openOrders';
import styles from './style';
import MyTabBar from './Tab';
import {UserDataContext} from '../../context/UserDataContext';

const order = () => {
  // Order Variables

  const Tab = createMaterialTopTabNavigator();

  return (
    <>
      <HeaderNav
        title="Order History"
        isBack="true"
        routeName="Order History"
      />

      <View style={styles.container}>
        <Tab.Navigator
          style={{backgroundColor: theme.primaryColor}}
          sceneContainerStyle={{backgroundColor: theme.themeColor}}
          tabBar={(props) => <MyTabBar {...props} />}
          swipeEnabled={false}
          initialRouteName={'AllOrders'}
          backBehavior="initialRoute"
          animationEnabled={true}
          tabBarOptions={{scrollEnabled: true}}>
          <Tab.Screen name="All" component={AllOrders}  />

          <Tab.Screen name="Open" component={OpenOrders} />

          <Tab.Screen name="Closed" component={ClosedOrders} />
        </Tab.Navigator>
      </View>

      <BottomNav routeName="Portfolio" />
    </>
  );
};

export default order;
