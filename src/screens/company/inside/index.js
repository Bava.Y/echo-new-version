import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import {isEmpty} from 'lodash';
import React, {useContext, useEffect, useState} from 'react';
import {ActivityIndicator, View} from 'react-native';
import Snackbar from 'react-native-snackbar';
import {company} from '../../../api';
import BottomNav from '../../../components/BottomNav';
import HeaderNav from '../../../components/HeaderNav';
import {UserDataContext} from '../../../context/UserDataContext';
import theme from '../../../utils/theme';
import Ticker from '../../Dashboard/ticker';
import RoasterScreen from './../InsideRoaster';
import SummaryScreen from './../InsideSummary';
import TransactionScreen from './../Transaction';
import MyTabBar from './InsideBar';
import styles from './style';

const Tab = createMaterialTopTabNavigator();

const CompanyDetail = ({route}) => {
  const {symbol, companyName, initial} = route.params;

  const {tickerData, token} = useContext(UserDataContext);

  const [insides, setInsides] = useState({});

  useEffect(() => {
    let ac = new AbortController();

    getInsides();

    return () => {
      ac.abort();
    };
  }, []);

  const getInsides = async () => {
    const res = await company.getInsides(symbol, token);

    if (res) {
      if (res.keyword == 'success') {
        setInsides(res.data);
      } else {
        Snackbar.show({
          duration: Snackbar.LENGTH_SHORT,
          text: res.message,
          backgroundColor: 'red',
        });
      }
    } else {
    }
  };

  return (
    <>
      <HeaderNav isBack="true" title={companyName} routeName="Company" />

      {!isEmpty(insides) ? (
        <View style={styles.container}>
          <Ticker data={tickerData} />

          <View
            style={{
              flex: 1,
              backgroundColor: '#2e2e2e52',
              paddingHorizontal: 8,
              paddingVertical: 8,
            }}>
            <Tab.Navigator
              sceneContainerStyle={{backgroundColor: theme.primaryColor}}
              tabBar={(props) => <MyTabBar {...props} />}
              swipeEnabled={true} //can change dynamically
              initialRouteName={initial}
              backBehavior="initialRoute"
              animationEnabled={true}
              tabBarOptions={{scrollEnabled: true}}>
              <Tab.Screen
                name="Inside Roaster"
                component={RoasterScreen}
                initialParams={{roast: insides.roster}}
              />

              <Tab.Screen
                name="Inside Summary"
                component={SummaryScreen}
                initialParams={{summary: insides.summary}}
              />

              <Tab.Screen
                name="Inside Transaction"
                component={TransactionScreen}
                initialParams={{transaction: insides.transactions}}
              />
            </Tab.Navigator>
          </View>
        </View>
      ) : (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: theme.primaryColor,
          }}>
          <ActivityIndicator size="large" color="white" />
        </View>
      )}

      <BottomNav routeName="Dashboard" />
    </>
  );
};

export default CompanyDetail;
