import {Platform, StyleSheet} from 'react-native';
import theme from '../../../../utils/theme';

const styles = StyleSheet.create({
  modalView: {
    width: '87.5%',
    backgroundColor: theme.primaryColor,
    borderRadius: 8,
    margin: 16,
    padding: 16,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 4,
  },
  questionTxt: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    color: theme.white,
    fontWeight: 'bold',
    textAlign: 'center',
    lineHeight: 20,
  },
  btnView: {
    flex: 0.375,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: theme.secondryColor,
    borderRadius: 4,
    paddingHorizontal: 6,
    paddingVertical: 6,
  },
  btnTxt: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    color: theme.white,
  },
});
export default styles;
