import {Platform} from 'react-native';
import {StyleSheet} from 'react-native';
import theme from '../utils/theme';

let style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.themeColor,
    paddingHorizontal: 20,
    paddingVertical: 20,
  },
  signuptitle: {
    fontSize: Platform.OS == 'ios' ? 18 : 20,
    color: 'white',
    textAlign: 'center',
  },
  forgot: {
    color: '#fff',
    marginTop: 20,
  },
  signinBtn: {
    backgroundColor: theme.secondryColor,
    borderRadius: 5,
    paddingHorizontal: 15,
    paddingVertical: 10,
    marginTop: 20,
    alignItems: 'center',
  },
  signintext: {
    color: '#fff',
    fontSize: Platform.OS == 'ios' ? 14 : 16,
  },
  cancelBtn: {
    borderRadius: 5,
    color: '#fff',
    borderColor: '#2a587f',
    borderWidth: 1,
    padding: 10,
    marginTop: 20,
    paddingLeft: 20,
    paddingRight: 20,
    alignItems: 'center',
    margin: 2,
    marginStart: 5,
    justifyContent: 'center',
  },
  canceltext: {
    color: 'white',
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    textAlign: 'center',
  },
  select: {
    height: 50,
    width: '100%',
    color: 'white',
  },
  altersignintext: {
    color: '#fff',
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    marginTop: 5,
    marginBottom: 10,
    textTransform: 'uppercase',
  },
  socialicon: {
    marginRight: 15,
    height: 25,
    width: 25,
  },
  checkboxContainer: {
    flexDirection: 'row',
    marginBottom: 20,
  },
  checkbox: {
    alignSelf: 'center',
  },
  label: {
    margin: 8,
  },
  datePickerStyle: {
    width: '100%',
    marginTop: 20,
    color: 'white',
  },
  errormsg: {
    fontSize: Platform.OS == 'ios' ? 12 : 14,
    color: '#ff0000a1',
    marginHorizontal: 4,
    marginVertical: 10,
  },
});

export default style;
