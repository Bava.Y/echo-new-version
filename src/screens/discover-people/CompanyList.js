import {useFocusEffect} from '@react-navigation/native';
import React, {useCallback, useContext, useState} from 'react';
import {Platform} from 'react-native';
import {ScrollView, Text, TouchableOpacity, View} from 'react-native';
import RBSheet from 'react-native-raw-bottom-sheet';
import Icons from 'react-native-vector-icons/FontAwesome';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {UserDataContext} from '../../context/UserDataContext';
import theme from '../../utils/theme';

const CompanyList = (props) => {
  // Props Variables
  let {filterRef, forStatus, onClose} = props;

  // CompanyList Variables
  const [timePeriodSelected, setTimePeriodSelected] = useState(null);
  const [brokerConnectionSelected, setBrokerConnectionSelected] = useState(
    null,
  );

  // Context Varibles
  const {
    orderHistoryBorkerConnection,
    portfolioConnection,
    portfolioTimePeriod,
    setOrderHistoryBorkerConnection,
    setPortfolioConnection,
    setPortfolioTimePeriod,
    setTrackingHistoryConnection,
    trackingHistoryConnection,
  } = useContext(UserDataContext);

  useFocusEffect(
    useCallback(() => {
      let isFocus = true;

      handleLoadStates();

      return () => {
        isFocus = false;
      };
    }, []),
  );

  const handleLoadStates = () => {
    switch (forStatus) {
      case 'Order History':
        setBrokerConnectionSelected(orderHistoryBorkerConnection);
        break;

      case 'Portfolio':
        setTimePeriodSelected(portfolioTimePeriod);
        setBrokerConnectionSelected(portfolioConnection);
        break;

      case 'Tracking History':
        setBrokerConnectionSelected(trackingHistoryConnection);
        break;

      default:
        setTimePeriodSelected('0');
        setBrokerConnectionSelected('all');
        break;
    }
  };

  const renderTimePeriod = () => {
    const timePeroidOptions = [
      {label: 'All', value: '0'},
      {label: 'Last Day', value: '2'},
      {label: 'Last Week', value: '3'},
      {label: 'Last Month', value: '4'},
      {label: 'Last Quarter', value: '5'},
      {label: 'YTD', value: '6'},
      {label: 'Last Year', value: '7'},
      {label: '5 Years', value: '8'},
      {label: '10 Years', value: '9'},
    ];

    return (
      <View style={{paddingHorizontal: 16, paddingVertical: 8}}>
        <Text
          style={{
            fontSize: Platform.OS == 'ios' ? 14 : 16,
            color: 'white',
            fontWeight: 'bold',
            marginVertical: 8,
          }}>
          Time Period
        </Text>
        <View style={{paddingLeft: 24}}>
          {timePeroidOptions.map((timeFrame, index) => (
            <TouchableOpacity
              key={index}
              onPress={() => {
                setTimePeriodSelected(timeFrame.value);
              }}>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                {timePeriodSelected == timeFrame.value && (
                  <Icons
                    name="check"
                    size={16}
                    color="grey"
                    style={{paddingRight: 4}}
                  />
                )}
                <Text
                  style={{
                    fontSize: Platform.OS == 'ios' ? 14 : 16,
                    color: 'white',
                    marginVertical: 6,
                  }}>
                  {timeFrame.label}
                </Text>
              </View>
            </TouchableOpacity>
          ))}
        </View>
      </View>
    );
  };

  const renderBrokerConnection = () => {
    const brokerConnectionOptions = [
      {label: 'All', value: 'all'},
      {label: 'Alpaca', value: '0'},
      {label: 'TD Ameritrade', value: '1'},
    ];

    return (
      <View style={{paddingHorizontal: 16, paddingVertical: 8}}>
        <Text
          style={{
            fontSize: Platform.OS == 'ios' ? 14 : 16,
            color: 'white',
            fontWeight: 'bold',
            marginVertical: 8,
          }}>
          Broker Connection
        </Text>
        <View style={{paddingLeft: 24}}>
          {brokerConnectionOptions.map((brokerData, index) => (
            <TouchableOpacity
              key={index}
              onPress={() => {
                setBrokerConnectionSelected(brokerData.value);
              }}>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                {brokerConnectionSelected == brokerData.value && (
                  <Icons
                    name="check"
                    size={16}
                    color="grey"
                    style={{paddingRight: 4}}
                  />
                )}
                <Text
                  style={{
                    fontSize: Platform.OS == 'ios' ? 14 : 16,
                    color: 'white',
                    marginVertical: 6,
                  }}>
                  {brokerData.label}
                </Text>
              </View>
            </TouchableOpacity>
          ))}
        </View>
      </View>
    );
  };

  const handleBrokerConnectionSelection = (selectedConnection) => {
    switch (forStatus) {
      case 'Order History':
        setOrderHistoryBorkerConnection(selectedConnection);
        break;

      case 'Portfolio':
        setPortfolioConnection(selectedConnection);
        break;

      case 'Tracking History':
        setTrackingHistoryConnection(selectedConnection);
        break;
    }
  };

  return (
    <RBSheet
      ref={filterRef}
      closeOnDragDown={true}
      closeOnPressMask={false}
      closeOnPressBack={true}
      height={forStatus == 'Portfolio' ? 480 : 280}
      openDuration={250}
      customStyles={{
        container: {
          backgroundColor: theme.themeColor,
          borderTopLeftRadius: 16,
          borderTopRightRadius: 16,
        },
        wrapper: {
          backgroundColor: 'grey',
          opacity: 0.9,
        },
        draggableIcon: {
          display: 'none',
        },
      }}>
      <TouchableOpacity
        onPress={() => {
          onClose();
        }}>
        <View
          style={{
            alignSelf: 'flex-end',
            paddingHorizontal: 16,
            paddingVertical: 16,
          }}>
          <Ionicons name="close" color="white" size={18} />
        </View>
      </TouchableOpacity>

      <View style={{flexDirection: 'row', justifyContent: 'center'}}>
        <TouchableOpacity
          onPress={() => {
            setPortfolioTimePeriod(timePeriodSelected);

            handleBrokerConnectionSelection(brokerConnectionSelected);

            onClose();
          }}>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              backgroundColor: theme.secondryColor,
              borderRadius: 16,
              marginHorizontal: 8,
              paddingHorizontal: 16,
              paddingVertical: 8,
            }}>
            <View
              style={{
                width: 20,
                height: 20,
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: 'white',
                borderRadius: 20 / 2,
              }}>
              <Icons name="check" color="black" size={12} />
            </View>
            <Text
              style={{
                fontSize: Platform.OS == 'ios' ? 14 : 16,
                color: 'white',
                paddingHorizontal: 4,
              }}>
              Apply
            </Text>
          </View>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => {
            handleLoadStates();
          }}>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              backgroundColor: 'red',
              borderRadius: 16,
              marginHorizontal: 8,
              paddingHorizontal: 16,
              paddingVertical: 8,
            }}>
            <View
              style={{
                width: 20,
                height: 20,
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: 'white',
                borderRadius: 20 / 2,
              }}>
              <Icons name="close" color="black" size={12} />
            </View>
            <Text
              style={{
                fontSize: Platform.OS == 'ios' ? 14 : 16,
                color: 'white',
                paddingHorizontal: 4,
              }}>
              Clear
            </Text>
          </View>
        </TouchableOpacity>
      </View>

      <ScrollView
        keyboardShouldPersistTaps={'handled'}
        showsVerticalScrollIndicator={false}
        style={{width: '100%', marginVertical: 8}}>
        {forStatus == 'Portfolio' && renderTimePeriod()}
        {renderBrokerConnection()}
      </ScrollView>
    </RBSheet>
  );
};

export default CompanyList;
