import jwt_decode from 'jwt-decode';
import React, {createContext, useEffect, useState} from 'react';
import {auth} from '../api';

export const UserDataContext = createContext();

export const UserDataProvider = (props) => {
  // UserDataProvider Variables
  const [authUserData, setUserData] = useState({});
  const [loginUserData, setLoginUserData] = useState();
  const [passwordotp, setPasswordOTP] = useState();
  const [tickerData, setTickerData] = useState([]);
  const [tickerVisible, setTickerVisible] = useState();
  const [companyDescribe, setCompanyDescribe] = useState({});
  const [dashboardPreference, setDashboardPreference] = useState({});
  const [rating, setRating] = useState({});
  const [peers, setPeers] = useState([]);
  const [ratingChanged, setRatingChanged] = useState('');
  const [token, setToken] = useState('');
  const [primary, setPrimary] = useState([]);
  const [secondary, setSecondary] = useState([]);
  const [secondaryInd, setSecondaryInd] = useState([]);
  const [secondarySect, setSecondarySect] = useState([]);
  const [portfolioConnection, setPortfolioConnection] = useState('0');
  const[portfolioconnection,setportfolioconnection]=useState('')
  const [portfolioTimePeriod, setPortfolioTimePeriod] = useState('1d');
  const [myStockConnection, setmyStockConnection] = useState('all');
  const [myStockTimePeriod, setmyStockTimePeriod] = useState('1d');
  const [alpacaConfigStatus, setAlpacaConfigStatus] = useState(false);
  const [tdConfigStatus, setTDConfigStatus] = useState(false);
  const [result, setResult] = useState(false);
  const [trackingHistoryConnection, setTrackingHistoryConnection] = useState(
    'all',
  );
  const [achRelationshipStatus, setACHRelationshipStatus] = useState(false);
  const [wireRelationshipStatus, setWireRelationshipStatus] = useState(false);
  const [
    orderHistoryBorkerConnection,
    setOrderHistoryBorkerConnection,
  ] = useState('all');
  const [singleChartSymbol, setSingleChartSymbol] = useState('SPY');
  const [fourChartSymbols, setFourChartSymbols] = useState([
    'SPY',
    'SPY',
    'SPY',
    'SPY',
  ]);
  const [
    ameritradeRefreshTokenStatus,
    setAmeritradeRefreshTokenStatus,
  ] = useState(false);
  const [ameritradeAlertStatus, setAmeritradeAlertStatus] = useState(false);
  const [citizenship, setcitizenship] = useState(null);
  const [none, setNone] = useState(false);
  const [cancelbtn, setCancelbtn] = useState(true);
  const [orderId, setOrderId] = useState(null);
  const [userId, setUserId] = useState(null);
  const [ownStock, setOwnStock] = useState(null);
  const [othersStock, setOthersStock] = useState(null);
  const [sellBtn,setSellBtn] = useState(null)
  const [showCurrentPage,setShowCurrentPage] = useState(0)
  const [loadMoreData,setLoadMoreData] = useState(true)
  const [cardLoad,setCardLoad] = useState(null)
  const [echoerLoad,setEchoerLoad] = useState(null)
  const [echoingOn,setEchoingOn] = useState(null)
  const [futureOn,setFutureOn] = useState(null)
  const [snapshotOn,setSnapshotOn] = useState(null)
  const [preferences, setPreferences] = useState(null);
  const [currentPage, setCurrentPage] = useState(0);
  const [currentPageDiscover, setCurrentPageDiscover] = useState(0);
  const [primarySelected, setPrimarySelected] = useState([]);
  const [discoverApply,setDiscoverApply] = useState(null)
  const [secondarySubCatID, setSecondarySubCatID] = useState()
  const [secondaryID, setSecondaryID] = useState()
  useEffect(() => {
    getUserData();
  }, []);

  const getUserData = async () => {
    let {token, userData} = await auth.getUserDataa();

    let bearer = `Bearer ${token}`;

    setToken(bearer);

    setUserData(JSON.parse(userData));

    const decoded = jwt_decode(token);

    let pref = {
      default_chart: decoded.default_chart,
      four_chart: decoded.four_chart,
      single_chart: decoded.single_chart,
    };

    setDashboardPreference(pref);

    let toBool =
      decoded.ticker === 'true' || decoded.ticker === null ? true : false;

    setTickerVisible(toBool);
  };

  return (
    <UserDataContext.Provider
      value={{
        authUserData,
        loginUserData,
        passwordotp,
        token,
        tickerData,
        companyDescribe,
        ratingChanged,
        rating,
        peers,
        symbol: companyDescribe.symbol,
        tickerVisible,
        primary,
        secondary,
        secondaryInd,
        secondarySect,
        dashboardPreference,
        portfolioConnection,
        portfolioconnection,
        portfolioTimePeriod,
        myStockConnection,
        myStockTimePeriod,
        alpacaConfigStatus,
        tdConfigStatus,
        trackingHistoryConnection,
        achRelationshipStatus,
        wireRelationshipStatus,
        orderHistoryBorkerConnection,
        singleChartSymbol,
        fourChartSymbols,
        ameritradeRefreshTokenStatus,
        ameritradeAlertStatus,
        citizenship,
        none,
        cancelbtn,
        orderId,
        userId,
        ownStock,
        othersStock,
        sellBtn,
        showCurrentPage,
        loadMoreData,
        cardLoad,
        echoerLoad,
        echoingOn,
        preferences,
        futureOn,
        snapshotOn,
        currentPage,
        primarySelected,
        currentPageDiscover,
        discoverApply,
        secondarySubCatID,
        secondaryID,
        setTickerVisible,
        setRating,
        setToken,
        setUserData,
        setLoginUserData,
        setPasswordOTP,
        setTickerData,
        setPreferences,
        setCompanyDescribe,
        setPeers,
        setRatingChanged,
        setPrimary,
        setSecondary,
        setSecondaryInd,
        setSecondarySect,
        setPortfolioConnection,
        setportfolioconnection,
        setPortfolioTimePeriod,
        setmyStockConnection,
        setmyStockTimePeriod,
        setAlpacaConfigStatus,
        setTDConfigStatus,
        setTrackingHistoryConnection,
        setACHRelationshipStatus,
        setWireRelationshipStatus,
        setOrderHistoryBorkerConnection,
        setSingleChartSymbol,
        setFourChartSymbols,
        setAmeritradeRefreshTokenStatus,
        setAmeritradeAlertStatus,
        setcitizenship,
        setNone,
        setCancelbtn,
        setOrderId,
        setUserId,
        setOwnStock,
        setOthersStock,
        setSellBtn,
        setShowCurrentPage,
        setLoadMoreData,
        setCardLoad,
        setEchoerLoad,
        setEchoingOn,
        setFutureOn,
        setSnapshotOn,
        setCurrentPage,
        setPrimarySelected,
        setCurrentPageDiscover,
        setDiscoverApply,
        setSecondarySubCatID,
        setSecondaryID

      }}>
      {props.children}
    </UserDataContext.Provider>
  );
};
