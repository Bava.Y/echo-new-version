import AsyncStorage from '@react-native-community/async-storage';
import {StackActions} from '@react-navigation/native';
import _ from 'lodash';
import React, {useState} from 'react';
import {
  Image,
  KeyboardAvoidingView,
  Platform,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import DeviceInfo from 'react-native-device-info';
import Snackbar from 'react-native-snackbar';
import {auth} from '../api';
import {APP_LOGO} from '../assets/images';
import {FloatingTitleTextInputField} from '../components/FloatingTitleTextInputField';
import Loading from '../components/Loading';
import style from '../styles/loginstyle';
import {LOGIN_USER, TOKENN} from '../utils/constants';
import theme from '../utils/theme';
import Modal from 'react-native-modal';

const initialValidationError = {
  userName: false,
  passWord: false,
  invalid: false,
};

const showSnackBar = (message) =>
  Snackbar.show({
    text: message,
    duration: Snackbar.LENGTH_SHORT,
  });

const SocialMpin = ({navigation, route}) => {
  let deviceId = DeviceInfo.getUniqueId();
  const [validationError, setValidationError] = useState({
    ...initialValidationError,
  });

  const [username, setUserName] = useState(route.params.user);
  const [password, setPassword] = useState('');
  const [loading, setLoading] = useState(false);
  const [isAlreadyExists, setAlreadyExists] = useState(false);
  const [id, setId] = useState(undefined);

  const deviceChangeLogin = async () => {
    let formData = changingFields();
    if (!_.isUndefined(formData)) {
      setAlreadyExists(false);
      setLoading(true);
      await AsyncStorage.removeItem(TOKENN);
      await AsyncStorage.removeItem(LOGIN_USER);
      let responce = await auth.deviceChangeLogin(formData);

      if (typeof responce === 'string') {
        setLoading(false);

        showSnackBar(responce);

        return;
      }
      showSnackBar('OTP sent successfully');
      setTimeout(() => {
        setLoading(false);
        let userData = {...responce, username, password};

        navigation.dispatch(
          StackActions.replace('OTPScreen', {
            data: userData,
          }),
        );
      }, 1500);
    } else {
      showSnackBar('Enter valid username or password');
    }
  };

  const changingFields = () => {
    let formData = new FormData();
    formData && formData.append('id', id);
    formData && formData.append('status', 1);
    formData && formData.append('unique_id', deviceId);
    formData && formData.append('username', username);
    formData && formData.append('password', password);
    return formData;
  };

  const handleSubmit = async () => {
    let formData = validateFields();

    if (!_.isUndefined(formData)) {
      setLoading(true);
      await AsyncStorage.removeItem(TOKENN);
      let responce = await auth.login({username, password});
      if (typeof responce === 'string') {
        let myResponse = JSON.parse(responce);

        setLoading(false);

        if (myResponse.message == 'This user already exist in another device') {
          setAlreadyExists(true);
          setId(Number(myResponse.data));
        } else {
          showSnackBar(myResponse.message);
        }

        return;
      }
      showSnackBar('OTP sent successfully');
      setTimeout(() => {
        setLoading(false);
        let userData = {...responce, username, password};

        navigation.dispatch(
          StackActions.replace('OTPScreen', {
            data: userData,
          }),
        );
      }, 1500);
    } else {
      showSnackBar('Invalid username or password');
    }
  };

  const validateFields = () => {
    let formData = new FormData();
    let error = validationError;
    if (username === '') {
      error['userName'] = 'Enter User Name';
      formData = undefined;
    } else {
      error['userName'] = false;
      formData && formData.append('username', username);
    }
    if (password === '') {
      error['passWord'] = 'Enter Password';
      formData = undefined;
    } else {
      error['passWord'] = false;
      formData && formData.append('password', password);
    }
    setValidationError({...validationError, ...error});
    return formData;
  };

  return (
    <>
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : null}
        keyboardVerticalOffset={Platform.OS === 'ios' ? 40 : 0}
        style={style.container}>
        <Image source={APP_LOGO} style={style.appLogo} />

        <Text style={style.forgottitle}>Sign in</Text>

        <FloatingTitleTextInputField
          ispassword={false}
          attrName="username"
          title="Email or User Name"
          value={username}
          updateMasterState={(text) => {
            setValidationError({
              ...validationError,
              userName: _.isEmpty(text) ? 'Enter User Name' : false,
            });
            setUserName(text);
          }}
        />
        {validationError.userName ? (
          <Text style={style.errormsg}>{validationError.userName}</Text>
        ) : null}

        <FloatingTitleTextInputField
          ispassword={true}
          attrName="password"
          title="Password"
          value={password}
          updateMasterState={(text) => {
            setValidationError({
              ...validationError,
              passWord: _.isEmpty(text) ? 'Enter Password' : false,
            });
            setPassword(text);
          }}
        />
        {validationError.passWord ? (
          <Text style={style.errormsg}>{validationError.passWord}</Text>
        ) : null}
        {validationError.invalid ? (
          <Text style={{color: '#ff0000a1', marginRight: '45%'}}>
            {validationError.invalid}
          </Text>
        ) : null}

        <TouchableOpacity style={style.signinBtn} onPress={handleSubmit}>
          <Text style={style.signintext}>Submit</Text>
        </TouchableOpacity>
      </KeyboardAvoidingView>

      <Modal
        animationType="fade"
        transparent={true}
        isVisible={isAlreadyExists}
        onBackdropPress={() => setAlreadyExists(false)}>
        <View style={style.welcomecontainer}>
          <Text style={style.welcometxt}>
            This user already exists in another device
          </Text>
          <Text style={style.steptext}>Do you want to continue?</Text>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-evenly',
            }}>
            <View
              style={style.startbtn}
              onStartShouldSetResponder={deviceChangeLogin}>
              <Text style={style.starttxt}>Yes</Text>
            </View>
            <View
              style={style.startbtn}
              onStartShouldSetResponder={() => setAlreadyExists(false)}>
              <Text style={style.starttxt}>No</Text>
            </View>
          </View>
        </View>
      </Modal>

      {loading && <Loading color={theme.white} />}
    </>
  );
};

export default SocialMpin;
