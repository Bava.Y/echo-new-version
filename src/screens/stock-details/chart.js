import React, {useEffect, useState} from 'react';
import {Dimensions} from 'react-native';
import WebView from 'react-native-webview';
import HeaderNav from '../../components/HeaderNav';

const Chart = () => {
  const [device, setDevice] = useState({
    width: 0,
    height: 0,
  });

  useEffect(() => {
    const deviceWidth = Math.round(Dimensions.get('window').width);
    const deviceHeight = Math.round(Dimensions.get('window').height);

    setDevice({
      width: deviceWidth,
      height: deviceHeight,
    });
  }, []);

  return (
    <>
      <HeaderNav title="Company" isBack="true" />
      <WebView
        source={{
          html: `
            <div class="tradingview-widget-container">
              <div id="tradingview_13433"></div>
              <div class="tradingview-widget-copyright"><a href="https://www.tradingview.com/symbols/NYSE-TWTR/" rel="noopener" target="_blank"><span class="blue-text">TWTR Chart</span></a> by TradingView</div>
              <script type="text/javascript" src="https://s3.tradingview.com/tv.js"></script>
              <script type="text/javascript">
              new TradingView.widget(
              {
              "autosize":true,
              "symbol": "NYSE:TWTR",
              "timezone": "Etc/UTC",
              "theme": "light",
              "style": "1",
              "locale": "en",
              "toolbar_bg": "#f1f3f6",
              "enable_publishing": false,
              "withdateranges": true,
              "range": "YTD",
              "hide_side_toolbar": false,
              "allow_symbol_change": true,
              "container_id": "tradingview_13433"
            }
              );
              </script>
            </div>`,
        }}
      />
    </>
  );
};

export default Chart;
