import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import React from 'react';
import {View} from 'react-native';
import theme from '../../../utils/theme';
import DepositFund from './../../deposit-fund/index';
import MyTabBar from './../components/financialTab';
import BankAccount from './bank-account';
import Channels from './channels';
import Echo from './echo';
import EchoWallet from './echo-wallet';
import FinanceSummary from './finance-summary';
import Portfolio from './portfolio';
import styles from './styles';
import Transaction from './transaction';

const FinanceSummaryTab = ({initialRoute}) => {
  // FinanceSummaryTab Variables
  const Tab = createMaterialTopTabNavigator();

  return (
    <View style={styles.container}>
      <Tab.Navigator
        style={{backgroundColor: theme.primaryColor}}
        sceneContainerStyle={{
          backgroundColor: theme.themeColor,
          paddingHorizontal: 8,
          paddingVertical: 8,
        }}
        tabBar={(props) => <MyTabBar {...props} />}
        backBehavior="initialRoute"
        initialRouteName={initialRoute}
        animationEnabled={true}
        swipeEnabled={true} //can change dynamically
        tabBarOptions={{scrollEnabled: true}}>
        <Tab.Screen name="Fund" component={DepositFund} />

        <Tab.Screen name="Bank Account" component={BankAccount} />

        <Tab.Screen name="Transactions" component={Transaction} />

        {/* <Tab.Screen name="Echo Wallet" component={EchoWallet} />

        <Tab.Screen name="Echo" component={Echo} /> */}

        <Tab.Screen name="Portfolio" component={Portfolio} />
{/* 
        <Tab.Screen name="Channels" component={Channels} />

        <Tab.Screen name="Subscription" component={FinanceSummary} /> */}
      </Tab.Navigator>
    </View>
  );
};

export default FinanceSummaryTab;
