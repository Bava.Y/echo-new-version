import {useFocusEffect} from '@react-navigation/core';
import React, {useCallback, useContext, useState,useEffect} from 'react';
import {Platform, ScrollView, Text, View} from 'react-native';
import Snackbar from 'react-native-snackbar';
import {portfolio} from '../../api';
import AmeritradeAlert from '../../components/AmeritradeAlert';
import Loading from '../../components/Loading';
import {UserDataContext} from '../../context/UserDataContext';
import theme from '../../utils/theme';
import Card from './card';
import styles from './style';

const openOrders = () => {
  // OpenOrders Variables
  const [orderData, setOrderData] = useState(null);
  const [statusValue, setStatus] = useState('open');
  const {orderId,setOrderId} = useContext(UserDataContext);


  // Context Variables
  const {
    ameritradeAlertStatus,
    orderHistoryBorkerConnection,
    setAmeritradeAlertStatus,
    token,
  } = useContext(UserDataContext);

  useFocusEffect(
    useCallback(() => {
      let isFocus = true;

      fetchOrdersHistory();

      return () => {
        isFocus = false;
      };
    }, [orderHistoryBorkerConnection]),
  );
// useEffect(()=>{
//   fetchOrdersHistory()
// })
  const fetchOrdersHistory = async () => {
    const response = await portfolio.getOrdersHistory(
      orderHistoryBorkerConnection,
      statusValue,
    );

    if (Boolean(response) && response.data.length !== 0) {
      if (response.keyword == 'success') {
        let helperArray = [];
        response.data.map((lol) => {
          const helperObject = {
            channel: lol.channel,
            qty: lol.qty,
            amount: lol?.amount ?? null,
            side: lol?.side ?? null,
            symbol: lol.symbol,
            orderId: lol?.order_id,
            status: lol.status || '',
          };
          setOrderId(helperObject.orderId);
          helperArray.push(helperObject);
        });

        filterOpenOrderStatus(helperArray);
      } else {
        if (response.hasOwnProperty('message')) {
          if (response.message.hasOwnProperty('error')) {
            setOrderData([]);

            switch (response.message.error) {
              case 'invalid_grant':
                handleAmeritradeAlertStatus();
                break;

              default:
                Snackbar.show({
                  duration: Snackbar.LENGTH_SHORT,
                  text: response.message.error,
                  backgroundColor: 'red',
                });
                break;
            }
          } else {
            setOrderData([]);
          }
        } else {
          setOrderData([]);
        }
      }
    } else {
      setOrderData([]);
    }
  };

  const filterOpenOrderStatus = (responseData) => {
    const filteredArray = responseData.filter(
      (value) =>
        Boolean(value.status) &&
        value.status.toLowerCase() !== handleStatus(value.status.toLowerCase()),
    );

    setOrderData(filteredArray);
  };

 

  const handleStatus = (status) => {
    switch (status) {
      case 'buy':
      case 'canceled':
      case 'canceling':
      case 'cancelled':
      case 'completed':
      case 'done':
      case 'expired':
      case 'filled':
      case 'not sent':
      case 'pending_cancel':
      case 'rejected':
      case 'sell':
      case 'stopped':
      case 'suspended':
      case 'tltc':
      case 'u r out':
        return status;
    }
  };

  const handleAmeritradeAlertStatus = () => {
    setAmeritradeAlertStatus(!ameritradeAlertStatus);
  };

  return (
    <>
      {Boolean(orderData) ? (
        orderData.length !== 0 ? (
          <ScrollView
            keyboardShouldPersistTaps={'handled'}
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
            style={styles.container}>
            <View style={{paddingHorizontal: 8, paddingVertical: 16}}>
              {orderData.map((lol, index) => (
                <Card key={index} orderData={lol} cancelbtn={true} />
              ))}
            </View>

            <View style={{paddingVertical: 12}} />
          </ScrollView>
        ) : (
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: theme.themeColor,
            }}>
            <Text
              style={{
                fontSize: Platform.OS == 'ios' ? 14 : 16,
                fontWeight: '600',
                color: theme.white,
              }}>
              No Data Found
            </Text>
          </View>
        )
      ) : (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: theme.themeColor,
          }}>
          <Loading color={theme.secondryColor} />
        </View>
      )}

      {/* Hided due to TD Ameritrade Echo App revoked issue */}
      {/* <AmeritradeAlert
        modalStatus={ameritradeAlertStatus}
        close={handleAmeritradeAlertStatus}
      /> */}
    </>
  );
};

export default openOrders;
