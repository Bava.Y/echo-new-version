import AsyncStorage from '@react-native-community/async-storage';
import {StackActions, useFocusEffect} from '@react-navigation/native';
import React, {useCallback, useRef, useState} from 'react';
import {Image, Platform, Text, TouchableOpacity, View} from 'react-native';
import DeviceInfo from 'react-native-device-info';
import ReactNativePinView from 'react-native-pin-view';
import Snackbar from 'react-native-snackbar';
import {auth} from '../api/Authentication.service';
import {CANCEL, OK} from '../assets/images';
import Loading from '../components/Loading';
import style from '../styles/mpinstyle';
import {IS_FIRST_USER, MPIN} from '../utils/constants';
import theme from '../utils/theme';

const ResetNewCPIN = ({navigation, route}) => {
  const pinView = useRef(null);

  useFocusEffect(
    useCallback(() => {
      let isActive = true;
      const setDeviceInfo = () => {
        var uniqueId = DeviceInfo.getUniqueId();
        setDeviceId(uniqueId);
      };
      setDeviceInfo();

      return () => {
        isActive = false;
      };
    }, [navigation]),
  );

  let userId = route.params.userInfo.user_id;

  const [deviceId, setDeviceId] = useState('');

  const [otp, setOtp] = useState('');

  const [validationError, setValidationError] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');
  const [loading, setLoading] = useState(false);

  const onTextChange = (text) => {
    setOtp(text);
    setValidationError(false);
  };

  const handleSubmit = async () => {
    let formData = validateFields();
    if (formData !== undefined) {
      setLoading(true);

      const data = await auth.saveMpin(formData);

      if (typeof data === 'string') {
        if (data === 'redirect') {
          return navigation.dispatch(StackActions.replace('Login'));
        }
        return Snackbar.show({
          text: data,
          duration: Snackbar.LENGTH_SHORT,
        });
      }
      await AsyncStorage.setItem(IS_FIRST_USER, data.is_first_login);
      setLoading(false);
      Snackbar.show({
        text: `MPIN registered successfully`,
        duration: Snackbar.LENGTH_SHORT,
      });
      await AsyncStorage.setItem(MPIN, 'true');
      setTimeout(() => {
        navigation.dispatch(
          StackActions.replace('MpinScreen', {
            deviceId: deviceId,
            userInfo: route.params.userInfo,
          }),
        );
      }, 2000);
    }
  };

  const validateFields = () => {
    let formData = new FormData();
    let previousOtp = route.params.previousOtp;
    if (otp.length !== 4) {
      setErrorMessage('Enter proper MPIN');
      setValidationError(true);
      formData = undefined;
    } else if (otp !== previousOtp) {
      setErrorMessage('MPIN mismatch');
      setValidationError(true);
      formData = undefined;
    } else {
      setErrorMessage('');
      setValidationError(false);
      formData && formData.append('mpin', otp);
      formData && formData.append('unique_id', deviceId);
      formData && formData.append('id', userId);
    }
    return formData;
  };

  return (
    <>
      <View style={style.container}>
        <Text style={style.verifyText}>Set PIN</Text>

        <Text style={style.mpinText}>Confirm your 4-digit 
PIN</Text>

        <ReactNativePinView
          inputSize={20}
          ref={pinView}
          pinLength={4}
          buttonSize={60}
          buttonAreaStyle={{
            marginTop: 24,
          }}
          inputViewEmptyStyle={{
            backgroundColor: 'transparent',
            borderColor: '#FFF',
            borderWidth: 1,
          }}
          inputViewFilledStyle={{
            backgroundColor: validationError ? 'red' : '#FFF',
          }}
          buttonViewStyle={{
            borderColor: '#FFF',
            borderWidth: 1,
          }}
          onButtonPress={(key) => {
            if (key === 'custom_left') {
              pinView.current.clear();
            }
          }}
          customLeftButton={
            <View>
              <Image source={CANCEL} style={style.customButton} />
            </View>
          }
          customRightButton={
            <TouchableOpacity onPress={handleSubmit}>
              <Image source={OK} style={style.customButton} />
            </TouchableOpacity>
          }
          onValueChange={onTextChange}
        />
        {validationError ? (
          <Text
            style={{
              fontSize: Platform.OS == 'ios' ? 14 : 16,
              color: 'red',
              marginVertical: 20,
            }}>
            {errorMessage}
          </Text>
        ) : null}
      </View>

      {loading && <Loading color={theme.white} />}
    </>
  );
};

export default ResetNewCPIN;
