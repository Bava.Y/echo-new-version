import React, {useState, useContext, useEffect} from 'react';
import {Image, Platform, Text, View, ActivityIndicator} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {ALPACA, AMERITRADE} from '../../assets/images/index';
import Theme from '../../utils/theme';
import {portfolio} from '../../api/index';
import Snackbar from 'react-native-snackbar';
import {UserDataContext} from '../../context/UserDataContext';
import theme from '../../utils/theme';

const card = (props) => {

  const [loading , setLoading] =useState(null)
  // Props Variables
  const orderData = props.orderData;
  let {cancelbtn} = props;
  const {orderId} = useContext(UserDataContext);

  const handleStatusColor = (status) => {
    switch (status) {
      case 'accepted':
      case 'buy':
      case 'completed':
      case 'filled':
      case 'u r out':
        return '#008000';

      case 'cancelled':
      case 'canceled':
      case 'canceling':
      case 'done':
      case 'expired':
      case 'not sent':
      case 'pending_cancel':
      case 'rejected':
      case 'sell':
      case 'stopped':
      case 'suspended':
      case 'tltc':
        return '#FF0000';

      case 'done_for_day':
      case 'partially_filled':
        return '#DFFF00';

      case 'calculated':
      case 'submitting':
        return '#FFBF00';

      case 'pending_replace':
      case 'replaced':
      case 'replacing':
        return '#FF0080';

      case 'pending':
      case 'queued':
      case status.toLowerCase().includes('wait'):
      case 'working':
        return '#8000FF';

      default:
        return '#6495ED';
    }
  };

  const isFloat = (n) => {
    return Number(n) % 1 !== 0;
  };

  const removeCard = (index) => {
    const cancCard = [...orderDatas];
    cancCard.splice(index, 1);
    setOrderDatas(cancCard);
  };

  const CancelOrder = async () => {
    setLoading(true)
    let requestData = new FormData();
    requestData.append('order_id', orderId);

    let response = await portfolio.cancelOrderAlpaca(requestData);
setLoading(false)
    Snackbar.show({
      backgroundColor: 'green',
      duration: Snackbar.LENGTH_SHORT,
      text: response.message,
    });
    portfolio.cancelOrderAlpaca(requestData);
  };

  useEffect(()=>{
    if(loading == false){
      CancelOrder()
    }
  },[])
  return (
    <View
      style={{
        backgroundColor: Theme.primaryColor,
        borderColor: Theme.primaryColor,
        borderWidth: 0.25,
        borderRadius: 8,
        marginBottom: 8,
        paddingHorizontal: 12,
        paddingVertical: 12,
        elevation: 4,
      }}>
      <View style={{flexDirection: 'row'}}>
        <View style={{flex: 0.33, justifyContent: 'center'}}>
          <Text
            style={{
              fontSize: Platform.OS == 'ios' ? 14 : 16,
              fontWeight: '800',
              color: Theme.white,
            }}>
            {orderData.symbol}
          </Text>
        </View>

        <View style={{flex: 0.5, paddingHorizontal: 10}}>
          <Image
            source={orderData.channel == 1 ? AMERITRADE : ALPACA}
            style={{width: 60, height: 24, resizeMode: 'contain'}}
          />
        </View>

        {Boolean(orderData.status) && (
          <View
            style={{
              flex: 0.265,
              justifyContent: 'center',
              backgroundColor: handleStatusColor(
                orderData.status.toLowerCase(),
              ),
              paddingHorizontal: 6,
              paddingVertical: 6,
              borderRadius: 4,
            }}>
            <Text
              style={{
                fontSize: Platform.OS == 'ios' ? 10 : 12,
                fontWeight: 'bold',
                color: Theme.white,
                textTransform: 'uppercase',
                textAlign: 'center',
              }}>
              {orderData.status}
            </Text>
          </View>
        )}
      </View>

      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          marginTop: 8,
        }}>
        <View style={{justifyContent: 'center', flex: 0.33}}>
          <Text
            style={{
              fontSize: Platform.OS == 'ios' ? 12 : 14,
              fontWeight: '600',
              color: '#696969',
              textAlign: 'left',
            }}>
            QTY{'   '}
            <Text
              style={{
                fontSize: Platform.OS == 'ios' ? 14 : 16,
                fontWeight: '800',
                color: '#C0C0C0',
              }}>
              {parseFloat(orderData.qty).toFixed(
                isFloat(orderData.qty) ? 2 : 0,
              )}
            </Text>
          </Text>
        </View>

        <View
          style={{
            justifyContent: 'center',
            flex: cancelbtn == true ? 0.53 : 0.75,
          }}>
          <Text
            style={{
              fontSize: Platform.OS == 'ios' ? 12 : 14,
              fontWeight: '600',
              color: '#696969',
              textAlign: 'left',
            }}>
            PRICE{'   '}
            <Text
              style={{
                fontSize: Platform.OS == 'ios' ? 14 : 16,
                fontWeight: '800',
                color: '#C0C0C0',
              }}>
              {`$${
                Boolean(orderData.amount)
                  ? parseFloat(orderData.amount).toFixed(2)
                  : ' - '
              }`}
            </Text>
          </Text>
        </View>

        {cancelbtn && (
          <TouchableOpacity
            onPress={() => {
              CancelOrder();
            }}
            style={{
              flex: 0.35,
              justifyContent: 'center',
              backgroundColor: 'red',
              paddingHorizontal: 21,
              paddingVertical: 6,
              borderRadius: 4,
            }}>
              
                <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 10 : 12,
                  fontWeight: 'bold',
                  color: Theme.white,
                  textTransform: 'uppercase',
                  textAlign: 'center',
                }}>
              {loading == true ?  <View style={{paddingHorizontal:14}}><ActivityIndicator size={15} color={theme.secondryColor} /></View> : 'CANCEL'}
              </Text>
              
          
          </TouchableOpacity>
        )}
      </View>

      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginTop: 8,
        }}>
        <TouchableOpacity style={{justifyContent: 'center', flex: 0.2}}>
          <Text
            style={{
              fontSize: Platform.OS == 'ios' ? 10 : 12,
              fontWeight: 'bold',
              color: Theme.white,
              textAlign: 'center',
              paddingHorizontal: 12.1,
              paddingVertical: 6,
              backgroundColor: '#808080',
              borderRadius: 4,
            }}>
            NYSE
          </Text>
        </TouchableOpacity>

        <View style={{flex: 0.8}}>
          <View
            style={{
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontSize: Platform.OS == 'ios' ? 12 : 14,
                fontWeight: '600',
                color: '#696969',
                textAlign: 'left',
              }}>
              SIDE{'    '}
              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 14 : 16,
                  fontWeight: '800',
                  color: Boolean(orderData.side)
                    ? handleStatusColor(orderData.side.toLowerCase())
                    : '#C0C0C0',
                  textTransform: 'capitalize',
                }}>
                {Boolean(orderData.side) ? orderData.side.toUpperCase() : ' - '}
              </Text>
            </Text>
          </View>
        </View>
      </View>
    </View>
  );
};

export default card;
