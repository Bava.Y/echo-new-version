import {func, number, object, string} from 'prop-types';
import React, {Component} from 'react';
import {Animated, Platform, StyleSheet, TextInput, View} from 'react-native';

export class FloatingTitleTextInputField extends Component {
  static propTypes = {
    attrName: string.isRequired,
    title: string.isRequired,
    value: string.isRequired,
    updateMasterState: func.isRequired,
    keyboardType: string,
    titleActiveSize: number, // to control size of title when field is active
    titleInActiveSize: number, // to control size of title when field is inactive
    titleActiveColor: string, // to control color of title when field is active
    titleInactiveColor: string, // to control color of title when field is active
    textInputStyles: object,
    otherTextInputProps: object,
  };

  static defaultProps = {
    keyboardType: 'default',
    titleActiveSize: 12,
    titleInActiveSize: 14,
    titleActiveColor: '#ffffffc9',
    titleInactiveColor: '#ffffffc9',
    textInputStyles: {},
    otherTextInputAttributes: {},
  };

  constructor(props) {
    super(props);
    const {value} = this.props;
    this.position = new Animated.Value(value ? 1 : 0);
    this.state = {
      isFieldActive: false,
    };
  }

  _handleFocus = () => {
    if (!this.state.isFieldActive) {
      this.setState({isFieldActive: true});
      Animated.timing(this.position, {
        toValue: 1,
        duration: 150,
        useNativeDriver: false,
      }).start();
    }
  };

  _handleBlur = () => {
    if (this.state.isFieldActive && !this.props.value) {
      this.setState({isFieldActive: false});
      Animated.timing(this.position, {
        toValue: 0,
        duration: 150,
        useNativeDriver: false,
      }).start();
    }
  };

  _onChangeText = (updatedValue) => {
    const {updateMasterState} = this.props;
    updateMasterState(updatedValue);
  };

  _returnAnimatedTitleStyles = () => {
    const {isFieldActive} = this.state;
    const {
      titleActiveColor,
      titleInactiveColor,
      titleActiveSize,
      titleInActiveSize,
    } = this.props;

    return {
      top: this.position.interpolate({
        inputRange: [0, 1],
        outputRange: [16, 0],
      }),
      fontSize: isFieldActive ? titleActiveSize : titleInActiveSize,
      color: isFieldActive ? titleActiveColor : titleInactiveColor,
    };
  };

  render() {
    // const ispassword
    return (
      <View style={Styles.container}>
        <Animated.Text
          style={[Styles.titleStyles, this._returnAnimatedTitleStyles()]}>
          {this.props.title}
        </Animated.Text>
        <View style={Styles.textInputContainer}>
          <TextInput
            secureTextEntry={this.props.ispassword ? true : false}
            value={this.props.value}
            style={[Styles.textInput, this.props.textInputStyles]}
            underlineColorAndroid="transparent"
            onFocus={this._handleFocus}
            onBlur={this._handleBlur}
            onChangeText={this._onChangeText}
            keyboardType={this.props.keyboardType}
          />
        </View>
      </View>
    );
  }
}

const Styles = StyleSheet.create({
  container: {
    width: '100%',
    marginVertical: 4,
    borderBottomColor: 'white',
    borderBottomWidth: 0.5,
  },
  textInputContainer: {
    height: 48,
    justifyContent: 'center',
    top: 4,
  },
  textInput: {
    fontSize: Platform.OS == 'ios' ? 12 : 14,
    fontFamily: 'Roboto',
    color: 'white',
    marginTop: 4,
    paddingHorizontal: 4,
  },
  titleStyles: {
    position: 'absolute',
    fontFamily: 'Roboto',
    left: 4,
  },
});
