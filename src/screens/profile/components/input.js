import React, {useState} from 'react';
import {TextInput} from 'react-native';

const SwitchScreen = () => {
  const [value, setValue] = useState();
  return (
    <TextInput
      style={{borderColor: 'red', borderWidth: 1, width: 30}}
      onChangeText={(text) => setValue(text)}
      value={value}
    />
  );
};

export default SwitchScreen;
