import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {Button, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import {
  VictoryAxis,
  VictoryBar,
  VictoryChart,
  VictoryContainer,
  VictoryGroup,
  VictoryLegend,
} from 'victory-native';
import BottomNav from '../../components/BottomNav';
import HeaderNav from '../../components/HeaderNav';
import theme from '../../utils/theme';
import styles from './style';

const Company = ({route, navigation}) => {
  const symbol = route.params.symbol;
  const LIGHT_GREY = 'hsl(355, 20%, 90%)';
  const [profitList, setProfitList] = useState([]);
  const [incomeList, setIncomeList] = useState([]);
  const [isloading, setIsLoading] = useState();
  let maxDomainVal = 0;

  useEffect(() => {
    navigation.setOptions({title: symbol});

    axios
      .get(
        `https://cloud.iexapis.com/stable/stock/${route.params.symbol}/income?token=sk_e328cad5acc24d7599ce23417c522bd0&period=annual&last=5`,
      )
      .then((res) => {
        setIsLoading(true);
        const statement = res.data.income;
        if (profitList[0].x === '2020' || profitList[0].x === '2019') {
          profitList.reverse();
          incomeList.reverse();
        }
        const incomeMax = Math.max.apply(
          Math,
          incomeList.map(function (o) {
            return o.y;
          }),
        );
        maxDomainVal = incomeMax + findPercentage(incomeMax);
        setIsLoading(false);
      });
  }, []);

  function findPercentage(val) {
    return (val * 5) / 100;
  }

  return (
    <>
      <HeaderNav title={'Financial Report (' + symbol + ')'} isBack="true" />
      <View
        style={{
          backgroundColor: '#2b2a31',
          height: '100%',
        }}>
        <ScrollView>
          <View
            style={{
              borderBottomColor: 'white',
              borderWidth: 1,
              paddingBottom: 10,
            }}>
            <View style={{flexDirection: 'row'}}>
              <View style={{width: '50%', padding: 10}}>
                <Text style={styles.title}>Last Price</Text>
                <Text style={styles.lastpricevalue}>$0.008</Text>
              </View>
              <View style={{width: '50%', padding: 10}}>
                <Text style={styles.title}>Today's Change</Text>
                <Text style={styles.todaychange}>0.001(14.29%)</Text>
              </View>
            </View>
            <View
              style={{flexDirection: 'row', backgroundColor: theme.themeColor}}>
              <View style={{width: '25%', padding: 10}}>
                <Text style={styles.stitle}>Open($)</Text>
                <Text style={styles.pricevalue}>0.007</Text>
              </View>
              <View style={{width: '25%', padding: 10}}>
                <Text style={styles.stitle}>High($)</Text>
                <Text style={styles.pricevalue}>0.001</Text>
              </View>
              <View style={{width: '25%', padding: 10}}>
                <Text style={styles.stitle}>Low($)</Text>
                <Text style={styles.pricevalue}>0.007</Text>
              </View>
              <View style={{width: '25%', padding: 10}}>
                <Text style={styles.stitle}>Volume</Text>
                <Text style={styles.pricevalue}>283.75M</Text>
              </View>
            </View>
            <View style={{flexDirection: 'row'}}>
              <View style={{width: '50%', padding: 10}}>
                <Button
                  title="Buy"
                  color="#0ec60ec9"
                  onPress={() => {
                    navigation.navigate('Order', {side: 'buy', symbol: symbol});
                  }}
                />
              </View>
              <View style={{width: '50%', padding: 10}}>
                <Button
                  title="Sell"
                  color="#d12828"
                  onPress={() => {
                    navigation.navigate('Order', {
                      side: 'sell',
                      symbol: symbol,
                    });
                  }}
                />
              </View>
            </View>
          </View>
          <Text
            style={{
              color: 'white',
              textAlign: 'center',
              fontSize: 18,
              fontWeight: 'bold',
              marginTop: 10,
            }}>
            Chart
          </Text>
          <VictoryChart
            style={{labels: {fontSize: 8}}}
            height={430}
            padding={{top: 40, bottom: 40, left: 85, right: 70}}
            containerComponent={<VictoryContainer responsive={true} />}>
            <VictoryAxis
              style={{
                axis: {
                  stroke: 'grey',
                },
                axisLabel: {
                  fill: LIGHT_GREY,
                  padding: 20,

                  fontSize: 13,
                  fontStyle: 'italic',
                },
                tickLabels: {
                  fill: LIGHT_GREY,
                  fontSize: 13,
                },
              }}
            />
            <VictoryAxis
              dependentAxis
              style={{
                axis: {
                  stroke: 'transparent',
                },
                tickLabels: {
                  fill: LIGHT_GREY,
                  fontSize: 13,
                },
                axisLabel: {
                  fill: 'red',
                  padding: 40,
                  fontSize: 13,
                  fontStyle: 'italic',
                },
                grid: {
                  fill: LIGHT_GREY,
                  stroke: LIGHT_GREY,
                  strokeWidth: 0.1,
                },
              }}
            />

            <VictoryGroup
              offset={20}
              colorScale={['navy', 'blue']}
              domainPadding={{x: [10, -10], y: 5}}>
              <VictoryBar data={incomeList} style={{marginLeft: 10}} />
              <VictoryBar data={profitList} />
            </VictoryGroup>
            <VictoryLegend
              x={125}
              y={410}
              orientation="horizontal"
              gutter={20}
              style={{
                data: {fill: 'blue', stroke: 'navy', strokeWidth: 2},
                labels: {fill: 'navy'},

                title: {fontSize: 20},
              }}
              data={[
                {
                  name: 'Income',
                  symbol: {fill: 'navy'},
                  labels: {fill: 'white'},
                },
                {
                  name: 'Profit',
                  symbol: {fill: 'blue'},
                  labels: {fill: 'white'},
                },
              ]}
            />
          </VictoryChart>
          <View style={{marginTop: 20, padding: 10, marginBottom: 40}}>
            <TouchableOpacity
              style={{
                alignItems: 'center',
                backgroundColor: '#fcd700',
                padding: 10,
              }}
              onPress={() => {
                navigation.navigate('chart');
              }}>
              <Text style={{color: 'black', fontSize: 18, fontWeight: 'bold'}}>
                Technical Chart
              </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
      <BottomNav />
    </>
  );
};

export default Company;
