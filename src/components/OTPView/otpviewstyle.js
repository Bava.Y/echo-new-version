import {Platform} from 'react-native';
import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  textInput: {
    height: 48,
    width: 48,
    fontSize: Platform.OS == 'ios' ? 18 : 20,
    fontWeight: '600',
    color: '#000000',
    textAlign: 'center',
    margin: 8,
    borderBottomWidth: 4,
  },
});

export default styles;
