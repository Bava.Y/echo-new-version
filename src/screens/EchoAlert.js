import {useFocusEffect} from '@react-navigation/native';
import React, {useCallback, useContext, useState} from 'react';
import {
  ActivityIndicator,
  Image,
  Linking,
  Modal,
  Platform,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Snackbar from 'react-native-snackbar';
import FontAwesomeIcons from 'react-native-vector-icons/FontAwesome5';
import {dashboard} from '../api';
import HeaderNav from '../components/HeaderNav';
import {UserDataContext} from '../context/UserDataContext';
import theme from '../utils/theme';

const EchoAlert = (props) => {
  // Props variables
  const id = props?.route?.params?.id ?? null;

  // EchoAlert Variables
  const [responseData, setResponseData] = useState(null);
  const [imageViewStatus, setImageViewStatus] = useState(false);
  const [imageViewURI, setImageViewURI] = useState(null);

  // Context Variables
  const {token} = useContext(UserDataContext);

  // Other Variables
  const [imageLoader, setImageLoader] = useState(false);

  useFocusEffect(
    useCallback(() => {
      let isFocus = true;

      if (Boolean(id)) {
        fetchNotifications();
      } else {
        setResponseData({});
      }

      return () => {
        isFocus = false;
      };
    }, []),
  );

  const fetchNotifications = async () => {
    const res = await dashboard.getEchoMessage(token, id);

    if (res) {
      if (res.keyword === 'success') {
        setResponseData(res.data ? res.data : {});
      } else {
        Snackbar.show({duration: Snackbar.LENGTH_SHORT, text: res.message});

        setResponseData([]);
      }
    } else {
      setResponseData({});
    }
  };

  const renderImageView = () => {
    return (
      <Modal
        animationType="slide"
        transparent={true}
        onRequestClose={() => {
          setImageViewURI(null);

          setImageViewStatus(!imageViewStatus);
        }}
        hardwareAccelerated={true}
        visible={imageViewStatus}>
        <View style={{flex: 1, backgroundColor: theme.black}}>
          <TouchableOpacity
            onPress={() => {
              setImageViewURI(null);

              setImageViewStatus(!imageViewStatus);
            }}
            style={{
              position: 'absolute',
              top: Platform.OS == 'ios' ? 32 : 16,
              right: 8,
              zIndex: 1,
            }}>
            <FontAwesomeIcons name="times" size={20} color={theme.white} />
          </TouchableOpacity>

          <View
            style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
            <Image
              onLoadStart={() => {
                setImageLoader(true);
              }}
              onLoadEnd={() => {
                setImageLoader(false);
              }}
              source={{uri: imageViewURI}}
              style={{width: '100%', height: '50%'}}
            />

            {Boolean(imageLoader) ? (
              <ActivityIndicator
                size="large"
                color={theme.secondryColor}
                style={{
                  position: 'absolute',
                  left: 0,
                  top: 0,
                  right: 0,
                  bottom: 0,
                }}
              />
            ) : null}
          </View>
        </View>
      </Modal>
    );
  };

  return (
    <>
      <HeaderNav title="Notification" isBack="true" />

      {Boolean(responseData) ? (
        Object.keys(responseData).length != 0 ? (
          <ScrollView
            keyboardShouldPersistTaps="handled"
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
            style={{flex: 1, backgroundColor: theme.themeColor}}>
            <View style={{paddingHorizontal: 16, paddingVertical: 16}}>
              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 16 : 18,
                  color: theme.secondryColor,
                  fontWeight: '800',
                  textAlign: 'center',
                  lineHeight: 18,
                }}>
                {responseData.title}
              </Text>

              <View style={{marginVertical: 8}}>
                <Text
                  style={{
                    fontSize: Platform.OS == 'ios' ? 14 : 16,
                    color: theme.white,
                    fontWeight: '600',
                  }}>
                  Message:
                </Text>

                <View style={{marginTop: 8}}>
                  <Text
                    style={{
                      fontSize: Platform.OS == 'ios' ? 12 : 14,
                      color: theme.textColor,
                      textAlign: 'justify',
                      lineHeight: 18,
                    }}>
                    {`\t `}
                    {responseData.message}
                  </Text>
                </View>
              </View>

              {responseData.hasOwnProperty('link') &&
                Boolean(responseData.link) &&
                Array.isArray(responseData.link) &&
                responseData.link.length != 0 && (
                  <View style={{marginVertical: 8}}>
                    <Text
                      style={{
                        fontSize: Platform.OS == 'ios' ? 14 : 16,
                        color: theme.white,
                        fontWeight: '600',
                      }}>
                      Link(s):
                    </Text>

                    {responseData.link.map((lol, index) => (
                      <View
                        key={index}
                        style={{
                          flexDirection: 'row',
                          justifyContent: 'flex-start',
                          marginTop: 8,
                          marginLeft: 16,
                        }}
                        onPre>
                        <Text
                          style={{
                            fontSize: Platform.OS == 'ios' ? 12 : 14,
                            color: theme.secondryColor,
                            fontWeight: '600',
                          }}>
                          {index + 1}){' '}
                        </Text>

                        <TouchableOpacity
                          onPress={() => {
                            Linking.canOpenURL(lol)
                              .then((checkURL) => {
                                if (checkURL) {
                                  Linking.openURL(lol);
                                } else {
                                  Snackbar.show({
                                    backgroundColor: 'red',
                                    text: 'Unable to open the url',
                                    duration: Snackbar.LENGTH_LONG,
                                  });
                                }
                              })
                              .catch(() => {
                                Snackbar.show({
                                  backgroundColor: 'red',
                                  text: 'Facing some issues in open the url',
                                  duration: Snackbar.LENGTH_LONG,
                                });
                              });
                          }}>
                          <Text
                            style={{
                              fontSize: Platform.OS == 'ios' ? 12 : 14,
                              color: theme.secondryColor,
                              fontWeight: '600',
                              textDecorationLine: 'underline',
                            }}>
                            {lol}
                          </Text>
                        </TouchableOpacity>
                      </View>
                    ))}
                  </View>
                )}

              {responseData.hasOwnProperty('file') &&
                Boolean(responseData.file) && (
                  <View style={{marginVertical: 8}}>
                    <Text
                      style={{
                        fontSize: Platform.OS == 'ios' ? 14 : 16,
                        color: theme.white,
                        fontWeight: '600',
                      }}>
                      Attachment(s):
                    </Text>

                    <View style={{marginTop: 8}}>
                      <TouchableOpacity
                        onPress={() => {
                          setImageViewURI(responseData.file);

                          setImageViewStatus(!imageViewStatus);
                        }}>
                        <Image
                          resizeMode="cover"
                          onLoadStart={() => {
                            setImageLoader(true);
                          }}
                          onLoadEnd={() => {
                            setImageLoader(false);
                          }}
                          source={{
                            uri: responseData.file,
                          }}
                          style={{
                            width: '100%',
                            height: 200,
                            borderColor: theme.textColor,
                            borderWidth: 1,
                            borderRadius: 8,
                          }}
                        />
                        {Boolean(imageLoader) && (
                          <ActivityIndicator
                            size={'large'}
                            color={theme.secondryColor}
                            style={{
                              position: 'absolute',
                              left: 0,
                              top: 0,
                              right: 0,
                              bottom: 0,
                            }}
                          />
                        )}
                      </TouchableOpacity>
                    </View>
                  </View>
                )}
            </View>

            {renderImageView()}
          </ScrollView>
        ) : (
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: theme.themeColor,
            }}>
            <View style={{paddingHorizontal: 16, paddingVertical: 16}}>
              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 14 : 16,
                  fontWeight: '600',
                  color: theme.white,
                }}>
                No notification(s) found
              </Text>
            </View>
          </View>
        )
      ) : (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: theme.themeColor,
          }}>
          <ActivityIndicator size={'large'} color={theme.white} />
        </View>
      )}
    </>
  );
};

export default EchoAlert;
