import React from 'react';
import {ActivityIndicator, Text, View} from 'react-native';
import Modal from 'react-native-modal';
import styles from './styles';

const SuccessPopUp = ({res, successText, modal, onBackdropPress}) => {
  return (
    <Modal
      animationType="fade"
      transparent={true}
      isVisible={modal}
      onBackdropPress={onBackdropPress}
      backdropOpacity={0.7}
      useNativeDriver={true}>
      {res != undefined ? (
        <>
          <View style={styles.modalView1}>
            <Text style={styles.starttxt}>{successText} successfully</Text>
          </View>
        </>
      ) : (
        <ActivityIndicator color={'white'} />
      )}
    </Modal>
  );
};

export default SuccessPopUp;
