import React, {useContext, useEffect, useState} from 'react';
import {Platform, ScrollView, Text, TextInput, View} from 'react-native';
import Modal from 'react-native-modal';
import Icon from 'react-native-vector-icons/FontAwesome';
import BottomNav from '../../components/BottomNav';
import HeaderNav from '../../components/HeaderNav';
import Loading from '../../components/Loading';
import {UserDataContext} from '../../context/UserDataContext';
import axiosInstance from '../../utils/Apicall';
import theme from '../../utils/theme';
import Metrics from './metrics';
import RenderEcho from './partials/RenderEcho';
import styles from './style';

const LeaderBoard = ({navigation}) => {
  const [searchSubmitted, setSearchSubmitted] = useState(false);
  const [searchWith, setSearchWith] = useState('');
  const [loading, setLoading] = useState(true);
  const [people, setPeople] = useState([]);
  const [isMetrics, setMetrics] = useState(false);

  const {token} = useContext(UserDataContext);

  const fetchEchoLists = async (params = null) => {
    setLoading(true);

    let path = 'discoverPeople?user_sorting=desc';

    if (!!params) {
      path = path + '&' + params;
    }

    let response = await axiosInstance.get(path, {
      headers: {
        Authorization: token,
      },
    });

    if (response && response.data && response.data.keyword === 'success') {
      setPeople(response.data.data);
      setLoading(false);
    } else {
      setPeople([]);
      setLoading(false);
    }
  };

  useEffect(() => {
    fetchEchoLists();
  }, []);

  return (
    <>
      <HeaderNav
        title="Leaderboard"
        isBack="true"
        routeName="Leaderboard"
        onFilterApply={(query) => fetchEchoLists(query)}
      />

      <View style={styles.container}>
        {loading ? (
          <Loading color={theme.white} />
        ) : (
          <>
            <View style={{backgroundColor: theme.primaryColor}}>
              <View style={styles.searchSection}>
                <Icon
                  style={styles.searchIcon}
                  name="search"
                  size={16}
                  color="grey"
                />

                <TextInput
                  style={styles.input}
                  placeholder="Search"
                  placeholderTextColor="grey"
                  onChangeText={setSearchWith}
                  value={searchWith}
                />

                {searchSubmitted ? (
                  <Icon
                    style={styles.searchIcon}
                    name="close"
                    size={16}
                    color="grey"
                    onPress={() => {
                      setSearchWith('');

                      setSearchSubmitted(false);

                      fetchEchoLists();
                    }}
                  />
                ) : (
                  <Icon
                    style={styles.searchIcon}
                    name="arrow-right"
                    size={16}
                    color="grey"
                    onPress={() => {
                      setSearchSubmitted(true);

                      fetchEchoLists('searchWith=' + searchWith);
                    }}
                  />
                )}
              </View>
            </View>

            {people.length > 0 ? (
              <ScrollView
                keyboardShouldPersistTaps="handled"
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}>
                <View style={{paddingHorizontal: 16, paddingVertical: 16}}>
                  {people.map((item, index) => (
                    <RenderEcho
                      key={item.id}
                      item={item}
                      metrics={setMetrics}
                      screenName={'Leaderboard'}
                      onPress={() =>
                        navigation.navigate('DiscoverPeopleProfile', {
                          id: item.id,
                          loginStatus: item.login_status,
                        })
                      }
                    />
                  ))}
                </View>
              </ScrollView>
            ) : (
              <View
                style={{
                  flex: 1,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    fontSize: Platform.OS == 'ios' ? 12 : 14,
                    color: theme.white,
                  }}>
                  Does not match any results!
                </Text>
              </View>
            )}
          </>
        )}
      </View>

      <Modal
        isVisible={isMetrics}
        coverScreen={false}
        onBackdropPress={() => setMetrics(false)}>
        <Metrics onSelect={setMetrics} />
      </Modal>

      <BottomNav routeName="Discover People" />
    </>
  );
};

export default LeaderBoard;
