import axiosInstance from '../utils/Apicall';
import Errors from '../configs/Errors';
import Snackbar from 'react-native-snackbar';
import {TOKEN} from '../utils/constants';
import AsyncStorage from '@react-native-community/async-storage';

const getCompanyReport = async url => {
  let token = await AsyncStorage.getItem(TOKEN);
  return await axiosInstance
    .get(`income/${url}`, {
      headers: {
        Authorization: token,
      },
    })
    .then(res => {
      return res.data;
    })
    .catch(error => {
      Snackbar.show({
        duration: Snackbar.LENGTH_SHORT,
        text: Errors(error),
        backgroundColor: 'red',
      });

      return false; // here return empty object or null or false. based on your business logic
    });
};

const getQuotesLite = async symbol => {
  let token = await AsyncStorage.getItem(TOKEN);
  return await axiosInstance
    .get(`companyLastPricelite/${symbol}`, {
      headers: {
        Authorization: token,
      },
    })
    .then(res => {
      return res.data;
    })
    .catch(error => {
      Snackbar.show({
        duration: Snackbar.LENGTH_SHORT,
        text: Errors(error),
        backgroundColor: 'red',
      });

      return false; // here return empty object or null or false. based on your business logic
    });
};

// Place Order (Previous Endpoint) - orderCreatealpacatd

const placeOrder = async order => {
  let token = await AsyncStorage.getItem(TOKEN);
  return await axiosInstance
    .post(`order_create_alpaca_td`, order, {
      headers: {
        Authorization: token,
        'Content-Type': 'multipart/form-data',
      },
    })
    .then(res => {
      return res.data;
    })
    .catch(error => {
      Snackbar.show({
        duration: Snackbar.LENGTH_SHORT,
        text: Errors(error),
        backgroundColor: 'red',
      });

      return false; // here return empty object or null or false. based on your business logic
    });
};

const newPlaceOrder = async order => {
  let token = await AsyncStorage.getItem(TOKEN);
  return await axiosInstance
    .post(`orderCreatenew`, order, {
      headers: {
        Authorization: token,
        'content-type': 'multipart/form-data',
      },
    })
    .then(res => {
      return res.data;
    })
    .catch(() => {
      return false; // here return empty object or null or false. based on your business logic
    });
};

const getCompanyOverview = async symbol => {
  let token = await AsyncStorage.getItem(TOKEN);
  return await axiosInstance
    .get(`companyOverView/${symbol}`, {
      headers: {
        Authorization: token,
      },
    })
    .then(res => {
      return res.data;
    })
    .catch(error => {
      Snackbar.show({
        duration: Snackbar.LENGTH_SHORT,
        text: Errors(error),
        backgroundColor: 'red',
      });

      return false;
    });
};

const getInsides = async symbol => {
  let token = await AsyncStorage.getItem(TOKEN);
  return await axiosInstance
    .get(`companyInsiders/${symbol}`, {
      headers: {
        Authorization: token,
      },
    })
    .then(res => {
      return res.data;
    })
    .catch(error => {
      Snackbar.show({
        duration: Snackbar.LENGTH_SHORT,
        text: Errors(error),
        backgroundColor: 'red',
      });

      return false;
    });
};

const sendRatings = async body => {
  let token = await AsyncStorage.getItem(TOKEN);
  return await axiosInstance
    .post(`rating/create`, body, {
      headers: {
        Authorization: token,
        'content-type': 'multipart/form-data',
      },
    })
    .then(res => {
      return res.data;
    })
    .catch(error => {
      Snackbar.show({
        duration: Snackbar.LENGTH_SHORT,
        text: Errors(error),
        backgroundColor: 'red',
      });

      return false;
    });
};

const getRatings = async symbol => {
  let token = await AsyncStorage.getItem(TOKEN);
  return await axiosInstance
    .get(`rating/${symbol}`, {
      headers: {
        Authorization: token,
      },
    })
    .then(res => {
      return res.data;
    })
    .catch(() => {
      return false;
    });
};

const getCorpAction = async symbol => {
  let token = await AsyncStorage.getItem(TOKEN);
  return await axiosInstance
    .get(`corpAction/${symbol}`, {
      headers: {
        Authorization: token,
      },
    })
    .then(res => {
      return res.data;
    })
    .catch(() => {
      return false;
    });
};

const getCompanyNews = async symbol => {
  let token = await AsyncStorage.getItem(TOKEN);
  return await axiosInstance
    .get(`company_news/${symbol}`, {
      headers: {
        Authorization: token,
      },
    })
    .then(res => {
      return res.data;
    })
    .catch(error => {
      Snackbar.show({
        duration: Snackbar.LENGTH_SHORT,
        text: Errors(error),
        backgroundColor: 'red',
      });

      return false;
    });
};

export const company = {
  getCompanyReport,
  getQuotesLite,
  placeOrder,
  getCompanyOverview,
  getInsides,
  sendRatings,
  getRatings,
  getCorpAction,
  newPlaceOrder,
  getCompanyNews,
};
