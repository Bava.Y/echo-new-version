import React, {useContext, useEffect, useState} from 'react';
import {
  ActivityIndicator,
  LogBox,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Snackbar from 'react-native-snackbar';
import {
  VictoryAxis,
  VictoryBar,
  VictoryChart,
  VictoryContainer,
  VictoryGroup,
  VictoryLabel,
  VictoryLegend,
} from 'victory-native';
import {company} from '../../api';
import {MoneyFormat} from '../../containers/valueConversion';
import {UserDataContext} from '../../context/UserDataContext';
import theme from '../../utils/theme';
import styles from './style';

LogBox.ignoreLogs(['Require cycle:']);

const RevenueChart = ({symbol}) => {
  const {token} = useContext(UserDataContext);

  const [revenue, setRevenue] = useState([]);
  const [profit, setProfit] = useState([]);
  const [loading, setLoading] = useState(false);
  const [timeframe, setTimeframe] = useState('annual');

  const LIGHT_GREY = 'hsl(355, 20%, 90%)';

  useEffect(() => {
    let ac = new AbortController();
    getCompanyReports();
    return () => {
      ac.abort();
    };
  }, [timeframe]);

  const getCompanyReports = async () => {
    setLoading(true);

    const url =
      timeframe == 'annual'
        ? `${symbol}/${timeframe}/4`
        : `${symbol}/${timeframe}/6`;

    const res = await company.getCompanyReport(url, token);

    if (res) {
      if (res.keyword == 'success') {
        let revenue = [];
        let profit = [];
        res.data.income.map((company) => {
          revenue.push({
            x: company.report_date,
            y: company.cost_of_revenue,
          });

          profit.push({
            x: company.report_date,
            y: company.gross_profit,
          });
        });

        setProfit(profit);
        setRevenue(revenue);
      } else {
        Snackbar.show({
          duration: Snackbar.LENGTH_SHORT,
          text: res.message,
          backgroundColor: 'red',
        });
      }
    } else {
    }

    setLoading(false);
  };

  return (
    <>
      <View style={styles.chartdetail}>
        {!loading ? (
          <VictoryChart
            height={250}
            padding={{top: 40, bottom: 40, left: 15, right: 95}}
            containerComponent={<VictoryContainer responsive={true} />}>
            <VictoryAxis
              style={{marginLeft: 10}}
              tickLabelComponent={
                <VictoryLabel
                  angle={profit.length <= 4 ? 0 : -50}
                  dy={profit.length <= 4 ? 0 : 8}
                  dx={profit.length <= 4 ? 0 : -5}
                />
              }
              style={{
                axis: {
                  stroke: 'grey',
                },
                axisLabel: {
                  fill: LIGHT_GREY,
                  padding: 20,

                  fontSize: 10,
                  fontStyle: 'italic',
                },
                tickLabels: {
                  fill: LIGHT_GREY,
                  fontSize: profit.length <= 4 ? 10 : 10,
                },
              }}
            />
            <VictoryAxis
              orientation="right"
              dependentAxis
              tickFormat={(tick) => `${MoneyFormat(tick)}`}
              style={{
                axis: {
                  stroke: 'transparent',
                },
                tickLabels: {
                  fill: LIGHT_GREY,
                  fontSize: 10,
                },
                axisLabel: {
                  fill: 'red',
                  padding: 40,
                  fontSize: 10,
                  fontStyle: 'italic',
                },
                grid: {
                  fill: LIGHT_GREY,
                  stroke: LIGHT_GREY,
                  strokeWidth: 0.1,
                },
              }}
            />

            <VictoryGroup
              offset={15}
              colorScale={['#08d0df', '#0f676d']}
              domainPadding={{x: [10, 10], y: 10}}>
              <VictoryBar data={profit.reverse()} />
              <VictoryBar data={revenue.reverse()} />
            </VictoryGroup>
            <VictoryLegend
              x={25}
              y={0}
              orientation="horizontal"
              gutter={20}
              data={[
                {
                  name: 'Revenue',
                  symbol: {fill: '#08d0df'},
                  labels: {fill: 'white'},
                },
                {
                  name: 'Gross Profit',
                  symbol: {fill: '#0f676d'},
                  labels: {fill: 'white'},
                },
              ]}
            />
          </VictoryChart>
        ) : (
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
              height: 250,
            }}>
            <ActivityIndicator size="large" color="white" />
          </View>
        )}
        <View
          style={[
            {
              flexDirection: 'row',
              alignSelf: 'center',
              backgroundColor: theme.themeColor,
              padding: 3,
              marginTop: 15,
              borderRadius: 8,
            },
          ]}>
          <TouchableOpacity onPress={() => setTimeframe('interim')}>
            <View
              style={
                timeframe == 'interim' ? styles.durationactive : styles.duration
              }>
              <Text style={styles.durationtxt}>Interim</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => setTimeframe('annual')}>
            <View
              style={
                timeframe == 'annual' ? styles.durationactive : styles.duration
              }>
              <Text style={styles.durationtxt}>Annual</Text>
            </View>
          </TouchableOpacity>
        </View>
        <View style={styles.bottomiconsec}>
          <View style={{width: '30%'}}>
            <View style={[styles.bggreen, {alignSelf: 'flex-start'}]}>
              <Text style={styles.icontext}>VM</Text>
            </View>
          </View>
          <View style={{width: '40%'}}>
            <View
              style={[
                styles.bggreen,
                {alignSelf: 'flex-end', paddingHorizontal: 19},
              ]}>
              <Text style={styles.icontext}>SEC EDGAR</Text>
            </View>
          </View>
          <View style={{width: '30%'}}>
            <View
              style={[
                styles.bggreen,
                {alignSelf: 'flex-end', paddingHorizontal: 19},
              ]}>
              <Text style={styles.icontext}>E</Text>
            </View>
          </View>
        </View>
      </View>
    </>
  );
};

export default RevenueChart;
