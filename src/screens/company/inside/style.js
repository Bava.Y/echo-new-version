import {StyleSheet} from 'react-native';
import theme from '../../../utils/theme';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.themeColor,
  },
  container1: {
    flex: 1,
    flexDirection: 'column',
  },
  companydetail: {
    alignItems: 'center',
    marginVertical: 20,
  },
  smalltxt: {
    fontSize: 14,
    color: theme.white,
    padding: 5,
  },
  bigtxt: {
    fontSize: 20,
    color: theme.white,
    padding: 5,
  },
  mediumtxt: {
    fontSize: 16,
    color: theme.white,
    padding: 5,
  },
  successtxt: {
    fontSize: 16,
    color: 'green',
    padding: 5,
  },
  desctxt: {
    fontSize: 18,
    color: theme.white,
  },
  //chart details
  chartdetail: {
    backgroundColor: theme.primaryColor,
    borderRadius: 10,
    padding: 10,
    margin: 15,
  },
  dayview: {
    flexDirection: 'row',
    alignSelf: 'center',
    marginVertical: 10,
  },
  day: {
    color: 'white',
    paddingHorizontal: 10,
    textAlign: 'center',
  },
  active: {
    color: theme.secondryColor,
    paddingHorizontal: 3,
  },
  tabssec1: {
    backgroundColor: '#2e2e2e52',

    paddingHorizontal: 10,
    paddingVertical: 20,
  },
  tabhead: {
    flexDirection: 'row',
    alignSelf: 'center',
    backgroundColor: theme.themeColor,
    width: '100%',
    borderRadius: 10,
    padding: 5,
  },

  headingactivebtn: {
    backgroundColor: theme.secondryColor,

    paddingVertical: 5,
    borderRadius: 10,
    marginHorizontal: 4,
  },
  headingbtn: {
    backgroundColor: 'transparent',
    borderRadius: 10,
    paddingVertical: 5,
  },
  headingtxt: {
    color: 'white',
    textAlign: 'center',
    paddingHorizontal: 20,
  },
  headingactivetxt: {
    color: 'white',
    textAlign: 'center',
    paddingHorizontal: 20,
  },

  topsec: {
    marginHorizontal: 10,
    paddingHorizontal: 10,
    paddingVertical: 20,
  },
  toptabhead: {
    flexDirection: 'row',
    alignSelf: 'center',
    backgroundColor: '#0000004d',
    width: '100%',
    borderRadius: 10,
    padding: 5,
  },

  topheadingactivebtn: {
    backgroundColor: theme.white,

    paddingVertical: 5,
    borderRadius: 10,
    marginHorizontal: 4,
  },
  topheadingbtn: {
    backgroundColor: 'transparent',
    borderRadius: 10,
    paddingVertical: 5,
  },
  topheadingtxt: {
    color: theme.white,
    textAlign: 'center',
    paddingHorizontal: 20,
    fontWeight: 'bold',
  },
  topheadingactivetxt: {
    color: theme.secondryColor,
    textAlign: 'center',
    paddingHorizontal: 20,
    fontWeight: 'bold',
  },
  describeitem: {
    padding: 10,
  },
  describetitle: {
    fontSize: 18,
    color: theme.white,
  },
  describecontent: {
    fontSize: 16,
    color: theme.white,
  },
  companygroup: {
    flexDirection: 'row',
    padding: 15,
    alignItems: 'center',
    backgroundColor: theme.secondryColor,
  },
  groupname: {
    backgroundColor: theme.white,
    borderWidth: 1,
    borderColor: theme.secondryColor,
    padding: 10,
    borderRadius: 50,
    alignSelf: 'center',
    marginHorizontal: 30,
  },

  title: {
    fontSize: 16,
    color: theme.white,
  },
  lastpricevalue: {
    fontSize: 20,
    color: theme.white,
    fontWeight: 'bold',
    marginTop: 5,
  },
  todaychange: {
    color: theme.successGreen,
    fontSize: 20,
    fontWeight: 'bold',
    marginTop: 5,
  },
  stitle: {
    fontSize: 14,
    color: 'grey',
    textAlign: 'center',
  },
  pricevalue: {
    fontSize: 16,
    color: theme.white,
    textAlign: 'center',
  },
});

export default styles;
