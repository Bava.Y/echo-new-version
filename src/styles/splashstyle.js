import {Dimensions, StyleSheet} from 'react-native';
import theme from '../utils/theme';
const {height, width} = Dimensions.get('window');

let style = StyleSheet.create({
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover',
  },
  container: {
    backgroundColor: '#edfbfe',
    flex: 1,
    flexDirection: 'column',
    alignContent: 'center',
  },
  appLogo: {
    top: height * 0.2,
    height: 55,
    width: 55,
    tintColor: '#51397f',
    marginStart: width * 0.45,
    marginEnd: width * 0.45,
  },

  powerBy: {
    marginBottom: 10,
    fontFamily: theme.mediumFont,
  },
  view: {
    flex: 1,
    flexDirection: 'column-reverse',
    alignItems: 'center',
  },
});

export default style;
