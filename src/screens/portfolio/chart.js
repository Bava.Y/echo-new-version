import {useNavigation} from '@react-navigation/native';
import {useFocusEffect} from '@react-navigation/core';
import moment from 'moment';
import React, {useContext, useCallback} from 'react';
import {
  ActivityIndicator,
  Animated,
  Dimensions,
  Platform,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {
  VictoryAxis,
  VictoryBar,
  VictoryChart,
  VictoryContainer,
  VictoryGroup,
  VictoryLegend,
  VictoryLine,
  VictoryPie,
  VictoryTooltip,
  VictoryVoronoiContainer,
} from 'victory-native';
import {UserDataContext} from '../../context/UserDataContext';
import theme from '../../utils/theme';
import styles from './style';
import {portfolio} from '../../api';
import {
  CandlestickChart,
  LineChart,
  LineChartCursorCrosshair,
  LineChartCursorLine,
  LineChartCursorProps,
} from 'react-native-wagmi-charts';
import {GestureHandlerRootView} from 'react-native-gesture-handler';
import {color} from 'react-native-reanimated';
import Svg from 'react-native-svg';

const PortfolioChart = props => {
  // Props Variables
  const showAccountValue = props.showAccountValue || false;
  const totalReturns = props.totalReturns || false;
  const orderData = props.orderData || [];
  const chartRange = props?.chartRange ?? '1d';
  const lineChartData = props?.lineChartData ?? [];
  const lineChartDataMyStock = props?.lineChartDataMyStock ?? [];

  const lineChartDataPeople = props?.lineChartDataPeople ?? [];
  const alpachaAccountValue = props.alpachaAccountValue || null;
  const alpachaCashValue = props.alpachaCashValue || null;
  const alpachaPowerValue = props.alpachaPowerValue || null;
  const tdAccountValue = props.tdAccountValue || null;
  const tdCashValue = props.tdCashValue || null;
  const tdPowerValue = props.tdPowerValue || null;
  const returnsValue = props?.returnsValue ?? null;
  const returnsAmount = props?.returnsAmount ?? null;
  const pieLoading = props?.pieLoading ?? null;
  const from = props.from || null;

  // Context Variables
  const {
    alpacaConfigStatus,
    tdConfigStatus,
    myStockTimePeriod,
    setmyStockTimePeriod,
    portfolioTimePeriod,
    setPortfolioTimePeriod,
  } = useContext(UserDataContext);

  // Other Variables
  const loading = props.loading;
  const navigation = useNavigation();
  const chartTypes = ['line', 'pie', 'bar'];
  from == 'People' && chartTypes.shift();
  const deviceWidth = Dimensions.get('window').width;
  const deviceHeight = Dimensions.get('window').height;
  let scrollX = new Animated.Value(0);
  let position = Animated.divide(scrollX, deviceWidth);

  const renderAccountValue = () => {
    return (
      <>
        {alpacaConfigStatus && (
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <View
              style={{
                flex: 0.475,
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                backgroundColor: theme.themeColor,
                borderRadius: 4,
                marginRight: tdConfigStatus ? 0 : 40,
                paddingHorizontal: 8,
                paddingVertical: 6,
              }}>
              <Text style={styles.chartLabelTxt}>Total Portfolio</Text>

              <Text style={styles.chartValueTxt}>
                ${' '}
                {alpachaAccountValue
                  ? parseFloat(alpachaAccountValue).toFixed(2)
                  : 0.0}
              </Text>
            </View>

            <View
              style={{
                flex: 0.475,
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                backgroundColor: theme.themeColor,
                borderRadius: 4,
                marginRight: alpacaConfigStatus ? 0 : 40,
                paddingHorizontal: 8,
                paddingVertical: 6,
                right: alpacaConfigStatus ? 40 : 0,
              }}>
              <Text style={styles.chartLabelTxt}>Cash</Text>

              <Text style={styles.chartValueTxt}>
                ${' '}
                {alpachaCashValue
                  ? parseFloat(alpachaCashValue).toFixed(2)
                  : 0.0}
              </Text>
            </View>
          </View>
        )}

        {tdConfigStatus && (
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginTop: alpacaConfigStatus ? 8 : 0,
            }}>
            <View
              style={{
                flex: 0.475,
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                backgroundColor: theme.themeColor,
                borderRadius: 4,
                marginRight: tdConfigStatus ? 0 : 40,
                paddingHorizontal: 8,
                paddingVertical: 6,
              }}>
              <Text style={styles.chartLabelTxt}>TD Portfolio</Text>

              <Text style={styles.chartValueTxt}>
                $ {tdAccountValue ? parseFloat(tdAccountValue).toFixed(2) : 0.0}
              </Text>
            </View>

            <View
              style={{
                flex: 0.475,
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                backgroundColor: theme.themeColor,
                borderRadius: 4,
                marginRight: alpacaConfigStatus ? 0 : 40,
                paddingHorizontal: 8,
                paddingVertical: 6,
                right: alpacaConfigStatus ? 40 : 0,
              }}>
              <Text style={styles.chartLabelTxt}>Cash</Text>

              <Text style={styles.chartValueTxt}>
                $ {tdCashValue ? parseFloat(tdCashValue).toFixed(2) : 0.0}
              </Text>
            </View>
          </View>
        )}

        {(alpachaPowerValue || tdPowerValue) && (
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginTop: 8,
            }}>
            {alpachaPowerValue && (
              <View
                style={{
                  flex: tdPowerValue ? 0.475 : 1,
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  backgroundColor: theme.themeColor,
                  borderRadius: 4,
                  marginRight: tdPowerValue ? 0 : 40,
                  paddingHorizontal: 8,
                  paddingVertical: 6,
                }}>
                <Text style={styles.chartLabelTxt}>Buying Power</Text>

                <Text style={styles.chartValueTxt}>
                  ${' '}
                  {alpachaPowerValue
                    ? parseFloat(alpachaPowerValue).toFixed(2)
                    : 0.0}
                </Text>
              </View>
            )}

            {tdPowerValue && (
              <View
                style={{
                  flex: alpachaPowerValue ? 0.475 : 1,
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  backgroundColor: theme.themeColor,
                  borderRadius: 4,
                  marginRight: alpachaPowerValue ? 0 : 40,
                  paddingHorizontal: 8,
                  paddingVertical: 6,
                  right: alpachaPowerValue ? 40 : 0,
                }}>
                <Text style={styles.chartLabelTxt}>TD Buy PWR</Text>

                <Text style={styles.chartValueTxt}>
                  $ {tdPowerValue ? parseFloat(tdPowerValue).toFixed(2) : 0.0}
                </Text>
              </View>
            )}
          </View>
        )}
      </>
    );
  };

  const renderReturns = () => {
    return (
      <View
        style={{
          backgroundColor: theme.themeColor,
          borderRadius: 4,
          marginTop: 8,
          marginRight: 40,
          paddingHorizontal: 8,
          paddingVertical: 6,
          width: deviceWidth * 0.9,
        }}>
        <TouchableOpacity
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}
          onPress={() => {
            navigation.navigate('IndividualReturns', {from});
          }}>
          <View style={{flex: 0.325, justifyContent: 'center'}}>
            <Text style={styles.chartLabelTxt}>Total Returns</Text>
          </View>

          <View style={{flex: 0.125, justifyContent: 'center'}}>
            <Icon
              name={
                Boolean(returnsValue) && Math.sign(Number(returnsValue)) == -1
                  ? 'arrow-down'
                  : 'arrow-up'
              }
              color={
                Boolean(returnsValue) && Math.sign(Number(returnsValue)) == -1
                  ? theme.danger
                  : theme.successGreen
              }
              size={12}
              style={{paddingHorizontal: 4, alignSelf: 'center'}}
            />
          </View>

          <View style={{flex: 0.375, justifyContent: 'center'}}>
            <Text style={styles.chartValueTxt}>
              ${' '}
              {Boolean(returnsAmount)
                ? parseFloat(returnsAmount).toFixed(2)
                : '0.00'}
            </Text>
          </View>

          <View
            style={{
              flex: 0.196,
              justifyContent: 'center',
              alignItems: 'flex-end',
            }}>
            <Text style={styles.chartValueTxt}>
              {Boolean(returnsValue)
                ? parseFloat(returnsValue).toFixed(2)
                : '0.00'}{' '}
              %
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  };

  const renderTimePeriod = () => {
    return (
      <View style={styles.dayview}>
        <TouchableOpacity
          onPress={() => {
            // if (from === 'Stock') {
            //   changeChartRange(null);
            // }
            // // else{
            // //
            // // }
            changeChartRange('1d');
            setPortfolioTimePeriod('1d');
            setmyStockTimePeriod('1d');
          }}>
          <Text style={chartRange === '1d' || '' ? styles.active : styles.day}>
            1D
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            changeChartRange('1w');
            setPortfolioTimePeriod('1w');
            setmyStockTimePeriod('1w');
          }}>
          <Text style={chartRange === '1w' ? styles.active : styles.day}>
            1W
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            changeChartRange('1m');
            setPortfolioTimePeriod('1m');
            setmyStockTimePeriod('1m');
          }}>
          <Text style={chartRange === '1m' ? styles.active : styles.day}>
            1M
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            changeChartRange('3m');
            setPortfolioTimePeriod('3m');
            setmyStockTimePeriod('3m');
          }}>
          <Text style={chartRange === '3m' ? styles.active : styles.day}>
            3M
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            changeChartRange('6m');
            setPortfolioTimePeriod('6m');
            setmyStockTimePeriod('6m');
          }}>
          <Text style={chartRange === '6m' ? styles.active : styles.day}>
            6M
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            changeChartRange('1y');
            setPortfolioTimePeriod('1y');
            setmyStockTimePeriod('1y');
          }}>
          <Text style={chartRange === '1y' ? styles.active : styles.day}>
            1Y
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            changeChartRange('max');
            setPortfolioTimePeriod('max');
            setmyStockTimePeriod('max');
          }}>
          <Text style={chartRange === 'max' ? styles.active : styles.day}>
            MAX
          </Text>
        </TouchableOpacity>
      </View>
    );
  };

  const changeChartRange = timePeriod => {
    props?.onRangeChange(timePeriod);
  };

  const renderNoTradesFound = () => {
    return (
      <View
        style={{
          height:
            from == 'Portfolio' ? deviceHeight * 0.525 : deviceHeight * 0.4625,

          width: '100%',
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <Text
          style={{
            fontSize: Platform.OS == 'ios' ? 14 : 16,
            fontWeight: '400',
            color: theme.white,
          }}>
          <Text>No chart Found</Text>
        </Text>
      </View>
    );
  };

  const renderNoEchoingFound = () => {
    return (
      <TouchableOpacity
        onPress={() => {
          navigation.navigate('DiscoverPeopleList');
        }}
        style={{
          height:
            from == 'Portfolio' ? deviceHeight * 0.525 : deviceHeight * 0.4625,

          width: '100%',
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <Text
          style={{
            fontSize: Platform.OS == 'ios' ? 14 : 16,
            fontWeight: '400',
            color: theme.white,
          }}>
          <Text>You Are Not Echoing Anyone!</Text>
        </Text>
      </TouchableOpacity>
    );
  };

  const renderNoStocksFound = () => {
    return (
      <View
        style={{
          height:
            from == 'Portfolio' ? deviceHeight * 0.525 : deviceHeight * 0.4625,

          width: '100%',
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <View style={{marginBottom: 150}}>
          <Text
            style={{
              fontSize: Platform.OS == 'ios' ? 14 : 16,
              fontWeight: '400',
              color: theme.white,
            }}>
            <Text>No Stocks found</Text>
          </Text>
        </View>
      </View>
    );
  };
  const renderNoChartFound = () => {
    return (
      <View
        style={{
          height:
            from == 'Portfolio' ? deviceHeight * 0.525 : deviceHeight * 0.4625,

          width: '100%',
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <View style={{marginBottom: 150}}>
          <Text
            style={{
              fontSize: Platform.OS == 'ios' ? 14 : 16,
              fontWeight: '400',
              color: theme.white,
            }}>
            <Text>No Chart found</Text>
          </Text>
        </View>
        <View style={{paddingTop: 50}}>{renderTimePeriod()}</View>
      </View>
    );
  };
  const renderLoading = () => {
    return (
      <View
        style={{
          height:
            from == 'Portfolio' ? deviceHeight * 0.525 : deviceHeight * 0.4625,
          width: '100%',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <ActivityIndicator size="large" color={theme.secondryColor} />
      </View>
    );
  };

  return (
    <>
      <View style={styles.chartdetail}>
        <ScrollView
          onScroll={Animated.event([
            {nativeEvent: {contentOffset: {x: scrollX}}},
          ])}
          keyboardShouldPersistTaps={'handled'}
          horizontal={true}
          pagingEnabled={true}
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
          style={{paddingHorizontal: 12, paddingVertical: 12}}>
          {from === 'Portfolio' && (
            <View style={{width: deviceWidth * 0.95, flexDirection: 'row'}}>
              {loading == false ? (
                lineChartData?.length > 0 ? (
                  <View>
                    {Boolean(lineChartData) && lineChartData?.length > 0 && (
                      <GestureHandlerRootView>
                        <LineChart.Provider data={lineChartData}>
                          <LineChart width={deviceWidth * 0.90}>
                            <LineChart.Path
                              color={theme.secondryColor}
                              width={1.5}>
                              <LineChart.Tooltip
                                textStyle={{color: 'white', fontSize: 13}}
                              />
                              <LineChart.Gradient />
                            </LineChart.Path>
                            <LineChart.CursorLine color="white" />

                            <LineChart.Tooltip position="bottom">
                              <LineChart.DatetimeText
                                style={{
                                  borderRadius: 4,
                                  color: 'white',
                                  fontSize: 13,
                                  padding: 4,
                                }}
                              />
                            </LineChart.Tooltip>
                          </LineChart>
                        </LineChart.Provider>
                      </GestureHandlerRootView>
                    )}
                    {renderTimePeriod()}
                  </View>
                ) : (
                  renderNoChartFound()
                )
              ) : (
                //  from == 'Portfolio'||'Stock' &&
                renderLoading()
              )}
            </View>
          )}
          {from === 'Stock' && (
            <View style={{width: deviceWidth * 0.95, flexDirection: 'row'}}>
              {loading == false ? (
                lineChartDataMyStock?.length > 0 ? (
                  <View>
                    {from == 'Stock' && renderReturns()}

                    <GestureHandlerRootView>
                      <LineChart.Provider data={lineChartDataMyStock}>
                        <LineChart width={deviceWidth * 0.87}>
                          <LineChart.Path
                            color={theme.secondryColor}
                            width={1.5}>
                            <LineChart.Tooltip
                              textStyle={{color: 'white', fontSize: 13}}
                            />
                            <LineChart.Gradient />
                          </LineChart.Path>
                          <LineChart.CursorLine color="white" />

                          <LineChart.Tooltip position="bottom">
                            <LineChart.DatetimeText
                              style={{
                                borderRadius: 4,
                                color: 'white',
                                fontSize: 13,
                                padding: 4,
                              }}
                            />
                          </LineChart.Tooltip>
                        </LineChart>
                      </LineChart.Provider>
                    </GestureHandlerRootView>

                    {renderTimePeriod()}
                  </View>
                ) : (
                  renderNoChartFound()
                )
              ) : (
                //  from == 'Portfolio'||'Stock' &&
                renderLoading()
              )}
            </View>
          )}

          {from === 'Echoer' && (
            <View style={{width: deviceWidth * 0.95, flexDirection: 'row'}}>
              {loading == false ? (
                lineChartDataPeople?.length > 0 ? (
                  <View>
                    {from == 'Echoer' && renderReturns()}

                    <GestureHandlerRootView>
                      <LineChart.Provider data={lineChartDataPeople}>
                        <LineChart width={deviceWidth * 0.87}>
                          <LineChart.Path
                            color={theme.secondryColor}
                            width={1.5}>
                            <LineChart.Tooltip
                              textStyle={{color: 'white', fontSize: 13}}
                            />
                            <LineChart.Gradient />
                          </LineChart.Path>
                          <LineChart.CursorLine color="white" />

                          <LineChart.Tooltip position="bottom">
                            <LineChart.DatetimeText
                              style={{
                                borderRadius: 4,
                                color: 'white',
                                fontSize: 13,
                                padding: 4,
                              }}
                            />
                          </LineChart.Tooltip>
                        </LineChart>
                      </LineChart.Provider>
                    </GestureHandlerRootView>

                    {renderTimePeriod()}
                  </View>
                ) : (
                  renderNoChartFound()
                )
              ) : (
                //  from == 'Portfolio'||'Stock' &&
                renderLoading()
              )}
            </View>
          )}

          {from === 'Portfolio' && (
            <View style={{width: deviceWidth * 0.95, flexDirection: 'row'}}>
              {pieLoading == false ? (
                orderData.length != 0 ? (
                  <View>
                    {/* {renderReturns()} */}
                    <VictoryPie
                      height={280}
                      padding={{top: 0, bottom: 0, left: 0, right: 40}}
                      colorScale={[
                        '#10B4C0',
                        '#0EA0AB',
                        '#0B8993',
                        '#0B727A',
                        '#09585d',
                        '#074246',
                        '#042c2f',
                      ]}
                      sortKey="x"
                      sortOrder="ascending"
                      data={orderData}
                      labels={() => null}
                      radius={deviceWidth * 0.3}
                      style={{
                        labels: {
                          fontSize: Platform.OS == 'ios' ? 10 : 12,
                          fill: 'white',
                          fontWeight: '600',
                        },
                      }}
                      events={[
                        {
                          target: 'data',
                          eventHandlers: {
                            onPressIn: () => {
                              return [
                                {
                                  target: 'data',
                                  mutation: ({style}) => {
                                    return style.fill === theme.white
                                      ? null
                                      : {style: {fill: theme.white}};
                                  },
                                },
                                {
                                  target: 'labels',
                                  mutation: ({datum, text}) => {
                                    return text === datum.x
                                      ? null
                                      : {text: datum.x};
                                  },
                                },
                              ];
                            },
                          },
                        },
                      ]}
                    />

                    <VictoryLegend
                      height={60}
                      orientation='horizontal'
                      gutter={16}
                      itemsPerRow={2}
                      colorScale={[
                        '#10B4C0',
                        '#0EA0AB',
                        '#0B8993',
                        '#0B727A',
                        '#09585d',
                        '#074246',
                        '#042c2f',
                      ]}
                      sortKey="x"
                      sortOrder="ascending"
                      style={{
                        labels: {
                          fontSize: 10,
                          fill: 'white',
                          fontWeight: '600',
                        },
                      }}
                      data={orderData}
                    />
                  </View>
                ) : (
                  renderNoStocksFound()
                )
              ) : (
                //  from == 'Portfolio'||'Stock' &&
                renderLoading()
              )}
            </View>
          )}
          {from === 'Stock' && (
            <View style={{width: deviceWidth * 0.95, flexDirection: 'row'}}>
              {loading == false ? (
                orderData.length != 0 ? (
                  <View>
                    {renderReturns()}

                    <VictoryPie
                      height={280}
                      padding={{top: 0, bottom: 0, left: 0, right: 40}}
                      colorScale={[
                        '#10B4C0',
                        '#0EA0AB',
                        '#0B8993',
                        '#0B727A',
                        '#09585d',
                        '#074246',
                        '#042c2f',
                      ]}
                      sortKey="x"
                      sortOrder="ascending"
                      data={orderData}
                      labels={() => null}
                      radius={deviceWidth * 0.3}
                      style={{
                        labels: {
                          fontSize: Platform.OS == 'ios' ? 10 : 12,
                          fill: 'white',
                          fontWeight: '600',
                        },
                      }}
                      events={[
                        {
                          target: 'data',
                          eventHandlers: {
                            onPressIn: () => {
                              return [
                                {
                                  target: 'data',
                                  mutation: ({style}) => {
                                    return style.fill === theme.white
                                      ? null
                                      : {style: {fill: theme.white}};
                                  },
                                },
                                {
                                  target: 'labels',
                                  mutation: ({datum, text}) => {
                                    return text === datum.x
                                      ? null
                                      : {text: datum.x};
                                  },
                                },
                              ];
                            },
                          },
                        },
                      ]}
                    />

                    <VictoryLegend
                      height={60}
                      orientation='horizontal'
                      gutter={16}
                      itemsPerRow={2}
                      colorScale={[
                        '#10B4C0',
                        '#0EA0AB',
                        '#0B8993',
                        '#0B727A',
                        '#09585d',
                        '#074246',
                        '#042c2f',
                      ]}
                      sortKey="x"
                      sortOrder="ascending"
                      style={{
                        labels: {
                          fontSize: 10,
                          fill: 'white',
                          fontWeight: '600',
                        },
                      }}
                      data={orderData}
                    />
                  </View>
                ) : (
                  renderNoStocksFound()
                )
              ) : (
                //  from == 'Portfolio'||'Stock' &&
                renderLoading()
              )}
            </View>
          )}
          {from === 'People' && (
            <View style={{width: deviceWidth * 0.95, flexDirection: 'row'}}>
              {loading == false ? (
                orderData.length != 0 ? (
                  <View>
                    {/* {showAccountValue && renderAccountValue()} */}


                    <VictoryPie
                      height={280}
                      padding={{top: 0, bottom: 0, left: 0, right: 40}}
                      colorScale={[
                        '#10B4C0',
                        '#0EA0AB',
                        '#0B8993',
                        '#0B727A',
                        '#09585d',
                        '#074246',
                        '#042c2f',
                      ]}
                      sortKey="x"
                      sortOrder="ascending"
                      data={orderData}
                      labels={() => null}
                      radius={deviceWidth * 0.3}
                      style={{
                        labels: {
                          fontSize: Platform.OS == 'ios' ? 10 : 12,
                          fill: 'white',
                          fontWeight: '600',
                        },
                      }}
                      events={[
                        {
                          target: 'data',
                          eventHandlers: {
                            onPressIn: () => {
                              return [
                                {
                                  target: 'data',
                                  mutation: ({style}) => {
                                    return style.fill === theme.white
                                      ? null
                                      : {style: {fill: theme.white}};
                                  },
                                },
                                {
                                  target: 'labels',
                                  mutation: ({datum, text}) => {
                                    return text === datum.x
                                      ? null
                                      : {text: datum.x};
                                  },
                                },
                              ];
                            },
                          },
                        },
                      ]}
                    />

                    <VictoryLegend
                      height={60}
                      orientation='horizontal'
                      gutter={16}
                      itemsPerRow={2}
                      colorScale={[
                        '#10B4C0',
                        '#0EA0AB',
                        '#0B8993',
                        '#0B727A',
                        '#09585d',
                        '#074246',
                        '#042c2f',
                      ]}
                      sortKey="x"
                      sortOrder="ascending"
                      style={{
                        labels: {
                          fontSize: 10,
                          fill: 'white',
                          fontWeight: '600',
                        },
                      }}
                      data={orderData}
                    />
                  </View>
                ) : (
                  renderNoStocksFound()
                )
              ) : (
                //  from == 'Portfolio'||'Stock' &&
                renderLoading()
              )}
            </View>
          )}

          <View style={{width: deviceWidth * 0.95, flexDirection: 'row'}}>
            {!loading ? (
              <View>
                {showAccountValue && renderAccountValue()}

                {renderReturns()}

                <VictoryChart
                  height={280}
                  padding={{top: 40, bottom: 40, left: 0, right: 40}}>
                  <VictoryAxis
                    style={{
                      axis: {
                        stroke: 'transparent',
                      },
                      axisLabel: {
                        fill: 'transparent',
                      },
                      ticks: {stroke: 'transparent'},
                      tickLabels: {
                        fill: 'transparent',
                      },
                    }}
                  />

                  <VictoryAxis
                    dependentAxis
                    style={{
                      axis: {
                        stroke: 'transparent',
                      },
                      tickLabels: {
                        fill: 'transparent',
                      },
                      grid: {
                        fill: 'transparent',
                        stroke: 'transparent',
                      },
                    }}
                  />

                  <VictoryGroup
                    offset={16}
                    colorScale={['#08d0df', '#0f676d']}
                    domainPadding={{x: [8, 8], y: 8}}>
                    <VictoryBar
                      data={[
                        {x: 1, y: Math.random() * (1 - 0) + 0},
                        {x: 2, y: Math.random() * (1 - 0) + 0},
                        {x: 3, y: Math.random() * (1 - 0) + 0},
                        {x: 4, y: Math.random() * (1 - 0) + 0},
                        {x: 5, y: Math.random() * (1 - 0) + 0},
                      ]}
                    />
                    <VictoryBar
                      data={[
                        {x: 1, y: Math.random() * (1 - 0) + 0},
                        {x: 2, y: Math.random() * (1 - 0) + 0},
                        {x: 3, y: Math.random() * (1 - 0) + 0},
                        {x: 4, y: Math.random() * (1 - 0) + 0},
                        {x: 5, y: Math.random() * (1 - 0) + 0},
                      ]}
                    />
                  </VictoryGroup>

                  <VictoryLegend
                    x={deviceWidth * 0.25}
                    y={8}
                    orientation='horizontal'
                    gutter={16}
                    data={[
                      {
                        name: 'Apple',
                        symbol: {fill: '#08d0df'},
                        labels: {fill: 'white'},
                      },
                      {
                        name: 'Disney',
                        symbol: {fill: '#0f676d'},
                        labels: {fill: 'white'},
                      },
                    ]}
                  />
                </VictoryChart>

                {renderTimePeriod()}
              </View>
            ) : (
              renderLoading()
            )}
          </View>
        </ScrollView>
      </View>

      <View
        style={[
          {flexDirection: 'row', alignSelf: 'center', marginVertical: 16},
        ]}>
        {chartTypes?.map((_, i) => {
          let backgroundColor = position.interpolate({
            inputRange: [i - 1, i, i + 1],
            outputRange: [theme.white, theme.secondryColor, theme.white],
            extrapolate: 'clamp',
          });

          return (
            <Animated.View
              key={i}
              style={{
                width: 8,
                height: 8,
                backgroundColor,
                borderRadius: 8 / 2,
                marginHorizontal: 8,
              }}
            />
          );
        })}
      </View>
    </>
  );
};

export default PortfolioChart;
