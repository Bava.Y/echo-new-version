import moment from 'moment';
import React, {useContext, useEffect, useState} from 'react';
import {
  ActivityIndicator,
  Image,
  KeyboardAvoidingView,
  Linking,
  Platform,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import Snackbar from 'react-native-snackbar';
import Icon from 'react-native-vector-icons/FontAwesome';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {dashboard} from '../../api';
import LITERALS from '../../assets/strings';
import BottomNav from '../../components/BottomNav';
import HeaderNav from '../../components/HeaderNav';
import {UserDataContext} from '../../context/UserDataContext';
import theme from '../../utils/theme';
import styles from './styles';
import Ticker from './ticker';

const News = ({navigation}) => {
  const [searchtext, setSearchtext] = useState(null);
  const {token, tickerData} = useContext(UserDataContext);
  const [newsData, setNewsData] = useState(null);
  const [helperNewsData, setHelperNewsData] = useState(null);
  const [closeStatus, setCloseStatus] = useState(false);

  useEffect(() => {
    let isFocus = true;

    getNews();

    return () => {
      isFocus = false;
    };
  }, []);

  const getNews = async () => {
    const res = await dashboard.getNews();

    if (res) {
      if (res.keyword == 'success') {
        if (res.data.length != 0) {
          let helperArray = [];

          res.data.map((lol) => {
            const helperObject = {...lol, imageLoadingStatus: true};

            helperArray.push(helperObject);
          });

          setNewsData(helperArray);
          setHelperNewsData(helperArray);
        } else {
          setNewsData(res.data);
          setHelperNewsData(res.data);
        }
      } else {
        setNewsData([]);
        setHelperNewsData([]);

        Snackbar.show({
          duration: Snackbar.LENGTH_SHORT,
          text: res.message,
          backgroundColor: 'red',
        });
      }
    } else {
      setNewsData([]);
      setHelperNewsData([]);
    }
  };

  const handleSearch = (txt) => {
    if (txt && newsData.length != 0) {
      setSearchtext(txt);

      setCloseStatus(true);

      const filterArray = newsData.filter((lol) => {
        return lol.headline.toLowerCase().match(txt.toLowerCase());
      });

      setNewsData(filterArray);
    } else {
      alert();

      setCloseStatus(false);

      setSearchtext(null);

      setNewsData(helperNewsData);
    }
  };

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : null}
      keyboardVerticalOffset={Platform.OS === 'ios' ? 40 : 0}
      style={{flex: 1, backgroundColor: theme.themeColor}}>
      <HeaderNav title={LITERALS.dashboard} isBack={false} />

      <Ticker data={tickerData} />

      <ScrollView
        keyboardShouldPersistTaps={'handled'}
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}>
        <View style={styles.tabssec1}>
          <View style={styles.tabhead}>
            <View style={[styles.headingbtn, styles.tabheadlabel]}>
              <TouchableOpacity
                onPress={() => navigation.navigate('Dashboard')}>
                <Text style={styles.headingtxt}>Trading</Text>
              </TouchableOpacity>
            </View>

            <View style={[styles.headingbtn, styles.tabheadlabel]}>
              <TouchableOpacity onPress={() => navigation.navigate('Trending')}>
                <Text style={styles.headingactivetxt}>Trending</Text>
              </TouchableOpacity>
            </View>

            <View style={[styles.headingactivebtn, styles.tabheadlabel]}>
              <TouchableOpacity onPress={() => navigation.navigate('News')}>
                <Text style={[styles.headingtxt]}>News</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>

        <View style={styles.newsSec}>
          <View style={{flex: 1, alignSelf: 'flex-end'}}>
            <Ionicons name="book" size={20} color="white" />
          </View>

          <View style={styles.recenttrendingsearchSection}>
            <Icon
              style={styles.searchIcon}
              name="search"
              size={16}
              color="white"
            />

            <TextInput
              style={styles.searchtext}
              placeholder="Search"
              placeholderTextColor="white"
              onChangeText={(text) => {
                handleSearch(text);
              }}
              value={searchtext}
            />

            {closeStatus && (
              <Icon
                style={styles.searchIcon}
                name="times"
                size={16}
                color="white"
                onPress={() => {
                  setCloseStatus(false);

                  setSearchtext(null);

                  setNewsData(helperNewsData);
                }}
              />
            )}
          </View>

          {newsData ? (
            newsData.length != 0 ? (
              newsData.map((lol, index) => (
                <View
                  key={index}
                  style={newsData.length - 1 != index && styles.singlenews}>
                  <View
                    key={index}
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                    }}>
                    <View
                      style={{
                        width: '25%',
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      <Image
                        onLoadEnd={() => {
                          let helperArray = [...newsData];

                          helperArray[index].imageLoadingStatus = false;

                          setNewsData(helperArray);
                          setHelperNewsData(helperArray);
                        }}
                        source={lol.image ? {uri: lol.image} : DISCOVER}
                        style={{
                          width: 60,
                          height: 60,
                          borderRadius: 4,
                          elevation: 4,
                        }}
                      />

                      {lol.imageLoadingStatus && (
                        <ActivityIndicator
                          color={theme.secondryColor}
                          style={{
                            position: 'absolute',
                          }}
                        />
                      )}
                    </View>

                    <View style={{width: '75%', justifyContent: 'center'}}>
                      <TouchableOpacity
                        onPress={async () => {
                          Linking.canOpenURL(lol.qmUrl)
                            .then((checkURL) => {
                              if (checkURL) {
                                Linking.openURL(lol.qmUrl);
                              } else {
                                Snackbar.show({
                                  backgroundColor: 'red',
                                  text: 'Unable to open the url',
                                  duration: Snackbar.LENGTH_LONG,
                                });
                              }
                            })
                            .catch(() => {
                              Snackbar.show({
                                backgroundColor: 'red',
                                text: 'Facing some issues in open the url',
                                duration: Snackbar.LENGTH_LONG,
                              });
                            });
                        }}>
                        <Text style={styles.newstitle}>{lol.headline}</Text>
                      </TouchableOpacity>

                      <Text style={styles.updatetime}>
                        {moment(lol.datetime).startOf('day').fromNow()}
                      </Text>
                    </View>
                  </View>
                </View>
              ))
            ) : (
              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginVertical: 24,
                }}>
                <Text
                  style={{
                    fontSize: Platform.OS == 'ios' ? 12 : 14,
                    color: theme.white,
                    paddingHorizontal: 8,
                  }}>
                  No news feed(s) found!
                </Text>
              </View>
            )
          ) : (
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
                marginVertical: 24,
              }}>
              <ActivityIndicator size={24} color={theme.secondryColor} />
              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 14 : 16,
                  color: theme.white,
                  paddingHorizontal: 8,
                }}>
                Loading news
              </Text>
            </View>
          )}
        </View>
      </ScrollView>

      <BottomNav routeName="Dashboard" />
    </KeyboardAvoidingView>
  );
};

export default News;
