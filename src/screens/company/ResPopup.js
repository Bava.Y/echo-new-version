import React from 'react';
import {Platform} from 'react-native';
import {ActivityIndicator, StyleSheet, Text, View} from 'react-native';
import Modal from 'react-native-modal';
import theme from '../../utils/theme';

const ResPopUp = ({res, modal, loading}) => {
  return (
    <Modal
      animationType="fade"
      transparent={true}
      isVisible={modal}
      backdropOpacity={0.5}>
      {!loading ? (
        <View style={styles.modalView1}>
          <Text style={styles.starttxt}>{res.message}</Text>
        </View>
      ) : (
        <ActivityIndicator color="white" size="large" />
      )}
    </Modal>
  );
};

export default ResPopUp;

const styles = StyleSheet.create({
  modalView1: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#2e2e2e',
    borderRadius: 8,
    margin: 8,
    padding: 16,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  starttxt: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    color: theme.white,
  },
});
