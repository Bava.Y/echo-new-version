const LITERALS = {
  dashboard: 'Dashboard',
  continue: 'Continue',
  sendTo: `Send to`,
  dontreceive: `Did not receive an OTP?`,
  sendAgain: `Send OTP again`,
  verify: 'Verify your mobile number',
};

export default LITERALS;
