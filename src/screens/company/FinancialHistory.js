import React, {useContext, useState} from 'react';
import {
  Dimensions,
  KeyboardAvoidingView,
  Modal,
  Platform,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {
  VictoryArea,
  VictoryAxis,
  VictoryBar,
  VictoryChart,
  VictoryGroup,
  VictoryLegend,
} from 'victory-native';
import BottomNav from '../../components/BottomNav';
import HeaderNav from '../../components/HeaderNav';
import {UserDataContext} from '../../context/UserDataContext';
import theme from '../../utils/theme';
import Ticker from '../Dashboard/ticker';
import styles from './style';

const FinancialHistory = ({route}) => {
  const companyName = route.params.companyName || null;

  const [value, onChangeText] = useState();
  const [show, setShow] = useState(true);
  const [view, setView] = useState('annual');
  const [modalVisible, setModalVisible] = useState(false);

  const LIGHT_GREY = 'hsl(355, 20%, 90%)';
  const deviceWidth = Dimensions.get('window').width;

  const {tickerData} = useContext(UserDataContext);

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : null}
      keyboardVerticalOffset={Platform.OS === 'ios' ? 40 : 0}
      style={{flex: 1, backgroundColor: theme.themeColor}}>
      <HeaderNav title={companyName} isBack="true" routeName="Company" />

      <View style={styles.container}>
        <Ticker data={tickerData} />

        <View style={styles.recentsearch}>
          <View style={styles.recenttrendingsearchSection}>
            <Icon
              style={styles.searchIcon}
              name="search"
              size={16}
              color="grey"
            />

            <TextInput
              style={styles.searchtext}
              placeholder="Comparison tool"
              placeholderTextColor="white"
              onChangeText={(text) => onChangeText(text)}
              value={value}
            />
          </View>
        </View>

        <View style={styles.chartdetail}>
          <View style={styles.topiconsec}>
            <Text
              style={{
                width: '85%',
                fontSize: Platform.OS == 'ios' ? 16 : 18,
                color: theme.white,
                fontWeight: '600',
              }}>
              Financial History
            </Text>

            <TouchableOpacity
              style={{
                width: '15%',
                justifyContent: 'center',
                alignItems: 'flex-end',
              }}
              onPress={() => setShow(!show)}>
              <View
                style={{
                  backgroundColor: theme.secondryColor,
                  borderRadius: 4,
                  paddingVertical: 8,
                  paddingHorizontal: 8,
                }}>
                <Icon
                  name={show ? 'line-chart' : 'bar-chart'}
                  size={16}
                  color="white"
                />
              </View>
            </TouchableOpacity>
          </View>

          {/* Line chart */}
          {show ? (
            <View style={{marginVertical: 8}}>
              <VictoryChart
                width={deviceWidth * 0.875}
                height={200}
                padding={{top: 40, bottom: 0, left: 0, right: 40}}>
                <VictoryAxis
                  orientation="right"
                  dependentAxis
                  style={{
                    axis: {
                      stroke: 'transparent',
                    },
                    tickLabels: {
                      fill: LIGHT_GREY,
                      fontSize: 10,
                    },
                    axisLabel: {
                      fill: 'red',
                      fontSize: 10,
                      fontStyle: 'italic',
                    },
                    grid: {
                      fill: LIGHT_GREY,
                      stroke: LIGHT_GREY,
                      strokeWidth: 0.1,
                    },
                  }}
                />

                <VictoryGroup
                  offset={16}
                  colorScale={['#08d0df', '#0f676d']}
                  domainPadding={{x: [8, 8], y: 8}}>
                  <VictoryBar
                    data={[
                      {x: 1, y: Math.random() * (1 - 0) + 0},
                      {x: 2, y: Math.random() * (1 - 0) + 0},
                      {x: 3, y: Math.random() * (1 - 0) + 0},
                      {x: 4, y: Math.random() * (1 - 0) + 0},
                      {x: 5, y: Math.random() * (1 - 0) + 0},
                    ]}
                  />

                  <VictoryBar
                    data={[
                      {x: 1, y: Math.random() * (1 - 0) + 0},
                      {x: 2, y: Math.random() * (1 - 0) + 0},
                      {x: 3, y: Math.random() * (1 - 0) + 0},
                      {x: 4, y: Math.random() * (1 - 0) + 0},
                      {x: 5, y: Math.random() * (1 - 0) + 0},
                    ]}
                  />
                </VictoryGroup>

                <VictoryLegend
                  orientation="horizontal"
                  gutter={16}
                  data={[
                    {
                      name: 'Revenue',
                      symbol: {fill: '#08d0df'},
                      labels: {fill: 'white'},
                    },
                    {
                      name: 'Gross Profit',
                      symbol: {fill: '#0f676d'},
                      labels: {fill: 'white'},
                    },
                  ]}
                />
              </VictoryChart>
            </View>
          ) : (
            <View style={{marginVertical: 8}}>
              <VictoryChart
                width={deviceWidth * 0.875}
                height={200}
                padding={{top: 20, bottom: 0, left: 0, right: 40}}>
                <VictoryAxis
                  orientation="right"
                  dependentAxis
                  style={{
                    axis: {
                      stroke: 'transparent',
                    },
                    tickLabels: {
                      fill: LIGHT_GREY,
                      fontSize: 10,
                    },
                    axisLabel: {
                      fill: 'red',
                      fontSize: 10,
                      fontStyle: 'italic',
                    },
                    grid: {
                      fill: LIGHT_GREY,
                      stroke: LIGHT_GREY,
                      strokeWidth: 0.1,
                    },
                  }}
                />

                <VictoryArea
                  interpolation="natural"
                  style={{
                    data: {
                      fill: theme.secondryColor,
                      fillOpacity: 0.5,
                      stroke: theme.secondryColor,
                      strokeWidth: 2,
                    },
                  }}
                  data={[
                    {x: 1, y: Math.random() * (1 - 0) + 0},
                    {x: 2, y: Math.random() * (1 - 0) + 0},
                    {x: 3, y: Math.random() * (1 - 0) + 0},
                    {x: 4, y: Math.random() * (1 - 0) + 0},
                    {x: 5, y: Math.random() * (1 - 0) + 0},
                  ]}
                />
              </VictoryChart>
            </View>
          )}

          <View
            style={[
              styles.selectdata,
              {marginVertical: 16, alignSelf: 'center'},
            ]}>
            <TouchableOpacity
              style={{width: '40%'}}
              onPress={() => {
                setView('interim');
              }}>
              <View
                style={
                  view == 'interim' ? styles.durationactive : styles.duration
                }>
                <Text style={styles.durationtxt}>Interim</Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              style={{width: '40%'}}
              onPress={() => {
                setView('annual');
              }}>
              <View
                style={
                  view == 'annual' ? styles.durationactive : styles.duration
                }>
                <Text style={styles.durationtxt}>Annual</Text>
              </View>
            </TouchableOpacity>
          </View>

          <View style={[styles.bottomiconsec, {marginTop: 8}]}>
            <View style={{width: '30%', justifyContent: 'center'}}>
              <TouchableOpacity
                style={{
                  alignSelf: 'flex-start',
                  backgroundColor: theme.secondryColor,
                  paddingHorizontal: 16,
                  paddingVertical: 8,
                }}
                onPress={() => {
                  setModalVisible(!modalVisible);
                }}>
                <Text style={styles.icontext}>VM</Text>
              </TouchableOpacity>
            </View>

            <View style={{width: '40%', justifyContent: 'center'}}>
              <TouchableOpacity
                style={{
                  alignSelf: 'center',
                  backgroundColor: theme.secondryColor,
                  paddingHorizontal: 16,
                  paddingVertical: 8,
                }}
                onPress={() => {
                  setModalVisible(!modalVisible);
                }}>
                <Text style={styles.icontext}>SEC EDGAR</Text>
              </TouchableOpacity>
            </View>

            <View style={{width: '30%', justifyContent: 'center'}}>
              <TouchableOpacity
                style={{
                  alignSelf: 'flex-end',
                  backgroundColor: theme.secondryColor,
                  paddingHorizontal: 16,
                  paddingVertical: 8,
                }}
                onPress={() => {
                  setModalVisible(!modalVisible);
                }}>
                <Text style={styles.icontext}>E</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>

      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(!modalVisible);
        }}>
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View
            style={{
              width: '75%',
              backgroundColor: theme.primaryColor,
              borderColor: theme.secondryColor,
              borderWidth: 1,
              borderRadius: 8,
              margin: 16,
              padding: 16,
              shadowColor: '#000',
              shadowOffset: {
                width: 0,
                height: 2,
              },
              shadowOpacity: 0.25,
              shadowRadius: 4,
              elevation: 4,
            }}>
            <Text
              style={{
                fontSize: Platform.OS == 'ios' ? 14 : 16,
                color: theme.white,
                fontWeight: 'bold',
                textAlign: 'center',
              }}>
              This feature is comming soon
            </Text>

            <TouchableOpacity
              style={{
                alignSelf: 'flex-end',
                backgroundColor: theme.secondryColor,
                borderRadius: 5,
                marginTop: 16,
                paddingHorizontal: 8,
                paddingVertical: 4,
              }}
              onPress={() => {
                setModalVisible(!modalVisible);
              }}>
              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 12 : 14,
                  color: theme.white,
                  textAlign: 'center',
                }}>
                Close
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>

      <BottomNav routeName="Dashboard" />
    </KeyboardAvoidingView>
  );
};

export default FinancialHistory;
