import React, {useContext, useState} from 'react';
import {ScrollView, Text, View} from 'react-native';
import {profile} from '../../../api';
import {UserDataContext} from '../../../context/UserDataContext';
import Switch from './../components/switch';
import styles from './styles';

const Social = ({preferences}) => {
  // Props Variables
  const [social, setSocial] = useState(
    preferences.social_feature != undefined
      ? Boolean(preferences.social_feature)
      : false,
  );
  const [media, setMedia] = useState(
    preferences.social_media != undefined
      ? Boolean(preferences.social_media)
      : false,
  );
  const [myMedia, setMyMedia] = useState(
    preferences.exclude_socialmedia != undefined
      ? Boolean(preferences.exclude_socialmedia)
      : false,
  );
  const [include, setInclude] = useState(
    preferences.include_socialmedia != undefined
      ? Boolean(preferences.include_socialmedia)
      : false,
  );
  const [channels, setChannels] = useState(
    preferences.channel != undefined ? Boolean(preferences.channel) : false,
  );
  const [memberShip, setMembership] = useState(
    preferences.channel_membership != undefined
      ? Boolean(preferences.channel_membership)
      : false,
  );
  const [leadership, setLeadership] = useState(
    preferences.channel_leadership != undefined
      ? Boolean(preferences.channel_leadership)
      : false,
  );
  const [chatter, setChatter] = useState(
    preferences.chatter != undefined ? Boolean(preferences.chatter) : false,
  );
  const [newsFeed, setNewsFeed] = useState(
    preferences.chatter_newsfeed != undefined
      ? Boolean(preferences.chatter_newsfeed)
      : false,
  );

  // Context Variables
  const {authUserData, token} = useContext(UserDataContext);

  // Other Variables
  let formData = new FormData();

  const toggleSocial = async (current) => {
    setSocial((previousState) => !previousState);

    if (current == false) {
      setMedia(false);
      setMyMedia(false);
      setInclude(false);
      setChannels(false);
      setLeadership(false);
      setMembership(false);
      setChatter(false);
      setNewsFeed(false);
    }

    if (current == true) {
      setMedia(true);
      setMyMedia(true);
      setInclude(true);
      setChannels(true);
      setLeadership(true);
      setMembership(true);
      setChatter(true);
      setNewsFeed(true);
    }

    let status = current == false ? 0 : 1;

    formData && formData.append('status', status);
    formData && formData.append('type', 'social_feature');
    formData && formData.append('id', authUserData.user_id);

    await profile.updatePreference(token, formData);

    formData && formData.append('status', status);
    formData && formData.append('type', 'social_media');
    formData && formData.append('id', authUserData.user_id);

    await profile.updatePreference(token, formData);

    formData && formData.append('status', status);
    formData && formData.append('type', 'exclude_socialmedia');
    formData && formData.append('id', authUserData.user_id);

    await profile.updatePreference(token, formData);

    formData && formData.append('status', status);
    formData && formData.append('type', 'include_socialmedia');
    formData && formData.append('id', authUserData.user_id);

    await profile.updatePreference(token, formData);

    formData && formData.append('status', status);
    formData && formData.append('type', 'channel');
    formData && formData.append('id', authUserData.user_id);

    await profile.updatePreference(token, formData);

    formData && formData.append('status', status);
    formData && formData.append('type', 'channel_leadership');
    formData && formData.append('id', authUserData.user_id);

    await profile.updatePreference(token, formData);

    formData && formData.append('status', status);
    formData && formData.append('type', 'channel_membership');
    formData && formData.append('id', authUserData.user_id);

    await profile.updatePreference(token, formData);

    formData && formData.append('status', status);
    formData && formData.append('type', 'chatter');
    formData && formData.append('id', authUserData.user_id);

    await profile.updatePreference(token, formData);

    formData && formData.append('status', status);
    formData && formData.append('type', 'chatter_newsfeed');
    formData && formData.append('id', authUserData.user_id);

    await profile.updatePreference(token, formData);
  };

  const toggleMedia = async (current) => {
    setMedia((previousState) => !previousState);

    let status = current == false ? 0 : 1;

    if (current == false) {
      setMyMedia(false);
      setInclude(false);
    }

    if (current == true) {
      setMyMedia(true);
      setInclude(true);
    }

    formData && formData.append('status', status);
    formData && formData.append('type', 'social_media');
    formData && formData.append('id', authUserData.user_id);

    await profile.updatePreference(token, formData);

    formData && formData.append('status', status);
    formData && formData.append('type', 'exclude_socialmedia');
    formData && formData.append('id', authUserData.user_id);

    await profile.updatePreference(token, formData);

    formData && formData.append('status', status);
    formData && formData.append('type', 'include_socialmedia');
    formData && formData.append('id', authUserData.user_id);

    await profile.updatePreference(token, formData);
  };

  const toggleMyMedia = async (current) => {
    setMyMedia((previousState) => !previousState);

    let status = current == false ? 0 : 1;

    formData && formData.append('status', status);
    formData && formData.append('type', 'exclude_socialmedia');
    formData && formData.append('id', authUserData.user_id);

    await profile.updatePreference(token, formData);
  };

  const toggleInclude = async (current) => {
    setInclude((previousState) => !previousState);

    let status = current == false ? 0 : 1;

    formData && formData.append('status', status);
    formData && formData.append('type', 'include_socialmedia');
    formData && formData.append('id', authUserData.user_id);

    await profile.updatePreference(token, formData);
  };

  const toggleChannels = async (current) => {
    setChannels((previousState) => !previousState);

    let status = current == false ? 0 : 1;

    if (current == false) {
      setLeadership(false);
      setMembership(false);
    }

    if (current == true) {
      setLeadership(true);
      setMembership(true);
    }

    formData && formData.append('status', status);
    formData && formData.append('type', 'channel');
    formData && formData.append('id', authUserData.user_id);

    await profile.updatePreference(token, formData);

    formData && formData.append('status', status);
    formData && formData.append('type', 'channel_leadership');
    formData && formData.append('id', authUserData.user_id);

    await profile.updatePreference(token, formData);

    formData && formData.append('status', status);
    formData && formData.append('type', 'channel_membership');
    formData && formData.append('id', authUserData.user_id);

    await profile.updatePreference(token, formData);
  };

  const toggleLeader = async (current) => {
    setLeadership((previousState) => !previousState);

    let status = current == false ? 0 : 1;

    formData && formData.append('status', status);
    formData && formData.append('type', 'channel_leadership');
    formData && formData.append('id', authUserData.user_id);

    await profile.updatePreference(token, formData);
  };

  const toggleMember = async (current) => {
    setMembership((previousState) => !previousState);

    let status = current == false ? 0 : 1;

    formData && formData.append('status', status);
    formData && formData.append('type', 'channel_membership');
    formData && formData.append('id', authUserData.user_id);

    await profile.updatePreference(token, formData);
  };

  const toggleChatter = async (current) => {
    setChatter((previousState) => !previousState);

    let status = current == false ? 0 : 1;

    if (current == false) {
      setNewsFeed(false);
    }

    if (current == true) {
      setNewsFeed(true);
    }

    formData && formData.append('status', status);
    formData && formData.append('type', 'chatter');
    formData && formData.append('id', authUserData.user_id);

    await profile.updatePreference(token, formData);

    formData && formData.append('status', status);
    formData && formData.append('type', 'chatter_newsfeed');
    formData && formData.append('id', authUserData.user_id);

    await profile.updatePreference(token, formData);
  };

  const toggleNewsFeed = async (current) => {
    setNewsFeed((previousState) => !previousState);

    setChatter((previousState) => !previousState);

    let status = current == false ? 0 : 1;

    if (current == false) {
      setChatter(false);
    }

    if (current == true) {
      setChatter(true);
    }

    formData && formData.append('status', status);
    formData && formData.append('type', 'chatter_newsfeed');
    formData && formData.append('id', authUserData.user_id);

    await profile.updatePreference(token, formData);

    formData && formData.append('status', status);
    formData && formData.append('type', 'chatter');
    formData && formData.append('id', authUserData.user_id);

    await profile.updatePreference(token, formData);
  };

  return (
    <ScrollView
      keyboardShouldPersistTaps={'handled'}
      showsHorizontalScrollIndicator={false}
      showsVerticalScrollIndicator={false}
      style={styles.container}>
      <View style={styles.mainsec}>
        <View style={styles.switchContainer}>
          <View style={styles.switchLabelContainer}>
            <Text style={styles.headingTxt}>Social Features</Text>
          </View>

          <View style={styles.switchToggleContainer}>
            <Switch isEnabled={social} toggleSwitch={toggleSocial} />
          </View>
        </View>

        <View style={{marginVertical: 8}}>
          <View style={styles.switchContainer}>
            <View style={styles.switchLabelContainer}>
              <Text style={styles.headingTxt}>Social Media</Text>
            </View>

            <View style={styles.switchToggleContainer}>
              <Switch isEnabled={media} toggleSwitch={toggleMedia} />
            </View>
          </View>

          <View style={styles.switchContainer}>
            <View style={styles.switchLabelContainer}>
              <Text style={styles.labelTxt}>Include my social media</Text>
            </View>

            <View style={styles.switchToggleContainer}>
              <Switch isEnabled={include} toggleSwitch={toggleInclude} />
            </View>
          </View>

          <View style={styles.switchContainer}>
            <View style={styles.switchLabelContainer}>
              <Text style={styles.labelTxt}>Exclude my social media</Text>
            </View>

            <View style={styles.switchToggleContainer}>
              <Switch isEnabled={myMedia} toggleSwitch={toggleMyMedia} />
            </View>
          </View>
        </View>

        <View style={{marginVertical: 8}}>
          <View style={styles.switchContainer}>
            <View style={styles.switchLabelContainer}>
              <Text style={styles.headingTxt}>Channels</Text>
            </View>

            <View style={styles.switchToggleContainer}>
              <Switch isEnabled={channels} toggleSwitch={toggleChannels} />
            </View>
          </View>

          <View style={styles.switchContainer}>
            <View style={styles.switchLabelContainer}>
              <Text style={styles.labelTxt}>Channels Leadership</Text>
            </View>

            <View style={styles.switchToggleContainer}>
              <Switch isEnabled={leadership} toggleSwitch={toggleLeader} />
            </View>
          </View>

          <View style={styles.switchContainer}>
            <View style={styles.switchLabelContainer}>
              <Text style={styles.labelTxt}>Channels Membership</Text>
            </View>

            <View style={styles.switchToggleContainer}>
              <Switch isEnabled={memberShip} toggleSwitch={toggleMember} />
            </View>
          </View>
        </View>

        <View style={{marginVertical: 8}}>
          <View style={styles.switchContainer}>
            <View style={styles.switchLabelContainer}>
              <Text style={styles.headingTxt}>Chatter</Text>
            </View>

            <View style={styles.switchToggleContainer}>
              <Switch isEnabled={chatter} toggleSwitch={toggleChatter} />
            </View>
          </View>

          <View style={styles.switchContainer}>
            <View style={styles.switchLabelContainer}>
              <Text style={styles.labelTxt}>See chatter Newsfeed</Text>
            </View>

            <View style={styles.switchToggleContainer}>
              <Switch isEnabled={newsFeed} toggleSwitch={toggleNewsFeed} />
            </View>
          </View>
        </View>
      </View>
    </ScrollView>
  );
};

export default Social;
