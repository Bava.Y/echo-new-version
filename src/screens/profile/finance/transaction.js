import {useFocusEffect} from '@react-navigation/core';
import React, {useCallback, useContext, useEffect, useState} from 'react';
import {Image, Platform, ScrollView, Text, View} from 'react-native';
import {profile} from '../../../api';
import {ALPACA, AMERITRADE} from '../../../assets/images/index';
import Loading from '../../../components/Loading';
import {UserDataContext} from '../../../context/UserDataContext';
import theme from '../../../utils/theme';
import styles from './styles';

const transaction = () => {
  // Transaction Variables
  const [transactionList, setTransacitonList] = useState(null);
  const [transactionType, setTransacitonListType] = useState([]);
  const [types, setTypes] = useState([]);
  const [typesValue, setTypesValue] = useState(null);

  console.log('transactionList', transactionList);
  console.log('transactionType>>>>>>', transactionType);
  console.log('types+++++++', types);
  // Context Variables
  const {token} = useContext(UserDataContext);

  useFocusEffect(
    useCallback(() => {
      let isFocus = true;

      getTransactionList();
      listType();
      return () => {
        isFocus = false;
      };
    }, []),
  );
  useEffect(() => {
    // listType();
  }, []);

  const getTransactionList = async () => {
    let response = await profile.getTransactionList(token);

    if (response.keyword == 'success' && response.data) {
      setTransacitonList(response.data);
      setTransacitonListType(response.data);
    } else {
      setTransacitonList([]);
    }
  };

  const Typelist = transactionType.map((dataType) => {
    return dataType;
  });

  const listTypeS=Typelist
  console.log('Typelist;;;;;;;;', Typelist);

  const listType = () => {
    listTypeS.map((orderType) => {
    
      if(orderType.type===1){
        setTypes('Sell');
      }
      else if(orderType.type===2){
        setTypes('Buy');
      }
      // else if(orderType.type==='INCOMING'){
      //   setTypes('Deposit');
      // }
      else if(orderType.type==='OUTGOING'){
        setTypes('Withdrawal');
      }
    });
  };

  return (
    <View style={styles.container}>
      {transactionList ? (
        transactionList.length != 0 ? (
          <>
          {/* //Now Hide This View Dani Say
            {/* <View style={styles.walletContainer}>
              <View style={styles.walletLabelContainer}>
                <Text style={styles.walletLabelTxt}>Echo Wallet Balance</Text>
              </View>

              <View style={styles.walletValueContainer}>
                <Text style={styles.walletValueTxt}>$ 0.00</Text>
              </View>
            </View> */}

            <ScrollView
              keyboardShouldPersistTaps={'handled'}
              showsHorizontalScrollIndicator={false}
              showsVerticalScrollIndicator={false}
              style={{marginTop: 8}}>
              <View style={styles.mainsec}>
                <Text style={styles.headingTxt}>Transactions</Text>

                <View style={{marginVertical: 16}}>
                  {transactionList.map((item, index) => (
                    <View
                      key={index}
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        marginTop: index != 0 ? 8 : 0,
                      }}>
                      <View style={{flex: 0.225, justifyContent: 'center'}}>
                        <Text
                          style={{
                            fontSize: Platform.OS == 'ios' ? 10 : 12,
                            color: '#ffffffc2',
                            fontWeight: '400',
                            textAlign: 'left',
                          }}>
                          {item.created_on}
                        </Text>
                      </View>

                      <View style={{flex: 0.3625, justifyContent: 'center',}}>
                        <Text
                          style={{
                            fontSize: Platform.OS == 'ios' ? 10 : 12 ,
                            color: theme.white,
                            fontWeight: '600',
                            textAlign: 'left',
                          }}>
                          {item.type==='INCOMING' || item.type== 'OUTGOING' ? '':item.name}
                        </Text>

                        <Text
                          style={{
                            fontSize: Platform.OS == 'ios' ? (item.type== 'INCOMING'? 14 : 0 || item.type== 'OUTGOING'? 14 : 0 || item.type==1 ? 10 : 0 ||  item.type==2 ? 10 : 0) :(item.type== 'INCOMING'? 16 : 0 || item.type== 'OUTGOING'? 16 : 0 || item.type==1 ? 12 : 0 ||  item.type==2 ? 12 : 0),
                            color: '#ffffffc2',
                            fontWeight: '400',
                            textAlign: 'left',
                            marginTop:  2,
                            paddingBottom:item.type== 'INCOMING' || item.type== 'OUTGOING' ?  14: 0,
                          }}>
                          {item.type==1 ? 'Buy' : '' ||item.type==2 ? 'Sell' : '' || item.type== 'INCOMING'? 'Deposit' : '' || item.type== 'OUTGOING'? 'Withdrawal' : '' }
                        </Text>
                      </View>

                      <View
                        style={{
                          flex: 0.1875,
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}>
                        <Image
                          source={
                            item.broker_connection == 1 ? AMERITRADE : ALPACA
                          }
                          style={{width: 56, height: 28, resizeMode: 'contain'}}
                        />
                      </View>

                      <View style={{flex: 0.225, justifyContent: 'center'}}>
                        <Text
                          style={{
                            fontSize: Platform.OS == 'ios' ? 10 : 12,
                            color: item.type==1 ? 'red' : '' ||item.type==2 ? 'green' : '' || item.type== 'INCOMING'? 'green' : '' || item.type== 'OUTGOING'? 'red' : '' ,
                            fontWeight: '600',
                            textAlign: 'right',
                          }}>
                          {item.amount != null
                            ? `$ ${item.amount}`
                            : 'None'}
                        </Text>
                      </View>
                    </View>
                  ))}
                </View>
              </View>

              <View style={{paddingVertical: 16}} />
            </ScrollView>
          </>
        ) : (
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text style={styles.noDataTxt}>No transaction(s) found</Text>
          </View>
        )
      ) : (
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Loading color="white" />
        </View>
      )}
    </View>
  );
};

export default transaction;
