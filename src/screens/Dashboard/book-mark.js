import React, {useContext} from 'react';
import {
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Ionicons from 'react-native-vector-icons/Ionicons';
import LITERALS from '../../assets/strings';
import BottomNav from '../../components/BottomNav';
import HeaderNav from '../../components/HeaderNav';
import {UserDataContext} from '../../context/UserDataContext';
import theme from '../../utils/theme';
import styles from './styles';
import Ticker from './ticker';

const BookMark = ({navigation}) => {
  const [value, onChangeText] = React.useState('');
  const {tickerData} = useContext(UserDataContext);

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : null}
      keyboardVerticalOffset={Platform.OS === 'ios' ? 40 : 0}
      style={{flex: 1, backgroundColor: theme.themeColor}}>
      <HeaderNav title={LITERALS.dashboard} isBack={true} />

      <Ticker data={tickerData} />

      <ScrollView
        keyboardShouldPersistTaps={'handled'}
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}>
        <View
          style={{
            marginHorizontal: 8,
            marginVertical: 8,
            paddingHorizontal: 8,
            paddingVertical: 16,
          }}>
          <Text
            style={{
              fontSize: Platform.OS == 'ios' ? 16 : 18,
              color: theme.white,
              fontWeight: '600',
              textAlign: 'center',
            }}>
            Bookmarks
          </Text>

          <View style={{position: 'absolute', top: 16, right: 8}}>
            <Ionicons name="book" size={20} color="white" />
          </View>

          <View style={[styles.TrendingsearchSection, {marginTop: 24}]}>
            <Icon
              style={styles.searchIcon}
              name="search"
              size={16}
              color="white"
            />

            <TextInput
              style={styles.searchtext}
              placeholder="Search"
              placeholderTextColor="white"
              onChangeText={(text) => onChangeText(text)}
              value={value}
            />
          </View>

          <View style={{paddingVertical: 16}}>
            <Text style={styles.recentviewtitle}>Last 7 Days</Text>

            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginTop: 32,
              }}>
              <View style={styles.upcompanydata}>
                <Text numberOfLines={1} style={styles.Companyname}>
                  Apple.Inc
                </Text>

                <Text style={styles.symbol}>APPL</Text>

                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Text style={styles.textgreen}>30.58</Text>

                  <View
                    style={{
                      width: 16,
                      alignSelf: 'center',
                      backgroundColor: 'green',
                      marginHorizontal: 8,
                      paddingVertical: 4,
                      borderRadius: 4,
                    }}>
                    <Icon
                      name="arrow-up"
                      color="white"
                      size={8}
                      style={{alignSelf: 'center'}}
                    />
                  </View>

                  <Text style={styles.textgreen}>0.05</Text>
                </View>
              </View>

              <View style={styles.downcompanydata}>
                <Text numberOfLines={1} style={styles.Companyname}>
                  Apple.Inc
                </Text>

                <Text style={styles.symbol}>APPL</Text>

                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Text style={styles.textred}>30.58</Text>

                  <View
                    style={{
                      width: 16,
                      alignSelf: 'center',
                      backgroundColor: 'red',
                      marginHorizontal: 8,
                      paddingVertical: 4,
                      borderRadius: 4,
                    }}>
                    <Icon
                      name="arrow-up"
                      color="white"
                      size={8}
                      style={{alignSelf: 'center'}}
                    />
                  </View>

                  <Text style={styles.textred}>0.05</Text>
                </View>
              </View>
            </View>

            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginTop: 32,
                marginBottom: 16,
              }}>
              <View style={styles.downcompanydata}>
                <Text numberOfLines={1} style={styles.Companyname}>
                  Apple.Inc
                </Text>

                <Text style={styles.symbol}>APPL</Text>

                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Text style={styles.textred}>30.58</Text>

                  <View
                    style={{
                      width: 16,
                      alignSelf: 'center',
                      backgroundColor: 'red',
                      marginHorizontal: 8,
                      paddingVertical: 4,
                      borderRadius: 4,
                    }}>
                    <Icon
                      name="arrow-up"
                      color="white"
                      size={8}
                      style={{alignSelf: 'center'}}
                    />
                  </View>

                  <Text style={styles.textred}>0.05</Text>
                </View>
              </View>

              <View style={styles.upcompanydata}>
                <Text numberOfLines={1} style={styles.Companyname}>
                  Apple.Inc
                </Text>

                <Text style={styles.symbol}>APPL</Text>

                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Text style={styles.textgreen}>30.58</Text>
                  <View
                    style={{
                      width: 16,
                      alignSelf: 'center',
                      backgroundColor: 'green',
                      marginHorizontal: 8,
                      paddingVertical: 4,
                      borderRadius: 4,
                    }}>
                    <Icon
                      name="arrow-up"
                      color="white"
                      size={8}
                      style={{alignSelf: 'center'}}
                    />
                  </View>

                  <Text style={styles.textgreen}>0.05</Text>
                </View>
              </View>
            </View>
          </View>
        </View>
      </ScrollView>

      <View style={[styles.tabssec1, {bottom: 32}]}>
        <View style={styles.tabhead}>
          <View style={[styles.headingbtn, styles.tabheadlabel]}>
            <TouchableOpacity onPress={() => navigation.navigate('Dashboard')}>
              <Text style={styles.headingtxt}>Trading</Text>
            </TouchableOpacity>
          </View>

          <View style={[styles.headingactivebtn, styles.tabheadlabel]}>
            <Text style={styles.headingactivetxt}>Trending</Text>
          </View>

          <View style={[styles.headingbtn, styles.tabheadlabel]}>
            <TouchableOpacity onPress={() => navigation.navigate('News')}>
              <Text style={styles.headingtxt}>News</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>

      <BottomNav routeName="Dashboard" />
    </KeyboardAvoidingView>
  );
};

export default BookMark;
