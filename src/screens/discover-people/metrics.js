import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {Platform, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import theme from '../../utils/theme';

const Metrics = ({onSelect}) => {
  const navigation = useNavigation();

  const metricsArray = [
    [
      'Gross Profit Margin',
      'Net Profit Margin',
      'Net Debt / EBITDA',
      'EV/EBITDA (TTM)',
    ],
    [
      'EV/EBITDA (FYE)',
      'Operating Profit Margin',
      'Debt/Equity Ratio',
      'Book Value / Share',
    ],
    ['P/E Ratio (TTM)', 'P/E Ratio (FYE)', 'Current Ratio', 'EBITDA (TTM)'],
    ['EBITDA (FYE)', 'EV', 'PEG', 'Operating Cash Flow Margin'],
    [
      'Free Cash Flow Margin',
      'Interest Coverage Ratio',
      'Inventory Turnover Rate',
      'Pre-Tax Net Profit Margin',
    ],
    [
      'Price / Operating Cash Flow Ratio (TTM)',
      'Price / Operating Cash Flow Ratio (FYE)',
      'Price / Free Cash Flow Ratio (TTM)',
      'Price / Free Cash Flow Ratio (FYE)',
    ],
    ['Tangible Book Value / Share', 'Asset Turnover Ratio', null, null],
  ];

  return (
    <View
      style={{
        height: '5%',
        backgroundColor: theme.secondryColor,
        borderRadius: 8,
        marginHorizontal: 8,
        marginVertical: 8,
        justifyContent:'center'
      }}>
      {/* <ScrollView
        keyboardShouldPersistTaps="handled"
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}>
        {metricsArray.map((outer, outerIndex) => (
          <View
            key={outerIndex}
            style={{
              flexDirection: 'row',
              justifyContent: 'flex-start',
              borderColor: theme.white,
              borderBottomWidth: metricsArray.length - 1 == outerIndex ? 0 : 1,
            }}>
            {outer.map((inner, innerIndex) => (
              <View
                key={innerIndex}
                style={{
                  flex: 0.25,
                  justifyContent: 'center',
                  borderColor: theme.white,
                  borderRightWidth:
                    (innerIndex + 1) % 4 != 0 && Boolean(inner) ? 1 : 0,
                }}>
                <TouchableOpacity
                  onPress={() => {
                    onSelect(false);

                    navigation.navigate('MetricsResult');
                  }}
                  style={{
                    paddingHorizontal: 8,
                    paddingVertical: 8,
                  }}>
                  <Text
                    style={{
                      fontSize: Platform.OS == 'ios' ? 12 : 14,
                      color: theme.white,
                      textAlign: 'center',
                    }}>
                    {inner}
                  </Text>
                </TouchableOpacity>
              </View>
            ))}
          </View>
        ))} */}
      {/* </ScrollView> */}
      <Text style={{color:'white',alignSelf:'center'}}>Advanced Filtering Metrics Coming Soon</Text>
    </View>
  );
};

export default Metrics;
