import React, {useContext, useEffect, useState} from 'react';
import {Platform, ScrollView, Text, View} from 'react-native';
import {company} from '../../api/Company.service';
import Loading from '../../components/Loading';
import {UserDataContext} from '../../context/UserDataContext';
import theme from '../../utils/theme';

const CorporateAction = ({route}) => {
  const [name] = useState(route.params.name);
  const [dataArr, setDataArr] = useState([]);
  const {token, symbol} = useContext(UserDataContext);

  useEffect(() => {
    let ac = new AbortController();

    getCorpData();

    return () => {
      ac.abort();
    };
  }, [name, symbol]);

  const [loading, setLoading] = useState(false);

  const getCorpData = async () => {
    setLoading(true);

    const res = await company.getCorpAction(`${name}/${symbol}`, token);

    if (res) {
      if (res.keyword == 'success') {
        setDataArr(res.data);
      } else {
        setDataArr(res.data);
      }
    } else {
    }

    setLoading(false);
  };

  return (
    <>
      {loading ? (
        <Loading color={'white'} />
      ) : !loading && dataArr.length == 0 ? (
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Text
            style={{fontSize: Platform.OS == 'ios' ? 14 : 16, color: 'white'}}>
            No data found
          </Text>
        </View>
      ) : (
        <ScrollView
          keyboardShouldPersistTaps={'handled'}
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
          style={{
            flex: 1,
            backgroundColor: theme.themeColor,
            marginHorizontal: 16,
            marginVertical: 16,
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <View
              style={{
                width: '35%',
                justifyContent: 'center',
                paddingHorizontal: 8,
              }}>
              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 14 : 16,
                  color: theme.secondryColor,
                  fontWeight: '600',
                  textAlign: 'left',
                }}>
                Date
              </Text>
            </View>

            <View
              style={{
                width: '65%',
                justifyContent: 'center',
              }}>
              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 14 : 16,
                  color: theme.secondryColor,
                  fontWeight: '600',
                  textAlign: 'left',
                }}>
                Description
              </Text>
            </View>
          </View>

          {dataArr.map((data, index) => (
            <View
              key={index.toString()}
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                backgroundColor: theme.primaryColor,
                borderRadius: 4,
                marginVertical: 8,
                paddingHorizontal: 8,
                paddingVertical: 8,
              }}>
              <View style={{width: '35%', justifyContent: 'center'}}>
                <Text
                  style={{
                    fontSize: Platform.OS == 'ios' ? 12 : 14,
                    color: 'white',
                    textAlign: 'left',
                  }}>
                  {data.exDate}
                </Text>
              </View>

              <View style={{width: '65%', justifyContent: 'center'}}>
                <Text
                  style={{
                    fontSize: Platform.OS == 'ios' ? 12 : 14,
                    color: 'white',
                    textAlign: 'justify',
                  }}
                  numberOfLines={2}>
                  {Boolean(data) &&
                  data.hasOwnProperty('notes') &&
                  Boolean(data.notes)
                    ? data.notes
                    : 'None'}
                </Text>
              </View>
            </View>
          ))}

          <View style={{paddingVertical: 16}} />
        </ScrollView>
      )}
    </>
  );
};

export default CorporateAction;
