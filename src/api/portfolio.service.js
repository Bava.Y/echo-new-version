import axiosInstance from '../utils/Apicall';
import Errors from '../configs/Errors';
import Snackbar from 'react-native-snackbar';
import AsyncStorage from '@react-native-community/async-storage';
import {TOKEN} from '../utils/constants';

const getOrdersData = async path => {
  let token = await AsyncStorage.getItem(TOKEN);
  return await axiosInstance
    .get(`${path}`, {
      headers: {
        Authorization: token,
      },
    })
    .then(res => {
      return res.data;
    })
    .catch(() => {
      return false; // here return empty object or null or false. based on your business logic
    });
};

const getPortfolioReturnsDetails = async path => {
  let token = await AsyncStorage.getItem(TOKEN);
  return await axiosInstance
    .get(`portfolioReturnsDetails/${path}`, {
      headers: {
        Authorization: token,
      },
    })
    .then(res => {
      return res.data;
    })
    .catch(() => {
      return false; // here return empty object or null or false. based on your business logic
    });
};

const getOrdersChartPortfolio = async path => {
  let token = await AsyncStorage.getItem(TOKEN);
  return await axiosInstance
    .get(`${path}`, {
      headers: {
        Authorization: token,
      },
    })
    .then(res => {
      return res.data;
    })
    .catch(() => {
      return false; // here return empty object or null or false. based on your business logic
    });
};

const getOrdersChartMystock = async path => {
  let token = await AsyncStorage.getItem(TOKEN);
  return await axiosInstance
    .get(`${path}`, {
      headers: {
        Authorization: token,
      },
    })
    .then(res => {
      return res.data;
    })
    .catch(() => {
      return false; // here return empty object or null or false. based on your business logic
    });
};

const getOrdersChart = async path => {
  let token = await AsyncStorage.getItem(TOKEN);
  return await axiosInstance
    .get(`${path}`, {
      headers: {
        Authorization: token,
      },
    })
    .then(res => {
      return res.data;
    })
    .catch(() => {
      return false; // here return empty object or null or false. based on your business logic
    });
};

const getFromToDateMyStockChart = async (path, range) => {
  let token = await AsyncStorage.getItem(TOKEN);
  return await axiosInstance
    .get(`${path}`, {
      headers: {
        Authorization: token,
      },
    })
    .then(res => {
      return res.data;
    })
    .catch(() => {
      return false; // here return empty object or null or false. based on your business logic
    });
};

const getFromToDateMyPeopleChart = async (id, rangeFrom, endpoint) => {
  let token = await AsyncStorage.getItem(TOKEN);
  return await axiosInstance
    .get(`${rangeFrom}`, {
      headers: {
        Authorization: token,
      },
    })
    .then(res => {
      return res.data;
    })
    .catch(() => {
      return false; // here return empty object or null or false. based on your business logic
    });
};

const getTrackingError = async id => {
  let token = await AsyncStorage.getItem(TOKEN);
  return await axiosInstance
    .get(`getTrackingError/${id}`, {
      headers: {
        Authorization: token,
      },
    })
    .then(res => {
      return res.data;
    })
    .catch(error => {
      Snackbar.show({
        backgroundColor: 'red',
        duration: Snackbar.LENGTH_LONG,
        text: Errors(error),
      });

      return false; // here return empty object or null or false. based on your business logic
    });
};

const acceptTrackingError = async body => {
  let token = await AsyncStorage.getItem(TOKEN);
  return await axiosInstance
    .post(`accept/rebalanceViaAmount`, body, {
      headers: {
        Authorization: token,
        'Content-Type': 'multipart/form-data',
      },
    })
    .then(res => {
      return res.data;
    })
    .catch(() => {
      return false; // here return empty object or null or false. based on your business logic
    });
};

const ignoreTrackingError = async body => {
  let token = await AsyncStorage.getItem(TOKEN);
  return await axiosInstance
    .post(`ignoreTracking`, body, {
      headers: {
        Authorization: token,
        'Content-Type': 'multipart/form-data',
      },
    })
    .then(res => {
      return res.data;
    })
    .catch(() => {
      return false; // here return empty object or null or false. based on your business logic
    });
};

const getRebalance = async id => {
  let token = await AsyncStorage.getItem(TOKEN);
  return await axiosInstance
    .get(`rebalance/${id}`, {
      headers: {
        Authorization: token,
      },
    })
    .then(res => {
      return res.data;
    })
    .catch(error => {
      Snackbar.show({
        backgroundColor: 'red',
        duration: Snackbar.LENGTH_LONG,
        text: Errors(error),
      });

      return false; // here return empty object or null or false. based on your business logic
    });
};

const acceptRebalance = async body => {
  let token = await AsyncStorage.getItem(TOKEN);
  return await axiosInstance
    .post(`rebalanceAtAnytime`, body, {
      headers: {
        Authorization: token,
        'Content-Type': 'multipart/form-data',
      },
    })
    .then(res => {
      return res.data;
    })
    .catch(() => {
      return false; // here return empty object or null or false. based on your business logic
    });
};

const getOrdersHistory = async (borkerStatus, requestData) => {
  let token = await AsyncStorage.getItem(TOKEN);
  return await axiosInstance
    .get(`orderHistory/${borkerStatus}?status=${requestData}`, {
      headers: {
        Authorization: token,
      },
    })
    .then(res => {
      return res.data;
    })
    .catch(() => {
      return false; // here return empty object or null or false. based on your business logic
    });
};

const getOrdersHistoryMore = async (borkerStatus, requestData) => {
  let token = await AsyncStorage.getItem(TOKEN);
  return await axiosInstance
    .get(`orderHistory/${borkerStatus}?status=${requestData}&limit=500`, {
      headers: {
        Authorization: token,
      },
    })
    .then(res => {
      return res.data;
    })
    .catch(() => {
      return false; // here return empty object or null or false. based on your business logic
    });
};

// const cancelOrderAlpaca = async ( body, token) => {
//   return await axiosInstance.post(`cancelOrderAlpaca`,body,{
//     headers: {
//       Authorization:`Bearer ${token}`
//     },
//   })
//     .then((res) => {
//       return res.data;
//     })
//     .catch((error) => {
//       Snackbar.show({
//         backgroundColor: 'red',
//         duration: Snackbar.LENGTH_LONG,
//         text: Errors(error),
//       });

//       return false; // here return empty object or null or false. based on your business logic
//     });
// };

const cancelOrderAlpaca = async body => {
  let token = await AsyncStorage.getItem(TOKEN);
  let response = await axiosInstance.post(`cancelOrderAlpaca`, body, {
    headers: {
      Authorization: `Bearer ${token}`,
      'Content-Type': 'multipart/form-data',
    },
  });
  try {
    if (response && response.data && response.data.keyword === 'success') {
      return response.data;
    } else {
      return response.data;
    }
  } catch (err) {
    return 'Something went wrong';
  }
};

const getDisallowedTrade = async id => {
  let token = await AsyncStorage.getItem(TOKEN);
  return await axiosInstance
    .get(`financial_metrics_disallowed/${id}`, {
      headers: {
        Authorization: token,
      },
    })
    .then(res => {
      return res.data;
    })
    .catch(error => {
      Snackbar.show({
        backgroundColor: 'red',
        duration: Snackbar.LENGTH_LONG,
        text: Errors(error),
      });

      return false; // here return empty object or null or false. based on your business logic
    });
};

const updateRebalance = async id => {
  let token = await AsyncStorage.getItem(TOKEN);
  return await axiosInstance
    .get(`rebalance_suggestion_status/${id}`, {
      headers: {
        Authorization: token,
      },
    })
    .then(res => {
      return res.data;
    })
    .catch(() => {
      return false; // here return empty object or null or false. based on your business logic
    });
};

const updateDisallowedTrade = async id => {
  let token = await AsyncStorage.getItem(TOKEN);
  return await axiosInstance
    .get(`financial_metrics_disallowed_suggestion_status/${id}`, {
      headers: {
        Authorization: token,
      },
    })
    .then(res => {
      return res.data;
    })
    .catch(() => {
      return false; // here return empty object or null or false. based on your business logic
    });
};

const getTotalPortfolioDayChart = async () => {
  let token = await AsyncStorage.getItem(TOKEN);
  return await axiosInstance
    .get(`totalPortfolioDayChart/0`, {
      headers: {
        Authorization: token,
      },
    })
    .then(res => {
      return res.data;
    })
    .catch(error => {
      // Snackbar.show({
      //   duration: Snackbar.LENGTH_SHORT,
      //   text: Errors(error),
      //   backgroundColor: '#323334',
      // });

      return false; // here return empty object or null or false. based on your business logic
    });
};

const getTotalPortfolioWeekChart = async () => {
  let token = await AsyncStorage.getItem(TOKEN);
  return await axiosInstance
    .get(`totalPortfolioWeekChart`, {
      headers: {
        Authorization: token,
      },
    })
    .then(res => {
      return res.data;
    })
    .catch(error => {
      // Snackbar.show({
      //   duration: Snackbar.LENGTH_SHORT,
      //   text: Errors(error),
      //   backgroundColor: '#323334',
      // });

      return false; // here return empty object or null or false. based on your business logic
    });
};

const getTotalPortfolioMonthChart = async range => {
  let token = await AsyncStorage.getItem(TOKEN);
  return await axiosInstance
    .get(`totalPortfolioMonthChart/${range}`, {
      headers: {
        Authorization: token,
      },
    })
    .then(res => {
      return res.data;
    })
    .catch(error => {
      // Snackbar.show({
      //   duration: Snackbar.LENGTH_SHORT,
      //   text: Errors(error),
      //   backgroundColor: '#323334',
      // });

      return false; // here return empty object or null or false. based on your business logic
    });
};

const getOrdersDataMystock = async path => {
  let token = await AsyncStorage.getItem(TOKEN);
  return await axiosInstance
    .get(`${path}`, {
      headers: {
        Authorization: token,
      },
    })
    .then(res => {
      return res.data;
    })
    .catch(() => {
      return false; // here return empty object or null or false. based on your business logic
    });
};

const getTotalMyStocksDayChart = async () => {
  let token = await AsyncStorage.getItem(TOKEN);
  return await axiosInstance
    .get(`totalPortfolioDayChartMyStock`, {
      headers: {
        Authorization: token,
      },
    })
    .then(res => {
      return res.data;
    })
    .catch(error => {
      // Snackbar.show({
      //   duration: Snackbar.LENGTH_SHORT,
      //   text: Errors(error),
      //   backgroundColor: '#323334',
      // });

      return false; // here return empty object or null or false. based on your business logic
    });
};

const getTotalMyStocksWeekChart = async () => {
  let token = await AsyncStorage.getItem(TOKEN);
  return await axiosInstance
    .get(`totalPortfolioWeekChartMyStock`, {
      headers: {
        Authorization: token,
      },
    })
    .then(res => {
      return res.data;
    })
    .catch(error => {
      // Snackbar.show({
      //   duration: Snackbar.LENGTH_SHORT,
      //   text: Errors(error),
      //   backgroundColor: '#323334',
      // });

      return false; // here return empty object or null or false. based on your business logic
    });
};

const getTotalMyStocksMonthChart = async (range, dateSet) => {
  let token = await AsyncStorage.getItem(TOKEN);
  return await axiosInstance
    .get(`totalPortfolioMonthChartPeopleMyStock/${range}?${dateSet}`, {
      headers: {
        Authorization: token,
      },
      //params: requestData
    })
    .then(res => {
      return res.data;
    })
    .catch(error => {
      // Snackbar.show({
      //   duration: Snackbar.LENGTH_SHORT,
      //   text: Errors(error),
      //   backgroundColor: '#323334',
      // });

      return false; // here return empty object or null or false. based on your business logic
    });
};

const getTotalEchoerDayChart = async id => {
  let token = await AsyncStorage.getItem(TOKEN);
  return await axiosInstance
    .get(`totalPortfolioDayChartPeople/${id}`, {
      headers: {
        Authorization: token,
      },
    })
    .then(res => {
      return res.data;
    })
    .catch(error => {
      // Snackbar.show({
      //   duration: Snackbar.LENGTH_SHORT,
      //   text: Errors(error),
      //   backgroundColor: '#323334',
      // });

      return false; // here return empty object or null or false. based on your business logic
    });
};

const getTotalEchoerWeekChart = async id => {
  let token = await AsyncStorage.getItem(TOKEN);
  return await axiosInstance
    .get(`totalPortfolioWeekChartPeople/${id}`, {
      headers: {
        Authorization: token,
      },
    })
    .then(res => {
      return res.data;
    })
    .catch(error => {
      // Snackbar.show({
      //   duration: Snackbar.LENGTH_SHORT,
      //   text: Errors(error),
      //   backgroundColor: '#323334',
      // });

      return false; // here return empty object or null or false. based on your business logic
    });
};

const getTotalEchoerMonthChart = async (id, range) => {
  let token = await AsyncStorage.getItem(TOKEN);
  return await axiosInstance
    .get(`totalPortfolioMonthChartPeople/${range}/${id}`, {
      headers: {
        Authorization: token,
      },
    })
    .then(res => {
      return res.data;
    })
    .catch(error => {
      // Snackbar.show({
      //   duration: Snackbar.LENGTH_SHORT,
      //   text: Errors(error),
      //   backgroundColor: '#323334',
      // });

      return false; // here return empty object or null or false. based on your business logic
    });
};

const peopleSymbolDetailsPortfolio = async id => {
  let token = await AsyncStorage.getItem(TOKEN);
  return await axiosInstance
    .get(`peopleSymbolDetailsPortfolio/${id}/0`, {
      headers: {
        Authorization: token,
      },
    })
    .then(res => {
      return res.data;
    })
    .catch(error => {
      // Snackbar.show({
      //   duration: Snackbar.LENGTH_SHORT,
      //   text: Errors(error),
      //   backgroundColor: 'red',
      // });

      return false; // here return empty object or null or false. based on your business logic
    });
};

const getechoingList = async () => {
  let token = await AsyncStorage.getItem(TOKEN);
  return await axiosInstance
    .get(`echoing_data_list`, {
      headers: {
        Authorization: token,
      },
    })
    .then(res => {
      return res.data;
    })
    .catch(error => {
      // Snackbar.show({
      //   duration: Snackbar.LENGTH_SHORT,
      //   text: Errors(error),
      //   backgroundColor: 'red',
      // });

      return false; // here return empty object or null or false. based on your business logic
    });
};

const getechowerList = async () => {
  let token = await AsyncStorage.getItem(TOKEN);
  return await axiosInstance
    .get(`unechoing_data_list`, {
      headers: {
        Authorization: token,
      },
    })
    .then(res => {
      return res.data;
    })
    .catch(error => {
      // Snackbar.show({
      //   duration: Snackbar.LENGTH_SHORT,
      //   text: Errors(error),
      //   backgroundColor: 'red',
      // });

      return false; // here return empty object or null or false. based on your business logic
    });
};

export const portfolio = {
  getOrdersData,
  getPortfolioReturnsDetails,
  getOrdersChartPortfolio,
  getOrdersChartMystock,
  getOrdersChart,
  getFromToDateMyStockChart,
  getFromToDateMyPeopleChart,
  getTrackingError,
  acceptTrackingError,
  ignoreTrackingError,
  getRebalance,
  acceptRebalance,
  getOrdersHistory,
  cancelOrderAlpaca,
  getDisallowedTrade,
  updateRebalance,
  updateDisallowedTrade,
  getTotalPortfolioDayChart,
  getTotalPortfolioWeekChart,
  getTotalPortfolioMonthChart,
  getOrdersDataMystock,
  getTotalMyStocksDayChart,
  getTotalMyStocksWeekChart,
  getTotalMyStocksMonthChart,
  getTotalEchoerDayChart,
  getTotalEchoerWeekChart,
  getTotalEchoerMonthChart,
  peopleSymbolDetailsPortfolio,
  getechoingList,
  getechowerList,
  getOrdersHistoryMore,
};
