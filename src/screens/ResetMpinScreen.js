import {StackActions} from '@react-navigation/native';
import _ from 'lodash';
import React, {useState} from 'react';
import {
  Image,
  KeyboardAvoidingView,
  Platform,
  Text,
  TouchableOpacity,
} from 'react-native';
import Snackbar from 'react-native-snackbar';
import {auth} from '../api';
import {APP_LOGO} from '../assets/images';
import {FloatingTitleTextInputField} from '../components/FloatingTitleTextInputField';
import Loading from '../components/Loading';
import style from '../styles/loginstyle';
import theme from '../utils/theme';

const initialValidationError = {
  userName: false,
  passWord: false,
  invalid: false,
};

const ResetMpinScreen = ({navigation}) => {
  const [validationError, setValidationError] = useState({
    ...initialValidationError,
  });

  const [username, setUserName] = useState('');
  const [password, setPassword] = useState('');
  const [loading, setLoading] = useState(false);

  const handleSubmit = async () => {
    let formData = validateFields();
    if (formData !== undefined) {
      setLoading(true);

      const data = await auth.forgotMpin(formData);

      setLoading(false);

      if (typeof data === 'string') {
        setValidationError({
          ...validationError,
          invalid:
            data == 'Email address or password invalid'
              ? 'Username or password invalid'
              : false,
        });

        if (data === 'redirect') {
          return navigation.dispatch(StackActions.replace('Login'));
        }
        return Snackbar.show({
          text: data,
          duration: Snackbar.LENGTH_SHORT,
        });
      }
      Snackbar.show({
        text: `OTP sent successfully`,
        duration: Snackbar.LENGTH_SHORT,
      });
      setTimeout(() => {
        setLoading(false);
        navigation.dispatch(
          StackActions.replace('ResettingMpin', {data: data}),
        );
      }, 1500);
    }
  };

  const validateFields = () => {
    let formData = new FormData();
    let error = validationError;
    if (username === '') {
      error['userName'] = 'Enter user name';
      formData = undefined;
    } else {
      error['userName'] = false;
      formData && formData.append('username', username);
    }
    if (password === '') {
      error['passWord'] = 'Enter password';
      formData = undefined;
    } else {
      error['passWord'] = false;
      formData && formData.append('password', password);
    }
    setValidationError({...validationError, ...error});
    return formData;
  };

  return (
    <>
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : null}
        keyboardVerticalOffset={Platform.OS === 'ios' ? 40 : 0}
        style={style.container}>
        <Image source={APP_LOGO} style={style.appLogo} />

        <Text style={style.forgottitle}>Forgot MPIN?</Text>

        <FloatingTitleTextInputField
          ispassword={false}
          attrName="username"
          title="User Name"
          value={username}
          updateMasterState={(text) => {
            setValidationError({
              ...validationError,
              userName: _.isEmpty(text) ? 'Enter user name' : false,
            });
            setUserName(text);
          }}
        />
        {validationError.userName ? (
          <Text style={style.errormsg}>{validationError.userName}</Text>
        ) : null}

        <FloatingTitleTextInputField
          ispassword={true}
          attrName="password"
          title="Password"
          value={password}
          updateMasterState={(text) => {
            setValidationError({
              ...validationError,
              passWord: _.isEmpty(text) ? 'Enter password' : false,
            });
            setPassword(text);
          }}
        />
        {validationError.passWord ? (
          <Text style={style.errormsg}>{validationError.passWord}</Text>
        ) : null}
        {validationError.invalid ? (
          <Text style={style.errormsg}>{validationError.invalid}</Text>
        ) : null}

        <TouchableOpacity style={style.signinBtn} onPress={handleSubmit}>
          <Text style={style.signintext}>Submit</Text>
        </TouchableOpacity>
      </KeyboardAvoidingView>

      {loading && <Loading color={theme.white} />}
    </>
  );
};

export default ResetMpinScreen;
