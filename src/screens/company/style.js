import {Platform, StyleSheet} from 'react-native';
import theme from '../../utils/theme';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.themeColor,
  },
  container1: {
    flex: 1,
    flexDirection: 'column',
  },
  companydetail: {
    alignItems: 'center',
    marginTop: 16,
    marginBottom: 8,
  },
  smalltxt: {
    fontSize: 12,
    color: theme.textColor,
    paddingVertical: 5,
  },
  bigtxt: {
    fontSize: 20,
    color: theme.white,
    padding: 5,
  },
  mediumtxt: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    color: theme.white,
    paddingVertical: 8,
  },
  successtxt: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    color: 'green',
    paddingVertical: 8,
  },
  negativetxt: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    color: 'red',
    paddingVertical: 8,
  },
  desctxt: {
    fontSize: Platform.OS == 'ios' ? 12 : 14,
    color: theme.white,
    textAlign: 'justify',
    lineHeight: 16,
    paddingHorizontal: 8,
  },
  updatetime: {
    fontSize: Platform.OS == 'ios' ? 10 : 12,
    color: theme.textColor,
    textAlign: 'right',
    paddingHorizontal: 8,
    marginTop: 8,
  },
  //chart details
  chartdetail: {
    backgroundColor: theme.primaryColor,
    borderRadius: 8,
    margin: 16,
    padding: 16,
  },
  dayview: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 16,
  },
  day: {
    fontSize: Platform.OS == 'ios' ? 12 : 14,
    fontWeight: 'bold',
    color: 'white',
    paddingHorizontal: 8,
  },
  active: {
    fontSize: Platform.OS == 'ios' ? 12 : 14,
    fontWeight: 'bold',
    color: theme.secondryColor,
    paddingHorizontal: 8,
  },
  tabssec1: {
    backgroundColor: theme.primaryColor,
    paddingHorizontal: 16,
    paddingVertical: 20,
  },
  tabhead: {
    flexDirection: 'row',
    backgroundColor: theme.themeColor,
    borderRadius: 8,
  },
  tabheadlabel: {
    width: '33%',
  },
  headingactivebtn: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: theme.secondryColor,
    paddingHorizontal: 6,
    paddingVertical: 6,
    borderRadius: 6,
  },
  headingbtn: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
    paddingHorizontal: 6,
    paddingVertical: 6,
    borderRadius: 6,
  },
  headingtxt: {
    fontSize: Platform.OS == 'ios' ? 12 : 14,
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  headingactivetxt: {
    fontSize: Platform.OS == 'ios' ? 12 : 14,
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  toptabhead: {
    flexDirection: 'row',
    alignSelf: 'center',
    backgroundColor: '#0000004d',
    width: '100%',
    borderRadius: 10,
    padding: 10,
    margin: 10,
  },
  topheadingactivebtn: {
    backgroundColor: theme.white,
    paddingVertical: 5,
    borderRadius: 10,
    marginHorizontal: 4,
  },
  topheadingbtn: {
    backgroundColor: 'transparent',
    borderRadius: 10,
    paddingVertical: 5,
  },
  topheadingtxt: {
    color: theme.white,
    textAlign: 'center',
    paddingHorizontal: 10,
  },
  topheadingactivetxt: {
    color: theme.secondryColor,
    textAlign: 'center',
    paddingHorizontal: 10,
  },
  describeitem: {
    padding: 10,
  },
  describetitle: {
    fontSize: 18,
    color: theme.white,
  },
  describecontent: {
    fontSize: 16,
    color: theme.white,
  },
  companygroup: {
    flexDirection: 'row',
    padding: 15,
    alignContent: 'space-between',
    backgroundColor: theme.secondryColor,
  },
  groupname: {
    backgroundColor: theme.white,
    borderWidth: 1,
    borderColor: theme.secondryColor,
    padding: 12,
    borderRadius: 50,
    alignSelf: 'center',
    marginHorizontal: 30,
    width: '55%',
    height: 70,
    justifyContent: 'center',
  },

  title: {
    fontSize: 16,
    color: theme.white,
  },
  lastpricevalue: {
    fontSize: 20,
    color: theme.white,
    fontWeight: 'bold',
    marginTop: 5,
  },
  todaychange: {
    color: theme.successGreen,
    fontSize: 20,
    fontWeight: 'bold',
    marginTop: 5,
  },
  stitle: {
    fontSize: 14,
    color: 'grey',
    textAlign: 'center',
  },
  pricevalue: {
    fontSize: 16,
    color: theme.white,
    textAlign: 'center',
  },
  bggreen: {
    color: theme.white,
    backgroundColor: theme.greenBg,
    padding: 10,
    paddingHorizontal: 17,
  },
  topiconsec: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  bottomiconsec: {
    flexDirection: 'row',
    marginTop: 24,
  },
  icontext: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    color: 'white',
    textAlign: 'center',
  },
  recenttrendingsearchSection: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: theme.themeColor,
    borderRadius: 8,
    marginVertical: 16,
  },
  recentsearch: {
    backgroundColor: theme.primaryColor,
    paddingHorizontal: 16,
  },
  searchtext: {
    flex: 1,
    fontSize: 14,
    color: 'white',
    paddingLeft: 0,
    paddingRight: 8,
  },
  searchIcon: {
    padding: 12,
  },
  Companyname: {
    color: 'white',
    fontSize: 18,
    marginHorizontal: 5,
  },
  sharerate: {
    flexDirection: 'row',
    marginHorizontal: 5,
  },
  shareamt: {
    color: theme.white,
    fontSize: 16,
  },
  toptabhead: {
    flexDirection: 'row',
    alignSelf: 'center',
    backgroundColor: '#00000020',
    borderRadius: 8,
    padding: 4,
  },
  topheadingbtn: {
    backgroundColor: 'transparent',
    borderRadius: 8,
    paddingVertical: 4,
  },
  topheadingtxt: {
    fontSize: Platform.OS == 'ios' ? 12 : 14,
    color: theme.white,
    fontWeight: '600',
    textAlign: 'center',
    paddingHorizontal: 8,
  },
  describeitem: {
    paddingHorizontal: 16,
    paddingVertical: 8,
  },
  describetitle: {
    fontSize: Platform.OS == 'ios' ? 16 : 18,
    fontWeight: '600',
    color: theme.white,
    marginBottom: 8,
  },
  describecontent: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    color: theme.white,
    lineHeight: 18,
  },
  groupname: {
    backgroundColor: theme.white,
    borderWidth: 1,
    borderColor: theme.secondryColor,
    padding: 10,
    borderRadius: 50,
    alignSelf: 'center',
    marginHorizontal: 30,
  },

  title: {
    fontSize: 16,
    color: theme.white,
  },
  lastpricevalue: {
    fontSize: 20,
    color: theme.white,
    fontWeight: 'bold',
    marginTop: 5,
  },
  todaychange: {
    color: theme.successGreen,
    fontSize: 20,
    fontWeight: 'bold',
    marginTop: 5,
  },
  stitle: {
    fontSize: 14,
    color: 'grey',
    textAlign: 'center',
  },
  pricevalue: {
    fontSize: 16,
    color: theme.white,
    textAlign: 'center',
  },
  trending: {
    flex: 1,
    justifyContent: 'center',
    marginTop: 15,
    borderRadius: 10,
    alignItems: 'center',
    padding: 25,
    backgroundColor: theme.primaryColor,
  },
  Companyname: {
    color: 'white',
    fontSize: 18,
    marginHorizontal: 5,
  },
  sharerate: {
    flexDirection: 'row',
    marginHorizontal: 5,
  },
  shareamt: {
    color: theme.white,
    fontSize: 16,
  },

  sharepercentage: {
    color: theme.successGreen,
    fontSize: 16,
    textAlign: 'right',
  },
  selectdata: {
    flexDirection: 'row',
    backgroundColor: theme.themeColor,
    borderRadius: 8,
  },
  durationactive: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: theme.secondryColor,
    paddingHorizontal: 6,
    paddingVertical: 6,
    borderRadius: 6,
  },
  duration: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
    paddingHorizontal: 6,
    paddingVertical: 6,
    borderRadius: 6,
  },
  durationactive1: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: theme.secondryColor,
    paddingHorizontal: 6,
    paddingVertical: 6,
    borderRadius: 6,
  },
  duration1: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
    paddingHorizontal: 6,
    paddingVertical: 6,
    borderRadius: 6,
  },
  durationtxt: {
    fontSize: Platform.OS == 'ios' ? 12 : 14,
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  buybtn: {
    backgroundColor: 'green',
    borderRadius: 4,
    paddingHorizontal: 24,
    paddingVertical: 8,
  },
  buyText: {
    fontSize: Platform.OS == 'ios' ? 12 : 14,
    fontWeight: '600',
    color: theme.white,
    textAlign: 'center',
  },
  sellbtn: {
    backgroundColor: 'red',
    borderRadius: 4,
    paddingHorizontal: 24,
    paddingVertical: 8,
  },
  sellText: {
    fontSize: Platform.OS == 'ios' ? 12 : 14,
    fontWeight: '600',
    color: theme.white,
    textAlign: 'center',
  },
  companyrateimg: {
    width: 40,
    height: 40,
    borderRadius: 50,
  },
});

export default styles;
