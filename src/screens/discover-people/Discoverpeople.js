import React, {
  useContext,
  useEffect,
  useState,
  useCallback,
  useRef,
} from 'react';
import {useFocusEffect} from '@react-navigation/native';
import {
  Platform,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import Modal from 'react-native-modal';
import Icon from 'react-native-vector-icons/FontAwesome';
import BottomNav from '../../components/BottomNav';
import HeaderNav from '../../components/HeaderNav';
import Loading from '../../components/Loading';
import {UserDataContext} from '../../context/UserDataContext';
import axiosInstance from '../../utils/Apicall';
import theme from '../../utils/theme';
import Metrics from './metrics';
import RenderEcho from './partials/RenderEcho';
import styles from './style';
import Filter from './filter';

const Discoverpeople = ({navigation}) => {
  const FilterlistPopup = useRef();
  const [searchSubmitted, setSearchSubmitted] = useState(false);
  const [searchWith, setSearchWith] = useState('');
  const [loading, setLoading] = useState(true);
  const [people, setPeople] = useState([]);
  const [isMetrics, setMetrics] = useState(false);
  const {
    token,
    echoerLoad,
    setEchoerLoad,
    portfolioTimePeriod,
    setPortfolioTimePeriod,
    currentPageDiscover,
    setCurrentPageDiscover,
    primarySelected,
    setPrimarySelected,
    discoverApply,
    setDiscoverApply,
  } = useContext(UserDataContext);

  useFocusEffect(
    useCallback(() => {
      let isFocus = true;
      setCurrentPageDiscover(0);
      setPrimarySelected([]);

      setPortfolioTimePeriod('1d');

      return () => {
        isFocus = false;
      };
    }, []),
  );

  useFocusEffect(
    useCallback(() => {
      let isFocus = true;

      fetchEchoLists();
      return () => {
        isFocus = false;
      };
    }, []),
  );

  useEffect(() => {}, []);
  const fetchEchoLists = async params => {
    setCurrentPageDiscover(0);
    setPeople([]);
    setLoading(true);
    setEchoerLoad(true);

    let path = `discoverPeople?`;
    if (!!params) {
      path = path + '&' + params;
    }

    let response = await axiosInstance.get(path, {
      headers: {
        Authorization: token,
      },
    });

    if (response && response.data && response.data.keyword === 'success') {
      setPeople(response?.data?.data);
      // setCurrentPageDiscover(currentPageDiscover + 1);
      setLoading(false);
    } else if (
      response &&
      response.data &&
      response.data.keyword === 'failed'
    ) {
      setPeople([]);
      setLoading(false);
      setEchoerLoad(false);
    }
  };

  return (
    <>
      <HeaderNav
        title="Discover People"
        isBack="true"
        routeName="Discover People"
        onFilterApply={query => fetchEchoLists(query)}
      />

      <View style={styles.container}>
        <>
          <View style={{backgroundColor: theme.primaryColor}}>
            <View style={styles.searchSection}>
              <Icon
                style={styles.searchIcon}
                name="search"
                size={16}
                color="grey"
              />

              <TextInput
                style={styles.input}
                placeholder="Search"
                placeholderTextColor="grey"
                onChangeText={setSearchWith}
                value={searchWith}
              />

              {searchSubmitted ? (
                <Icon
                  style={styles.searchIcon}
                  name="close"
                  size={16}
                  color="grey"
                  onPress={() => {
                    setSearchWith('');

                    setSearchSubmitted(false);

                    fetchEchoLists();
                  }}
                />
              ) : (
                <Icon
                  style={styles.searchIcon}
                  name="arrow-right"
                  size={16}
                  color="grey"
                  onPress={() => {
                    setSearchSubmitted(true);

                    //  fetchSearchEchoLists('searchWith=' + searchWith);
                  }}
                />
              )}
            </View>
          </View>
          <ScrollView
            showsVerticalScrollIndicator={false}
            style={{paddingHorizontal: 16, paddingVertical: 16}}>
            {people?.length < 0 ? (
              <RenderEcho loadingEchoer={loading} />
            ) : (
              <RenderEcho
                loadingEchoer={loading}
                item={people}
                metrics={setMetrics}
                screenName={'Discover People'}
              />
            )}
            <TouchableOpacity
              style={{marginBottom: '16%'}}
              onPress={() => navigation.navigate('LeaderBoard')}>
              <View style={styles.btnview}>
                <Text style={styles.btntext}>Leaderboard</Text>
              </View>
            </TouchableOpacity>
          </ScrollView>
        </>
      </View>

      <Modal
        isVisible={isMetrics}
        coverScreen={false}
        onBackdropPress={() => setMetrics(false)}>
        <Metrics onSelect={setMetrics} />
      </Modal>
      <Filter
        filterlist={FilterlistPopup}
        onChange={() => FilterlistPopup.current.close()}
      />
      <BottomNav routeName="Discover People" />
    </>
  );
};

export default Discoverpeople;
