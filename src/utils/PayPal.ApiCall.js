import axios from 'axios';
import * as AxiosLogger from 'axios-logger';
import * as URL from '../../env';

const axiosInstance = axios.create({
  baseURL: URL.payPalBaseURL,
  timeout: 1000000,
});

axiosInstance.interceptors.request.use((request) => {
  return AxiosLogger.requestLogger(request, {
    prefixText: `<=====================${request.method.toUpperCase()} ${
      request.baseURL + request.url
    }================>`,
    status: true,
    headers: true,
    url: false,
    method: false,
  });
});

axiosInstance.interceptors.response.use(
  AxiosLogger.responseLogger,
  AxiosLogger.errorLogger,
);

export default axiosInstance;
