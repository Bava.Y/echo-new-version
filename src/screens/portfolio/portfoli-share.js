import {useFocusEffect,useNavigation} from  '@react-navigation/native';
import moment from 'moment';
import React, {useCallback, useContext, useEffect, useState} from 'react';
import {
  ActivityIndicator,
  Alert,
  Dimensions,
  Image,
  Keyboard,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  FlatList,
} from 'react-native';
import Snackbar from 'react-native-snackbar';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {portfolio} from '../../api';
import HeaderNav from '../../components/HeaderNav';
import Loading from '../../components/Loading';
import {UserDataContext} from '../../context/UserDataContext';
import theme from '../../utils/theme';
import PortfolioChart from './chart';
import CompanyList from './companyList';
//import Date from './date';
import styles from './style';
import Modal from 'react-native-modal';
import {PROFILE} from '../../assets/images/index';
import SelectDropdown from 'react-native-select-dropdown';
import Icon from 'react-native-vector-icons/FontAwesome';
import Ionicon from 'react-native-vector-icons/Ionicons';
import axiosInstance from '../../utils/Apicall';
import FontAwesomeIcons from 'react-native-vector-icons/FontAwesome5';
import {GestureHandlerRootView} from 'react-native-gesture-handler';
import {
  CandlestickChart,
  LineChart,
  LineChartCursorCrosshair,
  LineChartCursorLine,
  LineChartCursorProps,
} from 'react-native-wagmi-charts';
import Svg from 'react-native-svg';
import * as haptics from 'react-native-haptic-feedback';
import DateTimePicker from '@react-native-community/datetimepicker';
const PortfolioShare = ({route}) => {
  // Props Variables
  const {id} = route.params;
  console.log('id>><<??', id);
  // Echoers Variables
  const [chartOrderData, setChartOrderData] = useState(null);
  const [returnsValue, setReturnsValue] = useState(null);
  const [returnsAmount, setReturnsAmount] = useState(null);
  const [listOrderData, setListOrderData] = useState([]);
  const [lineChartDataPeople, setlineChartDataPeople] = useState(null);
  const [chartRange, setChartRange] = useState('1d');
  const [rangeFrom, setrangeFrom] = useState(0);

  //ECHOING STATES

  const [profile, setProfile] = useState({});
  const [echoing, setEchoing] = useState([]);
  const [alphacaAllocation, setAlphacaAllocation] = useState(null);
  const [ameritradeAllocation, setAmeritradeAllocation] = useState(null);
  const [chosenAlphaca, setChosenAlphaca] = useState();
  const [chosenTDAmeritrade, setChosenTDAmeritrade] = useState();
  const [alloationStatus, setAlloationStatus] = useState(false);
  const [echoingOptions, setEchoingOptions] = useState(null);
  const [echoingOptionsModal, setEchoingOptionsModal] = useState(false);
  const [selectedEchoingOption, setSelectedEchoingOption] = useState(null);
  const [portfolioconnection, setportfolioconnection] = useState();

  // Context Variables

  // Error Variables
  const [alphacaError, setAlphacaError] = useState(false);
  const [chosenAlphacaError, setChosenAlphacaError] = useState(false);
  const [ameritradeError, setAmeritradeError] = useState(false);
  const [chosenAmeritradeError, setChosenAmeritradeError] = useState(false);
  const [textInputError, setTextInputError] = useState(false);
  const [chosenError, setChosenError] = useState(false);
  const [selectedEchoingOptionError, setSelectedEchoingOptionError] =
    useState(false);
  const [chooseconnection, setchooseconnection] = useState(false);

  // Other Variables

  const [ELoading, setELoading] = useState(false);
  const [echoLoading, setEchoLoading] = useState(false);
  const [isModalVisible, setModalVisible] = useState(false);
  const [echoingAlertStatus, setEchoingAlertStatus] = useState(false);
  const [returnsstate, setreturnsstate] = useState();
  const [peopleScr, setPeopleScr] = useState(true);

  const [brokerconnectionstate, setbrokerconnectionstate] = useState();

  const [broker, setBroker] = useState('Select');

  //END ECHOING STATES

  // Context Variables
  const {
    portfolioConnection,
    portfolioTimePeriod,
    token,
    alpacaConfigStatus,
    ameritradeAlertStatus,
    setAmeritradeAlertStatus,
    tdConfigStatus,
    userId,
    setUserId,
    setCardLoad,
  } = useContext(UserDataContext);

  // Date Variables
  const navigation = useNavigation();

  const [datePicker, setDatePicker] = useState(false);
  const [toDatePicker, setDoDatePicker] = useState(false);

  const [date, setDate] = useState(null);
  const [toDate, setDoDate] = useState(null);

  // Other VAriables
  const [peoplechartLoading, setpeoplechartLoading] = useState();
  const [listLoading, setListLoading] = useState(false);
  const [filterStatus, setFilterStatus] = useState(false);
  const [currentPage, setCurrentPage] = useState(0);
  const [loading, setLoading] = useState(false);
  const [editData, setEditData] = useState(null);

  useFocusEffect(
    useCallback(() => {
      let isFocus = true;

      getOrdersChart();
      fetchLineChartDataPeople();
      // setFromDate(moment());
      // setToDate(moment());
      setFilterStatus(false);

      return () => {
        isFocus = false;
      };
    }, [portfolioConnection, portfolioTimePeriod, chartRange]),
  );

  useEffect(() => {
    let ac = new AbortController();

    fetchProfile();

    fetchEchoLists();
    fetchreturns();

    return ac.abort();
  }, [id]);

  useEffect(() => {
    getOrdersChart();
    getPortfolioData();
  }, [currentPage]);

  // const handleDatePicker = (selectedDate, status) => {
  //   switch (status) {
  //     case 'from':
  //       setFromDate(selectedDate);
  //       break;

  //     case 'to':
  //       setToDate(selectedDate);
  //       break;
  //   }
  // };

  function onDateSelected(event, value) {
    setDatePicker(false);
    setDate(moment(value).format('DD-MM-YYYY'));
  }

  function onToDateSelected(event, value) {
    setDoDatePicker(false);
    setDoDate(moment(value).format('DD-MM-YYYY'));
  }

  const getOrdersChart = async () => {
    let endpoint = `people/chart/${id}/${portfolioConnection}`;

    if (!!portfolioTimePeriod) {
      endpoint = endpoint + '?time_period=' + chartRange;
    }

    const response = await portfolio.getOrdersChart(endpoint);

    const res = response?.data;

    if (res) {
      if (response?.keyword == 'success') {
        let orderArr = [];

        if (res.length != 0) {
          for (let i = 0; i < res?.chart?.length; i++) {
            orderArr.push({
              name: `${res?.chart[i]?.symbol} (${res?.chart[i]?.percentage}%)`,
              x: `${res?.chart[i]?.percentage}%`,
              y: Number(res?.chart[i]?.percentage / 30),
            });
          }

          setChartOrderData(orderArr);
          setReturnsValue(res?.portfolio_returns || null);
          setReturnsAmount(res?.portfolio_returns_amount || null);
        }
      } else {
        setChartOrderData([]);
        setReturnsValue(null);
        setReturnsAmount(null);
      }
    } else {
      setChartOrderData([]);
      setReturnsValue(null);
      setReturnsAmount(null);
    }
  };

  const getOrdersChartTime = async () => {
    let endpoint = `people/chart/${id}/${portfolioConnection}?from_date=${date}&to_date=${toDate}`;
    const response = await portfolio.getOrdersChart(endpoint);
    const res = response.data;

    if (res) {
      if (response.keyword == 'success') {
        let orderArr = [];

        if (res.length != 0) {
          for (let i = 0; i < res.chart.length; i++) {
            orderArr.push({
              name: `${res.chart[i].symbol} (${res.chart[i].percentage}%)`,
              x: `${res.chart[i].percentage}%`,
              y: Number(res.chart[i].percentage / 30),
            });
          }

          setChartOrderData(orderArr);
          setReturnsValue(res.portfolio_returns || null);
          setReturnsAmount(res.portfolio_returns_amount || null);
        }
      } else {
        setChartOrderData([]);
        setReturnsValue(res.portfolio_returns || null);
        setReturnsAmount(res.portfolio_returns_amount || null);
      }
    } else {
      setChartOrderData([]);
      setReturnsValue(null);
      setReturnsAmount(null);
    }
  };

  const getPortfolioData = async () => {
    setListLoading(true);
    setCardLoad(true);

    if (peopleScr == true) {
      const endpoint = `people/overall/${id}/${portfolioConnection}?limit=4&offset=${currentPage}`;

      const res = await portfolio.getOrdersData(endpoint);
      if (res) {
        if (res.keyword == 'success') {
          setListLoading(true);
          if (res?.data?.symbol?.length == 0) {
            setLoadMoreData(false);
          }

          await setListOrderData([...listOrderData, ...res.data]);
          setListLoading(false);
          setCurrentPage(currentPage + 1);
          setCardLoad(true);

          //await setListOrderData([...listOrderData]);

          //setLoadMoreData(true);
        } else if (res.keyword == 'failed') {
          setCardLoad(false);
          // setCurrentPage([...currentPage]);
          //  await setListOrderData([...listOrderData]);
          setListLoading(false);
        }
        // else {
        //   setLoadMoreData(false);
        //  await setListOrderData(listOrderData.concat(res.data));
        //   setListLoading(false);
        // }
      } else {
        setCardLoad(false);
        setListOrderData([]);
      }
    } else if (peopleScr == false) {
      const endpoint = `people/overall/${id}/${portfolioConnection}?from_date=${date}&to_date=${toDate}&limit=4&offset=${currentPage}`;

      const res = await portfolio.getOrdersData(endpoint);
      if (res) {
        if (res.keyword == 'success') {
          setListLoading(true);
          if (res?.data?.symbol?.length == 0) {
            setLoadMoreData(false);
          }

          await setListOrderData([...listOrderData, ...res.data]);
          setListLoading(false);
          setCurrentPage(currentPage + 1);
          setCardLoad(true);

          await setListOrderData([...listOrderData]);

          //setLoadMoreData(true);
        } else if (res.keyword == 'failed') {
          setCardLoad(false);

          setCurrentPage([...currentPage]);
          await setListOrderData([...listOrderData]);
          setListLoading(false);
        }
        // else {
        //   setLoadMoreData(false);
        //  await setListOrderData(listOrderData.concat(res.data));
        //   setListLoading(false);
        // }
      } else {
        setCardLoad(false);
        setListOrderData([]);
      }
    }

    setListLoading(false);
  };

  const fetchLineChartDataPeople = () => {
    switch (chartRange) {
      case '1d':
      default:
        getTotalEchoerDayChart();
        setpeoplechartLoading(true);
        break;

      case '1w':
        getTotalEchoerWeekChart();
        setpeoplechartLoading(true);
        break;

      case '1m':
      case '3m':
      case '6m':
      case '1y':
      case 'max':
        getTotalEchoerMonthChart();
        setpeoplechartLoading(true);
        break;
    }
  };

  const getTotalEchoerDayChart = async () => {
    const res = await portfolio.getTotalEchoerDayChart(id);

    if (res) {
      if (res.keyword == 'success') {
        if (res?.data) {
          setlineChartDataPeople(res?.data);
          setpeoplechartLoading(false);
          // const lineData = [];
          // res.data?.map(item => {
          //   const valueData = {};
          //   (valueData['timestamp'] = item?.timestamp),
          //     (valueData['value'] = Number(item?.value)),
          //     lineData.push(valueData);
          //   setlineChartDataPeople(lineData);
          //   setpeoplechartLoading(false);
          // });
        }
      } else {
        setlineChartDataPeople([]);
        setpeoplechartLoading(false);
      }
    } else {
      setlineChartDataPeople([]);
      setpeoplechartLoading(false);
    }
  };

  const getTotalEchoerWeekChart = async () => {
    const res = await portfolio.getTotalEchoerWeekChart(id);

    if (res) {
      if (res.keyword == 'success') {
        if (res?.data) {
          setlineChartDataPeople(res?.data);
          setpeoplechartLoading(false);
        }
      } else {
        setlineChartDataPeople([]);
        setpeoplechartLoading(false);
      }
    } else {
      setlineChartDataPeople([]);
      setpeoplechartLoading(false);
    }
  };

  const getFromToDateMyPeopleChart = async () => {
    setpeoplechartLoading(true);

    let endpoint = `totalPortfolioMonthChartPeople/${rangeFrom}/${id}?from_date=${date}&to_date=${toDate}`;

    const res = await portfolio.getFromToDateMyPeopleChart(
      id,
      endpoint,
      rangeFrom,
    );
    if (res) {
      if (res.keyword == 'success') {
        if (res?.data) {
          setlineChartDataPeople(res.data);
        } else {
          setlineChartDataPeople([]);
        }
      } else {
        setlineChartDataPeople([]);
        Snackbar.show({
          duration: Snackbar.LENGTH_SHORT,
          text: res.message,
          backgroundColor: 'red',
        });
      }
    } else {
      setlineChartDataPeople([]);
    }
  };

  const getTotalEchoerMonthChart = async () => {
    const res = await portfolio.getTotalEchoerMonthChart(id, chartRange);

    if (res) {
      if (res.keyword == 'success') {
        if (res?.data) {
          setlineChartDataPeople(res?.data);
          setpeoplechartLoading(false);
          // const lineData = [];
          // res.data?.map(item => {
          //   const valueData = {};
          //   (valueData['timestamp'] = item?.timestamp),
          //     (valueData['value'] = Number(item?.value)),
          //     lineData.push(valueData);
          //   setlineChartDataPeople(lineData);
          //   setpeoplechartLoading(false);
          // });
        }
      } else {
        setlineChartDataPeople([]);
        setpeoplechartLoading(false);
      }
    } else {
      setlineChartDataPeople([]);
      setpeoplechartLoading(false);
    }
  };

  //ECHOING FUNCTIONS
  const fetchProfile = async () => {
    setLoading(true);

    let path = 'getProfile/' + id;

    let response = await axiosInstance.get(path, {
      headers: {
        Authorization: token,
      },
    });

    if (response && response.data && response.data.keyword === 'success') {
      await setProfile(response.data.data);

      handleEchoingOptions(response.data.data);

      await setAlloationStatus(response.data.data.echo_percentage_allocation);

      await setLoading(false);
    }
  };

  const fetchreturns = async () => {
    setLoading(true);

    let path = 'portfolioDetailsList/' + id;

    let response = await axiosInstance.get(path, {
      headers: {
        Authorization: token,
      },
    });

    if (response && response.data && response.data.keyword === 'success') {
      const brokerConnection = response.data.retunrs_data.map(data => {
        return data.broker_connection;
      });

      const returns = response.data.retunrs_data.map(data => {
        return data.returns;
      });
      setbrokerconnectionstate(brokerConnection);
      setreturnsstate(returns);
    }
  };

  const handleEchoingOptions = responseData => {
    let helperArray = [];

    responseData.echo_movingfwd == 1 &&
      helperArray.push({label: ' Future Echoing', value: '0'});

    responseData.snapshot == 1 &&
      helperArray.push({label: 'Snapshot', value: '1'});

    responseData.hybrid == 1 && helperArray.push({label: 'Hybrid', value: '2'});

    if (helperArray.length == 1) {
      setSelectedEchoingOptionError(false);

      setSelectedEchoingOption(helperArray[0]);
    } else {
      setSelectedEchoingOptionError(false);

      setSelectedEchoingOption(null);
    }

    setEchoingOptions(helperArray);
  };

  const fetchEchoLists = async () => {
    setLoading(true);
    setEchoLoading(true);

    let path = 'echoingList';

    let response = await axiosInstance.get(path, {
      headers: {
        Authorization: token,
      },
    });

    if (response && response.data && response.data.keyword === 'success') {
      let echoingIds = [];

      response.data.echoing_data.map(item => echoingIds.push(item.followed_id));

      await setEchoing(echoingIds);

      await setEchoLoading(false);

      await setLoading(false);
    }
  };

  const unecho = async () => {
    setLoading(true);

    let body = new FormData();

    body.append('followed_id', id);
    body.append('status', 0);

    let response = await axiosInstance.post('echoFollower', body, {
      headers: {
        Authorization: token,
      },
    });

    if (response && response.data && response.data.keyword === 'success') {
      Alert.alert('', response.data.message);

      let echoingIds = echoing;

      if (!echoing.includes(id)) {
        // echoing
        echoingIds.push(id);
      } else {
        // un echoing
        echoingIds = echoingIds.filter(item => item !== id);
      }

      setEchoing(echoingIds);
      navigation.navigate('Portfolio');
    }

    setLoading(false);
  };

  const toggleModal = () => {
    setModalVisible(!isModalVisible);
  };

  const checkbox = () => {
    setChosenAlphacaError(false);

    setChosenError(false);

    setChosenAlphaca(!chosenAlphaca);
  };

  const echoMe = () => {
    Keyboard.dismiss();

    if (checkFormCondition()) {
      if (Boolean(selectedEchoingOption)) {
        echoingOptions && echoingOptions.length == 1
          ? Alert.alert(
              'Confirmation',
              `${profile.first_name}${
                profile.last_name ? ` ${profile.last_name}` : ''
              } has enabled only ${
                selectedEchoingOption.label
              }. Do you want to proceed?`,
              [
                {
                  text: 'Yes',
                  onPress: () => {
                    handleAPI();
                  },
                },
                {
                  text: 'No',
                  onPress: () => {},
                  style: 'cancel',
                },
              ],
            )
          : handleAPI();
      } else {
        setSelectedEchoingOptionError(false);

        setSelectedEchoingOption(null);

        setEchoingOptionsModal(true);
      }
    } else {
      handleErrorValidation();
    }
  };

  const checkFormCondition = () => {
    if (alpacaConfigStatus && tdConfigStatus) {
      if (
        alphacaConditions() &&
        chosenAlphaca &&
        !ameritradeConditions() &&
        !chosenTDAmeritrade
      ) {
        return true;
      } else if (
        ameritradeConditions() &&
        chosenTDAmeritrade &&
        !alphacaConditions() &&
        !chosenAlphaca
      ) {
        return true;
      } else if (
        alphacaConditions() &&
        chosenAlphaca &&
        ameritradeConditions() &&
        chosenTDAmeritrade
      ) {
        return true;
      } else {
        return false;
      }
    } else {
      if (alpacaConfigStatus) {
        if (alphacaConditions() && chosenAlphaca) {
          return true;
        } else {
          return false;
        }
      }

      if (tdConfigStatus) {
        if (ameritradeConditions() && chosenTDAmeritrade) {
          return true;
        } else {
          return false;
        }
      }
    }
  };

  const alphacaConditions = () => {
    if (handleAlphacaConditions()) {
      return false;
    } else {
      return true;
    }
  };

  const handleAlphacaConditions = () => {
    if (alloationStatus) {
      return (
        !alphacaAllocation || alphacaAllocation < 0 || alphacaAllocation > 100
      );
    } else {
      return !alphacaAllocation || alphacaAllocation < 0;
    }
  };

  const ameritradeConditions = () => {
    if (handleAmeritradeConditions()) {
      return false;
    } else {
      return true;
    }
  };

  const handleAmeritradeConditions = () => {
    if (alloationStatus) {
      return (
        !ameritradeAllocation ||
        ameritradeAllocation < 0 ||
        ameritradeAllocation > 100
      );
    } else {
      return !ameritradeAllocation || ameritradeAllocation < 0;
    }
  };

  const handleFormRequestData = () => {
    let requestData = new FormData();
    requestData.append('followed_id', id);
    requestData.append('status', 1);
    requestData.append('echoingType', selectedEchoingOption.value);

    alphacaConditions() && [
      requestData.append('percentage', alphacaAllocation),
      requestData.append('enable_alpaca', chosenAlphaca),
    ];
    ameritradeConditions() && [
      requestData.append('td_percentage', ameritradeAllocation),
      requestData.append('enable_td', chosenTDAmeritrade),
    ];

    return requestData;
  };

  const handleAmeritradeAlertStatus = () => {
    setAmeritradeAlertStatus(!ameritradeAlertStatus);
  };

  const clearFormStates = () => {
    setAlphacaAllocation(null);
    setAmeritradeAllocation(null);
    setChosenAlphaca(false);
    setChosenTDAmeritrade(false);
  };

  const handleErrorValidation = () => {
    if (alpacaConfigStatus && tdConfigStatus) {
      if (alphacaConditions() || ameritradeConditions()) {
        alphacaConditions() &&
          setChosenAlphacaError(chosenAlphaca ? false : true);

        ameritradeConditions() &&
          setChosenAmeritradeError(chosenTDAmeritrade ? false : true);
      } else {
        setTextInputError(true);
      }

      if (chosenAlphaca || chosenTDAmeritrade) {
        chosenAlphaca && setAlphacaError(!alphacaConditions());

        chosenTDAmeritrade && setAmeritradeError(!ameritradeConditions());
      } else {
        setChosenError(true);
      }
    } else {
      alpacaConfigStatus && [
        setAlphacaError(!alphacaConditions()),

        setChosenAlphacaError(chosenAlphaca ? false : true),
      ];

      tdConfigStatus && [
        setAmeritradeError(!ameritradeConditions()),

        setChosenAmeritradeError(chosenTDAmeritrade ? false : true),
      ];
    }
  };

  const handleAPI = async () => {
    setELoading(true);

    const requestData = handleFormRequestData();

    let response = await axiosInstance.post('echoFollower', requestData, {
      headers: {
        Authorization: token,
      },
    });

    if (response && response.data && response.data.keyword === 'success') {
      toggleModal();

      Alert.alert('', response.data.message);

      let echoingIds = echoing;

      if (!echoing.includes(id)) {
        // echoing
        echoingIds.push(id);
      } else {
        // un echoing
        echoingIds = echoingIds.filter(item => item !== id);
      }

      await setEchoing(echoingIds);
      navigation.navigate('Portfolio');
    } else if (
      response &&
      response.data &&
      response.data.keyword === 'Failed'
    ) {
      toggleModal();

      if (response.data.hasOwnProperty('message')) {
        if (response.data.message.hasOwnProperty('error')) {
          switch (response.data.message.error) {
            case 'invalid_grant':
              handleAmeritradeAlertStatus();
              break;

            default:
              Alert.alert('', response.data.message.error);
              break;
          }
        } else {
          Alert.alert(
            '',
            typeof response.data.message == 'string'
              ? response.data.message
              : 'Unable to echo person',
          );
        }
      } else {
        Alert.alert('', 'Unable to echo person');
      }
    }

    setELoading(false);

    clearFormStates();
  };
  function ProfileCard({data}) {
    return (
      <>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-evenly',
            marginHorizontal: 16,
            marginVertical: 20,
          }}>
          <TouchableOpacity
            style={styles.connect}
            onPress={() => {
              fetchEchoLists();
              const echo = profile.echo_feature == 1 ? true : false;
              const inc = echoing.includes(id);
              echo
                ? inc
                  ? echoing.includes(id) && unecho()
                  : echoingOptions && echoingOptions.length != 0
                  ? toggleModal()
                  : alert('No echoing(s) available!')
                : setEchoingAlertStatus(!echoingAlertStatus);
            }}>
            {echoLoading ? (
              <ActivityIndicator size={12} color={theme.white} />
            ) : (
              <Text style={styles.connectsocialstatus}>
                {echoing.includes(id) ? 'Un Echo' : 'Echo Me'}
              </Text>
            )}
          </TouchableOpacity>
        </View>
      </>
    );
  }

  // ECHOING FUNCTIONS END

  return (
    <>
      <HeaderNav title="Portfolio Share" isBack="true" />

      <View style={styles.container}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-evenly',
            alignItems: 'center',
            backgroundColor: '#2e2e2e52',
            padding: 8,
          }}>
          {/* <Date
            fromDate={fromDate}
            toDate={toDate}
            handleDatePicker={handleDatePicker}
          /> */}
          <View
            style={{
              flex: 0.4,
              backgroundColor: theme.primaryColor,
              paddingHorizontal: 15,
              paddingVertical: 8,
              borderColor: 'transparent',
              borderRadius: 4,
              alignItems: 'center',
            }}>
            <TouchableOpacity
              onPress={() => {
                setDatePicker(true);
              }}>
              <View style={{flexDirection: 'row'}}>
                <Ionicons
                  name="calendar-outline"
                  size={18}
                  color={theme.white}
                />
                <Text style={{color: 'white', textAlign: 'center'}}>
                  {Boolean(date) ? date : 'MM-DD-YYYY'}
                </Text>
              </View>
            </TouchableOpacity>
          </View>
          {datePicker && (
            <DateTimePicker
              value={new Date()}
              display={Platform.OS === 'android' ? 'default' : 'spinner'}
              onChange={onDateSelected}
            />
          )}

          <View
            style={{
              flex: 0.4,
              backgroundColor: theme.primaryColor,
              paddingHorizontal: 15,
              paddingVertical: 8,
              borderColor: 'transparent',
              borderRadius: 4,
              alignItems: 'center',
            }}>
            <TouchableOpacity
              onPress={() => {
                setDoDatePicker(true);
              }}
              //style={Styles.dateContainer}
            >
              <View style={{flexDirection: 'row'}}>
                <Ionicons
                  name="calendar-outline"
                  size={18}
                  color={theme.white}
                />
                <Text style={{color: 'white', textAlign: 'center'}}>
                  {Boolean(toDate) ? toDate : 'MM-DD-YYYY'}
                </Text>
              </View>
            </TouchableOpacity>
          </View>

          {toDatePicker && (
            <DateTimePicker
              value={new Date()}
              display={Platform.OS === 'android' ? 'default' : 'spinner'}
              onChange={onToDateSelected}
            />
          )}
          <View
            style={{
              flex: 0.1,
              justifyContent: 'center',
              alignItems: 'center',
              borderColor: 'transparent',
            }}>
            {filterStatus ? (
              <TouchableOpacity
                onPress={() => {
                  setFilterStatus(!filterStatus);
                  handleDatePicker(moment().format('MM-DD-YYYY'), 'from');
                  handleDatePicker(moment().format('MM-DD-YYYY'), 'to');
                  getOrdersChart();
                }}>
                <Ionicons
                  name="close"
                  size={18}
                  color={theme.white}
                  onPress={() => {
                    setPeopleScr(true), getOrdersChartTime();
                  }}
                />
              </TouchableOpacity>
            ) : (
              <TouchableOpacity
                onPress={() => {
                  setFilterStatus(!filterStatus),
                    setPeopleScr(false),
                    getOrdersChartTime();
                }}>
                <Ionicons name="search-outline" size={18} color={theme.white} />
              </TouchableOpacity>
            )}
          </View>
        </View>

        <ScrollView
          keyboardShouldPersistTaps="handled"
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}>
          <View style={{paddingHorizontal: 8, paddingVertical: 8}}>
            {!Boolean(lineChartDataPeople) && Boolean(peoplechartLoading) ? (
              <PortfolioChart loading={peoplechartLoading} />
            ) : (
              <PortfolioChart
                loading={peoplechartLoading}
                from={'Echoer'}
                chartRange={chartRange}
                lineChartDataPeople={lineChartDataPeople}
                returnsValue={returnsValue}
                returnsAmount={returnsAmount}
                orderData={chartOrderData}
                // onPress={onPress}
                onRangeChange={range => {
                  setChartRange(range);

                  // setlineChartDataPeople(null);
                }}
              />
            )}
          </View>

          <View style={{height: 420}}>
            {!Boolean(listOrderData) ? (
              <CompanyList loading={listLoading} />
            ) : (
              <CompanyList loading={listLoading} orderList={listOrderData} />
            )}
          </View>
          <View style={{paddingVertical: 12}} />
          <ProfileCard data={profile} />
        </ScrollView>
      </View>

      {/* //Echo Me Tap */}

      <Modal isVisible={isModalVisible}>
        <KeyboardAvoidingView
          behavior={Platform.OS === 'ios' ? 'padding' : null}
          keyboardVerticalOffset={Platform.OS === 'ios' ? 40 : 0}
          style={{backgroundColor: theme.primaryColor, borderRadius: 8}}>
          <View
            style={{
              backgroundColor: theme.secondryColor,
              borderBottomColor: 'grey',
              borderBottomWidth: 1,
              paddingVertical: 8,
              borderTopLeftRadius: 8,
              borderTopRightRadius: 8,
            }}>
            <Text
              style={{
                fontSize: Platform.OS == 'ios' ? 14 : 16,
                color: 'white',
                fontWeight: 'bold',
                textAlign: 'center',
              }}>
              Echo
            </Text>
          </View>

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginHorizontal: 12,
              marginVertical: 12,
            }}>
            <View
              style={{
                flex: 0.15,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Image
                resizeMode={'cover'}
                source={
                  Boolean(profile.photo_url)
                    ? {uri: profile.photo_url}
                    : PROFILE
                }
                style={{width: 40, height: 40, borderRadius: 40 / 2}}
              />
            </View>

            <View
              style={{
                flex: 0.85,
                justifyContent: 'center',
                paddingHorizontal: 8,
              }}>
              <Text style={styles.username}>
                {profile.first_name + ' ' + profile.last_name}
              </Text>

              <Text style={styles.userposition}>
                {profile?.designation != 'null' ? profile?.designation : ' '}
              </Text>
            </View>
          </View>

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              borderBottomColor: 'white',
              borderBottomWidth: 1,
              marginHorizontal: 16,
              marginBottom: 8,
              paddingBottom: 8,
            }}>
            <Text
              style={{
                width: '35%',
                fontSize: Platform.OS == 'ios' ? 12 : 14,
                color: theme.greenBg,
                fontWeight: '600',
                textAlign: 'left',
              }}>
              Accounts
            </Text>

            <Text
              style={{
                width: '10%',
                fontSize: Platform.OS == 'ios' ? 12 : 14,
                color: theme.greenBg,
                fontWeight: '600',
                textAlign: 'center',
              }}>
              -
            </Text>

            <Text
              style={{
                width: '55%',
                fontSize: Platform.OS == 'ios' ? 12 : 14,
                color: theme.greenBg,
                fontWeight: '600',
                textAlign: 'left',
              }}>{`Allowed (${
              alloationStatus ? '%' : '$'
            }) to this user`}</Text>
          </View>

          {alpacaConfigStatus && (
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginHorizontal: 16,
                marginVertical: 8,
              }}>
              <Text
                style={{
                  width: '35%',
                  fontSize: Platform.OS == 'ios' ? 12 : 14,
                  fontWeight: '800',
                  color: 'white',
                  textAlign: 'left',
                }}>
                Alpaca
              </Text>

              <Text
                style={{
                  width: '10%',
                  fontSize: Platform.OS == 'ios' ? 12 : 14,
                  fontWeight: '800',
                  color: theme.greenBg,
                  textAlign: 'center',
                }}>
                -
              </Text>

              <View>
                <View
                  style={{
                    width: '55%',
                    flexDirection: 'row',
                    justifyContent: 'flex-start',
                  }}>
                  <TextInput
                    keyboardType={'decimal-pad'}
                    value={alphacaAllocation}
                    onChangeText={text => {
                      setAlphacaError(text ? false : true);

                      setTextInputError(false);

                      setAlphacaAllocation(text);
                    }}
                    style={{
                      width: '100%',
                      fontSize: Platform.OS == 'ios' ? 12 : 14,
                      color: 'white',
                      fontWeight: '400',
                      borderBottomColor: 'white',
                      borderBottomWidth: 1,
                      paddingBottom: 8,
                    }}
                  />

                  <Text
                    style={{
                      fontSize: Platform.OS == 'ios' ? 14 : 16,
                      color: 'white',
                      fontWeight: '400',
                      paddingHorizontal: 4,
                    }}>{`( ${alloationStatus ? '%' : '$'} )`}</Text>
                </View>

                {!textInputError && alphacaError && (
                  <Text
                    style={{
                      fontSize: Platform.OS == 'ios' ? 12 : 14,
                      color: 'red',
                      fontWeight: '600',
                      marginTop: 8,
                    }}>
                    {`Enter a valid ${
                      alloationStatus ? 'percentage' : 'amount'
                    }`}
                  </Text>
                )}
              </View>
            </View>
          )}

          {tdConfigStatus && (
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginHorizontal: 16,
                marginVertical: 8,
                marginTop: alpacaConfigStatus ? 16 : 8,
              }}>
              <Text
                style={{
                  width: '35%',
                  fontSize: Platform.OS == 'ios' ? 12 : 14,
                  fontWeight: '800',
                  color: 'white',
                  textAlign: 'left',
                }}>
                TD Amiratrade
              </Text>

              <Text
                style={{
                  width: '10%',
                  fontSize: Platform.OS == 'ios' ? 12 : 14,
                  fontWeight: '800',
                  color: theme.greenBg,
                  textAlign: 'center',
                }}>
                -
              </Text>

              <View>
                <View
                  style={{
                    width: '55%',
                    flexDirection: 'row',
                    justifyContent: 'flex-start',
                  }}>
                  <TextInput
                    keyboardType={'decimal-pad'}
                    value={ameritradeAllocation}
                    onChangeText={text => {
                      setAmeritradeError(text ? false : true);

                      setTextInputError(false);

                      setAmeritradeAllocation(text);
                    }}
                    style={{
                      width: '100%',
                      fontSize: Platform.OS == 'ios' ? 12 : 14,
                      color: 'white',
                      fontWeight: '400',
                      borderBottomColor: 'white',
                      borderBottomWidth: 1,
                      paddingBottom: 8,
                    }}
                  />

                  <Text
                    style={{
                      fontSize: Platform.OS == 'ios' ? 14 : 16,
                      color: 'white',
                      fontWeight: '400',
                      paddingHorizontal: 4,
                    }}>{`( ${alloationStatus ? '%' : '$'} )`}</Text>
                </View>

                {!textInputError && ameritradeError && (
                  <Text
                    style={{
                      fontSize: Platform.OS == 'ios' ? 12 : 14,
                      color: 'red',
                      fontWeight: '600',
                      marginTop: 8,
                    }}>
                    {`Enter a valid ${
                      alloationStatus ? 'percentage' : 'amount'
                    }`}
                  </Text>
                )}
              </View>
            </View>
          )}

          {textInputError && (
            <Text
              style={{
                fontSize: Platform.OS == 'ios' ? 12 : 14,
                color: 'red',
                fontWeight: '600',
                marginHorizontal: 16,
                marginTop: 8,
              }}>
              {`Enter any one of the ${
                alloationStatus ? 'percentage' : 'amount'
              }`}
            </Text>
          )}

          <View
            style={{
              borderTopColor: 'white',
              borderTopWidth: 1,
              marginHorizontal: 16,
              marginVertical: 8,
            }}
          />

          <View style={{paddingHorizontal: 16, paddingVertical: 8}}>
            <Text
              style={{
                fontSize: Platform.OS == 'ios' ? 14 : 16,
                color: 'white',
                fontWeight: '800',
                textDecorationLine: 'underline',
              }}>
              Auto Trade:
            </Text>

            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'flex-start',
                marginVertical: 16,
              }}>
              {alpacaConfigStatus && (
                <TouchableOpacity
                  onPress={() => {
                    checkbox();
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'center',
                      marginRight: tdConfigStatus ? 16 : 0,
                    }}>
                    <View
                      style={[
                        {
                          width: 16,
                          height: 16,
                          borderWidth: 1,
                          borderRadius: 2,
                        },
                        chosenAlphaca
                          ? {
                              backgroundColor: theme.greenBg,
                              borderColor: theme.greenBg,
                            }
                          : {borderColor: 'white'},
                      ]}>
                      {chosenAlphaca ? (
                        <Ionicon
                          name="checkmark-outline"
                          size={12}
                          color="white"
                          style={{alignSelf: 'center'}}
                        />
                      ) : (
                        <></>
                      )}
                    </View>

                    <Text
                      style={{
                        fontSize: Platform.OS == 'ios' ? 12 : 14,
                        color: 'white',
                        fontWeight: '600',
                        marginHorizontal: 8,
                      }}>
                      Alpaca
                    </Text>
                  </View>
                </TouchableOpacity>
              )}

              {tdConfigStatus && (
                <TouchableOpacity
                  onPress={() => {
                    checkbox1();
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'center',
                    }}>
                    <View
                      style={[
                        {
                          width: 16,
                          height: 16,
                          borderWidth: 1,
                          borderRadius: 2,
                        },
                        chosenTDAmeritrade
                          ? {
                              backgroundColor: theme.greenBg,
                              borderColor: theme.greenBg,
                            }
                          : {borderColor: 'white'},
                      ]}>
                      {chosenTDAmeritrade ? (
                        <Ionicon
                          name="checkmark-outline"
                          size={12}
                          color="white"
                          style={{alignSelf: 'center'}}
                        />
                      ) : (
                        <></>
                      )}
                    </View>

                    <Text
                      style={{
                        fontSize: Platform.OS == 'ios' ? 12 : 14,
                        color: 'white',
                        fontWeight: '600',
                        marginHorizontal: 8,
                      }}>
                      TD Amiratrade
                    </Text>
                  </View>
                </TouchableOpacity>
              )}
            </View>

            {!chosenError && (chosenAlphacaError || chosenAmeritradeError) && (
              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 12 : 14,
                  color: 'red',
                  fontWeight: '600',
                }}>
                Please! Enable auto trade for particular account
              </Text>
            )}

            {chosenError && (
              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 12 : 14,
                  color: 'red',
                  fontWeight: '600',
                  marginTop: 8,
                }}>
                Please! Enable any one of the auto trade
              </Text>
            )}

            {alpacaConfigStatus && tdConfigStatus && (
              <View style={{marginVertical: 8}}>
                <Text
                  style={{
                    color: 'white',
                    fontWeight: '600',
                    textAlign: 'left',
                  }}>
                  <Text style={{fontSize: Platform.OS == 'ios' ? 14 : 16}}>
                    Note :{' '}
                  </Text>

                  <Text style={{fontSize: Platform.OS == 'ios' ? 12 : 14}}>
                    Should select any one of the account
                  </Text>
                </Text>
              </View>
            )}
          </View>

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
              marginVertical: 8,
              paddingBottom: 16,
            }}>
            <TouchableOpacity
              disabled={ELoading}
              style={[
                styles.btnview1,
                ELoading && {paddingHorizontal: 36, paddingVertical: 8},
              ]}
              onPress={() => {
                echoMe();
              }}>
              {ELoading ? (
                <ActivityIndicator size={12} color="white" />
              ) : (
                <Text style={styles.btntext}>Echo Me</Text>
              )}
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                clearFormStates();

                toggleModal();
              }}>
              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 14 : 16,
                  color: theme.secondryColor,
                  fontWeight: 'bold',
                  textAlign: 'center',
                  marginHorizontal: 16,
                }}>
                Cancel
              </Text>
            </TouchableOpacity>
          </View>
        </KeyboardAvoidingView>

        <Modal
          isVisible={echoingAlertStatus}
          onBackdropPress={() => {
            setEchoingAlertStatus(!echoingAlertStatus);
          }}>
          <View
            style={{
              backgroundColor: theme.primaryColor,
              borderRadius: 4,
            }}>
            <View
              style={{
                backgroundColor: theme.secondryColor,
                borderTopLeftRadius: 4,
                borderTopRightRadius: 4,
                paddingVertical: 8,
              }}>
              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 14 : 16,
                  color: '#FF0000',
                  fontWeight: 'bold',
                  textAlign: 'center',
                }}>
                ALERT
              </Text>

              <View
                style={{
                  position: 'absolute',
                  width: 28,
                  height: 28,
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: theme.primaryColor,
                  borderRadius: 28 / 2,
                  top: -12,
                  right: -12,
                }}>
                <Icon
                  name="close"
                  size={16}
                  color={theme.white}
                  onPress={() => {
                    setEchoingAlertStatus(!echoingAlertStatus);
                  }}
                />
              </View>
            </View>

            <View style={{marginVertical: 12, marginHorizontal: 12}}>
              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 12 : 14,
                  color: theme.white,
                  fontWeight: '600',
                  textAlign: 'center',
                  marginVertical: 8,
                }}>
                <Text
                  style={{
                    fontSize: Platform.OS == 'ios' ? 12 : 14,
                    fontWeight: '900',
                  }}>
                  {profile.first_name + ' ' + profile.last_name}
                </Text>{' '}
                has disabled the echoing feature
              </Text>

              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 10 : 12,
                  color: theme.textColor,
                  fontWeight: '400',
                  textAlign: 'left',
                  marginVertical: 8,
                }}>
                NOTE: You can echo{' '}
                {profile.first_name + ' ' + profile.last_name} only if the
                echoing feature is enabled.
              </Text>
            </View>
          </View>
        </Modal>

        <Modal isVisible={echoingOptionsModal}>
          <View style={{backgroundColor: theme.primaryColor, borderRadius: 8}}>
            <View
              style={{
                backgroundColor: theme.secondryColor,
                borderBottomColor: 'grey',
                borderBottomWidth: 1,
                borderTopLeftRadius: 8,
                borderTopRightRadius: 8,
                paddingVertical: 8,
              }}>
              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 14 : 16,
                  color: 'white',
                  fontWeight: 'bold',
                  textAlign: 'center',
                }}>
                Echoing(s)
              </Text>
            </View>

            <View style={{marginHorizontal: 12, marginVertical: 12}}>
              {echoingOptions &&
                echoingOptions.map((lol, index) => (
                  <TouchableOpacity
                    key={index}
                    onPress={() => {
                      setSelectedEchoingOptionError(false);

                      setSelectedEchoingOption(
                        selectedEchoingOption != lol ? lol : null,
                      );
                    }}
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      marginVertical: 8,
                    }}>
                    <View
                      style={{
                        flex: 0.0875,
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      <View
                        style={{
                          width: 20,
                          height: 20,
                          justifyContent: 'center',
                          alignItems: 'center',
                          borderColor: theme.secondryColor,
                          borderWidth: 1,
                          borderRadius: 20 / 2,
                        }}>
                        {selectedEchoingOption == lol && (
                          <View
                            style={{
                              width: 10,
                              height: 10,
                              backgroundColor: theme.secondryColor,
                              borderRadius: 10 / 2,
                            }}
                          />
                        )}
                      </View>
                    </View>

                    <View
                      style={{
                        flex: 0.8875,
                        justifyContent: 'center',
                      }}>
                      <Text
                        style={{
                          fontSize: Platform.OS == 'ios' ? 12 : 14,
                          color: 'white',
                          fontWeight: '600',
                        }}>
                        {lol.label}
                      </Text>
                    </View>
                  </TouchableOpacity>
                ))}

              {selectedEchoingOptionError && (
                <Text
                  style={{
                    fontSize: Platform.OS == 'ios' ? 12 : 14,
                    color: 'red',
                    fontWeight: '600',
                    marginHorizontal: 4,
                    marginVertical: 8,
                  }}>
                  Please! Select any one of the echoing(s)!
                </Text>
              )}

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginVertical: 4,
                }}>
                <TouchableOpacity
                  style={styles.btnview1}
                  onPress={() => {
                    if (Boolean(selectedEchoingOption)) {
                      setEchoingOptionsModal(!echoingOptionsModal);

                      echoMe();
                    } else {
                      setSelectedEchoingOptionError(
                        selectedEchoingOption ? false : true,
                      );
                    }
                  }}>
                  <Text style={styles.btntext}>Confirm</Text>
                </TouchableOpacity>

                <TouchableOpacity
                  onPress={() => {
                    setEchoingOptionsModal(!echoingOptionsModal);
                  }}>
                  <Text
                    style={{
                      fontSize: Platform.OS == 'ios' ? 14 : 16,
                      color: theme.secondryColor,
                      fontWeight: 'bold',
                      textAlign: 'center',
                      marginHorizontal: 16,
                    }}>
                    Cancel
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
      </Modal>
    </>
  );
};

export default PortfolioShare;
