import {useFocusEffect} from '@react-navigation/core';
import moment from 'moment';
import React, {useCallback, useContext, useEffect, useState} from 'react';
import {
  ActivityIndicator,
  Alert,
  Dimensions,
  Image,
  Keyboard,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import Snackbar from 'react-native-snackbar';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Modal from 'react-native-modal';
import styles from './styles';
import HeaderNav from '../../components/HeaderNav';
import theme from '../../utils/theme';
import {UserDataContext} from '../../context/UserDataContext';
import {TLOGO} from '../../assets/images/index';

const stockList = ({route, navigation}) => {
  const {processOrder} = route?.params;
  // const processOrder = props.processOrder || [];
  const {
    ownStock,
    setOwnStock,
    othersStock,
    setOthersStock,
    userId,
    setUserId,
    setSellBtn,
    sellBtn,
  } = useContext(UserDataContext);
  console.log('userId^^^^^^^^_____', userId);

  //   const processOrder = async (type) => {
  //     let order = {
  //       ...searchedResult,
  //       side: type,
  //       fromPage: disAllowedProceedStatus ? 'Direct' : '',
  //     };

  //     await setOrderData(order);

  //     Order.current.open();
  //   };

  return (
    <>
      <HeaderNav title="Portfolio StockList" isBack="true" />
      <View style={{backgroundColor: theme.themeColor, flex: 1}}>
        <Text style={styles.title}>My own Stock</Text>

        <View>
          {Boolean(ownStock) ? (
            ownStock.length != 0 ? (
              ownStock.map((item, index) => (
                <View style={[styles.companylist, {marginTop: 16}]}>
                  <Text key={index} style={[styles.companySymbolTxt]}>
                    {item?.symbol}
                  </Text>

                  <Text style={[styles.companyShareAmtTxt]}>
                    {item?.qty.toFixed(2)} Shares
                  </Text>

                  <Text style={[styles.companyShareAmtTxt]}>
                    $ {item.total_amount.toFixed(2)}
                  </Text>

                  <TouchableOpacity
                    style={[styles.sell]}
                    onPress={() => {
                      setUserId(0), processOrder('sell'), setSellBtn(true);
                      //setShowList(false);
                    }}>
                    <Text style={styles.companySellTxt}>Sell</Text>
                  </TouchableOpacity>
                </View>
              ))
            ) : (
              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 12 : 14,
                  fontWeight: '600',
                  color: theme.white,
                  textAlign: 'center',
                  paddingHorizontal: 8,
                  marginTop:10
                }}>
                No Stock(s) found
              </Text>
            )
          ) : (
            <View style={{paddingHorizontal: 8}}>
              <ActivityIndicator size={'large'} color={theme.secondryColor} />
            </View>
          )}
        </View>

        <Text style={[styles.title, {marginTop: 14}]}>Echoed Stocks</Text>
        <View>
        {Boolean(othersStock) ? (
            othersStock.length != 0 ? (
              othersStock.map((lol, index) => (
         
              <View style={[styles.companylist2, {marginTop: 16}]}>
                <View style={{flexDirection: 'row'}}>
                  <TouchableOpacity style={styles.logosec}>
                    <Image
                      resizeMode={'contain'}
                      source={TLOGO}
                      style={styles.applogo}
                    />
                  </TouchableOpacity>
                  <View>
                    <TouchableOpacity style={styles.nameView}>
                      <Text key={index} style={styles.nameStyle}>
                        {lol?.name}
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    marginTop: 3,
                  }}>
                  <Text style={styles.companySymbolTxt}>{lol?.symbol}</Text>
                  <Text style={styles.companyShareAmtTxt}>
                    {lol?.qty.toFixed(2)} Shares
                  </Text>
                  <Text style={styles.companyShareAmtTxt}>
                    $ {lol?.total_amount.toFixed(2)}
                  </Text>
                  <TouchableOpacity
                    style={styles.sell}
                    onPress={() => {
                      setUserId(lol.created_by);
                      processOrder('sell');
                      //setShowList(false);
                      setSellBtn(true);
                    }}>
                    <Text style={styles.companySellTxt}>Sell</Text>
                  </TouchableOpacity>
                </View>
              </View>
                  ))
                  ) : (
                    <Text
                      style={{
                        fontSize: Platform.OS == 'ios' ? 12 : 14,
                        fontWeight: '600',
                        color: theme.white,
                        textAlign: 'center',
                        paddingHorizontal: 8,
                        marginTop:10
                      }}>
                      No Stock(s) found
                    </Text>
                  )
                ) : (
                  <View style={{paddingHorizontal: 8}}>
                    <ActivityIndicator size={'large'} color={theme.secondryColor} />
                  </View>
                )}
        </View>
      </View>
    </>
  );
};

export default stockList;
