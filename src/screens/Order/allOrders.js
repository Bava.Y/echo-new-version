import {useFocusEffect} from '@react-navigation/core';
import React, {useCallback, useContext, useState} from 'react';
import {Platform, ScrollView, Text, View} from 'react-native';
import Snackbar from 'react-native-snackbar';
import {portfolio} from '../../api';
import AmeritradeAlert from '../../components/AmeritradeAlert';
import Loading from '../../components/Loading';
import {UserDataContext} from '../../context/UserDataContext';
import theme from '../../utils/theme';
import Colors from '../../utils/theme';
import Card from './card';
import styles from './style';
import { TouchableOpacity } from 'react-native-gesture-handler';


const allOrders = () => {
  // OpenOrders Variables
  const [orderData, setOrderData] = useState([]);
  const [orderData2, setOrderData2] = useState([]);
  const [statusValue, setStatus] = useState('all');
  const [loading, setLoading] = useState(null);


  // Context Variables
  const {
    ameritradeAlertStatus,
    orderHistoryBorkerConnection,
    setAmeritradeAlertStatus,
    token,
  } = useContext(UserDataContext);

  useFocusEffect(
    useCallback(() => {
      let isFocus = true;

      fetchOrdersHistory();

      return () => {
        isFocus = false;
      };
    }, [orderHistoryBorkerConnection]),
  );

  const fetchOrdersHistory = async () => {
    setLoading(true)
    const response = await portfolio.getOrdersHistory(
      orderHistoryBorkerConnection,
      statusValue,
    );

    if (Boolean(response) && response.data.length !== 0) {
      setLoading(true)
      if (response.keyword == 'success') {
        setOrderData(response.data);
        setLoading(false)
      } else {
        if (response.hasOwnProperty('message')) {
          if (response.message.hasOwnProperty('error')) {
            setOrderData([]);
            setLoading(false)
            switch (response.message.error) {
              case 'invalid_grant':
                handleAmeritradeAlertStatus();
                break;

              default:
                Snackbar.show({
                  duration: Snackbar.LENGTH_SHORT,
                  text: response.message.error,
                  backgroundColor: 'red',
                });
                break;
            }
          } else {
            setOrderData([]);
            setLoading(false)
          }
        } else {
          setOrderData([]);
          setLoading(false)
        }
      }
    } else {
      setOrderData([]);
    }
  };
  const fetchOrdersHistoryMore = async () => {
    const response = await portfolio.getOrdersHistoryMore(
      orderHistoryBorkerConnection,
      statusValue,
    );

    if (Boolean(response) && response.data.length !== 0) {
      if (response.keyword == 'success') {
        setOrderData(response.data);
    
      } else {
        if (response.hasOwnProperty('message')) {
          if (response.message.hasOwnProperty('error')) {
            setOrderData([]);

            switch (response.message.error) {
              case 'invalid_grant':
                handleAmeritradeAlertStatus();
                break;

              default:
                Snackbar.show({
                  duration: Snackbar.LENGTH_SHORT,
                  text: response.message.error,
                  backgroundColor: 'red',
                });
                break;
            }
          } else {
            setOrderData([]);
          }
        } else {
          setOrderData([]);
        }
      }
    } else {
      setOrderData([]);
    }
  };
  const handleAmeritradeAlertStatus = () => {
    setAmeritradeAlertStatus(!ameritradeAlertStatus);
  };

  return (
    <>
      {!loading ? (
        orderData.length !== 0 ? (
          <ScrollView
            keyboardShouldPersistTaps={'handled'}
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
            style={styles.container}>
            <View style={{paddingHorizontal: 8, paddingVertical: 16}}>
              {orderData.map((lol, index) => (
                <Card key={index} orderData={lol}/>
              ))}
            </View>
            <TouchableOpacity
          style={{
            width: '37.5%',
            alignSelf: 'center',
            backgroundColor: Colors.secondryColor,
            borderRadius: 4,
            marginHorizontal: 16,
            marginVertical: 16,
            paddingHorizontal: 16,
            paddingVertical: 8,
          }}
          onPress={() => {
            fetchOrdersHistoryMore()
          }}>
          <Text
            style={{
              fontSize: Platform.OS == 'ios' ? 14 : 16,
              color: Colors.white,
              textAlign: 'center',
            }}>
            View More
          </Text>
        </TouchableOpacity>
            <View style={{paddingVertical: 12}} />
          </ScrollView>
        ) : (
          <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: theme.themeColor,
          }}>
          <Text
            style={{
              fontSize: Platform.OS == 'ios' ? 14 : 16,
              fontWeight: '600',
              color: theme.white,
            }}>
            No Data Found
          </Text>
        </View>
        )
      ) : (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: theme.themeColor,
          }}>
          <Loading color={theme.secondryColor} />
        </View>
      )}

      {/* Hided due to TD Ameritrade Echo App revoked issue */}
      {/* <AmeritradeAlert
        modalStatus={ameritradeAlertStatus}
        close={handleAmeritradeAlertStatus}
      /> */}
    </>
  );
};

export default allOrders;
