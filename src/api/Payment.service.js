import QueryString from 'query-string';
import axiosInstance from '../utils/PayPal.ApiCall';

const getAccessToken = async (body) => {
  return await axiosInstance
    .post(`oauth2/token`, QueryString.stringify(body), {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        Authorization:
          'Basic ' +
          btoa(
            'AYXBdMlqUkBSzbEK21rXVN61b_g3OhIl7mcobhoKC_hANIul0ZdVQJ5eu_2aGopdV831ch-jExGzQ4Ja' +
              ':' +
              'EB9VHbPKjAMMAK0auIMB0gWFAgVh10wSpSXKWLqeqn_J_97fDUTdGfwR_b2oIVhv8qw1d5VAqQmfDkjQ',
          ),
      },
    })
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err;
    });
};

const btoa = (input) => {
  const chars =
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/_-~!@#$%^&*=';

  let str = input;
  let output = '';

  for (
    let block = 0, charCode, i = 0, map = chars;
    str.charAt(i | 0) || ((map = '='), i % 1);
    output += map.charAt(63 & (block >> (8 - (i % 1) * 8)))
  ) {
    charCode = str.charCodeAt((i += 3 / 4));

    if (charCode > 0xff) {
      throw new Error(
        "'btoa' failed: The string to be encoded contains characters outside of the Latin1 range.",
      );
    }

    block = (block << 8) | charCode;
  }

  return output;
};

const createPayment = async (body, token) => {
  return await axiosInstance
    .post(`payments/payment`, body, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + token,
      },
    })
    .then((res) => {
      return res;
    })
    .catch((err) => {
      return err;
    });
};

const makePayment = async (paymentId, body, token) => {
  return await axiosInstance
    .post(`payments/payment/${paymentId}/execute`, body, {
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + token,
      },
    })
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return err;
    });
};

export const payment = {getAccessToken, createPayment, makePayment};
