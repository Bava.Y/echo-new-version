import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import React, {Fragment} from 'react';
import {Platform, SafeAreaView, StatusBar, View} from 'react-native';
import {UserDataProvider} from './src/context/UserDataContext';
import CorpAction from './src/screens/company/CorpAction';
import FinancialHistory from './src/screens/company/FinancialHistory';
import CompanyDetail from './src/screens/company/index';
import Inside from './src/screens/company/inside/index';
import Technicals from './src/screens/company/Technicals';
import WebView from './src/screens/company/WebView';
import Dashboard from './src/screens/Dashboard';
import stockList from './src/screens/Dashboard/StockList';
import Bookmarks from './src/screens/Dashboard/book-mark';
import LineChart from './src/screens/Dashboard/line-chart';
import News from './src/screens/Dashboard/news';
import TrendingScreen from './src/screens/Dashboard/trending';
import AddFund from './src/screens/deposit-fund/add-fund';
import AddRelationship from './src/screens/deposit-fund/add-relationship';
import Payment from './src/screens/deposit-fund/Payment';
import TransactionHistory from './src/screens/deposit-fund/TransactionHistory';
import DisallowedTrades from './src/screens/DisallowedTrades';
import DiscoverPeopleList from './src/screens/discover-people/Discoverpeople';
import DiscoverPeople from './src/screens/discover-people/index';
import LeaderBoard from './src/screens/discover-people/LeaderBoard';
import MetricsResult from './src/screens/discover-people/metrics-result';
import DiscoverPeopleProfile from './src/screens/discover-people/Profile';
import Rebalance from './src/screens/discover-people/Rebalance';
import TrackingErrorHistory from './src/screens/discover-people/tracking-error-history';
import EchoAlert from './src/screens/EchoAlert';
import ForgotpasswordScreen from './src/screens/ForgotpasswordScreen';
import Home from './src/screens/home';
import LoginScreen from './src/screens/LoginScreen';
import MpinScreen from './src/screens/MpinScreen';
import NewRegister from './src/screens/NewRegister';
import Notification from './src/screens/Notification';
import Order from './src/screens/Order/order';
import MPinOTPScreen from './src/screens/otp/MPinOTPScreen';
import OTPScreen from './src/screens/otp/OTPScreen';
import Echoers from './src/screens/portfolio/echoers';
import Portfolio from './src/screens/portfolio/index';
import PortfolioShare from './src/screens/portfolio/portfoli-share';
import IndividualReturns from './src/screens/portfolio/individualReturns';
import FinancialSummary from './src/screens/profile/finance/finance-summary';
import Profile from './src/screens/profile/index';
import Social from './src/screens/profile/profile/social';
import Socialchatter from './src/screens/profile/profile/socialchatter';
import RegisterScreen from './src/screens/RegisterScreen';
import ResetMpinScreen from './src/screens/ResetMpinScreen';
import ResetNewCPIN from './src/screens/ResetNewCPIN';
import ResetNewPin from './src/screens/ResetNewPin';
import ResetPassword from './src/screens/ResetPassword';
import ResettingMpin from './src/screens/ResettingMpin';
import SetCMpinScreen from './src/screens/SetCMpinScreen';
import SetMpinScreen from './src/screens/SetMpinScreen';
import SocialMpin from './src/screens/SocialMpin';
import SplashScreen from './src/screens/splash';
import Chart from './src/screens/stock-details/chart';
import Company from './src/screens/stock-details/Company';
import SearchCompany from './src/screens/stock-details/SearchCompany';
import ViewAlpaca from './src/screens/ViewAlpaca';
import theme from './src/utils/theme';

const Stack = createStackNavigator();

const App = () => {
  return (
    <View style={{flex: 1, backgroundColor: theme.themeColor}}>
      <SafeAreaView
        style={[
          {backgroundColor: theme.secondryColor},
          Platform.OS == 'ios' && {height: StatusBar.currentHeight},
        ]}>
        <StatusBar
          backgroundColor={theme.secondryColor}
          barStyle={'light-content'}
        />
      </SafeAreaView>

      <View style={{flex: 1, backgroundColor: theme.themeColor}}>
        <UserDataProvider>
          <NavigationContainer>
            <Stack.Navigator
              initialRouteName="SplashScreen"
              screenOptions={{safeAreaInsets: {bottom: 0}}}>
              <Stack.Screen
                name="SplashScreen"
                component={SplashScreen}
                options={{headerShown: false}}
              />

              <Stack.Screen
                name="LineChart"
                component={LineChart}
                options={{headerShown: false}}
              />

              <Stack.Screen
                name="Login"
                component={LoginScreen}
                options={{headerShown: false}}
              />

              <Stack.Screen
                name="Home"
                component={Home}
                options={{headerShown: false, headerLeft: null}}
              />

              <Stack.Screen
                name="Profile"
                component={Profile}
                options={{headerShown: false, headerLeft: null}}
              />

              <Stack.Screen
                name="Dashboard"
                component={Dashboard}
                options={{headerShown: false, headerLeft: null}}
              />

              <Stack.Screen
                name="stockList"
                component={stockList}
                options={{headerShown: false, headerLeft: null}}
              />

              <Stack.Screen
                name="Forgotpassword"
                component={ForgotpasswordScreen}
                options={{headerShown: false}}
              />

              <Stack.Screen
                name="OTPScreen"
                component={OTPScreen}
                options={{headerShown: false, headerLeft: null}}
              />

              <Stack.Screen
                name="MpinScreen"
                component={MpinScreen}
                options={{headerShown: false, headerLeft: null}}
              />

              <Stack.Screen
                name="MPinOTPScreen"
                component={MPinOTPScreen}
                options={{headerShown: false, headerLeft: null}}
              />

              <Stack.Screen
                name="SocialMpin"
                component={SocialMpin}
                options={{headerShown: false}}
              />

              <Stack.Screen
                name="ResettingMpin"
                component={ResettingMpin}
                options={{headerShown: false, headerLeft: null}}
              />

              <Stack.Screen
                name="ResetNewPin"
                component={ResetNewPin}
                options={{headerShown: false, headerLeft: null}}
              />

              <Stack.Screen
                name="ResetNewCPIN"
                component={ResetNewCPIN}
                options={{headerShown: false, headerLeft: null}}
              />

              <Stack.Screen
                name="SetMpinScreen"
                component={SetMpinScreen}
                options={{headerShown: false, headerLeft: null}}
              />

              <Stack.Screen
                name="SetCMpinScreen"
                component={SetCMpinScreen}
                options={{headerShown: false, headerLeft: null}}
              />

              <Stack.Screen
                name="ResetMpinScreen"
                component={ResetMpinScreen}
                options={{headerShown: false}}
              />

              <Stack.Screen
                name="Registration"
                component={RegisterScreen}
                options={{headerShown: false}}
              />

              <Stack.Screen
                name="ResetPassword"
                component={ResetPassword}
                options={{headerShown: false}}
              />

              <Stack.Screen
                name="SearchCompany"
                component={SearchCompany}
                options={{headerShown: false}}
              />

              <Stack.Screen
                name="company"
                component={Company}
                options={{headerShown: false}}
              />

              <Stack.Screen
                name="CompanyDetail"
                component={CompanyDetail}
                options={{headerShown: false}}
              />

              <Stack.Screen
                name="Inside"
                component={Inside}
                options={{headerShown: false}}
              />

              <Stack.Screen
                name="chart"
                component={Chart}
                options={{headerShown: false}}
              />

              <Stack.Screen
                name="Trending"
                component={TrendingScreen}
                options={{headerShown: false}}
              />

              <Stack.Screen
                name="News"
                component={News}
                options={{headerShown: false}}
              />

              <Stack.Screen
                name="Bookmarks"
                component={Bookmarks}
                options={{headerShown: false}}
              />

              <Stack.Screen
                name="FinancialHistory"
                component={FinancialHistory}
                options={{headerShown: false}}
              />

              <Stack.Screen
                name="FinancialSummary"
                component={FinancialSummary}
                options={{headerShown: false}}
              />

              <Stack.Screen
                name="WebView"
                component={WebView}
                options={{
                  headerTitle: 'Trading View',
                  headerTitleAlign: 'center',
                  headerTintColor: theme.white,
                  headerTitleStyle: {color: theme.white},
                  headerStyle: {
                    backgroundColor: theme.secondryColor,
                    height: 52,
                  },
                }}
              />

              <Stack.Screen
                name="Technicals"
                component={Technicals}
                options={{headerShown: false}}
              />

              <Stack.Screen
                name="CorpAction"
                component={CorpAction}
                options={{headerShown: false}}
              />

              <Stack.Screen
                name="Portfolio"
                component={Portfolio}
                options={{headerShown: false}}
              />
              <Stack.Screen
                name="PortfolioShare"
                component={PortfolioShare}
                options={{headerShown: false}}
              />
              <Stack.Screen
                name="DiscoverPeople"
                component={DiscoverPeople}
                options={{headerShown: false}}
              />

              <Stack.Screen
                name="DiscoverPeopleList"
                component={DiscoverPeopleList}
                options={{headerShown: false}}
              />

              <Stack.Screen
                name="LeaderBoard"
                component={LeaderBoard}
                options={{headerShown: false}}
              />

              <Stack.Screen
                name="DiscoverPeopleProfile"
                component={DiscoverPeopleProfile}
                options={{headerShown: false}}
              />

              <Stack.Screen
                name="TrackingErrorHistory"
                component={TrackingErrorHistory}
                options={{headerShown: false}}
              />

              <Stack.Screen
                name="AddFund"
                component={AddFund}
                options={{headerShown: false}}
              />

              <Stack.Screen
                name="AddRelationship"
                component={AddRelationship}
                options={{headerShown: false}}
              />

              <Stack.Screen
                name="MetricsResult"
                component={MetricsResult}
                options={{headerShown: false}}
              />

              <Stack.Screen
                name="Echoers"
                component={Echoers}
                options={{headerShown: false}}
              />

              <Stack.Screen
                name="Order"
                component={Order}
                options={{headerShown: false}}
              />

              <Stack.Screen
                name="Payment"
                component={Payment}
                options={{headerShown: false}}
              />

              <Stack.Screen
                name="NewRegister"
                component={NewRegister}
                options={{headerShown: false}}
              />

              <Stack.Screen
                name="ViewAlpaca"
                component={ViewAlpaca}
                options={{headerShown: false}}
              />

              <Stack.Screen
                name="TransactionHistory"
                component={TransactionHistory}
                options={{headerShown: false}}
              />

              <Stack.Screen
                name="Rebalance"
                component={Rebalance}
                options={{headerShown: false}}
              />

              <Stack.Screen
                name="DisallowedTrades"
                component={DisallowedTrades}
                options={{headerShown: false}}
              />

              <Stack.Screen
                name="Social"
                component={Social}
                options={{headerShown: false, headerLeft: null}}
              />

              <Stack.Screen
                name="Socialchatter"
                component={Socialchatter}
                options={{headerShown: false, headerLeft: null}}
              />

              <Stack.Screen
                name="Notification"
                component={Notification}
                options={{headerShown: false, headerLeft: null}}
              />

              <Stack.Screen
                name="IndividualReturns"
                component={IndividualReturns}
                options={{headerShown: false, headerLeft: null}}
              />

              <Stack.Screen
                name="EchoAlert"
                component={EchoAlert}
                options={{headerShown: false}}
              />
            </Stack.Navigator>
          </NavigationContainer>
        </UserDataProvider>
      </View>
    </View>
  );
};

export default App;