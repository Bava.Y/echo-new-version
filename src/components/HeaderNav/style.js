import {Platform} from 'react-native';
import {StyleSheet} from 'react-native';
import theme from '../../utils/theme';

const styles = StyleSheet.create({
  mainContainer: {
    height: 50,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor:theme.secondryColor,
    borderWidth: 0,
    paddingHorizontal: 16,
    elevation: 0,
  },
  title: {
    flex: 1,
    fontSize: Platform.OS == 'ios' ? 16 : 18,
    color: 'white',
    textAlign: 'center',
    fontFamily: theme.mediumFont,
    //backgroundColor:"red",
    marginRight:4.5
   

  },
  /*drdw:{
    flex:1,
    color:"red",
    fontSize:16,
    textAlign: "center",
    backgroundColor:"white",
    justifyContent:"center"
  },*/
  singlesubscription: {
    flex: 1,
    alignSelf: 'center',
    backgroundColor: theme.secondryColor,
    marginHorizontal: 16,
    marginVertical: 8,
    padding: 16,
    borderRadius: 8,
  },
  subscribename: {
    fontSize: Platform.OS == 'ios' ? 16 : 18,
    color: theme.white,
    fontWeight: 'bold',
  },
  subscribeamount: {
    fontSize: Platform.OS == 'ios' ? 16 : 18,
    color: theme.white,
    fontWeight: 'bold',
    textAlign: 'right',
  },
  benefittitle: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    color: theme.white,
    marginTop: 8,
  },
  benefits: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    color: theme.white,
  },
  item: {
    marginHorizontal: 4,
    marginVertical: 4,
  },
  startbtn: {
    width: '45%',
    alignSelf: 'center',
    backgroundColor: theme.secondryColor,
    borderRadius: 4,
    paddingHorizontal: 6,
    paddingVertical: 6,
  },
  starttxt: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    color: theme.white,
    textAlign: 'center',
  },
  steptext: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    color: theme.white,
    textAlign: 'center',
    marginVertical: 20,
  },
  welcomecontainer: {
    backgroundColor: theme.primaryColor,
    borderRadius: 10,
    margin: 10,
    padding: 20,
  },
  welcometxt: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    color: theme.white,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  listBroker:{
    flex:0.760,
    color:'white',
    height: Platform.OS == 'ios' ? 36 : 48,
    borderColor: 'white',
    borderWidth: 2,
    backgroundColor: theme.secondryColor,
    borderRadius: 10,
    marginVertical: 8,
    width:"28%",
    marginLeft:10
  },

  listBrokerText:{
    fontSize: Platform.OS == 'ios' ? 16 : 18,
    //fontWeight: '500',
    color: 'white',
    textAlign: 'left',
    paddingHorizontal: Platform.OS == 'ios' ? 0 : 4,
    fontFamily: theme.mediumFont
  },

  brokerItemText:{
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    fontWeight: '400',
    color: 'black',
    textAlign: 'left',
  },
  listBrokerSelectedItemText:{
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    fontWeight: '400',
    color: theme.secondryColor,
    textAlign: 'left',
    fontFamily: theme.mediumFont
  },
});

export default styles;
