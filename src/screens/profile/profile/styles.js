import {StyleSheet} from 'react-native';
import theme from '../../utils/theme';

const styles = StyleSheet.create({
  viewmore: {
    color: theme.secondryColor,
    textAlign: 'right',
    textDecorationLine: 'underline',
  },
  biodata: {
    color: theme.white,
    textAlign: 'justify',
    lineHeight: 22,
  },
  saveButton: {
    paddingHorizontal: 40,
    paddingVertical: 10,
    marginVertical: 20,
    borderRadius: 50,
    alignSelf: 'center',
    backgroundColor: theme.secondryColor,
  },
});
export default styles;
