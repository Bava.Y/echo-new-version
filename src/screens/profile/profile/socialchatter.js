import AsyncStorage from '@react-native-community/async-storage';
import {useFocusEffect} from '@react-navigation/native';
import React, {useCallback, useEffect, useRef, useState} from 'react';
import {BackHandler, Image, Platform, View} from 'react-native';
import Snackbar from 'react-native-snackbar';
import WebView from 'react-native-webview';
import BottomNav from '../../../components/BottomNav';
import HeaderNav from '../../../components/HeaderNav';
import theme from '../../../utils/theme';

const Socialchatter = () => {
  // Socialchatter Variables
  const [username, setUsername] = useState(null);
  const [password, setPassword] = useState(null);

  // Ref Variables
  const WEBVIEW_REF = useRef(null);

  useEffect(() => {
    if (Platform.OS === 'android') {
      BackHandler.addEventListener('hardwareBackPress', handleBackButton);

      return () => {
        BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
      };
    }
  }, []);

  const handleBackButton = () => {
    if (WEBVIEW_REF.current.canGoBack) {
      WEBVIEW_REF.current.goBack();

      return true;
    }

    return false;
  };

  useFocusEffect(
    useCallback(() => {
      let isFocus = true;

      AsyncStorage.getItem('uname').then((uname) => {
        if (uname) {
          setUsername(uname);
        }
      });

      AsyncStorage.getItem('pwd').then((pwd) => {
        if (pwd) {
          setPassword(pwd);
        }
      });

      return () => {
        isFocus = false;
      };
    }, []),
  );

  const _handleLoadEnd = () => {
    Snackbar.dismiss();

    let myscript = `$("#username").val("${username}");$("#password").val("${password}");$("#login").submit();true;`;

    WEBVIEW_REF.current.injectJavaScript(myscript);
  };

  return (
    <>
      <HeaderNav title="Social" isBack={true} />

      <View style={{flex: 1, backgroundColor: theme.themeColor}}>
        <WebView
        useWebKit={true}
          incognito={true}
          onLoadStart={() => {
            Snackbar.show({
              text: 'Please wait! Loading your social chatter(s)!',
              duration: Snackbar.LENGTH_INDEFINITE,
            });
          }}
          onLoadEnd={_handleLoadEnd}
          onNavigationStateChange={(navState) => {
            WEBVIEW_REF.current.canGoBack = navState.canGoBack;
          }}
          ref={WEBVIEW_REF}
          renderLoading={() => (
            <View
              style={{
                width: '100%',
                height: '100%',
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: theme.themeColor,
              }}>
              <Image
                style={{width: 100, height: 90}}
                source={{
                  uri:
                    'https://testsocial.echoapp.biz/themes/wowonder/img/logo.png',
                }}
              />
            </View>
          )}
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
          startInLoadingState={true}
          source={{uri: 'https://testsocial.echoapp.biz/logout?mode=night'}}
        />
      </View>

      <BottomNav routeName="Social" />
    </>
  );
};

export default Socialchatter;
