import React from 'react';
import {ScrollView, Text, View} from 'react-native';
import styles from './styles';

const FinancialSummary = () => {
  return (
    <>
      <View style={styles.walletContainer}>
        <View style={styles.walletLabelContainer}>
          <Text style={styles.walletLabelTxt}>Echo Wallet Balance</Text>
        </View>

        <View style={styles.walletValueContainer}>
          <Text style={styles.walletValueTxt}>$ 5035.00</Text>
        </View>
      </View>

      <ScrollView
        keyboardShouldPersistTaps={'handled'}
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
        style={[styles.container, {marginVertical: 8}]}>
        <View style={styles.mainsec}>
          <Text style={styles.headingTxt}>Financial Summary</Text>

          <View style={{marginVertical: 8}}>
            <View style={styles.lineContainer}>
              <View style={styles.lineLabelContainer}>
                <Text style={styles.lineLabelTxt}>Investment Portfolio</Text>
              </View>

              <View style={styles.lineValueContainer}>
                <Text style={styles.lineValueTxt}>$ 5035.00</Text>
              </View>
            </View>

            <View style={[styles.lineContainer, {marginTop: 8}]}>
              <View style={styles.lineLabelContainer}>
                <Text style={styles.lineLabelTxt}>Echo Wallet</Text>
              </View>

              <View style={styles.lineValueContainer}>
                <Text style={styles.lineValueTxt}>$ 505.00</Text>
              </View>
            </View>

            <View style={[styles.lineContainer, {marginTop: 8}]}>
              <View style={styles.lineLabelContainer}>
                <Text style={styles.lineLabelTxt}>Paid Channels (10)</Text>
              </View>

              <View style={styles.lineValueContainer}>
                <Text style={styles.lineValueTxt}>$ 505.00</Text>
              </View>
            </View>

            <View style={[styles.lineContainer, {marginTop: 8}]}>
              <View style={styles.lineLabelContainer}>
                <Text style={styles.lineLabelTxt}>Echoing</Text>
              </View>

              <View style={styles.lineValueContainer}>
                <Text style={styles.lineValueRedTxt}>$ 505.00</Text>
              </View>
            </View>

            <View style={[styles.lineContainer, {marginTop: 8}]}>
              <View style={styles.lineLabelContainer}>
                <Text style={styles.lineLabelTxt}>Echors</Text>
              </View>

              <View style={styles.lineValueContainer}>
                <Text style={styles.lineValueTxt}>$ 505.00</Text>
              </View>
            </View>

            <View style={[styles.lineContainer, {marginTop: 8}]}>
              <View style={styles.lineLabelContainer}>
                <Text style={styles.lineLabelTxt}>Subscriptions (Premium)</Text>
              </View>

              <View style={styles.lineValueContainer}>
                <Text style={styles.lineValueRedTxt}>$ 505.00</Text>
              </View>
            </View>
          </View>

          <View style={[styles.lineContainer, {marginTop: 16}]}>
            <View style={styles.lineLabelContainer}>
              <Text style={styles.finalLabelTxt}>Profit / Loss</Text>
            </View>

            <View style={styles.lineValueContainer}>
              <Text style={styles.finalValueTxt}>$ 5035.00</Text>
            </View>
          </View>
        </View>

        <View style={{paddingVertical: 12}} />
      </ScrollView>
    </>
  );
};

export default FinancialSummary;
