import React, {useState} from 'react';
import {
  Platform,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import BottomNav from '../../components/BottomNav';
import HeaderNav from '../../components/HeaderNav';
import theme from '../../utils/theme';
import styles from './style';

const MetricsResult = ({navigation}) => {
  const [value, setValue] = useState();

  return (
    <>
      <HeaderNav title="Metrics" isBack="true" />

      <View style={styles.container}>
        <View style={{backgroundColor: theme.primaryColor}}>
          <View style={styles.searchSection}>
            <Icon
              style={styles.searchIcon}
              name="search"
              size={16}
              color="grey"
            />

            <TextInput
              style={styles.input}
              placeholder="Search"
              placeholderTextColor="grey"
              onChangeText={(text) => setValue(text)}
              value={value}
            />

            {Boolean(value) ? (
              <Icon
                style={styles.searchIcon}
                name="close"
                size={16}
                color="grey"
                onPress={() => {
                  setValue(null);
                }}
              />
            ) : (
              <Icon
                style={styles.searchIcon}
                name="arrow-right"
                size={16}
                color="grey"
                onPress={() => {}}
              />
            )}
          </View>
        </View>

        <ScrollView
          keyboardShouldPersistTaps="handled"
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}>
          <View style={{paddingHorizontal: 16, paddingVertical: 16}}>
            <TouchableOpacity
              onPress={() => navigation.navigate('LeaderBoard')}>
              <View style={styles.echoinglist}>
                <View
                  style={{
                    flex: 0.5,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}>
                  <View
                    style={{
                      flex: 0.2875,
                      justifyContent: 'center',
                    }}>
                    <View
                      style={{
                        width: 40,
                        height: 40,
                        backgroundColor: theme.themeColor,
                        justifyContent: 'center',
                        alignItems: 'center',
                        borderRadius: 40 / 2,
                      }}>
                      <Text
                        style={{
                          fontSize: Platform.OS == 'ios' ? 12 : 14,
                          fontWeight: '600',
                          color: '#ffffff8a',
                        }}>
                        BK
                      </Text>
                    </View>
                  </View>

                  <View
                    style={{
                      flex: 0.7125,
                      justifyContent: 'center',
                      paddingHorizontal: 4,
                      paddingVertical: 2,
                    }}>
                    <Text
                      style={{
                        fontSize: Platform.OS == 'ios' ? 12 : 14,
                        fontWeight: '600',
                        color: theme.white,
                      }}>
                      Daniel Jones
                    </Text>

                    <Text
                      style={{
                        fontSize: Platform.OS == 'ios' ? 10 : 12,
                        fontWeight: '400',
                        color: '#ffffff8a',
                        marginTop: 2,
                      }}>
                      Ben
                    </Text>
                  </View>
                </View>

                <View
                  style={{
                    flex: 0.25,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Text
                    style={{
                      fontSize: Platform.OS == 'ios' ? 12 : 14,
                      fontWeight: '600',
                      color: theme.white,
                    }}>
                    6.999
                  </Text>
                </View>

                <View
                  style={{
                    flex: 0.25,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Text
                    style={{
                      fontSize: Platform.OS == 'ios' ? 12 : 14,
                      fontWeight: '600',
                      color: theme.white,
                    }}>
                    25 %
                  </Text>
                </View>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => navigation.navigate('LeaderBoard')}>
              <View style={styles.echoinglist}>
                <View
                  style={{
                    flex: 0.5,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}>
                  <View
                    style={{
                      flex: 0.2875,
                      justifyContent: 'center',
                    }}>
                    <View
                      style={{
                        width: 40,
                        height: 40,
                        backgroundColor: theme.themeColor,
                        justifyContent: 'center',
                        alignItems: 'center',
                        borderRadius: 40 / 2,
                      }}>
                      <Text
                        style={{
                          fontSize: Platform.OS == 'ios' ? 12 : 14,
                          fontWeight: '600',
                          color: '#ffffff8a',
                        }}>
                        TP
                      </Text>
                    </View>
                  </View>

                  <View
                    style={{
                      flex: 0.7125,
                      justifyContent: 'center',
                      paddingHorizontal: 4,
                      paddingVertical: 2,
                    }}>
                    <Text
                      style={{
                        fontSize: Platform.OS == 'ios' ? 12 : 14,
                        fontWeight: '600',
                        color: theme.white,
                      }}>
                      Kathi Soohey
                    </Text>

                    <Text
                      style={{
                        fontSize: Platform.OS == 'ios' ? 10 : 12,
                        fontWeight: '400',
                        color: '#ffffff8a',
                        marginTop: 2,
                      }}>
                      Ben
                    </Text>
                  </View>
                </View>

                <View
                  style={{
                    flex: 0.25,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Text
                    style={{
                      fontSize: Platform.OS == 'ios' ? 12 : 14,
                      fontWeight: '600',
                      color: theme.white,
                    }}>
                    6.999
                  </Text>
                </View>

                <View
                  style={{
                    flex: 0.25,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Text
                    style={{
                      fontSize: Platform.OS == 'ios' ? 12 : 14,
                      fontWeight: '600',
                      color: theme.white,
                    }}>
                    25 %
                  </Text>
                </View>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => navigation.navigate('LeaderBoard')}>
              <View style={styles.echoinglist}>
                <View
                  style={{
                    flex: 0.5,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}>
                  <View
                    style={{
                      flex: 0.2875,
                      justifyContent: 'center',
                    }}>
                    <View
                      style={{
                        width: 40,
                        height: 40,
                        backgroundColor: theme.themeColor,
                        justifyContent: 'center',
                        alignItems: 'center',
                        borderRadius: 40 / 2,
                      }}>
                      <Text
                        style={{
                          fontSize: Platform.OS == 'ios' ? 12 : 14,
                          fontWeight: '600',
                          color: '#ffffff8a',
                        }}>
                        BK
                      </Text>
                    </View>
                  </View>

                  <View
                    style={{
                      flex: 0.7125,
                      justifyContent: 'center',
                      paddingHorizontal: 4,
                      paddingVertical: 2,
                    }}>
                    <Text
                      style={{
                        fontSize: Platform.OS == 'ios' ? 12 : 14,
                        fontWeight: '600',
                        color: theme.white,
                      }}>
                      Daniel Jones
                    </Text>

                    <Text
                      style={{
                        fontSize: Platform.OS == 'ios' ? 10 : 12,
                        fontWeight: '400',
                        color: '#ffffff8a',
                        marginTop: 2,
                      }}>
                      Ben
                    </Text>
                  </View>
                </View>

                <View
                  style={{
                    flex: 0.25,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Text
                    style={{
                      fontSize: Platform.OS == 'ios' ? 12 : 14,
                      fontWeight: '600',
                      color: theme.white,
                    }}>
                    6.999
                  </Text>
                </View>

                <View
                  style={{
                    flex: 0.25,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Text
                    style={{
                      fontSize: Platform.OS == 'ios' ? 12 : 14,
                      fontWeight: '600',
                      color: theme.white,
                    }}>
                    25 %
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>

      <BottomNav routeName="Discover People" />
    </>
  );
};

export default MetricsResult;
