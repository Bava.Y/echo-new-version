const environmentTypes = {
    production: {
      ameritradeBaseURL: 'https://api.tdameritrade.com/v1/',
      ameritradeClientId: '2GJBIMQJIO5JRUNTEHN3VENJWAS08QTQ',
      ameritradeRedirectURI: 'http://echoapp.biz/',
      baseURL: 'https://test.echoapp.biz/portal/api/U1/',
      payPalBaseURL: 'https://api.sandbox.paypal.com/v1/',
      plaidBaseURL: 'https://development.plaid.com/',
      plaidClientId: '6019727bdd4284000f36c432',
      plaidClientName: 'Echo Financial Corporation',
      plaidCountryCodes: ['US'],
      plaidSecretKey: '2837b209ae2f05b9b330bb8f677668',
    },
    staging: {
      ameritradeBaseURL: 'https://api.tdameritrade.com/v1/',
      ameritradeClientId: '2GJBIMQJIO5JRUNTEHN3VENJWAS08QTQ',
      ameritradeRedirectURI: 'http://echoapp.biz/',
      baseURL: 'https://test.echoapp.biz/portal/api/U1/',
      payPalBaseURL: 'https://api.sandbox.paypal.com/v1/',
      plaidBaseURL: 'https://development.plaid.com/',
      plaidClientId: '6019727bdd4284000f36c432',
      plaidClientName: 'Echo Financial Corporation',
      plaidCountryCodes: ['US'],
      plaidSecretKey: '2837b209ae2f05b9b330bb8f677668',
    },
    development: {
      ameritradeBaseURL: 'https://api.tdameritrade.com/v1/',
      ameritradeClientId: '2GJBIMQJIO5JRUNTEHN3VENJWAS08QTQ',
      ameritradeRedirectURI: 'http://echoapp.biz/',
      baseURL: 'https://dev.echoapp.biz/portal/api/U1/',
      payPalBaseURL: 'https://api.sandbox.paypal.com/v1/',
      plaidBaseURL: 'https://development.plaid.com/',
      plaidClientId: '6019727bdd4284000f36c432',
      plaidClientName: 'Echo Financial Corporation',
      plaidCountryCodes: ['US'],
      plaidSecretKey: '2837b209ae2f05b9b330bb8f677668',
    },
  
    Optimize: {
      ameritradeBaseURL: 'https://api.tdameritrade.com/v1/',
      ameritradeClientId: '2GJBIMQJIO5JRUNTEHN3VENJWAS08QTQ',
      ameritradeRedirectURI: 'http://echoapp.biz/',
      baseURL: 'https://optimize.echoapp.biz/portal/api/U1/',
      payPalBaseURL: 'https://api.sandbox.paypal.com/v1/',
      plaidBaseURL: 'https://development.plaid.com/',
      plaidClientId: '6019727bdd4284000f36c432',
      plaidClientName: 'Echo Financial Corporation',
      plaidCountryCodes: ['US'],
      plaidSecretKey: '2837b209ae2f05b9b330bb8f677668',
    },
  };
  
  
  
  
  
  const setEnvironment = () => {
    // Insert Current Platform (e.g. development, production, staging, Optimize etc)
    const environment = 'staging';
  
    return {...environmentTypes[environment], currentEnvironment: environment};
  };
  
  const currentEnvironment = setEnvironment();
  
  module.exports = currentEnvironment;
  