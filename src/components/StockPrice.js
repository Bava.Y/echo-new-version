import React, {useEffect, useState} from 'react';
import {Platform, StyleSheet, Text} from 'react-native';

const StockPrice = ({price}) => {
  const [amount, setAmount] = useState({});

  useEffect(() => {
    let ac = new AbortController();

    let val = price.toString();
    let dollor = val.substring(0, val.indexOf('.'));
    let cents = val.substring(val.indexOf('.') + 1);

    setAmount({
      dollor: dollor,
      cents: cents,
    });

    return () => {
      ac.abort();
    };
  }, []);

  return (
    <Text>
      <Text style={styles.smalltxt}>$ </Text>
      <Text style={styles.bigtxt}>{amount.dollor}.</Text>
      <Text style={styles.smalltxt}>{amount.cents}</Text>
    </Text>
  );
};

export default StockPrice;

const styles = StyleSheet.create({
  smalltxt: {
    fontSize: Platform.OS == 'ios' ? 12 : 14,
    color: 'white',
    fontWeight: 'bold',
    paddingVertical: 8,
  },
  bigtxt: {
    fontSize: Platform.OS == 'ios' ? 22 : 24,
    fontFamily: 'Roboto',
    color: 'white',
    fontWeight: 'bold',
    paddingVertical: 8,
  },
});
