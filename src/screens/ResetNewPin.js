import {StackActions} from '@react-navigation/native';
import React, {useRef, useState} from 'react';
import {Image, Text, View} from 'react-native';
import ReactNativePinView from 'react-native-pin-view';
import {CANCEL, OK} from '../assets/images';
import style from '../styles/mpinstyle';

const ResetNewPin = ({navigation, route}) => {
  const pinView = useRef(null);
  const [otp, setOtp] = useState('');

  const onTextChange = (text) => {
    setOtp(text);
    if (text.length == 4) {
      navigation.dispatch(
        StackActions.replace('ResetNewCPIN', {
          previousOtp: text,
          userInfo: route.params.userInfo,
        }),
      );
    }
  };

  return (
    <View style={style.container}>
      <Text style={style.verifyText}>Set PIN</Text>

      <Text style={style.mpinText}>Confirm your 4-digit 
PIN</Text>

      <ReactNativePinView
        inputSize={20}
        ref={pinView}
        pinLength={4}
        buttonSize={60}
        buttonAreaStyle={{
          marginTop: 24,
        }}
        inputViewEmptyStyle={{
          borderColor: '#FFF',
          backgroundColor: 'transparent',
          borderWidth: 1,
        }}
        inputViewFilledStyle={{
          backgroundColor: '#FFF',
        }}
        buttonViewStyle={{
          borderColor: '#FFF',
          borderWidth: 1,
        }}
        onButtonPress={(key) => {
          if (key === 'custom_left') {
            pinView.current.clear();
          }
          if (key === 'custom_right') {
            if (otp.length == 4) {
              navigation.dispatch(
                StackActions.replace('ResetNewCPIN', {
                  previousOtp: otp,
                  userInfo: route.params.userInfo,
                }),
              );
            }
          }
        }}
        customLeftButton={
          <View>
            <Image source={CANCEL} style={style.customButton} />
          </View>
        }
        customRightButton={
          <View pointerEvents="none">
            <Image source={OK} style={style.customButton} />
          </View>
        }
        onValueChange={onTextChange}
      />
    </View>
  );
};

export default ResetNewPin;
