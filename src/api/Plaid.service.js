import axiosInstance from '../utils/PlaidApiCall';

const getLinkToken = async (body) => {
  return await axiosInstance
    .post(`link/token/create`, body, {
      headers: {
        'Content-Type': 'application/json',
      },
    })
    .then((res) => {
      return res.data;
    })
    .catch(() => {
      return false;
    });
};

const getAccessToken = async (body) => {
  return await axiosInstance
    .post(`item/public_token/exchange`, body, {
      headers: {
        'Content-Type': 'application/json',
      },
    })
    .then((res) => {
      return res.data;
    })
    .catch(() => {
      return false;
    });
};

export const plaid = {
  getLinkToken,
  getAccessToken,
};
