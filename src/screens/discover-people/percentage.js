import React from 'react';
import {ScrollView, Text, View} from 'react-native';
import theme from '../../utils/theme';
import styles from './style';

const DiscoverPeopleProfile = () => {
  return (
    <View style={{marginHorizontal: 16, marginVertical: 16}}>
      <ScrollView
        keyboardShouldPersistTaps={'handled'}
        nestedScrollEnabled={true}
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
        style={{height: '91.625%', borderRadius: 8}}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-around',
            backgroundColor: '#17BCC9',
            borderTopLeftRadius: 8,
            borderTopRightRadius: 8,
          }}>
          <Text style={styles.percentagedata}></Text>

          <Text style={styles.percentagedata}>Daniel's Return</Text>

          <Text style={styles.percentagedata}>Return S&P 500</Text>
        </View>

        <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
          <Text
            style={[
              styles.percentagedata,
              {backgroundColor: theme.secondryColor},
            ]}>
            Today
          </Text>

          <Text style={[styles.percentagedata, {backgroundColor: '#AADEE7'}]}>
            1.2%
          </Text>

          <Text style={[styles.percentagedata, {backgroundColor: '#AADEE7'}]}>
            1.2%
          </Text>
        </View>

        <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
          <Text
            style={[
              styles.percentagedata,
              {backgroundColor: theme.secondryColor},
            ]}>
            1 Week
          </Text>

          <Text style={[styles.percentagedata, {backgroundColor: '#AADEE7'}]}>
            1.2%
          </Text>

          <Text style={[styles.percentagedata, {backgroundColor: '#AADEE7'}]}>
            1.2%
          </Text>
        </View>

        <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
          <Text
            style={[
              styles.percentagedata,
              {backgroundColor: theme.secondryColor},
            ]}>
            1 Month
          </Text>

          <Text style={[styles.percentagedata, {backgroundColor: '#AADEE7'}]}>
            1.2%
          </Text>

          <Text style={[styles.percentagedata, {backgroundColor: '#AADEE7'}]}>
            1.2%
          </Text>
        </View>

        <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
          <Text
            style={[
              styles.percentagedata,
              {backgroundColor: theme.secondryColor},
            ]}>
            1 Quarter
          </Text>

          <Text style={[styles.percentagedata, {backgroundColor: '#AADEE7'}]}>
            1.2%
          </Text>

          <Text style={[styles.percentagedata, {backgroundColor: '#AADEE7'}]}>
            1.2%
          </Text>
        </View>

        <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
          <Text
            style={[
              styles.percentagedata,
              {backgroundColor: theme.secondryColor},
            ]}>
            6 Month
          </Text>

          <Text style={[styles.percentagedata, {backgroundColor: '#AADEE7'}]}>
            1.2%
          </Text>

          <Text style={[styles.percentagedata, {backgroundColor: '#AADEE7'}]}>
            1.2%
          </Text>
        </View>

        <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
          <Text
            style={[
              styles.percentagedata,
              {backgroundColor: theme.secondryColor},
            ]}>
            Year-to-Date
          </Text>

          <Text style={[styles.percentagedata, {backgroundColor: '#AADEE7'}]}>
            1.2%
          </Text>

          <Text style={[styles.percentagedata, {backgroundColor: '#AADEE7'}]}>
            1.2%
          </Text>
        </View>

        <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
          <Text
            style={[
              styles.percentagedata,
              {backgroundColor: theme.secondryColor},
            ]}>
            1 Year
          </Text>

          <Text style={[styles.percentagedata, {backgroundColor: '#AADEE7'}]}>
            1.2%
          </Text>

          <Text style={[styles.percentagedata, {backgroundColor: '#AADEE7'}]}>
            1.2%
          </Text>
        </View>

        <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
          <Text
            style={[
              styles.percentagedata,
              {backgroundColor: theme.secondryColor},
            ]}>
            2 Year
          </Text>

          <Text style={[styles.percentagedata, {backgroundColor: '#AADEE7'}]}>
            1.2%
          </Text>

          <Text style={[styles.percentagedata, {backgroundColor: '#AADEE7'}]}>
            1.2%
          </Text>
        </View>

        <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
          <Text
            style={[
              styles.percentagedata,
              {backgroundColor: theme.secondryColor},
            ]}>
            3 Year
          </Text>

          <Text style={[styles.percentagedata, {backgroundColor: '#AADEE7'}]}>
            1.2%
          </Text>

          <Text style={[styles.percentagedata, {backgroundColor: '#AADEE7'}]}>
            1.2%
          </Text>
        </View>

        <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
          <Text
            style={[
              styles.percentagedata,
              {backgroundColor: theme.secondryColor},
            ]}>
            5 Year
          </Text>

          <Text style={[styles.percentagedata, {backgroundColor: '#AADEE7'}]}>
            1.2%
          </Text>

          <Text style={[styles.percentagedata, {backgroundColor: '#AADEE7'}]}>
            1.2%
          </Text>
        </View>

        <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
          <Text
            style={[
              styles.percentagedata,
              {backgroundColor: theme.secondryColor},
            ]}>
            Since Inception
          </Text>

          <Text style={[styles.percentagedata, {backgroundColor: '#AADEE7'}]}>
            1.2%
          </Text>

          <Text style={[styles.percentagedata, {backgroundColor: '#AADEE7'}]}>
            1.2%
          </Text>
        </View>
      </ScrollView>
    </View>
  );
};

export default DiscoverPeopleProfile;
