import React from 'react';
import {Platform, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import theme from '../utils/theme';

const RadioButton = ({options, isOption, onChange, selected}) => {
  return (
    <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
      {options &&
        options.map((res, index) => {
          return (
            <View key={res.id} style={styles.container}>
              {isOption ? (
                <Text> {String.fromCharCode(97 + index).toUpperCase()})</Text>
              ) : (
                <TouchableOpacity
                  style={styles.radioCircle}
                  onPress={() => {
                    onChange(res);
                  }}>
                  {selected === res.id && <View style={styles.selectedRb} />}
                </TouchableOpacity>
              )}
              <TouchableOpacity
                onPress={() => {
                  onChange(res);
                }}>
                <Text style={styles.radioText}>{res.value}</Text>
              </TouchableOpacity>
            </View>
          );
        })}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  radioText: {
    fontSize: Platform.OS == 'ios' ? 12 : 14,
    color: '#ffffffc9',
    fontFamily: theme.mediumFont,
    marginHorizontal: 4,
  },
  radioCircle: {
    height: 16,
    width: 16,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: theme.secondryColor,
    borderWidth: 2,
    borderRadius: 16 / 2,
  },
  selectedRb: {
    width: 8,
    height: 8,
    backgroundColor: theme.secondryColor,
    borderRadius: 8 / 2,
  },
});

export default RadioButton;
