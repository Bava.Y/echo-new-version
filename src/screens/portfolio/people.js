import {useFocusEffect, useNavigation} from '@react-navigation/native';
import moment from 'moment';
import React, {useCallback, useContext, useEffect, useState} from 'react';
import {
  ActivityIndicator,
  Image,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  FlatList,
} from 'react-native';
import Modal from 'react-native-modal';
import Snackbar from 'react-native-snackbar';
import Icon from 'react-native-vector-icons/FontAwesome';
import {
  default as Ionicon,
  default as Ionicons,
} from 'react-native-vector-icons/Ionicons';
import {portfolio} from '../../api';
import AmeritradeAlert from '../../components/AmeritradeAlert';
import Loading from '../../components/Loading';
import {UserDataContext} from '../../context/UserDataContext';
import axiosInstance from '../../utils/Apicall';
import theme from '../../utils/theme';
import {PROFILE} from './../../assets/images/index';
import PortfolioChart from './chart';
//import Date from './date';
import styles from './style';
import DateTimePicker from '@react-native-community/datetimepicker';

const PeopleTab = () => {
  // PeopleTab Variables
  const [chartOrderData, setChartOrderData] = useState(null);
  const [people, setPeople] = useState(null);
  const [unEchoedPeople, setUnEchoedPeople] = useState(null);
  const [editData, setEditData] = useState(null);
  const [alphacaAllocation, setAlphacaAllocation] = useState(null);
  const [ameritradeAllocation, setAmeritradeAllocation] = useState(null);
  const [chosenAlphaca, setChosenAlphaca] = useState(false);
  const [chosenTDAmeritrade, setChosenTDAmeritrade] = useState(false);
  const [alloationStatus, setAlloationStatus] = useState(false);
  const [returnsValue, setReturnsValue] = useState(null);
  const [returnsAmount, setReturnsAmount] = useState(null);
  console.log('loplpooioop>>>>>>', unEchoedPeople);
  // Context Variables
  const {
    alpacaConfigStatus,
    ameritradeAlertStatus,
    portfolioConnection,
    portfolioTimePeriod,
    setAmeritradeAlertStatus,
    tdConfigStatus,
    token,
  } = useContext(UserDataContext);

  // Date Variables
  const [datePicker, setDatePicker] = useState(false);
  const [toDatePicker, setDoDatePicker] = useState(false);

  const [date, setDate] = useState(null);
  const [toDate, setDoDate] = useState(null);
  // Error Variables
  const [alphacaError, setAlphacaError] = useState(false);
  const [chosenAlphacaError, setChosenAlphacaError] = useState(false);
  const [ameritradeError, setAmeritradeError] = useState(false);
  const [chosenAmeritradeError, setChosenAmeritradeError] = useState(false);
  const [textInputError, setTextInputError] = useState(false);
  const [chosenError, setChosenError] = useState(false);

  // Other Variables
  const navigation = useNavigation();
  const [loadingStatus, setLoadingStatus] = useState(false);
  const [chartLoading, setChartLoading] = useState(false);
  const [isModalVisible, setModalVisible] = useState(false);
  const [filterStatus, setFilterStatus] = useState(false);
  const [echoedPeople, setEchoedPeople] = useState(null);
  const [peopleScr, setPeopleScr] = useState(true);

  useFocusEffect(
    useCallback(() => {
      let isFocus = true;

      // setFromDate(moment());
      // setToDate(moment());
      setFilterStatus(false);
      getOrdersChart();

      return () => {
        isFocus = false;
      };
    }, [portfolioConnection, portfolioTimePeriod]),
  );

  useEffect(() => {
    // getOrdersChart();
  }, [filterStatus]);

  function onDateSelected(event, value) {
    setDatePicker(false);
    setDate(moment(value).format('DD-MM-YYYY'));
  }

  function onToDateSelected(event, value) {
    setDoDatePicker(false);
    setDoDate(moment(value).format('DD-MM-YYYY'));
  }

  const getOrdersChart = async () => {
    setChartLoading(true);

    let endpoint = `echoing/chart/${portfolioConnection}`;

    if (!!portfolioTimePeriod) {
      endpoint = endpoint + '&time_period=' + portfolioTimePeriod;
    }

    const response = await portfolio.getOrdersChart(endpoint);

    const res = response.data;

    if (res) {
      if (response.keyword == 'success') {
        let orderArr = [];

        if (res.length != 0) {
          res?.map(item => {
            orderArr.push({
              name: `${item?.echo_customer_name} (${item?.percentage}%)`,
              x: `${item?.percentage}%`,
              y: Number(item?.percentage / 30),
            });
          });

          setChartOrderData(orderArr);
          setReturnsValue(res.portfolio_returns || null);
          setReturnsAmount(res.portfolio_returns_amount || null);
        }
      } else {
        setChartOrderData([]);
        setReturnsValue(null);
        setReturnsAmount(null);
      }
    } else {
      setChartOrderData([]);
      setReturnsValue(null);
      setReturnsAmount(null);
    }

    setChartLoading(false);
  };

  useFocusEffect(
    useCallback(() => {
      let ac = new AbortController();

      getPeople();
      getEchoingPeople();
      return () => {
        ac.abort();
      };
    }, []),
  );

  const getPeople = async () => {
    setUnEchoedPeople(null);

    let path = 'unechoing_data_list';

    let response = await axiosInstance.get(path, {
      headers: {
        Authorization: token,
      },
    });

    if (response && response.data && response.data.keyword === 'success') {
      await setUnEchoedPeople(response?.data?.data || []);

      await setAlloationStatus(response.data.echo_percentage_allocation);
    } else {
      await setUnEchoedPeople([]);
    }
  };

  const getEchoingPeople = async () => {
    setPeople(null);

    let path = 'echoing_data_list';

    let response = await axiosInstance.get(path, {
      headers: {
        Authorization: token,
      },
    });

    if (response) {
      if (response && response.data && response.data.keyword == 'success') {
        await setPeople(response?.data?.data || []);
        await setAlloationStatus(response.data.echo_percentage_allocation);
        // await setUnEchoedPeople(res?.data?.unechoing_data || []);
      } else {
        await setPeople([]);
        // await setUnEchoedPeople([]);
      }
    }
  };

  // const handleDatePicker = (selectedDate, status) => {
  //   switch (status) {
  //     case 'from':
  //       setFromDate(selectedDate);
  //       break;

  //     case 'to':
  //       setToDate(selectedDate);
  //       break;
  //   }
  // };

  const getOrdersChartPeople = async () => {
    setChartLoading(true);

    let endpoint = `people/chart/${id}/${portfolioConnection}`;

    if (!!portfolioTimePeriod) {
      endpoint = endpoint + '&time_period=' + chartRange;
    }

    const response = await portfolio.getOrdersChart(endpoint);

    const res = response.data;

    if (res) {
      if (response.keyword == 'success') {
        let orderArr = [];

        if (res.length != 0) {
          for (let i = 0; i < res.chart.length; i++) {
            orderArr.push({
              name: `${res.chart[i].symbol} (${res.chart[i].percentage}%)`,
              x: `${res.chart[i].percentage}%`,
              y: Number(res.chart[i].percentage / 30),
            });
          }

          setChartOrderData(orderArr);
          setReturnsValue(res.portfolio_returns || null);
          setReturnsAmount(res.portfolio_returns_amount || null);
        }
      } else {
        setChartOrderData([]);
        setReturnsValue(null);
        setReturnsAmount(null);
      }
    } else {
      setChartOrderData([]);
      setReturnsValue(null);
      setReturnsAmount(null);
    }

    setChartLoading(false);
  };

  const getOrdersChartTime = async () => {
    setChartLoading(true);

    let endpoint = `people/chart/${id}/${portfolioConnection}?from_date=${moment(
      date,
      'MM-DD-YYYY',
    ).format('YYYY-MM-DD')}&to_date=${moment(toDate, 'MM-DD-YYYY').format(
      'YYYY-MM-DD',
    )}`;
    const response = await portfolio.getOrdersChart(endpoint);
    const res = response.data;

    if (res) {
      if (response.keyword == 'success') {
        let orderArr = [];

        if (res.length != 0) {
          for (let i = 0; i < res.chart.length; i++) {
            orderArr.push({
              name: `${res.chart[i].symbol} (${res.chart[i].percentage}%)`,
              x: `${res.chart[i].percentage}%`,
              y: Number(res.chart[i].percentage / 30),
            });
          }

          setChartOrderData(orderArr);
          setReturnsValue(res.portfolio_returns || null);
          setReturnsAmount(res.portfolio_returns_amount || null);
        }
      } else {
        setChartOrderData([]);
        setReturnsValue(null);
        setReturnsAmount(null);
      }
    } else {
      setChartOrderData([]);
      setReturnsValue(null);
      setReturnsAmount(null);
    }

    setChartLoading(false);
  };

  const toggleModal = () => {
    setModalVisible(!isModalVisible);
  };

  const checkbox = () => {
    setChosenAlphacaError(false);

    setChosenError(false);

    setChosenAlphaca(!chosenAlphaca);
  };

  const checkbox1 = () => {
    setChosenAmeritradeError(false);

    setChosenError(false);

    setChosenTDAmeritrade(!chosenTDAmeritrade);
  };

  const updatePercentage = async () => {
    if (checkFormCondition()) {
      setLoadingStatus(true);

      const requestData = handleFormRequestData();

      let response = await axiosInstance.post('echoFollower', requestData, {
        headers: {
          Authorization: token,
        },
      });

      if (response && response.data) {
        setLoadingStatus(false);

        if (response.data.keyword === 'success') {
          Snackbar.show({
            text: response.data.message,
            duration: Snackbar.LENGTH_SHORT,
          });

          toggleModal();

          clearFormStates();

          getPeople();
        } else {
          if (response.data.message.hasOwnProperty('error')) {
            switch (response.data.message.error) {
              case 'invalid_grant':
                handleAmeritradeAlertStatus();
                break;

              default:
                alert(response.data.message.error);

                break;
            }
          } else {
            alert(
              typeof response.data.message == 'string'
                ? response.data.message
                : 'Unable to echo person',
            );
          }
        }
      } else {
        setLoadingStatus(false);

        alert('Unable to update echo percentage');
      }
    } else {
      handleErrorValidation();
    }
  };

  const checkFormCondition = () => {
    if (alpacaConfigStatus && tdConfigStatus) {
      if (
        alphacaConditions() &&
        chosenAlphaca &&
        !ameritradeConditions() &&
        !chosenTDAmeritrade
      ) {
        return true;
      } else if (
        ameritradeConditions() &&
        chosenTDAmeritrade &&
        !alphacaConditions() &&
        !chosenAlphaca
      ) {
        return true;
      } else if (
        alphacaConditions() &&
        chosenAlphaca &&
        ameritradeConditions() &&
        chosenTDAmeritrade
      ) {
        return true;
      } else {
        return false;
      }
    } else {
      if (alpacaConfigStatus) {
        if (alphacaConditions() && chosenAlphaca) {
          return true;
        } else {
          return false;
        }
      }

      if (tdConfigStatus) {
        if (ameritradeConditions() && chosenTDAmeritrade) {
          return true;
        } else {
          return false;
        }
      }
    }
  };

  const alphacaConditions = () => {
    if (handleAlphacaConditions()) {
      return false;
    } else {
      return true;
    }
  };

  const handleAlphacaConditions = () => {
    if (alloationStatus) {
      return (
        !alphacaAllocation || alphacaAllocation < 0 || alphacaAllocation > 100
      );
    } else {
      return !alphacaAllocation || alphacaAllocation < 0;
    }
  };

  const ameritradeConditions = () => {
    if (handleAmeritradeConditions()) {
      return false;
    } else {
      return true;
    }
  };

  const handleAmeritradeConditions = () => {
    if (alloationStatus) {
      return (
        !ameritradeAllocation ||
        ameritradeAllocation < 0 ||
        ameritradeAllocation > 100
      );
    } else {
      return !ameritradeAllocation || ameritradeAllocation < 0;
    }
  };

  const handleFormRequestData = () => {
    let requestData = new FormData();
    requestData.append('followed_id', editData.followed_id);
    requestData.append('status', 1);

    alphacaConditions() && [
      requestData.append('percentage', alphacaAllocation),
      requestData.append('enable_alpaca', chosenAlphaca),
    ];
    ameritradeConditions() && [
      requestData.append('td_percentage', ameritradeAllocation),
      requestData.append('enable_td', chosenTDAmeritrade),
    ];

    return requestData;
  };

  const handleAmeritradeAlertStatus = () => {
    setAmeritradeAlertStatus(!ameritradeAlertStatus);
  };

  const clearFormStates = () => {
    setAlphacaAllocation(null);
    setAmeritradeAllocation(null);
    setChosenAlphaca(false);
    setChosenTDAmeritrade(false);
  };

  const handleErrorValidation = () => {
    if (alpacaConfigStatus && tdConfigStatus) {
      if (alphacaConditions() || ameritradeConditions()) {
        alphacaConditions() &&
          setChosenAlphacaError(chosenAlphaca ? false : true);

        ameritradeConditions() &&
          setChosenAmeritradeError(chosenTDAmeritrade ? false : true);
      } else {
        setTextInputError(true);
      }

      if (chosenAlphaca || chosenTDAmeritrade) {
        chosenAlphaca && setAlphacaError(!alphacaConditions());

        chosenTDAmeritrade && setAmeritradeError(!ameritradeConditions());
      } else {
        setChosenError(true);
      }
    } else {
      alpacaConfigStatus && [
        setAlphacaError(!alphacaConditions()),

        setChosenAlphacaError(chosenAlphaca ? false : true),
      ];

      tdConfigStatus && [
        setAmeritradeError(!ameritradeConditions()),

        setChosenAmeritradeError(chosenTDAmeritrade ? false : true),
      ];
    }
  };

  return (
    <View style={styles.container}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-evenly',
          alignItems: 'center',
          backgroundColor: '#2e2e2e52',
          padding: 8,
        }}>
        {/* <Date
          fromDate={fromDate}
          toDate={toDate}
          handleDatePicker={handleDatePicker}
        /> */}
        <View
          style={{
            flex: 0.4,
            backgroundColor: theme.primaryColor,
            paddingHorizontal: 15,
            paddingVertical: 8,
            borderColor: 'transparent',
            borderRadius: 4,
            alignItems: 'center',
          }}>
          <TouchableOpacity
            onPress={() => {
              setDatePicker(true);
            }}>
            <View style={{flexDirection: 'row'}}>
              <Ionicons name="calendar-outline" size={18} color={theme.white} />
              <Text style={{color: 'white', textAlign: 'center'}}>
                {Boolean(date) ? date : 'MM-DD-YYYY'}
              </Text>
            </View>
          </TouchableOpacity>
        </View>
        {datePicker && (
          <DateTimePicker
            value={new Date()}
            display={Platform.OS === 'android' ? 'default' : 'spinner'}
            onChange={onDateSelected}
          />
        )}
        <View
          style={{
            flex: 0.4,
            backgroundColor: theme.primaryColor,
            paddingHorizontal: 15,
            paddingVertical: 8,
            borderColor: 'transparent',
            borderRadius: 4,
            alignItems: 'center',
          }}>
          <TouchableOpacity
            onPress={() => {
              setDoDatePicker(true);
            }}
            //style={Styles.dateContainer}
          >
            <View style={{flexDirection: 'row'}}>
              <Ionicons name="calendar-outline" size={18} color={theme.white} />
              <Text style={{color: 'white', textAlign: 'center'}}>
                {Boolean(toDate) ? toDate : 'MM-DD-YYYY'}
              </Text>
            </View>
          </TouchableOpacity>
        </View>

        {toDatePicker && (
          <DateTimePicker
            value={new Date()}
            display={Platform.OS === 'android' ? 'default' : 'spinner'}
            onChange={onToDateSelected}
          />
        )}
        <View
          style={{
            flex: 0.1,
            justifyContent: 'center',
            alignItems: 'center',
            borderColor: 'transparent',
          }}>
          {filterStatus ? (
            <TouchableOpacity
              onPress={() => {
                setFilterStatus(!filterStatus);
                // handleDatePicker(moment().format('MM-DD-YYYY'), 'from');
                // handleDatePicker(moment().format('MM-DD-YYYY'), 'to');

                setPeopleScr(true), getOrdersChart();
              }}>
              <Ionicons name="close" size={18} color={theme.white} />
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              onPress={() => {
                // setFilterStatus(!filterStatus);
                setFilterStatus(!filterStatus),
                  setPeopleScr(false),
                  getOrdersChartTime();
              }}>
              <Ionicons name="search-outline" size={18} color={theme.white} />
            </TouchableOpacity>
          )}
        </View>
      </View>

      <ScrollView
        keyboardShouldPersistTaps="handled"
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}>
        <View style={{paddingVertical: 8}}>
          <View style={{paddingHorizontal: 8}}>
            {!Boolean(chartOrderData) ? (
              <PortfolioChart loading={chartLoading} />
            ) : (
              <PortfolioChart
                loading={chartLoading}
                from={'People'}
                orderData={chartOrderData}
              />
            )}
          </View>

          <View
            style={{
              backgroundColor: theme.primaryColor,
              paddingHorizontal: 8,
              paddingVertical: 8,
            }}>
            <Text
              style={{
                fontSize: Platform.OS == 'ios' ? 12 : 14,
                color: theme.textColor,
                fontWeight: 'bold',
              }}>
              People You Are echoing
            </Text>
          </View>

          <View style={{paddingHorizontal: 8, paddingVertical: 16}}>
            {Boolean(people) ? (
              people?.length != 0 ? (
                people?.map((data, index) => (
                  <View
                    key={index}
                    style={[
                      styles.companylist,
                      {
                        marginTop: index != 0 ? 8 : 0,
                        paddingHorizontal: 12,
                        paddingVertical: 12,
                      },
                    ]}>
                    <TouchableOpacity
                      style={{
                        flex: 0.425,
                        justifyContent: 'center',
                        alignItems: 'flex-start',
                      }}
                      onPress={() =>
                        navigation.navigate('Echoers', {id: data.followed_id})
                      }>
                      <Text
                        style={{
                          fontSize: Platform.OS == 'ios' ? 14 : 16,
                          color: theme.white,
                          fontWeight: '600',
                        }}>
                        {data.first_name + ' ' + data.last_name}
                      </Text>
                    </TouchableOpacity>

                    {Boolean(data.percentage) && (
                      <View
                        style={{
                          flex: 0.2,
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}>
                        <Text
                          style={{
                            fontSize: Platform.OS == 'ios' ? 14 : 16,
                            color: theme.white,
                            fontWeight: '600',
                            textAlign: 'center',
                          }}>
                          {`${data.percentage} %`}
                        </Text>
                      </View>
                    )}

                    {Boolean(data.td_percentage) && data.td_percentage != 0 && (
                      <View
                        style={{
                          flex: 0.2,
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}>
                        <Text
                          style={{
                            fontSize: Platform.OS == 'ios' ? 14 : 16,
                            color: theme.white,
                            fontWeight: '600',
                            textAlign: 'center',
                          }}>
                          {`${data.td_percentage} %`}
                        </Text>
                      </View>
                    )}

                    {Boolean(data.echoing_id) && data.echoing_id !== 0 && (
                      <TouchableOpacity
                        style={{
                          flex: 0.125,
                          justifyContent: 'center',
                          alignItems: 'flex-end',
                        }}
                        onPress={() => {
                          setEditData(data);

                          alpacaConfigStatus && [
                            setAlphacaAllocation(
                              alloationStatus
                                ? data.percentage
                                : data.allocated_amount,
                            ),
                            setChosenAlphaca(
                              data.enable_alpaca == 1 ? true : false,
                            ),
                          ];

                          tdConfigStatus && [
                            setAmeritradeAllocation(
                              alloationStatus
                                ? data.td_percentage
                                : data.td_allocated_amount,
                            ),
                            setChosenTDAmeritrade(
                              data.enable_td == 1 ? true : false,
                            ),
                          ];

                          toggleModal();
                        }}>
                        <Icon name="edit" color="white" size={16} />
                      </TouchableOpacity>
                    )}
                  </View>
                ))
              ) : (
                <Text
                  style={{
                    fontSize: Platform.OS == 'ios' ? 12 : 14,
                    fontWeight: '600',
                    color: theme.white,
                    textAlign: 'center',
                    paddingHorizontal: 8,
                  }}>
                  No echoing(s) found
                </Text>
              )
            ) : (
              <View style={{paddingHorizontal: 8}}>
                <ActivityIndicator size={'large'} color={theme.secondryColor} />
              </View>
            )}
          </View>

          <View
            style={{
              backgroundColor: theme.primaryColor,
              paddingHorizontal: 8,
              paddingVertical: 8,
            }}>
            <Text
              style={{
                fontSize: Platform.OS == 'ios' ? 12 : 14,
                color: theme.textColor,
                fontWeight: 'bold',
              }}>
              Previous Echoes were unechoing
            </Text>
          </View>

          <View style={{paddingHorizontal: 8, paddingVertical: 16}}>
            {Boolean(unEchoedPeople) ? (
              unEchoedPeople.length != 0 ? (
                unEchoedPeople.map((data, index) => (
                  <View
                    key={index}
                    style={[
                      styles.companylist,
                      {
                        marginTop: index != 0 ? 8 : 0,
                        paddingHorizontal: 12,
                        paddingVertical: 12,
                      },
                    ]}>
                    <TouchableOpacity
                      style={{
                        flex: 0.425,
                        justifyContent: 'center',
                        alignItems: 'flex-start',
                      }}
                      onPress={() =>
                        navigation.navigate('Echoers', {id: data.followed_id})
                      }>
                      <Text
                        style={{
                          fontSize: Platform.OS == 'ios' ? 14 : 16,
                          color: theme.white,
                          fontWeight: '600',
                        }}>
                        {data.first_name + ' ' + data.last_name}
                      </Text>
                    </TouchableOpacity>

                    {Boolean(data.percentage) && (
                      <View
                        style={{
                          flex: 0.2,
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}>
                        <Text
                          style={{
                            fontSize: Platform.OS == 'ios' ? 14 : 16,
                            color: theme.white,
                            fontWeight: '600',
                            textAlign: 'center',
                          }}>
                          {`${data.percentage} %`}
                        </Text>
                      </View>
                    )}

                    {Boolean(data.td_percentage) && data.td_percentage != 0 && (
                      <View
                        style={{
                          flex: 0.2,
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}>
                        <Text
                          style={{
                            fontSize: Platform.OS == 'ios' ? 14 : 16,
                            color: theme.white,
                            fontWeight: '600',
                            textAlign: 'center',
                          }}>
                          {`${data.td_percentage} %`}
                        </Text>
                      </View>
                    )}

                    <View
                      style={{
                        flex: 0.125,
                        paddingHorizontal: 4,
                        paddingVertical: 4,
                      }}
                    />
                  </View>
                ))
              ) : (
                <Text
                  style={{
                    fontSize: Platform.OS == 'ios' ? 12 : 14,
                    fontWeight: '600',
                    color: theme.white,
                    textAlign: 'center',
                    paddingHorizontal: 8,
                  }}>
                  No echoed(s) found
                </Text>
              )
            ) : (
              <View style={{paddingHorizontal: 8}}>
                <ActivityIndicator size={'large'} color={theme.secondryColor} />
              </View>
            )}
          </View>
        </View>

        <View style={{paddingVertical: 12}} />

        {isModalVisible && (
          <Modal isVisible={isModalVisible}>
            <KeyboardAvoidingView
              behavior={Platform.OS === 'ios' ? 'padding' : null}
              keyboardVerticalOffset={Platform.OS === 'ios' ? 40 : 0}
              style={{backgroundColor: theme.primaryColor, borderRadius: 8}}>
              <View
                style={{
                  backgroundColor: theme.secondryColor,
                  borderBottomColor: 'grey',
                  borderBottomWidth: 1,
                  paddingVertical: 8,
                  borderTopLeftRadius: 8,
                  borderTopRightRadius: 8,
                }}>
                <Text
                  style={{
                    fontSize: Platform.OS == 'ios' ? 14 : 16,
                    color: 'white',
                    fontWeight: 'bold',
                    textAlign: 'center',
                  }}>
                  Echo
                </Text>
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  marginHorizontal: 12,
                  marginVertical: 12,
                }}>
                <View
                  style={{
                    flex: 0.15,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Image
                    source={
                      Boolean(editData.photo) ? {uri: editData.photo} : PROFILE
                    }
                    style={{width: 40, height: 40, borderRadius: 40 / 2}}
                  />
                </View>

                <View
                  style={{
                    flex: 0.85,
                    justifyContent: 'center',
                    paddingHorizontal: 8,
                  }}>
                  <Text
                    style={{
                      fontSize: Platform.OS == 'ios' ? 14 : 16,
                      color: 'white',
                      fontWeight: '600',
                    }}>
                    {editData.first_name} {editData.last_name}
                  </Text>

                  <Text
                    style={{
                      fontSize: Platform.OS == 'ios' ? 12 : 14,
                      color: '#ffffffb3',
                      fontWeight: '400',
                      marginTop: 2,
                    }}>
                    {editData.designation}
                  </Text>
                </View>
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  borderBottomColor: 'white',
                  borderBottomWidth: 1,
                  marginHorizontal: 16,
                  marginBottom: 8,
                  paddingBottom: 8,
                }}>
                <Text
                  style={{
                    width: '35%',
                    fontSize: Platform.OS == 'ios' ? 12 : 14,
                    color: theme.greenBg,
                    fontWeight: '600',
                    textAlign: 'left',
                  }}>
                  Accounts
                </Text>

                <Text
                  style={{
                    width: '10%',
                    fontSize: Platform.OS == 'ios' ? 12 : 14,
                    color: theme.greenBg,
                    fontWeight: '600',
                    textAlign: 'center',
                  }}>
                  -
                </Text>

                <Text
                  style={{
                    width: '55%',
                    fontSize: Platform.OS == 'ios' ? 12 : 14,
                    color: theme.greenBg,
                    fontWeight: '600',
                    textAlign: 'left',
                  }}>{`Allowed (${
                  alloationStatus ? '%' : '$'
                }) to this user`}</Text>
              </View>

              {alpacaConfigStatus && (
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    marginHorizontal: 16,
                    marginVertical: 8,
                  }}>
                  <Text
                    style={{
                      width: '35%',
                      fontSize: Platform.OS == 'ios' ? 12 : 14,
                      fontWeight: '800',
                      color: 'white',
                      textAlign: 'left',
                    }}>
                    Alpaca
                  </Text>

                  <Text
                    style={{
                      width: '10%',
                      fontSize: Platform.OS == 'ios' ? 12 : 14,
                      fontWeight: '800',
                      color: theme.greenBg,
                      textAlign: 'center',
                    }}>
                    -
                  </Text>

                  <View>
                    <View
                      style={{
                        width: '55%',
                        flexDirection: 'row',
                        justifyContent: 'flex-start',
                      }}>
                      <TextInput
                        keyboardType={'decimal-pad'}
                        value={alphacaAllocation}
                        onChangeText={text => {
                          setAlphacaError(text ? false : true);

                          setTextInputError(false);

                          setAlphacaAllocation(text);
                        }}
                        style={{
                          width: '100%',
                          fontSize: Platform.OS == 'ios' ? 12 : 14,
                          color: 'white',
                          fontWeight: '400',
                          borderBottomColor: 'white',
                          borderBottomWidth: 1,
                          paddingBottom: 8,
                        }}
                      />

                      <Text
                        style={{
                          fontSize: Platform.OS == 'ios' ? 14 : 16,
                          color: 'white',
                          fontWeight: '400',
                          paddingHorizontal: 4,
                        }}>{`( ${alloationStatus ? '%' : '$'} )`}</Text>
                    </View>

                    {!textInputError && alphacaError && (
                      <Text
                        style={{
                          fontSize: Platform.OS == 'ios' ? 12 : 14,
                          color: 'red',
                          fontWeight: '600',
                          marginTop: 8,
                        }}>
                        {`Enter a valid ${
                          alloationStatus ? 'percentage' : 'amount'
                        }`}
                      </Text>
                    )}
                  </View>
                </View>
              )}

              {tdConfigStatus && (
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    marginHorizontal: 16,
                    marginVertical: 8,
                    marginTop: alpacaConfigStatus ? 16 : 8,
                  }}>
                  <Text
                    style={{
                      width: '35%',
                      fontSize: Platform.OS == 'ios' ? 12 : 14,
                      fontWeight: '800',
                      color: 'white',
                      textAlign: 'left',
                    }}>
                    TD Amiratrade
                  </Text>

                  <Text
                    style={{
                      width: '10%',
                      fontSize: Platform.OS == 'ios' ? 12 : 14,
                      fontWeight: '800',
                      color: theme.greenBg,
                      textAlign: 'center',
                    }}>
                    -
                  </Text>

                  <View>
                    <View
                      style={{
                        width: '55%',
                        flexDirection: 'row',
                        justifyContent: 'flex-start',
                      }}>
                      <TextInput
                        keyboardType={'decimal-pad'}
                        value={ameritradeAllocation}
                        onChangeText={text => {
                          setAmeritradeError(text ? false : true);

                          setTextInputError(false);

                          setAmeritradeAllocation(text);
                        }}
                        style={{
                          width: '100%',
                          fontSize: Platform.OS == 'ios' ? 12 : 14,
                          color: 'white',
                          fontWeight: '400',
                          borderBottomColor: 'white',
                          borderBottomWidth: 1,
                          paddingBottom: 8,
                        }}
                      />

                      <Text
                        style={{
                          fontSize: Platform.OS == 'ios' ? 14 : 16,
                          color: 'white',
                          fontWeight: '400',
                          paddingHorizontal: 4,
                        }}>{`( ${alloationStatus ? '%' : '$'} )`}</Text>
                    </View>

                    {!textInputError && ameritradeError && (
                      <Text
                        style={{
                          fontSize: Platform.OS == 'ios' ? 12 : 14,
                          color: 'red',
                          fontWeight: '600',
                          marginTop: 8,
                        }}>
                        {`Enter a valid ${
                          alloationStatus ? 'percentage' : 'amount'
                        }`}
                      </Text>
                    )}
                  </View>
                </View>
              )}

              {textInputError && (
                <Text
                  style={{
                    fontSize: Platform.OS == 'ios' ? 12 : 14,
                    color: 'red',
                    fontWeight: '600',
                    marginHorizontal: 16,
                    marginTop: 8,
                  }}>
                  {`Enter any one of the ${
                    alloationStatus ? 'percentage' : 'amount'
                  }`}
                </Text>
              )}

              <View
                style={{
                  borderTopColor: 'white',
                  borderTopWidth: 1,
                  marginHorizontal: 16,
                  marginVertical: 8,
                }}
              />

              <View style={{paddingHorizontal: 16, paddingVertical: 8}}>
                <Text
                  style={{
                    fontSize: Platform.OS == 'ios' ? 14 : 16,
                    color: 'white',
                    fontWeight: '800',
                    textDecorationLine: 'underline',
                  }}>
                  Auto Trade:
                </Text>

                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'flex-start',
                    marginVertical: 16,
                  }}>
                  {alpacaConfigStatus && (
                    <TouchableOpacity
                      onPress={() => {
                        checkbox();
                      }}>
                      <View
                        style={{
                          flexDirection: 'row',
                          justifyContent: 'center',
                          marginRight: tdConfigStatus ? 16 : 0,
                        }}>
                        <View
                          style={[
                            {
                              width: 16,
                              height: 16,
                              borderWidth: 1,
                              borderRadius: 2,
                            },
                            chosenAlphaca
                              ? {
                                  backgroundColor: theme.greenBg,
                                  borderColor: theme.greenBg,
                                }
                              : {borderColor: 'white'},
                          ]}>
                          {chosenAlphaca ? (
                            <Ionicon
                              name="checkmark-outline"
                              size={12}
                              color="white"
                              style={{alignSelf: 'center'}}
                            />
                          ) : (
                            <></>
                          )}
                        </View>

                        <Text
                          style={{
                            fontSize: Platform.OS == 'ios' ? 12 : 14,
                            color: 'white',
                            fontWeight: '600',
                            marginHorizontal: 8,
                          }}>
                          Alpaca
                        </Text>
                      </View>
                    </TouchableOpacity>
                  )}

                  {tdConfigStatus && (
                    <TouchableOpacity
                      onPress={() => {
                        checkbox1();
                      }}>
                      <View
                        style={{
                          flexDirection: 'row',
                          justifyContent: 'center',
                        }}>
                        <View
                          style={[
                            {
                              width: 16,
                              height: 16,
                              borderWidth: 1,
                              borderRadius: 2,
                            },
                            chosenTDAmeritrade
                              ? {
                                  backgroundColor: theme.greenBg,
                                  borderColor: theme.greenBg,
                                }
                              : {borderColor: 'white'},
                          ]}>
                          {chosenTDAmeritrade ? (
                            <Ionicon
                              name="checkmark-outline"
                              size={12}
                              color="white"
                              style={{alignSelf: 'center'}}
                            />
                          ) : (
                            <></>
                          )}
                        </View>

                        <Text
                          style={{
                            fontSize: Platform.OS == 'ios' ? 12 : 14,
                            color: 'white',
                            fontWeight: '600',
                            marginHorizontal: 8,
                          }}>
                          TD Amiratrade
                        </Text>
                      </View>
                    </TouchableOpacity>
                  )}
                </View>

                {!chosenError &&
                  (chosenAlphacaError || chosenAmeritradeError) && (
                    <Text
                      style={{
                        fontSize: Platform.OS == 'ios' ? 12 : 14,
                        color: 'red',
                        fontWeight: '600',
                      }}>
                      Please! Enable auto trade for particular account
                    </Text>
                  )}

                {chosenError && (
                  <Text
                    style={{
                      fontSize: Platform.OS == 'ios' ? 12 : 14,
                      color: 'red',
                      fontWeight: '600',
                      marginTop: 8,
                    }}>
                    Please! Enable any one of the auto trade
                  </Text>
                )}

                {alpacaConfigStatus && tdConfigStatus && (
                  <View style={{marginVertical: 8}}>
                    <Text
                      style={{
                        color: 'white',
                        fontWeight: '600',
                        textAlign: 'left',
                      }}>
                      <Text style={{fontSize: Platform.OS == 'ios' ? 14 : 16}}>
                        Note :{' '}
                      </Text>

                      <Text style={{fontSize: Platform.OS == 'ios' ? 12 : 14}}>
                        Should select any one of the account
                      </Text>
                    </Text>
                  </View>
                )}
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginVertical: 8,
                  paddingBottom: 16,
                }}>
                <TouchableOpacity
                  disabled={loadingStatus}
                  style={[
                    {
                      backgroundColor: theme.secondryColor,
                      borderRadius: 4,
                      paddingHorizontal: 12,
                      paddingVertical: 6,
                    },
                    loadingStatus && {
                      paddingHorizontal: 36,
                      paddingVertical: 8,
                    },
                  ]}
                  onPress={() => {
                    updatePercentage();
                  }}>
                  {loadingStatus ? (
                    <ActivityIndicator size={12} color="white" />
                  ) : (
                    <Text
                      style={{
                        fontSize: Platform.OS == 'ios' ? 14 : 16,
                        color: 'white',
                        fontWeight: 'bold',
                        textAlign: 'center',
                      }}>
                      Update
                    </Text>
                  )}
                </TouchableOpacity>

                <TouchableOpacity
                  onPress={() => {
                    clearFormStates();

                    toggleModal();
                  }}>
                  <Text
                    style={{
                      fontSize: Platform.OS == 'ios' ? 14 : 16,
                      color: theme.secondryColor,
                      fontWeight: 'bold',
                      textAlign: 'center',
                      marginHorizontal: 16,
                    }}>
                    Cancel
                  </Text>
                </TouchableOpacity>
              </View>
            </KeyboardAvoidingView>
          </Modal>
        )}

        {/* Hided due to TD Ameritrade Echo App revoked issue */}
        {/* <AmeritradeAlert
          modalStatus={ameritradeAlertStatus}
          close={handleAmeritradeAlertStatus}
        /> */}
      </ScrollView>
    </View>
  );
};

export default PeopleTab;
