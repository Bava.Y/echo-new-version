import React from 'react';
import {FlatList, Platform, Text, View} from 'react-native';
import Loading from '../../components/Loading';
import theme from '../../utils/theme';

const InsideRoaster = ({route}) => {
  const {roast} = route.params;

  const renderItem = ({item}) => <Item item={item} />;

  const Item = ({item}) => (
    <View
      style={{
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginVertical: 8,
      }}>
      <View style={{width: '50%', justifyContent: 'center'}}>
        <Text
          style={{
            fontSize: Platform.OS == 'ios' ? 12 : 14,
            color: 'white',
            fontWeight: '600',
            textAlign: 'left',
          }}>
          {item.entity_name}
        </Text>
      </View>

      <View style={{width: '50%', justifyContent: 'center'}}>
        <Text
          style={{
            fontSize: Platform.OS == 'ios' ? 12 : 14,
            color: 'white',
            fontWeight: '600',
            textAlign: 'right',
          }}>
          {item.position}
        </Text>
      </View>
    </View>
  );

  const Title = () => (
    <View
      style={{
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderBottomColor: 'white',
        borderBottomWidth: 1,
        paddingBottom: 8,
      }}>
      <Text
        style={{
          fontSize: Platform.OS == 'ios' ? 14 : 16,
          color: theme.secondryColor,
          fontWeight: 'bold',
        }}>
        Name
      </Text>

      <Text
        style={{
          fontSize: Platform.OS == 'ios' ? 14 : 16,
          color: theme.secondryColor,
          fontWeight: 'bold',
        }}>
        No of Stocks
      </Text>
    </View>
  );

  return (
    <>
      {Boolean(roast) && Array.isArray(roast) ? (
        roast.length == 0 ? (
          <>
            <FlatList
              keyboardShouldPersistTaps={'handled'}
              showsHorizontalScrollIndicator={false}
              showsVerticalScrollIndicator={false}
              style={{paddingHorizontal: 8, paddingVertical: 8}}
              data={roast}
              renderItem={renderItem}
              keyExtractor={(_item, index) => index.toString()}
              ListHeaderComponent={Title}
            />

            <View style={{paddingVertical: 12}} />
          </>
        ) : (
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontSize: Platform.OS == 'ios' ? 14 : 16,
                color: 'white',
              }}>
              No data found
            </Text>
          </View>
        )
      ) : (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Loading color={'white'} />
        </View>
      )}
    </>
  );
};

export default InsideRoaster;
