import React, { useState } from 'react';
import { View } from 'react-native';
import MultiSelect from 'react-native-multiple-select';
import theme from '../utils/theme';

const SearchInput = (searchdata) => {
  const [search, setSearch] = useState(undefined);

  return (
    <View style={{borderRadius: 10}}>
      <MultiSelect
        hideTags
        items={searchdata}
        uniqueKey="id"
        onSelectedItemsChange={(itemValue) => {
          setSearch(itemValue);
        }}
        selectedItems={search}
        single={true}
        selectText=""
        searchInputPlaceholderText="Search"
        tagRemoveIconColor="transparent"
        tagBorderColor="white"
        tagTextColor="white"
        selectedItemTextColor="white"
        selectedItemIconColor="transparent"
        itemTextColor="white"
        styleInputGroup={{
          backgroundColor: theme.themeColor,
        }}
        styleListContainer={{
          backgroundColor: theme.themeColor,
          maxHeight: 70,
        }}
        searchInputStyle={{
          backgroundColor: theme.themeColor,
        }}
        styleItemsContainer={{
          backgroundColor: theme.themeColor,
        }}
        styleDropdownMenuSubsection={{
          backgroundColor: 'transparent',
          borderColor: 'white',
          borderWidth: 1,
          paddingLeft: 10,
          borderRadius: 50,
        }}
        displayKey="name"
        searchInputStyle={{color: 'white'}}
      />
    </View>
  );
};

export default SearchInput;
