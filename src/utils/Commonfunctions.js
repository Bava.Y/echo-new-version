export const formatText = (str) => {
  let splitStr;

  if (str.toLowerCase().includes('_')) {
    splitStr = str.toLowerCase().split('_');
  } else {
    splitStr = str.toLowerCase().split(' ');
  }

  for (var i = 0; i < splitStr.length; i++) {
    splitStr[i] =
      splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
  }

  return splitStr.join(' ');
};
