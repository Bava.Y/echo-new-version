import {StyleSheet} from 'react-native';
import theme from '../../utils/theme';

const styles = StyleSheet.create({
  bottom: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderWidth: 0,
    elevation: 0,
  },
  navigationItems: {
    flex: 1,
    alignItems: 'center',
    alignSelf: 'center',
    flexDirection: 'column',
    padding: 8,
  },
  icon: {
    height: 25,
    width: 25,
  },
  text: {
    color: theme.white,
    fontSize: 12,
  },
  applogo: {
    width: 40,
    height: 40,
    borderRadius: 40 / 2,
  },
  logosec: {
    width: 60,
    height: 60,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: theme.primaryColor,
    borderRadius: 60 / 2,
    bottom: 34,
  },
  menu: {
    flex: 0.2,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default styles;
