import {useFocusEffect} from '@react-navigation/core';
import React, {useCallback, useContext, useState} from 'react';
import {
  ActivityIndicator,
  KeyboardAvoidingView,
  Platform,
  Text,
  TextInput,
  View,
} from 'react-native';
import {ScrollView, TouchableOpacity} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/FontAwesome';
import Ionicons from 'react-native-vector-icons/Ionicons';
import LITERALS from '../../assets/strings';
import BottomNav from '../../components/BottomNav';
import HeaderNav from '../../components/HeaderNav';
import {UserDataContext} from '../../context/UserDataContext';
import theme from '../../utils/theme';
import styles from './styles';
import Ticker from './ticker';

const Trending = ({navigation}) => {
  const [value, onChangeText] = React.useState('');
  const {tickerData} = useContext(UserDataContext);
  const [data, setData] = useState(null);
  const [loadIndex, setLoadIndex] = useState(4);

  useFocusEffect(
    useCallback(() => {
      let isFocus = true;

      setCustomTickerData();

      return () => {
        isFocus = false;
      };
    }, [loadIndex]),
  );

  const setCustomTickerData = () => {
    if (tickerData.length != 0) {
      if (loadIndex <= tickerData.length) {
        if (loadIndex == 4) {
          handleTickerData();
        } else {
          setTimeout(() => {
            handleTickerData();
          }, 15000);
        }
      } else {
        setLoadIndex(4);
      }
    } else {
      setData([]);
    }
  };

  const handleTickerData = () => {
    const customIndex = loadIndex - 4;

    const splicedTicker = tickerData.slice(customIndex, loadIndex);

    setData(splicedTicker);

    setLoadIndex(loadIndex + 4);
  };

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : null}
      keyboardVerticalOffset={Platform.OS === 'ios' ? 40 : 0}
      style={{flex: 1, backgroundColor: theme.themeColor}}>
      <HeaderNav title={LITERALS.dashboard} isBack={false} />

      <Ticker data={tickerData} />

      <ScrollView
        keyboardShouldPersistTaps={'handled'}
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}>
        <View
          style={{
            marginHorizontal: 8,
            marginVertical: 8,
            paddingHorizontal: 8,
            paddingVertical: 16,
          }}>
          <TouchableOpacity onPress={() => navigation.navigate('Bookmarks')}>
            <View style={{flex: 1, alignSelf: 'flex-end'}}>
              <Ionicons name="book" size={20} color="white" />
            </View>
          </TouchableOpacity>

          <View style={[styles.TrendingsearchSection, {marginTop: 24}]}>
            <Icon
              style={styles.searchIcon}
              name="search"
              size={16}
              color="white"
            />

            <TextInput
              style={styles.searchtext}
              placeholder="Search"
              placeholderTextColor="white"
              onChangeText={(text) => onChangeText(text)}
              value={value}
            />
          </View>

          <View style={styles.trending}>
            <Text
              style={{
                fontSize: Platform.OS == 'ios' ? 16 : 18,
                color: theme.white,
              }}>
              Trending
            </Text>
          </View>

          {data ? (
            data.length != 0 ? (
              <>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    marginTop: 32,
                  }}>
                  <View
                    style={
                      Math.sign(data[0].change) == -1
                        ? styles.downcompanydata
                        : styles.upcompanydata
                    }>
                    <Text numberOfLines={1} style={styles.Companyname}>
                      {data[0].company_name}
                    </Text>

                    <Text style={styles.symbol}>{data[0].symbol}</Text>

                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      <Text
                        style={
                          Math.sign(data[0].change) == -1
                            ? styles.textred
                            : styles.textgreen
                        }>
                        {data[0].latest_price}
                      </Text>

                      <View
                        style={{
                          width: 16,
                          alignSelf: 'center',
                          backgroundColor:
                            Math.sign(data[0].change) == -1 ? 'red' : 'green',
                          marginHorizontal: 8,
                          paddingVertical: 4,
                          borderRadius: 4,
                        }}>
                        <Icon
                          name={
                            Math.sign(data[0].change) == -1
                              ? 'arrow-down'
                              : 'arrow-up'
                          }
                          color="white"
                          size={8}
                          style={{alignSelf: 'center'}}
                        />
                      </View>

                      <Text
                        style={
                          Math.sign(data[0].change) == -1
                            ? styles.textred
                            : styles.textgreen
                        }>
                        {data[0].change}
                      </Text>
                    </View>
                  </View>

                  <View
                    style={
                      Math.sign(data[1].change) == -1
                        ? styles.downcompanydata
                        : styles.upcompanydata
                    }>
                    <Text numberOfLines={1} style={styles.Companyname}>
                      {data[1].company_name}
                    </Text>

                    <Text style={styles.symbol}>{data[1].symbol}</Text>

                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      <Text
                        style={
                          Math.sign(data[1].change) == -1
                            ? styles.textred
                            : styles.textgreen
                        }>
                        {data[1].latest_price}
                      </Text>

                      <View
                        style={{
                          width: 16,
                          alignSelf: 'center',
                          backgroundColor:
                            Math.sign(data[1].change) == -1 ? 'red' : 'green',
                          marginHorizontal: 8,
                          paddingVertical: 4,
                          borderRadius: 4,
                        }}>
                        <Icon
                          name={
                            Math.sign(data[1].change) == -1
                              ? 'arrow-down'
                              : 'arrow-up'
                          }
                          color="white"
                          size={8}
                          style={{alignSelf: 'center'}}
                        />
                      </View>

                      <Text
                        style={
                          Math.sign(data[1].change) == -1
                            ? styles.textred
                            : styles.textgreen
                        }>
                        {data[1].change}
                      </Text>
                    </View>
                  </View>
                </View>

                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    marginTop: 32,
                    marginBottom: 16,
                  }}>
                  <View
                    style={
                      Math.sign(data[2].change) == -1
                        ? styles.downcompanydata
                        : styles.upcompanydata
                    }>
                    <Text numberOfLines={1} style={styles.Companyname}>
                      {data[2].company_name}
                    </Text>

                    <Text style={styles.symbol}>{data[2].symbol}</Text>

                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      <Text
                        style={
                          Math.sign(data[2].change) == -1
                            ? styles.textred
                            : styles.textgreen
                        }>
                        {data[2].latest_price}
                      </Text>

                      <View
                        style={{
                          width: 16,
                          alignSelf: 'center',
                          backgroundColor:
                            Math.sign(data[2].change) == -1 ? 'red' : 'green',
                          marginHorizontal: 8,
                          paddingVertical: 4,
                          borderRadius: 4,
                        }}>
                        <Icon
                          name={
                            Math.sign(data[2].change) == -1
                              ? 'arrow-down'
                              : 'arrow-up'
                          }
                          color="white"
                          size={8}
                          style={{alignSelf: 'center'}}
                        />
                      </View>

                      <Text
                        style={
                          Math.sign(data[2].change) == -1
                            ? styles.textred
                            : styles.textgreen
                        }>
                        {data[2].change}
                      </Text>
                    </View>
                  </View>

                  <View
                    style={
                      Math.sign(data[3].change) == -1
                        ? styles.downcompanydata
                        : styles.upcompanydata
                    }>
                    <Text numberOfLines={1} style={styles.Companyname}>
                      {data[3].company_name}
                    </Text>

                    <Text style={styles.symbol}>{data[3].symbol}</Text>

                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      <Text
                        style={
                          Math.sign(data[3].change) == -1
                            ? styles.textred
                            : styles.textgreen
                        }>
                        {data[3].latest_price}
                      </Text>

                      <View
                        style={{
                          width: 16,
                          alignSelf: 'center',
                          backgroundColor:
                            Math.sign(data[3].change) == -1 ? 'red' : 'green',
                          marginHorizontal: 8,
                          paddingVertical: 4,
                          borderRadius: 4,
                        }}>
                        <Icon
                          name={
                            Math.sign(data[3].change) == -1
                              ? 'arrow-down'
                              : 'arrow-up'
                          }
                          color="white"
                          size={8}
                          style={{alignSelf: 'center'}}
                        />
                      </View>

                      <Text
                        style={
                          Math.sign(data[3].change) == -1
                            ? styles.textred
                            : styles.textgreen
                        }>
                        {data[3].change}
                      </Text>
                    </View>
                  </View>
                </View>
              </>
            ) : (
              <View
                style={{
                  height: '56.25%',
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: theme.primaryColor,
                  borderRadius: 8,
                  marginVertical: 16,
                  paddingHorizontal: 16,
                  paddingVertical: 24,
                }}>
                <Text
                  style={{
                    fontSize: Platform.OS == 'ios' ? 12 : 14,
                    color: theme.white,
                    paddingHorizontal: 8,
                  }}>
                  No data found
                </Text>
              </View>
            )
          ) : (
            <View
              style={{
                height: '56.25%',
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: theme.primaryColor,
                borderRadius: 8,
                marginVertical: 16,
                paddingHorizontal: 16,
                paddingVertical: 24,
              }}>
              <ActivityIndicator size={24} color={theme.secondryColor} />
              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 14 : 16,
                  color: theme.white,
                  paddingHorizontal: 8,
                }}>
                Loading
              </Text>
            </View>
          )}
        </View>
      </ScrollView>

      <View style={[styles.tabssec1, {bottom: 32}]}>
        <View style={styles.tabhead}>
          <View style={[styles.headingbtn, styles.tabheadlabel]}>
            <TouchableOpacity onPress={() => navigation.navigate('Dashboard')}>
              <Text style={styles.headingtxt}>Trading</Text>
            </TouchableOpacity>
          </View>

          <View style={[styles.headingactivebtn, styles.tabheadlabel]}>
            <Text style={[styles.headingactivetxt]}>Trending</Text>
          </View>

          <View style={[styles.headingbtn, styles.tabheadlabel]}>
            <TouchableOpacity onPress={() => navigation.navigate('News')}>
              <Text style={styles.headingtxt}>News</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>

      <BottomNav routeName="Dashboard" />
    </KeyboardAvoidingView>
  );
};

export default Trending;
