import React from 'react';
import {View} from 'react-native';
import {VictoryChart, VictoryLine, VictoryZoomContainer} from 'victory-native';

export default class App extends React.Component {
  handleZoom(domain) {
    this.setState({selectedDomain: domain});
  }

  handleBrush(domain) {
    this.setState({zoomDomain: domain});
  }

  render() {
    return (
      <View>
        <VictoryChart
          width={360}
          height={300}
          scale={{x: 'time'}}
          containerComponent={
            <VictoryZoomContainer
              responsive={false}
              zoomDimension="x"
              zoomDomain={this.state.zoomDomain}
              onZoomDomainChange={this.handleZoom.bind(this)}
            />
          }>
          <VictoryLine
            style={{
              data: {stroke: 'tomato'},
            }}
            data={[
              {x: new Date(1982, 1, 1), y: 125},
              {x: new Date(1987, 1, 1), y: 257},
              {x: new Date(1993, 1, 1), y: 345},
              {x: new Date(1997, 1, 1), y: 515},
              {x: new Date(2001, 1, 1), y: 132},
              {x: new Date(2005, 1, 1), y: 305},
              {x: new Date(2011, 1, 1), y: 270},
              {x: new Date(2015, 1, 1), y: 470},
              {x: new Date(2017, 1, 1), y: 132},
              {x: new Date(2019, 1, 1), y: 305},
              {x: new Date(2020, 1, 1), y: 270},
              {x: new Date(2021, 1, 1), y: 470},
            ]}
          />
        </VictoryChart>
      </View>
    );
  }
}
