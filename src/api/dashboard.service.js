import axiosInstance from '../utils/Apicall';
import Errors from '../configs/Errors';
import Snackbar from 'react-native-snackbar';
import { TOKEN } from '../utils/constants';
import AsyncStorage from '@react-native-community/async-storage';

const getDashBoardQuote = async (symbol, range) => {
  let token = await AsyncStorage.getItem(TOKEN);
  return await axiosInstance
    .get(`chart/${symbol}/${range}`, {
      headers: {
        Authorization: token,
      },
    })
    .then((res) => {
      return res.data;
    })
    .catch((error) => {
      Snackbar.show({
        duration: Snackbar.LENGTH_SHORT,
        text: Errors(error),
        backgroundColor: 'red',
      });

      return false;
    });
};

const sendFeedback = async (body) => {
  let token = await AsyncStorage.getItem(TOKEN);
  return await axiosInstance
    .post(`feedback/create`, body, {
      headers: {
        Authorization: token,
      },
    })
    .then((res) => {
      return res.data;
    })
    .catch((error) => {
      Snackbar.show({
        backgroundColor: 'red',
        duration: Snackbar.LENGTH_SHORT,
        text: Errors(error),
      });

      return false;
    });
};

const updateShareCount = async () => {
  let token = await AsyncStorage.getItem(TOKEN);
  return await axiosInstance
    .post(`share/create`, token, {
      headers: {
        Authorization: token,
      },
    })
    .then((res) => {
      return res.data;
    })
    .catch(() => {
      return false;
    });
};

const searchStock = async (query) => {
  let token = await AsyncStorage.getItem(TOKEN);
  return await axiosInstance
    .get(`search/${query}`, {
      headers: {
        Authorization: token,
      },
    })
    .then((res) => {
      return res.data;
    })
    .catch((error) => {
      Snackbar.show({
        duration: Snackbar.LENGTH_SHORT,
        text: Errors(error),
        backgroundColor: 'red',
      });

      return false;
    });
};

const getTickerData = async (symbols) => {
  let token = await AsyncStorage.getItem(TOKEN);
  return await axiosInstance
    .get(`ticker/${symbols}`, {
      headers: {
        Authorization: token,
      },
    })
    .then((res) => {
      return res.data;
    })
    .catch((error) => {
      Snackbar.show({
        duration: Snackbar.LENGTH_SHORT,
        text: Errors(error),
        backgroundColor: 'red',
      });

      return false;
    });
};

const PostTickerVisible = async (formData) => {
  let token = await AsyncStorage.getItem(TOKEN);
  return await axiosInstance
    .post(`updateTicker`, formData, {
      headers: {
        Authorization: token,
        'content-type': 'multipart/form-data',
      },
    })
    .then((res) => {
      return res.data;
    })
    .catch(() => {
      return false;
    });
};

const SendDefaultDashboard = async (formData) => {
  let token = await AsyncStorage.getItem(TOKEN);
  return await axiosInstance
    .post(`updateDefaultChart`, formData, {
      headers: {
        Authorization: token,
        'content-type': 'multipart/form-data',
      },
    })
    .then((res) => {
      return res.data;
    })
    .catch((error) => {
      Snackbar.show({
        duration: Snackbar.LENGTH_SHORT,
        text: Errors(error),
        backgroundColor: 'red',
      });

      return false;
    });
};

const updateChartpreferences = async (formData) => {
  let token = await AsyncStorage.getItem(TOKEN);
  return await axiosInstance
    .post(`chartPreferenceStore`, formData, {
      headers: {
        Authorization: token,
        'content-type': 'multipart/form-data',
      },
    })
    .then((res) => {
      return res.data;
    })
    .catch(() => {
      return false;
    });
};

const getNews = async () => {
  let token = await AsyncStorage.getItem(TOKEN);
  return await axiosInstance
    .get(`general_news`, {
      headers: {
        Authorization: token,
      },
    })
    .then((res) => {
      return res.data;
    })
    .catch((error) => {
      Snackbar.show({
        duration: Snackbar.LENGTH_SHORT,
        text: Errors(error),
        backgroundColor: 'red',
      });

      return false;
    });
};

const getNotifications = async () => {
  let token = await AsyncStorage.getItem(TOKEN);
  return await axiosInstance
    .get(`notifications`, {
      headers: {
        Authorization: token,
      },
    })
    .then((res) => {
      return res.data;
    })
    .catch((error) => {
      Snackbar.show({
        backgroundColor: 'red',
        duration: Snackbar.LENGTH_LONG,
        text: Errors(error),
      });

      return false;
    });
};

const getEchoMessage = async (id) => {
  let token = await AsyncStorage.getItem(TOKEN);
  return await axiosInstance
    .get(`echoMessageView/${id}`, {
      headers: {
        Authorization: token,
      },
    })
    .then((res) => {
      return res.data;
    })
    .catch((error) => {
      Snackbar.show({
        backgroundColor: 'red',
        duration: Snackbar.LENGTH_LONG,
        text: Errors(error),
      });

      return false;
    });
};

const getStockDetailsByEchoers = async (body) => {
  let token = await AsyncStorage.getItem(TOKEN);
  return await axiosInstance
    .get(`getStockDetailsByEchoers/${body}`, {
      headers: {
        Authorization: token,
      },
    })
    .then((res) => {
      return res.data;
    })
    .catch((error) => {
      Snackbar.show({
        duration: Snackbar.LENGTH_SHORT,
        text: Errors(error),
        backgroundColor: 'red',
      });

      return false; // here return empty object or null or false. based on your business logic
    });
};


export const dashboard = {
  getDashBoardQuote,
  sendFeedback,
  updateShareCount,
  searchStock,
  getTickerData,
  PostTickerVisible,
  SendDefaultDashboard,
  updateChartpreferences,
  getNews,
  getNotifications,
  getEchoMessage,
  getStockDetailsByEchoers,
};
