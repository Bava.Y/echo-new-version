import React from 'react';
import {FlatList, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import RBSheet from 'react-native-raw-bottom-sheet';
import Ionicons from 'react-native-vector-icons/Ionicons';
import theme from '../../utils/theme';
import styles from './style';

const Benefit = [
  {title: '1 Free Echo'},
  {title: 'Fundamental Portal Level II'},
  {title: 'Technical Portal Level II'},
  {title: 'EBooks (5)'},
];

const Subscription = ({subscription, onChange}) => {
  const renderItem = ({item}) => <Item title={item.title} />;

  const Item = ({title}) => (
    <View style={styles.item}>
      <View style={{flexDirection: 'row'}}>
        <Text style={styles.benefits}>{'\u2022'} </Text>
        <Text style={styles.benefits}>{title}</Text>
      </View>
    </View>
  );

  return (
    <RBSheet
      ref={subscription}
      closeOnDragDown={true}
      closeOnPressMask={false}
      closeOnPressBack={true}
      height={480}
      openDuration={250}
      customStyles={{
        container: {
          backgroundColor: theme.themeColor,
          borderTopLeftRadius: 16,
          borderTopRightRadius: 16,
        },
        wrapper: {
          backgroundColor: 'grey',
          opacity: 0.9,
        },
        draggableIcon: {
          display: 'none',
        },
      }}>
      <TouchableOpacity onPress={onChange}>
        <View
          style={{
            alignSelf: 'flex-end',
            paddingHorizontal: 16,
            paddingVertical: 16,
          }}>
          <Ionicons name="close" color="white" size={18}></Ionicons>
        </View>
      </TouchableOpacity>

      <ScrollView
        keyboardShouldPersistTaps={'handled'}
        showsVerticalScrollIndicator={false}>
        <View style={styles.singlesubscription}>
          <View style={{flexDirection: 'row'}}>
            <View style={{width: '50%'}}>
              <Text style={styles.subscribename}>Bronze</Text>
            </View>
            <View style={{width: '50%'}}>
              <Text style={styles.subscribeamount}>$200</Text>
            </View>
          </View>
          <View>
            <Text style={styles.benefittitle}>Benefits: </Text>
            <FlatList
              style={{marginTop: 8}}
              data={Benefit}
              renderItem={renderItem}
            />
          </View>
        </View>

        <View style={styles.singlesubscription}>
          <View style={{flexDirection: 'row'}}>
            <View style={{width: '50%'}}>
              <Text style={styles.subscribename}>Silver</Text>
            </View>
            <View style={{width: '50%'}}>
              <Text style={styles.subscribeamount}>$200</Text>
            </View>
          </View>
          <View>
            <Text style={styles.benefittitle}>Benefits: </Text>
            <FlatList
              style={{marginTop: 10}}
              data={Benefit}
              renderItem={renderItem}
            />
          </View>
        </View>
      </ScrollView>
    </RBSheet>
  );
};

export default Subscription;
