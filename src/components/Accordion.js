import React from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import theme from '../utils/theme';

const Accordian = ({
  title,
  status,
  minValue,
  maxValue,
  years,
  referenceMin,
  referenceMax,
  referenceYear,
  minValueChange,
  maxValueChange,
  yearChange,
  minError,
  maxError,
  yearsError,
  expanded,
  toggleExpand,
}) => {
  return (
    <>
      <TouchableOpacity
        style={[
          styles.row,
          expanded
            ? {borderTopLeftRadius: 4, borderTopRightRadius: 4}
            : {borderRadius: 4},
        ]}
        onPress={toggleExpand}>
        <View
          style={{
            flex: 0.9,
            flexDirection: 'row',
            justifyContent: 'flex-start',
            alignItems: 'center',
          }}>
          <Text style={styles.title}>{title}</Text>

          {status == 1 && (
            <View
              style={{
                backgroundColor: theme.secondryColor,
                borderRadius: 24,
                marginHorizontal: 4,
                paddingHorizontal: 2,
                paddingVertical: 2,
              }}>
              <Icon
                name="check"
                size={14}
                color={theme.white}
                style={{alignSelf: 'center'}}
              />
            </View>
          )}
        </View>

        <View
          style={{
            flex: 0.1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Icon
            name={expanded ? 'keyboard-arrow-up' : 'keyboard-arrow-down'}
            size={24}
            color="white"
            style={{alignSelf: 'center'}}
          />
        </View>
      </TouchableOpacity>

      {expanded && (
        <View style={styles.child}>
          <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <View
              style={{
                width: '33%',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text style={styles.inputtitle}>Min</Text>

              <TextInput
                keyboardType={'number-pad'}
                ref={(point) => {
                  referenceMin.current = point;
                }}
                style={[
                  styles.inputvalue,
                  {borderColor: minError == 'yes' ? 'red' : theme.white},
                ]}
                value={minValue}
                onChangeText={minValueChange}
              />
            </View>

            <View
              style={{
                width: '33%',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text style={styles.inputtitle}>Max</Text>

              <TextInput
                keyboardType={'number-pad'}
                ref={(point) => {
                  referenceMax.current = point;
                }}
                value={maxValue}
                style={[
                  styles.inputvalue,
                  {borderColor: maxError == 'yes' ? 'red' : theme.white},
                ]}
                onChangeText={maxValueChange}
              />
            </View>

            <View
              style={{
                width: '33%',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text style={styles.inputtitle}># of Years</Text>

              <TextInput
                keyboardType={'number-pad'}
                ref={(point) => {
                  referenceYear.current = point;
                }}
                value={years}
                style={[
                  styles.inputvalue,
                  {borderColor: yearsError == 'yes' ? 'red' : theme.white},
                ]}
                onChangeText={yearChange}
              />
            </View>
          </View>
        </View>
      )}
    </>
  );
};

const styles = StyleSheet.create({
  title: {
    fontSize: Platform.OS == 'ios' ? 12 : 14,
    color: theme.white,
    fontWeight: '600',
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: theme.primaryColor,
    paddingHorizontal: 8,
    paddingVertical: 4,
  },
  child: {
    backgroundColor: theme.primaryColor,
    borderBottomLeftRadius: 4,
    borderBottomRightRadius: 4,
    paddingHorizontal: 16,
    paddingVertical: 8,
  },
  inputtitle: {
    fontSize: Platform.OS == 'ios' ? 12 : 14,
    color: theme.white,
    fontWeight: '400',
    textAlign: 'center',
  },
  inputvalue: {
    width: '75%',
    alignSelf: 'center',
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    color: theme.white,
    textAlign: 'center',
    borderWidth: 1,
    borderRadius: 24,
    marginTop: 8,
    paddingHorizontal: 8,
    paddingVertical: 4,
  },
});

export default Accordian;
