import AsyncStorage from '@react-native-community/async-storage';
import {useFocusEffect} from '@react-navigation/native';
import React, {useCallback, useContext, useEffect, useState} from 'react';
import {
  ActivityIndicator,
  Dimensions,
  Image,
  Keyboard,
  Platform,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import Modal from 'react-native-modal';
import RBSheet from 'react-native-raw-bottom-sheet';
import SelectDropdown from 'react-native-select-dropdown';
import Snackbar from 'react-native-snackbar';
import FontAwesomeIcons from 'react-native-vector-icons/FontAwesome5';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {company, portfolio} from '../../api/index';
import {ALPACA, AMERITRADE} from '../../assets/images/index';
import AmeritradeAlert from '../../components/AmeritradeAlert';
import {UserDataContext} from '../../context/UserDataContext';
import styles from '../../styles/order';
import {TOKEN} from '../../utils/constants';
import theme from '../../utils/theme';
import SuccessPopUp from '../profile/components/successPopUp/successPopUp';
import axiosInstance from '../../utils/Apicall';

const OrderShareAmount = ({buyform, onChange, orderData, stockData}, props) => {
  const orderList = props.orderList || [];
  // OrderShareAmount Variables
  const [limitPrice, setLimitPrice] = useState(null);
  const [stopPrice, setStopPrice] = useState(null);
  const [shareAmount, setShareAmount] = useState(0);
  const [qty, setQty] = useState(0);
  const [response] = useState('');
  const [loading, setLoading] = useState(false);
  const [mLoading, setMLoading] = useState(false);
  const [orderType, setOrderType] = useState(null);
  const [timeInForce, setTimeInForce] = useState(null);
  const [modalVisible, setModalVisible] = useState(false);
  const [orderStatus, setorderStaus] = useState(false);
  const [amount, setAmount] = useState(null);
  const [ownQty, setOwnQty] = useState(0);
  const [othersQty, setOthersQty] = useState(0);
  const [res] = useState(undefined);
  console.log('shareAmount//////', shareAmount);

  console.log('qtererr DATA>>>>>>>', qty);
  console.log('shareAmount>>>>>>>', shareAmount);
  console.log('order qty DATA>>>>>>>', orderData.qty);

  // Error Variables
  let errorValidationStates = {
    symbol: false,
    shareAmount: false,
    side: false,
    type: false,
    time_in_force: false,
    limit_price: false,
    stop_price: false,
  };
  const [errorValidation, setErrorValidation] = useState({
    ...errorValidationStates,
  });

  // Context Variables
  const {
    userId,
    setUserId,
    authUserData,
    ameritradeAlertStatus,
    setAmeritradeAlertStatus,
    setSellBtn,
    sellBtn,
    ownStock,
    setOwnStock,
    othersStock,
    setOthersStock,
  } = useContext(UserDataContext);
console.log('authUserData.user_id>>>>>',authUserData.user_id)
  // Other Variables
  const order_type = orderData.side == 'buy' ? 1 : 2;
  const windowHeight = Dimensions.get('window').height;

  const orderOptions = [
    {label: 'Market', value: 'market'},
    {label: 'Stop', value: 'stop'},
    {label: 'Limit', value: 'limit'},
    {label: 'Stop Limit', value: 'stop_limit'},
  ];

  const timeOptions = [
    {label: 'Day', value: 'day'},
    {label: 'GTC - Good til Canceled', value: 'gtc'},
    {label: 'FOK - Fill or Kill', value: 'fok'},
    {label: 'IOC - Immediate or Cancel', value: 'ioc'},
    {label: 'OPG - At-the-Open', value: 'opg'},
    {label: 'CLS - At-the-Close', value: 'cls'},
  ];

  useFocusEffect(
    useCallback(() => {
      let isFocus = true;

      loadOrderType('market');

      loadTimeInForce('day');

      // switch (orderData.fromPage) {
      //   case 'Disallowed Trades':
      //   case 'Portfolio People':
      //   case 'Rebalance':
      //   //  sellAll();
      //     break;

      //   default:
      //     break;
      // }

      return () => {
        isFocus = false;
      };
    }, [orderData]),
  );
  useEffect(() => {
    loadstate();
  }, [loadstate]);

  const loadOrderType = (value) => {
    if (
      Boolean(orderOptions) &&
      Array.isArray(orderOptions) &&
      orderOptions.length != 0
    ) {
      const filteredOrderType = orderOptions.filter(
        (lol) => lol?.value == value,
      );

      setOrderType(filteredOrderType.length != 0 ? filteredOrderType[0] : null);
    } else {
      setOrderType(null);
    }
  };

  const loadTimeInForce = (value) => {
    if (
      Boolean(timeOptions) &&
      Array.isArray(timeOptions) &&
      timeOptions.length != 0
    ) {
      const filteredTimeInForce = timeOptions.filter(
        (lol) => lol?.value == value,
      );

      setTimeInForce(
        filteredTimeInForce.length != 0 ? filteredTimeInForce[0] : null,
      );
    } else {
      setTimeInForce(null);
    }
  };
  const loadstate = async () => {
    setQty(shareAmount / orderData.latest_price);
  };
  const reviewOrder = async () => {
    if (shareAmount == '') {
      Snackbar.show({
        backgroundColor: 'red',
        duration: Snackbar.LENGTH_LONG,
        text: 'Please enter share above zero',
      });
    } else {
      let response = await axiosInstance.get(`checkMarketOpen`);
      let formData = validateOrder();
      console.log('FORM>', formData);
      try {
        if (formData !== undefined && response.data.data === true) {
          setModalVisible(true);
        } else if (response.data.data === false) {
          Snackbar.show({
            backgroundColor: 'red',
            duration: Snackbar.LENGTH_LONG,
            text: response.data.message,
          });
        }
      } catch (err) {
        Snackbar.show(err);
      }
    }
  };

  const checkMarketOpen = async () => {
    let response = await axiosInstance.get(`checkMarketOpen`);
    try {
      if (response.data.keyword === 'success') {
        console.log('DONEEEEEEEEEE');
        Snackbar.show({
          backgroundColor: 'green',
          duration: Snackbar.LENGTH_SHORT,
          text: response.data.message,
        });
      } else {
        Snackbar.show({
          backgroundColor: 'red',
          duration: Snackbar.LENGTH_SHORT,
          text: response.data.message,
        });
      }
    } catch (err) {
      Snackbar.show(err);
    }
  };

  const loadStates = () => {
    // setShareAmount(orderData.qty * orderData.latest_price);

    setQty(orderData.qty);
  };

  const sellAll = () => {
    setShareAmount(
      orderData.qty * orderData.latest_price ||
        ownStock[qty]?.qty * orderData.latest_price ||
        othersStock[qty]?.qty * orderData.latest_price,
    );
    //setAmount(orderData.latestPrice * orderData.qty);
    //checkMarketOpen();
    //  loadstate();
  };

  const validateOrder = () => {
    setQty(shareAmount / orderData.latest_price);
    let formData = new FormData();
    let error = errorValidation;

    if (orderData.symbol === undefined) {
      error['symbol'] = true;
      formData = undefined;
    } else {
      error['symbol'] = false;
      formData && formData.append('symbol', orderData.symbol);
    }

    if (orderType?.value === 'stop' || orderType?.value === 'stop_limit') {
      if (stopPrice === null) {
        error['stop_price'] = true;
        formData = undefined;
      } else {
        error['stop_price'] = false;
        formData && formData.append('stop_price', stopPrice);
      }
    }

    if (orderType?.value === 'limit' || orderType?.value === 'stop_limit') {
      if (limitPrice === null) {
        error['limit_price'] = true;
        formData = undefined;
      } else {
        error['limit_price'] = false;
        formData && formData.append('limit_price', limitPrice);
      }
    }

    if (qty > orderData.qty) {
      console.log;
      error['shareAmount'] = true;
      formData = undefined;
    } else {
      error['shareAmount'] = false;
      formData && formData.append('qty', qty);
    }

    formData && formData.append('time_in_force', timeInForce?.value ?? null);
    formData && formData.append('type', orderType?.value ?? null);
    formData && formData.append('side', orderData.side);
    formData && formData.append('fractionable', orderData.fractionable);
    formData &&
      formData.append('amount', (orderData.latest_price * qty).toFixed(2));
    formData && formData.append('order_type', order_type);
    formData &&
      formData.append(
        'creatorId',
        orderData.creatorId ? orderData.creatorId : authUserData.user_id,
      );
    formData &&
      formData.append(
        'check_financial_metrix_disallowed',
        orderData.fromPage === 'Direct' ||
          orderData.fromPage === 'Disallowed Trades'
          ? 1
          : 0,
      );
    formData &&
      formData.append(
        'total_amount_direct_user',
        orderData.directUserTotalAmount || null,
      );
    formData &&
      formData.append(
        'percentage_direct_user',
        orderData.directUserPercentage || null,
      );

    formData && formData.append('stock_buyed_by', userId);

    setErrorValidation({...errorValidation, ...error});

    return formData;
  };

  const submit = async () => {
    setLoading(true);

    let formData = validateOrder();
    let token = await AsyncStorage.getItem(TOKEN);

    if (formData !== undefined) {
      const res = await company.placeOrder(formData, token);
      if (res) {
        setModalVisible(!modalVisible);
        if (res.keyword == 'success') {
          Snackbar.show({
            duration: Snackbar.LENGTH_LONG,
            text: res.message,
            backgroundColor: 'green',
          });

          switch (orderData.fromPage) {
            case 'Disallowed Trades':
              handleDisallowedStatusUpdate();
              break;

            case 'Rebalance':
              handleRebalanceStatusUpdate();
              break;

            default:
              break;
          }
        } else {
          if (res.hasOwnProperty('message')) {
            if (res.message.hasOwnProperty('error')) {
              switch (res.message.error) {
                case 'invalid_grant':
                  handleAmeritradeAlertStatus();
                  break;

                default:
                  Snackbar.show({
                    duration: Snackbar.LENGTH_LONG,
                    text: res.message.error,
                    backgroundColor: 'red',
                  });
                  break;
              }
            } else {
              Snackbar.show({
                duration: Snackbar.LENGTH_LONG,
                text:
                  typeof res.message == 'string'
                    ? res.message
                    : 'Unable to place the order',
                backgroundColor: 'red',
              });
            }
          } else {
            Snackbar.show({
              duration: Snackbar.LENGTH_LONG,
              text: res.message,
              backgroundColor: 'red',
            });
          }
        }
      } else {
        setModalVisible(!modalVisible);
      }

      setMLoading(true);
      setLoading(false);
    }
    setTimeout(() => {
      setErrorValidation(errorValidationStates);
      setShareAmount(0);
      setQty(0);
      loadOrderType('market');
      setLimitPrice(null);
      setStopPrice(null);
      loadTimeInForce('day');
      setMLoading(false);
      onChange();
    }, 2000);
  };

  const handleDisallowedStatusUpdate = async () => {
    let token = await AsyncStorage.getItem(TOKEN);

    await portfolio.updateDisallowedTrade(orderData.id, token);
  };

  const handleRebalanceStatusUpdate = async () => {
    let token = await AsyncStorage.getItem(TOKEN);

    await portfolio.updateRebalance(orderData.id, token);
  };

  const onclose = () => {
    setErrorValidation(errorValidationStates);
    setShareAmount(0);
    setQty(0);
    loadOrderType('market');
    setLimitPrice(null);
    setStopPrice(null);
    loadTimeInForce('day');
    onChange();
  };

  const handleAmeritradeAlertStatus = () => {
    setAmeritradeAlertStatus(!ameritradeAlertStatus);
  };

  const isFloat = (n) => {
    return Number(n) % 1 !== 0;
  };

  return (
    <>
      <RBSheet
        ref={buyform}
        closeOnDragDown={true}
        closeOnPressMask={true}
        closeOnPressBack={true}
        height={windowHeight * 0.5}
        openDuration={250}
        customStyles={{
          container: {
            backgroundColor: theme.themeColor,
            borderTopLeftRadius: 16,
            borderTopRightRadius: 16,
          },
          wrapper: {backgroundColor: 'grey', opacity: 0.9},
          draggableIcon: {
            display: 'none',
          },
        }}>
        <TouchableOpacity onPress={onclose}>
          <View
            style={{
              alignSelf: 'flex-end',
              paddingHorizontal: 16,
              paddingVertical: 16,
            }}>
            <Ionicons name="close" color="white" size={18} />
          </View>
        </TouchableOpacity>

        <ScrollView
          keyboardShouldPersistTaps="handled"
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
          style={{paddingHorizontal: 16, marginBottom: 24}}>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            {orderData.hasOwnProperty('brokerConnection') && (
              <Image
                source={orderData.brokerConnection == 1 ? AMERITRADE : ALPACA}
                style={{
                  width: 60,
                  height: 40,
                  resizeMode: 'contain',
                }}
              />
            )}
          </View>

          <Text
            style={{
              fontSize: Platform.OS == 'ios' ? 18 : 20,
              fontWeight: 'bold',
              color: orderData.side == 'buy' ? 'green' : 'red',
              textAlign: 'center',
              marginBottom: 8,
            }}>
            {orderData.side.toUpperCase()}
          </Text>

          <View
            style={[
              {flexDirection: 'row', justifyContent: 'space-between'},
              styles.field,
            ]}>
            <View style={{width: '60%', justifyContent: 'center'}}>
              <TextInput
                placeholder="Symbol"
                editable={false}
                placeholderTextColor="white"
                style={styles.fieldvalue}
                value={orderData.symbol}
                autoCapitalize="characters"
                keyboardType="default"
              />
              {errorValidation.symbol && (
                <Text style={styles.errormsg}>Please enter symbol</Text>
              )}
            </View>

            <View style={{width: '40%', justifyContent: 'flex-start'}}>
              <Text style={[styles.fieldlabel, {textAlign: 'center'}]}>
                Market Price
              </Text>
              <Text style={styles.marketprice}>
                ${orderData.latest_price || '0.00'}
              </Text>
            </View>
          </View>

          <SelectDropdown
            buttonStyle={styles.dropdownWrapper}
            data={orderOptions}
            defaultButtonText="Order Type"
            dropdownStyle={{width: '80%', marginLeft: 8, alignSelf: 'center'}}
            onSelect={(selectedItem) => {
              setOrderType(selectedItem);
            }}
            renderCustomizedButtonChild={(selectedItem) => {
              return (
                <View style={styles.dropdownItemContainer}>
                  <View style={styles.dropdownLabelContainer}>
                    <Text style={styles.dropdownLabelText}>
                      {selectedItem?.label ?? 'Order Type'}
                    </Text>
                  </View>
                  <View style={styles.dropdownIconContainer}>
                    <FontAwesomeIcons
                      name="caret-down"
                      size={16}
                      color={'white'}
                    />
                  </View>
                </View>
              );
            }}
            rowStyle={{borderBottomWidth: 0}}
            rowTextForSelection={(item) => item?.label}
            rowTextStyle={styles.dropdownItemText}
            selectedRowTextStyle={styles.dropdownSelectedItemText}
            defaultValue={orderType}
          />

          {orderType?.value === 'stop' || orderType?.value === 'stop_limit' ? (
            <View style={styles.field}>
              <TextInput
                placeholder="Stop Price"
                onFocus={() =>
                  setErrorValidation({...errorValidation, stop_price: false})
                }
                placeholderTextColor="white"
                style={styles.fieldvalue}
                keyboardType="decimal-pad"
                onChangeText={(text) => setStopPrice(text)}
                value={stopPrice}
              />
              {errorValidation.stop_price && (
                <Text style={styles.errormsg}>Please enter price</Text>
              )}
            </View>
          ) : null}

          {orderType?.value === 'limit' || orderType?.value === 'stop_limit' ? (
            <View style={styles.field}>
              <TextInput
                placeholder="Limit Price"
                placeholderTextColor="white"
                style={styles.fieldvalue}
                keyboardType="decimal-pad"
                onFocus={() =>
                  setErrorValidation({...errorValidation, limit_price: false})
                }
                onChangeText={(text) => setLimitPrice(text)}
                value={limitPrice}
              />
              {errorValidation.limit_price && (
                <Text style={styles.errormsg}>Please enter limit</Text>
              )}
            </View>
          ) : null}

          <SelectDropdown
            buttonStyle={styles.dropdownWrapper}
            data={timeOptions}
            defaultButtonText="Time in Force"
            dropdownStyle={{width: '80%', marginLeft: 8, alignSelf: 'center'}}
            onSelect={(selectedItem) => {
              setTimeInForce(selectedItem);
            }}
            renderCustomizedButtonChild={(selectedItem) => {
              return (
                <View style={styles.dropdownItemContainer}>
                  <View style={styles.dropdownLabelContainer}>
                    <Text style={styles.dropdownLabelText}>
                      {selectedItem?.label ?? 'Time in Force'}
                    </Text>
                  </View>
                  <View style={styles.dropdownIconContainer}>
                    <FontAwesomeIcons
                      name="caret-down"
                      size={16}
                      color={'white'}
                    />
                  </View>
                </View>
              );
            }}
            rowStyle={{borderBottomWidth: 0}}
            rowTextForSelection={(item) => item?.label}
            rowTextStyle={styles.dropdownItemText}
            selectedRowTextStyle={styles.dropdownSelectedItemText}
            defaultValue={timeInForce}
          />

          <View
            style={[
              {flexDirection: 'row', justifyContent: 'space-between'},
              styles.field,
            ]}>
            <View style={{width: '60%', justifyContent: 'center'}}>
              <TextInput
                placeholder="Share Amount"
                placeholderTextColor="white"
                style={styles.fieldvalue}
                keyboardType="decimal-pad"
                value={shareAmount.toString()}
                onFocus={() =>
                  setErrorValidation({...errorValidation, shareAmount: false})
                }
                onChangeText={(val) => {
                  setShareAmount(val);

                  setQty(val / orderData.latest_price);
                }}
              />

              {errorValidation.shareAmount && (
                <Text style={styles.errormsg}>
                  {qty > orderData.qty
                    ? `You have ${parseFloat(orderData.qty).toFixed(
                        isFloat(orderData.qty) ? 2 : 0,
                      )} share(s) to sell`
                    : qty <= 0
                    ? 'Please enter share amount'
                    : ''}
                </Text>
              )}
            </View>

            <View style={{width: '40%', justifyContent: 'flex-start'}}>
              <Text style={[styles.fieldlabel, {textAlign: 'center'}]}>
                Estimated Share(s)
              </Text>
              <Text style={styles.marketprice}>
                {Boolean(shareAmount) && Boolean(orderData.latest_price)
                  ? shareAmount / orderData.latest_price
                  : '0.00'}
              </Text>
            </View>
          </View>
          <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
            <TouchableOpacity
              style={styles.submitBtn}
              onPressIn={() => Keyboard.dismiss()}
              onPress={() => {
                reviewOrder();
              }}>
              <Text style={styles.submittext}>Review Order</Text>
            </TouchableOpacity>
            {sellBtn && (
              <TouchableOpacity
                style={styles.submitBtn2}
                onPressIn={() => Keyboard.dismiss()}
                onPress={() => {
                  sellAll();
                }}>
                <Text style={styles.submittext}>Sell All</Text>
              </TouchableOpacity>
            )}
          </View>
        </ScrollView>

        <Modal
          transparent={true}
          isVisible={modalVisible}
          hasBackdrop={true}
          backdropColor="black">
          <View style={styles.centeredView}>
            {mLoading ? (
              <View style={styles.modalView}>
                <Text
                  style={{
                    fontSize: Platform.OS == 'ios' ? 14 : 16,
                    color: 'white',
                    textAlign: 'center',
                  }}>
                  {response}
                </Text>
              </View>
            ) : (
              <View style={styles.modalView}>
                <Text style={styles.modalText}>
                  Are you sure you want to{' '}
                  {orderData.side == 'buy' ? 'place' : 'sell'} this order?
                </Text>

                <Text
                  style={[
                    styles.modalText,
                    {fontWeight: 'bold', marginVertical: 8},
                  ]}>
                  {Boolean(orderType) && orderType?.label?.toUpperCase()}{' '}
                  {orderData.side} for{' '}
                  {parseFloat(qty).toFixed(isFloat(qty) ? 2 : 0)} share of{' '}
                  {orderData.symbol}
                </Text>

                <Text style={[styles.modalText, {marginVertical: 8}]}>
                  If so, press {orderData.side == 'buy' ? 'confirm' : 'sell'}{' '}
                  order to {orderData.side == 'buy' ? 'place' : 'sell'} your
                  trade.
                </Text>

                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-evenly',
                    alignItems: 'center',
                    marginTop: 8,
                  }}>
                  <TouchableOpacity
                    style={styles.openButton}
                    onPress={() => {
                      setModalVisible(false);
                    }}>
                    <Text style={styles.textStyle}>Edit</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={styles.openButton}
                    onPress={() => submit()}>
                    <Text style={styles.textStyle}>
                      {orderData.side == 'buy' ? 'Confirm' : 'Sell'} Order
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            )}
          </View>
        </Modal>

        <Modal
          transparent={true}
          isVisible={loading}
          hasBackdrop={true}
          backdropColor="black">
          <ActivityIndicator size="large" color="white" />
        </Modal>

        <SuccessPopUp
          modal={orderStatus}
          onBackdropPress={() => setorderStaus(false)}
          successText={'Order has been placed'}
          res={res}
        />

        {/* Hided due to TD Ameritrade Echo App revoked issue */}
        {/* <AmeritradeAlert
          modalStatus={ameritradeAlertStatus}
          close={handleAmeritradeAlertStatus}
        /> */}
      </RBSheet>
    </>
  );
};

export default OrderShareAmount;
