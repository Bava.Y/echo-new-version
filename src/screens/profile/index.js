import AsyncStorage from '@react-native-community/async-storage';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import {useFocusEffect} from '@react-navigation/native';
import _ from 'lodash';
import React, {
  useCallback,
  useContext,
  useEffect,
  useRef,
  useState,
} from 'react';
import {
  BackHandler,
  Dimensions,
  Image,
  LogBox,
  Platform,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import ImagePicker from 'react-native-image-crop-picker';
import RBSheet from 'react-native-raw-bottom-sheet';
import Icon from 'react-native-vector-icons/FontAwesome';
import {auth, profile} from '../../api';
import {APP_LOGO} from '../../assets/images/index';
import BottomNav from '../../components/BottomNav';
import HeaderNav from '../../components/HeaderNav';
import Loading from '../../components/Loading';
import {UserDataContext} from '../../context/UserDataContext';
import {
  BASICUPDATED,
  BIOUPDATED,
  P_INVESTUPDATED,
  S_INVESTUPDATED,
} from '../../utils/constants';
import theme from '../../utils/theme';
import MyTabBar from './components/profileTab';
import SuccessPopUp from './components/successPopUp/successPopUp';
import EditProfile from './edit-profile';
import FinancialScreen from './finance/finance';
import PreferencesScreen from './preference/preference';
import ProfileScreen from './profile/profile';
import styles from './style';
import Welcome from './welcome';

LogBox.ignoreAllLogs();

const Profile = ({navigation}) => {
  // Profile Variables
  const Tab = createMaterialTopTabNavigator();
  const [userToken, setUserToken] = useState(undefined);
  const [id, setid] = useState(undefined);
  const [userData, SetUserData] = useState({});
 
  const [performanceRes, setPerformanceRes] = useState(null);
  const [selectData, setSelectdata] = useState({
    ethnicity: [],
    income: [],
    state: [],
    experience: [],
    gender: [],
    primaryInv: [],
    secondaryInv: [],
    secondarySub: [],
  });
  let appUri = Image.resolveAssetSource(APP_LOGO).uri;
  const [photoUrl, setPhotoUrl] = useState(appUri);
  const [modal, setModal] = useState(false);
  const [res, setRes] = useState(undefined);
  const [successText, setSuccessText] = useState('');
  const [welcomePopup, setWelcomePopup] = useState(false);
  const [text, setText] = useState('');
  const [stepText, setStepText] = useState('');
  const [startText, setStartText] = useState('');
  const [isProfile, setIsProfile] = useState(false);

  // Context Variables
  const {authUserData, token, setPreferences, preferences} = useContext(UserDataContext);
console.log('authUserData>>>>>',authUserData)
  // Ref Variables
  const imageRBSheet = useRef();
  const EditProfileSheet = useRef();

  // Other Variables
  const [loading, setLoading] = useState(false);
  const windowHeight = Dimensions.get('window').height;

  useEffect(() => {
    let ac = new AbortController();

    const backAction = () => {
      navigation.goBack();

      return true;
    };

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction,
    );

    return () => {
      ac.abort();
      backHandler.remove();
    };
  }, []);

  useEffect(() => {
   
   
    getDetails();
  }, []);

  const getDetails = async () => {
    setid(authUserData?.user_id);

    setUserToken(token);

    let res = await profile.getUserData(authUserData?.user_id, token);
    setLoading(true)
    SetUserData(res);

    if (!_.isNull(res.photo_url) && !_.isEmpty(res.photo_url)) {
      setPhotoUrl(res.photo_url);
      setLoading(false)
    } else {
      setPhotoUrl(appUri);
    }

    let selectRes = await profile.getUserData(authUserData?.user_id, token);

    let ethnicityData = await auth.getEthnicity();
    let ethnicity = selectMap(ethnicityData);

    let incomeData = await auth.getIncome();
    let income = selectMap(incomeData);

    let stateData = await auth.getState();
    let state = selectMap(stateData);

    let experienceData = await auth.getExperience();
    let experience = selectMap(experienceData);

    let primeInvest = await profile.getPrimaryInv(userToken);
    let primaryValue = selectionMap(primeInvest);

    let secondaryInvest = await profile.getSecondaryInv(userToken);
    let secondaryValue = selectMap(secondaryInvest);

    let subId =
      !_.isNull(selectRes.secondary_investment_id) &&
      parseInt(selectRes.secondary_investment_id);

    let response = await profile.getSecondaryInvSubCatergory(subId, userToken);
    let subOptions = selectMap(response);

    const setSelect = {
      ethnicity: ethnicity,
      income: income,
      state: state,
      experience: experience,
      gender: [
        {label: 'Male', value: 'Male'},
        {label: 'Female', value: 'Female'},
        {label: 'Others', value: 'Others'},
      ],
      primaryInv: primaryValue,
      secondaryInv: secondaryValue,
      secondarySub: subOptions,
    };

    setSelectdata(setSelect);

    let performance = await profile.getPermormance(token);

    setPerformanceRes(performance[0]);

    setLoading(false);
  };

  const selectMap = (selectData) => {
    return selectData.map((data) => {
      let selectArray = {
        value: Object.values(data)[0],
        label: Object.values(data)[1],
      };
      return selectArray;
    });
  };

  const selectionMap = (selectData) => {
    return selectData.map((data) => {
      let selectArray = {
        value: Object.values(data)[1],
        label: Object.values(data)[0],
      };
      return selectArray;
    });
  };

  useFocusEffect(
    useCallback(() => {
      let isActive = true;

      getPreferences();

      return () => {
        isActive = false;
      };
    }, [token]),
  );

  const getPreferences = async () => {
    let preference = await profile.getPreference(token, authUserData?.user_id);
    setPreferences(preference);
  };

  const overAllUpdate = (status) => {
    let update = status == 'yes' ? true : false;

    setText('Welcome to Echo - Social Investing!');

    setStepText('Step Two');

    setStartText(`Begin Echoing`);

    setWelcomePopup(update);

    setIsProfile(true);
  };

  const openCropper = () => {
    imageRBSheet.current.close();

    setTimeout(() => {
      ImagePicker.openCropper({
        path: photoUrl,
        cropping: true,
        freeStyleCropEnabled: true,
      })
        .then((image) => {
          setPhotoUrl(image.path);

          let pathParts = image.path.split('/');

          let fileName = pathParts[pathParts.length - 1];

          const updateInfo = async () => {
            let formData = insertImages(image.path, image.mime, fileName);

            if (!_.isUndefined(formData)) {
              imageUpdate(formData);
            }
          };

          updateInfo();
        })
        .catch(() => {
          return false;
        });
    }, 250);
  };

  const takePicture = () => {
    imageRBSheet.current.close();

    setTimeout(() => {
      ImagePicker.openCamera({
        cropping: true,
        freeStyleCropEnabled: true,
      }).then((image) => {
        setPhotoUrl(image.path);

        let pathParts = image.path.split('/');

        let fileName = pathParts[pathParts.length - 1];

        const updateInfo = async () => {
          let formData = insertImages(image.path, image.mime, fileName);
          if (!_.isUndefined(formData)) {
            imageUpdate(formData);
          }
        };

        updateInfo();
      });
    }, 250);
  };

  const getPictureFromGallery = () => {
    imageRBSheet.current.close();

    setTimeout(() => {
      ImagePicker.openPicker({
        cropping: true,
        freeStyleCropEnabled: true,
      }).then((image) => {
        setPhotoUrl(image.path);

        let pathParts = image.path.split('/');

        let fileName = pathParts[pathParts.length - 1];

        const updateInfo = async () => {
          let formData = insertImages(image.path, image.mime, fileName);

          if (!_.isUndefined(formData)) {
            imageUpdate(formData);
          }
        };

        updateInfo();
      });
    }, 250);
  };

  const imageUpdate = async (formData) => {
    await profile.updateBasicInfo(userToken, formData);
  };

  const insertImages = (path, type, name) => {
    let formData = new FormData();

    formData && formData.append('id', id);
    formData.append('first_name', userData.first_name);
    formData.append('last_name', userData.last_name);
    formData.append('email', userData.email);
    formData.append('designation', userData.designation);
    formData.append('company_name', userData.company_name);
    formData &&
      formData.append('resident_state', parseInt(userData.resident_state));
    formData && formData.append('gender', userData.gender);
    formData && formData.append('ethnicity', parseInt(userData.ethnicity));
    formData &&
      formData.append('income_level', parseInt(userData.income_level));
    formData &&
      formData.append('years_emp_invest', parseInt(userData.experiencename));
    formData &&
      formData.append('photo', {
        name: name,
        uri: path,
        type: type,
      });

    formData && formData.append('is_basic_update', 0);

    return formData;
  };

  const basicUpdate = async () => {
    let resp = await profile.getUserData(authUserData?.user_id, token);

    setRes(resp);

    SetUserData(resp);

    if (!_.isNull(resp.photo_url) && !_.isEmpty(resp.photo_url)) {
      setPhotoUrl(resp.photo_url);
    } else {
      setPhotoUrl(appUri);
    }

    profileUpdatedStatus();

    setTimeout(() => {
      setRes(undefined);
    }, 1000);
  };

  const basicPopupp = (status) => {
    setModal(status);

    setSuccessText('Basic info updated');

    setModal(status);
  };

  const profileUpdatedStatus = async () => {
    let bioStatus = await AsyncStorage.getItem(BIOUPDATED);
    let basicStatus = await AsyncStorage.getItem(BASICUPDATED);
    let primaryInvestStatus = await AsyncStorage.getItem(P_INVESTUPDATED);
    let secondaryInvestStatus = await AsyncStorage.getItem(S_INVESTUPDATED);

    if (
      JSON.parse(bioStatus) == true &&
      JSON.parse(basicStatus) == true &&
      JSON.parse(primaryInvestStatus) == true &&
      JSON.parse(secondaryInvestStatus) == true
    ) {
    }
  };

  const updateStatus = async () => {
    setWelcomePopup(false);

    navigation.navigate('Financial');
  };

  return (
    <>
      <HeaderNav title="Profile" isBack={true} />

      <View style={{flex: 1, backgroundColor: theme.secondryColor}}>
        <View style={styles.userdetails}>
          {loading ? (
            <View
              style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Loading color={'white'} />
            </View>
          ) : (
            <View style={styles.user}>
              <View
                style={{
                  flex: 0.1875,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <TouchableOpacity onPress={openCropper}>
                  <Image source={{uri: photoUrl}} style={styles.applogo} />
                </TouchableOpacity>

                <Icon
                  name="camera"
                  color={theme.white}
                  size={16}
                  style={styles.cameraicon}
                  onPress={() => imageRBSheet.current.open()}
                />
              </View>

              <View
                style={{
                  flex: 0.8125,
                  justifyContent: 'center',
                  paddingHorizontal: 8,
                  paddingVertical: 4,
                }}>
                <Text
                  style={styles.username}
                  ellipsizeMode={'tail'}
                  numberOfLines={2}>
                  {userData.first_name} {userData.last_name}
                </Text>

                <Text
                  style={{
                    fontSize: Platform.OS == 'ios' ? 12 : 14,
                    color: 'white',
                    fontWeight: '600',
                  }}
                  numberOfLines={2}
                  ellipsizeMode={'tail'}>
                  {userData.designation != 'null' &&
                    userData.company_name != 'null' &&
                    !_.isEmpty(userData.designation) &&
                    !_.isNull(userData.designation) &&
                    !_.isEmpty(userData.company_name) &&
                    !_.isNull(userData.company_name) &&
                    `${userData.designation}  - ${userData.company_name}`}
                </Text>
              </View>
            </View>
          )}

          <View style={styles.actioncolumn}>
            <Icon
              name="pencil"
              color={theme.white}
              size={20}
              onPress={() => EditProfileSheet.current.open()}
            />
          </View>
        </View>

        <Tab.Navigator
          sceneContainerStyle={{backgroundColor: theme.themeColor}}
          tabBar={(props) => <MyTabBar {...props} />}
          swipeEnabled={false}
          initialRouteName="Profile">
          <Tab.Screen name="Profile">
            {() => (
              <ProfileScreen
                userDataa={userData}
                userId={id}
                tokenOfUser={userToken}
                profileUpdatedStatus={profileUpdatedStatus}
                overAllUpdate={overAllUpdate}
                selectData={selectData}
              />
            )}
          </Tab.Screen>

          <Tab.Screen name="Financial" component={FinancialScreen} />

          <Tab.Screen name="Preferences">
            {() => (
              <PreferencesScreen
                preferences={
                  _.isNull(preferences) ||
                  _.isEmpty(preferences) ||
                  _.isUndefined(preferences)
                    ? {}
                    : preferences
                }
                performanceRes={
                  _.isNull(performanceRes) ||
                  _.isEmpty(performanceRes) ||
                  _.isUndefined(performanceRes)
                    ? {}
                    : performanceRes
                }
              />
            )}
          </Tab.Screen>
        </Tab.Navigator>
      </View>

      <Welcome
        visibility={welcomePopup}
        onChange={() => {
          setWelcomePopup(false);
        }}
        text={text}
        stepText={stepText}
        startText={startText}
        onPress={updateStatus}
        isProfile={isProfile}
      />

      <RBSheet
        ref={imageRBSheet}
        closeOnDragDown={true}
        closeOnPressMask={false}
        closeOnPressBack={true}
        height={160}
        openDuration={250}
        customStyles={{
          container: {
            backgroundColor: theme.themeColor,
            borderColor: 'white',
            borderWidth: 1,
          },
          wrapper: {backgroundColor: 'grey', opacity: 0.9},
          draggableIcon: {
            display: 'none',
          },
        }}>
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            paddingHorizontal: 16,
            paddingVertical: 16,
          }}>
          <Text
            style={{
              fontSize: Platform.OS == 'ios' ? 14 : 16,
              color: 'white',
              fontWeight: 'bold',
              marginVertical: 8,
            }}>
            Profile Picture
          </Text>

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginVertical: 16,
            }}>
            <TouchableOpacity
              style={{
                flex: 0.375,
                justifyContent: 'center',
                alignItems: 'center',
              }}
              onPress={takePicture}>
              <View
                style={{
                  width: 48,
                  height: 48,
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: theme.secondryColor,
                  borderColor: 'white',
                  borderWidth: 1,
                  borderRadius: 48 / 2,
                }}>
                <Icon name="camera" color={'white'} size={20} />
              </View>

              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 12 : 14,
                  color: 'white',
                  fontWeight: '600',
                  marginVertical: 8,
                }}>
                Take Picture
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={{
                flex: 0.375,
                justifyContent: 'center',
                alignItems: 'center',
              }}
              onPress={getPictureFromGallery}>
              <View
                style={{
                  width: 48,
                  height: 48,
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: theme.secondryColor,
                  borderColor: 'white',
                  borderWidth: 1,
                  borderRadius: 48 / 2,
                }}>
                <Icon name="upload" color={'white'} size={20} />
              </View>

              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 12 : 14,
                  color: 'white',
                  fontWeight: '600',
                  marginVertical: 8,
                }}>
                Upload from gallery
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </RBSheet>

      <RBSheet
        ref={EditProfileSheet}
        closeOnDragDown={false}
        closeOnPressMask={false}
        closeOnPressBack={true}
        height={windowHeight * 0.875}
        openDuration={250}
        customStyles={{
          container: {
            backgroundColor: theme.themeColor,
            borderTopLeftRadius: 16,
            borderTopRightRadius: 16,
          },
          wrapper: {backgroundColor: 'grey', opacity: 0.9},
          draggableIcon: {
            display: 'none',
          },
        }}>
        <TouchableOpacity onPress={() => EditProfileSheet.current.close()}>
          <View
            style={{
              alignSelf: 'flex-end',
              paddingHorizontal: 16,
              paddingVertical: 16,
            }}>
            <Icon name="close" color="white" size={18} />
          </View>
        </TouchableOpacity>

        <EditProfile
          userData={userData}
          navigation={navigation}
          id={id}
          token={userToken}
          closeRbSheet={() => EditProfileSheet.current.close()}
          selectData={selectData}
          photoUrl={photoUrl}
          basicPopup={basicPopupp}
          basicUpdate={basicUpdate}
          overAllUpdate={overAllUpdate}
        />
      </RBSheet>

      <SuccessPopUp
        res={res}
        successText={successText}
        modal={modal}
        onBackdropPress={() => setModal(false)}
      />

      <BottomNav routeName="Profile" />
    </>
  );
};

export default Profile;
