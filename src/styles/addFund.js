import {Platform, StyleSheet} from 'react-native';
import theme from '../utils/theme';

const styles = StyleSheet.create({
  container: {flex: 1, backgroundColor: theme.themeColor},
  mainsec: {
    backgroundColor: theme.primaryColor,
    borderRadius: 8,
    paddingHorizontal: 16,
    paddingVertical: 16,
  },
  lineContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  lineLabelContainer: {
    flex: 0.65,
    justifyContent: 'center',
  },
  lineValueContainer: {
    flex: 0.35,
    justifyContent: 'center',
  },
  lineLabelTxt: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    color: '#ffffffc2',
    fontWeight: '400',
  },
  lineValueTxt: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    color: '#ffffffc2',
    fontWeight: '400',
    textAlign: 'right',
  },
  borderView: {
    borderBottomColor: theme.white,
    borderBottomWidth: 0.25,
    paddingBottom: 8,
  },
  statusTxt: {
    fontSize: 10,
    color: theme.white,
    fontWeight: 'bold',
  },
  noDataTxt: {
    fontSize: Platform.OS == 'ios' ? 12 : 14,
    color: theme.white,
  },
  btntxt: {
    fontSize: Platform.OS == 'ios' ? 12 : 14,
    color: theme.white,
    fontWeight: '400',
    textAlign: 'center',
  },
  submitview: {
    width: '50%',
    alignSelf: 'center',
    backgroundColor: theme.secondryColor,
    borderRadius: 4,
    marginHorizontal: 16,
    marginVertical: 16,
    paddingHorizontal: 16,
    paddingVertical: 8,
  },
  labelTxt: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    color: theme.white,
    fontWeight: 'bold',
  },
  viewcard: {
    backgroundColor: theme.primaryColor,
    padding: 10,
    borderRadius: 10,
  },
  viewdetail: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  viewheading: {
    fontSize: Platform.OS == 'ios' ? 12 : 14,
    color: theme.white,
    fontWeight: '600',
    lineHeight: 18,
  },
  viewdata: {
    fontSize: Platform.OS == 'ios' ? 12 : 14,
    color: theme.white,
    fontWeight: '400',
    lineHeight: 18,
  },
  transferbtn: {
    flex: 0.3875,
    alignItems: 'center',
    backgroundColor: theme.secondryColor,
    borderColor: theme.secondryColor,
    borderWidth: 1,
    borderRadius: 24,
    paddingHorizontal: 16,
    paddingVertical: 4,
  },
  cancelbtn: {
    flex: 0.3875,
    alignItems: 'center',
    backgroundColor: '#ff0000c4',
    borderColor: '#ff0000c4',
    borderWidth: 1,
    borderRadius: 24,
    paddingHorizontal: 16,
    paddingVertical: 4,
  },
  status: {
    alignSelf: 'flex-start',
    borderRadius: 4,
    borderWidth: 2,
    marginTop: 4,
    paddingHorizontal: 8,
    paddingVertical: 4,
  },
  btnsec: {
    flex: 0.7125,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
  },
  title: {
    fontSize: Platform.OS == 'ios' ? 16 : 18,
    color: theme.white,
    fontWeight: 'bold',
    marginBottom: 8,
  },
  textInput: {
    flex: 1,
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    color: theme.white,
    borderRadius: 24,
    paddingHorizontal: 4,
  },
  errorMsg: {
    fontSize: Platform.OS == 'ios' ? 12 : 14,
    color: '#ff0000a1',
    textAlign: 'left',
    marginHorizontal: 12,
    marginVertical: 8,
  },
  saveButton: {
    width: '62.5%',
    alignSelf: 'center',
    backgroundColor: theme.secondryColor,
    borderRadius: 8,
    marginVertical: 16,
    paddingHorizontal: 16,
    paddingVertical: 8,
  },
  hintTxt: {
    fontSize: Platform.OS == 'ios' ? 12 : 14,
    color: '#ffffffc2',
    fontWeight: '600',
    textAlign: 'left',
    marginVertical: 8,
  },
});

export default styles;
