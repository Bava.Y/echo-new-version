import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import theme from '../../../utils/theme';

const MyTabBar = ({state, descriptors, navigation}) => {
  const focusedOptions = descriptors[state.routes[state.index].key].options;

  if (focusedOptions.tabBarVisible === false) {
    return null;
  }

  return (
    <View
      style={{
        flexDirection: 'row',
        marginLeft: 10,
        marginRight: 10,
        marginBottom: 20,
        borderRadius: 10,
      }}>
      {state.routes.map((route, index) => {
        const {options} = descriptors[route.key];

        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
            ? options.title
            : route.name;

        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true,
          });

          if (!isFocused && !event.defaultPrevented) {
            {
              /* navigation.navigate(route.name); //can change dynamically */
            }
          }
        };

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };

        return (
          <TouchableOpacity
            key={index}
            accessibilityRole="button"
            accessibilityState={isFocused ? {selected: true} : {}}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            onLongPress={onLongPress}
            style={{
              flex: 1,
              backgroundColor: theme.primaryColor,
            }}>
            <View
              style={{
                backgroundColor: isFocused ? '#02c7d7' : theme.primaryColor,
                height: 30,
                borderWidth: isFocused ? 1 : 0,
                borderRadius: isFocused ? 8 : 0,
                alignItems: 'center',
                justifyContent: 'center',
                borderColor: '#02c7d7',
              }}>
              <Text
                style={{
                  color: isFocused ? 'white' : '#02c7d7',
                  fontWeight: 'bold',
                }}>
                {label}
              </Text>
            </View>
          </TouchableOpacity>
        );
      })}
    </View>
  );
};

export default MyTabBar;
