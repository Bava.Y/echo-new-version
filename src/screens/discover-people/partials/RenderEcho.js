import {useFocusEffect, useNavigation} from '@react-navigation/native';
import React, {useCallback, useContext, useState} from 'react';
import {
  ActivityIndicator,
  Dimensions,
  Image,
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  FlatList,
  ScrollView,
} from 'react-native';

import Modal from 'react-native-modal';
import Snackbar from 'react-native-snackbar';
import Icon from 'react-native-vector-icons/FontAwesome';
import {company, portfolio} from '../../../api';
import {UserDataContext} from '../../../context/UserDataContext';
import theme from '../../../utils/theme';

export default function RenderEcho(props) {
  // Props Variables
  const {item, onPress, reLoad, screenName, loadingEchoer} = props;

  // RenderEcho Variables

  const [trackingErrorData, setTrackingErrorData] = useState({});
  const [imageLoader, setImageLoader] = useState(true);

  // Context Variables
  const {token, loadMoreData, echoerLoad} = useContext(UserDataContext);

  // Error Variables
  const [isTrackingError, setTrackingError] = useState(false);
  const [show, setShow] = useState(false);

  // Navigation Variables
  const navigation = useNavigation();

  // Other Variables
  const [broker, setBroker] = useState(false);
  const [loading, setLoading] = useState(false);
  const [showText, setShowText] = useState(true);
  const [currentPage, setCurrentPage] = useState(0);

  const deviceHeight = Dimensions.get('window').height;
  const trackingError =
    item.tracking_error == 0 ? false : item.tracking_error == 1 ? true : null;

  useFocusEffect(
    useCallback(() => {
      let isFocus = true;

      // Change the state every second or the time given by User.
      const interval = setInterval(() => {
        setShowText((showText) => !showText);
      }, 1000);

      return () => {
        isFocus = false;

        clearInterval(interval);
      };
    }, []),
  );

  const getTrackingError = async () => {
    setTrackingError(true);

    setLoading(true);

    const res = await portfolio.getTrackingError(item.customer_id, token);

    res && res.keyword == 'success'
      ? setTrackingErrorData(res.data)
      : Snackbar.show({duration: Snackbar.LENGTH_SHORT, text: res.message});

    setLoading(false);
  };

  const acceptError = async () => {
    let formData = new FormData();

    formData.append('symbol', trackingErrorData.symbol);
    formData.append('qty', trackingErrorData.echo_qty);
    formData.append('stop_price', trackingErrorData.stop_price);
    formData.append('limit_price', trackingErrorData.limit_price);
    formData.append('time_in_force', 'day');
    formData.append('type', trackingErrorData.type);
    formData.append('side', trackingErrorData.side);
    formData.append('amount', trackingErrorData.amount);
    formData.append('order_type', trackingErrorData.side == 'buy' ? 1 : 2);
    formData.append('creatorId', trackingErrorData.created_by);
    formData.append('check_financial_metrix_disallowed', 0);
    formData.append('total_amount_direct_user', null);
    formData.append('percentage_direct_user', null);
    formData.append(
      'tracking_percentage_id',
      trackingErrorData.tracking_percentage_id,
    );

    setTrackingError(false);

    Snackbar.show({
      duration: Snackbar.LENGTH_INDEFINITE,
      text: 'Accepting tracking error...',
    });

    const res = await company.placeOrder(formData, token);

    res &&
      res.keyword.toLowerCase() === 'success' &&
      Snackbar.show({
        duration: Snackbar.LENGTH_SHORT,
        text: res.message,
        backgroundColor: 'green',
      });

    reLoad();
  };

  const ignoreError = async () => {
    setTrackingError(false);

    reLoad();
  };
  const renderItem = (data, index) => {
    return (
      <>
        <View
          key={index?.toString()}
          style={{
            backgroundColor: theme.primaryColor,
            borderTopEndRadius: 10,
            borderTopStartRadius: 10,
            //padding: 3,
            // borderBottomWidth: 1,
            // borderBottomColor: '#666262',
            marginBottom: 10,
            borderBottomEndRadius: 10,
            borderBottomStartRadius: 10,
          }}>
          <View style={styles.listContainer}>
            <View style={styles.userContainer}>
              <TouchableOpacity style={styles.imageView} onPress={onPress}>
                <Image
                  onLoadStart={() => {
                    setImageLoader(true);
                  }}
                  onLoadEnd={() => {
                    setImageLoader(false);
                  }}
                  resizeMode={'cover'}
                  style={styles.imageStyle}
                  source={{uri: data?.item?.photo}}
                />
                {imageLoader && (
                  <ActivityIndicator size={12} style={{position: 'absolute'}} />
                )}
              </TouchableOpacity>

              <TouchableOpacity
                style={styles.nameView}
                onPress={() =>
                  navigation.navigate('DiscoverPeopleProfile', {
                    id: data?.item?.id,
                    loginStatus: data?.item?.login_status,
                  })
                }>
                <Text style={styles.nameStyle}>
                  {data?.item?.first_name + ' ' + data?.item?.last_name}
                </Text>

                {data?.item?.designation &&
                  (data?.item?.designation === 'null' ? (
                    <Text style={styles.designationStyle}></Text>
                  ) : (
                    <Text style={styles.designationStyle}>
                      {data?.item?.designation}
                    </Text>
                  ))}
              </TouchableOpacity>
            </View>

            <View style={styles.errorContainer}>
              {screenName === 'Echoing' && (
                <TouchableOpacity
                  style={styles.blueDotView}
                  onPress={() =>
                    navigation.navigate('TrackingErrorHistory', {
                      echoerData: item,
                    })
                  }>
                  <Text style={styles.text12Style}>H</Text>
                </TouchableOpacity>
              )}

              {screenName === 'Echoing' && (
                <TouchableOpacity
                  style={styles.blueDotView}
                  onPress={() => {
                    navigation.navigate('Rebalance', {id: 'all'});
                  }}>
                  <Text style={styles.text12Style}>R</Text>
                </TouchableOpacity>
              )}

              {screenName === 'Echoing' && trackingError && (
                <TouchableOpacity
                  style={[
                    styles.dotView,
                    {backgroundColor: showText ? '#FF0000' : '#FF00005A'},
                  ]}
                  onPress={getTrackingError}>
                  <Text style={styles.text12Style}>T</Text>
                </TouchableOpacity>
              )}
            </View>

            <View style={styles.smallBlueDotContainer}>
              <TouchableOpacity
                style={styles.smallBlueDotView}
                onPress={() => props.metrics(true)}
              />
            </View>
          </View>

       
              <View key={index?.toString()} style={styles.listReturn}>
                <View
                  style={{flexDirection: 'row', marginTop: 6, marginBottom: 3}}>
                  <Text
                    style={{flex: 0.9, marginHorizontal: '5%', color: 'white'}}>
                    My Stocks Returns
                  </Text>
                  <Text style={{color: 'white', fontSize: 13}}>
                    {data?.item?.mystocks_return}%
                  </Text>
                </View>
              </View>
        
        </View>
      </>
    );
  };

  const renderLoader = () => {
    return loadingEchoer ? (
      <View style={styles.companyListLoader}>
        <ActivityIndicator size="large" color={theme.secondryColor} />
      </View>
    ) : null;
  };
  const loadMoreItem = () => {
    if (loadMoreData) {
      setCurrentPage(currentPage + 1);
    }
  };

  const ItemSeparatorView = () => {
    return (
      // Flat List Item Separator
      <View />
    );
  };
  return (
    <>
      <FlatList
        data={item}
        renderItem={renderItem}
        ItemSeparatorComponent={ItemSeparatorView}
        keyExtractor={(item) => {
          item.id;
        }}
        ListFooterComponent={renderLoader}
        onEndReached={loadMoreItem}
        onEndReachedThreshold={0.01}
        showsVerticalScrollIndicator={false}
      />

      {/* Tracking Error */}
      <Modal
        isVisible={isTrackingError}
        onBackdropPress={() => setTrackingError(false)}>
        <View
          style={{
            backgroundColor: theme.themeColor,
            borderRadius: 10,
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              borderBottomColor: 'grey',
              borderBottomWidth: 1,
              paddingHorizontal: 16,
              paddingVertical: 16,
            }}>
            <Text
              style={{
                fontSize: Platform.OS == 'ios' ? 16 : 18,
                color: 'white',
                fontWeight: 'bold',
              }}>
              Tracking Errors
            </Text>

            <TouchableOpacity onPress={() => setTrackingError(false)}>
              <Icon name="close" size={20} color="white" />
            </TouchableOpacity>
          </View>

          {loading ? (
            <ActivityIndicator
              style={{
                height: deviceHeight * 0.5,
                justifyContent: 'center',
                alignItems: 'center',
              }}
              color="white"
              animating={loading}
            />
          ) : (
            <>
              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 14 : 16,
                  color: 'white',
                  fontWeight: 'bold',
                  paddingHorizontal: 16,
                  paddingVertical: 16,
                }}>
                Overall Tracking Error Percentage:{' '}
                <Text
                  style={{
                    fontSize: Platform.OS == 'ios' ? 16 : 18,
                    color: theme.secondryColor,
                    fontWeight: 'bold',
                  }}>
                  {trackingErrorData.total_percentage
                    ? `${parseFloat(trackingErrorData.total_percentage).toFixed(
                        2,
                      )}%`
                    : '-'}
                </Text>
              </Text>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'flex-start',
                  paddingHorizontal: 16,
                }}>
                <Image style={styles.imageStyle} source={{uri: item.photo}} />

                <View style={{marginHorizontal: 8}}>
                  <Text style={styles.title}>
                    {item.first_name + ' ' + item.last_name}
                  </Text>

                  <Text style={styles.hint}>{item.designation || 'N/A'}</Text>
                </View>
              </View>

              <View
                style={{
                  backgroundColor: theme.primaryColor,
                  marginVertical: 8,
                  paddingHorizontal: 16,
                  paddingVertical: 8,
                }}>
                <Text
                  style={{
                    fontSize: Platform.OS == 'ios' ? 12 : 14,
                    color: 'white',
                    fontWeight: '600',
                  }}>
                  Echoing
                </Text>
              </View>

              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 14 : 16,
                  color: 'white',
                  fontWeight: 'bold',
                  paddingHorizontal: 16,
                }}>
                Purchase Qty : {trackingErrorData.customer_qty}
              </Text>

              <View style={{paddingHorizontal: 16, marginVertical: 16}}>
                <View
                  style={{
                    width: '100%',
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}>
                  <View
                    style={{
                      width: '30%',
                      alignItems: 'flex-start',
                      borderColor: 'grey',
                      borderWidth: 1,
                      paddingHorizontal: 8,
                      paddingVertical: 4,
                    }}>
                    <Text
                      style={{
                        fontSize: Platform.OS == 'ios' ? 12 : 14,
                        color: 'white',
                        fontWeight: '600',
                      }}>
                      Symbol
                    </Text>
                  </View>

                  <View
                    style={{
                      width: '20%',
                      alignItems: 'flex-end',
                      borderColor: 'grey',
                      borderWidth: 1,
                      paddingHorizontal: 8,
                      paddingVertical: 4,
                    }}>
                    <Text
                      style={{
                        fontSize: Platform.OS == 'ios' ? 12 : 14,
                        color: 'white',
                        fontWeight: '600',
                      }}>
                      Qty
                    </Text>
                  </View>

                  <View
                    style={{
                      width: '30%',
                      alignItems: 'flex-end',
                      borderColor: 'grey',
                      borderWidth: 1,
                      paddingHorizontal: 8,
                      paddingVertical: 4,
                    }}>
                    <Text
                      style={{
                        fontSize: Platform.OS == 'ios' ? 12 : 14,
                        color: 'white',
                        fontWeight: '600',
                      }}>
                      Amount ($)
                    </Text>
                  </View>

                  <View
                    style={{
                      width: '20%',
                      alignItems: 'flex-end',
                      borderColor: 'grey',
                      borderWidth: 1,
                      paddingHorizontal: 8,
                      paddingVertical: 4,
                    }}>
                    <Text
                      style={{
                        fontSize: Platform.OS == 'ios' ? 12 : 14,
                        color: 'white',
                        fontWeight: '600',
                      }}>
                      (%)
                    </Text>
                  </View>
                </View>

                <View
                  style={{
                    width: '100%',
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}>
                  <View
                    style={{
                      width: '30%',
                      alignItems: 'flex-start',
                      borderColor: 'grey',
                      borderWidth: 1,
                      borderTopColor: 'transparent',
                      paddingHorizontal: 8,
                      paddingVertical: 4,
                    }}>
                    <Text
                      style={{
                        fontSize: Platform.OS == 'ios' ? 12 : 14,
                        color: 'white',
                        fontWeight: '400',
                      }}>
                      {trackingErrorData.symbol}
                    </Text>
                  </View>

                  <View
                    style={{
                      width: '20%',
                      alignItems: 'flex-end',
                      borderColor: 'grey',
                      borderWidth: 1,
                      borderTopColor: 'transparent',
                      paddingHorizontal: 8,
                      paddingVertical: 4,
                    }}>
                    <Text
                      style={{
                        fontSize: Platform.OS == 'ios' ? 12 : 14,
                        color: 'white',
                        fontWeight: '400',
                      }}>
                      {trackingErrorData.echo_qty}
                    </Text>
                  </View>

                  <View
                    style={{
                      width: '30%',
                      alignItems: 'flex-end',
                      borderColor: 'grey',
                      borderWidth: 1,
                      borderTopColor: 'transparent',
                      paddingHorizontal: 8,
                      paddingVertical: 4,
                    }}>
                    <Text
                      style={{
                        fontSize: Platform.OS == 'ios' ? 12 : 14,
                        color: 'white',
                        fontWeight: '400',
                      }}>
                      {trackingErrorData.amount
                        ? `$${parseFloat(trackingErrorData.amount).toFixed(2)}`
                        : '-'}
                    </Text>
                  </View>

                  <View
                    style={{
                      width: '20%',
                      alignItems: 'flex-end',
                      borderColor: 'grey',
                      borderWidth: 1,
                      borderTopColor: 'transparent',
                      paddingHorizontal: 8,
                      paddingVertical: 4,
                    }}>
                    <Text
                      style={{
                        fontSize: Platform.OS == 'ios' ? 12 : 14,
                        color: 'white',
                        fontWeight: '400',
                      }}>
                      {trackingErrorData.percentage
                        ? `${parseFloat(trackingErrorData.percentage).toFixed(
                            2,
                          )}%`
                        : '-'}
                    </Text>
                  </View>
                </View>
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  marginBottom: 20,
                }}>
                <TouchableOpacity
                  onPress={() => {
                    acceptError();
                  }}>
                  <View
                    style={{
                      backgroundColor: 'green',
                      borderRadius: 4,
                      marginHorizontal: 4,
                      paddingHorizontal: 16,
                      paddingVertical: 8,
                    }}>
                    <Text
                      style={{
                        fontSize: Platform.OS == 'ios' ? 12 : 14,
                        color: 'white',
                        fontWeight: 'bold',
                      }}>
                      Accept
                    </Text>
                  </View>
                </TouchableOpacity>

                <TouchableOpacity onPress={ignoreError}>
                  <View
                    style={{
                      backgroundColor: '#d82424f7',
                      borderRadius: 4,
                      marginHorizontal: 4,
                      paddingHorizontal: 16,
                      paddingVertical: 8,
                    }}>
                    <Text
                      style={{
                        fontSize: Platform.OS == 'ios' ? 12 : 14,
                        color: 'white',
                        fontWeight: 'bold',
                      }}>
                      Ignore
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            </>
          )}
        </View>
      </Modal>
    </>
  );
}

const styles = StyleSheet.create({
  listContainer: {
    flexDirection: 'row',
    backgroundColor: theme.primaryColor,
    borderTopEndRadius: 10,
    borderTopStartRadius: 10,
    padding: 8,
    borderBottomWidth: 1,
    borderBottomColor: '#666262',
  },
  userContainer: {
    flex: 0.7,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 5,
  },
  imageView: {
    flex: 0.2875,
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageStyle: {
    width: 40,
    height: 40,
    borderRadius: 40 / 2,
  },
  nameView: {
    flex: 0.7125,
    justifyContent: 'center',
    paddingHorizontal: 4,
    paddingVertical: 2,
  },
  nameStyle: {
    fontSize: Platform.OS == 'ios' ? 12 : 14,
    fontWeight: '600',
    color: theme.white,
  },
  designationStyle: {
    fontSize: Platform.OS == 'ios' ? 10 : 12,
    fontWeight: '400',
    color: '#ffffff8a',
    marginTop: 2,
  },
  errorContainer: {
    flex: 0.2,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },
  dotContainer: {
    flex: 0.33,
  },
  smallBlueDotContainer: {
    flex: 0.08,
    justifyContent: 'center',
    alignItems: 'center',
  },
  smallBlueDotView: {
    width: 12,
    height: 12,
    backgroundColor: theme.secondryColor,
    borderRadius: 12 / 2,
  },
  blueDotView: {
    width: 20,
    height: 20,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20 / 2,
    backgroundColor: theme.secondryColor,
  },
  dotView: {
    width: 20,
    height: 20,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20 / 2,
  },
  text12Style: {
    fontSize: Platform.OS == 'ios' ? 10 : 12,
    fontWeight: '600',
    color: theme.white,
  },
  revenueView: {
    flex: 0.15,
    justifyContent: 'center',
    alignItems: 'flex-end',
    padding: 2,
  },
  title: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    color: theme.white,
  },
  hint: {
    fontSize: Platform.OS == 'ios' ? 12 : 14,
    color: '#ffffff8a',
    marginTop: 4,
  },
  listReturn: {
    backgroundColor: theme.primaryColor,
    borderBottomEndRadius: 10,
    borderBottomStartRadius: 10,
    padding: 1,
    marginBottom: 2,
  },
});
