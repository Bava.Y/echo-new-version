import * as URL from '../../env';

export const LOGIN_USER = 'LOGIN_USER';
export const MPIN = 'MPIN';
export const MPINSTATUS = JSON.stringify(false);
export const IS_FIRST_USER = 'IS_FIRST_USER';
export const IS_TWO_STEP = 'IS_TWO_STEP';
export const IS_LOGIN = 'IS_LOGIN';
export const TOKEN = 'TOKEN';
export const PERSONSELECTED = 'PERSONSELECTED';
export const TOKENN = 'TOKENN';
export const COMPLETED = 'COMPLETED';
export const BEARER = 'BEARER';
export const FCMTOKEN = 'FCMTOKEN';
export const BASE_URL = URL.baseURL;
export const TWITTER_COMSUMER_KEY = 'slqP3m2mUScRiRQFqD3qBoE4g';
export const TWITTER_CONSUMER_SECRET =
  'nwrktmbOM9zxXcp3UB0V0sedpFY5oeYhejeXlOSqZQunE27dVg';
export const AMERITRADE_BASE_URL = URL.ameritradeBaseURL;
export const AMERITRADE_CLIENT_ID = URL.ameritradeClientId;
export const AMERITRADE_REDIRECT_URI = URL.ameritradeRedirectURI;
export const GOOGLE_WEB_CLIENT_ID =
  '103183859621-i0cj85ur7e6nbo5i7a0hhtkhhf78jg7b.apps.googleusercontent.com';
export const PLAID_BASE_URL = URL.plaidBaseURL;
export const PLAID_CLIENT_ID = URL.plaidClientId;
export const PLAID_CLIENT_NAME = URL.plaidClientName;
export const PLAID_COUNTRY_CODES = URL.plaidCountryCodes;
export const PLAID_SECRET_KEY = URL.plaidSecretKey;

//Account
export const PROFILE = JSON.stringify(false);
export const VISUAL = JSON.stringify(false);
export const EMAIL_SEEN = JSON.stringify(false);
export const PHONE_SEEN = JSON.stringify(false);

//for profile popups
export const BASICUPDATED = JSON.stringify(false);
export const BIOUPDATED = JSON.stringify(false);
export const P_INVESTUPDATED = JSON.stringify(false);
export const S_INVESTUPDATED = JSON.stringify(false);

export const fileFormat = ['jpg', 'jpeg', 'png', 'bmp', 'pdf'];
export const WELCOME = 'WELCOME';
