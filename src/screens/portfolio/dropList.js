import React, {} from 'react';
import {View} from 'react-native';
import SelectDropdown from 'react-native-select-dropdown';
import FontAwesomeIcons from 'react-native-vector-icons/FontAwesome5';


const list=()=>{
    return(
        <SelectDropdown
        buttonStyle={styles.listBroker}
        buttonTextStyle={styles.listBrokerText}
         data={listof}
         dropdownStyle={{alignSelf: 'center',}}
        defaultButtonText="CLICK"
        onSelect={(selectedItem) => {
         setBroker(selectedItem)
        }}
        renderDropdownIcon={() => (
         <FontAwesomeIcons name="caret-down" size={16} color={'white'}/>
        )}
        rowStyle={{borderBottomWidth: 0}}
        rowTextStyle={styles.brokerItemText}
        selectedRowTextStyle={styles.listBrokerSelectedItemText}
       />
    )
};

export default list;