import {StackActions} from '@react-navigation/native';
import _ from 'lodash';
import React, {useState,useContext} from 'react';
import {
  KeyboardAvoidingView,
  Platform,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Snackbar from 'react-native-snackbar';
import {auth} from '../api/Authentication.service';
import {FloatingTitleTextInputField} from '../components/FloatingTitleTextInputField';
import Loading from '../components/Loading';
import style from '../styles/forgotpasswordstyle';
import theme from '../utils/theme';
import { UserDataContext } from '../context/UserDataContext';

const initialValidationError = {
  emailId: false,
};

const ForgotpasswordScreen = ({navigation}) => {
  const [validationError, setValidationError] = useState({
    ...initialValidationError,
  });

  const [mail, setMail] = useState('');
  const [loading, setLoading] = useState(false);

  const {
    setPasswordOTP,
  } = useContext(UserDataContext);
  const validateFields = () => {
    let formData = new FormData();
    let error = validationError;

    if (mail === '') {
      error['emailId'] = 'Enter email';
      formData = undefined;
    } else {
      error['emailId'] = false;
      formData && formData.append('email', mail);
    }
    setValidationError({...validationError, ...error});
    return formData;
  };

  const handleSubmit = async () => {
    let formData = validateFields();
    if (formData !== undefined) {
      setLoading(true);

      const data = await auth.forgotPassword(formData);
      setPasswordOTP(data?.otp)
console.log('otp>>>>>',data?.otp)
      setLoading(false);
      if (typeof data === 'string') {
        if (data === 'redirect') {
          return navigation.dispatch(StackActions.replace('Login'));
        }
        return Snackbar.show({
          text: data,
          duration: Snackbar.LENGTH_SHORT,
        });
      }
      Snackbar.show({
        text: `OTP has sent to your email. Kindly check email inbox`,
        duration: Snackbar.LENGTH_SHORT,
      });
      setTimeout(() => {
        setLoading(false);
        navigation.dispatch(
          StackActions.replace('ResetPassword', {otp: data.otp}),
        );
      }, 1500);
    }
  };
  return (
    <>
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : null}
        keyboardVerticalOffset={Platform.OS === 'ios' ? 40 : 0}
        style={style.container}>
        <Text style={style.forgottitle}>Forgot Password?</Text>

        <FloatingTitleTextInputField
          keyboardType={'email-address'}
          attrName="email"
          title="Email"
          value={mail}
          updateMasterState={(text) => {
            setValidationError({
              ...validationError,
              emailId: _.isEmpty(text) ? 'Enter email' : false,
            });
            let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            if (reg.test(text) === false) {
              setMail(text);
              setValidationError({
                ...validationError,
                emailId: 'Enter valid email',
              });
            } else {
              setMail(text);
              setValidationError({
                ...validationError,
                emailId: false,
              });
            }
          }}
        />
        {validationError.emailId == 'Enter email' ? (
          <Text style={style.errormsg}>{validationError.emailId}</Text>
        ) : null}
        {validationError.emailId == 'Enter valid email' ? (
          <Text style={style.errormsg}>{validationError.emailId}</Text>
        ) : null}

        <View
          style={{
            width: '100%',
            flexDirection: 'row',
            justifyContent: 'space-evenly',
            marginVertical: 24,
          }}>
          <TouchableOpacity
            style={style.cancelBtn}
            onPress={() => navigation.navigate('Login')}>
            <Text style={style.canceltext}>BACK</Text>
          </TouchableOpacity>
          <TouchableOpacity style={style.signinBtn}>
            <Text style={style.signintext} onPress={handleSubmit}>
              Submit
            </Text>
          </TouchableOpacity>
        </View>
      </KeyboardAvoidingView>

      {loading && <Loading color={theme.white} />}
    </>
  );
};

export default ForgotpasswordScreen;
