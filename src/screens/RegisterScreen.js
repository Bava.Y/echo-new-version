import CheckBox from '@react-native-community/checkbox';
import {StackActions} from '@react-navigation/native';
import _ from 'lodash';
import React, {useState} from 'react';
import {
  ActivityIndicator,
  KeyboardAvoidingView,
  LogBox,
  Platform,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Snackbar from 'react-native-snackbar';
import {auth} from '../api/Authentication.service';
import {FloatingTitleTextInputField} from '../components/FloatingTitleTextInputField';
import style from '../styles/registerstyle';
import theme from '../utils/theme';

LogBox.ignoreAllLogs();

const initialValidationError = {
  firstName: false,
  lastName: false,
  emailId: false,
  passWord: false,
  conPassword: false,
  userName: false,
  terms: false,
  phone: false,
};

const RegisterScreen = ({navigation, route}) => {
  // RegisterScreen Variables
  const [firstname, setFirstname] = useState(
    route.params.fiRstName ? route.params.fiRstName : '',
  );
  const [lastname, setLastName] = useState(
    route.params.laStName ? route.params.laStName : '',
  );
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [phoneNumber, setPhoneNumber] = useState('');
  const [email, setEmail] = useState(
    route.params.eMail ? route.params.eMail : '',
  );
  const [isSelected, setSelection] = useState(false);
  const [selected, setSelected] = useState(true);

  // Error Variables
  const [validationError, setValidationError] = useState({
    ...initialValidationError,
  });

  // Other Variables
  const [loading, setLoading] = useState(false);

  const validateFields = () => {
    let formData = new FormData();
    let error = validationError;

    if (firstname && firstname.trim().length != 0) {
      error['firstName'] = false;
      formData && formData.append('first_name', firstname);
    } else {
      error['firstName'] = 'Enter first name';
      formData = undefined;
    }

    if (lastname && lastname.trim().length != 0) {
      error['lastName'] = false;
      formData && formData.append('last_name', lastname);
    } else {
      error['lastName'] = 'Enter last name';
      formData = undefined;
    }

    if (username && username.trim().length != 0) {
      error['userName'] = false;
      formData && formData.append('username', username);
    } else {
      error['userName'] = 'Enter user name';
      formData = undefined;
    }

    if (
      password &&
      password.trim().length != 0 &&
      confirmPassword &&
      confirmPassword.trim().length != 0
    ) {
      error['passWord'] = false;
      error['conPassword'] = false;
      formData && formData.append('password', password);
    } else if (password !== confirmPassword) {
      error['conPassword'] = 'Password mismatch';
      formData = undefined;
    } else {
      error['passWord'] = 'Enter password';
      error['conPassword'] = 'Enter confim password';
      formData = undefined;
    }

    if (phoneNumber && phoneNumber.trim().length != 0) {
      error['phone'] = false;
      formData && formData.append('phone', phoneNumber);
    } else {
      error['phone'] = 'Enter phone number';
      formData = undefined;
    }

    if (email && email.trim().length != 0) {
      error['emailId'] = false;
      formData && formData.append('email', email);
    } else {
      error['emailId'] = 'Enter email';
      formData = undefined;
    }

    if (isSelected === false) {
      error['terms'] = 'You should agree terms and conditions';
      formData = undefined;
    } else {
      error['terms'] = false;
    }

    if (selected === true) {
      formData && formData.append('two_step_verification', 1);
    } else {
      formData && formData.append('two_step_verification', 0);
    }

    setValidationError({...validationError, ...error});

    return formData;
  };

  const submitForm = async () => {
    let formData = validateFields();

    if (formData !== undefined) {
      setLoading(true);

      const data = await auth.signUp(formData);

      if (typeof data === 'string') {
        setLoading(false);
        if (data === 'redirect') {
          return navigation.dispatch(StackActions.replace('Login'));
        }
        return Snackbar.show({
          text: data,
          duration: Snackbar.LENGTH_SHORT,
        });
      }
      setLoading(false);
      Snackbar.show({
        text: `Registered sucessfully`,
        duration: Snackbar.LENGTH_SHORT,
      });

      setTimeout(() => {
        navigation.goBack();
      }, 2000);
    }
  };

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : null}
      keyboardVerticalOffset={Platform.OS === 'ios' ? 40 : 0}
      style={style.container}>
      <ScrollView
        keyboardShouldPersistTaps={'handled'}
        showsVerticalScrollIndicator={false}>
        <Text style={style.signuptitle}>Sign Up</Text>

        <FloatingTitleTextInputField
          ispassword={false}
          attrName="firstname"
          title={
            <Text>
              First Name<Text style={{color: 'red'}}> *</Text>
            </Text>
          }
          value={firstname}
          updateMasterState={(text) => {
            setValidationError({
              ...validationError,
              firstName: _.isEmpty(text) ? 'Enter first name' : false,
            });
            setFirstname(text);
          }}
        />
        {validationError.firstName ? (
          <Text style={style.errormsg}>{validationError.firstName}</Text>
        ) : null}

        <FloatingTitleTextInputField
          ispassword={false}
          attrName="lastname"
          title={
            <Text>
              Last Name<Text style={{color: 'red'}}> *</Text>
            </Text>
          }
          value={lastname}
          updateMasterState={(text) => {
            setValidationError({
              ...validationError,
              lastName: _.isEmpty(text) ? 'Enter last name' : false,
            });
            setLastName(text);
          }}
        />
        {validationError.lastName ? (
          <Text style={style.errormsg}>{validationError.lastName}</Text>
        ) : null}

        <FloatingTitleTextInputField
          ispassword={false}
          attrName="username"
          title={
            <Text>
              User Name
              <Text style={{color: 'red'}}> *</Text>
            </Text>
          }
          value={username}
          updateMasterState={(text) => {
            setValidationError({
              ...validationError,
              userName: _.isEmpty(text) ? 'Enter user name' : null,
            });
            setUsername(text);
          }}
        />
        {validationError.userName ? (
          <Text style={style.errormsg}>{validationError.userName}</Text>
        ) : null}

        <FloatingTitleTextInputField
          ispassword={true}
          attrName="password"
          title={
            <Text>
              Password
              <Text style={{color: 'red'}}> *</Text>
            </Text>
          }
          value={password}
          updateMasterState={(text) => {
            setValidationError({
              ...validationError,
              passWord: _.isEmpty(text) ? 'Enter password' : null,
            });
            setPassword(text);
          }}
        />
        {validationError.passWord ? (
          <Text style={style.errormsg}>{validationError.passWord}</Text>
        ) : null}

        <FloatingTitleTextInputField
          ispassword={true}
          attrName="cpassword"
          title={
            <Text>
              Confirm Password
              <Text style={{color: 'red'}}> *</Text>
            </Text>
          }
          value={confirmPassword}
          updateMasterState={(text) => {
            setValidationError({
              ...validationError,
              conPassword: _.isEmpty(text) ? 'Enter confirm password' : null,
            });
            setConfirmPassword(text);
          }}
        />
        {validationError.conPassword ? (
          <Text style={style.errormsg}>{validationError.conPassword}</Text>
        ) : null}

        <FloatingTitleTextInputField
          keyboardType={'number-pad'}
          ispassword={false}
          attrName="Phone Number"
          title={
            <Text>
              Phone Number<Text style={{color: 'red'}}> *</Text>
            </Text>
          }
          value={phoneNumber}
          updateMasterState={(text) => {
            setValidationError({
              ...validationError,
              phone: _.isEmpty(text) ? 'Enter phone number' : false,
            });
            setPhoneNumber(text);
          }}
        />
        {validationError.phone ? (
          <Text style={style.errormsg}>{validationError.phone}</Text>
        ) : null}

        <FloatingTitleTextInputField
          keyboardType={'email-address'}
          ispassword={false}
          attrName="email"
          title={
            <Text>
              Email
              <Text style={{color: 'red'}}> *</Text>
            </Text>
          }
          value={email}
          updateMasterState={(text) => {
            setValidationError({
              ...validationError,
              emailId: _.isEmpty(text) ? 'Enter email' : false,
            });
            let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            if (reg.test(text) === false) {
              setEmail(text);
              setValidationError({
                ...validationError,
                emailId: 'Enter valid email',
              });
            } else {
              setEmail(text);
              setValidationError({
                ...validationError,
                emailId: false,
              });
            }
          }}
        />
        {validationError.emailId ? (
          <Text style={style.errormsg}>{validationError.emailId}</Text>
        ) : null}

        <View style={{flexDirection: 'row', marginTop: 20}}>
          <CheckBox
            boxType={'square'}
            value={isSelected}
            onValueChange={() => {
              setValidationError({
                ...validationError,
                terms: _.isBoolean(isSelected)
                  ? null
                  : 'You should agree terms and conditions',
              });
              setSelection(!isSelected);
            }}
            tintColors={{true: '#2A587F', false: 'black'}}
            style={[
              {alignSelf: 'center', color: '#fff'},
              Platform.OS == 'ios' && {
                transform: [{scaleX: 0.75}, {scaleY: 0.75}],
              },
            ]}
          />
          <View style={{marginTop: 6}}>
            <Text
              style={{fontSize: Platform.OS == 'ios' ? 12 : 14, color: '#fff'}}>
              I agree to{' '}
              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 12 : 14,
                  color: '#2a587f',
                }}>
                Terms & Conditions<Text style={{color: 'red'}}> *</Text>
              </Text>
            </Text>
          </View>
        </View>

        {validationError.terms ? (
          <Text style={style.errormsg}>{validationError.terms}</Text>
         ) : null}
         <View style={{flexDirection: 'row', marginTop: 20}}>
          <CheckBox
            boxType={'square'}
            value={selected}
            onValueChange={async () => {
              setSelected(!selected);
            }}
            tintColors={{true: '#2A587F', false: 'black'}}
            style={[
              {alignSelf: 'center', color: '#fff'},
              Platform.OS == 'ios' && {
                transform: [{scaleX: 0.75}, {scaleY: 0.75}],
              },
            ]}
          />
          <View style={{marginTop: 6}}>
            <Text
              style={{fontSize: Platform.OS == 'ios' ? 12 : 14, color: '#fff'}}>
              Do you want to enable two step verificaiton?{' '}
            </Text>
          </View>
         </View>

         <View style={{flexDirection: 'row', marginTop: 20}}>
          <Text
            style={{fontSize: Platform.OS == 'ios' ? 12 : 14, color: '#fff'}}>
            Already have an account?
          </Text>
          <TouchableOpacity onPress={() => navigation.navigate('Login')}>
            <Text
              style={{
                fontSize: Platform.OS == 'ios' ? 12 : 14,
                color: '#fff',
                marginLeft: 8,
              }}>
              Sign In
            </Text>
          </TouchableOpacity>
         </View>
         <TouchableOpacity style={style.signinBtn} onPress={submitForm}>
          <Text style={style.signintext}>Done</Text>
         </TouchableOpacity>

         <TouchableOpacity onPress={() => navigation.navigate('Login')}>
          <Text style={[style.canceltext, {marginVertical: 20}]}>Cancel</Text>
         </TouchableOpacity>

         {loading && (
          <ActivityIndicator
            size="large"
            color={theme.white}
            style={{
              position: 'absolute',
              left: 0,
              right: 0,
              top: 800,
              bottom: 0,
              alignItems: 'center',
              justifyContent: 'center',
              zIndex: 999,
            }}
          />
        )}
      </ScrollView>
    </KeyboardAvoidingView>
  );
};

export default RegisterScreen;
