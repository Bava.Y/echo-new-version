import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,

    backgroundColor: 'white',
  },
  header: {
    backgroundColor: '#2d2d2d',
  },
  column: {
    padding: 4,
    borderRadius: 4,
  },
  mainheading: {
    backgroundColor: 'black',
    borderColor: '#f0a223',
    borderWidth: 1,
    alignItems: 'center',
    padding: 18,
    marginLeft: 24,
    marginRight: 24,
    borderRadius: 10,
    height: 110,
    marginTop: 20,
  },
  maintitle: {
    fontWeight: 'bold',
    fontSize: 18,
    color: '#fef4e4',
    textAlign: 'center',
    paddingTop: 25,
    textTransform: 'uppercase',
  },
  subheading: {
    backgroundColor: '#0d0d0d',
    borderColor: '#f0a223',
    borderWidth: 1,
    textAlign: 'center',
    alignItems: 'center',
    padding: 6,
    borderRadius: 10,
    height: 90,
  },
  subtitle: {
    fontWeight: 'bold',
    fontSize: 14,
    color: '#fef4e4',
    textAlign: 'center',
    paddingTop: 28,
    textTransform: 'uppercase',
  },
  container: {
    padding: 10,
    flexDirection: 'column',
    justifyContent: 'space-between',
    backgroundColor: 'rgba(0, 0, 0, 0.05)',
  },
  ticker: {
    backgroundColor: 'black',
    height: 50,
    elevation: 5,
    flexDirection: 'row',
    alignItems: 'center',
  },
  tickercolumn: {
    flexDirection: 'row',
    width: '33.3%',
  },
  companyname: {
    color: 'white',
  },
  textred: {
    color: 'red',
    fontSize: 12,
  },
  textgreen: {
    color: 'green',
  },
  content: {
    marginLeft: 10,
  },
  imageIcon: {
    width: 50,
    height: 40,
  },
  imageIcon1: {
    width: 60,
    height: 35,
  },
  imageIcon2: {
    width: 40,
    height: 40,
  },
});

export default styles;
