import React,{useState} from 'react';
import {
  ActivityIndicator,
  Dimensions,
  Platform,
  Text,
  View,
} from 'react-native';
import {VictoryArea, VictoryAxis, VictoryChart} from 'victory-native';
import theme from '../../utils/theme';
import styles from './styles';
import {
  CandlestickChart,
  LineChart,
  LineChartCursorCrosshair,
  LineChartCursorLine,
  LineChartCursorProps,
} from 'react-native-wagmi-charts';
import {GestureHandlerRootView} from 'react-native-gesture-handler';

const SmallChart = ({data, loading}) => {

  const [fourChartData,setFourChartData] =useState(data?.data)
  const deviceWidth = Dimensions.get('window').width;
  return (
    <>
      {loading ? (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <ActivityIndicator size="small" color="white" animating={loading} />
        </View>
      ) : (
        <>
          <Text style={styles.Companyname1}>{data?.symbol}</Text>
          <View style={styles.sharerate1}>
            <Text style={styles.shareamt1}>
              {data?.price ? `$ ${data?.price}` : ''}
            </Text>
            <Text
              style={[
                styles.sharepercentage1,
                {
                  color:
                    data?.change && Math.sign(data?.change) == -1
                      ? 'red'
                      : 'green',
                },
              ]}>
              {data.change}
            </Text>
          </View>

          {data?.data?.length == 0 && (
            <View
              style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 10 : 12,
                  color: theme.white,
                  textAlign: 'center',
                }}>
                No chart preview available
              </Text>
            </View>
          )}

          {data?.data?.length != 0 && (
            <GestureHandlerRootView>
            <LineChart.Provider data={data?.data}>
              <LineChart width={deviceWidth *0.4} height={80} >
                <LineChart.Path
                  color={theme.secondryColor}
                  width={1.5}>
                  <LineChart.Tooltip
                    textStyle={{color: 'white', fontSize: 13}}
                  />
                  <LineChart.Gradient />
                </LineChart.Path>
                <LineChart.CursorLine color="white" />

                <LineChart.Tooltip position="bottom">
                  <LineChart.DatetimeText
                    style={{
                      borderRadius: 4,
                      color: 'white',
                      fontSize: 13,
                      padding: 4,
                    }}
                  />
                </LineChart.Tooltip>
              </LineChart>
            </LineChart.Provider>
          </GestureHandlerRootView>
            // <VictoryChart
            //   width={deviceWidth * 0.4}
            //   height={90}
            //   padding={{top: 8, bottom: 0, left: 0, right: 0}}
            //   maxDomain={{y: data.max}}
            //   minDomain={{y: data.min}}>
            //   <VictoryAxis
            //     style={{
            //       axis: {
            //         stroke: 'transparent',
            //       },
            //       axisLabel: {
            //         fill: 'transparent',
            //       },
            //       ticks: {stroke: 'transparent'},
            //       tickLabels: {
            //         fill: 'transparent',
            //       },
            //     }}
            //   />

            //   <VictoryAxis
            //     dependentAxis={false}
            //     style={{
            //       axis: {
            //         stroke: 'transparent',
            //       },
            //       tickLabels: {
            //         fill: 'transparent',
            //       },
            //       grid: {
            //         fill: 'transparent',
            //         stroke: 'transparent',
            //       },
            //     }}
            //   />

            //   <VictoryArea
            //     interpolation="natural"
            //     style={{
            //       data: {
            //         fill: theme.secondryColor,
            //         fillOpacity: 0.5,
            //         stroke: theme.secondryColor,
            //         strokeWidth: 2,
            //       },
            //     }}
            //     data={data.data}
            //   />
            // </VictoryChart>
          )}
        </>
      )}
    </>
  );
};

export default SmallChart;
