import AsyncStorage from '@react-native-community/async-storage';
import {useFocusEffect} from '@react-navigation/core';
import {StackActions, useNavigation} from '@react-navigation/native';
import _ from 'lodash';
import React, {
  useContext,
  useRef,
  useState,
  useEffect,
  useCallback,
} from 'react';
import {
  ActivityIndicator,
  Dimensions,
  Image,
  Keyboard,
  KeyboardAvoidingView,
  LogBox,
  Platform,
  ScrollView,
  Share,
  Text,
  TextInput,
  TouchableHighlight,
  TouchableOpacity,
  View,
} from 'react-native';
import Modal from 'react-native-modal';
import RBSheet from 'react-native-raw-bottom-sheet';
import SelectDropdown from 'react-native-select-dropdown';
import Snackbar from 'react-native-snackbar';
import Icon from 'react-native-vector-icons/FontAwesome';
import FontAwesomeIcons from 'react-native-vector-icons/FontAwesome5';
import {auth, dashboard, profile} from '../../../api';
import {APP_LOGO} from '../../../assets/images/index';
import {UserDataContext} from '../../../context/UserDataContext';
import {
  BIOUPDATED,
  P_INVESTUPDATED,
  S_INVESTUPDATED,
  TOKEN,
} from '../../../utils/constants';
import theme from '../../../utils/theme';
import SuccessPopUp from '../components/successPopUp/successPopUp';
import EditBio from '../edit-bio';
import PrimaryInvestment from '../primary-investment';
import SecondaryInvestment from '../secondary-investment';
import styles from '../style';

const initialvalidationErrors = {
  primary: false,
  secondary: false,
  secondarySub: false,
  hybrid: false,
};

LogBox.ignoreAllLogs();

const ProfileScreen = ({
  userDataa,
  userId,
  tokenOfUser,
  profileUpdatedStatus,
  selectData,
  overAllUpdate,
}) => {
  // Props Variables
  const [userToken, setUserToken] = useState(tokenOfUser);
  const [id, setid] = useState(userId);
  const [userData, SetUserData] = useState(userDataa);
  const [bio, setBio] = useState(userDataa.bio || null);
  let appUri = Image.resolveAssetSource(APP_LOGO).uri;
  const [photoUrl, setPhotoUrl] = useState(
    // appUri,
    _.isNull(userData.photo_url) ? appUri : photoUrl,
  );
  const [originalRes, setOriginalRes] = useState(userDataa);
  const [secondaryRes, setSecondaryRes] = useState(userDataa);
  console.log('userDataa>>>>>>>>', loginUserData);
  // ProfileScreen Variables
  const [investment, setInvestment] = useState(
    !_.isUndefined(userDataa?.primary_investment_id) &&
      !_.isNull(userDataa?.primary_investment_id)
      ? userDataa?.primary_investment_id?.length == 1
        ? false
        : true
      : false,
  );
  const [sector, setSector] = useState(false);
  const [sub, setSub] = useState(
    !_.isUndefined(userDataa?.primary_investment_id) &&
      !_.isNull(userDataa?.primary_investment_id)
      ? userDataa?.primary_investment_id?.length == 1
        ? []
        : convertPrimary(userDataa?.primary_investment_id)
      : [],
  );
  const [primary, setPrimary] = useState(
    !_.isUndefined(userDataa?.primary_investment_id) &&
      !_.isNull(userDataa?.primary_investment_id)
      ? Number(
          userDataa?.primary_investment_id?.length == 1
            ? userDataa?.primary_investment_id[0]
            : 35,
        )
      : null,
  );
  const [primaryName, setPrimaryName] = useState(
    !_.isNull(userDataa?.primary_investment_name),
  );

  const [secondarySubCat, setSecondarySubCat] = useState(
    !_.isNull(userDataa?.secondary_investment_subcategory_id)
      ? userDataa?.secondary_investment_subcategory_id
      : null,
  );
  console.log('secondarySubCat</////', secondarySubCat);
  const [secondary, setSecondary] = useState(
    !_.isNull(userDataa?.secondary_investment_id)
      ? parseInt(userDataa?.secondary_investment_id)
      : null,
  );
  // const [secondaryName, setSecondaryName] = useState(
  //   !_.isNull(userDataa?.secondary_investment_name)
  //     ? String(userDataa?.secondary_investment_name)
  //     : '',
  // );

  const [secondaryName, setSecondaryName] = useState(
    !_.isNull(userDataa?.secondary_investment_name),
  );
  const [secondarySubCatName, setSecondarySubCatName] = useState(
    !_.isNull(userDataa?.secondary_investment_subcategory_name),
  );
  const [subject, setSubject] = useState(null);
  const [note, SetNote] = useState(null);
  const [myBio, setMyBio] = useState('');
  const [subOptions, setSubOptions] = useState([...selectData?.secondarySub]);
  const [updateSub, setUpdateSub] = useState(false);

  // Context Variables
  const {
    authUserData,
    token,
    secondarySubCatID,
    setSecondarySubCatID,
    secondaryID,
    setSecondaryID,
    loginUserData,
  } = useContext(UserDataContext);

  console.log('secondaryI?>>><<<', secondaryID);
  // Error Variables
  const [validationError, setValidationError] = useState({
    ...initialvalidationErrors,
  });
  const errorInitialState = {
    subject: false,
    note: false,
  };
  const [fieldError, setFieldError] = useState(errorInitialState);

  // Ref Variables
  const PrimaryInvestmentSheet = useRef(null);
  const SecondaryInvestmentSheet = useRef(null);
  const EditBioSheet = useRef(null);
  const ShareSheet = useRef(null);

  // Other Variables
  const navigation = useNavigation();
  const windowHeight = Dimensions.get('window').height;
  const [res, setRes] = useState(undefined);
  const [modalVisible, setModalVisible] = useState(false);
  const [fLoading, setFLoading] = useState(false);
  const [feedbackres, setFeedbackRes] = useState('');
  const [modal, setModal] = useState(false);
  const [successText, setSuccessText] = useState('');
  const [enabled, setEnabled] = useState(true);
  const [viewMore, setViewMore] = useState(false);

  const myArray = [...selectData.primaryInv];
  var removeIndex = myArray.map(item => item.label).indexOf('Hybrid');
  ~removeIndex && myArray.splice(removeIndex, 1);

  const category = [
    {value: 1, label: 'Bug Fix'},
    {value: 2, label: 'Investors'},
    {value: 3, label: 'General Questions'},
    {value: 4, label: 'Advistisers'},
  ];

  const convertPrimary = array => {
    let myArray = !_.isUndefined(userDataa.primary_investment_id) ? array : [];

    let primaryInvest = [];

    for (let i = 0; i < myArray?.length; i++) {
      primaryInvest.push(Number(myArray[i]));
    }

    return primaryInvest;
  };

  useFocusEffect(
    useCallback(() => {
      let isFocus = true;

      secondarySubmit();
      return () => {
        isFocus = false;
      };
    }, []),
  );

  // useEffect(() => {
  //   primarySubmit();
  // }, []);

  const updateBio = async () => {
    Keyboard.dismiss();

    let formData = inserDetails();

    setModal(true);

    if (!_.isUndefined(formData)) {
      EditBioSheet.current.close();

      let res = await profile.updateBio(userToken, formData);

      overAllUpdate(res.is_overall_update);

      setBio(_.isNull(res.bio) ? null : res.bio);

      setMyBio(res.bio);

      setRes(res);
    }

    await AsyncStorage.setItem(BIOUPDATED, JSON.stringify(true));

    setSuccessText('Bio updated');

    profileUpdatedStatus();

    setTimeout(() => {
      setRes(undefined);
    }, 1000);

    setTimeout(() => {
      setModal(false);
    }, 600);
  };

  const inserDetails = () => {
    let formData = new FormData();

    formData && formData.append('id', userId);
    formData && formData.append('bio', bio);
    formData && formData.append('is_bio_update', 1);

    return formData;
  };

  const selectMap = selectData => {
    return selectData.map(data => {
      let selectArray = {
        value: Object.values(data)[0],
        label: Object.values(data)[1],
      };

      return selectArray;
    });
  };

  const onHybridChange = itemValue => {
    setValidationError({
      ...validationError,
      hybrid: false,
    });

    if (itemValue.length <= 2) {
      setSub(itemValue);
    } else if (itemValue.length > 2) {
      Snackbar.show({
        text: 'You have to choose only two investment philosophies',
        duration: Snackbar.LENGTH_SHORT,
      });
    }

    if (itemValue.length <= 2 && itemValue[itemValue.length - 1] == 27) {
      setSector(true);

      SecondaryInvestmentSheet.current.open();

      setSecondary(2);

      setSecondarySubCat(null);

      getSecondaryData(2);
    } else {
      setSector(false);

      SecondaryInvestmentSheet.current.close();
    }
  };

  const getSecondaryData = async id => {
    setUpdateSub(true);

    let response = await profile.getSecondaryInvSubCatergory(id, userToken);

    if (typeof response === 'string') {
      if (response == 'redirect') {
        return navigation.dispatch(StackActions.replace('MpinScreen'));
      }
    }

    if (_.isEmpty(response)) {
      setSubOptions([
        {
          label: 'No sub category under this secondary investment philosophy',
          value: null,
        },
      ]);

      setEnabled(false);
    } else {
      setSubOptions(selectMap(response));
    }
  };

  const channel = async () => {
    let newdata = await profile.getUserData(authUserData.user_id, token);

    let password = newdata.password;

    let username = newdata.username;

    await AsyncStorage.setItem('pwd', password);
    await AsyncStorage.setItem('uname', username);

    navigation.navigate('Social');
  };

  const primarySubmit = async () => {
    let formData = insertPrimary();

    if (formData != undefined) {
      setModal(true);

      PrimaryInvestmentSheet.current.close();

      await AsyncStorage.setItem(P_INVESTUPDATED, JSON.stringify(true));

      let res = await profile.updateInvestment(tokenOfUser, formData);

      let response = await profile.getUserData(userId, token);

      setOriginalRes(response);

      setPrimary(
        !_.isUndefined(response.primary_investment_id) &&
          !_.isNull(response.primary_investment_id)
          ? Number(
              response.primary_investment_id.length == 1
                ? response.primary_investment_id[0]
                : 35,
            )
          : null,
      );
      

      setSub(
        !_.isUndefined(response.primary_investment_id) &&
          !_.isNull(response.primary_investment_id)
          ? response.primary_investment_id.length == 1
            ? []
            : convertPrimary(response.primary_investment_id)
          : [],
      );

      let primaryString = `Hybrid - `
        .concat(response.primary_investment_name[0])
        .concat(`,${response.primary_investment_name[1]}`);

      let myString =
        !_.isUndefined(response.primary_investment_id) &&
        !_.isNull(response.primary_investment_id)
          ? response.primary_investment_id.length == 1
            ? response.primary_investment_name
            : primaryString
          : response.primary_investment_name;

      setPrimaryName(myString);
      // overAllUpdate(res.data.is_overall_update);

      setRes(res);
    }

    setSuccessText('Primary investment updated');

    profileUpdatedStatus();

    setTimeout(() => {
      setRes(undefined);
    }, 1000);

    setTimeout(() => {
      setModal(false);
    }, 600);
  };

  const insertPrimary = () => {
    let formData = new FormData();

    let error = validationError;

    let primeselected = [];
    let secID = AsyncStorage.getItem('secInvestmentID').then(value2 => {
      secondaryID(value2);
    });
    console.log('secID??????', secID);
    if (investment == true) {
      primeselected = sub;
    } else {
      primeselected.push(primary?.value ?? primary);
    }

    formData && formData.append('id', userId);
    formData &&
      formData.append('secondary_investment_subcategory_id', secondarySubCatID);
    formData && formData.append('secondary_investment_id', secondaryID);

    if ((primary?.value ?? primary) == undefined) {
      error['primary'] = 'Choose primary investment philosophy category';
      formData = undefined;
    } else if (
      (primary?.value ?? primary) != undefined &&
      investment == true &&
      sub.length == 0
    ) {
      error['hybrid'] = 'Choose any two primary investment philosophy';
      formData = undefined;
    } else if (
      (primary?.value ?? primary) != undefined &&
      investment == true &&
      sub.length != 2
    ) {
      error['hybrid'] = 'Choose any two primary investment philosophy';
      formData = undefined;
    } else if (
      (primary?.value ?? primary) != undefined &&
      sector == true &&
      (secondarySubCat?.value ?? secondarySubCat) == undefined
    ) {
      setSecondary(2);

      getSecondaryData(2);

      error['primary'] = 'You have to choose secondary sub category for sector';

      Snackbar.show({
        text: 'You have to choose secondary sub category for sector',
        duration: Snackbar.LENGTH_SHORT,
      });

      setTimeout(() => {
        SecondaryInvestmentSheet.current.open();
      }, 1000);

      formData = undefined;
    } else if (
      primeselected != undefined &&
      primeselected.length > 1 &&
      sector == true &&
      (secondary?.value ?? secondary) != 2
    ) {
      setSecondary(2);

      getSecondaryData(2);

      error['hybrid'] = 'You have to choose secondary sub category for sector';

      Snackbar.show({
        text: 'You have to choose secondary sub category for sector',
        duration: Snackbar.LENGTH_SHORT,
      });

      setTimeout(() => {
        SecondaryInvestmentSheet.current.open();
      }, 1000);

      formData = undefined;
    } else {
      formData.append('primary_investment_id', JSON.stringify(primeselected));

      // formData && formData.append('secondary_investment_id', secondary);

      // formData.append(
      //   'secondary_investment_subcategory_id',
      //   secondarySubCat?.value ?? secondarySubCat,
      // );

      formData && formData.append('is_primeinvest_update', 1);
    }

    setValidationError({...validationError, ...error});

    return formData;
  };

  const secondarySubmit = async () => {
    let formData = insertSecondary();

    if (formData != undefined) {
      setModal(true);

      SecondaryInvestmentSheet.current.close();

      let res = await profile.updateSecondaryInvestment(userToken, formData);
      //  let response = await profile.getUserData(authUserData.user_id, token);
      setSecondaryRes(res);

      setSuccessText('Secondary investment updated');

      setSecondary(Number(res?.secondary_investment_id));
      await AsyncStorage.setItem(
        'secInvestmentID',
        JSON.stringify(res?.secondary_investment_id),
      );
      setSecondaryID(res?.secondary_investment_id);
      setSecondarySubCat(Number(res?.secondary_investment_subcategory_id));
      setSecondarySubCatID(res?.secondary_investment_subcategory_id);
      setSecondaryName(res?.secondary_investment_name);

      setSecondarySubCatName(res?.secondary_investment_subcategory_name);

      setRes(res);
    }

    await AsyncStorage.setItem(S_INVESTUPDATED, JSON.stringify(true));

    profileUpdatedStatus();

    setTimeout(() => {
      setRes(undefined);
    }, 1000);

    setTimeout(() => {
      setModal(false);
    }, 600);
  };

  const insertSecondary = () => {
    let formData = new FormData();

    let error = validationError;

    let primeselected = [];

    if (investment == true) {
      primeselected = sub;
    } else {
      primeselected.push(primary?.value ?? primary);
    }

    formData && formData.append('id', userId);

    if ((secondary?.value ?? secondary) == undefined) {
      error['secondary'] = 'Choose secondary investment philosophy';

      formData = undefined;
    } else {
      formData &&
        formData.append(
          'secondary_investment_subcategory_id',
          secondarySubCat?.value ?? secondarySubCat,
        );
    }

    if ((secondarySubCat?.value ?? secondarySubCat) == undefined) {
      error['secondarySub'] = 'Choose secondary investment sub category';

      formData = undefined;
    }

    if (
      sector == true &&
      (secondary?.value ?? secondary) != 2 &&
      (primary?.value ?? primary) == 27
    ) {
      error['secondary'] = 'You don`t have access to choose inudstry';

      formData = undefined;
    } else {
      formData &&
        formData.append(
          'secondary_investment_id',
          secondary?.value ?? secondary,
        );
    }

    if (
      primeselected != undefined &&
      primeselected.length > 1 &&
      (secondary?.value ?? secondary) != 2 &&
      sector == true
    ) {
      error['secondary'] = 'You don`t have access to choose inudstry';

      formData = undefined;
    }

    formData && formData.append('is_secinvest_update', 1);

    setValidationError({...validationError, ...error});

    return formData;
  };

  const handleLogout = async () => {
    Snackbar.show({
      duration: Snackbar.LENGTH_SHORT,
      text: 'Logging out...',
    });

    const response = await auth.logout();

    if (response.keyword == 'success') {
      Snackbar.show({
        duration: Snackbar.LENGTH_SHORT,
        text: response.message,
      });

      updateFCM();

      await AsyncStorage.getAllKeys().then(
        async keys => await AsyncStorage.multiRemove(keys),
      );

      navigation.dispatch(StackActions.replace('Login'));
    } else {
      Snackbar.show({
        backgroundColor: 'red',
        duration: Snackbar.LENGTH_LONG,
        text: response.message,
      });
    }
  };

  const updateFCM = async () => {
    const requestData = {
      firebase_token: null,
    };

    await auth.updateFCM(requestData);
  };

  const onShare = () => {
    ShareSheet.current.close();

    setTimeout(async () => {
      try {
        let text = 'Invest and trade with echo investing';

        if (Platform.OS === 'android') {
          text = text.concat(' - http://echoapp.biz/');
        } else {
        }

        let options = {
          subject: 'Echo Investing',
          title: 'Echo Investing',
          message: text,
          url: 'http://echoapp.biz/',
        };

        const result = await Share.share(options);

        if (result.action === Share.sharedAction) {
          if (result.activityType) {
            // shared with activity type of result.activityType
          } else {
            // shared
          }
        } else if (result.action === Share.dismissedAction) {
          // dismissed
        }
      } catch (error) {}
    }, 250);
  };

  const sendFeedback = async () => {
    let formData = validateFields();

    let token = await AsyncStorage.getItem(TOKEN);

    if (formData !== undefined) {
      setFLoading(true);

      const res = await dashboard.sendFeedback(formData, token);

      setFLoading(false);

      res
        ? (setFeedbackRes(res.keyword),
          setTimeout(() => {
            setModalVisible(false);
            setSubject(null);
            setFeedbackRes('');
            SetNote('');
            setFieldError(errorInitialState);
          }, 2000))
        : (setModalVisible(false),
          setSubject(null),
          setFeedbackRes(''),
          SetNote(''),
          setFieldError(errorInitialState));
    }
  };

  const validateFields = () => {
    let formData = new FormData();

    let error = fieldError;

    if (subject?.value ?? null !== null) {
      error['subject'] = false;

      formData && formData.append('subject', subject?.value ?? null);
    } else {
      error['subject'] = true;

      formData = undefined;
    }

    if (note !== '' && note.length > 25) {
      error['note'] = false;

      formData && formData.append('content', note);
    } else {
      error['note'] = true;

      formData = undefined;
    }

    setFieldError({
      ...fieldError,
      ...error,
    });

    return formData;
  };

  const renderRBSecondary = () => {
    return (
      <RBSheet
        ref={SecondaryInvestmentSheet}
        closeOnDragDown={false}
        closeOnPressMask={false}
        closeOnPressBack={true}
        height={280}
        openDuration={250}
        customStyles={{
          container: {
            backgroundColor: theme.themeColor,
            borderTopLeftRadius: 16,
            borderTopRightRadius: 16,
          },
          wrapper: {backgroundColor: 'grey', opacity: 0.9},
          draggableIcon: {
            display: 'none',
          },
        }}>
        <TouchableOpacity
          onPress={() => {
            setSecondary(Number(secondaryRes.secondary_investment_id));

            setSecondarySubCat(
              Number(secondaryRes.secondary_investment_subcategory_id),
            );

            SecondaryInvestmentSheet.current.close();
          }}>
          <View
            style={{
              alignSelf: 'flex-end',
              paddingHorizontal: 16,
              paddingVertical: 16,
            }}>
            <Icon name="close" color="white" size={18} />
          </View>
        </TouchableOpacity>

        <SecondaryInvestment
          options={selectData.secondaryInv}
          value={secondary}
          onChange={itemValue => {
            setSecondary(itemValue);

            getSecondaryData(itemValue?.value);

            setSecondarySubCat(null);

            setValidationError({
              ...validationError,
              primary: false,
              secondary: _.isNumber(itemValue?.value)
                ? false
                : 'Choose secondary investment philosophy category',
            });
          }}
          enabled={enabled}
          subOptions={updateSub ? subOptions : selectData.secondarySub}
          subValue={parseInt(secondarySubCat)}
          onSubChange={itemValue => {
            setSecondarySubCat(itemValue);

            setValidationError({
              ...validationError,
              secondarySub: _.isNumber(itemValue?.value)
                ? false
                : 'Choose secondary investment sub category',
            });
          }}
          onSecondarySubmit={secondarySubmit}
          validationError={validationError}
        />
      </RBSheet>
    );
  };

  const renderFeedBackModal = () => {
    return (
      <Modal animationType="fade" transparent={true} isVisible={modalVisible}>
        <KeyboardAvoidingView
          behavior={Platform.OS === 'ios' ? 'padding' : null}
          keyboardVerticalOffset={Platform.OS === 'ios' ? 40 : 0}>
          <View style={styles.modalView}>
            {fLoading ? (
              <ActivityIndicator size="small" color="white" />
            ) : feedbackres === 'success' ? (
              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 14 : 16,
                  color: theme.white,
                  textAlign: 'center',
                }}>
                Feedback submitted successfully
              </Text>
            ) : (
              <>
                <TouchableOpacity
                  onPress={() => {
                    setModalVisible(false);
                  }}>
                  <View style={{alignSelf: 'flex-end'}}>
                    <Icon name="close" color="white" size={18} />
                  </View>
                </TouchableOpacity>

                <Text style={styles.popuptitle}>Feedback</Text>

                <SelectDropdown
                  buttonStyle={styles.dropdownWrapper}
                  data={category ?? []}
                  defaultButtonText="Subject Line"
                  dropdownStyle={{
                    width: '60%',
                    marginLeft: 8,
                    alignSelf: 'center',
                  }}
                  onSelect={selectedItem => {
                    setSubject(selectedItem);
                  }}
                  renderCustomizedButtonChild={selectedItem => {
                    return (
                      <View style={styles.dropdownItemContainer}>
                        <View style={styles.dropdownLabelContainer}>
                          <Text style={styles.dropdownLabelText}>
                            {selectedItem?.label ?? 'Subject Line'}
                          </Text>
                        </View>
                        <View style={styles.dropdownIconContainer}>
                          <FontAwesomeIcons
                            name="caret-down"
                            size={16}
                            color={'white'}
                          />
                        </View>
                      </View>
                    );
                  }}
                  rowStyle={{borderBottomWidth: 0}}
                  rowTextForSelection={item => item?.label}
                  rowTextStyle={styles.dropdownItemText}
                  selectedRowTextStyle={styles.dropdownSelectedItemText}
                  defaultValue={subject}
                />

                {fieldError.subject && (
                  <Text style={[styles.errorMsg, {marginVertical: 4}]}>
                    Choose subject
                  </Text>
                )}

                <View style={styles.selectfield}>
                  <Text style={styles.labeltxt}> Note</Text>

                  <TextInput
                    value={note}
                    multiline={true}
                    numberOfLines={4}
                    onChangeText={text => {
                      SetNote(text);
                    }}
                    style={[
                      styles.selectinput,
                      {
                        height: windowHeight * 0.15,
                        borderRadius: 8,
                        marginTop: 8,
                      },
                    ]}
                  />
                </View>

                {fieldError.note && (
                  <Text style={[styles.errorMsg, {marginVertical: 4}]}>
                    Enter minimum 25 characters
                  </Text>
                )}

                <TouchableHighlight
                  style={styles.saveButton}
                  onPress={() => {
                    sendFeedback();
                  }}>
                  <Text style={styles.btntext}>Save</Text>
                </TouchableHighlight>
              </>
            )}
          </View>
        </KeyboardAvoidingView>
      </Modal>
    );
  };

  return (
    <>
      <ScrollView
        keyboardShouldPersistTaps={'handled'}
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
        style={styles.container}>
        <View style={styles.bio}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginVertical: 8,
            }}>
            <View style={{flex: 0.9, justifyContent: 'center'}}>
              <Text style={styles.labeltxt}>Bio</Text>
            </View>

            <View
              style={{
                flex: 0.1,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Icon
                name="pencil"
                color={theme.secondryColor}
                size={18}
                onPress={() => EditBioSheet.current.open()}
              />
            </View>
          </View>

          {String(bio).length > 250 ? (
            <Text style={styles.biodata}>
              {viewMore == false && (
                <>
                  {String(bio).substr(0, 250)}...
                  <Text
                    style={styles.viewmore}
                    onPress={() => {
                      setViewMore(true);
                    }}>
                    View More
                  </Text>
                </>
              )}
              {viewMore == true && (
                <>
                  {String(bio)}...
                  <Text
                    style={styles.viewmore}
                    onPress={() => {
                      setViewMore(false);
                    }}>
                    View Less
                  </Text>
                </>
              )}
            </Text>
          ) : (
            <Text style={styles.biodata}>
              {_.isEmpty(bio) || _.isNull(bio) ? (
                <Text style={{color: 'grey'}}>
                  Click edit icon and update your bio
                </Text>
              ) : (
                `${bio}`
              )}
            </Text>
          )}

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginVertical: 8,
            }}>
            <View style={{flex: 0.9, justifyContent: 'center'}}>
              <Text style={[styles.labeltxt, {color: theme.secondryColor}]}>
                Primary Investment Philosophy
              </Text>
            </View>

            <View
              style={{
                flex: 0.1,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Icon
                name="pencil"
                color={theme.secondryColor}
                size={18}
                onPress={() => PrimaryInvestmentSheet.current.open()}
              />
            </View>
          </View>

          <Text style={styles.biodata}>
            {primaryName != null
              ? !_.isEmpty(primaryName)
                ? [...primaryName]
                : userDataa?.primary_investment_name
              : '-'}
          </Text>

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginVertical: 8,
            }}>
            <View style={{flex: 0.9, justifyContent: 'center'}}>
              <Text style={[styles.labeltxt, {color: theme.secondryColor}]}>
                Secondary Investment Philosophy
              </Text>
            </View>

            <View
              style={{
                flex: 0.1,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Icon
                name="pencil"
                color={theme.secondryColor}
                size={18}
                onPress={() => {
                  SecondaryInvestmentSheet.current &&
                    SecondaryInvestmentSheet.current.open();
                }}
              />
            </View>
          </View>

          <Text style={styles.biodata}>
            {!_.isEmpty(secondaryName)
              ? secondaryName == null || undefined ? '-' : `${secondaryName} - ${secondarySubCatName}`
              :  userDataa?.secondary_investment_name == null || undefined ? '-' : ` ${ userDataa?.secondary_investment_name} - ${userDataa?.secondary_investment_subcategory_name} `}
          </Text>

          <View style={{marginVertical: 8}}>
            <Text style={styles.labeltxt}>Email</Text>

            <Text style={styles.biodata}>{userDataa.email || '-'}</Text>
          </View>

          <View style={{marginVertical: 8}}>
            <Text style={styles.labeltxt}>Phone</Text>

            <Text style={styles.biodata}>
              {userDataa?.phone != 'null' ? userDataa.phone : '-'}
            </Text>
          </View>
        </View>

        <View style={styles.socialtitle}>
          <Text style={styles.socialtxt}>Get Social</Text>
        </View>

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            marginHorizontal: 16,
            marginVertical: 16,
          }}>
          <TouchableOpacity
            style={{
              flex: 0.475,
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: '#02c7d7',
              borderRadius: 4,
              paddingHorizontal: 12,
              paddingVertical: 12,
            }}
            onPress={channel}>
            <Text
              style={{
                fontSize: Platform.OS == 'ios' ? 12 : 14,
                color: theme.white,
                fontWeight: '600',
                textAlign: 'center',
              }}>
              Channel
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={{
              flex: 0.475,
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: '#2e2e2e',
              borderRadius: 4,
              paddingHorizontal: 12,
              paddingVertical: 12,
            }}
            onPress={() => ShareSheet.current.open()}>
            <Text
              style={{
                fontSize: Platform.OS == 'ios' ? 12 : 14,
                color: theme.white,
                fontWeight: '600',
                textAlign: 'center',
              }}>
              Share
            </Text>
          </TouchableOpacity>
        </View>

        <TouchableOpacity
          onPress={() => {
            handleLogout();
          }}
          style={styles.logout}>
          <Text
            style={{
              fontSize: Platform.OS == 'ios' ? 14 : 16,
              color: theme.secondryColor,
              fontWeight: '800',
              textAlign: 'center',
            }}>
            Logout
          </Text>
        </TouchableOpacity>

        <RBSheet
          ref={EditBioSheet}
          closeOnDragDown={true}
          closeOnPressMask={false}
          closeOnPressBack={true}
          height={windowHeight * 0.375}
          openDuration={250}
          customStyles={{
            container: {
              backgroundColor: theme.themeColor,
              borderTopLeftRadius: 16,
              borderTopRightRadius: 16,
            },
            wrapper: {backgroundColor: 'grey', opacity: 0.9},
            draggableIcon: {
              display: 'none',
            },
          }}>
          <>
            <TouchableOpacity
              onPress={() => {
                setMyBio(myBio);

                EditBioSheet.current.close();
              }}>
              <View
                style={{
                  alignSelf: 'flex-end',
                  paddingHorizontal: 16,
                  paddingVertical: 16,
                }}>
                <Icon name="close" color="white" size={18} />
              </View>
            </TouchableOpacity>

            <EditBio
              onChange={text => setBio(text)}
              value={bio}
              onSubmit={updateBio}
            />
          </>
        </RBSheet>

        {renderRBSecondary()}

        <RBSheet
          ref={PrimaryInvestmentSheet}
          closeOnDragDown={false}
          closeOnPressMask={false}
          closeOnPressBack={true}
          height={investment ? 280 : 220}
          openDuration={250}
          customStyles={{
            container: {
              backgroundColor: theme.themeColor,
              borderTopLeftRadius: 16,
              borderTopRightRadius: 16,
            },
            wrapper: {backgroundColor: 'grey', opacity: 0.9},
            draggableIcon: {
              display: 'none',
            },
          }}>
          <TouchableOpacity
            onPress={() => {
              setPrimary(
                !_.isUndefined(originalRes.primary_investment_id) &&
                  !_.isNull(originalRes.primary_investment_id)
                  ? Number(
                      originalRes.primary_investment_id?.length == 1
                        ? originalRes.primary_investment_id[0]
                        : 35,
                    )
                  : null,
              );

              if (originalRes.primary_investment_id?.length > 1) {
                setInvestment(true);
              } else {
                setInvestment(false);
              }

              setSub(
                !_.isUndefined(originalRes.primary_investment_id) &&
                  !_.isNull(originalRes.primary_investment_id)
                  ? originalRes.primary_investment_id?.length == 1
                    ? []
                    : convertPrimary(originalRes.primary_investment_id)
                  : [],
              );

              PrimaryInvestmentSheet.current.close();
            }}>
            <View
              style={{
                alignSelf: 'flex-end',
                paddingHorizontal: 16,
                paddingVertical: 16,
              }}>
              <Icon name="close" color="white" size={18} />
            </View>
          </TouchableOpacity>

          <PrimaryInvestment
            options={selectData.primaryInv}
            hybridOptions={myArray}
            value={primary}
            investment={investment}
            onChange={itemValue => {
              setPrimary(itemValue);

              setSub([]);

              if (itemValue?.value == 35) {
                setInvestment(true);
              } else {
                setInvestment(false);
              }

              if (itemValue?.value == 27) {
                setSector(true);

                SecondaryInvestmentSheet.current.open();

                setSecondary(2);

                setSecondarySubCat(null);

                getSecondaryData(2);
              } else {
                setSector(false);

                SecondaryInvestmentSheet.current.close();
              }

              setValidationError({
                ...validationError,
                hybrid: false,
                primary: _.isNumber(itemValue?.value)
                  ? false
                  : 'Choose primary investment philosophy category',
              });
            }}
            onPrimarySubmit={primarySubmit}
            validationError={validationError}
            onHybridChange={onHybridChange}
            sub={sub}
          />

          {renderRBSecondary()}
        </RBSheet>

        <RBSheet
          ref={ShareSheet}
          closeOnDragDown={true}
          closeOnPressMask={false}
          closeOnPressBack={true}
          height={160}
          openDuration={250}
          customStyles={{
            container: {
              backgroundColor: theme.themeColor,
              borderTopLeftRadius: 16,
              borderTopRightRadius: 16,
            },
            wrapper: {backgroundColor: 'grey', opacity: 0.9},
            draggableIcon: {
              display: 'none',
            },
          }}>
          <TouchableOpacity onPress={() => ShareSheet.current.close()}>
            <View
              style={{
                alignSelf: 'flex-end',
                paddingHorizontal: 16,
                paddingVertical: 16,
              }}>
              <Icon name="close" color="white" size={18} />
            </View>
          </TouchableOpacity>

          <View style={{paddingHorizontal: 16, paddingVertical: 8}}>
            <Text
              style={{
                fontSize: Platform.OS == 'ios' ? 16 : 18,
                color: theme.white,
                fontWeight: 'bold',
              }}
              onPress={() => {
                onShare();
              }}>
              Share App
            </Text>

            <Text
              style={{
                fontSize: Platform.OS == 'ios' ? 16 : 18,
                color: theme.white,
                fontWeight: 'bold',
                marginTop: 16,
              }}
              onPress={() => {
                ShareSheet.current.close();

                setTimeout(() => {
                  setModalVisible(true);
                }, 250);
              }}>
              Feedback
            </Text>
          </View>
        </RBSheet>

        {renderFeedBackModal()}

        <SuccessPopUp
          res={res}
          successText={successText}
          modal={modal}
          onBackdropPress={() => setModal(false)}
        />
      </ScrollView>
    </>
  );
};
export default ProfileScreen;
