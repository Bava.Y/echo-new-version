import {useFocusEffect} from '@react-navigation/native';
import React, {useCallback, useState} from 'react';
import {
  KeyboardAvoidingView,
  LogBox,
  Platform,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import MultiSelect from 'react-native-multiple-select';
import SelectDropdown from 'react-native-select-dropdown';
import FontAwesomeIcons from 'react-native-vector-icons/FontAwesome5';
import theme from '../../utils/theme';
import styles from './style';

LogBox.ignoreAllLogs();

const PrimaryInvestment = ({
  value,
  onChange,
  options,
  onPrimarySubmit,
  validationError,
  sub,
  hybridOptions,
  onHybridChange,
  investment,
}) => {
  // PrimaryInvestment Variables
  const [primary, setPrimary] = useState(null);

  useFocusEffect(
    useCallback(() => {
      loadState(value ?? null);
    }, [value]),
  );

  const loadState = (value) => {
    if (Boolean(options) && Array.isArray(options) && options.length != 0) {
      const filteredPrimary = options.filter((lol) => lol?.value == value);

      setPrimary(filteredPrimary.length != 0 ? filteredPrimary[0] : null);
    } else {
      setPrimary(null);
    }
  };

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : null}
      keyboardVerticalOffset={Platform.OS === 'ios' ? 40 : 0}
      style={{flex: 1, backgroundColor: theme.themeColor}}>
      <ScrollView
        keyboardShouldPersistTaps="handled"
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
        style={{marginBottom: 24, paddingHorizontal: 16}}>
        <Text style={styles.title}>Primary Investment Philosophy</Text>

        <SelectDropdown
          buttonStyle={styles.dropdownWrapper}
          data={options ?? []}
          defaultButtonText="Primary Investment Philosophy"
          dropdownStyle={{width: '80%', marginLeft: 8, alignSelf: 'center'}}
          onSelect={onChange}
          renderCustomizedButtonChild={(selectedItem) => {
            return (
              <View style={styles.dropdownItemContainer}>
                <View style={styles.dropdownLabelContainer}>
                  <Text style={styles.dropdownLabelText}>
                    {selectedItem?.label ?? 'Primary Investment Philosophy'}
                  </Text>
                </View>
                <View style={styles.dropdownIconContainer}>
                  <FontAwesomeIcons
                    name="caret-down"
                    size={16}
                    color={'white'}
                  />
                </View>
              </View>
            );
          }}
          rowStyle={{borderBottomWidth: 0}}
          rowTextForSelection={(item) => item?.label}
          rowTextStyle={styles.dropdownItemText}
          selectedRowTextStyle={styles.dropdownSelectedItemText}
          defaultValue={primary}
        />

        {validationError.primary ? (
          <Text style={[styles.errorMsg, {marginVertical: 4}]}>
            {validationError.primary}
          </Text>
        ) : null}

        {investment && (
          <MultiSelect
            displayKey="label"
            hideTags={true}
            items={hybridOptions}
            itemTextColor="white"
            onSelectedItemsChange={onHybridChange}
            searchInputPlaceholderText="Choose"
            searchInputStyle={{
              fontSize: Platform.OS == 'ios' ? 14 : 16,
              color: 'white',
            }}
            selectedItems={sub}
            selectText="Choose"
            styleDropdownMenuSubsection={{
              height: 50,
              backgroundColor: theme.primaryColor,
              borderColor: 'white',
              borderWidth: 1,
              borderRadius: 50,
            }}
            styleInputGroup={{
              backgroundColor: theme.primaryColor,
              borderColor: 'white',
              borderWidth: 1,
              borderRadius: 50,
              paddingHorizontal: 12,
              paddingVertical: 10,
            }}
            styleItemsContainer={{
              height: 160,
              backgroundColor: theme.themeColor,
              paddingHorizontal: 8,
              paddingVertical: 8,
            }}
            styleMainWrapper={{
              marginVertical: 8,
            }}
            styleTextDropdown={{
              fontSize: Platform.OS == 'ios' ? 1 : 16,
              color: 'white',
              paddingHorizontal: 8,
            }}
            styleTextDropdownSelected={{
              fontSize: Platform.OS == 'ios' ? 14 : 16,
              color: 'white',
              paddingHorizontal: 8,
            }}
            hideSubmitButton={true}
            uniqueKey="value"
          />
        )}

        {investment && validationError.hybrid ? (
          <Text style={[styles.errorMsg, {marginVertical: 4}]}>
            {validationError.hybrid}
          </Text>
        ) : null}

        <TouchableOpacity style={styles.saveButton} onPress={onPrimarySubmit}>
          <Text style={styles.btntext}>Submit</Text>
        </TouchableOpacity>
      </ScrollView>
    </KeyboardAvoidingView>
  );
};

export default PrimaryInvestment;
