import {useNavigation} from '@react-navigation/native';
import React, {useContext} from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import AutoScrolling from 'react-native-auto-scrolling';
import Icon from 'react-native-vector-icons/FontAwesome';
import {CISCO} from './../assets/images';
import {UserDataContext} from '../context/UserDataContext';
import styles from './../screens/Dashboard/styles';

const Ticker = ({data}) => {
  const {tickerVisible} = useContext(UserDataContext);
  const navigation = useNavigation();

  return tickerVisible ? (
    <View style={styles.ticker}>
      {/* <Ticker> */}
      <AutoScrolling endPaddingWidth={0}>
        <View style={{flexDirection: 'row'}}>
          {data.map((ticker, index) => {
            let isNegative = ticker.change < 0 ? true : false;

            return (
              <TouchableOpacity
                key={index}
                style={styles.tickercolumn}
                onPress={() =>
                  navigation.push('CompanyDetail', {
                    symbol: ticker.symbol,
                    name: ticker.company_name,
                    ticker,
                  })
                }>
                <View style={{justifyContent: 'center', alignItems: 'center'}}>
                  <Image
                    resizeMode={'contain'}
                    style={styles.imageIcon}
                    source={ticker.logo ? {uri: ticker.logo} : CISCO}
                  />
                </View>

                <View style={styles.content}>
                  <Text style={styles.companyname}>{ticker.company_name}</Text>

                  <View style={{flexDirection: 'row', marginTop: 4}}>
                    <Text
                      style={isNegative ? styles.textred : styles.textgreen}>
                      {ticker.latest_price ? `$ ${ticker.latest_price}` : ''}
                    </Text>

                    <View
                      style={{
                        width: 16,
                        alignSelf: 'center',
                        backgroundColor: isNegative ? 'red' : 'green',
                        marginHorizontal: 8,
                        paddingVertical: 4,
                        borderRadius: 4,
                      }}>
                      <Icon
                        name={isNegative ? 'arrow-down' : 'arrow-up'}
                        color="white"
                        size={8}
                        style={{alignSelf: 'center'}}
                      />
                    </View>

                    <Text
                      style={isNegative ? styles.textred : styles.textgreen}>
                      {ticker.change}
                    </Text>
                  </View>
                </View>
              </TouchableOpacity>
            );
          })}
        </View>
      </AutoScrolling>
    </View>
  ) : (
    <></>
  );
};

export default Ticker;
