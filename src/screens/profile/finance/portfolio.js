import {useFocusEffect} from '@react-navigation/core';
import React, {useCallback, useContext, useState} from 'react';
import {ScrollView, Text, TouchableOpacity, View} from 'react-native';
import Snackbar from 'react-native-snackbar';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {profile} from '../../../api';
import Loading from '../../../components/Loading';
import {UserDataContext} from '../../../context/UserDataContext';
import theme from '../../../utils/theme';
import styles from './styles';

const Portfolio = () => {
  // Portfolio Variables
  const [responseData, setResponseData] = useState(null);
  const [alpacaShares, setAlpacaShares] = useState(null);
  const [tdShares, setTDShares] = useState(null);

  // Context Variables
  const {alpacaConfigStatus, tdConfigStatus} = useContext(UserDataContext);

  // Other Variables
  const [alpacaExpandStatus, setAlpacaExpandStatus] = useState(false);
  const [tdExpandStatus, setTDExpandStatus] = useState(false);

  useFocusEffect(
    useCallback(() => {
      let isFocus = true;

      fetchPortfolio();

      return () => {
        isFocus = false;
      };
    }, []),
  );

  const fetchPortfolio = async () => {
    const response = await profile.getPortfolio();

    if (response && response.keyword == 'success') {
      if (response.data) {
        setResponseData(response.data);
        setAlpacaShares(response.data.alpaca_share_details || null);
        setTDShares(response.data.tdma_share_details || null);
      } else {
        setResponseData(null);
        setAlpacaShares(null);
        setTDShares(null);
      }
    } else if (response.keyword == 'failed') {
      Snackbar.show({
        duration: Snackbar.LENGTH_SHORT,
        text: response.message,
      });

      setResponseData(null);
      setAlpacaShares(null);
      setTDShares(null);
    } else {
      Snackbar.show({
        backgroundColor: 'red',
        duration: Snackbar.LENGTH_SHORT,
        text: response.message,
      });

      setResponseData(null);
      setAlpacaShares(null);
      setTDShares(null);
    }
  };

  const isFloat = (n) => {
    return Number(n) % 1 !== 0;
  };

  return (
    <View style={styles.container}>
      {responseData ? (
        <ScrollView
          keyboardShouldPersistTaps={'handled'}
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}>
          <View style={styles.mainsec}>
            <Text style={styles.headingTxt}>Echo Portfolio</Text>

            <View style={{marginVertical: 8}}>
              <View style={[styles.lineContainer, {marginTop: 8}]}>
                <View style={styles.lineLabelContainer}>
                  <Text style={[styles.lineLabelTxt, {fontWeight: '600'}]}>
                    Total Stocks & ETFs
                  </Text>
                </View>

                <View style={styles.lineValueContainer}>
                  <Text style={[styles.lineValueTxt, {fontWeight: '600'}]}>
                    ${' '}
                    {responseData.total_portfolio_balance ?responseData.total_portfolio_balance
                     
                     : 0.0}
                  </Text>
                </View>
              </View>

              {alpacaConfigStatus && (
                <View style={[styles.lineContainer, {marginTop: 8}]}>
                  <View style={styles.lineLabelContainer}>
                    <Text style={styles.lineLabelTxt}>
                      Alpaca Stocks & ETFs
                    </Text>
                  </View>

                  <View style={styles.lineValueContainer}>
                    <Text style={styles.lineValueTxt}>
                      ${' '}
                      {responseData.alpaca_portfolio_balance? 
                        responseData.alpaca_portfolio_balance : 0.0}
                    </Text>
                  </View>
                </View>
              )}

              {tdConfigStatus && (
                <View style={[styles.lineContainer, {marginTop: 8}]}>
                  <View style={styles.lineLabelContainer}>
                    <Text style={styles.lineLabelTxt}>
                      TD Ameritrade Portfolio Balance
                    </Text>
                  </View>

                  <View style={styles.lineValueContainer}>
                    <Text style={styles.lineValueTxt}>
                      ${' '}
                      {parseFloat(responseData.td_portfolio_balance)}
                    </Text>
                  </View>
                </View>
              )}
            </View>

            {((alpacaShares && alpacaShares.length != 0) ||
              (tdShares && tdShares.length != 0)) && (
              <View style={{marginVertical: 8}}>
                {alpacaConfigStatus &&
                  alpacaShares &&
                  Array.isArray(alpacaShares) &&
                  alpacaShares.length != 0 && (
                    <>
                      <View
                        style={{
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                          alignItems: 'center',
                        }}>
                        <Text style={styles.finalLabelTxt}>Alpaca Stocks</Text>

                        <TouchableOpacity
                          onPress={() => {
                            setAlpacaExpandStatus(!alpacaExpandStatus);
                          }}>
                          <MaterialIcons
                            name={
                              alpacaExpandStatus
                                ? 'remove-circle'
                                : 'add-circle'
                            }
                            size={20}
                            color={'#ffffffc2'}
                          />
                        </TouchableOpacity>
                      </View>

                      {alpacaExpandStatus &&
                        alpacaShares.map((alpacaShareData, index) => (
                          <View
                            key={index}
                            style={[styles.lineContainer, {marginTop: 8}]}>
                            <View
                              style={[
                                styles.lineLabelContainer,
                                {
                                  flexDirection: 'row',
                                  justifyContent: 'space-between',
                                },
                              ]}>
                              <View
                                style={{flex: 0.375, justifyContent: 'center'}}>
                                <Text style={styles.lineLabelTxt}>
                                  {alpacaShareData.symbol}
                                </Text>
                              </View>

                              <View
                                style={{flex: 0.625, justifyContent: 'center'}}>
                                <Text style={styles.lineLabelTxt}>
                                  {parseFloat(alpacaShareData.qty).toFixed(
                                    isFloat(alpacaShareData.qty) ? 2 : 0,
                                  ) || 0}{' '}
                                  shares
                                  {alpacaShareData.qty &&
                                  alpacaShareData.total_amount &&
                                  alpacaShareData.total_amount != 0
                                    ? ` @ $ ${(
                                        alpacaShareData.total_amount /
                                        parseFloat(alpacaShareData.qty).toFixed(
                                          isFloat(alpacaShareData.qty) ? 2 : 0,
                                        )
                                      ).toFixed(2)}`
                                    : null}
                                </Text>
                              </View>
                            </View>

                            <View style={styles.lineValueContainer}>
                              <Text style={styles.lineValueTxt}>
                                ${' '}
                                {parseFloat(
                                  alpacaShareData.total_amount,
                                ).toFixed(
                                  isFloat(alpacaShareData.total_amount) ? 2 : 0,
                                ) || 0.0}
                              </Text>
                            </View>
                          </View>
                        ))}
                    </>
                  )}

                {tdConfigStatus &&
                  tdShares &&
                  Array.isArray(tdShares) &&
                  tdShares.length != 0 && (
                    <>
                      <View
                        style={{
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                          alignItems: 'center',
                          marginTop:
                            alpacaConfigStatus &&
                            alpacaShares &&
                            Array.isArray(alpacaShares) &&
                            alpacaShares.length != 0
                              ? 16
                              : 0,
                        }}>
                        <Text style={styles.finalLabelTxt}>
                          TD Ameritarde Stocks
                        </Text>

                        <TouchableOpacity
                          onPress={() => {
                            setTDExpandStatus(!tdExpandStatus);
                          }}>
                          <MaterialIcons
                            name={
                              tdExpandStatus ? 'remove-circle' : 'add-circle'
                            }
                            size={20}
                            color={'#ffffffc2'}
                          />
                        </TouchableOpacity>
                      </View>

                      {tdExpandStatus &&
                        tdShares.map((tdShareData, index) => (
                          <View
                            key={index}
                            style={[styles.lineContainer, {marginTop: 8}]}>
                            <View
                              style={[
                                styles.lineLabelContainer,
                                {
                                  flexDirection: 'row',
                                  justifyContent: 'space-between',
                                },
                              ]}>
                              <View
                                style={{flex: 0.375, justifyContent: 'center'}}>
                                <Text style={styles.lineLabelTxt}>
                                  {tdShareData.symbol}
                                </Text>
                              </View>

                              <View
                                style={{flex: 0.625, justifyContent: 'center'}}>
                                <Text style={styles.lineLabelTxt}>
                                  {parseFloat(tdShareData.qty).toFixed(
                                    isFloat(tdShareData.qty) ? 2 : 0,
                                  ) || 0}{' '}
                                  shares
                                  {tdShareData.qty &&
                                  tdShareData.total_amount &&
                                  tdShareData.total_amount != 0
                                    ? ` @ $ ${(
                                        tdShareData.total_amount /
                                        parseFloat(tdShareData.qty).toFixed(
                                          isFloat(tdShareData.qty) ? 2 : 0,
                                        )
                                      ).toFixed(2)}`
                                    : null}
                                </Text>
                              </View>
                            </View>

                            <View style={styles.lineValueContainer}>
                              <Text style={styles.lineValueTxt}>
                                ${' '}
                                {parseFloat(tdShareData.total_amount).toFixed(
                                  isFloat(tdShareData.total_amount) ? 2 : 0,
                                ) || 0.0}
                              </Text>
                            </View>
                          </View>
                        ))}
                    </>
                  )}
              </View>
            )}

            <View style={{marginVertical: 8}}>
              <View style={[styles.lineContainer, {marginTop: 8}]}>
                <View style={styles.lineLabelContainer}>
                  <Text style={[styles.lineLabelTxt, {fontWeight: '600'}]}>
                    Total Cash
                  </Text>
                </View>

                <View style={styles.lineValueContainer}>
                  <Text style={[styles.lineValueTxt, {fontWeight: '600'}]}>
                    ${' '}
                    {responseData.total_cash_balance}
                  </Text>
                </View>
              </View>

              {alpacaConfigStatus && (
                <View style={[styles.lineContainer, {marginTop: 8}]}>
                  <View style={styles.lineLabelContainer}>
                    <Text style={styles.lineLabelTxt}>Alpaca Cash</Text>
                  </View>

                  <View style={styles.lineValueContainer}>
                    <Text style={styles.lineValueTxt}>
                      ${' '}
                      {responseData.alpaca_balance}
                    </Text>
                  </View>
                </View>
              )}

              {tdConfigStatus && (
                <View style={[styles.lineContainer, {marginTop: 8}]}>
                  <View style={styles.lineLabelContainer}>
                    <Text style={styles.lineLabelTxt}>
                      TD Ameritrade Cash Balance
                    </Text>
                  </View>

                  <View style={styles.lineValueContainer}>
                    <Text style={styles.lineValueTxt}>
                      ${' '}
                      {parseFloat(responseData.td_cash_balance).toFixed(
                        isFloat(responseData.td_cash_balance) ? 2 : 0,
                      ) || 0.0}
                    </Text>
                  </View>
                </View>
              )}
            </View>
          </View>

          <View style={{paddingVertical: 16}} />
        </ScrollView>
      ) : (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Loading color={theme.white} />
        </View>
      )}
    </View>
  );
};

export default Portfolio;
