import {useFocusEffect} from '@react-navigation/core';
import moment from 'moment';
import React, {useCallback, useContext, useEffect, useState} from 'react';
import {
  ScrollView,
  TouchableOpacity,
  View,
  FlatList,
  Text,
  Dimensions,
} from 'react-native';
import Snackbar from 'react-native-snackbar';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {portfolio} from '../../api';
import Loading from '../../components/Loading';
import {UserDataContext} from '../../context/UserDataContext';
import theme from '../../utils/theme';
import PortfolioChart from './chart';
import CompanyList from './companyList';
//import Date from './date';
import styles from './style';
import {
  CandlestickChart,
  LineChart,
  LineChartCursorCrosshair,
  LineChartCursorLine,
  LineChartCursorProps,
} from 'react-native-wagmi-charts';
import DateTimePicker from '@react-native-community/datetimepicker';

const MystockTab = () => {
  // MystockTab Variables
  const [chartOrderData, setChartOrderData] = useState(null);
  const [listOrderData, setListOrderData] = useState([]);
  const [returnsValue, setReturnsValue] = useState(null);
  const [returnsAmount, setReturnsAmount] = useState(null);
  const [lineChartDataMyStock, setLineChartDataMyStock] = useState([]);
  const [chartRange, setChartRange] = useState('1d');
  const [apiCall, setApiCall] = useState(false);

  console.log('returnsValue>>>', returnsValue);
  // Context Variables
  const {
    portfolioConnection,
    portfolioTimePeriod,
    myStockConnection,
    myStockTimePeriod,
    token,
    setmyStockTimePeriod,
    loadMoreData,
    setLoadMoreData,
    setCardLoad,
  } = useContext(UserDataContext);

  // Date Variables
  const [datePicker, setDatePicker] = useState(false);
  const [toDatePicker, setDoDatePicker] = useState(false);

  const [date, setDate] = useState(null);
  const [toDate, setDoDate] = useState(null);
  console.log('toDate?????', toDate);
  // Other VAriables
  const [chartLoading, setChartLoading] = useState(false);
  const [listLoading, setListLoading] = useState(false);
  const [filterStatus, setFilterStatus] = useState(false);
  const [currentPage, setCurrentPage] = useState(0);

  useFocusEffect(
    useCallback(() => {
      let isFocus = true;
      fetchLineChartDataMyStock();
      //  setDate(moment());
      //setDoDate(moment());
      setFilterStatus(false);
      getOrdersChartMystock();
      getOrdersDataMystock();
      return () => {
        isFocus = false;
      };
    }, [chartRange]),
  );
  const deviceWidth = Dimensions.get('window').width;
  useEffect(() => {
    getOrdersDataMystock();
  }, [currentPage]);

  // const handleDatePicker = (selectedDate, status) => {
  //   switch (status) {
  //     // case 'from':
  //     //   setFromDate(selectedDate);
  //     //   break;

  //     case 'to':
  //       setToDate(selectedDate);
  //       break;
  //   }
  // };

  function onDateSelected(event, value) {
    setDatePicker(false);
    setDate(moment(value).format('DD-MM-YYYY'));
  }

  function onToDateSelected(event, value) {
    setDoDatePicker(false);
    setDoDate(moment(value).format('DD-MM-YYYY'));
  }

  const getOrdersChartMystock = async () => {
    let endpoint = `myStock/chart/0?`;

    if (!!myStockTimePeriod) {
      endpoint = endpoint + 'time_period=' + myStockTimePeriod;
    }
    const response = await portfolio.getOrdersChartMystock(endpoint);
    const res = response?.data?.chart;
    const resp = response?.data;
    if (res) {
      if (response.keyword == 'success') {
        let orderArr = [];
        if (response?.length != 0) {
          res?.map(item => {
            orderArr.push({
              name: `${item?.symbol} (${item?.percentage}%)`,
              x: `${item?.percentage}%`,
              y: Number(item?.percentage / 30),
            });
          });

          setChartOrderData(orderArr);
          setReturnsValue(resp?.portfolio_returns);
          setReturnsAmount(resp?.portfolio_returns_amount);
        }
      } else {
        setChartOrderData([]);
        setReturnsValue(null);
        setReturnsAmount(null);
      }
    } else {
      setChartOrderData([]);
      setReturnsValue(null);
      setReturnsAmount(null);
    }
  };

  const getOrdersChartMystockTime = async () => {
    let endpoint = `myStock/chart/${portfolioConnection}?from_date=${date}&to_date=${toDate}`;

    const response = await portfolio.getOrdersChartMystock(endpoint);
    const res = response?.data?.chart;
    const resp = response?.data;
  
    if (res) {
      
      if (response.keyword == 'success') {
        console.log('resp<<>>>',resp)
        let orderArr = [];
        if (res?.length != 0) {
          res?.map(item => {
            orderArr.push({
              name: `${item?.symbol} (${item?.percentage}%)`,
              x: `${item?.percentage}%`,
              y: Number(item?.percentage / 30),
            });
          });

          setChartOrderData(orderArr);
          setReturnsValue(resp?.portfolio_returns || null);
          setReturnsAmount(resp?.portfolio_returns_amount || null);
        }
      } else {
        setChartOrderData([]);
        setReturnsValue(resp?.portfolio_returns || null);
        setReturnsAmount(resp?.portfolio_returns_amount || null);
      }
    } else {
      setChartOrderData([]);
      setReturnsValue(null);
      setReturnsAmount(null);
    }
  };

  const getOrdersDataMystock = async () => {
    setListLoading(true);
    setCardLoad(true);
    const endpoint = `portfolio/myStock/0?limit=4&offset=${currentPage}`;
    const res = await portfolio.getOrdersDataMystock(endpoint);
    if (res) {
      if (res.keyword == 'success') {
        setListLoading(true);
        if (res?.data?.symbol?.length == 0) {
          setLoadMoreData(false);
        }
        await setListOrderData([...listOrderData, ...res.data]);
        setListLoading(false);
        setCurrentPage(currentPage + 1);
        setCardLoad(true);
      } else if (res.keyword == 'failed') {
        setCardLoad(false);
        setListLoading(false);
      }
    } else {
      setCardLoad(false);
      setListOrderData([]);
    }
    setListLoading(false);
  };

  const fetchLineChartDataMyStock = () => {
    switch (chartRange) {
      case '1d':
        setLineChartDataMyStock([]);
        setChartLoading(true);
        getTotalMyStocksDayChart();
      default:
        break;
      case '1w':
        setChartLoading(true);
        setLineChartDataMyStock([]);

        getTotalMyStocksWeekChart();

        break;

      case '1m':
      case '3m':
      case '6m':
      case '1y':
      case 'max':
        setLineChartDataMyStock([]);
        setChartLoading(true);
        getTotalMyStocksMonthChart();

        break;
    }
  };

  const getTotalMyStocksDayChart = async () => {
    const res = await portfolio.getTotalMyStocksDayChart();
    if (res) {
      if (res.keyword == 'success') {
        if (res?.data) {
          const lineData = [];
          setLineChartDataMyStock(res?.data);
          setChartLoading(false);
        }
      } else {
        setLineChartDataMyStock([]);
        setChartLoading(false);
      }
    } else {
      setLineChartDataMyStock([]);
      setChartLoading(false);
    }
  };

  const getTotalMyStocksWeekChart = async () => {
    const res = await portfolio.getTotalMyStocksWeekChart();
    if (res) {
      if (res.keyword == 'success') {
        if (res?.data) {
          setLineChartDataMyStock(res?.data);
          setChartLoading(false);
        }
      } else {
        setLineChartDataMyStock([]);
        setChartLoading(false);
      }
    } else {
      setLineChartDataMyStock([]);
      setChartLoading(false);
    }
  };

  const getTotalMyStocksMonthChart = async () => {
    const res = await portfolio.getTotalMyStocksMonthChart(chartRange);
    if (res) {
      if (res.keyword == 'success') {
        if (res?.data) {
          setLineChartDataMyStock(res?.data);
          setChartLoading(false);
          // const lineData = [];
          // res.data?.map(item => {
          //   const valueData = {};
          //   (valueData['timestamp'] = item?.timestamp),
          //     (valueData['value'] = Number(item?.value)),
          //     lineData.push(valueData);
          //     setLineChartDataMyStock(lineData);
          //   setChartLoading(false);
          // });
        }
      } else {
        setLineChartDataMyStock([]);
        setChartLoading(false);
      }
    } else {
      setLineChartDataMyStock([]);
      setChartLoading(false);
    }
  };

  return (
    <View style={styles.container}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-evenly',
          alignItems: 'center',
          backgroundColor: '#2e2e2e52',
          padding: 8,
        }}>
        {/* <Date
          fromDate={fromDate}
          toDate={toDate}
          handleDatePicker={handleDatePicker}
        /> */}
        <View
          style={{
            flex: 0.4,
            backgroundColor: theme.primaryColor,
            paddingHorizontal: 15,
            paddingVertical: 8,
            borderColor: 'transparent',
            borderRadius: 4,
            alignItems: 'center',
          }}>
          <TouchableOpacity
            onPress={() => {
              setDatePicker(true);
            }}>
            <View style={{flexDirection: 'row'}}>
              <Ionicons name="calendar-outline" size={18} color={theme.white} />
              <Text style={{color: 'white', textAlign: 'center'}}>
                {Boolean(date) ? date : 'MM-DD-YYYY'}
              </Text>
            </View>
          </TouchableOpacity>
        </View>

        {datePicker && (
          <DateTimePicker
            value={new Date()}
            display={Platform.OS === 'android' ? 'default' : 'spinner'}
            onChange={onDateSelected}
          />
        )}
        <View
          style={{
            flex: 0.4,
            backgroundColor: theme.primaryColor,
            paddingHorizontal: 15,
            paddingVertical: 8,
            borderColor: 'transparent',
            borderRadius: 4,
            borderRadius: 4,
            alignItems: 'center',
          }}>
          <TouchableOpacity
            onPress={() => {
              setDoDatePicker(true);
            }}
            //style={Styles.dateContainer}
          >
            <View style={{flexDirection: 'row'}}>
              <Ionicons name="calendar-outline" size={18} color={theme.white} />
              <Text style={{color: 'white', textAlign: 'center'}}>
                {Boolean(toDate) ? toDate : 'MM-DD-YYYY'}
              </Text>
            </View>
          </TouchableOpacity>
        </View>
        {toDatePicker && (
          <DateTimePicker
            value={new Date()}
            display={Platform.OS === 'android' ? 'default' : 'spinner'}
            onChange={onToDateSelected}
          />
        )}

        <View
          style={{
            flex: 0.1,
            justifyContent: 'center',
            alignItems: 'center',
            borderColor: 'transparent',
          }}>
          {filterStatus ? (
            <TouchableOpacity
              onPress={() => {
                setFilterStatus(!filterStatus);
                // handleDatePicker(moment().format('MM-DD-YYYY'), 'from');
                // handleDatePicker(moment().format('MM-DD-YYYY'), 'to');
                setApiCall(false);
                getOrdersChartMystock();
              }}>
              <Ionicons name="close" size={18} color={theme.white} />
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              onPress={() => {
                setFilterStatus(!filterStatus);
                setApiCall(true), getOrdersChartMystockTime();
              }}>
              <Ionicons name="search-outline" size={18} color={theme.white} />
            </TouchableOpacity>
          )}
        </View>
      </View>

      <ScrollView
        keyboardShouldPersistTaps="handled"
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}>
        <View style={{paddingHorizontal: 8, paddingVertical: 8}}>
          {!Boolean(lineChartDataMyStock) && Boolean(chartLoading) ? (
            <PortfolioChart loading={chartLoading} />
          ) : (
            <PortfolioChart
              loading={chartLoading}
              from={'Stock'}
              chartRange={chartRange}
              lineChartDataMyStock={lineChartDataMyStock}
              returnsValue={returnsValue}
              returnsAmount={returnsAmount}
              orderData={chartOrderData}
              onRangeChange={range => {
                setChartRange(range);
                // setLineChartData(null);
              }}
            />
          )}
        </View>

        <View style={{height: 420}}>
          {!Boolean(listOrderData) ? (
            <CompanyList loading={listLoading} />
          ) : (
            <CompanyList loading={listLoading} orderList={listOrderData} />
          )}
        </View>

        <View style={{paddingVertical: 12}} />
      </ScrollView>
    </View>
  );
};

export default MystockTab;
