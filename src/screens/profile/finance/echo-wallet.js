import React from 'react';
import {ScrollView, Text, View} from 'react-native';
import styles from './styles';

const EchoWallet = () => {
  return (
    <>
      <View style={styles.walletContainer}>
        <View style={styles.walletLabelContainer}>
          <Text style={styles.walletLabelTxt}>Echo Wallet Balance</Text>
        </View>

        <View style={styles.walletValueContainer}>
          <Text style={styles.walletValueTxt}>$ 5035.00</Text>
        </View>
      </View>

      <ScrollView
        keyboardShouldPersistTaps={'handled'}
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
        style={[styles.container, {marginVertical: 8}]}>
        <View style={styles.mainsec}>
          <View style={styles.lineContainer}>
            <View style={styles.lineLabelContainer}>
              <Text style={styles.finalLabelTxt}>Echo Wallet</Text>
            </View>

            <View style={styles.lineValueContainer}>
              <Text style={[styles.finalValueTxt, {color: 'green'}]}>
                $ 5035.00
              </Text>
            </View>
          </View>

          <View style={{marginVertical: 8}}>
            <View style={styles.lineContainer}>
              <View style={styles.lineLabelContainer}>
                <Text style={styles.lineLabelTxt}>Echoing</Text>
              </View>

              <View style={styles.lineValueContainer}>
                <Text style={styles.lineValueRedTxt}>$ 505.00</Text>
              </View>
            </View>

            <View style={[styles.lineContainer, {marginTop: 8}]}>
              <View style={styles.lineLabelContainer}>
                <Text style={styles.lineLabelTxt}>Echors</Text>
              </View>

              <View style={styles.lineValueContainer}>
                <Text style={[styles.lineValueTxt, {color: 'green'}]}>
                  $ 505.00
                </Text>
              </View>
            </View>

            <View style={[styles.lineContainer, {marginTop: 8}]}>
              <View style={styles.lineLabelContainer}>
                <Text style={styles.lineLabelTxt}>Your Paid Channels(3)</Text>
              </View>

              <View style={styles.lineValueContainer}>
                <Text style={[styles.lineValueTxt, {color: 'green'}]}>
                  $ 15035.00
                </Text>
              </View>
            </View>

            <View style={[styles.lineContainer, {marginTop: 8}]}>
              <View style={styles.lineLabelContainer}>
                <Text style={styles.lineLabelTxt}>
                  Your Membership Channels(5)
                </Text>
              </View>

              <View style={styles.lineValueContainer}>
                <Text style={styles.lineValueRedTxt}>$ 1505.00</Text>
              </View>
            </View>

            <View style={[styles.lineContainer, {marginTop: 16}]}>
              <View style={styles.lineLabelContainer}>
                <Text style={styles.finalLabelTxt}>Subscriptions(fees)</Text>
              </View>

              <View style={styles.lineValueContainer}>
                <Text style={styles.finalValueTxt}>$ 0.00</Text>
              </View>
            </View>
          </View>
        </View>

        <View style={{paddingVertical: 16}} />
      </ScrollView>
    </>
  );
};

export default EchoWallet;
