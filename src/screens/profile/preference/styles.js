import {Platform, StyleSheet} from 'react-native';
import theme from '../../../utils/theme';

const styles = StyleSheet.create({
  container: {flex: 1, backgroundColor: theme.themeColor},
  subsec: {
    marginVertical: 10,
  },
  mainsec: {
    backgroundColor: theme.primaryColor,
    borderRadius: 8,
    paddingHorizontal: 16,
    paddingVertical: 16,
  },
  headingTxt: {
    fontSize: Platform.OS == 'ios' ? 16 : 18,
    color: theme.white,
    fontWeight: 'bold',
  },
  labelTxt: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    color: theme.white,
    fontWeight: '400',
  },
  switchContainer: {flexDirection: 'row', justifyContent: 'space-between'},
  switchLabelContainer: {flex: 0.8, justifyContent: 'center'},
  switchToggleContainer: {
    flex: 0.2,
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  paraTxt: {
    fontSize: Platform.OS == 'ios' ? 12 : 14,
    color: theme.white,
    fontWeight: '400',
    lineHeight: 18,
    textAlign: 'justify',
  },
  textinput: {
    width: 48,
    fontSize: Platform.OS == 'ios' ? 12 : 14,
    color: theme.secondryColor,
    fontWeight: 'bold',
    textAlign: 'center',
    borderBottomColor: theme.black,
    borderBottomWidth: 1,
    paddingHorizontal: 4,
    paddingVertical: 4,
  },
  btnview: {
    alignSelf: 'center',
    backgroundColor: theme.secondryColor,
    borderRadius: 4,
    paddingHorizontal: 12,
    paddingVertical: 6,
  },
  btntxt: {
    fontSize: Platform.OS == 'ios' ? 12 : 14,
    color: theme.white,
    fontWeight: '400',
    textAlign: 'center',
  },
  submitview: {
    width: '50%',
    alignSelf: 'center',
    backgroundColor: theme.secondryColor,
    borderRadius: 4,
    marginHorizontal: 16,
    marginVertical: 16,
    paddingHorizontal: 16,
    paddingVertical: 8,
  },
});

export default styles;
