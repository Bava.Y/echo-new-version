import {Platform, StyleSheet} from 'react-native';
import theme from '../../utils/theme';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.themeColor,
  },
  viewmore: {
    fontSize: Platform.OS == 'ios' ? 12 : 14,
    color: theme.secondryColor,
    fontWeight: '600',
    textAlign: 'right',
  },
  applogo: {
    width: 52,
    height: 52,
    borderRadius: 52 / 2,
  },
  userdetails: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: theme.secondryColor,
    paddingHorizontal: 16,
    paddingVertical: 8,
  },
  actioncolumn: {
    flex: 0.1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 4,
  },
  cameraicon: {
    position: 'absolute',
    bottom: 0,
    right: 0,
  },
  bioicon: {
    width: '10%',
  },
  username: {
    fontSize: Platform.OS == 'ios' ? 16 : 18,
    color: theme.white,
    fontWeight: 'bold',
  },
  user: {
    flex: 0.9,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  title: {
    fontSize: Platform.OS == 'ios' ? 16 : 18,
    color: theme.white,
    fontWeight: 'bold',
    marginBottom: 8,
  },
  labeltxt: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    color: theme.white,
    fontWeight: '600',
  },
  fieldlabel: {
    color: theme.white,
    fontSize: 16,
  },
  bio: {
    backgroundColor: theme.primaryColor,
    borderRadius: 8,
    marginHorizontal: 16,
    marginVertical: 16,
    paddingHorizontal: 16,
    paddingVertical: 8,
  },
  biodata: {
    fontSize: Platform.OS == 'ios' ? 12 : 14,
    color: theme.white,
    fontWeight: '400',
    textAlign: 'justify',
    lineHeight: 20,
    marginTop: 4,
  },
  email: {
    color: 'white',
  },

  saveButton: {
    width: '62.5%',
    alignSelf: 'center',
    backgroundColor: theme.secondryColor,
    borderRadius: 8,
    marginVertical: 16,
    paddingHorizontal: 16,
    paddingVertical: 8,
  },
  saveButton2: {
    paddingHorizontal: 50,
    paddingVertical: 10,
    marginTop: 15,
    marginBottom: 15,
  },
  btntext: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    color: '#fff',
    textAlign: 'center',
  },
  border: {
    borderWidth: 1,
    borderColor: 'white',
    marginVertical: 10,
    marginHorizontal: 5,
  },
  border2: {
    borderWidth: 1,
    borderColor: 'grey',
    marginVertical: 10,
    marginHorizontal: 5,
  },
  errorMsg: {
    fontSize: Platform.OS == 'ios' ? 12 : 14,
    color: '#ff0000a1',
    textAlign: 'left',
    marginHorizontal: 12,
    marginVertical: 8,
  },

  tabhead: {
    flexDirection: 'row',
    backgroundColor: '#00000038',
    //opacity: 0.9,
    margin: 10,
  },
  tabheading: {
    textAlign: 'center',
    alignSelf: 'center',
    color: 'white',
    width: '33.3%',
    fontWeight: 'bold',
    paddingHorizontal: 10,
    paddingVertical: 5,
  },
  activetabheading: {
    textAlign: 'center',
    alignSelf: 'center',
    width: '33.3%',
    backgroundColor: 'white',
    fontWeight: 'bold',
    borderRadius: 10,
    color: theme.secondryColor,
    backgroundColor: theme.white,
    paddingHorizontal: 10,
    paddingVertical: 5,
  },
  // Welcome Popup
  welcomecontainer: {
    backgroundColor: theme.primaryColor,
    borderRadius: 8,
    marginHorizontal: 8,
    marginVertical: 8,
    paddingHorizontal: 16,
    paddingVertical: 16,
  },
  welcometxt: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    color: theme.white,
    fontWeight: '800',
    textAlign: 'center',
  },
  startbtn: {
    alignSelf: 'center',
    backgroundColor: theme.secondryColor,
    borderRadius: 4,
    paddingHorizontal: 16,
    paddingVertical: 8,
  },
  steptext: {
    fontSize: Platform.OS == 'ios' ? 12 : 14,
    color: theme.white,
    fontWeight: '600',
    textAlign: 'center',
    marginVertical: 16,
  },
  starttxt: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    color: theme.white,
    fontWeight: '600',
  },

  //Investment Philosophy
  investmentsec: {
    flexDirection: 'column',
    borderBottomWidth: 1,
    borderBottomColor: theme.secondryColor,
    paddingBottom: 7,
    marginTop: 15,
  },
  singlefield: {
    flexDirection: 'row',
  },
  singlefieldvalue: {
    width: '95%',
    color: theme.secondryColor,
  },

  //Social Connection
  socialtitle: {
    backgroundColor: theme.primaryColor,
    paddingHorizontal: 16,
    paddingVertical: 8,
  },
  socialtxt: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    color: theme.white,
    fontWeight: '800',
  },
  socialbtntab: {
    flexDirection: 'row',
    backgroundColor: theme.primaryColor,
    opacity: 0.8,
    margin: 30,
  },
  socialbtn: {
    alignSelf: 'center',
    width: '50%',
    paddingHorizontal: 10,
    paddingVertical: 5,
  },
  socialtext: {
    fontWeight: 'bold',
    color: theme.white,
    textAlign: 'center',
  },
  activesocialbtn: {
    alignSelf: 'center',
    width: '50%',
    backgroundColor: theme.secondryColor,
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderRadius: 7,
  },
  activesocialtext: {
    fontWeight: 'bold',
    color: theme.white,
    textAlign: 'center',
  },
  socialmedia: {
    marginHorizontal: 15,
  },
  socialmedialist: {
    backgroundColor: theme.primaryColor,
    flexDirection: 'row',
    padding: 15,
    marginBottom: 10,
    borderRadius: 7,
  },
  activesocialmedialist: {
    backgroundColor: theme.primaryColor,

    padding: 15,
    marginBottom: 10,
    borderRadius: 7,
  },
  socialicon: {
    width: 30,
    height: 30,
    borderRadius: 50,
    marginTop: 4,
    alignSelf: 'center',
  },
  bgsocialicon: {
    backgroundColor: theme.themeColor,
    padding: 6,
    borderRadius: 50,
    width: 50,
    height: 50,
  },
  socialmediatitle: {
    color: theme.white,
    fontSize: 16,
    paddingLeft: 15,
    paddingTop: 15,
  },
  socialstatus: {
    color: theme.secondryColor,
    fontSize: 14,
    textAlign: 'center',
  },
  connectsocialstatus: {
    color: '#24ac3c',
    fontSize: 14,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  notconnect: {
    position: 'relative',
    bottom: 10,
    backgroundColor: '#274f52',
    paddingHorizontal: 10,
    paddingVertical: 5,
    alignSelf: 'flex-end',
    width: '30%',
    borderRadius: 6,
    borderWidth: 1,
    borderColor: theme.secondryColor,
  },
  connect: {
    position: 'relative',
    bottom: 10,
    backgroundColor: '#345039',
    paddingHorizontal: 10,
    paddingVertical: 5,
    alignSelf: 'flex-end',
    width: '30%',
    borderRadius: 6,
    borderWidth: 1,
    borderColor: '#24ac3c',
  },
  followersection: {
    marginHorizontal: 15,
    marginTop: 15,
    flexDirection: 'row',
  },
  followname: {
    fontSize: 16,
    color: theme.white,
  },
  followers: {
    width: '33.3%',
    alignSelf: 'center',
  },
  borderRight: {
    borderRightWidth: 1,
    borderRightColor: 'white',
    marginRight: 20,
  },
  count: {
    fontSize: 18,
    color: theme.secondryColor,
    fontWeight: 'bold',
  },

  // Logout
  logout: {
    backgroundColor: theme.primaryColor,
    borderRadius: 4,
    marginHorizontal: 16,
    marginBottom: 32,
    paddingHorizontal: 16,
    paddingVertical: 12,
  },

  //Select field
  selectfield: {marginVertical: 8},
  selectinput: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    color: 'white',
    borderColor: 'white',
    borderWidth: 1,
    borderRadius: 24,
    paddingHorizontal: 12,
    paddingVertical: 10,
  },
  selectfieldvalue: {
    borderColor: 'white',
    borderWidth: 1,
    borderRadius: 50,
    paddingHorizontal: Platform.OS == 'ios' ? 0 : 4,
  },
  dropdownWrapper: {
    width: '100%',
    height: Platform.OS == 'ios' ? 36 : 48,
    borderColor: 'white',
    borderWidth: 1,
    backgroundColor: 'transparent',
    borderRadius: 50,
    marginVertical: 8,
  },

  listBroker:{
    flex:0.4,
    color:'white',
    height: Platform.OS == 'ios' ? 36 : 48,
    borderColor: 'white',
    borderWidth: 1,
    backgroundColor: theme.secondryColor,
    borderRadius: 10,
    marginVertical: 8,
    elevation:50
  },

  listBrokerText:{
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    fontWeight: '500',
    color: 'white',
    textAlign: 'left',
    paddingHorizontal: Platform.OS == 'ios' ? 0 : 4,
  },
  listBrokerSelectedItemText:{
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    fontWeight: '400',
    color: theme.secondryColor,
    textAlign: 'left',
  },

  brokerItemText:{
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    fontWeight: '400',
    color: 'black',
    textAlign: 'left',
  },

  dropdownItemContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  dropdownLabelContainer: {
    flex: 0.9,
    justifyContent: 'center',
  },
  dropdownLabelText: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    fontWeight: '400',
    color: 'white',
    textAlign: 'left',
    paddingHorizontal: Platform.OS == 'ios' ? 0 : 4,
  },
  dropdownIconContainer: {
    flex: 0.1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  dropdownItemText: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    fontWeight: '400',
    color: 'black',
    textAlign: 'left',
  },
  dropdownSelectedItemText: {
    fontSize: Platform.OS == 'ios' ? 14 : 16,
    fontWeight: '400',
    color: theme.secondryColor,
    textAlign: 'left',
  },
  popuptitle: {
    fontSize: Platform.OS == 'ios' ? 16 : 18,
    color: theme.secondryColor,
    fontWeight: 'bold',
    marginBottom: 8,
  },
  modalView: {
    width: '87.5%',
    backgroundColor: theme.themeColor,
    borderRadius: 8,
    margin: 16,
    padding: 16,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 4,
  },
});
export default styles;
