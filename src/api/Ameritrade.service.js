import QueryString from 'query-string';
import axiosInstance from '../utils/AmeritradeApiCall';
import {
  AMERITRADE_CLIENT_ID,
  AMERITRADE_REDIRECT_URI,
} from '../utils/constants';

const generateAuthorizationCode = async (authorizationCode) => {
  const requestData = {
    grant_type: 'authorization_code',
    access_type: 'offline',
    code: authorizationCode,
    client_id: AMERITRADE_CLIENT_ID,
    redirect_uri: AMERITRADE_REDIRECT_URI,
  };

  return await axiosInstance
    .post('oauth2/token', QueryString.stringify(requestData), {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
    })
    .then((res) => {
      return res.data;
    })
    .catch(() => {
      return false;
    });
};

const generateAccessToken = async (refreshToken) => {
  const requestData = {
    grant_type: 'refresh_token',
    refresh_token: refreshToken,
    client_id: AMERITRADE_CLIENT_ID,
  };

  return await axiosInstance
    .post('oauth2/token', QueryString.stringify(requestData), {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
    })
    .then((res) => {
      return res.data;
    })
    .catch(() => {
      return false;
    });
};

const getAccounts = async (token) => {
  return await axiosInstance
    .get('accounts', {
      headers: {
        Authorization: 'Bearer ' + token,
      },
    })
    .then((res) => {
      return res.data;
    })
    .catch(() => {
      return false;
    });
};

export const ameritrade = {
  generateAuthorizationCode,
  generateAccessToken,
  getAccounts,
};
