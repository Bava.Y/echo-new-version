import React from 'react';
import {ActivityIndicator} from 'react-native';
import theme from '../../utils/theme';

const Loading = ({color}) => (
  <ActivityIndicator
    size="large"
    color={color ? color : theme.primaryColor}
    style={{
      position: 'absolute',
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      alignItems: 'center',
      justifyContent: 'center',
      zIndex: 999,
    }}
  />
);

export default Loading;
