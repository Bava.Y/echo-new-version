import React, {useContext, useEffect, useState} from 'react';
import {
  ActivityIndicator,
  Alert,
  Dimensions,
  Image,
  Keyboard,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import Modal from 'react-native-modal';
import Swiper from 'react-native-swiper';
import Icon from 'react-native-vector-icons/FontAwesome';
import Ionicon from 'react-native-vector-icons/Ionicons';
import AmeritradeAlert from '../../components/AmeritradeAlert';
import BottomNav from '../../components/BottomNav';
import HeaderNav from '../../components/HeaderNav';
import Loading from '../../components/Loading';
import {UserDataContext} from '../../context/UserDataContext';
import axiosInstance from '../../utils/Apicall';
import theme from '../../utils/theme';
import {PROFILE} from './../../assets/images/index';
import AreaChart from './areachart';
import BarChart from './barchart';
import Percentage from './percentage';
import styles from './style';
import SelectDropdown from 'react-native-select-dropdown';
import FontAwesomeIcons from 'react-native-vector-icons/FontAwesome5';

const DiscoverPeopleProfile = ({route}) => {
  // Props Variables
  const {id, loginStatus} = route.params;
  const demo = ['Alpaca', 'TDAmeritrade'];
  // DiscoverPeopleProfile Variables
  const [profile, setProfile] = useState({});
  const [echoing, setEchoing] = useState([]);
  const [alphacaAllocation, setAlphacaAllocation] = useState(null);
  const [ameritradeAllocation, setAmeritradeAllocation] = useState(null);
  const [chosenAlphaca, setChosenAlphaca] = useState();
  const [chosenTDAmeritrade, setChosenTDAmeritrade] = useState();
  const [alloationStatus, setAlloationStatus] = useState(false);
  const [echoingOptions, setEchoingOptions] = useState(null);
  const [echoingOptionsModal, setEchoingOptionsModal] = useState(false);
  const [selectedEchoingOption, setSelectedEchoingOption] = useState(null);
  const [portfolioconnection, setportfolioconnection] = useState();

  // Context Variables
  const {
    alpacaConfigStatus,
    ameritradeAlertStatus,
    setAmeritradeAlertStatus,
    tdConfigStatus,
    token,
  } = useContext(UserDataContext);

  // Error Variables
  const [alphacaError, setAlphacaError] = useState(false);
  const [chosenAlphacaError, setChosenAlphacaError] = useState(false);
  const [ameritradeError, setAmeritradeError] = useState(false);
  const [chosenAmeritradeError, setChosenAmeritradeError] = useState(false);
  const [textInputError, setTextInputError] = useState(false);
  const [chosenError, setChosenError] = useState(false);
  const [selectedEchoingOptionError, setSelectedEchoingOptionError] =
    useState(false);
  const [chooseconnection, setchooseconnection] = useState(false);

  // Other Variables
  const [loading, setLoading] = useState(false);
  const [ELoading, setELoading] = useState(false);
  const [echoLoading, setEchoLoading] = useState(false);
  const [isModalVisible, setModalVisible] = useState(false);
  const [echoingAlertStatus, setEchoingAlertStatus] = useState(false);
  const [returnsstate, setreturnsstate] = useState();

  const [brokerconnectionstate, setbrokerconnectionstate] = useState();

  const [broker, setBroker] = useState('Select');

  const deviceHeight = Dimensions.get('window').height;

  useEffect(() => {
    let ac = new AbortController();

    fetchProfile();

    fetchEchoLists();
    fetchreturns();

    return ac.abort();
  }, [id]);

  const fetchProfile = async () => {
    setLoading(true);

    let path = 'getProfile/' + id;

    let response = await axiosInstance.get(path, {
      headers: {
        Authorization: token,
      },
    });

    if (response && response.data && response.data.keyword === 'success') {
      await setProfile(response.data.data);

      handleEchoingOptions(response.data.data);

      await setAlloationStatus(response.data.data.echo_percentage_allocation);

      await setLoading(false);
    }
  };

  const fetchreturns = async () => {
    setLoading(true);

    let path = 'portfolioDetailsList/' + id;

    let response = await axiosInstance.get(path, {
      headers: {
        Authorization: token,
      },
    });

    if (response && response.data && response.data.keyword === 'success') {
      const brokerConnection = response.data.retunrs_data.map(data => {
        return data.broker_connection;
      });

      const returns = response.data.retunrs_data.map(data => {
        return data.returns;
      });
      setbrokerconnectionstate(brokerConnection);
      setreturnsstate(returns);
    }
  };

  const handleEchoingOptions = responseData => {
    let helperArray = [];

    responseData.echo_movingfwd == 1 &&
      helperArray.push({label: ' Future Echoing', value: '0'});

    responseData.snapshot == 1 &&
      helperArray.push({label: 'Snapshot', value: '1'});

    responseData.hybrid == 1 && helperArray.push({label: 'Hybrid', value: '2'});

    if (helperArray.length == 1) {
      setSelectedEchoingOptionError(false);

      setSelectedEchoingOption(helperArray[0]);
    } else {
      setSelectedEchoingOptionError(false);

      setSelectedEchoingOption(null);
    }

    setEchoingOptions(helperArray);
  };

  const fetchEchoLists = async () => {
    setLoading(true);
    setEchoLoading(true);

    let path = 'echoingList';

    let response = await axiosInstance.get(path, {
      headers: {
        Authorization: token,
      },
    });

    if (response && response.data && response.data.keyword === 'success') {
      let echoingIds = [];

      response.data.echoing_data.map(item => echoingIds.push(item.followed_id));

      await setEchoing(echoingIds);

      await setEchoLoading(false);

      await setLoading(false);
    }
  };

  function ProfileCard({data}) {
    return (
      <>
        <View style={styles.userdata}>
          <View
            style={{
              width: '75%',
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <View
              style={{
                width: '15%',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Image
                resizeMode={'cover'}
                source={{uri: data.photo_url}}
                style={{width: 44, height: 44, borderRadius: 44 / 2}}
              />

              <View
                style={
                  loginStatus && loginStatus == 1
                    ? styles.online
                    : styles.offline
                }></View>
            </View>

            <View
              style={{
                width: '85%',
                justifyContent: 'center',
                paddingHorizontal: 8,
              }}>
              <Text style={styles.username}>
                {data.first_name + ' ' + data.last_name}
              </Text>

              <Text style={styles.userposition}>
                {data?.designation != 'null' ? data?.designation : ' '}
              </Text>
            </View>
          </View>

          <View
            style={{
              width: '25%',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View
              style={{flexDirection: 'row', justifyContent: 'space-between'}}>
              <View style={styles.upward}>
                <Icon
                  name="arrow-up"
                  color="white"
                  size={10}
                  style={{alignSelf: 'center'}}
                />
              </View>

              <Text style={styles.upwardtxt}>0%</Text>
            </View>

            <Text style={styles.todaytxt}>Today</Text>
          </View>
        </View>

        <Swiper
          style={{height: deviceHeight * 0.53125}}
          showsButtons={false}
          dotColor="white"
          loop={false}
          activeDotColor={theme.secondryColor}>
          <View style={styles.bio}>
            <ScrollView
              keyboardShouldPersistTaps={'handled'}
              nestedScrollEnabled={true}
              showsHorizontalScrollIndicator={false}
              showsVerticalScrollIndicator={false}
              style={{height: '82.625%'}}>
              <View style={{marginVertical: 8}}>
                <Text style={styles.biotitle}>Bio</Text>

                <Text style={styles.biovalue}>{data.bio || '-'}</Text>
              </View>

              <View style={{marginVertical: 8}}>
                <Text style={styles.investment}>Primary Investment</Text>

                <Text style={styles.biovalue}>
                  {data.primary_investment_names &&
                  data.primary_investment_names.length != 0
                    ? data.primary_investment_names
                    : '-'}
                </Text>
              </View>

              <View style={{marginVertical: 8}}>
                <Text style={styles.investment}>Secondary Investment</Text>

                <Text style={styles.biovalue}>
                  {data?.secondary_investment_name &&
                  data?.secondary_investment_name.length != 0
                    ? data?.secondary_investment_name
                    : '-'}
                </Text>
              </View>

              <View style={{marginVertical: 8}}>
                <Text style={styles.biotitle}>Email</Text>

                <Text style={styles.biovalue}>
                  {data.email_icon == 1 && data.email ? data.email : '-'}
                </Text>
              </View>

              <View style={{marginVertical: 8}}>
                <Text style={[styles.biotitle, {marginTop: 12}]}>Phone</Text>

                <Text style={styles.biovalue}>
                  {data.phone_icon == 1 && data.phone ? data.phone : '-'}
                </Text>
              </View>
            </ScrollView>
          </View>

          <View style={styles.bio}>
            <AreaChart />
          </View>

          <View style={styles.bio}>
            <BarChart />
          </View>

          <View>
            <Percentage />
          </View>
        </Swiper>

        <View style={styles.socialconnection}>
          <Text style={styles.socialtitle}>Social Connection</Text>
        </View>

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-evenly',
            marginHorizontal: 16,
            marginVertical: 20,
          }}>
          <View style={styles.connect}>
            <Text style={styles.connectsocialstatus}>
              Followers {data.follower_count}
            </Text>
          </View>

          <TouchableOpacity
            style={styles.connect}
            onPress={() => {
              const echo = profile.echo_feature == 1 ? true : false;
              const inc = echoing.includes(id);
              echo
                ? inc
                  ? echoing.includes(id) && unecho()
                  : echoingOptions && echoingOptions.length != 0
                  ? toggleModal()
                  : alert('No echoing(s) available!')
                : setEchoingAlertStatus(!echoingAlertStatus);
            }}>
            {echoLoading ? (
              <ActivityIndicator size={12} color={theme.white} />
            ) : (
              <Text style={styles.connectsocialstatus}>
                {echoing.includes(id) ? 'Un Echo' : 'Echo Me'}
              </Text>
            )}
          </TouchableOpacity>

          <View style={styles.connect}>
            <Text style={styles.connectsocialstatus}>
              Echoers {data.echoing_count}
            </Text>
          </View>
        </View>
      </>
    );
  }

  const toggleModal = () => {
    setModalVisible(!isModalVisible);
  };

  const checkbox = () => {
    Keyboard.dismiss();

    setChosenAlphacaError(false);

    setChosenError(false);

    setChosenAlphaca(!chosenAlphaca);
  };

  const checkbox1 = () => {
    Keyboard.dismiss();

    setChosenAmeritradeError(false);

    setChosenError(false);

    setChosenTDAmeritrade(!chosenTDAmeritrade);
  };

  const echoMe = () => {
    Keyboard.dismiss();

    if (checkFormCondition()) {
      if (Boolean(selectedEchoingOption)) {
        echoingOptions && echoingOptions.length == 1
          ? Alert.alert(
              'Confirmation',
              `${profile.first_name}${
                profile.last_name ? ` ${profile.last_name}` : ''
              } has enabled only ${
                selectedEchoingOption.label
              }. Do you want to proceed?`,
              [
                {
                  text: 'Yes',
                  onPress: () => {
                    handleAPI();
                  },
                },
                {
                  text: 'No',
                  onPress: () => {},
                  style: 'cancel',
                },
              ],
            )
          : handleAPI();
      } else {
        setSelectedEchoingOptionError(false);

        setSelectedEchoingOption(null);

        setEchoingOptionsModal(true);
      }
    } else {
      handleErrorValidation();
    }
  };

  const checkFormCondition = () => {
    if (alpacaConfigStatus && tdConfigStatus) {
      if (
        alphacaConditions() &&
        chosenAlphaca &&
        !ameritradeConditions() &&
        !chosenTDAmeritrade
      ) {
        return true;
      } else if (
        ameritradeConditions() &&
        chosenTDAmeritrade &&
        !alphacaConditions() &&
        !chosenAlphaca
      ) {
        return true;
      } else if (
        alphacaConditions() &&
        chosenAlphaca &&
        ameritradeConditions() &&
        chosenTDAmeritrade
      ) {
        return true;
      } else {
        return false;
      }
    } else {
      if (alpacaConfigStatus) {
        if (alphacaConditions() && chosenAlphaca) {
          return true;
        } else {
          return false;
        }
      }

      if (tdConfigStatus) {
        if (ameritradeConditions() && chosenTDAmeritrade) {
          return true;
        } else {
          return false;
        }
      }
    }
  };

  const alphacaConditions = () => {
    if (handleAlphacaConditions()) {
      return true;
    } else {
      return false;
    }
  };

  const handleAlphacaConditions = () => {
    if (alloationStatus) {
      return (
        alphacaAllocation &&
        alphacaAllocation != 0 &&
        alphacaAllocation > 0 &&
        alphacaAllocation < 100
      );
    } else {
      return (
        alphacaAllocation && alphacaAllocation != 0 && alphacaAllocation > 0
      );
    }
  };

  const ameritradeConditions = () => {
    if (handleAmeritradeConditions()) {
      return true;
    } else {
      return false;
    }
  };

  const handleAmeritradeConditions = () => {
    if (alloationStatus) {
      return (
        ameritradeAllocation &&
        ameritradeAllocation != 0 &&
        ameritradeAllocation > 0 &&
        ameritradeAllocation < 100
      );
    } else {
      return (
        ameritradeAllocation &&
        ameritradeAllocation != 0 &&
        ameritradeAllocation > 0
      );
    }
  };

  const handleAPI = async () => {
    setELoading(true);

    const requestData = handleFormRequestData();

    let response = await axiosInstance.post('echoFollower', requestData, {
      headers: {
        Authorization: token,
      },
    });

    if (response && response.data && response.data.keyword === 'success') {
      toggleModal();

      Alert.alert('', response.data.message);

      let echoingIds = echoing;

      if (!echoing.includes(id)) {
        // echoing
        echoingIds.push(id);
      } else {
        // un echoing
        echoingIds = echoingIds.filter(item => item !== id);
      }

      await setEchoing(echoingIds);
    } else if (
      response &&
      response.data &&
      response.data.keyword === 'Failed'
    ) {
      toggleModal();

      if (response.data.hasOwnProperty('message')) {
        if (response.data.message.hasOwnProperty('error')) {
          switch (response.data.message.error) {
            case 'invalid_grant':
              handleAmeritradeAlertStatus();
              break;

            default:
              Alert.alert('', response.data.message.error);
              break;
          }
        } else {
          Alert.alert(
            '',
            typeof response.data.message == 'string'
              ? response.data.message
              : 'Unable to echo person',
          );
        }
      } else {
        Alert.alert('', 'Unable to echo person');
      }
    }

    setELoading(false);

    clearFormStates();
  };

  const handleFormRequestData = () => {
    let requestData = new FormData();
    requestData.append('followed_id', id);
    requestData.append('status', 1);
    requestData.append('echoingType', selectedEchoingOption.value);

    alphacaConditions() && [
      requestData.append('percentage', alphacaAllocation),
      requestData.append('enable_alpaca', chosenAlphaca),
    ];
    ameritradeConditions() && [
      requestData.append('td_percentage', ameritradeAllocation),
      requestData.append('enable_td', chosenTDAmeritrade),
    ];

    return requestData;
  };

  const clearFormStates = () => {
    setAlphacaAllocation(null);
    setAmeritradeAllocation(null);
    setChosenAlphaca('0');
    setChosenTDAmeritrade('0');
  };

  const handleErrorValidation = () => {
    if (alpacaConfigStatus && tdConfigStatus) {
      if (alphacaConditions() || ameritradeConditions()) {
        alphacaConditions() &&
          setChosenAlphacaError(chosenAlphaca ? false : true);

        ameritradeConditions() &&
          setChosenAmeritradeError(chosenTDAmeritrade ? false : true);
      } else {
        setTextInputError(true);
      }

      if (chosenAlphaca || chosenTDAmeritrade) {
        chosenAlphaca && setAlphacaError(!alphacaConditions());

        chosenTDAmeritrade && setAmeritradeError(!ameritradeConditions());
      } else {
        setChosenError(true);
      }
    } else {
      alpacaConfigStatus && [
        setAlphacaError(!alphacaConditions()),

        setChosenAlphacaError(chosenAlphaca ? false : true),
      ];

      tdConfigStatus && [
        setAmeritradeError(!ameritradeConditions()),

        setChosenAmeritradeError(chosenTDAmeritrade ? false : true),
      ];
    }
  };

  const unecho = async () => {
    setLoading(true);

    let body = new FormData();

    body.append('followed_id', id);
    body.append('status', 0);

    let response = await axiosInstance.post('echoFollower', body, {
      headers: {
        Authorization: token,
      },
    });

    if (response && response.data && response.data.keyword === 'success') {
      Alert.alert('', response.data.message);

      let echoingIds = echoing;

      if (!echoing.includes(id)) {
        // echoing
        echoingIds.push(id);
      } else {
        // un echoing
        echoingIds = echoingIds.filter(item => item !== id);
      }

      setEchoing(echoingIds);
    } else if (response.keyword === 'Failed') {
      console.log('>>>><<<<', response.keyword === 'Failed');
      Alert.alert('', response.message);
    }

    setLoading(false);
  };

  const handleAmeritradeAlertStatus = () => {
    setAmeritradeAlertStatus(!ameritradeAlertStatus);
  };

  return (
    <>
      <HeaderNav title="Profile" isBack="true" drop={<listView />} ha="Close" />

      <View style={styles.container}>
        {!Boolean(loading) &&
        Boolean(profile) &&
        Object.keys(profile).length != 0 ? (
          <ScrollView
            keyboardShouldPersistTaps={'handled'}
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}>
            <ProfileCard data={profile} />
          </ScrollView>
        ) : (
          <Loading color={theme.white} />
        )}
      </View>

      <Modal isVisible={isModalVisible}>
        <KeyboardAvoidingView
          behavior={Platform.OS === 'ios' ? 'padding' : null}
          keyboardVerticalOffset={Platform.OS === 'ios' ? 40 : 0}
          style={{backgroundColor: theme.primaryColor, borderRadius: 8}}>
          <View
            style={{
              backgroundColor: theme.secondryColor,
              borderBottomColor: 'grey',
              borderBottomWidth: 1,
              paddingVertical: 8,
              borderTopLeftRadius: 8,
              borderTopRightRadius: 8,
            }}>
            <Text
              style={{
                fontSize: Platform.OS == 'ios' ? 14 : 16,
                color: 'white',
                fontWeight: 'bold',
                textAlign: 'center',
              }}>
              Echo
            </Text>
          </View>

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginHorizontal: 12,
              marginVertical: 12,
            }}>
            <View
              style={{
                flex: 0.15,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Image
                resizeMode={'cover'}
                source={
                  Boolean(profile.photo_url)
                    ? {uri: profile.photo_url}
                    : PROFILE
                }
                style={{width: 40, height: 40, borderRadius: 40 / 2}}
              />
            </View>

            <View
              style={{
                flex: 0.85,
                justifyContent: 'center',
                paddingHorizontal: 8,
              }}>
              <Text style={styles.username}>
                {profile.first_name + ' ' + profile.last_name}
              </Text>

              <Text style={styles.userposition}>
                {profile?.designation != 'null' ? profile?.designation : ' '}
              </Text>
            </View>
          </View>

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              borderBottomColor: 'white',
              borderBottomWidth: 1,
              marginHorizontal: 16,
              marginBottom: 8,
              paddingBottom: 8,
            }}>
            <Text
              style={{
                width: '35%',
                fontSize: Platform.OS == 'ios' ? 12 : 14,
                color: theme.greenBg,
                fontWeight: '600',
                textAlign: 'left',
              }}>
              Accounts
            </Text>

            <Text
              style={{
                width: '10%',
                fontSize: Platform.OS == 'ios' ? 12 : 14,
                color: theme.greenBg,
                fontWeight: '600',
                textAlign: 'center',
              }}>
              -
            </Text>

            <Text
              style={{
                width: '55%',
                fontSize: Platform.OS == 'ios' ? 12 : 14,
                color: theme.greenBg,
                fontWeight: '600',
                textAlign: 'left',
              }}>{`Allowed (${
              alloationStatus ? '%' : '$'
            }) to this user`}</Text>
          </View>

          {alpacaConfigStatus && (
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginHorizontal: 16,
                marginVertical: 8,
              }}>
              <Text
                style={{
                  width: '35%',
                  fontSize: Platform.OS == 'ios' ? 12 : 14,
                  fontWeight: '800',
                  color: 'white',
                  textAlign: 'left',
                }}>
                Alpaca
              </Text>

              <Text
                style={{
                  width: '10%',
                  fontSize: Platform.OS == 'ios' ? 12 : 14,
                  fontWeight: '800',
                  color: theme.greenBg,
                  textAlign: 'center',
                }}>
                -
              </Text>

              <View>
                <View
                  style={{
                    width: '55%',
                    flexDirection: 'row',
                    justifyContent: 'flex-start',
                  }}>
                  <TextInput
                    keyboardType={'decimal-pad'}
                    value={alphacaAllocation}
                    onChangeText={text => {
                      setAlphacaError(text ? false : true);

                      setTextInputError(false);

                      setAlphacaAllocation(text);
                    }}
                    style={{
                      width: '100%',
                      fontSize: Platform.OS == 'ios' ? 12 : 14,
                      color: 'white',
                      fontWeight: '400',
                      borderBottomColor: 'white',
                      borderBottomWidth: 1,
                      paddingBottom: 8,
                    }}
                  />

                  <Text
                    style={{
                      fontSize: Platform.OS == 'ios' ? 14 : 16,
                      color: 'white',
                      fontWeight: '400',
                      paddingHorizontal: 4,
                    }}>{`( ${alloationStatus ? '%' : '$'} )`}</Text>
                </View>

                {!textInputError && alphacaError && (
                  <Text
                    style={{
                      fontSize: Platform.OS == 'ios' ? 12 : 14,
                      color: 'red',
                      fontWeight: '600',
                      marginTop: 8,
                    }}>
                    {`Enter a valid ${
                      alloationStatus ? 'percentage' : 'amount'
                    }`}
                  </Text>
                )}
              </View>
            </View>
          )}

          {tdConfigStatus && (
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginHorizontal: 16,
                marginVertical: 8,
                marginTop: alpacaConfigStatus ? 16 : 8,
              }}>
              <Text
                style={{
                  width: '35%',
                  fontSize: Platform.OS == 'ios' ? 12 : 14,
                  fontWeight: '800',
                  color: 'white',
                  textAlign: 'left',
                }}>
                TD Amiratrade
              </Text>

              <Text
                style={{
                  width: '10%',
                  fontSize: Platform.OS == 'ios' ? 12 : 14,
                  fontWeight: '800',
                  color: theme.greenBg,
                  textAlign: 'center',
                }}>
                -
              </Text>

              <View>
                <View
                  style={{
                    width: '55%',
                    flexDirection: 'row',
                    justifyContent: 'flex-start',
                  }}>
                  <TextInput
                    keyboardType={'decimal-pad'}
                    value={ameritradeAllocation}
                    onChangeText={text => {
                      setAmeritradeError(text ? false : true);

                      setTextInputError(false);

                      setAmeritradeAllocation(text);
                    }}
                    style={{
                      width: '100%',
                      fontSize: Platform.OS == 'ios' ? 12 : 14,
                      color: 'white',
                      fontWeight: '400',
                      borderBottomColor: 'white',
                      borderBottomWidth: 1,
                      paddingBottom: 8,
                    }}
                  />

                  <Text
                    style={{
                      fontSize: Platform.OS == 'ios' ? 14 : 16,
                      color: 'white',
                      fontWeight: '400',
                      paddingHorizontal: 4,
                    }}>{`( ${alloationStatus ? '%' : '$'} )`}</Text>
                </View>

                {!textInputError && ameritradeError && (
                  <Text
                    style={{
                      fontSize: Platform.OS == 'ios' ? 12 : 14,
                      color: 'red',
                      fontWeight: '600',
                      marginTop: 8,
                    }}>
                    {`Enter a valid ${
                      alloationStatus ? 'percentage' : 'amount'
                    }`}
                  </Text>
                )}
              </View>
            </View>
          )}

          {textInputError && (
            <Text
              style={{
                fontSize: Platform.OS == 'ios' ? 12 : 14,
                color: 'red',
                fontWeight: '600',
                marginHorizontal: 16,
                marginTop: 8,
              }}>
              {`Enter any one of the ${
                alloationStatus ? 'percentage' : 'amount'
              }`}
            </Text>
          )}

          <View
            style={{
              borderTopColor: 'white',
              borderTopWidth: 1,
              marginHorizontal: 16,
              marginVertical: 8,
            }}
          />

          <View style={{paddingHorizontal: 16, paddingVertical: 8}}>
            <Text
              style={{
                fontSize: Platform.OS == 'ios' ? 14 : 16,
                color: 'white',
                fontWeight: '800',
                textDecorationLine: 'underline',
              }}>
              Auto Trade:
            </Text>

            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'flex-start',
                marginVertical: 16,
              }}>
              {alpacaConfigStatus && (
                <TouchableOpacity
                  onPress={() => {
                    checkbox();
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'center',
                      marginRight: tdConfigStatus ? 16 : 0,
                    }}>
                    <View
                      style={[
                        {
                          width: 16,
                          height: 16,
                          borderWidth: 1,
                          borderRadius: 2,
                        },
                        chosenAlphaca
                          ? {
                              backgroundColor: theme.greenBg,
                              borderColor: theme.greenBg,
                            }
                          : {borderColor: 'white'},
                      ]}>
                      {chosenAlphaca ? (
                        <Ionicon
                          name="checkmark-outline"
                          size={12}
                          color="white"
                          style={{alignSelf: 'center'}}
                        />
                      ) : (
                        <></>
                      )}
                    </View>

                    <Text
                      style={{
                        fontSize: Platform.OS == 'ios' ? 12 : 14,
                        color: 'white',
                        fontWeight: '600',
                        marginHorizontal: 8,
                      }}>
                      Alpaca
                    </Text>
                  </View>
                </TouchableOpacity>
              )}

              {tdConfigStatus && (
                <TouchableOpacity
                  onPress={() => {
                    checkbox1();
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'center',
                    }}>
                    <View
                      style={[
                        {
                          width: 16,
                          height: 16,
                          borderWidth: 1,
                          borderRadius: 2,
                        },
                        chosenTDAmeritrade
                          ? {
                              backgroundColor: theme.greenBg,
                              borderColor: theme.greenBg,
                            }
                          : {borderColor: 'white'},
                      ]}>
                      {chosenTDAmeritrade ? (
                        <Ionicon
                          name="checkmark-outline"
                          size={12}
                          color="white"
                          style={{alignSelf: 'center'}}
                        />
                      ) : (
                        <></>
                      )}
                    </View>

                    <Text
                      style={{
                        fontSize: Platform.OS == 'ios' ? 12 : 14,
                        color: 'white',
                        fontWeight: '600',
                        marginHorizontal: 8,
                      }}>
                      TD Amiratrade
                    </Text>
                  </View>
                </TouchableOpacity>
              )}
            </View>
            {/* <View>
              <SelectDropdown
                buttonStyle={styles.listBroker}
                buttonTextStyle={styles.listBrokerText}
                data={brokerconnectionstate}
                dropdownStyle={{alignSelf: 'center'}}
                defaultButtonText={broker}
                // defaultValue='Alpaca'
                onSelect={(selectedItem) => {
                  setBroker(selectedItem);
                  switch (selectedItem) {
                    case 'Alpaca':
                      setChosenAlphaca('1');
                      break;
                    case 'TD Ameritrade':
                      setChosenTDAmeritrade('1');

                      break;
                  }
                }}
                renderDropdownIcon={() => (
                  <FontAwesomeIcons
                    name="caret-down"
                    size={16}
                    color={'white'}
                  />
                )}
                rowStyle={{borderBottomWidth: 0}}
                rowTextStyle={styles.brokerItemText}
                selectedRowTextStyle={styles.listBrokerSelectedItemText}
              />
            </View> */}
            {!chosenError && (chosenAlphacaError || chosenAmeritradeError) && (
              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 12 : 14,
                  color: 'red',
                  fontWeight: '600',
                }}>
                Please! Enable auto trade for particular account
              </Text>
            )}

            {chosenError && (
              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 12 : 14,
                  color: 'red',
                  fontWeight: '600',
                  marginTop: 8,
                }}>
                Please! Enable any one of the auto trade
              </Text>
            )}

            {alpacaConfigStatus && tdConfigStatus && (
              <View style={{marginVertical: 8}}>
                <Text
                  style={{
                    color: 'white',
                    fontWeight: '600',
                    textAlign: 'left',
                  }}>
                  <Text style={{fontSize: Platform.OS == 'ios' ? 14 : 16}}>
                    Note :{' '}
                  </Text>

                  <Text style={{fontSize: Platform.OS == 'ios' ? 12 : 14}}>
                    Should select any one of the account
                  </Text>
                </Text>
              </View>
            )}
          </View>

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
              marginVertical: 8,
              paddingBottom: 16,
            }}>
            <TouchableOpacity
              disabled={ELoading}
              style={[
                styles.btnview1,
                ELoading && {paddingHorizontal: 36, paddingVertical: 8},
              ]}
              onPress={() => {
                echoMe();
              }}>
              {ELoading ? (
                <ActivityIndicator size={12} color="white" />
              ) : (
                <Text style={styles.btntext}>Echo Me</Text>
              )}
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                clearFormStates();

                toggleModal();
              }}>
              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 14 : 16,
                  color: theme.secondryColor,
                  fontWeight: 'bold',
                  textAlign: 'center',
                  marginHorizontal: 16,
                }}>
                Cancel
              </Text>
            </TouchableOpacity>
          </View>
        </KeyboardAvoidingView>

        <Modal
          isVisible={echoingAlertStatus}
          onBackdropPress={() => {
            setEchoingAlertStatus(!echoingAlertStatus);
          }}>
          <View
            style={{
              backgroundColor: theme.primaryColor,
              borderRadius: 4,
            }}>
            <View
              style={{
                backgroundColor: theme.secondryColor,
                borderTopLeftRadius: 4,
                borderTopRightRadius: 4,
                paddingVertical: 8,
              }}>
              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 14 : 16,
                  color: '#FF0000',
                  fontWeight: 'bold',
                  textAlign: 'center',
                }}>
                ALERT
              </Text>

              <View
                style={{
                  position: 'absolute',
                  width: 28,
                  height: 28,
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: theme.primaryColor,
                  borderRadius: 28 / 2,
                  top: -12,
                  right: -12,
                }}>
                <Icon
                  name="close"
                  size={16}
                  color={theme.white}
                  onPress={() => {
                    setEchoingAlertStatus(!echoingAlertStatus);
                  }}
                />
              </View>
            </View>

            <View style={{marginVertical: 12, marginHorizontal: 12}}>
              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 12 : 14,
                  color: theme.white,
                  fontWeight: '600',
                  textAlign: 'center',
                  marginVertical: 8,
                }}>
                <Text
                  style={{
                    fontSize: Platform.OS == 'ios' ? 12 : 14,
                    fontWeight: '900',
                  }}>
                  {profile.first_name + ' ' + profile.last_name}
                </Text>{' '}
                has disabled the echoing feature
              </Text>

              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 10 : 12,
                  color: theme.textColor,
                  fontWeight: '400',
                  textAlign: 'left',
                  marginVertical: 8,
                }}>
                NOTE: You can echo{' '}
                {profile.first_name + ' ' + profile.last_name} only if the
                echoing feature is enabled.
              </Text>
            </View>
          </View>
        </Modal>

        <Modal isVisible={echoingOptionsModal}>
          <View style={{backgroundColor: theme.primaryColor, borderRadius: 8}}>
            <View
              style={{
                backgroundColor: theme.secondryColor,
                borderBottomColor: 'grey',
                borderBottomWidth: 1,
                borderTopLeftRadius: 8,
                borderTopRightRadius: 8,
                paddingVertical: 8,
              }}>
              <Text
                style={{
                  fontSize: Platform.OS == 'ios' ? 14 : 16,
                  color: 'white',
                  fontWeight: 'bold',
                  textAlign: 'center',
                }}>
                Echoing(s)
              </Text>
            </View>

            <View style={{marginHorizontal: 12, marginVertical: 12}}>
              {echoingOptions &&
                echoingOptions.map((lol, index) => (
                  <TouchableOpacity
                    key={index}
                    onPress={() => {
                      setSelectedEchoingOptionError(false);

                      setSelectedEchoingOption(
                        selectedEchoingOption != lol ? lol : null,
                      );
                    }}
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      marginVertical: 8,
                    }}>
                    <View
                      style={{
                        flex: 0.0875,
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      <View
                        style={{
                          width: 20,
                          height: 20,
                          justifyContent: 'center',
                          alignItems: 'center',
                          borderColor: theme.secondryColor,
                          borderWidth: 1,
                          borderRadius: 20 / 2,
                        }}>
                        {selectedEchoingOption == lol && (
                          <View
                            style={{
                              width: 10,
                              height: 10,
                              backgroundColor: theme.secondryColor,
                              borderRadius: 10 / 2,
                            }}
                          />
                        )}
                      </View>
                    </View>

                    <View
                      style={{
                        flex: 0.8875,
                        justifyContent: 'center',
                      }}>
                      <Text
                        style={{
                          fontSize: Platform.OS == 'ios' ? 12 : 14,
                          color: 'white',
                          fontWeight: '600',
                        }}>
                        {lol.label}
                      </Text>
                    </View>
                  </TouchableOpacity>
                ))}

              {selectedEchoingOptionError && (
                <Text
                  style={{
                    fontSize: Platform.OS == 'ios' ? 12 : 14,
                    color: 'red',
                    fontWeight: '600',
                    marginHorizontal: 4,
                    marginVertical: 8,
                  }}>
                  Please! Select any one of the echoing(s)!
                </Text>
              )}

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginVertical: 4,
                }}>
                <TouchableOpacity
                  style={styles.btnview1}
                  onPress={() => {
                    if (Boolean(selectedEchoingOption)) {
                      setEchoingOptionsModal(!echoingOptionsModal);

                      echoMe();
                    } else {
                      setSelectedEchoingOptionError(
                        selectedEchoingOption ? false : true,
                      );
                    }
                  }}>
                  <Text style={styles.btntext}>Confirm</Text>
                </TouchableOpacity>

                <TouchableOpacity
                  onPress={() => {
                    setEchoingOptionsModal(!echoingOptionsModal);
                  }}>
                  <Text
                    style={{
                      fontSize: Platform.OS == 'ios' ? 14 : 16,
                      color: theme.secondryColor,
                      fontWeight: 'bold',
                      textAlign: 'center',
                      marginHorizontal: 16,
                    }}>
                    Cancel
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </Modal>
      </Modal>

      {/* Hided due to TD Ameritrade Echo App revoked issue */}
      {/* <AmeritradeAlert
        modalStatus={ameritradeAlertStatus}
        close={handleAmeritradeAlertStatus}
      /> */}

      <BottomNav routeName="Discover People" />
    </>
  );
};

export default DiscoverPeopleProfile;
