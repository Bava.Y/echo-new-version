import React from 'react';
import {FlatList, Platform, Text, View} from 'react-native';
import Loading from '../../components/Loading';
import theme from '../../utils/theme';

const InsideSummary = ({route}) => {
  const {summary} = route.params;

  const renderItem = ({item}) => <Item item={item} />;

  const Item = ({item}) => (
    <View style={{marginVertical: 8}}>
      <Text
        style={{
          fontSize: Platform.OS == 'ios' ? 14 : 16,
          color: theme.secondryColor,
          fontWeight: 'bold',
          marginVertical: 8,
        }}>
        {item?.fullName ?? '-'}
      </Text>

      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginVertical: 4,
        }}>
        <View style={{width: '50%', justifyContent: 'center'}}>
          <Text
            style={{
              fontSize: Platform.OS == 'ios' ? 12 : 14,
              color: 'white',
              fontWeight: '600',
            }}>
            Net Transaction Shares
          </Text>
        </View>

        <View style={{width: '50%', justifyContent: 'center'}}>
          <Text
            style={{
              fontSize: Platform.OS == 'ios' ? 12 : 14,
              color: 'white',
              fontWeight: '600',
              textAlign: 'right',
            }}>
            {item?.netTransacted ?? '-'}
          </Text>
        </View>
      </View>

      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginVertical: 4,
        }}>
        <View style={{width: '50%', justifyContent: 'center'}}>
          <Text
            style={{
              fontSize: Platform.OS == 'ios' ? 12 : 14,
              color: 'white',
              fontWeight: '600',
            }}>
            Share Purchased
          </Text>
        </View>

        <View style={{width: '50%', justifyContent: 'center'}}>
          <Text
            style={{
              fontSize: Platform.OS == 'ios' ? 12 : 14,
              color: 'white',
              fontWeight: '600',
              textAlign: 'right',
            }}>
            {item?.totalBought ?? '-'}
          </Text>
        </View>
      </View>

      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginVertical: 4,
        }}>
        <View style={{width: '50%', justifyContent: 'center'}}>
          <Text
            style={{
              fontSize: Platform.OS == 'ios' ? 12 : 14,
              color: 'white',
              fontWeight: '600',
            }}>
            Shares Sold
          </Text>
        </View>

        <View style={{width: '50%', justifyContent: 'center'}}>
          <Text
            style={{
              fontSize: Platform.OS == 'ios' ? 12 : 14,
              color: 'white',
              fontWeight: '600',
              textAlign: 'right',
            }}>
            {item?.totalSold ?? '-'}
          </Text>
        </View>
      </View>
    </View>
  );

  return (
    <>
      {Boolean(summary) && Array.isArray(summary) ? (
        summary.length != 0 ? (
          <>
            <FlatList
              keyboardShouldPersistTaps={'handled'}
              showsHorizontalScrollIndicator={false}
              showsVerticalScrollIndicator={false}
              style={{paddingHorizontal: 8, paddingVertical: 8}}
              data={summary}
              renderItem={renderItem}
              keyExtractor={(_item, index) => index.toString()}
            />

            <View style={{paddingVertical: 12}} />
          </>
        ) : (
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontSize: Platform.OS == 'ios' ? 14 : 16,
                color: 'white',
              }}>
              No data found
            </Text>
          </View>
        )
      ) : (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Loading color={'white'} />
        </View>
      )}
    </>
  );
};

export default InsideSummary;
