import AsyncStorage from '@react-native-community/async-storage';
import PushNotificationIOS from '@react-native-community/push-notification-ios';
import {StackActions, useFocusEffect} from '@react-navigation/native';
import jwt from 'jwt-decode';
import React, {useCallback, useContext, useRef, useState} from 'react';
import {Image, Platform, Text, TouchableOpacity, View} from 'react-native';
import DeviceInfo from 'react-native-device-info';
import firebase from '@react-native-firebase/app';
import ReactNativePinView from 'react-native-pin-view';
import PushNotification from 'react-native-push-notification';
import Snackbar from 'react-native-snackbar';
import {auth} from '../api/Authentication.service';
import {CANCEL, OK} from '../assets/images';
import Loading from '../components/Loading';
import {UserDataContext} from '../context/UserDataContext';
import style from '../styles/mpinstyle';
import {
  BEARER,
  IS_FIRST_USER,
  LOGIN_USER,
  MPIN,
  WELCOME,
} from '../utils/constants';
import theme from '../utils/theme';

const SetCMpinScreen = ({navigation, route}) => {
  const pinView = useRef(null);

  const [fcmToken, setFCMToken] = useState(null);

  // Context Variables
  const {
    setAlpacaConfigStatus,
    setFourChartSymbols,
    setSingleChartSymbol,
    setTDConfigStatus,
    setToken,
    setUserData,
  } = useContext(UserDataContext);

  useFocusEffect(
    useCallback(() => {
      let isFocus = true;

      handlePushNotification();

      return () => {
        isFocus = false;
      };
    }, []),
  );

  const handlePushNotification = () => {
    PushNotification.checkPermissions((permission) => {});

    PushNotification.getChannels((channelId) => {});

    PushNotification.configure({
      onRegister: function (token) {
        Platform.OS === 'ios'
          ? getIOSToken()
          : [
              console.log('FCM Token::: ', token.token),

              setFCMToken(token.token),
            ];
      },

      onNotification: function (notification) {
        const clicked = notification.userInteraction;

        if (clicked) {
          handleNotificationNavigation(notification);
        }

        notification.finish(PushNotificationIOS.FetchResult.NoData);
      },

      onAction: function (notification) {
        PushNotification.invokeApp(notification);
      },

      onRegistrationError: function (registrationError) {},

      permissions: {
        alert: true,
        badge: true,
        sound: true,
      },

      popInitialNotification: true,
      requestPermissions: true,
      senderID: '103183859621',
    });
  };

  const getIOSToken = async () => {
    const fToken = await firebase.messaging().getToken();

    fToken && [console.log('FCM Token::: ', fToken), setFCMToken(fToken)];
  };

  const handleNotificationNavigation = (notification) => {
    const data = JSON.parse(notification.data.data);
    const page = notification.data.page;

    switch (page) {
      case 'admin':
        navigation.navigate('EchoAlert', {
          id: data.echo_message_id,
        });
        break;

      case 'EchoMetrics':
      case 'SymbolDisallowed':
        navigation.navigate('DisallowedTrades', {
          id: data.customer_id,
        });
        break;

      case 'rebalance':
        navigation.navigate('Rebalance', {
          id: data.customer_id,
        });
        break;

      case 'echoing':
        navigation.navigate('DiscoverPeopleProfile', {
          id: data.customer_id,
          loginStatus: data.login_status,
        });
        break;

      case 'people':
        navigation.navigate('Echoers', {id: data.direct_customer_id});
        break;

      case 'notification':
        navigation.navigate('Notification');
        break;

      default:
        break;
    }
  };

  useFocusEffect(
    useCallback(() => {
      let isActive = true;
      const setDeviceInfo = () => {
        var uniqueId = DeviceInfo.getUniqueId();
        setDeviceId(uniqueId);
      };
      setDeviceInfo();

      return () => {
        isActive = false;
      };
    }, [navigation]),
  );

  let userId = route.params.userInfo.user_id;

  const [deviceId, setDeviceId] = useState('');

  const [otp, setOtp] = useState('');

  const [validationError, setValidationError] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');
  const [loading, setLoading] = useState(false);

  const onTextChange = (text) => {
    setOtp(text);
    setValidationError(false);
  };

  const handleSubmit = async () => {
    let formData = validateFields();
    if (formData !== undefined) {
      setLoading(true);

      const data = await auth.saveMpin(formData);
      if (typeof data === 'string') {
        if (data === 'redirect') {
          return navigation.dispatch(StackActions.replace('Login'));
        }
        return Snackbar.show({
          text: data,
          duration: Snackbar.LENGTH_SHORT,
        });
      }

      await AsyncStorage.setItem(IS_FIRST_USER, data.is_first_login);
      setLoading(false);
      Snackbar.show({
        text: `MPIN registered successfully`,
        duration: Snackbar.LENGTH_SHORT,
      });
      await AsyncStorage.setItem(MPIN, 'true');
      setTimeout(() => {
        handleLogin();
      }, 2000);
    }
  };

  const validateFields = () => {
    let formData = new FormData();
    let previousOtp = route.params.previousOtp;
    if (otp.length !== 4) {
      setErrorMessage('Enter proper MPIN');
      setValidationError(true);
      formData = undefined;
    } else if (otp !== previousOtp) {
      setErrorMessage('MPIN mismatch');
      setValidationError(true);
      formData = undefined;
    } else {
      setErrorMessage('');
      formData && formData.append('mpin', otp);
      formData && formData.append('unique_id', deviceId);
      formData && formData.append('id', userId);
    }
    return formData;
  };

  const handleLogin = async () => {
    let formData = new FormData();

    formData && formData.append('mpin', otp);
    formData && formData.append('unique_id', deviceId);

    if (formData !== undefined) {
      setLoading(true);
      let isFirst = await AsyncStorage.getItem(IS_FIRST_USER);
      const data = await auth.loginMpin(formData);

      if (typeof data === 'string') {
        if (data == 'Invalid MPIN') {
          setLoading(false);
          setValidationError(true);
          setErrorMessage('Incorrect PIN');
        }
        if (data === 'redirect') {
          return navigation.dispatch(StackActions.replace('Login'));
        }
        return Snackbar.show({
          text: data,
          duration: Snackbar.LENGTH_SHORT,
        });
      }
      await AsyncStorage.setItem(IS_FIRST_USER, data.is_first_loggedin);
      setLoading(false);
      Snackbar.show({
        text:
          data.two_step_verification == 0
            ? `Logged in successfully`
            : `OTP sent successfully`,
        duration: Snackbar.LENGTH_SHORT,
      });
      await auth.saveLogin(data.token);
      let bearer = 'Bearer ' + data.token;
      setToken(bearer);
      await AsyncStorage.setItem(BEARER, `${bearer}`);
      let decoded = jwt(data.token);
      setUserData(decoded);
      await AsyncStorage.setItem(LOGIN_USER, JSON.stringify(decoded));
      const alpacaConfigStatus =
        data && data.hasOwnProperty('decoded')
          ? data.decoded.alpaca_account
          : false;

      setAlpacaConfigStatus(alpacaConfigStatus);
      data &&
        data.hasOwnProperty('decoded') &&
        data.decoded.single_chart_symbol &&
        setSingleChartSymbol(data.decoded.single_chart_symbol);
      data &&
        data.hasOwnProperty('decoded') &&
        data.decoded.four_chart_symbols &&
        setFourChartSymbols(data.decoded.four_chart_symbols);

      const ameritradeConfigStatus =
        data && data.hasOwnProperty('decoded')
          ? data.decoded.refresh_token
          : false;

      setTDConfigStatus(ameritradeConfigStatus);

      updateFCM();

      setTimeout(() => {
        setLoading(false);
        let userData = {...data, deviceId};

        navigation.dispatch(
          StackActions.replace(
            data.two_step_verification == 0
              ? // ? changeScreen == true
                isFirst == 'no'
                ? 'Dashboard'
                : 'Profile'
              : // : 'Profile'
                'MPinOTPScreen',
            {
              data: userData,
            },
          ),
        );
        AsyncStorage.setItem(WELCOME, 'true');
      }, 1500);
    }
  };

  const updateFCM = async () => {
    const requestData = {
      firebase_token: fcmToken,
    };

    await auth.updateFCM(requestData);
  };

  return (
    <>
      <View style={style.container}>
        <Text style={style.verifyText}>Set PIN</Text>

        <Text style={style.mpinText}>Confirm your 4-digit 
PIN</Text>

        <ReactNativePinView
          inputSize={20}
          ref={pinView}
          pinLength={4}
          buttonSize={60}
          buttonAreaStyle={{
            marginTop: 24,
          }}
          inputViewEmptyStyle={{
            backgroundColor: 'transparent',
            borderColor: '#FFF',
            borderWidth: 1,
          }}
          inputViewFilledStyle={{
            backgroundColor: validationError ? 'red' : '#FFF',
          }}
          buttonViewStyle={{
            borderColor: '#FFF',
            borderWidth: 1,
          }}
          onButtonPress={(key) => {
            if (key === 'custom_left') {
              pinView.current.clear();
            }
          }}
          customLeftButton={
            <View>
              <Image source={CANCEL} style={style.customButton} />
            </View>
          }
          customRightButton={
            <TouchableOpacity onPress={handleSubmit}>
              <Image source={OK} style={style.customButton} />
            </TouchableOpacity>
          }
          onValueChange={onTextChange}
        />
        {validationError ? (
          <Text
            style={{
              fontSize: Platform.OS == 'ios' ? 14 : 16,
              color: 'red',
              marginVertical: 20,
            }}>
            {errorMessage}
          </Text>
        ) : null}
      </View>

      {loading && <Loading color={theme.white} />}
    </>
  );
};

export default SetCMpinScreen;
