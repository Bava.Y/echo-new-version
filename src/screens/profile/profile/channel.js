import React from 'react';
import {Image, Text, View} from 'react-native';
import {
  FACEBOOK,
  INSTAGRAM,
  LINKEDIN,
  TWITTER,
} from '../../../assets/images/index';
import theme from '../../../utils/theme';
import styles from '../style';

const Channels = ({}) => {
  return (
    <View style={{backgroundColor: theme.primaryColor, flex: 1}}>
      <View style={styles.socialmedia}>
        <View style={styles.activesocialmedialist}>
          <View style={{flexDirection: 'row'}}>
            <View style={{width: '70%', flexDirection: 'row'}}>
              <View style={styles.bgsocialicon}>
                <Image source={INSTAGRAM} style={styles.socialicon}></Image>
              </View>
              <Text style={styles.socialmediatitle}>instagram</Text>
            </View>
            <View style={styles.connect}>
              <Text style={styles.connectsocialstatus}>Connected</Text>
            </View>
          </View>
          <View style={styles.followersection}>
            <View style={[styles.followers, styles.borderRight]}>
              <Text style={styles.followname}>Followers</Text>
              <Text style={styles.count}>108k</Text>
            </View>
            <View style={[styles.followers, styles.borderRight]}>
              <Text style={styles.followname}>Following</Text>
              <Text style={styles.count}>50</Text>
            </View>
            <View style={styles.followers}>
              <Text style={styles.followname}>Echoers</Text>
              <Text style={styles.count}>95k</Text>
            </View>
          </View>
        </View>
        <View style={styles.socialmedialist}>
          <View style={{width: '70%', flexDirection: 'row'}}>
            <View style={styles.bgsocialicon}>
              <Image source={FACEBOOK} style={styles.socialicon}></Image>
            </View>
            <Text style={styles.socialmediatitle}>Facebook</Text>
          </View>
          <View style={styles.notconnect}>
            <Text style={styles.socialstatus}>Connect</Text>
          </View>
        </View>
        <View style={styles.socialmedialist}>
          <View style={{width: '70%', flexDirection: 'row'}}>
            <View style={styles.bgsocialicon}>
              <Image source={TWITTER} style={styles.socialicon}></Image>
            </View>
            <Text style={styles.socialmediatitle}>Twitter</Text>
          </View>
          <View style={styles.notconnect}>
            <Text style={styles.socialstatus}>Connect</Text>
          </View>
        </View>
        <View style={styles.socialmedialist}>
          <View style={{width: '70%', flexDirection: 'row'}}>
            <View style={styles.bgsocialicon}>
              <Image source={LINKEDIN} style={styles.socialicon}></Image>
            </View>
            <Text style={styles.socialmediatitle}>LinkedIn</Text>
          </View>
          <View style={styles.notconnect}>
            <Text style={styles.socialstatus}>Connect</Text>
          </View>
        </View>
      </View>
    </View>
  );
};

export default Channels;
