import React from 'react';
import RadioButtonVertical from '../../components/RadioButtonVertical';
import {UserDataContext} from '../../context/UserDataContext';

const PearGroup = ({isVisible, hide}) => {
  const {peers} = React.useContext(UserDataContext);

  return <RadioButtonVertical peer={peers} isVisible={isVisible} hide={hide} />;
};

export default PearGroup;
