import {StackActions} from '@react-navigation/native';
import _ from 'lodash';
import React, {useState,useContext} from 'react';
import {
  KeyboardAvoidingView,
  Platform,
  Text,
  TouchableOpacity,
} from 'react-native';
import Snackbar from 'react-native-snackbar';
import {auth} from '../api/Authentication.service';
import {FloatingTitleTextInputField} from '../components/FloatingTitleTextInputField';
import Loading from '../components/Loading';
import style from '../styles/forgotpasswordstyle';
import theme from '../utils/theme';
import { UserDataContext } from '../context/UserDataContext';

const initialValidationError = {
  otpErr: false,
  passWord: false,
  conPassword: false,
};

const ResetPassword = ({navigation, route}) => {
  const [validationError, setValidationError] = useState({
    ...initialValidationError,
  });

  const [otp, setOtp] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');

  const [loading, setLoading] = useState(false);

  const {
    passwordotp,
  } = useContext(UserDataContext);

  const validateFields = () => {
    let formData = new FormData();
    let error = validationError;

    if (otp === '') {
      error['otpErr'] = 'Enter OTP received';
      formData = undefined;
    } else if (otp != route.params.otp) {
      error['otpErr'] = 'Invalid OTP';
      formData = undefined;
    } else {
      error['otpErr'] = false;
      formData && formData.append('otp', otp);
    }

    if (password === '') {
      error['passWord'] = 'Enter password';
      error['conPassword'] = 'Enter confimation password';
      formData = undefined;
    } else if (confirmPassword == '') {
      error['conPassword'] = 'Enter confimation password';
      formData = undefined;
    } else if (password !== confirmPassword) {
      error['conPassword'] = 'Password mismatch';
      formData = undefined;
    } else {
      error['passWord'] = false;
      error['conPassword'] = false;
      formData && formData.append('password', password);
    }
    setValidationError({...validationError, ...error});
    return formData;
  };

  const handleSubmit = async () => {
    let formData = validateFields();
    if (formData !== undefined) {
      setLoading(true);

      const data = await auth.resetPassword(formData);

      setLoading(false);
      if (typeof data === 'string') {
        if (data === 'redirect') {
          return navigation.dispatch(StackActions.replace('Login'));
        }
        return Snackbar.show({
          text: data,
          duration: Snackbar.LENGTH_SHORT,
        });
      }
      Snackbar.show({
        text: `Password resets successfully`,
        duration: Snackbar.LENGTH_SHORT,
      });
      setTimeout(() => {
        navigation.navigate('Login');
      }, 2000);
    }
  };
  return (
    <>
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : null}
        keyboardVerticalOffset={Platform.OS === 'ios' ? 40 : 0}
        style={style.container}>
        <Text style={style.forgottitle}>Reset Password</Text>

        <FloatingTitleTextInputField
          keyboardType={'number-pad'}
          ispassword={false}
          attrName="otp"
          title="OTP"
          value={otp}
          updateMasterState={(text) => {
            setValidationError({
              ...validationError,
              otpErr: _.isEmpty(text) ? 'Enter OTP received' : false,
            });
            setOtp(text);
          }}
        />
        {validationError.otpErr == 'Invalid OTP' && (
          <Text style={style.errorotpinvalid}>{validationError.otpErr}</Text>
        )}
        {validationError.otpErr == 'Enter OTP received' && (
          <Text style={style.errorotp}>{validationError.otpErr}</Text>
        )}

        <FloatingTitleTextInputField
          ispassword={true}
          attrName="password"
          title="Password"
          value={password}
          updateMasterState={(text) => {
            setValidationError({
              ...validationError,
              passWord: _.isEmpty(text) ? 'Enter password' : false,
            });
            setPassword(text);
          }}
        />
        {validationError.passWord ? (
          <Text style={style.errormsg}>{validationError.passWord}</Text>
        ) : null}

        <FloatingTitleTextInputField
          ispassword={true}
          attrName="confirmPassword"
          title="Confirm Password"
          value={confirmPassword}
          updateMasterState={(text) => {
            setValidationError({
              ...validationError,
              conPassword: _.isEmpty(text) ? 'Re-enter password' : false,
            });
            setConfirmPassword(text);
          }}
        />
        {validationError.conPassword == 'Enter confimation password' ? (
          <Text style={style.errorconf}>{validationError.conPassword}</Text>
        ) : (
          <Text style={style.errormismatch}>{validationError.conPassword}</Text>
        )}
<Text style={{fontSize: Platform.OS == 'ios' ? 14 : 16, color: 'white'}}>{passwordotp}</Text>
        <TouchableOpacity
          style={[
            style.signinBtn,
            {width: '37.5%', flex: 0, marginVertical: 10},
          ]}
          onPress={handleSubmit}>
          <Text style={style.signintext}>RESET</Text>
        </TouchableOpacity>
      </KeyboardAvoidingView>

      {loading && <Loading color={theme.white} />}
    </>
  );
};

export default ResetPassword;
