import {useFocusEffect} from '@react-navigation/core';
import _ from 'lodash';
import React, {useContext, useRef, useState} from 'react';
import {
  ActivityIndicator,
  Keyboard,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Snackbar from 'react-native-snackbar';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {profile} from '../../../api/profile.service';
import {UserDataContext} from '../../../context/UserDataContext';
import theme from '../../../utils/theme';
import Search from '../../Dashboard/Search';
import SuccessPopUp from '../components/successPopUp/successPopUp';
import Accordian from './../../../components/Accordion';
import styles from './styles';

let initialValidationError = {
  eqmin: false,
  eqmax: false,
  eqyears: false,
  assetmin: false,
  assetmax: false,
  assetyears: false,
  netPromin: false,
  netPromax: false,
  netProyears: false,
  opCashmin: false,
  opCashmax: false,
  opCashyears: false,
  freeCashmin: false,
  freeCashmax: false,
  freeCashyears: false,
  ebMarginmin: false,
  ebMarginmax: false,
  ebMarginyears: false,
  revGrowthmin: false,
  revGrowthmax: false,
  revGrowthyears: false,
  shareGrowthmin: false,
  shareGrowthmax: false,
  shareGrowthyears: false,
  cashFlowGrowthmin: false,
  cashFlowGrowthmax: false,
  cashFlowGrowthyears: false,
  freeCashFlowGrowthmin: false,
  freeCashFlowGrowthmax: false,
  freeCashFlowGrowthyears: false,
  marketCapmin: false,
  marketCapmax: false,
  marketCapyears: false,
  entValmin: false,
  entValmax: false,
  entValyears: false,
  netDebtmin: false,
  netDebtmax: false,
  netDebtyears: false,
  evEbimin: false,
  evEbimax: false,
  evEbiyears: false,
  longTermmin: false,
  longTermmax: false,
  longTermyears: false,
  currentRatiomin: false,
  currentRatiomax: false,
  currentRatioyears: false,
  peRatiomin: false,
  peRatiomax: false,
  peRatioyears: false,
  pegRatiomin: false,
  pegRatiomax: false,
  pegRatioyears: false,
  poCashmin: false,
  poCashmax: false,
  poCashyears: false,
  pfCashmin: false,
  pfCashmax: false,
  pfCashyears: false,
};

const Performance = () => {
  // Performance Variables
  // Disallowed Trade Variables
  const [disallowed, setDisallowed] = useState([]);
  const [dloading, setDloading] = useState(false);
  const [tempArr, setTempArr] = useState([]);
  const [disArrLength, setDisArrLength] = useState(0);

  // Return on equity Variables
  const [eqMinValue, setEqMinValue] = useState('');
  const [eqMaxValue, setEqMaxValue] = useState('');
  const [eqYears, setEqYears] = useState('');
  const [eqStatus, setEqStatus] = useState(0);
  const [equityExpanded, setEquityExpanded] = useState(false);

  // Return on equity - Ref Variables
  const eqMinRef = useRef();
  const eqMaxRef = useRef();
  const eqYearRef = useRef();

  // Return on assets Variables
  const [assestsMinValue, setassestsMinValue] = useState('');
  const [assestsMaxValue, setassestsMaxValue] = useState('');
  const [assestsYears, setassestsYears] = useState('');
  const [assestsStatus, setassestsStatus] = useState(0);
  const [assetsExpanded, setAssetsExpanded] = useState(false);

  // Return on assets - Ref Variables
  const assetsMinRef = useRef();
  const assetsMaxRef = useRef();
  const assetsYearRef = useRef();

  // Net Profit Variables
  const [netProMinValue, setnetProMinValue] = useState('');
  const [netProMaxValue, setnetProMaxValue] = useState('');
  const [netProYears, setnetProYears] = useState('');
  const [netProStatus, setnetProStatus] = useState(0);
  const [netProfitExpanded, setNetProfitExpanded] = useState(false);

  // Net Profit - Ref Variables
  const netProMinRef = useRef();
  const netProMaxRef = useRef();
  const netProYearRef = useRef();

  // Operating cash Variables
  const [opCashMinValue, setopCashMinValue] = useState('');
  const [opCashMaxValue, setopCashMaxValue] = useState('');
  const [opCashYears, setopCashYears] = useState('');
  const [opCashStatus, setopCashStatus] = useState(0);
  const [cashFlowExpanded, setCashFlowExpanded] = useState(false);

  // Operating cash - Ref Variables
  const opCashMinRef = useRef();
  const opCashMaxRef = useRef();
  const opCashYearRef = useRef();

  // Free cash Variables
  const [freeCashMinValue, setfreeCashMinValue] = useState('');
  const [freeCashMaxValue, setfreeCashMaxValue] = useState('');
  const [freeCashYears, setfreeCashYears] = useState('');
  const [freeCashStatus, setfreeCashStatus] = useState(0);
  const [freeCashExpanded, setFreeCashExpanded] = useState(false);

  // Free cash - Ref Variables
  const freeCashMinRef = useRef();
  const freeCashMaxRef = useRef();
  const freeCashYearRef = useRef();

  // EB Margin Variables
  const [ebMarginMinValue, setebMarginMinValue] = useState('');
  const [ebMarginMaxValue, setebMarginMaxValue] = useState('');
  const [ebMarginYears, setebMarginYears] = useState('');
  const [ebMarginStatus, setebMarginStatus] = useState(0);
  const [ebExpanded, setEbExpanded] = useState(false);

  // EB Margin - Ref Variables
  const ebMarginMinRef = useRef();
  const ebMarginMaxRef = useRef();
  const ebMarginYearRef = useRef();

  // Revenue Growth Variables
  const [revGrowthMinValue, setrevGrowthMinValue] = useState('');
  const [revGrowthMaxValue, setrevGrowthMaxValue] = useState('');
  const [revGrowthYears, setrevGrowthYears] = useState('');
  const [revGrowthStatus, setrevGrowthStatus] = useState(0);
  const [revenueExpanded, setRevenueExpanded] = useState(false);

  // Revenue Growth - Ref Variables
  const revGrowthMinRef = useRef();
  const revGrowthMaxRef = useRef();
  const revGrowthYearRef = useRef();

  // Share Growth Variables
  const [shareGrowthMinValue, setshareGrowthMinValue] = useState('');
  const [shareGrowthMaxValue, setshareGrowthMaxValue] = useState('');
  const [shareGrowthYears, setshareGrowthYears] = useState('');
  const [shareGrowthStatus, setshareGrowthStatus] = useState(0);
  const [shareGrowthExpanded, setShareGrowthExpanded] = useState(false);

  // Share Growth - Ref Variables
  const shareGrowthMinRef = useRef();
  const shareGrowthMaxRef = useRef();
  const shareGrowthYearRef = useRef();

  // Cash Flow Growth Variables
  const [cashFlowGrowthMinValue, setcashFlowGrowthMinValue] = useState('');
  const [cashFlowGrowthMaxValue, setcashFlowGrowthMaxValue] = useState('');
  const [cashFlowGrowthYears, setcashFlowGrowthYears] = useState('');
  const [cashFlowGrowthStatus, setcashFlowGrowthStatus] = useState(0);
  const [opCashExpanded, setOpCashExpanded] = useState(false);

  // Cash Flow Growth - Ref Variables
  const cashFlowGrowthMinRef = useRef();
  const cashFlowGrowthMaxRef = useRef();
  const cashFlowGrowthYearRef = useRef();

  // Free Cash Flow Growth Variables
  const [freeCashFlowGrowthMinValue, setfreeCashFlowGrowthMinValue] = useState(
    '',
  );
  const [freeCashFlowGrowthMaxValue, setfreeCashFlowGrowthMaxValue] = useState(
    '',
  );
  const [freeCashFlowGrowthYears, setfreeCashFlowGrowthYears] = useState('');
  const [freeCashFlowGrowthStatus, setfreeCashFlowGrowthStatus] = useState(0);
  const [annFreeExpanded, setAnnFreeExpanded] = useState(false);

  // Free Cash Flow Growth - Ref Variables
  const freeCashFlowGrowthMinRef = useRef();
  const freeCashFlowGrowthMaxRef = useRef();
  const freeCashFlowGrowthYearRef = useRef();

  // Market Capital Variables
  const [marketCapMinValue, setmarketCapMinValue] = useState('');
  const [marketCapMaxValue, setmarketCapMaxValue] = useState('');
  const [marketCapYears, setmarketCapYears] = useState('');
  const [marketCapStatus, setmarketCapStatus] = useState(0);
  const [marCapExpanded, setMarCapExpanded] = useState(false);

  // Market Capital - Ref Variables
  const marketCapMinRef = useRef();
  const marketCapMaxRef = useRef();
  const marketCapYearRef = useRef();

  // Enterprise Value Variables
  const [entValMinValue, setentValMinValue] = useState('');
  const [entValMaxValue, setentValMaxValue] = useState('');
  const [entValYears, setentValYears] = useState('');
  const [entValStatus, setentValStatus] = useState(0);
  const [entValExpanded, setEntValExpanded] = useState(false);

  // Enterprise Value - Ref Variables
  const entValMinRef = useRef();
  const entValMaxRef = useRef();
  const entValYearRef = useRef();

  // Net Debt Variables
  const [netDebtMinValue, setnetDebtMinValue] = useState('');
  const [netDebtMaxValue, setnetDebtMaxValue] = useState('');
  const [netDebtYears, setnetDebtYears] = useState('');
  const [netDebtStatus, setnetDebtStatus] = useState(0);
  const [netDebtExpanded, setNetDebtExpanded] = useState(false);

  // Net Debt - Ref Variables
  const netDebtMinRef = useRef();
  const netDebtMaxRef = useRef();
  const netDebtYearRef = useRef();

  // EV EBI Variables
  const [evEbiMinValue, setevEbiMinValue] = useState('');
  const [evEbiMaxValue, setevEbiMaxValue] = useState('');
  const [evEbiYears, setevEbiYears] = useState('');
  const [evEbiStatus, setevEbiStatus] = useState(0);
  const [evEbiExpanded, setEvEbiExpanded] = useState(false);

  // EV EBI - Ref Variables
  const evEbiMinRef = useRef();
  const evEbiMaxRef = useRef();
  const evEbiYearRef = useRef();

  // Long Term Debt Variables
  const [longTermMinValue, setlongTermMinValue] = useState('');
  const [longTermMaxValue, setlongTermMaxValue] = useState('');
  const [longTermYears, setlongTermYears] = useState('');
  const [longTermStatus, setlongTermStatus] = useState(0);
  const [longTermExpanded, setLongTermExpanded] = useState(false);

  // Long Term Debt - Ref Variables
  const longTermMinRef = useRef();
  const longTermMaxRef = useRef();
  const longTermYearRef = useRef();

  // Current Ratio Variables
  const [currentRatioMinValue, setcurrentRatioMinValue] = useState('');
  const [currentRatioMaxValue, setcurrentRatioMaxValue] = useState('');
  const [currentRatioYears, setcurrentRatioYears] = useState('');
  const [currentRatioStatus, setcurrentRatioStatus] = useState(0);
  const [currentExpanded, setCurrentExpanded] = useState(false);

  // Current Ratio - Ref Variables
  const currentRatioMinRef = useRef();
  const currentRatioMaxRef = useRef();
  const currentRatioYearRef = useRef();

  // P/E Ratio Variables
  const [peRatioMinValue, setpeRatioMinValue] = useState('');
  const [peRatioMaxValue, setpeRatioMaxValue] = useState('');
  const [peRatioYears, setpeRatioYears] = useState('');
  const [peRatioStatus, setpeRatioStatus] = useState(0);
  const [peExpanded, setPeExpanded] = useState(false);

  // P/E Ratio - Ref Variables
  const peRatioMinRef = useRef();
  const peRatioMaxRef = useRef();
  const peRatioYearRef = useRef();

  // PEG Ratio Variables
  const [pegRatioMinValue, setpegRatioMinValue] = useState('');
  const [pegRatioMaxValue, setpegRatioMaxValue] = useState('');
  const [pegRatioYears, setpegRatioYears] = useState('');
  const [pegRatioStatus, setpegRatioStatus] = useState(0);
  const [pegExpanded, setPegExpanded] = useState(false);

  // PEG Ratio - Ref Variables
  const pegRatioMinRef = useRef();
  const pegRatioMaxRef = useRef();
  const pegRatioYearRef = useRef();

  // P/Operating Cash Flow Variables
  const [poCashMinValue, setpoCashMinValue] = useState('');
  const [poCashMaxValue, setpoCashMaxValue] = useState('');
  const [poCashYears, setpoCashYears] = useState('');
  const [poCashStatus, setpoCashStatus] = useState(0);
  const [poExpanded, setPoExpanded] = useState(false);

  const poCashMinRef = useRef();
  const poCashMaxRef = useRef();
  const poCashYearRef = useRef();

  // P/Free Cash Flow Variables
  const [pfCashMinValue, setpfCashMinValue] = useState('');
  const [pfCashMaxValue, setpfCashMaxValue] = useState('');
  const [pfCashYears, setpfCashYears] = useState('');
  const [pfCashStatus, setpfCashStatus] = useState(0);
  const [pfExpanded, setPfExpanded] = useState(false);

  // P/Free Cash Flow - Ref Variables
  const pfCashMinRef = useRef();
  const pfCashMaxRef = useRef();
  const pfCashYearRef = useRef();

  // Success Pop Up Variables
  const [modal, setModal] = useState(false);
  const [res, setRes] = useState(null);
  const [successText, setSuccessText] = useState('');

  // Context Variables
  const {token} = useContext(UserDataContext);

  // Other Variables
  const [validationError, setValidationError] = useState({
    ...initialValidationError,
  });
  const [paddingStatus, setPaddingStatus] = useState(false);

  useFocusEffect(
    React.useCallback(() => {
      let ac = new AbortController();

      getPerformance();

      getDisallowedTrade();

      return () => {
        ac.abort();
      };
    }, []),
  );

  const getPerformance = async () => {
    let responce = await profile.getPermormance(token);
    let resp = responce[0];

    setEqStatus(Number(resp.equity_status));
    setassestsStatus(Number(resp.assets_status));
    setnetProStatus(Number(resp.net_pro_status));
    setopCashStatus(Number(resp.op_cash_status));
    setfreeCashStatus(Number(resp.free_cash_status));
    setebMarginStatus(Number(resp.eb_margin_status));
    setrevGrowthStatus(Number(resp.rev_growth_status));
    setshareGrowthStatus(Number(resp.share_growth_status));
    setcashFlowGrowthStatus(Number(resp.cash_flowgrowth_status));
    setfreeCashStatus(Number(resp.free_cash_flow_status));
    setmarketCapStatus(Number(resp.mar_cap_status));
    setentValStatus(Number(resp.ent_val_status));
    setnetDebtStatus(Number(resp.net_debt_status));
    setevEbiStatus(Number(resp.ev_ebi_status));
    setlongTermStatus(Number(resp.long_term_status));
    setcurrentRatioStatus(Number(resp.cur_ratio_status));
    setpeRatioStatus(Number(resp.pe_ratio_status));
    setpegRatioStatus(Number(resp.peg_ratio_status));
    setpoCashStatus(Number(resp.po_cash_status));
    setpfCashStatus(Number(resp.pf_cash_status));
    setEqMinValue(!_.isNull(resp.equity_min) ? resp.equity_min : '');
    setEqMaxValue(!_.isNull(resp.equity_max) ? resp.equity_max : '');
    setEqYears(!_.isNull(resp.equity_years) ? resp.equity_years : '');
    setassestsMinValue(!_.isNull(resp.assets_min) ? resp.assets_min : '');
    setassestsMaxValue(!_.isNull(resp.assets_max) ? resp.assets_max : '');
    setassestsYears(!_.isNull(resp.assets_years) ? resp.assets_years : '');
    setnetProMinValue(!_.isNull(resp.net_pro_min) ? resp.net_pro_min : '');
    setnetProMaxValue(!_.isNull(resp.net_pro_max) ? resp.net_pro_max : '');
    setnetProYears(!_.isNull(resp.net_pro_years) ? resp.net_pro_years : '');
    setopCashMinValue(!_.isNull(resp.op_cash_min) ? resp.op_cash_min : '');
    setopCashMaxValue(!_.isNull(resp.op_cash_max) ? resp.op_cash_max : '');
    setopCashYears(!_.isNull(resp.op_cash_years) ? resp.op_cash_years : '');
    setfreeCashMinValue(
      !_.isNull(resp.free_cash_min) ? resp.free_cash_min : '',
    );
    setfreeCashMaxValue(
      !_.isNull(resp.free_cash_max) ? resp.free_cash_max : '',
    );
    setcashFlowGrowthYears(
      !_.isNull(resp.free_cash_years) ? resp.free_cash_years : '',
    );
    setebMarginMinValue(
      !_.isNull(resp.eb_margin_min) ? resp.eb_margin_min : '',
    );
    setebMarginMaxValue(
      !_.isNull(resp.eb_margin_max) ? resp.eb_margin_max : '',
    );
    setebMarginYears(
      !_.isNull(resp.eb_margin_years) ? resp.eb_margin_years : '',
    );
    setrevGrowthMinValue(
      !_.isNull(resp.rev_growth_min) ? resp.rev_growth_min : '',
    );
    setrevGrowthMaxValue(
      !_.isNull(resp.rev_growth_max) ? resp.rev_growth_max : '',
    );
    setrevGrowthYears(
      !_.isNull(resp.rev_growth_years) ? resp.rev_growth_years : '',
    );
    setshareGrowthMinValue(
      !_.isNull(resp.share_growth_min) ? resp.share_growth_min : '',
    );
    setshareGrowthMaxValue(
      !_.isNull(resp.share_growth_max) ? resp.share_growth_max : '',
    );
    setshareGrowthYears(
      !_.isNull(resp.share_growth_years) ? resp.share_growth_years : '',
    );
    setcashFlowGrowthMinValue(
      !_.isNull(resp.cash_flowgrowth_min) ? resp.cash_flowgrowth_min : '',
    );
    setcashFlowGrowthMaxValue(
      !_.isNull(resp.cash_flowgrowth_max) ? resp.cash_flowgrowth_max : '',
    );
    setcashFlowGrowthYears(
      !_.isNull(resp.cash_flowgrowth_years) ? resp.cash_flowgrowth_years : '',
    );
    setfreeCashMinValue(
      !_.isNull(resp.free_cash_flow_min) ? resp.free_cash_flow_min : '',
    );
    setfreeCashMaxValue(
      !_.isNull(resp.free_cash_flow_max) ? resp.free_cash_flow_max : '',
    );
    setfreeCashYears(
      !_.isNull(resp.free_cash_flow_years) ? resp.free_cash_flow_years : '',
    );
    setmarketCapMinValue(!_.isNull(resp.mar_cap_min) ? resp.mar_cap_min : '');
    setmarketCapMaxValue(!_.isNull(resp.mar_cap_max) ? resp.mar_cap_max : '');
    setmarketCapYears(!_.isNull(resp.mar_cap_years) ? resp.mar_cap_years : '');
    setentValMinValue(!_.isNull(resp.ent_val_min) ? resp.ent_val_min : '');
    setentValMaxValue(!_.isNull(resp.ent_val_max) ? resp.ent_val_max : '');
    setentValYears(!_.isNull(resp.ent_val_years) ? resp.ent_val_years : '');
    setnetDebtMinValue(!_.isNull(resp.net_debt_min) ? resp.net_debt_min : '');
    // alert(typeof resp.net_debt_min);
    setnetDebtMaxValue(!_.isNull(resp.net_debt_max) ? resp.net_debt_max : '');
    setnetDebtYears(!_.isNull(resp.net_debt_years) ? resp.net_debt_years : '');
    setevEbiMinValue(!_.isNull(resp.ev_ebi_min) ? resp.ev_ebi_min : '');
    setevEbiMaxValue(!_.isNull(resp.ev_ebi_max) ? resp.ev_ebi_max : '');
    setevEbiYears(!_.isNull(resp.ev_ebi_years) ? resp.ev_ebi_years : '');
    setlongTermMinValue(
      !_.isNull(resp.long_term_min) ? resp.long_term_min : '',
    );
    setlongTermMaxValue(
      !_.isNull(resp.long_term_max) ? resp.long_term_max : '',
    );
    setlongTermYears(
      !_.isNull(resp.long_term_years) ? resp.long_term_years : '',
    );
    setcurrentRatioMinValue(
      !_.isNull(resp.cur_ratio_min) ? resp.cur_ratio_min : '',
    );
    setcurrentRatioMaxValue(
      !_.isNull(resp.cur_ratio_max) ? resp.cur_ratio_max : '',
    );
    setcurrentRatioYears(
      !_.isNull(resp.cur_ratio_years) ? resp.cur_ratio_years : '',
    );
    setpeRatioMinValue(!_.isNull(resp.pe_ratio_min) ? resp.pe_ratio_min : '');
    setpeRatioMaxValue(!_.isNull(resp.pe_ratio_max) ? resp.pe_ratio_max : '');
    setpeRatioYears(!_.isNull(resp.pe_ratio_years) ? resp.pe_ratio_years : '');
    setpegRatioMinValue(
      !_.isNull(resp.peg_ratio_min) ? resp.peg_ratio_min : '',
    );
    setpegRatioMaxValue(
      !_.isNull(resp.peg_ratio_max) ? resp.peg_ratio_max : '',
    );
    setpegRatioYears(
      !_.isNull(resp.peg_ratio_years) ? resp.peg_ratio_years : '',
    );
    setpoCashYears(!_.isNull(resp.po_cash_min) ? resp.po_cash_min : '');
    setpoCashMinValue(!_.isNull(resp.po_cash_max) ? resp.po_cash_max : '');
    setpoCashMaxValue(!_.isNull(resp.po_cash_years) ? resp.po_cash_years : '');
    setpfCashMinValue(!_.isNull(resp.pf_cash_min) ? resp.pf_cash_min : '');
    setpfCashMaxValue(!_.isNull(resp.pf_cash_max) ? resp.pf_cash_max : '');
    setpfCashYears(!_.isNull(resp.pf_cash_years) ? resp.pf_cash_years : '');
  };

  const getDisallowedTrade = async () => {
    const res = await profile.getDisallowedTrade(token);

    setDisArrLength(res.data.length);

    setDisallowed(res.data);
  };

  const minEqValueChange = (text) => {
    setValidationError({
      ...validationError,
      eqmin: 'no',
    });

    setEqMinValue(text);
  };

  const maxEqValueChange = (text) => {
    setValidationError({
      ...validationError,
      eqmax: 'no',
    });

    setEqMaxValue(text);
  };

  const eqYearChange = (text) => {
    setValidationError({
      ...validationError,
      eqyears: 'no',
    });

    setEqYears(text);
  };

  const equitytoggleExpand = () => {
    setEquityExpanded(!equityExpanded);
  };

  const minAssetValueChange = (text) => {
    setValidationError({
      ...validationError,
      assetmin: 'no',
    });

    setassestsMinValue(text);
  };

  const maxAssetValueChange = (text) => {
    setValidationError({
      ...validationError,
      assetmax: 'no',
    });

    setassestsMaxValue(text);
  };

  const eqAssetChange = (text) => {
    setValidationError({
      ...validationError,
      assetyears: 'no',
    });

    setassestsYears(text);
  };

  const assettoggleExpand = () => {
    setAssetsExpanded(!assetsExpanded);
  };

  const minNetProValueChange = (text) => {
    setValidationError({
      ...validationError,
      netPromin: 'no',
    });

    setnetProMinValue(text);
  };

  const maxNetProValueChange = (text) => {
    setValidationError({
      ...validationError,
      netPromax: 'no',
    });

    setnetProMaxValue(text);
  };

  const yearsNetProChange = (text) => {
    setValidationError({
      ...validationError,
      netProyears: 'no',
    });

    setnetProYears(text);
  };

  const netProtoggleExpand = () => {
    setNetProfitExpanded(!netProfitExpanded);
  };

  const minOpCashValueChange = (text) => {
    setValidationError({
      ...validationError,
      opCashmin: 'no',
    });

    setopCashMinValue(text);
  };

  const maxOpCashValueChange = (text) => {
    setValidationError({
      ...validationError,
      opCashmax: 'no',
    });

    setopCashMaxValue(text);
  };

  const yearsOpCashChange = (text) => {
    setValidationError({
      ...validationError,
      opCashyears: 'no',
    });

    setopCashYears(text);
  };

  const cashFlowtoggleExpand = () => {
    setCashFlowExpanded(!cashFlowExpanded);
  };

  const minFreeCashValueChange = (text) => {
    setValidationError({
      ...validationError,
      freeCashmin: 'no',
    });

    setfreeCashMinValue(text);
  };

  const maxFreeCashValueChange = (text) => {
    setValidationError({
      ...validationError,
      freeCashmax: 'no',
    });

    setfreeCashMaxValue(text);
  };

  const yearsFreeCashChange = (text) => {
    setValidationError({
      ...validationError,
      freeCashyears: 'no',
    });

    setfreeCashYears(text);
  };

  const freeCashtoggleExpand = () => {
    setFreeCashExpanded(!freeCashExpanded);
  };

  const minEbMarginValueChange = (text) => {
    setValidationError({
      ...validationError,
      ebMarginmin: 'no',
    });

    setebMarginMinValue(text);
  };

  const maxEbMarginValueChange = (text) => {
    setValidationError({
      ...validationError,
      ebMarginmax: 'no',
    });

    setebMarginMaxValue(text);
  };

  const yearsEbMarginChange = (text) => {
    setValidationError({
      ...validationError,
      ebMarginyears: 'no',
    });

    setebMarginYears(text);
  };

  const ebitoggleExpand = () => {
    setEbExpanded(!ebExpanded);
  };

  const minRevGrowthValueChange = (text) => {
    setValidationError({
      ...validationError,
      revGrowthmin: 'no',
    });

    setrevGrowthMinValue(text);
  };

  const maxRevGrowthValueChange = (text) => {
    setValidationError({
      ...validationError,
      revGrowthmax: 'no',
    });

    setrevGrowthMaxValue(text);
  };

  const yearsRevGrowthChange = (text) => {
    setValidationError({
      ...validationError,
      revGrowthyears: 'no',
    });

    setrevGrowthYears(text);
  };

  const revenuetoggleExpand = () => {
    setRevenueExpanded(!revenueExpanded);
  };

  const minShareGrowthValueChange = (text) => {
    setValidationError({
      ...validationError,
      shareGrowthmin: 'no',
    });

    setshareGrowthMinValue(text);
  };

  const maxShareGrowthValueChange = (text) => {
    setValidationError({
      ...validationError,
      shareGrowthmax: 'no',
    });

    setshareGrowthMaxValue(text);
  };

  const yearsShareGrowthChange = (text) => {
    setValidationError({
      ...validationError,
      shareGrowthyears: 'no',
    });

    setshareGrowthYears(text);
  };

  const sharetoggleExpand = () => {
    setShareGrowthExpanded(!shareGrowthExpanded);
  };

  const minCashFlowGrowthValueChange = (text) => {
    setValidationError({
      ...validationError,
      cashFlowGrowthmin: 'no',
    });

    setcashFlowGrowthMinValue(text);
  };

  const maxCashFlowGrowthValueChange = (text) => {
    setValidationError({
      ...validationError,
      cashFlowGrowthmax: 'no',
    });

    setcashFlowGrowthMaxValue(text);
  };

  const yearsCashFlowGrowthChange = (text) => {
    setValidationError({
      ...validationError,
      cashFlowGrowthyears: 'no',
    });

    setcashFlowGrowthYears(text);
  };

  const opCashtoggleExpand = () => {
    setOpCashExpanded(!opCashExpanded);
  };

  const minFreeCashFlowGrowthValueChange = (text) => {
    setValidationError({
      ...validationError,
      freeCashFlowGrowthmin: 'no',
    });

    setfreeCashFlowGrowthMinValue(text);
  };

  const maxFreeCashFlowGrowthValueChange = (text) => {
    setValidationError({
      ...validationError,
      freeCashFlowGrowthmax: 'no',
    });

    setfreeCashFlowGrowthMaxValue(text);
  };

  const yearsFreeCashFlowGrowthChange = (text) => {
    setValidationError({
      ...validationError,
      freeCashFlowGrowthyears: 'no',
    });

    setfreeCashFlowGrowthYears(text);
  };

  const anCashtoggleExpand = () => {
    setAnnFreeExpanded(!annFreeExpanded);
  };

  const minMarCapValueChange = (text) => {
    setValidationError({
      ...validationError,
      marketCapmin: 'no',
    });

    setmarketCapMinValue(text);
  };

  const maxMarCapValueChange = (text) => {
    setValidationError({
      ...validationError,
      marketCapmax: 'no',
    });

    setmarketCapMaxValue(text);
  };

  const yearsMarCapChange = (text) => {
    setValidationError({
      ...validationError,
      marketCapyears: 'no',
    });

    setmarketCapYears(text);
  };

  const marCaptoggleExpand = () => {
    setMarCapExpanded(!marCapExpanded);
  };

  const minEntCapValueChange = (text) => {
    setValidationError({
      ...validationError,
      entValmin: 'no',
    });

    setentValMinValue(text);
  };

  const maxEntCapValueChange = (text) => {
    setValidationError({
      ...validationError,
      entValmax: 'no',
    });

    setentValMaxValue(text);
  };

  const yearsEntCapChange = (text) => {
    setValidationError({
      ...validationError,
      entValyears: 'no',
    });

    setentValYears(text);
  };

  const entValtoggleExpand = () => {
    setEntValExpanded(!entValExpanded);
  };

  const minNetDebtValueChange = (text) => {
    setValidationError({
      ...validationError,
      netDebtmin: 'no',
    });

    setnetDebtMinValue(text);
  };

  const maxNetDebtValueChange = (text) => {
    setValidationError({
      ...validationError,
      netDebtmax: 'no',
    });

    setnetDebtMaxValue(text);
  };

  const yearsNetDebtChange = (text) => {
    setValidationError({
      ...validationError,
      netDebtyears: 'no',
    });

    setnetDebtYears(text);
  };

  const netDebttoggleExpand = () => {
    setNetDebtExpanded(!netDebtExpanded);
  };

  const minEvEbitValueChange = (text) => {
    setValidationError({
      ...validationError,
      evEbimin: 'no',
    });

    setevEbiMinValue(text);
  };

  const maxEvEbitValueChange = (text) => {
    setValidationError({
      ...validationError,
      evEbimax: 'no',
    });

    setevEbiMaxValue(text);
  };

  const yearsEvEbitChange = (text) => {
    setValidationError({
      ...validationError,
      evEbiyears: 'no',
    });

    setevEbiYears(text);
  };

  const evEbitoggleExpand = () => {
    setEvEbiExpanded(!evEbiExpanded);
  };

  const minLongTermValueChange = (text) => {
    setValidationError({
      ...validationError,
      longTermmin: 'no',
    });

    setlongTermMinValue(text);
  };

  const maxLongTermValueChange = (text) => {
    setValidationError({
      ...validationError,
      longTermmax: 'no',
    });

    setlongTermMaxValue(text);
  };

  const yearsLongTermChange = (text) => {
    setValidationError({
      ...validationError,
      longTermyears: 'no',
    });

    setlongTermYears(text);
  };

  const longTermtoggleExpand = () => {
    setLongTermExpanded(!longTermExpanded);
  };

  const minCurrentRatioValueChange = (text) => {
    setValidationError({
      ...validationError,
      currentRatiomin: 'no',
    });

    setcurrentRatioMinValue(text);
  };

  const maxCurrentRatioValueChange = (text) => {
    setValidationError({
      ...validationError,
      currentRatiomax: 'no',
    });

    setcurrentRatioMaxValue(text);
  };

  const yearsCurrentRatioChange = (text) => {
    setValidationError({
      ...validationError,
      currentRatioyears: 'no',
    });

    setcurrentRatioYears(text);
  };

  const curRatiotoggleExpand = () => {
    setCurrentExpanded(!currentExpanded);
  };

  const minPeRatioValueChange = (text) => {
    setValidationError({
      ...validationError,
      peRatiomin: 'no',
    });

    setpeRatioMinValue(text);
  };

  const maxPeRatioValueChange = (text) => {
    setValidationError({
      ...validationError,
      peRatiomax: 'no',
    });

    setpeRatioMaxValue(text);
  };

  const yearsPeRatioChange = (text) => {
    setValidationError({
      ...validationError,
      peRatioyears: 'no',
    });

    setpeRatioYears(text);
  };

  const peRatiotoggleExpand = () => {
    setPeExpanded(!peExpanded);
  };

  const minPegRatioValueChange = (text) => {
    setValidationError({
      ...validationError,
      pegRatiomin: 'no',
    });

    setpegRatioMinValue(text);
  };

  const maxPegRatioValueChange = (text) => {
    setValidationError({
      ...validationError,
      pegRatiomax: 'no',
    });

    setpegRatioMaxValue(text);
  };

  const yearsPegRatioChange = (text) => {
    setValidationError({
      ...validationError,
      pegRatioyears: 'no',
    });

    setpegRatioYears(text);
  };

  const pegRatiotoggleExpand = () => {
    setPegExpanded(!pegExpanded);
  };

  const minPoCashValueChange = (text) => {
    setValidationError({
      ...validationError,
      poCashmin: 'no',
    });

    setpoCashMinValue(text);
  };

  const maxPoCashValueChange = (text) => {
    setValidationError({
      ...validationError,
      poCashmax: 'no',
    });

    setpoCashMaxValue(text);
  };

  const yearsPoCashChange = (text) => {
    setValidationError({
      ...validationError,
      poCashyears: 'no',
    });

    setpoCashYears(text);
  };

  const poRatiotoggleExpand = () => {
    setPoExpanded(!poExpanded);
  };

  const minPfCashValueChange = (text) => {
    setValidationError({
      ...validationError,
      pfCashmin: 'no',
    });

    setpfCashMinValue(text);
  };

  const maxPfCashValueChange = (text) => {
    setValidationError({
      ...validationError,
      pfCashmax: 'no',
    });

    setpfCashMaxValue(text);
  };

  const yearsPfCashChange = (text) => {
    setValidationError({
      ...validationError,
      pfCashyears: 'no',
    });

    setpfCashYears(text);
  };

  const pfRatiotoggleExpand = () => {
    setPfExpanded(!pfExpanded);
  };

  const menu = [
    {
      title: 'Return on Equity',
      status: eqStatus,
      minValue: eqMinValue,
      maxValue: eqMaxValue,
      years: eqYears,
      minValueChange: minEqValueChange,
      maxValueChange: maxEqValueChange,
      yearChange: eqYearChange,
      minError: validationError.eqmin,
      maxError: validationError.eqmax,
      yearsError: validationError.eqyears,
      referenceMin: eqMinRef,
      referenceMax: eqMaxRef,
      referenceYear: eqYearRef,
      expanded: equityExpanded,
      toggleExpand: equitytoggleExpand,
    },
    {
      title: 'Return on Assets',
      status: assestsStatus,
      minValue: assestsMinValue,
      maxValue: assestsMaxValue,
      years: assestsYears,
      minValueChange: minAssetValueChange,
      maxValueChange: maxAssetValueChange,
      yearChange: eqAssetChange,
      minError: validationError.assetmin,
      maxError: validationError.assetmax,
      yearsError: validationError.assetyears,
      referenceMin: assetsMinRef,
      referenceMax: assetsMaxRef,
      referenceYear: assetsYearRef,
      expanded: assetsExpanded,
      toggleExpand: assettoggleExpand,
    },
    {
      title: 'Net Profit Margin',
      status: netProStatus,
      minValue: netProMinValue,
      maxValue: netProMaxValue,
      years: netProYears,
      minValueChange: minNetProValueChange,
      maxValueChange: maxNetProValueChange,
      yearChange: yearsNetProChange,
      minError: validationError.netPromin,
      maxError: validationError.netPromax,
      yearsError: validationError.netProyears,
      referenceMin: netProMinRef,
      referenceMax: netProMaxRef,
      referenceYear: netProYearRef,
      expanded: netProfitExpanded,
      toggleExpand: netProtoggleExpand,
    },
    {
      title: 'Operating Cash Flow Margin',
      status: opCashStatus,
      minValue: opCashMinValue,
      maxValue: opCashMaxValue,
      years: opCashYears,
      minValueChange: minOpCashValueChange,
      maxValueChange: maxOpCashValueChange,
      yearChange: yearsOpCashChange,
      minError: validationError.opCashmin,
      maxError: validationError.opCashmax,
      yearsError: validationError.opCashyears,
      referenceMin: opCashMinRef,
      referenceMax: opCashMaxRef,
      referenceYear: opCashYearRef,
      expanded: cashFlowExpanded,
      toggleExpand: cashFlowtoggleExpand,
    },
    {
      title: 'Free Cash Flow Margin',
      status: freeCashStatus,
      minValue: freeCashMinValue,
      maxValue: freeCashMaxValue,
      years: freeCashYears,
      minValueChange: minFreeCashValueChange,
      maxValueChange: maxFreeCashValueChange,
      yearChange: yearsFreeCashChange,
      minError: validationError.freeCashmin,
      maxError: validationError.freeCashmax,
      yearsError: validationError.freeCashyears,
      referenceMin: freeCashMinRef,
      referenceMax: freeCashMaxRef,
      referenceYear: freeCashYearRef,
      expanded: freeCashExpanded,
      toggleExpand: freeCashtoggleExpand,
    },
    {
      title: 'EBITDA Margin',
      status: ebMarginStatus,
      minValue: ebMarginMinValue,
      maxValue: ebMarginMaxValue,
      years: ebMarginYears,
      minValueChange: minEbMarginValueChange,
      maxValueChange: maxEbMarginValueChange,
      yearChange: yearsEbMarginChange,
      minError: validationError.ebMarginmin,
      maxError: validationError.ebMarginmax,
      yearsError: validationError.ebMarginyears,
      referenceMin: ebMarginMinRef,
      referenceMax: ebMarginMaxRef,
      referenceYear: ebMarginYearRef,
      expanded: ebExpanded,
      toggleExpand: ebitoggleExpand,
    },
    {
      title: 'Annualized Revenue Growth',
      status: revGrowthStatus,
      minValue: revGrowthMinValue,
      maxValue: revGrowthMaxValue,
      years: revGrowthYears,
      minValueChange: minRevGrowthValueChange,
      maxValueChange: maxRevGrowthValueChange,
      yearChange: yearsRevGrowthChange,
      minError: validationError.revGrowthmin,
      maxError: validationError.revGrowthmax,
      yearsError: validationError.revGrowthyears,
      referenceMin: revGrowthMinRef,
      referenceMax: revGrowthMaxRef,
      referenceYear: revGrowthYearRef,
      expanded: revenueExpanded,
      toggleExpand: revenuetoggleExpand,
    },
    {
      title: 'Annualized Earnings / Share Growth',
      status: shareGrowthStatus,
      minValue: shareGrowthMinValue,
      maxValue: shareGrowthMaxValue,
      years: shareGrowthYears,
      minValueChange: minShareGrowthValueChange,
      maxValueChange: maxShareGrowthValueChange,
      yearChange: yearsShareGrowthChange,
      minError: validationError.shareGrowthmin,
      maxError: validationError.shareGrowthmax,
      yearsError: validationError.shareGrowthyears,
      referenceMin: shareGrowthMinRef,
      referenceMax: shareGrowthMaxRef,
      referenceYear: shareGrowthYearRef,
      expanded: shareGrowthExpanded,
      toggleExpand: sharetoggleExpand,
    },
    {
      title: 'Annualized Operating Cash Flow Growth',
      status: cashFlowGrowthStatus,
      minValue: cashFlowGrowthMinValue,
      maxValue: cashFlowGrowthMaxValue,
      years: cashFlowGrowthYears,
      minValueChange: minCashFlowGrowthValueChange,
      maxValueChange: maxCashFlowGrowthValueChange,
      yearChange: yearsCashFlowGrowthChange,
      minError: validationError.cashFlowGrowthmin,
      maxError: validationError.cashFlowGrowthmax,
      yearsError: validationError.cashFlowGrowthyears,
      referenceMin: cashFlowGrowthMinRef,
      referenceMax: cashFlowGrowthMaxRef,
      referenceYear: cashFlowGrowthYearRef,
      expanded: opCashExpanded,
      toggleExpand: opCashtoggleExpand,
    },
    {
      title: 'Annualized Free Cash Flow Growth',
      status: freeCashFlowGrowthStatus,
      minValue: freeCashFlowGrowthMinValue,
      maxValue: freeCashFlowGrowthMaxValue,
      years: freeCashFlowGrowthYears,
      minValueChange: minFreeCashFlowGrowthValueChange,
      maxValueChange: maxFreeCashFlowGrowthValueChange,
      yearChange: yearsFreeCashFlowGrowthChange,
      minError: validationError.freeCashFlowGrowthmin,
      maxError: validationError.freeCashFlowGrowthmax,
      yearsError: validationError.freeCashFlowGrowthyears,
      referenceMin: freeCashFlowGrowthMinRef,
      referenceMax: freeCashFlowGrowthMaxRef,
      referenceYear: freeCashFlowGrowthYearRef,
      expanded: annFreeExpanded,
      toggleExpand: anCashtoggleExpand,
    },
    {
      title: 'Market Capitalization',
      status: marketCapStatus,
      minValue: marketCapMinValue,
      maxValue: marketCapMaxValue,
      years: marketCapYears,
      minValueChange: minMarCapValueChange,
      maxValueChange: maxMarCapValueChange,
      yearChange: yearsMarCapChange,
      minError: validationError.marketCapmin,
      maxError: validationError.marketCapmax,
      yearsError: validationError.marketCapyears,
      referenceMin: marketCapMinRef,
      referenceMax: marketCapMaxRef,
      referenceYear: marketCapYearRef,
      expanded: marCapExpanded,
      toggleExpand: marCaptoggleExpand,
    },
    {
      title: 'Enterprise Value',
      status: entValStatus,
      minValue: entValMinValue,
      maxValue: entValMaxValue,
      years: entValYears,
      minValueChange: minEntCapValueChange,
      maxValueChange: maxEntCapValueChange,
      yearChange: yearsEntCapChange,
      minError: validationError.entValmin,
      maxError: validationError.entValmax,
      yearsError: validationError.entValyears,
      referenceMin: entValMinRef,
      referenceMax: entValMaxRef,
      referenceYear: entValYearRef,
      expanded: entValExpanded,
      toggleExpand: entValtoggleExpand,
    },
    {
      title: 'Net Debt / EBITDA',
      status: netDebtStatus,
      minValue: netDebtMinValue,
      maxValue: netDebtMaxValue,
      years: netDebtYears,
      minValueChange: minNetDebtValueChange,
      maxValueChange: maxNetDebtValueChange,
      yearChange: yearsNetDebtChange,
      minError: validationError.netDebtmin,
      maxError: validationError.netDebtmax,
      yearsError: validationError.netDebtyears,
      referenceMin: netDebtMinRef,
      referenceMax: netDebtMaxRef,
      referenceYear: netDebtYearRef,
      expanded: netDebtExpanded,
      toggleExpand: netDebttoggleExpand,
    },
    {
      title: 'EV / EBITDA',
      status: evEbiStatus,
      minValue: evEbiMinValue,
      maxValue: evEbiMaxValue,
      years: evEbiYears,
      minValueChange: minEvEbitValueChange,
      maxValueChange: maxEvEbitValueChange,
      yearChange: yearsEvEbitChange,
      minError: validationError.evEbimin,
      maxError: validationError.evEbimax,
      yearsError: validationError.evEbiyears,
      referenceMin: evEbiMinRef,
      referenceMax: evEbiMaxRef,
      referenceYear: evEbiYearRef,
      expanded: evEbiExpanded,
      toggleExpand: evEbitoggleExpand,
    },
    {
      title: 'Long - Term Debt/ Equity',
      status: longTermStatus,
      minValue: longTermMinValue,
      maxValue: longTermMaxValue,
      years: longTermYears,
      minValueChange: minLongTermValueChange,
      maxValueChange: maxLongTermValueChange,
      yearChange: yearsLongTermChange,
      minError: validationError.longTermmin,
      maxError: validationError.longTermmax,
      yearsError: validationError.longTermyears,
      referenceMin: longTermMinRef,
      referenceMax: longTermMaxRef,
      referenceYear: longTermYearRef,
      expanded: longTermExpanded,
      toggleExpand: longTermtoggleExpand,
    },
    {
      title: 'Current Ratio',
      status: currentRatioStatus,
      minValue: currentRatioMinValue,
      maxValue: currentRatioMaxValue,
      years: currentRatioYears,
      minValueChange: minCurrentRatioValueChange,
      maxValueChange: maxCurrentRatioValueChange,
      yearChange: yearsCurrentRatioChange,
      minError: validationError.currentRatiomin,
      maxError: validationError.currentRatiomax,
      yearsError: validationError.currentRatioyears,
      referenceMin: currentRatioMinRef,
      referenceMax: currentRatioMaxRef,
      referenceYear: currentRatioYearRef,
      expanded: currentExpanded,
      toggleExpand: curRatiotoggleExpand,
    },
    {
      title: 'P/E Ratio',
      status: peRatioStatus,
      minValue: peRatioMinValue,
      maxValue: peRatioMaxValue,
      years: peRatioYears,
      minValueChange: minPeRatioValueChange,
      maxValueChange: maxPeRatioValueChange,
      yearChange: yearsPeRatioChange,
      minError: validationError.peRatiomin,
      maxError: validationError.peRatiomax,
      yearsError: validationError.peRatioyears,
      referenceMin: peRatioMinRef,
      referenceMax: peRatioMaxRef,
      referenceYear: peRatioYearRef,
      expanded: peExpanded,
      toggleExpand: peRatiotoggleExpand,
    },
    {
      title: 'PEG Ratio',
      status: pegRatioStatus,
      minValue: pegRatioMinValue,
      maxValue: pegRatioMaxValue,
      years: pegRatioYears,
      minValueChange: minPegRatioValueChange,
      maxValueChange: maxPegRatioValueChange,
      yearChange: yearsPegRatioChange,
      minError: validationError.pegRatiomin,
      maxError: validationError.pegRatiomax,
      yearsError: validationError.pegRatioyears,
      referenceMin: pegRatioMinRef,
      referenceMax: pegRatioMaxRef,
      referenceYear: pegRatioYearRef,
      expanded: pegExpanded,
      toggleExpand: pegRatiotoggleExpand,
    },
    {
      title: 'P/Operating Cash Flow Ratio',
      status: poCashStatus,
      minValue: poCashMinValue,
      maxValue: poCashMaxValue,
      years: poCashYears,
      minValueChange: minPoCashValueChange,
      maxValueChange: maxPoCashValueChange,
      yearChange: yearsPoCashChange,
      minError: validationError.poCashmin,
      maxError: validationError.poCashmax,
      yearsError: validationError.poCashyears,
      referenceMin: poCashMinRef,
      referenceMax: poCashMaxRef,
      referenceYear: poCashYearRef,
      expanded: poExpanded,
      toggleExpand: poRatiotoggleExpand,
    },
    {
      title: 'P/Free Cash Flow Ratio',
      status: pfCashStatus,
      minValue: pfCashMinValue,
      maxValue: pfCashMaxValue,
      years: pfCashYears,
      minValueChange: minPfCashValueChange,
      maxValueChange: maxPfCashValueChange,
      yearChange: yearsPfCashChange,
      minError: validationError.pfCashmin,
      maxError: validationError.pfCashmax,
      yearsError: validationError.pfCashyears,
      referenceMin: pfCashMinRef,
      referenceMax: pfCashMaxRef,
      referenceYear: pfCashYearRef,
      expanded: pfExpanded,
      toggleExpand: pfRatiotoggleExpand,
    },
  ];

  const submitForm = async () => {
    Keyboard.dismiss();

    if (checkFormConditions()) {
      const requestData = handleRequestData();

      setModal(true);

      const response = await profile.performanceUpdate(token, requestData);

      if (response.keyword == 'success' || response.keyword != 'success') {
        setModal(true);

        setRes(response);

        setSuccessText(response.message);

        setTimeout(() => {
          setRes(null);

          setModal(false);
        }, 1000);
      } else {
        setModal(true);

        setRes(null);

        setSuccessText('Unable to update customer preference');

        setTimeout(() => {
          setRes(null);

          setModal(false);
        }, 1000);
      }
    } else {
      handleErrorValidation();
    }
  };

  const handleRequestData = () => {
    let formData = new FormData();

    formData.append('equity_min', eqMinValue);
    formData.append('equity_max', eqMaxValue);
    formData.append('equity_years', eqYears);

    formData.append('assets_min', assestsMinValue);
    formData.append('assets_max', assestsMaxValue);
    formData.append('assets_years', assestsYears);

    formData.append('net_pro_min', netProMinValue);
    formData.append('net_pro_max', netProMaxValue);
    formData.append('net_pro_years', netProYears);

    formData.append('op_cash_min', opCashMinValue);
    formData.append('op_cash_max', opCashMaxValue);
    formData.append('op_cash_years', opCashYears);

    formData.append('free_cash_min', freeCashMinValue);
    formData.append('free_cash_max', freeCashMaxValue);
    formData.append('free_cash_years', freeCashYears);

    formData.append('eb_margin_min', ebMarginMinValue);
    formData.append('eb_margin_max', ebMarginMaxValue);
    formData.append('eb_margin_years', ebMarginYears);

    formData.append('rev_growth_min', revGrowthMinValue);
    formData.append('rev_growth_max', revGrowthMaxValue);
    formData.append('rev_growth_years', revGrowthYears);

    formData.append('share_growth_min', shareGrowthMinValue);
    formData.append('share_growth_max', shareGrowthMaxValue);
    formData.append('share_growth_years', shareGrowthYears);

    formData.append('cash_flowgrowth_min', cashFlowGrowthMinValue);
    formData.append('cash_flowgrowth_max', cashFlowGrowthMaxValue);
    formData.append('cash_flowgrowth_years', cashFlowGrowthYears);

    formData.append('free_cash_flow_min', freeCashFlowGrowthMinValue);

    formData.append('free_cash_flow_max', freeCashFlowGrowthMaxValue);

    formData.append('free_cash_flow_years', freeCashFlowGrowthYears);

    formData.append('mar_cap_min', marketCapMinValue);
    formData.append('mar_cap_max', marketCapMaxValue);
    formData.append('mar_cap_years', marketCapYears);

    formData.append('ent_val_min', entValMinValue);
    formData.append('ent_val_max', entValMaxValue);
    formData.append('ent_val_years', entValYears);

    formData.append('net_debt_min', netDebtMinValue);
    formData.append('net_debt_max', netDebtMaxValue);
    formData.append('net_debt_years', netDebtYears);

    formData.append('ev_ebi_min', evEbiMinValue);
    formData.append('ev_ebi_max', evEbiMaxValue);
    formData.append('ev_ebi_years', evEbiYears);

    formData.append('long_term_min', longTermMinValue);
    formData.append('long_term_max', longTermMaxValue);
    formData.append('long_term_years', longTermYears);

    formData.append('cur_ratio_min', currentRatioMinValue);
    formData.append('cur_ratio_max', currentRatioMaxValue);
    formData.append('cur_ratio_years', currentRatioYears);

    formData.append('pe_ratio_min', peRatioMinValue);
    formData.append('pe_ratio_max', peRatioMaxValue);
    formData.append('pe_ratio_years', peRatioYears);

    formData.append('peg_ratio_min', pegRatioMinValue);
    formData.append('peg_ratio_max', pegRatioMaxValue);
    formData.append('peg_ratio_years', pegRatioYears);

    formData.append('po_cash_min', poCashMinValue);
    formData.append('po_cash_max', poCashMaxValue);
    formData.append('po_cash_years', poCashYears);

    formData.append('pf_cash_min', pfCashMinValue);
    formData.append('pf_cash_max', pfCashMaxValue);
    formData.append('pf_cash_years', pfCashYears);

    formData.append('industry', '');
    formData.append('company', '');

    return formData;
  };

  const checkFormConditions = () => {
    if (
      handleFormCondition(eqMinValue, eqMaxValue, eqYears) &&
      handleFormCondition(assestsMinValue, assestsMaxValue, assestsYears) &&
      handleFormCondition(netProMinValue, netProMaxValue, netProYears) &&
      handleFormCondition(opCashMinValue, opCashMaxValue, opCashYears) &&
      handleFormCondition(freeCashMinValue, freeCashMaxValue, freeCashYears) &&
      handleFormCondition(ebMarginMinValue, ebMarginMaxValue, ebMarginYears) &&
      handleFormCondition(
        revGrowthMinValue,
        revGrowthMaxValue,
        revGrowthYears,
      ) &&
      handleFormCondition(
        shareGrowthMinValue,
        shareGrowthMaxValue,
        shareGrowthYears,
      ) &&
      handleFormCondition(
        cashFlowGrowthMinValue,
        cashFlowGrowthMaxValue,
        cashFlowGrowthYears,
      ) &&
      handleFormCondition(
        freeCashFlowGrowthMinValue,
        freeCashFlowGrowthMaxValue,
        freeCashFlowGrowthYears,
      ) &&
      handleFormCondition(
        marketCapMinValue,
        marketCapMaxValue,
        marketCapYears,
      ) &&
      handleFormCondition(entValMinValue, entValMaxValue, entValYears) &&
      handleFormCondition(netDebtMinValue, netDebtMaxValue, netDebtYears) &&
      handleFormCondition(evEbiMinValue, evEbiMaxValue, evEbiYears) &&
      handleFormCondition(longTermMinValue, longTermMaxValue, longTermYears) &&
      handleFormCondition(
        currentRatioMinValue,
        currentRatioMaxValue,
        currentRatioYears,
      ) &&
      handleFormCondition(peRatioMinValue, peRatioMaxValue, peRatioYears) &&
      handleFormCondition(pegRatioMinValue, pegRatioMaxValue, pegRatioYears) &&
      handleFormCondition(poCashMinValue, poCashMaxValue, poCashYears) &&
      handleFormCondition(pfCashMinValue, pfCashMaxValue, pfCashYears)
    ) {
      return true;
    } else {
      return false;
    }
  };

  const handleFormCondition = (min, max, years) => {
    if (
      (!min && !max && !years) ||
      (handleFieldValidation(min) &&
        handleFieldValidation(max) &&
        handleFieldValidation(years, 'numeric'))
    ) {
      handleErrorValidation();

      return true;
    } else {
      return false;
    }
  };

  const handleErrorValidation = () => {
    let error = validationError;

    if (
      (!eqMinValue && !eqMaxValue && !eqYears) ||
      (handleFieldValidation(eqMinValue) &&
        handleFieldValidation(eqMaxValue) &&
        handleFieldValidation(eqYears, 'numeric'))
    ) {
      error.eqmin = 'no';
      error.eqmax = 'no';
      error.eqyears = 'no';

      handleFieldValidation(eqMinValue) &&
      handleFieldValidation(eqMaxValue) &&
      handleFieldValidation(eqYears, 'numeric')
        ? setEqStatus(1)
        : setEqStatus(0);
    } else {
      error.eqmin = handleFieldValidation(eqMinValue) ? 'no' : 'yes';
      error.eqmax = handleFieldValidation(eqMaxValue) ? 'no' : 'yes';
      error.eqyears = handleFieldValidation(eqYears, 'numeric') ? 'no' : 'yes';

      setEqStatus(0);
    }

    if (
      (!assestsMinValue && !assestsMaxValue && !assestsYears) ||
      (handleFieldValidation(assestsMinValue) &&
        handleFieldValidation(assestsMaxValue) &&
        handleFieldValidation(assestsYears, 'numeric'))
    ) {
      error.assetmin = 'no';
      error.assetmax = 'no';
      error.assetyears = 'no';

      handleFieldValidation(assestsMinValue) &&
      handleFieldValidation(assestsMaxValue) &&
      handleFieldValidation(assestsYears, 'numeric')
        ? setassestsStatus(1)
        : setassestsStatus(0);
    } else {
      error.assetmin = handleFieldValidation(assestsMinValue) ? 'no' : 'yes';
      error.assetmax = handleFieldValidation(assestsMaxValue) ? 'no' : 'yes';
      error.assetyears = handleFieldValidation(assestsYears, 'numeric')
        ? 'no'
        : 'yes';

      setassestsStatus(0);
    }

    if (
      (!netProMinValue && !netProMaxValue && !netProYears) ||
      (handleFieldValidation(netProMinValue) &&
        handleFieldValidation(netProMaxValue) &&
        handleFieldValidation(netProYears, 'numeric'))
    ) {
      error.netPromin = 'no';
      error.netPromax = 'no';
      error.netProyears = 'no';

      handleFieldValidation(netProMinValue) &&
      handleFieldValidation(netProMaxValue) &&
      handleFieldValidation(netProYears, 'numeric')
        ? setnetProStatus(1)
        : setnetProStatus(0);
    } else {
      error.netPromin = handleFieldValidation(netProMinValue) ? 'no' : 'yes';
      error.netPromax = handleFieldValidation(netProMaxValue) ? 'no' : 'yes';
      error.netProyears = handleFieldValidation(netProYears, 'numeric')
        ? 'no'
        : 'yes';

      setnetProStatus(0);
    }

    if (
      (!opCashMinValue && !opCashMaxValue && !opCashYears) ||
      (handleFieldValidation(opCashMinValue) &&
        handleFieldValidation(opCashMaxValue) &&
        handleFieldValidation(opCashYears, 'numeric'))
    ) {
      error.opCashmin = 'no';
      error.opCashmax = 'no';
      error.opCashyears = 'no';

      handleFieldValidation(opCashMinValue) &&
      handleFieldValidation(opCashMaxValue) &&
      handleFieldValidation(opCashYears, 'numeric')
        ? setopCashStatus(1)
        : setopCashStatus(0);
    } else {
      error.opCashmin = handleFieldValidation(opCashMinValue) ? 'no' : 'yes';
      error.opCashmax = handleFieldValidation(opCashMaxValue) ? 'no' : 'yes';
      error.opCashyears = handleFieldValidation(opCashYears, 'numeric')
        ? 'no'
        : 'yes';

      setopCashStatus(0);
    }

    if (
      (!freeCashMinValue && !freeCashMaxValue && !freeCashYears) ||
      (handleFieldValidation(freeCashMinValue) &&
        handleFieldValidation(freeCashMaxValue) &&
        handleFieldValidation(freeCashYears, 'numeric'))
    ) {
      error.freeCashmin = 'no';
      error.freeCashmax = 'no';
      error.freeCashyears = 'no';

      handleFieldValidation(freeCashMinValue) &&
      handleFieldValidation(freeCashMaxValue) &&
      handleFieldValidation(freeCashYears, 'numeric')
        ? setfreeCashStatus(1)
        : setfreeCashStatus(0);
    } else {
      error.freeCashmin = handleFieldValidation(freeCashMinValue)
        ? 'no'
        : 'yes';
      error.freeCashmax = handleFieldValidation(freeCashMaxValue)
        ? 'no'
        : 'yes';
      error.freeCashyears = handleFieldValidation(freeCashYears, 'numeric')
        ? 'no'
        : 'yes';

      setfreeCashStatus(0);
    }

    if (
      (!ebMarginMinValue && !ebMarginMaxValue && !ebMarginYears) ||
      (handleFieldValidation(ebMarginMinValue) &&
        handleFieldValidation(ebMarginMaxValue) &&
        handleFieldValidation(ebMarginYears, 'numeric'))
    ) {
      error.ebMarginmin = 'no';
      error.ebMarginmax = 'no';
      error.ebMarginyears = 'no';

      handleFieldValidation(ebMarginMinValue) &&
      handleFieldValidation(ebMarginMaxValue) &&
      handleFieldValidation(ebMarginYears, 'numeric')
        ? setebMarginStatus(1)
        : setebMarginStatus(0);
    } else {
      error.ebMarginmin = handleFieldValidation(ebMarginMinValue)
        ? 'no'
        : 'yes';
      error.ebMarginmax = handleFieldValidation(ebMarginMaxValue)
        ? 'no'
        : 'yes';
      error.ebMarginyears = handleFieldValidation(ebMarginYears, 'numeric')
        ? 'no'
        : 'yes';

      setebMarginStatus(0);
    }

    if (
      (!revGrowthMinValue && !revGrowthMaxValue && !revGrowthYears) ||
      (handleFieldValidation(revGrowthMinValue) &&
        handleFieldValidation(revGrowthMaxValue) &&
        handleFieldValidation(revGrowthYears, 'numeric'))
    ) {
      error.revGrowthmin = 'no';
      error.revGrowthmax = 'no';
      error.revGrowthyears = 'no';

      handleFieldValidation(revGrowthMinValue) &&
      handleFieldValidation(revGrowthMaxValue) &&
      handleFieldValidation(revGrowthYears, 'numeric')
        ? setrevGrowthStatus(1)
        : setrevGrowthStatus(0);
    } else {
      error.revGrowthmin = handleFieldValidation(revGrowthMinValue)
        ? 'no'
        : 'yes';
      error.revGrowthmax = handleFieldValidation(revGrowthMaxValue)
        ? 'no'
        : 'yes';
      error.revGrowthyears = handleFieldValidation(revGrowthYears, 'numeric')
        ? 'no'
        : 'yes';

      setrevGrowthStatus(0);
    }

    if (
      (!shareGrowthMinValue && !shareGrowthMaxValue && !shareGrowthYears) ||
      (handleFieldValidation(shareGrowthMinValue) &&
        handleFieldValidation(shareGrowthMaxValue) &&
        handleFieldValidation(shareGrowthYears, 'numeric'))
    ) {
      error.shareGrowthmin = 'no';
      error.shareGrowthmax = 'no';
      error.shareGrowthyears = 'no';

      handleFieldValidation(shareGrowthMinValue) &&
      handleFieldValidation(shareGrowthMaxValue) &&
      handleFieldValidation(shareGrowthYears, 'numeric')
        ? setshareGrowthStatus(1)
        : setshareGrowthStatus(0);
    } else {
      error.shareGrowthmin = handleFieldValidation(shareGrowthMinValue)
        ? 'no'
        : 'yes';
      error.shareGrowthmax = handleFieldValidation(shareGrowthMaxValue)
        ? 'no'
        : 'yes';
      error.shareGrowthyears = handleFieldValidation(
        shareGrowthYears,
        'numeric',
      )
        ? 'no'
        : 'yes';

      setshareGrowthStatus(0);
    }

    if (
      (!cashFlowGrowthMinValue &&
        !cashFlowGrowthMaxValue &&
        !cashFlowGrowthYears) ||
      (handleFieldValidation(cashFlowGrowthMinValue) &&
        handleFieldValidation(cashFlowGrowthMaxValue) &&
        handleFieldValidation(cashFlowGrowthYears, 'numeric'))
    ) {
      error.cashFlowGrowthmin = 'no';
      error.cashFlowGrowthmax = 'no';
      error.cashFlowGrowthyears = 'no';

      handleFieldValidation(cashFlowGrowthMinValue) &&
      handleFieldValidation(cashFlowGrowthMaxValue) &&
      handleFieldValidation(cashFlowGrowthYears, 'numeric')
        ? setcashFlowGrowthStatus(1)
        : setcashFlowGrowthStatus(0);
    } else {
      error.cashFlowGrowthmin = handleFieldValidation(cashFlowGrowthMinValue)
        ? 'no'
        : 'yes';
      error.cashFlowGrowthmax = handleFieldValidation(cashFlowGrowthMaxValue)
        ? 'no'
        : 'yes';
      error.cashFlowGrowthyears = handleFieldValidation(
        cashFlowGrowthYears,
        'numeric',
      )
        ? 'no'
        : 'yes';

      setcashFlowGrowthStatus(0);
    }

    if (
      (!freeCashFlowGrowthMinValue &&
        !freeCashFlowGrowthMaxValue &&
        !freeCashFlowGrowthYears) ||
      (handleFieldValidation(freeCashFlowGrowthMinValue) &&
        handleFieldValidation(freeCashFlowGrowthMaxValue) &&
        handleFieldValidation(freeCashFlowGrowthYears, 'numeric'))
    ) {
      error.freeCashFlowGrowthmin = 'no';
      error.freeCashFlowGrowthmax = 'no';
      error.freeCashFlowGrowthyears = 'no';

      handleFieldValidation(freeCashFlowGrowthMinValue) &&
      handleFieldValidation(freeCashFlowGrowthMaxValue) &&
      handleFieldValidation(freeCashFlowGrowthYears, 'numeric')
        ? setfreeCashFlowGrowthStatus(1)
        : setfreeCashFlowGrowthStatus(0);
    } else {
      error.freeCashFlowGrowthmin = handleFieldValidation(
        freeCashFlowGrowthMinValue,
      )
        ? 'no'
        : 'yes';
      error.freeCashFlowGrowthmax = handleFieldValidation(
        freeCashFlowGrowthMaxValue,
      )
        ? 'no'
        : 'yes';
      error.freeCashFlowGrowthyears = handleFieldValidation(
        freeCashFlowGrowthYears,
        'numeric',
      )
        ? 'no'
        : 'yes';
      ('');

      setfreeCashFlowGrowthStatus(0);
    }

    if (
      (!marketCapMinValue && !marketCapMaxValue && !marketCapYears) ||
      (handleFieldValidation(marketCapMinValue) &&
        handleFieldValidation(marketCapMaxValue) &&
        handleFieldValidation(marketCapYears, 'numeric'))
    ) {
      error.marketCapmin = 'no';
      error.marketCapmax = 'no';
      error.marketCapyears = 'no';

      handleFieldValidation(marketCapMinValue) &&
      handleFieldValidation(marketCapMaxValue) &&
      handleFieldValidation(marketCapYears, 'numeric')
        ? setmarketCapStatus(1)
        : setmarketCapStatus(0);
    } else {
      error.marketCapmin = handleFieldValidation(marketCapMinValue)
        ? 'no'
        : 'yes';
      error.marketCapmax = handleFieldValidation(marketCapMaxValue)
        ? 'no'
        : 'yes';
      error.marketCapyears = handleFieldValidation(marketCapYears, 'numeric')
        ? 'no'
        : 'yes';

      setmarketCapStatus(0);
    }

    if (
      (!entValMinValue && !entValMaxValue && !entValYears) ||
      (handleFieldValidation(entValMinValue) &&
        handleFieldValidation(entValMaxValue) &&
        handleFieldValidation(entValYears, 'numeric'))
    ) {
      error.entValmin = 'no';
      error.entValmax = 'no';
      error.entValyears = 'no';

      handleFieldValidation(entValMinValue) &&
      handleFieldValidation(entValMaxValue) &&
      handleFieldValidation(entValYears, 'numeric')
        ? setentValStatus(1)
        : setentValStatus(0);
    } else {
      error.entValmin = handleFieldValidation(entValMinValue) ? 'no' : 'yes';
      error.entValmax = handleFieldValidation(entValMaxValue) ? 'no' : 'yes';
      error.entValyears = handleFieldValidation(entValYears, 'numeric')
        ? 'no'
        : 'yes';

      setentValStatus(0);
    }

    if (
      (!netDebtMinValue && !netDebtMaxValue && !netDebtYears) ||
      (handleFieldValidation(netDebtMinValue) &&
        handleFieldValidation(netDebtMaxValue) &&
        handleFieldValidation(netDebtYears, 'numeric'))
    ) {
      error.netDebtmin = 'no';
      error.netDebtmax = 'no';
      error.netDebtyears = 'no';

      handleFieldValidation(netDebtMinValue) &&
      handleFieldValidation(netDebtMaxValue) &&
      handleFieldValidation(netDebtYears, 'numeric')
        ? setnetDebtStatus(1)
        : setnetDebtStatus(0);
    } else {
      error.netDebtmin = handleFieldValidation(netDebtMinValue) ? 'no' : 'yes';
      error.netDebtmax = handleFieldValidation(netDebtMaxValue) ? 'no' : 'yes';
      error.netDebtyears = handleFieldValidation(netDebtYears, 'numeric')
        ? 'no'
        : 'yes';

      setnetDebtStatus(0);
    }

    if (
      (!evEbiMinValue && !evEbiMaxValue && !evEbiYears) ||
      (handleFieldValidation(evEbiMinValue) &&
        handleFieldValidation(evEbiMaxValue) &&
        handleFieldValidation(evEbiYears, 'numeric'))
    ) {
      error.evEbimin = 'no';
      error.evEbimax = 'no';
      error.evEbiyears = 'no';

      handleFieldValidation(evEbiMinValue) &&
      handleFieldValidation(evEbiMaxValue) &&
      handleFieldValidation(evEbiYears, 'numeric')
        ? setevEbiStatus(1)
        : setevEbiStatus(0);
    } else {
      error.evEbimin = handleFieldValidation(evEbiMinValue) ? 'no' : 'yes';
      error.evEbimax = handleFieldValidation(evEbiMaxValue) ? 'no' : 'yes';
      error.evEbiyears = handleFieldValidation(evEbiYears, 'numeric')
        ? 'no'
        : 'yes';

      setevEbiStatus(0);
    }

    if (
      (!longTermMinValue && !longTermMaxValue && !longTermYears) ||
      (handleFieldValidation(longTermMinValue) &&
        handleFieldValidation(longTermMaxValue) &&
        handleFieldValidation(longTermYears, 'numeric'))
    ) {
      error.longTermmin = 'no';
      error.longTermmax = 'no';
      error.longTermyears = 'no';

      handleFieldValidation(longTermMinValue) &&
      handleFieldValidation(longTermMaxValue) &&
      handleFieldValidation(longTermYears, 'numeric')
        ? setlongTermStatus(1)
        : setlongTermStatus(0);
    } else {
      error.longTermmin = handleFieldValidation(longTermMinValue)
        ? 'no'
        : 'yes';
      error.longTermmax = handleFieldValidation(longTermMaxValue)
        ? 'no'
        : 'yes';
      error.longTermyears = handleFieldValidation(longTermYears, 'numeric')
        ? 'no'
        : 'yes';

      setlongTermStatus(0);
    }

    if (
      (!currentRatioMinValue && !currentRatioMaxValue && !currentRatioYears) ||
      (handleFieldValidation(currentRatioMinValue) &&
        handleFieldValidation(currentRatioMaxValue) &&
        handleFieldValidation(currentRatioYears))
    ) {
      error.currentRatiomin = 'no';
      error.currentRatiomax = 'no';
      error.currentRatioyears = 'no';

      handleFieldValidation(currentRatioMinValue) &&
      handleFieldValidation(currentRatioMaxValue) &&
      handleFieldValidation(currentRatioYears, 'numeric')
        ? setcurrentRatioStatus(1)
        : setcurrentRatioStatus(0);
    } else {
      error.currentRatiomin = handleFieldValidation(currentRatioMinValue)
        ? 'no'
        : 'yes';
      error.currentRatiomax = handleFieldValidation(currentRatioMaxValue)
        ? 'no'
        : 'yes';
      error.currentRatioyears = handleFieldValidation(
        currentRatioYears,
        'numeric',
      )
        ? 'no'
        : 'yes';

      setcurrentRatioStatus(0);
    }

    if (
      (!peRatioMinValue && !peRatioMaxValue && !peRatioYears) ||
      (handleFieldValidation(peRatioMinValue) &&
        handleFieldValidation(peRatioMaxValue) &&
        handleFieldValidation(peRatioYears, 'numeric'))
    ) {
      error.peRatiomin = 'no';
      error.peRatiomax = 'no';
      error.peRatioyears = 'no';

      handleFieldValidation(peRatioMinValue) &&
      handleFieldValidation(peRatioMaxValue) &&
      handleFieldValidation(peRatioYears, 'numeric')
        ? setpeRatioStatus(1)
        : setpeRatioStatus(0);
    } else {
      error.peRatiomin = handleFieldValidation(peRatioMinValue) ? 'no' : 'yes';
      error.peRatiomax = handleFieldValidation(peRatioMaxValue) ? 'no' : 'yes';
      error.peRatioyears = handleFieldValidation(peRatioYears, 'numeric')
        ? 'no'
        : 'yes';

      setpeRatioStatus(0);
    }

    if (
      (!pegRatioMinValue && !pegRatioMaxValue && !pegRatioYears) ||
      (handleFieldValidation(pegRatioMinValue) &&
        handleFieldValidation(pegRatioMaxValue) &&
        handleFieldValidation(pegRatioYears, 'numeric'))
    ) {
      error.pegRatiomin = 'no';
      error.pegRatiomax = 'no';
      error.pegRatioyears = 'no';

      handleFieldValidation(pegRatioMinValue) &&
      handleFieldValidation(pegRatioMaxValue) &&
      handleFieldValidation(pegRatioYears, 'numeric')
        ? setpegRatioStatus(1)
        : setpegRatioStatus(0);
    } else {
      error.pegRatiomin = handleFieldValidation(pegRatioMinValue)
        ? 'no'
        : 'yes';
      error.pegRatiomax = handleFieldValidation(pegRatioMaxValue)
        ? 'no'
        : 'yes';
      error.pegRatioyears = handleFieldValidation(pegRatioYears, 'numeric')
        ? 'no'
        : 'yes';

      setpegRatioStatus(0);
    }

    if (
      (!poCashMinValue && !poCashMaxValue && !poCashYears) ||
      (handleFieldValidation(poCashMinValue) &&
        handleFieldValidation(poCashMaxValue) &&
        handleFieldValidation(poCashYears, 'numeric'))
    ) {
      error.poCashmin = 'no';
      error.poCashmax = 'no';
      error.poCashyears = 'no';

      handleFieldValidation(poCashMinValue) &&
      handleFieldValidation(poCashMaxValue) &&
      handleFieldValidation(poCashYears, 'numeric')
        ? setpoCashStatus(1)
        : setpoCashStatus(0);
    } else {
      error.poCashmin = handleFieldValidation(poCashMinValue) ? 'no' : 'yes';
      error.poCashmax = handleFieldValidation(poCashMaxValue) ? 'no' : 'yes';
      error.poCashyears = handleFieldValidation(poCashYears, 'numeric')
        ? 'no'
        : 'yes';

      setpoCashStatus(0);
    }

    if (
      (!pfCashMinValue && !pfCashMaxValue && !pfCashYears) ||
      (handleFieldValidation(pfCashMinValue) &&
        handleFieldValidation(pfCashMaxValue) &&
        handleFieldValidation(pfCashYears, 'numeric'))
    ) {
      error.pfCashmin = 'no';
      error.pfCashmax = 'no';
      error.pfCashyears = 'no';

      handleFieldValidation(pfCashMinValue) &&
      handleFieldValidation(pfCashMaxValue) &&
      handleFieldValidation(pfCashYears, 'numeric')
        ? setpfCashStatus(1)
        : setpfCashStatus(0);
    } else {
      error.pfCashmin = handleFieldValidation(pfCashMinValue) ? 'no' : 'yes';
      error.pfCashmax = handleFieldValidation(pfCashMaxValue) ? 'no' : 'yes';
      error.pfCashyears = handleFieldValidation(pfCashYears, 'numeric')
        ? 'no'
        : 'yes';

      setpfCashStatus(0);
    }

    setValidationError({...validationError, ...error});
  };

  const handleFieldValidation = (value, type) => {
    if (type == 'numeric') {
      if (value > 0 && Number.isInteger(Number(value))) {
        return true;
      } else {
        return false;
      }
    } else {
      if (value > 0) {
        return true;
      } else {
        return false;
      }
    }
  };

  const postSelected = (symbol) => {
    Keyboard.dismiss();

    disallowed.includes(symbol)
      ? Snackbar.show({
          duration: Snackbar.LENGTH_SHORT,
          text: 'Symbol exsist in disallowed trades',
        })
      : [
          (setDisallowed([...disallowed, symbol]),
          setTempArr([...tempArr, symbol])),
        ];
  };

  const submitDisallowed = async () => {
    setDloading(true);

    let formData = new FormData();

    formData.append('symbol', JSON.stringify(disallowed));

    let res = await profile.postDisallowedTrade(token, formData);

    Snackbar.show({
      duration: Snackbar.DURATION_SHORT,
      text: res.message,
    });

    setTempArr([]);

    setDloading(false);
  };

  const removeChip = async (index) => {
    let removedArr = disallowed;

    removedArr.splice(index, 1);

    setDisallowed([...removedArr]);

    let formData = new FormData();

    formData.append('symbol', JSON.stringify(removedArr));

    await profile.postDisallowedTrade(token, formData);

    Snackbar.show({
      duration: Snackbar.DURATION_SHORT,
      text: 'Symbol removed successfully',
    });
  };

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : null}
      keyboardVerticalOffset={
        Platform.OS === 'ios' && Boolean(paddingStatus) ? 200 : 0
      }
      style={styles.container}>
      <ScrollView
        keyboardShouldPersistTaps={'handled'}
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}>
        {menu.map((item, index) => (
          <View key={index} style={{marginTop: index != 0 ? 8 : 0}}>
            <Accordian
              title={item.title}
              status={item.status}
              minValue={item.minValue}
              maxValue={item.maxValue}
              years={item.years}
              minValueChange={item.minValueChange}
              maxValueChange={item.maxValueChange}
              yearChange={item.yearChange}
              minError={item.minError}
              maxError={item.maxError}
              yearsError={item.yearsError}
              referenceMin={item.referenceMin}
              referenceMax={item.referenceMax}
              referenceYear={item.referenceYear}
              expanded={item.expanded}
              toggleExpand={item.toggleExpand}
            />
          </View>
        ))}

        <View style={styles.submitview}>
          <TouchableOpacity onPress={submitForm}>
            <Text style={styles.btntxt}>Submit</Text>
          </TouchableOpacity>
        </View>

        <View
          style={{
            backgroundColor: theme.primaryColor,
            borderRadius: 8,
            paddingHorizontal: 12,
            paddingVertical: 12,
          }}>
          <Text style={[styles.labelTxt, {fontWeight: '600'}]}>
            Disallowed Trade :
          </Text>

          <Search
            req={postSelected}
            onFocus={() => {
              setPaddingStatus(true);
            }}
            onBlur={() => {
              setPaddingStatus(false);
            }}
          />

          {disallowed.length != 0 && (
            <View
              style={{
                flexDirection: 'row',
                flexWrap: 'wrap',
                marginVertical: 8,
              }}>
              {disallowed.map((item, index) => (
                <View
                  key={index}
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    backgroundColor: tempArr.includes(item)
                      ? 'tomato'
                      : theme.secondryColor,
                    borderRadius: 24,
                    marginRight: 8,
                    marginTop: 4,
                    paddingHorizontal: 8,
                    paddingVertical: 8,
                  }}>
                  <Text
                    style={{
                      fontSize: 12,
                      color: theme.white,
                      fontWeight: '400',
                    }}>
                    {item}
                  </Text>

                  <TouchableOpacity
                    style={{
                      width: 16,
                      height: 16,
                      justifyContent: 'center',
                      alignItems: 'center',
                      backgroundColor: theme.black,
                      borderRadius: 16 / 2,
                      marginLeft: 4,
                    }}
                    onPress={() => removeChip(index)}>
                    <Ionicons name="close" color={theme.white} size={14} />
                  </TouchableOpacity>
                </View>
              ))}
            </View>
          )}

          {disArrLength != disallowed.length && tempArr.length != 0 && (
            <View
              style={{
                alignSelf: 'flex-end',
                backgroundColor: theme.secondryColor,
                borderRadius: 4,
                marginVertical: 8,
                paddingHorizontal: 16,
                paddingVertical: 8,
              }}>
              {dloading ? (
                <ActivityIndicator size="small" color="white" />
              ) : (
                <TouchableOpacity onPress={submitDisallowed}>
                  <Text style={styles.btntxt}>Submit</Text>
                </TouchableOpacity>
              )}
            </View>
          )}
        </View>

        <View style={{paddingVertical: 16}} />
      </ScrollView>

      <SuccessPopUp
        res={res}
        successText={successText}
        modal={modal}
        onBackdropPress={() => setModal(false)}
      />
    </KeyboardAvoidingView>
  );
};

export default Performance;
