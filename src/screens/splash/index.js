import AsyncStorage from '@react-native-community/async-storage';
import {StackActions, useFocusEffect} from '@react-navigation/native';
import React, {useCallback} from 'react';
import {ImageBackground, View} from 'react-native';
import DeviceInfo from 'react-native-device-info';
import {auth} from '../../api';
import {SPLASH_IMAGE} from '../../assets/images';
import {MPINSTATUS} from '../../utils/constants';
import style from './styles.js';

const SplashScreen = ({navigation}) => {
  let deviceId = DeviceInfo.getUniqueId();

  useFocusEffect(
    useCallback(() => {
      let ac = new AbortController();

      const getUser = async () => {
        let formData = new FormData();
        formData && formData.append('unique_id', deviceId);
        let response = await auth.getMpinStatus(formData);
        await AsyncStorage.setItem(
          MPINSTATUS,
          JSON.stringify(response.data.enabled),
        );
        try {
          let mPIN = response.data.enabled;
          if (mPIN == true) {
            changeScreen(true);
          } else {
            changeScreen(false);
          }
        } catch (err) {
          changeScreen(false);
        }
      };

      getUser();

      return () => {
        ac.abort();
      };
    }, [navigation]),
  );

  const changeScreen = (isRegistered) =>
    navigation.dispatch(
      StackActions.replace(isRegistered ? 'MpinScreen' : 'Login'),
    );

  return (
    <View style={style.container}>
      <ImageBackground source={SPLASH_IMAGE} style={style.backgroundImage}>
        <View style={style.view}></View>
      </ImageBackground>
    </View>
  );
};

export default SplashScreen;
