import React, {useContext, useState} from 'react';
import {Image, StyleSheet, TouchableOpacity, View} from 'react-native';
import Snackbar from 'react-native-snackbar';
import {company} from '../../api';
import {BEAR, BULL, NEUTRAL} from '../../assets/images/index';
import {UserDataContext} from '../../context/UserDataContext';
import theme from '../../utils/theme';
import ResPopUp from './ResPopup';

const CompanyRate = ({hide}) => {
  const {symbol, token, rating, setRating} = useContext(UserDataContext);
  const [modal, setModal] = useState(false);
  const [res, setRes] = useState({});
  const [loading, setLoading] = useState(false);

  const postRating = async (value) => {
    setModal(true);
    setLoading(true);

    let formData = new FormData();

    formData.append('symbol', symbol);
    formData.append('status', value);

    let res = await company.sendRatings(formData, token);

    if (res) {
      if (res.keyword == 'success') {
        setRes(res);
        setLoading(false);
        setRating(res.data.status);
        setTimeout(() => hide(false), 1000);
      } else {
        setRes(res);
        setLoading(false);
        setRating(res.data.status);
        setTimeout(() => hide(false), 1000);
      }
    } else {
    }
  };

  return (
    <>
      {modal ? (
        <ResPopUp res={res} modal={modal} loading={loading} />
      ) : (
        <View
          style={{
            backgroundColor: theme.secondryColor,
            paddingHorizontal: 8,
            paddingVertical: 8,
            borderBottomLeftRadius: 4,
            borderBottomRightRadius: 4,
          }}>
          <TouchableOpacity
            style={{marginVertical: 4}}
            onPress={() => postRating('bull')}>
            <View
              style={
                rating == 'bull'
                  ? styles.companyrateactive
                  : styles.companyrateinactive
              }>
              <Image
                source={BULL}
                style={{
                  width: 28,
                  height: 28,
                  borderRadius: 28 / 2,
                }}
              />
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            style={{marginVertical: 4}}
            onPress={() => postRating('bear')}>
            <View
              style={
                rating == 'bear'
                  ? styles.companyrateactive
                  : styles.companyrateinactive
              }>
              <Image
                source={BEAR}
                style={{
                  width: 28,
                  height: 28,
                  borderRadius: 28 / 2,
                }}
              />
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            style={{marginVertical: 4}}
            onPress={() => postRating('neutral')}>
            <View
              style={
                rating == 'neutral'
                  ? styles.companyrateactive
                  : styles.companyrateinactive
              }>
              <Image
                source={NEUTRAL}
                style={{
                  width: 28,
                  height: 28,
                  borderRadius: 28 / 2,
                }}
              />
            </View>
          </TouchableOpacity>
        </View>
      )}
    </>
  );
};
const styles = StyleSheet.create({
  companyrateactive: {
    width: 40,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: 'blue',
    borderWidth: 3,
    borderRadius: 40 / 2,
  },
  companyrateinactive: {
    width: 40,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 40 / 2,
  },
});
export default CompanyRate;
