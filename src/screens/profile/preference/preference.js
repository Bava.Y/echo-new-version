import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import _ from 'lodash';
import React from 'react';
import {View} from 'react-native';
import theme from '../../../utils/theme';
import MyTabBar from './../components/preferenceTab';
import Account from './account';
import Echo from './echo';
import FinancialTool from './financial-tools';
import Performance from './performance';
import styles from './styles';
import TrackingError from './tracking-errors';

const PreferencesScreen = ({preferences}) => {
  // PreferencesScreen Variables
  const Tab = createMaterialTopTabNavigator();

  return (
    <View style={styles.container}>
      <Tab.Navigator
        style={{backgroundColor: theme.primaryColor}}
        sceneContainerStyle={{
          backgroundColor: theme.themeColor,
          paddingHorizontal: 8,
          paddingVertical: 8,
        }}
        tabBar={(props) => <MyTabBar {...props} />}
        backBehavior="initialRoute"
        initialRouteName="Account"
        animationEnabled={true}
        swipeEnabled={true} //can change dynamically
        tabBarOptions={{scrollEnabled: true}}>
        <Tab.Screen name="Account">
          {() => (
            <Account preferences={_.isNull(preferences) ? {} : preferences} />
          )}
        </Tab.Screen>

        <Tab.Screen name="Performance" component={Performance} />

        <Tab.Screen name="Echo">
          {() => (
            <Echo preferences={_.isNull(preferences) ? {} : preferences} />
          )}
        </Tab.Screen>

        <Tab.Screen
          name="Tracking Errors"
          component={TrackingError}></Tab.Screen>

        {/* <Tab.Screen name="Financial Tools">
          {() => (
            <FinancialTool
              preferences={_.isNull(preferences) ? {} : preferences}
            />
          )}
        </Tab.Screen> */}
      </Tab.Navigator>
    </View>
  );
};

export default PreferencesScreen;
