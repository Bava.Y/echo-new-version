import {useFocusEffect} from '@react-navigation/native';
import _ from 'lodash';
import moment from 'moment';
import React, {useContext, useRef, useState} from 'react';
import {
  ActivityIndicator,
  Dimensions,
  Image,
  Platform,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Snackbar from 'react-native-snackbar';
import Icon from 'react-native-vector-icons/FontAwesome';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {ALPACA, AMERITRADE} from '../../assets/images/index';
import BottomNav from '../../components/BottomNav';
import HeaderNav from '../../components/HeaderNav';
import Loading from '../../components/Loading';
import {UserDataContext} from '../../context/UserDataContext';
import axiosInstance from '../../utils/Apicall';
import theme from '../../utils/theme';
import {PROFILE} from './../../assets/images/index';
import CompanyList from './CompanyList';
import styles from './style';

const TrackingErrorHistory = (props) => {
  // Props Variables
  const [echoerData] = useState(props.route.params.echoerData);

  // TrackingErrorHistory Variables
  const [responseData, setResponseData] = useState(null);
  const [trackingErrorEchoersData, setTrackingErrorEchoersData] = useState(
    null,
  );

  // Ref Variables
  const filterRef = useRef();

  // Context Variables
  const {
    alpacaConfigStatus,
    tdConfigStatus,
    token,
    trackingHistoryConnection,
  } = useContext(UserDataContext);

  // Other Variables
  const deviceHeight = Dimensions.get('window').height;

  useFocusEffect(
    React.useCallback(() => {
      let screenRefreshStatus = true;

      fetchTrackingHistoryErrors();

      return () => {
        screenRefreshStatus = false;
      };
    }, [trackingHistoryConnection]),
  );

  const fetchTrackingHistoryErrors = async () => {
    setTrackingErrorEchoersData(null);

    const response = await axiosInstance.get(
      `trackingErrorHistory/${echoerData.followed_id}/${echoerData.customer_id}/${trackingHistoryConnection}`,
      {
        headers: {
          Authorization: token,
        },
      },
    );

    if (
      (response.status =
        200 &&
        response.data &&
        response.data.keyword.toLowerCase() === 'success')
    ) {
      if (
        !_.isEmpty(response.data) &&
        response.data.keyword.toLowerCase() === 'success'
      ) {
        setResponseData(response.data.data);

        setTrackingErrorEchoersData(response.data.data.history);
      } else {
        alert(response.data.message);
      }
    } else {
      Snackbar.show({
        text: 'Error fetching history of tracking errors!',
        duration: Snackbar.LENGTH_SHORT,
      });
    }
  };

  return (
    <>
      <HeaderNav title="History of Tracking Errors" isBack="true" />

      <View style={styles.container}>
        {Boolean(responseData) ? (
          <ScrollView
            keyboardShouldPersistTaps={'handled'}
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}>
            <View style={{flex: 1, backgroundColor: theme.themeColor}}>
              <View style={{paddingHorizontal: 16, paddingVertical: 8}}>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}>
                  <Image
                    style={{
                      width: 40,
                      height: 40,
                      borderRadius: 40 / 2,
                    }}
                    source={
                      Boolean(echoerData.photo)
                        ? {uri: echoerData.photo}
                        : PROFILE
                    }
                  />

                  <View style={{justifyContent: 'center', marginHorizontal: 8}}>
                    <Text
                      style={{
                        fontSize: Platform.OS == 'ios' ? 14 : 16,
                        color: theme.white,
                        fontWeight: '600',
                      }}
                      numberOfLines={1}
                      ellipsizeMode="tail">
                      {`${echoerData.first_name} ${echoerData.last_name}`}
                    </Text>

                    {Boolean(echoerData.designation) && (
                      <Text
                        style={{
                          fontSize: Platform.OS == 'ios' ? 12 : 14,
                          color: '#ffffff8a',
                          fontWeight: '400',
                        }}>
                        {echoerData.designation}
                      </Text>
                    )}
                  </View>
                </View>

                {alpacaConfigStatus && (
                  <View style={{marginVertical: 8}}>
                    <Text
                      style={{
                        fontSize: Platform.OS == 'ios' ? 14 : 16,
                        fontWeight: 'bold',
                        color: theme.secondryColor,
                      }}>
                      Alpaca
                    </Text>

                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        marginTop: 8,
                      }}>
                      <View
                        style={{
                          flexDirection: 'row',
                          justifyContent: 'center',
                        }}>
                        <Text
                          style={{
                            fontSize: Platform.OS == 'ios' ? 12 : 14,
                            color: theme.white,
                            fontWeight: '600',
                          }}>
                          Allocated Fund (
                          {`${parseFloat(echoerData.percentage).toFixed(2)}%`})
                        </Text>

                        <TouchableOpacity style={{marginHorizontal: 8}}>
                          <Icon name="edit" color={theme.greenBg} size={16} />
                        </TouchableOpacity>
                      </View>

                      <Text
                        style={{
                          fontSize: Platform.OS == 'ios' ? 12 : 14,
                          color: theme.white,
                          fontWeight: '600',
                        }}>
                        {responseData.allocated_amount
                          ? `$ ${parseFloat(
                              responseData.allocated_amount,
                            ).toFixed(2)}`
                          : '$ 0.00'}
                      </Text>
                    </View>

                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        marginTop: 8,
                      }}>
                      <Text
                        style={{
                          fontSize: Platform.OS == 'ios' ? 12 : 14,
                          color: theme.white,
                          fontWeight: '600',
                        }}>
                        Available Fund
                      </Text>

                      <Text
                        style={{
                          fontSize: Platform.OS == 'ios' ? 12 : 14,
                          color: theme.white,
                          fontWeight: '600',
                        }}>
                        {responseData.balance_amount
                          ? `$ ${parseFloat(
                              responseData.balance_amount,
                            ).toFixed(2)}`
                          : '$ 0.00'}
                      </Text>
                    </View>
                  </View>
                )}

                {tdConfigStatus && (
                  <View style={{marginVertical: 8}}>
                    <Text
                      style={{
                        fontSize: Platform.OS == 'ios' ? 14 : 16,
                        fontWeight: 'bold',
                        color: theme.secondryColor,
                      }}>
                      TD Amirtrade
                    </Text>

                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        marginTop: 8,
                      }}>
                      <View
                        style={{flexDirection: 'row', alignItems: 'center'}}>
                        <Text
                          style={{
                            fontSize: Platform.OS == 'ios' ? 12 : 14,
                            color: theme.white,
                            fontWeight: '600',
                          }}>
                          Allocated Fund
                          {responseData.td_percentage
                            ? ` ${parseFloat(
                                responseData.td_percentage,
                              ).toFixed(2)}%`
                            : null}
                        </Text>

                        <TouchableOpacity style={{marginHorizontal: 8}}>
                          <Icon name="edit" color={theme.greenBg} size={16} />
                        </TouchableOpacity>
                      </View>

                      <Text
                        style={{
                          fontSize: Platform.OS == 'ios' ? 12 : 14,
                          color: theme.white,
                          fontWeight: '600',
                        }}>
                        {responseData.td_allocated_amount
                          ? `$ ${parseFloat(
                              responseData.td_allocated_amount,
                            ).toFixed(2)}`
                          : '$ 0.00'}
                      </Text>
                    </View>

                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        marginTop: 8,
                      }}>
                      <Text
                        style={{
                          fontSize: Platform.OS == 'ios' ? 12 : 14,
                          color: theme.white,
                          fontWeight: '600',
                        }}>
                        Available Fund
                      </Text>
                      <Text
                        style={{
                          fontSize: Platform.OS == 'ios' ? 12 : 14,
                          color: theme.white,
                          fontWeight: '600',
                        }}>
                        {responseData.td_balance_amount
                          ? `$ ${parseFloat(
                              responseData.td_balance_amount,
                            ).toFixed(2)}`
                          : '$ 0.00'}
                      </Text>
                    </View>
                  </View>
                )}
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  backgroundColor: theme.primaryColor,
                  marginVertical: 8,
                  paddingHorizontal: 16,
                  paddingVertical: 8,
                }}>
                <Text
                  style={{
                    fontSize: Platform.OS == 'ios' ? 14 : 16,
                    color: theme.white,
                    fontWeight: '600',
                  }}>
                  History of Tracking Errors
                </Text>

                <TouchableOpacity
                  onPress={() => {
                    filterRef.current.open();
                  }}>
                  <Ionicons name="filter" color={theme.white} size={20} />
                </TouchableOpacity>
              </View>

              <View style={{marginVertical: 8, paddingHorizontal: 16}}>
                <View
                  style={{
                    flex: 1,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    paddingBottom: 8,
                    borderBottomColor: 'grey',
                    borderBottomWidth: 1,
                  }}>
                  <View
                    style={{flex: 0.25, alignItems: 'flex-start', padding: 4}}>
                    <Text
                      style={{
                        fontSize: Platform.OS == 'ios' ? 12 : 14,
                        color: theme.secondryColor,
                        fontWeight: '600',
                      }}>
                      Date
                    </Text>
                  </View>

                  <View
                    style={{flex: 0.25, alignItems: 'flex-start', padding: 4}}>
                    <Text
                      style={{
                        fontSize: Platform.OS == 'ios' ? 12 : 14,
                        color: theme.secondryColor,
                        fontWeight: '600',
                      }}>
                      Broker
                    </Text>
                  </View>

                  <View
                    style={{flex: 0.2, alignItems: 'flex-start', padding: 4}}>
                    <Text
                      style={{
                        fontSize: Platform.OS == 'ios' ? 12 : 14,
                        color: theme.secondryColor,
                        fontWeight: '600',
                      }}>
                      Symbol
                    </Text>
                  </View>

                  <View style={{flex: 0.3, alignItems: 'flex-end', padding: 4}}>
                    <Text
                      style={{
                        fontSize: Platform.OS == 'ios' ? 12 : 14,
                        color: theme.secondryColor,
                        fontWeight: '600',
                      }}>
                      Percentage(%)
                    </Text>
                  </View>
                </View>

                {trackingErrorEchoersData ? (
                  trackingErrorEchoersData.length != 0 ? (
                    trackingErrorEchoersData.map((item, index) => (
                      <View
                        key={index}
                        style={{
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                          paddingVertical: 4,
                        }}>
                        <View
                          style={{
                            width: '25%',
                            alignItems: 'flex-start',
                            padding: 4,
                          }}>
                          <Text
                            style={{
                              fontSize: Platform.OS == 'ios' ? 12 : 14,
                              color: theme.white,
                              fontWeight: '400',
                            }}>
                            {moment(item.created_on).format('DD/MM/YYYY')}
                          </Text>
                        </View>

                        <View
                          style={{
                            width: '25%',
                            alignItems: 'flex-start',
                            padding: 4,
                          }}>
                          <Image
                            source={
                              item.broker_connection == 1 ? AMERITRADE : ALPACA
                            }
                            style={{
                              width: 60,
                              height: 20,
                              resizeMode: 'contain',
                            }}
                          />
                        </View>

                        <View
                          style={{
                            width: '20%',
                            alignItems: 'flex-start',
                            padding: 4,
                          }}>
                          <Text
                            style={{
                              fontSize: Platform.OS == 'ios' ? 12 : 14,
                              color: theme.white,
                              fontWeight: '400',
                            }}>
                            {item.symbol}
                          </Text>
                        </View>

                        <View
                          style={{
                            width: '30%',
                            alignItems: 'flex-end',
                            padding: 4,
                          }}>
                          <Text
                            style={{
                              fontSize: Platform.OS == 'ios' ? 12 : 14,
                              color: theme.white,
                              fontWeight: '400',
                            }}>
                            {`${parseFloat(item.percentage).toFixed(2)}%`}
                          </Text>
                        </View>
                      </View>
                    ))
                  ) : (
                    <View
                      style={{
                        height: deviceHeight * 0.5,
                        justifyContent: 'center',
                        alignItems: 'center',
                        marginVertical: 8,
                      }}>
                      <Text
                        style={{
                          fontSize: Platform.OS == 'ios' ? 12 : 14,
                          color: theme.white,
                          paddingHorizontal: 8,
                        }}>
                        No tracking error(s) found
                      </Text>
                    </View>
                  )
                ) : (
                  <View
                    style={{
                      height: deviceHeight * 0.5,
                      flexDirection: 'row',
                      justifyContent: 'center',
                      alignItems: 'center',
                      marginVertical: 8,
                    }}>
                    <ActivityIndicator size={24} color={theme.white} />
                    <Text
                      style={{
                        fontSize: Platform.OS == 'ios' ? 14 : 16,
                        color: theme.white,
                        paddingHorizontal: 8,
                      }}>
                      Loading
                    </Text>
                  </View>
                )}
              </View>
            </View>
          </ScrollView>
        ) : (
          <Loading color={theme.white} />
        )}
      </View>

      <CompanyList
        forStatus={'Tracking History'}
        filterRef={filterRef}
        onClose={() => filterRef.current.close()}
      />

      <BottomNav routeName="Discover People" />
    </>
  );
};

export default TrackingErrorHistory;
